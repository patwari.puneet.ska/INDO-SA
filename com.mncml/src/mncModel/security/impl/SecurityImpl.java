/**
 */
package mncModel.security.impl;

import mncModel.security.SecuredLink;
import mncModel.security.Security;
import mncModel.security.SecurityPackage;

import mncModel.utility.AuthorisationUtility;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Security</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link mncModel.security.impl.SecurityImpl#getAuthorisations <em>Authorisations</em>}</li>
 *   <li>{@link mncModel.security.impl.SecurityImpl#getSecuredLinks <em>Secured Links</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SecurityImpl extends MinimalEObjectImpl.Container implements Security {
	/**
	 * The cached value of the '{@link #getAuthorisations() <em>Authorisations</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAuthorisations()
	 * @generated
	 * @ordered
	 */
	protected AuthorisationUtility authorisations;

	/**
	 * The cached value of the '{@link #getSecuredLinks() <em>Secured Links</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSecuredLinks()
	 * @generated
	 * @ordered
	 */
	protected SecuredLink securedLinks;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SecurityImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SecurityPackage.Literals.SECURITY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AuthorisationUtility getAuthorisations() {
		return authorisations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAuthorisations(AuthorisationUtility newAuthorisations, NotificationChain msgs) {
		AuthorisationUtility oldAuthorisations = authorisations;
		authorisations = newAuthorisations;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, SecurityPackage.SECURITY__AUTHORISATIONS, oldAuthorisations, newAuthorisations);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAuthorisations(AuthorisationUtility newAuthorisations) {
		if (newAuthorisations != authorisations) {
			NotificationChain msgs = null;
			if (authorisations != null)
				msgs = ((InternalEObject)authorisations).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - SecurityPackage.SECURITY__AUTHORISATIONS, null, msgs);
			if (newAuthorisations != null)
				msgs = ((InternalEObject)newAuthorisations).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - SecurityPackage.SECURITY__AUTHORISATIONS, null, msgs);
			msgs = basicSetAuthorisations(newAuthorisations, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SecurityPackage.SECURITY__AUTHORISATIONS, newAuthorisations, newAuthorisations));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SecuredLink getSecuredLinks() {
		return securedLinks;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSecuredLinks(SecuredLink newSecuredLinks, NotificationChain msgs) {
		SecuredLink oldSecuredLinks = securedLinks;
		securedLinks = newSecuredLinks;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, SecurityPackage.SECURITY__SECURED_LINKS, oldSecuredLinks, newSecuredLinks);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSecuredLinks(SecuredLink newSecuredLinks) {
		if (newSecuredLinks != securedLinks) {
			NotificationChain msgs = null;
			if (securedLinks != null)
				msgs = ((InternalEObject)securedLinks).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - SecurityPackage.SECURITY__SECURED_LINKS, null, msgs);
			if (newSecuredLinks != null)
				msgs = ((InternalEObject)newSecuredLinks).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - SecurityPackage.SECURITY__SECURED_LINKS, null, msgs);
			msgs = basicSetSecuredLinks(newSecuredLinks, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SecurityPackage.SECURITY__SECURED_LINKS, newSecuredLinks, newSecuredLinks));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case SecurityPackage.SECURITY__AUTHORISATIONS:
				return basicSetAuthorisations(null, msgs);
			case SecurityPackage.SECURITY__SECURED_LINKS:
				return basicSetSecuredLinks(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case SecurityPackage.SECURITY__AUTHORISATIONS:
				return getAuthorisations();
			case SecurityPackage.SECURITY__SECURED_LINKS:
				return getSecuredLinks();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case SecurityPackage.SECURITY__AUTHORISATIONS:
				setAuthorisations((AuthorisationUtility)newValue);
				return;
			case SecurityPackage.SECURITY__SECURED_LINKS:
				setSecuredLinks((SecuredLink)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case SecurityPackage.SECURITY__AUTHORISATIONS:
				setAuthorisations((AuthorisationUtility)null);
				return;
			case SecurityPackage.SECURITY__SECURED_LINKS:
				setSecuredLinks((SecuredLink)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case SecurityPackage.SECURITY__AUTHORISATIONS:
				return authorisations != null;
			case SecurityPackage.SECURITY__SECURED_LINKS:
				return securedLinks != null;
		}
		return super.eIsSet(featureID);
	}

} //SecurityImpl
