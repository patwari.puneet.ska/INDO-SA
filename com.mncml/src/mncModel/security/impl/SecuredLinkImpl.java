/**
 */
package mncModel.security.impl;

import mncModel.security.SecuredLink;
import mncModel.security.SecurityPackage;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Secured Link</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class SecuredLinkImpl extends MinimalEObjectImpl.Container implements SecuredLink {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SecuredLinkImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SecurityPackage.Literals.SECURED_LINK;
	}

} //SecuredLinkImpl
