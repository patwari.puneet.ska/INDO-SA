/**
 */
package mncModel.security;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see mncModel.security.SecurityFactory
 * @model kind="package"
 * @generated
 */
public interface SecurityPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "security";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://mncModel/security/2.0";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "security";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	SecurityPackage eINSTANCE = mncModel.security.impl.SecurityPackageImpl.init();

	/**
	 * The meta object id for the '{@link mncModel.security.impl.AuthorisationImpl <em>Authorisation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mncModel.security.impl.AuthorisationImpl
	 * @see mncModel.security.impl.SecurityPackageImpl#getAuthorisation()
	 * @generated
	 */
	int AUTHORISATION = 0;

	/**
	 * The feature id for the '<em><b>Role</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AUTHORISATION__ROLE = 0;

	/**
	 * The feature id for the '<em><b>System Control</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AUTHORISATION__SYSTEM_CONTROL = 1;

	/**
	 * The feature id for the '<em><b>User Operations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AUTHORISATION__USER_OPERATIONS = 2;

	/**
	 * The number of structural features of the '<em>Authorisation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AUTHORISATION_FEATURE_COUNT = 3;

	/**
	 * The meta object id for the '{@link mncModel.security.impl.SecuredLinkImpl <em>Secured Link</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mncModel.security.impl.SecuredLinkImpl
	 * @see mncModel.security.impl.SecurityPackageImpl#getSecuredLink()
	 * @generated
	 */
	int SECURED_LINK = 1;

	/**
	 * The number of structural features of the '<em>Secured Link</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECURED_LINK_FEATURE_COUNT = 0;

	/**
	 * The meta object id for the '{@link mncModel.security.impl.ConfidentialityImpl <em>Confidentiality</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mncModel.security.impl.ConfidentialityImpl
	 * @see mncModel.security.impl.SecurityPackageImpl#getConfidentiality()
	 * @generated
	 */
	int CONFIDENTIALITY = 2;

	/**
	 * The number of structural features of the '<em>Confidentiality</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFIDENTIALITY_FEATURE_COUNT = 0;

	/**
	 * The meta object id for the '{@link mncModel.security.impl.SecurityImpl <em>Security</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mncModel.security.impl.SecurityImpl
	 * @see mncModel.security.impl.SecurityPackageImpl#getSecurity()
	 * @generated
	 */
	int SECURITY = 3;

	/**
	 * The feature id for the '<em><b>Authorisations</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECURITY__AUTHORISATIONS = 0;

	/**
	 * The feature id for the '<em><b>Secured Links</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECURITY__SECURED_LINKS = 1;

	/**
	 * The number of structural features of the '<em>Security</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECURITY_FEATURE_COUNT = 2;


	/**
	 * Returns the meta object for class '{@link mncModel.security.Authorisation <em>Authorisation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Authorisation</em>'.
	 * @see mncModel.security.Authorisation
	 * @generated
	 */
	EClass getAuthorisation();

	/**
	 * Returns the meta object for the reference '{@link mncModel.security.Authorisation#getRole <em>Role</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Role</em>'.
	 * @see mncModel.security.Authorisation#getRole()
	 * @see #getAuthorisation()
	 * @generated
	 */
	EReference getAuthorisation_Role();

	/**
	 * Returns the meta object for the containment reference '{@link mncModel.security.Authorisation#getSystemControl <em>System Control</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>System Control</em>'.
	 * @see mncModel.security.Authorisation#getSystemControl()
	 * @see #getAuthorisation()
	 * @generated
	 */
	EReference getAuthorisation_SystemControl();

	/**
	 * Returns the meta object for the containment reference list '{@link mncModel.security.Authorisation#getUserOperations <em>User Operations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>User Operations</em>'.
	 * @see mncModel.security.Authorisation#getUserOperations()
	 * @see #getAuthorisation()
	 * @generated
	 */
	EReference getAuthorisation_UserOperations();

	/**
	 * Returns the meta object for class '{@link mncModel.security.SecuredLink <em>Secured Link</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Secured Link</em>'.
	 * @see mncModel.security.SecuredLink
	 * @generated
	 */
	EClass getSecuredLink();

	/**
	 * Returns the meta object for class '{@link mncModel.security.Confidentiality <em>Confidentiality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Confidentiality</em>'.
	 * @see mncModel.security.Confidentiality
	 * @generated
	 */
	EClass getConfidentiality();

	/**
	 * Returns the meta object for class '{@link mncModel.security.Security <em>Security</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Security</em>'.
	 * @see mncModel.security.Security
	 * @generated
	 */
	EClass getSecurity();

	/**
	 * Returns the meta object for the containment reference '{@link mncModel.security.Security#getAuthorisations <em>Authorisations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Authorisations</em>'.
	 * @see mncModel.security.Security#getAuthorisations()
	 * @see #getSecurity()
	 * @generated
	 */
	EReference getSecurity_Authorisations();

	/**
	 * Returns the meta object for the containment reference '{@link mncModel.security.Security#getSecuredLinks <em>Secured Links</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Secured Links</em>'.
	 * @see mncModel.security.Security#getSecuredLinks()
	 * @see #getSecurity()
	 * @generated
	 */
	EReference getSecurity_SecuredLinks();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	SecurityFactory getSecurityFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link mncModel.security.impl.AuthorisationImpl <em>Authorisation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mncModel.security.impl.AuthorisationImpl
		 * @see mncModel.security.impl.SecurityPackageImpl#getAuthorisation()
		 * @generated
		 */
		EClass AUTHORISATION = eINSTANCE.getAuthorisation();

		/**
		 * The meta object literal for the '<em><b>Role</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AUTHORISATION__ROLE = eINSTANCE.getAuthorisation_Role();

		/**
		 * The meta object literal for the '<em><b>System Control</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AUTHORISATION__SYSTEM_CONTROL = eINSTANCE.getAuthorisation_SystemControl();

		/**
		 * The meta object literal for the '<em><b>User Operations</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AUTHORISATION__USER_OPERATIONS = eINSTANCE.getAuthorisation_UserOperations();

		/**
		 * The meta object literal for the '{@link mncModel.security.impl.SecuredLinkImpl <em>Secured Link</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mncModel.security.impl.SecuredLinkImpl
		 * @see mncModel.security.impl.SecurityPackageImpl#getSecuredLink()
		 * @generated
		 */
		EClass SECURED_LINK = eINSTANCE.getSecuredLink();

		/**
		 * The meta object literal for the '{@link mncModel.security.impl.ConfidentialityImpl <em>Confidentiality</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mncModel.security.impl.ConfidentialityImpl
		 * @see mncModel.security.impl.SecurityPackageImpl#getConfidentiality()
		 * @generated
		 */
		EClass CONFIDENTIALITY = eINSTANCE.getConfidentiality();

		/**
		 * The meta object literal for the '{@link mncModel.security.impl.SecurityImpl <em>Security</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mncModel.security.impl.SecurityImpl
		 * @see mncModel.security.impl.SecurityPackageImpl#getSecurity()
		 * @generated
		 */
		EClass SECURITY = eINSTANCE.getSecurity();

		/**
		 * The meta object literal for the '<em><b>Authorisations</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SECURITY__AUTHORISATIONS = eINSTANCE.getSecurity_Authorisations();

		/**
		 * The meta object literal for the '<em><b>Secured Links</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SECURITY__SECURED_LINKS = eINSTANCE.getSecurity_SecuredLinks();

	}

} //SecurityPackage
