/**
 */
package mncModel;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Response Translation Rule</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link mncModel.ResponseTranslationRule#getInputResponses <em>Input Responses</em>}</li>
 *   <li>{@link mncModel.ResponseTranslationRule#getTranslatedResponse <em>Translated Response</em>}</li>
 *   <li>{@link mncModel.ResponseTranslationRule#getParameterTranslations <em>Parameter Translations</em>}</li>
 *   <li>{@link mncModel.ResponseTranslationRule#getOperation <em>Operation</em>}</li>
 * </ul>
 *
 * @see mncModel.MncModelPackage#getResponseTranslationRule()
 * @model
 * @generated
 */
public interface ResponseTranslationRule extends EObject {
	/**
	 * Returns the value of the '<em><b>Input Responses</b></em>' reference list.
	 * The list contents are of type {@link mncModel.Response}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Input Responses</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Input Responses</em>' reference list.
	 * @see mncModel.MncModelPackage#getResponseTranslationRule_InputResponses()
	 * @model
	 * @generated
	 */
	EList<Response> getInputResponses();

	/**
	 * Returns the value of the '<em><b>Translated Response</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Translated Response</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Translated Response</em>' reference.
	 * @see #setTranslatedResponse(Response)
	 * @see mncModel.MncModelPackage#getResponseTranslationRule_TranslatedResponse()
	 * @model
	 * @generated
	 */
	Response getTranslatedResponse();

	/**
	 * Sets the value of the '{@link mncModel.ResponseTranslationRule#getTranslatedResponse <em>Translated Response</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Translated Response</em>' reference.
	 * @see #getTranslatedResponse()
	 * @generated
	 */
	void setTranslatedResponse(Response value);

	/**
	 * Returns the value of the '<em><b>Parameter Translations</b></em>' containment reference list.
	 * The list contents are of type {@link mncModel.ParameterTranslation}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parameter Translations</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parameter Translations</em>' containment reference list.
	 * @see mncModel.MncModelPackage#getResponseTranslationRule_ParameterTranslations()
	 * @model containment="true"
	 * @generated
	 */
	EList<ParameterTranslation> getParameterTranslations();

	/**
	 * Returns the value of the '<em><b>Operation</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operation</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operation</em>' containment reference.
	 * @see #setOperation(AbstractOperationableItems)
	 * @see mncModel.MncModelPackage#getResponseTranslationRule_Operation()
	 * @model containment="true"
	 * @generated
	 */
	AbstractOperationableItems getOperation();

	/**
	 * Sets the value of the '{@link mncModel.ResponseTranslationRule#getOperation <em>Operation</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Operation</em>' containment reference.
	 * @see #getOperation()
	 * @generated
	 */
	void setOperation(AbstractOperationableItems value);

} // ResponseTranslationRule
