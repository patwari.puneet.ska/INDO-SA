/**
 */
package mncModel.stakeholder;

import mncModel.ControlNode;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>System Control</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link mncModel.stakeholder.SystemControl#getControlNodes <em>Control Nodes</em>}</li>
 * </ul>
 *
 * @see mncModel.stakeholder.StakeholderPackage#getSystemControl()
 * @model
 * @generated
 */
public interface SystemControl extends UserOperation {
	/**
	 * Returns the value of the '<em><b>Control Nodes</b></em>' reference list.
	 * The list contents are of type {@link mncModel.ControlNode}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Control Nodes</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Control Nodes</em>' reference list.
	 * @see mncModel.stakeholder.StakeholderPackage#getSystemControl_ControlNodes()
	 * @model
	 * @generated
	 */
	EList<ControlNode> getControlNodes();

} // SystemControl
