/**
 */
package mncModel.stakeholder;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>User Operation</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see mncModel.stakeholder.StakeholderPackage#getUserOperation()
 * @model
 * @generated
 */
public interface UserOperation extends EObject {
} // UserOperation
