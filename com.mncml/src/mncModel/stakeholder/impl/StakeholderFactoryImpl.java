/**
 */
package mncModel.stakeholder.impl;

import mncModel.stakeholder.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class StakeholderFactoryImpl extends EFactoryImpl implements StakeholderFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static StakeholderFactory init() {
		try {
			StakeholderFactory theStakeholderFactory = (StakeholderFactory)EPackage.Registry.INSTANCE.getEFactory(StakeholderPackage.eNS_URI);
			if (theStakeholderFactory != null) {
				return theStakeholderFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new StakeholderFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StakeholderFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case StakeholderPackage.ROLE: return createRole();
			case StakeholderPackage.USER: return createUser();
			case StakeholderPackage.USER_OPERATION: return createUserOperation();
			case StakeholderPackage.ALARM_ACKNOWLEDGEMENT: return createAlarmAcknowledgement();
			case StakeholderPackage.SYSTEM_CONTROL: return createSystemControl();
			case StakeholderPackage.STAKEHOLDER: return createStakeholder();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Role createRole() {
		RoleImpl role = new RoleImpl();
		return role;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public User createUser() {
		UserImpl user = new UserImpl();
		return user;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UserOperation createUserOperation() {
		UserOperationImpl userOperation = new UserOperationImpl();
		return userOperation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AlarmAcknowledgement createAlarmAcknowledgement() {
		AlarmAcknowledgementImpl alarmAcknowledgement = new AlarmAcknowledgementImpl();
		return alarmAcknowledgement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SystemControl createSystemControl() {
		SystemControlImpl systemControl = new SystemControlImpl();
		return systemControl;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Stakeholder createStakeholder() {
		StakeholderImpl stakeholder = new StakeholderImpl();
		return stakeholder;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StakeholderPackage getStakeholderPackage() {
		return (StakeholderPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static StakeholderPackage getPackage() {
		return StakeholderPackage.eINSTANCE;
	}

} //StakeholderFactoryImpl
