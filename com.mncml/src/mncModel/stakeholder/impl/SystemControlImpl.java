/**
 */
package mncModel.stakeholder.impl;

import java.util.Collection;

import mncModel.ControlNode;

import mncModel.stakeholder.StakeholderPackage;
import mncModel.stakeholder.SystemControl;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>System Control</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link mncModel.stakeholder.impl.SystemControlImpl#getControlNodes <em>Control Nodes</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SystemControlImpl extends UserOperationImpl implements SystemControl {
	/**
	 * The cached value of the '{@link #getControlNodes() <em>Control Nodes</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getControlNodes()
	 * @generated
	 * @ordered
	 */
	protected EList<ControlNode> controlNodes;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SystemControlImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return StakeholderPackage.Literals.SYSTEM_CONTROL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ControlNode> getControlNodes() {
		if (controlNodes == null) {
			controlNodes = new EObjectResolvingEList<ControlNode>(ControlNode.class, this, StakeholderPackage.SYSTEM_CONTROL__CONTROL_NODES);
		}
		return controlNodes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case StakeholderPackage.SYSTEM_CONTROL__CONTROL_NODES:
				return getControlNodes();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case StakeholderPackage.SYSTEM_CONTROL__CONTROL_NODES:
				getControlNodes().clear();
				getControlNodes().addAll((Collection<? extends ControlNode>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case StakeholderPackage.SYSTEM_CONTROL__CONTROL_NODES:
				getControlNodes().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case StakeholderPackage.SYSTEM_CONTROL__CONTROL_NODES:
				return controlNodes != null && !controlNodes.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //SystemControlImpl
