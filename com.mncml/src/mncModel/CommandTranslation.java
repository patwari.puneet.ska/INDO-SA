/**
 */
package mncModel;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Command Translation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link mncModel.CommandTranslation#getInputCommand <em>Input Command</em>}</li>
 *   <li>{@link mncModel.CommandTranslation#getTranslatedCommands <em>Translated Commands</em>}</li>
 *   <li>{@link mncModel.CommandTranslation#getParameterTranslations <em>Parameter Translations</em>}</li>
 *   <li>{@link mncModel.CommandTranslation#getOperation <em>Operation</em>}</li>
 * </ul>
 *
 * @see mncModel.MncModelPackage#getCommandTranslation()
 * @model
 * @generated
 */
public interface CommandTranslation extends EObject {
	/**
	 * Returns the value of the '<em><b>Input Command</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Input Command</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Input Command</em>' reference.
	 * @see #setInputCommand(Command)
	 * @see mncModel.MncModelPackage#getCommandTranslation_InputCommand()
	 * @model
	 * @generated
	 */
	Command getInputCommand();

	/**
	 * Sets the value of the '{@link mncModel.CommandTranslation#getInputCommand <em>Input Command</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Input Command</em>' reference.
	 * @see #getInputCommand()
	 * @generated
	 */
	void setInputCommand(Command value);

	/**
	 * Returns the value of the '<em><b>Translated Commands</b></em>' reference list.
	 * The list contents are of type {@link mncModel.Command}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Translated Commands</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Translated Commands</em>' reference list.
	 * @see mncModel.MncModelPackage#getCommandTranslation_TranslatedCommands()
	 * @model
	 * @generated
	 */
	EList<Command> getTranslatedCommands();

	/**
	 * Returns the value of the '<em><b>Parameter Translations</b></em>' containment reference list.
	 * The list contents are of type {@link mncModel.ParameterTranslation}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parameter Translations</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parameter Translations</em>' containment reference list.
	 * @see mncModel.MncModelPackage#getCommandTranslation_ParameterTranslations()
	 * @model containment="true"
	 * @generated
	 */
	EList<ParameterTranslation> getParameterTranslations();

	/**
	 * Returns the value of the '<em><b>Operation</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operation</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operation</em>' reference.
	 * @see #setOperation(AbstractOperationableItems)
	 * @see mncModel.MncModelPackage#getCommandTranslation_Operation()
	 * @model
	 * @generated
	 */
	AbstractOperationableItems getOperation();

	/**
	 * Sets the value of the '{@link mncModel.CommandTranslation#getOperation <em>Operation</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Operation</em>' reference.
	 * @see #getOperation()
	 * @generated
	 */
	void setOperation(AbstractOperationableItems value);

} // CommandTranslation
