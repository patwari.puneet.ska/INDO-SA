/**
 */
package mncModel;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Float Value</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link mncModel.FloatValue#getFloatValue <em>Float Value</em>}</li>
 * </ul>
 *
 * @see mncModel.MncModelPackage#getFloatValue()
 * @model
 * @generated
 */
public interface FloatValue extends PrimitiveValue {
	/**
	 * Returns the value of the '<em><b>Float Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Float Value</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Float Value</em>' attribute.
	 * @see #setFloatValue(float)
	 * @see mncModel.MncModelPackage#getFloatValue_FloatValue()
	 * @model
	 * @generated
	 */
	float getFloatValue();

	/**
	 * Sets the value of the '{@link mncModel.FloatValue#getFloatValue <em>Float Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Float Value</em>' attribute.
	 * @see #getFloatValue()
	 * @generated
	 */
	void setFloatValue(float value);

} // FloatValue
