/**
 */
package mncModel;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Data Point Translation Rule</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link mncModel.DataPointTranslationRule#getDataPoints <em>Data Points</em>}</li>
 *   <li>{@link mncModel.DataPointTranslationRule#getOperation <em>Operation</em>}</li>
 * </ul>
 *
 * @see mncModel.MncModelPackage#getDataPointTranslationRule()
 * @model
 * @generated
 */
public interface DataPointTranslationRule extends EObject {
	/**
	 * Returns the value of the '<em><b>Data Points</b></em>' reference list.
	 * The list contents are of type {@link mncModel.DataPoint}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Data Points</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data Points</em>' reference list.
	 * @see mncModel.MncModelPackage#getDataPointTranslationRule_DataPoints()
	 * @model
	 * @generated
	 */
	EList<DataPoint> getDataPoints();

	/**
	 * Returns the value of the '<em><b>Operation</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operation</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operation</em>' containment reference.
	 * @see #setOperation(Operation)
	 * @see mncModel.MncModelPackage#getDataPointTranslationRule_Operation()
	 * @model containment="true"
	 * @generated
	 */
	Operation getOperation();

	/**
	 * Sets the value of the '{@link mncModel.DataPointTranslationRule#getOperation <em>Operation</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Operation</em>' containment reference.
	 * @see #getOperation()
	 * @generated
	 */
	void setOperation(Operation value);

} // DataPointTranslationRule
