/**
 */
package mncModel;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Event Translation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link mncModel.EventTranslation#getEventTranslationRules <em>Event Translation Rules</em>}</li>
 * </ul>
 *
 * @see mncModel.MncModelPackage#getEventTranslation()
 * @model
 * @generated
 */
public interface EventTranslation extends EObject {
	/**
	 * Returns the value of the '<em><b>Event Translation Rules</b></em>' containment reference list.
	 * The list contents are of type {@link mncModel.EventTranslationRule}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Event Translation Rules</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Event Translation Rules</em>' containment reference list.
	 * @see mncModel.MncModelPackage#getEventTranslation_EventTranslationRules()
	 * @model containment="true"
	 * @generated
	 */
	EList<EventTranslationRule> getEventTranslationRules();

} // EventTranslation
