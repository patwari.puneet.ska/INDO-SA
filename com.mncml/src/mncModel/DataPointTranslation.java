/**
 */
package mncModel;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Data Point Translation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link mncModel.DataPointTranslation#getDataPointTranslationRules <em>Data Point Translation Rules</em>}</li>
 * </ul>
 *
 * @see mncModel.MncModelPackage#getDataPointTranslation()
 * @model
 * @generated
 */
public interface DataPointTranslation extends EObject {
	/**
	 * Returns the value of the '<em><b>Data Point Translation Rules</b></em>' containment reference list.
	 * The list contents are of type {@link mncModel.DataPointTranslationRule}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Data Point Translation Rules</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data Point Translation Rules</em>' containment reference list.
	 * @see mncModel.MncModelPackage#getDataPointTranslation_DataPointTranslationRules()
	 * @model containment="true"
	 * @generated
	 */
	EList<DataPointTranslationRule> getDataPointTranslationRules();

} // DataPointTranslation
