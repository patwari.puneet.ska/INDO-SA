/**
 */
package mncModel;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Responsible Item List</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link mncModel.ResponsibleItemList#getResponsibleCommands <em>Responsible Commands</em>}</li>
 *   <li>{@link mncModel.ResponsibleItemList#getResponsibleEvents <em>Responsible Events</em>}</li>
 *   <li>{@link mncModel.ResponsibleItemList#getResponsibleDataPoints <em>Responsible Data Points</em>}</li>
 *   <li>{@link mncModel.ResponsibleItemList#getResponsibleAlarms <em>Responsible Alarms</em>}</li>
 * </ul>
 *
 * @see mncModel.MncModelPackage#getResponsibleItemList()
 * @model
 * @generated
 */
public interface ResponsibleItemList extends EObject {
	/**
	 * Returns the value of the '<em><b>Responsible Commands</b></em>' reference list.
	 * The list contents are of type {@link mncModel.Command}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Responsible Commands</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Responsible Commands</em>' reference list.
	 * @see mncModel.MncModelPackage#getResponsibleItemList_ResponsibleCommands()
	 * @model
	 * @generated
	 */
	EList<Command> getResponsibleCommands();

	/**
	 * Returns the value of the '<em><b>Responsible Events</b></em>' reference list.
	 * The list contents are of type {@link mncModel.Event}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Responsible Events</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Responsible Events</em>' reference list.
	 * @see mncModel.MncModelPackage#getResponsibleItemList_ResponsibleEvents()
	 * @model
	 * @generated
	 */
	EList<Event> getResponsibleEvents();

	/**
	 * Returns the value of the '<em><b>Responsible Data Points</b></em>' reference list.
	 * The list contents are of type {@link mncModel.DataPoint}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Responsible Data Points</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Responsible Data Points</em>' reference list.
	 * @see mncModel.MncModelPackage#getResponsibleItemList_ResponsibleDataPoints()
	 * @model
	 * @generated
	 */
	EList<DataPoint> getResponsibleDataPoints();

	/**
	 * Returns the value of the '<em><b>Responsible Alarms</b></em>' reference list.
	 * The list contents are of type {@link mncModel.Alarm}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Responsible Alarms</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Responsible Alarms</em>' reference list.
	 * @see mncModel.MncModelPackage#getResponsibleItemList_ResponsibleAlarms()
	 * @model
	 * @generated
	 */
	EList<Alarm> getResponsibleAlarms();

} // ResponsibleItemList
