/**
 */
package mncModel;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Primitive Value</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see mncModel.MncModelPackage#getPrimitiveValue()
 * @model
 * @generated
 */
public interface PrimitiveValue extends EObject {
} // PrimitiveValue
