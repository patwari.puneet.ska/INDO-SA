/**
 */
package mncModel;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Response Block</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link mncModel.ResponseBlock#getResponse <em>Response</em>}</li>
 *   <li>{@link mncModel.ResponseBlock#getResponseValidation <em>Response Validation</em>}</li>
 *   <li>{@link mncModel.ResponseBlock#getResponseTranslation <em>Response Translation</em>}</li>
 *   <li>{@link mncModel.ResponseBlock#getDestinationNodes <em>Destination Nodes</em>}</li>
 * </ul>
 *
 * @see mncModel.MncModelPackage#getResponseBlock()
 * @model
 * @generated
 */
public interface ResponseBlock extends EObject {
	/**
	 * Returns the value of the '<em><b>Response</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Response</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Response</em>' reference.
	 * @see #setResponse(Response)
	 * @see mncModel.MncModelPackage#getResponseBlock_Response()
	 * @model
	 * @generated
	 */
	Response getResponse();

	/**
	 * Sets the value of the '{@link mncModel.ResponseBlock#getResponse <em>Response</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Response</em>' reference.
	 * @see #getResponse()
	 * @generated
	 */
	void setResponse(Response value);

	/**
	 * Returns the value of the '<em><b>Response Validation</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Response Validation</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Response Validation</em>' containment reference.
	 * @see #setResponseValidation(ResponseValidation)
	 * @see mncModel.MncModelPackage#getResponseBlock_ResponseValidation()
	 * @model containment="true"
	 * @generated
	 */
	ResponseValidation getResponseValidation();

	/**
	 * Sets the value of the '{@link mncModel.ResponseBlock#getResponseValidation <em>Response Validation</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Response Validation</em>' containment reference.
	 * @see #getResponseValidation()
	 * @generated
	 */
	void setResponseValidation(ResponseValidation value);

	/**
	 * Returns the value of the '<em><b>Response Translation</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Response Translation</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Response Translation</em>' containment reference.
	 * @see #setResponseTranslation(ResponseTranslation)
	 * @see mncModel.MncModelPackage#getResponseBlock_ResponseTranslation()
	 * @model containment="true"
	 * @generated
	 */
	ResponseTranslation getResponseTranslation();

	/**
	 * Sets the value of the '{@link mncModel.ResponseBlock#getResponseTranslation <em>Response Translation</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Response Translation</em>' containment reference.
	 * @see #getResponseTranslation()
	 * @generated
	 */
	void setResponseTranslation(ResponseTranslation value);

	/**
	 * Returns the value of the '<em><b>Destination Nodes</b></em>' reference list.
	 * The list contents are of type {@link mncModel.ControlNode}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Destination Nodes</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Destination Nodes</em>' reference list.
	 * @see mncModel.MncModelPackage#getResponseBlock_DestinationNodes()
	 * @model
	 * @generated
	 */
	EList<ControlNode> getDestinationNodes();

} // ResponseBlock
