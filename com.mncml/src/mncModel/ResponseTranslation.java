/**
 */
package mncModel;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Response Translation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link mncModel.ResponseTranslation#getResponseTranslationRules <em>Response Translation Rules</em>}</li>
 * </ul>
 *
 * @see mncModel.MncModelPackage#getResponseTranslation()
 * @model
 * @generated
 */
public interface ResponseTranslation extends EObject {
	/**
	 * Returns the value of the '<em><b>Response Translation Rules</b></em>' containment reference list.
	 * The list contents are of type {@link mncModel.ResponseTranslationRule}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Response Translation Rules</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Response Translation Rules</em>' containment reference list.
	 * @see mncModel.MncModelPackage#getResponseTranslation_ResponseTranslationRules()
	 * @model containment="true"
	 * @generated
	 */
	EList<ResponseTranslationRule> getResponseTranslationRules();

} // ResponseTranslation
