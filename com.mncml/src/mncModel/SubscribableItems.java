/**
 */
package mncModel;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Subscribable Items</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see mncModel.MncModelPackage#getSubscribableItems()
 * @model
 * @generated
 */
public interface SubscribableItems extends EObject {
} // SubscribableItems
