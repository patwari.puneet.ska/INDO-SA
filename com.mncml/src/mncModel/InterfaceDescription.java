/**
 */
package mncModel;

import mncModel.utility.AlarmUtility;
import mncModel.utility.CommandUtility;
import mncModel.utility.DataPointUtility;
import mncModel.utility.EventUtility;
import mncModel.utility.ResponseUtility;
import mncModel.utility.StateUtility;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Interface Description</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link mncModel.InterfaceDescription#getName <em>Name</em>}</li>
 *   <li>{@link mncModel.InterfaceDescription#getIpaddress <em>Ipaddress</em>}</li>
 *   <li>{@link mncModel.InterfaceDescription#getPort <em>Port</em>}</li>
 *   <li>{@link mncModel.InterfaceDescription#getDataPoints <em>Data Points</em>}</li>
 *   <li>{@link mncModel.InterfaceDescription#getAlarms <em>Alarms</em>}</li>
 *   <li>{@link mncModel.InterfaceDescription#getCommands <em>Commands</em>}</li>
 *   <li>{@link mncModel.InterfaceDescription#getEvents <em>Events</em>}</li>
 *   <li>{@link mncModel.InterfaceDescription#getSubscribedEvents <em>Subscribed Events</em>}</li>
 *   <li>{@link mncModel.InterfaceDescription#getResponses <em>Responses</em>}</li>
 *   <li>{@link mncModel.InterfaceDescription#getOperatingStates <em>Operating States</em>}</li>
 *   <li>{@link mncModel.InterfaceDescription#getSubscribedItems <em>Subscribed Items</em>}</li>
 *   <li>{@link mncModel.InterfaceDescription#getUses <em>Uses</em>}</li>
 * </ul>
 *
 * @see mncModel.MncModelPackage#getInterfaceDescription()
 * @model
 * @generated
 */
public interface InterfaceDescription extends mncModel.System {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see mncModel.MncModelPackage#getInterfaceDescription_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link mncModel.InterfaceDescription#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Ipaddress</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ipaddress</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ipaddress</em>' containment reference.
	 * @see #setIpaddress(Address)
	 * @see mncModel.MncModelPackage#getInterfaceDescription_Ipaddress()
	 * @model containment="true"
	 * @generated
	 */
	Address getIpaddress();

	/**
	 * Sets the value of the '{@link mncModel.InterfaceDescription#getIpaddress <em>Ipaddress</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Ipaddress</em>' containment reference.
	 * @see #getIpaddress()
	 * @generated
	 */
	void setIpaddress(Address value);

	/**
	 * Returns the value of the '<em><b>Port</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Port</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Port</em>' containment reference.
	 * @see #setPort(Port)
	 * @see mncModel.MncModelPackage#getInterfaceDescription_Port()
	 * @model containment="true"
	 * @generated
	 */
	Port getPort();

	/**
	 * Sets the value of the '{@link mncModel.InterfaceDescription#getPort <em>Port</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Port</em>' containment reference.
	 * @see #getPort()
	 * @generated
	 */
	void setPort(Port value);

	/**
	 * Returns the value of the '<em><b>Data Points</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Data Points</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data Points</em>' containment reference.
	 * @see #setDataPoints(DataPointUtility)
	 * @see mncModel.MncModelPackage#getInterfaceDescription_DataPoints()
	 * @model containment="true"
	 * @generated
	 */
	DataPointUtility getDataPoints();

	/**
	 * Sets the value of the '{@link mncModel.InterfaceDescription#getDataPoints <em>Data Points</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Data Points</em>' containment reference.
	 * @see #getDataPoints()
	 * @generated
	 */
	void setDataPoints(DataPointUtility value);

	/**
	 * Returns the value of the '<em><b>Alarms</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Alarms</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Alarms</em>' containment reference.
	 * @see #setAlarms(AlarmUtility)
	 * @see mncModel.MncModelPackage#getInterfaceDescription_Alarms()
	 * @model containment="true"
	 * @generated
	 */
	AlarmUtility getAlarms();

	/**
	 * Sets the value of the '{@link mncModel.InterfaceDescription#getAlarms <em>Alarms</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Alarms</em>' containment reference.
	 * @see #getAlarms()
	 * @generated
	 */
	void setAlarms(AlarmUtility value);

	/**
	 * Returns the value of the '<em><b>Commands</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Commands</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Commands</em>' containment reference.
	 * @see #setCommands(CommandUtility)
	 * @see mncModel.MncModelPackage#getInterfaceDescription_Commands()
	 * @model containment="true"
	 * @generated
	 */
	CommandUtility getCommands();

	/**
	 * Sets the value of the '{@link mncModel.InterfaceDescription#getCommands <em>Commands</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Commands</em>' containment reference.
	 * @see #getCommands()
	 * @generated
	 */
	void setCommands(CommandUtility value);

	/**
	 * Returns the value of the '<em><b>Events</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Events</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Events</em>' containment reference.
	 * @see #setEvents(EventUtility)
	 * @see mncModel.MncModelPackage#getInterfaceDescription_Events()
	 * @model containment="true"
	 * @generated
	 */
	EventUtility getEvents();

	/**
	 * Sets the value of the '{@link mncModel.InterfaceDescription#getEvents <em>Events</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Events</em>' containment reference.
	 * @see #getEvents()
	 * @generated
	 */
	void setEvents(EventUtility value);

	/**
	 * Returns the value of the '<em><b>Subscribed Events</b></em>' reference list.
	 * The list contents are of type {@link mncModel.Event}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Subscribed Events</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Subscribed Events</em>' reference list.
	 * @see mncModel.MncModelPackage#getInterfaceDescription_SubscribedEvents()
	 * @model
	 * @generated
	 */
	EList<Event> getSubscribedEvents();

	/**
	 * Returns the value of the '<em><b>Responses</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Responses</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Responses</em>' containment reference.
	 * @see #setResponses(ResponseUtility)
	 * @see mncModel.MncModelPackage#getInterfaceDescription_Responses()
	 * @model containment="true"
	 * @generated
	 */
	ResponseUtility getResponses();

	/**
	 * Sets the value of the '{@link mncModel.InterfaceDescription#getResponses <em>Responses</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Responses</em>' containment reference.
	 * @see #getResponses()
	 * @generated
	 */
	void setResponses(ResponseUtility value);

	/**
	 * Returns the value of the '<em><b>Operating States</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operating States</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operating States</em>' containment reference.
	 * @see #setOperatingStates(StateUtility)
	 * @see mncModel.MncModelPackage#getInterfaceDescription_OperatingStates()
	 * @model containment="true"
	 * @generated
	 */
	StateUtility getOperatingStates();

	/**
	 * Sets the value of the '{@link mncModel.InterfaceDescription#getOperatingStates <em>Operating States</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Operating States</em>' containment reference.
	 * @see #getOperatingStates()
	 * @generated
	 */
	void setOperatingStates(StateUtility value);

	/**
	 * Returns the value of the '<em><b>Subscribed Items</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Subscribed Items</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Subscribed Items</em>' containment reference.
	 * @see #setSubscribedItems(SubscribableItemList)
	 * @see mncModel.MncModelPackage#getInterfaceDescription_SubscribedItems()
	 * @model containment="true"
	 * @generated
	 */
	SubscribableItemList getSubscribedItems();

	/**
	 * Sets the value of the '{@link mncModel.InterfaceDescription#getSubscribedItems <em>Subscribed Items</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Subscribed Items</em>' containment reference.
	 * @see #getSubscribedItems()
	 * @generated
	 */
	void setSubscribedItems(SubscribableItemList value);

	/**
	 * Returns the value of the '<em><b>Uses</b></em>' reference list.
	 * The list contents are of type {@link mncModel.InterfaceDescription}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Uses</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Uses</em>' reference list.
	 * @see mncModel.MncModelPackage#getInterfaceDescription_Uses()
	 * @model
	 * @generated
	 */
	EList<InterfaceDescription> getUses();

} // InterfaceDescription
