/**
 */
package mncModel;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Response Validation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link mncModel.ResponseValidation#getResponse <em>Response</em>}</li>
 *   <li>{@link mncModel.ResponseValidation#getValidationRules <em>Validation Rules</em>}</li>
 * </ul>
 *
 * @see mncModel.MncModelPackage#getResponseValidation()
 * @model
 * @generated
 */
public interface ResponseValidation extends EObject {
	/**
	 * Returns the value of the '<em><b>Response</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Response</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Response</em>' reference.
	 * @see #setResponse(Response)
	 * @see mncModel.MncModelPackage#getResponseValidation_Response()
	 * @model
	 * @generated
	 */
	Response getResponse();

	/**
	 * Sets the value of the '{@link mncModel.ResponseValidation#getResponse <em>Response</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Response</em>' reference.
	 * @see #getResponse()
	 * @generated
	 */
	void setResponse(Response value);

	/**
	 * Returns the value of the '<em><b>Validation Rules</b></em>' containment reference list.
	 * The list contents are of type {@link mncModel.CheckParameterCondition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Validation Rules</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Validation Rules</em>' containment reference list.
	 * @see mncModel.MncModelPackage#getResponseValidation_ValidationRules()
	 * @model containment="true"
	 * @generated
	 */
	EList<CheckParameterCondition> getValidationRules();

} // ResponseValidation
