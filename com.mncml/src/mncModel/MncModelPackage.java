/**
 */
package mncModel;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see mncModel.MncModelFactory
 * @model kind="package"
 * @generated
 */
public interface MncModelPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "mncModel";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://mncModel/3.0";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "mncModel";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	MncModelPackage eINSTANCE = mncModel.impl.MncModelPackageImpl.init();

	/**
	 * The meta object id for the '{@link mncModel.impl.ModelImpl <em>Model</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mncModel.impl.ModelImpl
	 * @see mncModel.impl.MncModelPackageImpl#getModel()
	 * @generated
	 */
	int MODEL = 0;

	/**
	 * The feature id for the '<em><b>Systems</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL__SYSTEMS = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL__NAME = 1;

	/**
	 * The feature id for the '<em><b>Import Section</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL__IMPORT_SECTION = 2;

	/**
	 * The feature id for the '<em><b>Security</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL__SECURITY = 3;

	/**
	 * The feature id for the '<em><b>Stakeholder</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL__STAKEHOLDER = 4;

	/**
	 * The number of structural features of the '<em>Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL_FEATURE_COUNT = 5;

	/**
	 * The meta object id for the '{@link mncModel.impl.ImportImpl <em>Import</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mncModel.impl.ImportImpl
	 * @see mncModel.impl.MncModelPackageImpl#getImport()
	 * @generated
	 */
	int IMPORT = 1;

	/**
	 * The feature id for the '<em><b>Imported Namespace</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMPORT__IMPORTED_NAMESPACE = 0;

	/**
	 * The number of structural features of the '<em>Import</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMPORT_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link mncModel.impl.SystemImpl <em>System</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mncModel.impl.SystemImpl
	 * @see mncModel.impl.MncModelPackageImpl#getSystem()
	 * @generated
	 */
	int SYSTEM = 2;

	/**
	 * The number of structural features of the '<em>System</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM_FEATURE_COUNT = 0;

	/**
	 * The meta object id for the '{@link mncModel.impl.ControlNodeImpl <em>Control Node</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mncModel.impl.ControlNodeImpl
	 * @see mncModel.impl.MncModelPackageImpl#getControlNode()
	 * @generated
	 */
	int CONTROL_NODE = 3;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_NODE__NAME = SYSTEM_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Interface Description</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_NODE__INTERFACE_DESCRIPTION = SYSTEM_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Child Nodes</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_NODE__CHILD_NODES = SYSTEM_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Parent Node</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_NODE__PARENT_NODE = SYSTEM_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Command Response Blocks</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_NODE__COMMAND_RESPONSE_BLOCKS = SYSTEM_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Event Blocks</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_NODE__EVENT_BLOCKS = SYSTEM_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Alarm Blocks</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_NODE__ALARM_BLOCKS = SYSTEM_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Data Point Blocks</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_NODE__DATA_POINT_BLOCKS = SYSTEM_FEATURE_COUNT + 7;

	/**
	 * The number of structural features of the '<em>Control Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_NODE_FEATURE_COUNT = SYSTEM_FEATURE_COUNT + 8;

	/**
	 * The meta object id for the '{@link mncModel.impl.InterfaceDescriptionImpl <em>Interface Description</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mncModel.impl.InterfaceDescriptionImpl
	 * @see mncModel.impl.MncModelPackageImpl#getInterfaceDescription()
	 * @generated
	 */
	int INTERFACE_DESCRIPTION = 4;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERFACE_DESCRIPTION__NAME = SYSTEM_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Ipaddress</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERFACE_DESCRIPTION__IPADDRESS = SYSTEM_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Port</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERFACE_DESCRIPTION__PORT = SYSTEM_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Data Points</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERFACE_DESCRIPTION__DATA_POINTS = SYSTEM_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Alarms</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERFACE_DESCRIPTION__ALARMS = SYSTEM_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Commands</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERFACE_DESCRIPTION__COMMANDS = SYSTEM_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Events</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERFACE_DESCRIPTION__EVENTS = SYSTEM_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Subscribed Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERFACE_DESCRIPTION__SUBSCRIBED_EVENTS = SYSTEM_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Responses</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERFACE_DESCRIPTION__RESPONSES = SYSTEM_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>Operating States</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERFACE_DESCRIPTION__OPERATING_STATES = SYSTEM_FEATURE_COUNT + 9;

	/**
	 * The feature id for the '<em><b>Subscribed Items</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERFACE_DESCRIPTION__SUBSCRIBED_ITEMS = SYSTEM_FEATURE_COUNT + 10;

	/**
	 * The feature id for the '<em><b>Uses</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERFACE_DESCRIPTION__USES = SYSTEM_FEATURE_COUNT + 11;

	/**
	 * The number of structural features of the '<em>Interface Description</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERFACE_DESCRIPTION_FEATURE_COUNT = SYSTEM_FEATURE_COUNT + 12;

	/**
	 * The meta object id for the '{@link mncModel.impl.AbstractInterfaceItemsImpl <em>Abstract Interface Items</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mncModel.impl.AbstractInterfaceItemsImpl
	 * @see mncModel.impl.MncModelPackageImpl#getAbstractInterfaceItems()
	 * @generated
	 */
	int ABSTRACT_INTERFACE_ITEMS = 5;

	/**
	 * The number of structural features of the '<em>Abstract Interface Items</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_INTERFACE_ITEMS_FEATURE_COUNT = 0;

	/**
	 * The meta object id for the '{@link mncModel.impl.AbstractOperationableItemsImpl <em>Abstract Operationable Items</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mncModel.impl.AbstractOperationableItemsImpl
	 * @see mncModel.impl.MncModelPackageImpl#getAbstractOperationableItems()
	 * @generated
	 */
	int ABSTRACT_OPERATIONABLE_ITEMS = 6;

	/**
	 * The number of structural features of the '<em>Abstract Operationable Items</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ABSTRACT_OPERATIONABLE_ITEMS_FEATURE_COUNT = 0;

	/**
	 * The meta object id for the '{@link mncModel.impl.OperatingStateImpl <em>Operating State</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mncModel.impl.OperatingStateImpl
	 * @see mncModel.impl.MncModelPackageImpl#getOperatingState()
	 * @generated
	 */
	int OPERATING_STATE = 7;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATING_STATE__NAME = 0;

	/**
	 * The feature id for the '<em><b>Parameters</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATING_STATE__PARAMETERS = 1;

	/**
	 * The number of structural features of the '<em>Operating State</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATING_STATE_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link mncModel.impl.CommandResponseBlockImpl <em>Command Response Block</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mncModel.impl.CommandResponseBlockImpl
	 * @see mncModel.impl.MncModelPackageImpl#getCommandResponseBlock()
	 * @generated
	 */
	int COMMAND_RESPONSE_BLOCK = 8;

	/**
	 * The feature id for the '<em><b>Command</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMAND_RESPONSE_BLOCK__COMMAND = 0;

	/**
	 * The feature id for the '<em><b>Command Validation</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMAND_RESPONSE_BLOCK__COMMAND_VALIDATION = 1;

	/**
	 * The feature id for the '<em><b>Transition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMAND_RESPONSE_BLOCK__TRANSITION = 2;

	/**
	 * The feature id for the '<em><b>Command Trigger Condition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMAND_RESPONSE_BLOCK__COMMAND_TRIGGER_CONDITION = 3;

	/**
	 * The feature id for the '<em><b>Command Translation</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMAND_RESPONSE_BLOCK__COMMAND_TRANSLATION = 4;

	/**
	 * The feature id for the '<em><b>Command Distributions</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMAND_RESPONSE_BLOCK__COMMAND_DISTRIBUTIONS = 5;

	/**
	 * The feature id for the '<em><b>Response Block</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMAND_RESPONSE_BLOCK__RESPONSE_BLOCK = 6;

	/**
	 * The number of structural features of the '<em>Command Response Block</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMAND_RESPONSE_BLOCK_FEATURE_COUNT = 7;

	/**
	 * The meta object id for the '{@link mncModel.impl.CommandImpl <em>Command</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mncModel.impl.CommandImpl
	 * @see mncModel.impl.MncModelPackageImpl#getCommand()
	 * @generated
	 */
	int COMMAND = 9;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMAND__NAME = ABSTRACT_INTERFACE_ITEMS_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Parameters</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMAND__PARAMETERS = ABSTRACT_INTERFACE_ITEMS_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Command</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMAND_FEATURE_COUNT = ABSTRACT_INTERFACE_ITEMS_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link mncModel.impl.CommandValidationImpl <em>Command Validation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mncModel.impl.CommandValidationImpl
	 * @see mncModel.impl.MncModelPackageImpl#getCommandValidation()
	 * @generated
	 */
	int COMMAND_VALIDATION = 10;

	/**
	 * The feature id for the '<em><b>Validation Rules</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMAND_VALIDATION__VALIDATION_RULES = 0;

	/**
	 * The feature id for the '<em><b>Operation</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMAND_VALIDATION__OPERATION = 1;

	/**
	 * The number of structural features of the '<em>Command Validation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMAND_VALIDATION_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link mncModel.impl.CommandDistributionImpl <em>Command Distribution</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mncModel.impl.CommandDistributionImpl
	 * @see mncModel.impl.MncModelPackageImpl#getCommandDistribution()
	 * @generated
	 */
	int COMMAND_DISTRIBUTION = 11;

	/**
	 * The feature id for the '<em><b>Command</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMAND_DISTRIBUTION__COMMAND = 0;

	/**
	 * The feature id for the '<em><b>Destination Nodes</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMAND_DISTRIBUTION__DESTINATION_NODES = 1;

	/**
	 * The number of structural features of the '<em>Command Distribution</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMAND_DISTRIBUTION_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link mncModel.impl.CommandTranslationImpl <em>Command Translation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mncModel.impl.CommandTranslationImpl
	 * @see mncModel.impl.MncModelPackageImpl#getCommandTranslation()
	 * @generated
	 */
	int COMMAND_TRANSLATION = 12;

	/**
	 * The feature id for the '<em><b>Input Command</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMAND_TRANSLATION__INPUT_COMMAND = 0;

	/**
	 * The feature id for the '<em><b>Translated Commands</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMAND_TRANSLATION__TRANSLATED_COMMANDS = 1;

	/**
	 * The feature id for the '<em><b>Parameter Translations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMAND_TRANSLATION__PARAMETER_TRANSLATIONS = 2;

	/**
	 * The feature id for the '<em><b>Operation</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMAND_TRANSLATION__OPERATION = 3;

	/**
	 * The number of structural features of the '<em>Command Translation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMAND_TRANSLATION_FEATURE_COUNT = 4;

	/**
	 * The meta object id for the '{@link mncModel.impl.CommandTriggerConditionImpl <em>Command Trigger Condition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mncModel.impl.CommandTriggerConditionImpl
	 * @see mncModel.impl.MncModelPackageImpl#getCommandTriggerCondition()
	 * @generated
	 */
	int COMMAND_TRIGGER_CONDITION = 13;

	/**
	 * The feature id for the '<em><b>Responsible Items</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMAND_TRIGGER_CONDITION__RESPONSIBLE_ITEMS = 0;

	/**
	 * The feature id for the '<em><b>Operation</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMAND_TRIGGER_CONDITION__OPERATION = 1;

	/**
	 * The number of structural features of the '<em>Command Trigger Condition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMAND_TRIGGER_CONDITION_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link mncModel.impl.ParameterTranslationImpl <em>Parameter Translation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mncModel.impl.ParameterTranslationImpl
	 * @see mncModel.impl.MncModelPackageImpl#getParameterTranslation()
	 * @generated
	 */
	int PARAMETER_TRANSLATION = 14;

	/**
	 * The feature id for the '<em><b>Input Parameters</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_TRANSLATION__INPUT_PARAMETERS = 0;

	/**
	 * The feature id for the '<em><b>Translated Parameters</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_TRANSLATION__TRANSLATED_PARAMETERS = 1;

	/**
	 * The number of structural features of the '<em>Parameter Translation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_TRANSLATION_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link mncModel.impl.TransitionImpl <em>Transition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mncModel.impl.TransitionImpl
	 * @see mncModel.impl.MncModelPackageImpl#getTransition()
	 * @generated
	 */
	int TRANSITION = 15;

	/**
	 * The feature id for the '<em><b>Current State</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__CURRENT_STATE = 0;

	/**
	 * The feature id for the '<em><b>Next State</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__NEXT_STATE = 1;

	/**
	 * The feature id for the '<em><b>Entry Action</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__ENTRY_ACTION = 2;

	/**
	 * The feature id for the '<em><b>Exit Action</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION__EXIT_ACTION = 3;

	/**
	 * The number of structural features of the '<em>Transition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSITION_FEATURE_COUNT = 4;

	/**
	 * The meta object id for the '{@link mncModel.impl.ResponseImpl <em>Response</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mncModel.impl.ResponseImpl
	 * @see mncModel.impl.MncModelPackageImpl#getResponse()
	 * @generated
	 */
	int RESPONSE = 16;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESPONSE__NAME = ABSTRACT_INTERFACE_ITEMS_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Parameters</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESPONSE__PARAMETERS = ABSTRACT_INTERFACE_ITEMS_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Publish</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESPONSE__PUBLISH = ABSTRACT_INTERFACE_ITEMS_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Response</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESPONSE_FEATURE_COUNT = ABSTRACT_INTERFACE_ITEMS_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link mncModel.impl.ResponseBlockImpl <em>Response Block</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mncModel.impl.ResponseBlockImpl
	 * @see mncModel.impl.MncModelPackageImpl#getResponseBlock()
	 * @generated
	 */
	int RESPONSE_BLOCK = 17;

	/**
	 * The feature id for the '<em><b>Response</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESPONSE_BLOCK__RESPONSE = 0;

	/**
	 * The feature id for the '<em><b>Response Validation</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESPONSE_BLOCK__RESPONSE_VALIDATION = 1;

	/**
	 * The feature id for the '<em><b>Response Translation</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESPONSE_BLOCK__RESPONSE_TRANSLATION = 2;

	/**
	 * The feature id for the '<em><b>Destination Nodes</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESPONSE_BLOCK__DESTINATION_NODES = 3;

	/**
	 * The number of structural features of the '<em>Response Block</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESPONSE_BLOCK_FEATURE_COUNT = 4;

	/**
	 * The meta object id for the '{@link mncModel.impl.ResponseValidationImpl <em>Response Validation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mncModel.impl.ResponseValidationImpl
	 * @see mncModel.impl.MncModelPackageImpl#getResponseValidation()
	 * @generated
	 */
	int RESPONSE_VALIDATION = 18;

	/**
	 * The feature id for the '<em><b>Response</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESPONSE_VALIDATION__RESPONSE = 0;

	/**
	 * The feature id for the '<em><b>Validation Rules</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESPONSE_VALIDATION__VALIDATION_RULES = 1;

	/**
	 * The number of structural features of the '<em>Response Validation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESPONSE_VALIDATION_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link mncModel.impl.ResponseDistributionImpl <em>Response Distribution</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mncModel.impl.ResponseDistributionImpl
	 * @see mncModel.impl.MncModelPackageImpl#getResponseDistribution()
	 * @generated
	 */
	int RESPONSE_DISTRIBUTION = 19;

	/**
	 * The feature id for the '<em><b>Response</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESPONSE_DISTRIBUTION__RESPONSE = 0;

	/**
	 * The feature id for the '<em><b>Destination Nodes</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESPONSE_DISTRIBUTION__DESTINATION_NODES = 1;

	/**
	 * The number of structural features of the '<em>Response Distribution</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESPONSE_DISTRIBUTION_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link mncModel.impl.ResponseTranslationImpl <em>Response Translation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mncModel.impl.ResponseTranslationImpl
	 * @see mncModel.impl.MncModelPackageImpl#getResponseTranslation()
	 * @generated
	 */
	int RESPONSE_TRANSLATION = 20;

	/**
	 * The feature id for the '<em><b>Response Translation Rules</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESPONSE_TRANSLATION__RESPONSE_TRANSLATION_RULES = 0;

	/**
	 * The number of structural features of the '<em>Response Translation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESPONSE_TRANSLATION_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link mncModel.impl.ResponseTranslationRuleImpl <em>Response Translation Rule</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mncModel.impl.ResponseTranslationRuleImpl
	 * @see mncModel.impl.MncModelPackageImpl#getResponseTranslationRule()
	 * @generated
	 */
	int RESPONSE_TRANSLATION_RULE = 21;

	/**
	 * The feature id for the '<em><b>Input Responses</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESPONSE_TRANSLATION_RULE__INPUT_RESPONSES = 0;

	/**
	 * The feature id for the '<em><b>Translated Response</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESPONSE_TRANSLATION_RULE__TRANSLATED_RESPONSE = 1;

	/**
	 * The feature id for the '<em><b>Parameter Translations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESPONSE_TRANSLATION_RULE__PARAMETER_TRANSLATIONS = 2;

	/**
	 * The feature id for the '<em><b>Operation</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESPONSE_TRANSLATION_RULE__OPERATION = 3;

	/**
	 * The number of structural features of the '<em>Response Translation Rule</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESPONSE_TRANSLATION_RULE_FEATURE_COUNT = 4;

	/**
	 * The meta object id for the '{@link mncModel.impl.PortImpl <em>Port</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mncModel.impl.PortImpl
	 * @see mncModel.impl.MncModelPackageImpl#getPort()
	 * @generated
	 */
	int PORT = 22;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT__NAME = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT__VALUE = 1;

	/**
	 * The number of structural features of the '<em>Port</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link mncModel.impl.EventBlockImpl <em>Event Block</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mncModel.impl.EventBlockImpl
	 * @see mncModel.impl.MncModelPackageImpl#getEventBlock()
	 * @generated
	 */
	int EVENT_BLOCK = 23;

	/**
	 * The feature id for the '<em><b>Event</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_BLOCK__EVENT = 0;

	/**
	 * The feature id for the '<em><b>Event Condition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_BLOCK__EVENT_CONDITION = 1;

	/**
	 * The feature id for the '<em><b>Event Handling</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_BLOCK__EVENT_HANDLING = 2;

	/**
	 * The number of structural features of the '<em>Event Block</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_BLOCK_FEATURE_COUNT = 3;

	/**
	 * The meta object id for the '{@link mncModel.impl.EventImpl <em>Event</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mncModel.impl.EventImpl
	 * @see mncModel.impl.MncModelPackageImpl#getEvent()
	 * @generated
	 */
	int EVENT = 24;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT__NAME = ABSTRACT_INTERFACE_ITEMS_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Publish</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT__PUBLISH = ABSTRACT_INTERFACE_ITEMS_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Parameters</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT__PARAMETERS = ABSTRACT_INTERFACE_ITEMS_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_FEATURE_COUNT = ABSTRACT_INTERFACE_ITEMS_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link mncModel.impl.EventTriggerConditionImpl <em>Event Trigger Condition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mncModel.impl.EventTriggerConditionImpl
	 * @see mncModel.impl.MncModelPackageImpl#getEventTriggerCondition()
	 * @generated
	 */
	int EVENT_TRIGGER_CONDITION = 25;

	/**
	 * The feature id for the '<em><b>Event Translation</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_TRIGGER_CONDITION__EVENT_TRANSLATION = 0;

	/**
	 * The feature id for the '<em><b>Operation</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_TRIGGER_CONDITION__OPERATION = 1;

	/**
	 * The feature id for the '<em><b>Responsible Items</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_TRIGGER_CONDITION__RESPONSIBLE_ITEMS = 2;

	/**
	 * The number of structural features of the '<em>Event Trigger Condition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_TRIGGER_CONDITION_FEATURE_COUNT = 3;

	/**
	 * The meta object id for the '{@link mncModel.impl.EventTranslationImpl <em>Event Translation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mncModel.impl.EventTranslationImpl
	 * @see mncModel.impl.MncModelPackageImpl#getEventTranslation()
	 * @generated
	 */
	int EVENT_TRANSLATION = 26;

	/**
	 * The feature id for the '<em><b>Event Translation Rules</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_TRANSLATION__EVENT_TRANSLATION_RULES = 0;

	/**
	 * The number of structural features of the '<em>Event Translation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_TRANSLATION_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link mncModel.impl.EventTranslationRuleImpl <em>Event Translation Rule</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mncModel.impl.EventTranslationRuleImpl
	 * @see mncModel.impl.MncModelPackageImpl#getEventTranslationRule()
	 * @generated
	 */
	int EVENT_TRANSLATION_RULE = 27;

	/**
	 * The feature id for the '<em><b>Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_TRANSLATION_RULE__EVENTS = 0;

	/**
	 * The feature id for the '<em><b>Translated Event</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_TRANSLATION_RULE__TRANSLATED_EVENT = 1;

	/**
	 * The feature id for the '<em><b>Operation</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_TRANSLATION_RULE__OPERATION = 2;

	/**
	 * The number of structural features of the '<em>Event Translation Rule</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_TRANSLATION_RULE_FEATURE_COUNT = 3;

	/**
	 * The meta object id for the '{@link mncModel.impl.EventHandlingImpl <em>Event Handling</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mncModel.impl.EventHandlingImpl
	 * @see mncModel.impl.MncModelPackageImpl#getEventHandling()
	 * @generated
	 */
	int EVENT_HANDLING = 28;

	/**
	 * The feature id for the '<em><b>Trigger Action</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_HANDLING__TRIGGER_ACTION = 0;

	/**
	 * The number of structural features of the '<em>Event Handling</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_HANDLING_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link mncModel.impl.AlarmBlockImpl <em>Alarm Block</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mncModel.impl.AlarmBlockImpl
	 * @see mncModel.impl.MncModelPackageImpl#getAlarmBlock()
	 * @generated
	 */
	int ALARM_BLOCK = 29;

	/**
	 * The feature id for the '<em><b>Alarm</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALARM_BLOCK__ALARM = 0;

	/**
	 * The feature id for the '<em><b>Alarm Condition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALARM_BLOCK__ALARM_CONDITION = 1;

	/**
	 * The feature id for the '<em><b>Alarm Handling</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALARM_BLOCK__ALARM_HANDLING = 2;

	/**
	 * The number of structural features of the '<em>Alarm Block</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALARM_BLOCK_FEATURE_COUNT = 3;

	/**
	 * The meta object id for the '{@link mncModel.impl.AlarmImpl <em>Alarm</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mncModel.impl.AlarmImpl
	 * @see mncModel.impl.MncModelPackageImpl#getAlarm()
	 * @generated
	 */
	int ALARM = 30;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALARM__NAME = ABSTRACT_INTERFACE_ITEMS_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALARM__TYPE = ABSTRACT_INTERFACE_ITEMS_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Level</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALARM__LEVEL = ABSTRACT_INTERFACE_ITEMS_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Publish</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALARM__PUBLISH = ABSTRACT_INTERFACE_ITEMS_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Parameters</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALARM__PARAMETERS = ABSTRACT_INTERFACE_ITEMS_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Alarm</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALARM_FEATURE_COUNT = ABSTRACT_INTERFACE_ITEMS_FEATURE_COUNT + 5;

	/**
	 * The meta object id for the '{@link mncModel.impl.AlarmTriggerConditionImpl <em>Alarm Trigger Condition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mncModel.impl.AlarmTriggerConditionImpl
	 * @see mncModel.impl.MncModelPackageImpl#getAlarmTriggerCondition()
	 * @generated
	 */
	int ALARM_TRIGGER_CONDITION = 31;

	/**
	 * The feature id for the '<em><b>Alarm Translation</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALARM_TRIGGER_CONDITION__ALARM_TRANSLATION = 0;

	/**
	 * The feature id for the '<em><b>Operation</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALARM_TRIGGER_CONDITION__OPERATION = 1;

	/**
	 * The feature id for the '<em><b>Responsible Items</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALARM_TRIGGER_CONDITION__RESPONSIBLE_ITEMS = 2;

	/**
	 * The number of structural features of the '<em>Alarm Trigger Condition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALARM_TRIGGER_CONDITION_FEATURE_COUNT = 3;

	/**
	 * The meta object id for the '{@link mncModel.impl.AlarmTranslationImpl <em>Alarm Translation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mncModel.impl.AlarmTranslationImpl
	 * @see mncModel.impl.MncModelPackageImpl#getAlarmTranslation()
	 * @generated
	 */
	int ALARM_TRANSLATION = 32;

	/**
	 * The feature id for the '<em><b>Alarm Translation Rules</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALARM_TRANSLATION__ALARM_TRANSLATION_RULES = 0;

	/**
	 * The number of structural features of the '<em>Alarm Translation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALARM_TRANSLATION_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link mncModel.impl.AlarmTranslationRuleImpl <em>Alarm Translation Rule</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mncModel.impl.AlarmTranslationRuleImpl
	 * @see mncModel.impl.MncModelPackageImpl#getAlarmTranslationRule()
	 * @generated
	 */
	int ALARM_TRANSLATION_RULE = 33;

	/**
	 * The feature id for the '<em><b>Operation</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALARM_TRANSLATION_RULE__OPERATION = 0;

	/**
	 * The number of structural features of the '<em>Alarm Translation Rule</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALARM_TRANSLATION_RULE_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link mncModel.impl.AlarmHandlingImpl <em>Alarm Handling</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mncModel.impl.AlarmHandlingImpl
	 * @see mncModel.impl.MncModelPackageImpl#getAlarmHandling()
	 * @generated
	 */
	int ALARM_HANDLING = 34;

	/**
	 * The feature id for the '<em><b>Trigger Action</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALARM_HANDLING__TRIGGER_ACTION = 0;

	/**
	 * The number of structural features of the '<em>Alarm Handling</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALARM_HANDLING_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link mncModel.impl.DataPointBlockImpl <em>Data Point Block</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mncModel.impl.DataPointBlockImpl
	 * @see mncModel.impl.MncModelPackageImpl#getDataPointBlock()
	 * @generated
	 */
	int DATA_POINT_BLOCK = 35;

	/**
	 * The feature id for the '<em><b>Data Point</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_POINT_BLOCK__DATA_POINT = 0;

	/**
	 * The feature id for the '<em><b>Data Point Condition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_POINT_BLOCK__DATA_POINT_CONDITION = 1;

	/**
	 * The feature id for the '<em><b>Data Point Handling</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_POINT_BLOCK__DATA_POINT_HANDLING = 2;

	/**
	 * The number of structural features of the '<em>Data Point Block</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_POINT_BLOCK_FEATURE_COUNT = 3;

	/**
	 * The meta object id for the '{@link mncModel.impl.DataPointImpl <em>Data Point</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mncModel.impl.DataPointImpl
	 * @see mncModel.impl.MncModelPackageImpl#getDataPoint()
	 * @generated
	 */
	int DATA_POINT = 36;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_POINT__NAME = ABSTRACT_INTERFACE_ITEMS_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_POINT__TYPE = ABSTRACT_INTERFACE_ITEMS_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_POINT__VALUE = ABSTRACT_INTERFACE_ITEMS_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Publish</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_POINT__PUBLISH = ABSTRACT_INTERFACE_ITEMS_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Parameters</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_POINT__PARAMETERS = ABSTRACT_INTERFACE_ITEMS_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Data Point</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_POINT_FEATURE_COUNT = ABSTRACT_INTERFACE_ITEMS_FEATURE_COUNT + 5;

	/**
	 * The meta object id for the '{@link mncModel.impl.DataPointTriggerConditionImpl <em>Data Point Trigger Condition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mncModel.impl.DataPointTriggerConditionImpl
	 * @see mncModel.impl.MncModelPackageImpl#getDataPointTriggerCondition()
	 * @generated
	 */
	int DATA_POINT_TRIGGER_CONDITION = 37;

	/**
	 * The feature id for the '<em><b>Data Point Translation</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_POINT_TRIGGER_CONDITION__DATA_POINT_TRANSLATION = 0;

	/**
	 * The feature id for the '<em><b>Responsible Items</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_POINT_TRIGGER_CONDITION__RESPONSIBLE_ITEMS = 1;

	/**
	 * The feature id for the '<em><b>Operation</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_POINT_TRIGGER_CONDITION__OPERATION = 2;

	/**
	 * The number of structural features of the '<em>Data Point Trigger Condition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_POINT_TRIGGER_CONDITION_FEATURE_COUNT = 3;

	/**
	 * The meta object id for the '{@link mncModel.impl.DataPointTranslationImpl <em>Data Point Translation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mncModel.impl.DataPointTranslationImpl
	 * @see mncModel.impl.MncModelPackageImpl#getDataPointTranslation()
	 * @generated
	 */
	int DATA_POINT_TRANSLATION = 38;

	/**
	 * The feature id for the '<em><b>Data Point Translation Rules</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_POINT_TRANSLATION__DATA_POINT_TRANSLATION_RULES = 0;

	/**
	 * The number of structural features of the '<em>Data Point Translation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_POINT_TRANSLATION_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link mncModel.impl.DataPointTranslationRuleImpl <em>Data Point Translation Rule</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mncModel.impl.DataPointTranslationRuleImpl
	 * @see mncModel.impl.MncModelPackageImpl#getDataPointTranslationRule()
	 * @generated
	 */
	int DATA_POINT_TRANSLATION_RULE = 39;

	/**
	 * The feature id for the '<em><b>Data Points</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_POINT_TRANSLATION_RULE__DATA_POINTS = 0;

	/**
	 * The feature id for the '<em><b>Operation</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_POINT_TRANSLATION_RULE__OPERATION = 1;

	/**
	 * The number of structural features of the '<em>Data Point Translation Rule</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_POINT_TRANSLATION_RULE_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link mncModel.impl.DataPointHandlingImpl <em>Data Point Handling</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mncModel.impl.DataPointHandlingImpl
	 * @see mncModel.impl.MncModelPackageImpl#getDataPointHandling()
	 * @generated
	 */
	int DATA_POINT_HANDLING = 40;

	/**
	 * The feature id for the '<em><b>Check Data Point</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_POINT_HANDLING__CHECK_DATA_POINT = 0;

	/**
	 * The feature id for the '<em><b>Action</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_POINT_HANDLING__ACTION = 1;

	/**
	 * The number of structural features of the '<em>Data Point Handling</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_POINT_HANDLING_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link mncModel.impl.DataPointValidConditionImpl <em>Data Point Valid Condition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mncModel.impl.DataPointValidConditionImpl
	 * @see mncModel.impl.MncModelPackageImpl#getDataPointValidCondition()
	 * @generated
	 */
	int DATA_POINT_VALID_CONDITION = 41;

	/**
	 * The feature id for the '<em><b>Data Point</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_POINT_VALID_CONDITION__DATA_POINT = 0;

	/**
	 * The feature id for the '<em><b>Check Values</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_POINT_VALID_CONDITION__CHECK_VALUES = 1;

	/**
	 * The feature id for the '<em><b>Check Max Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_POINT_VALID_CONDITION__CHECK_MAX_VALUE = 2;

	/**
	 * The feature id for the '<em><b>Check Min Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_POINT_VALID_CONDITION__CHECK_MIN_VALUE = 3;

	/**
	 * The number of structural features of the '<em>Data Point Valid Condition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_POINT_VALID_CONDITION_FEATURE_COUNT = 4;

	/**
	 * The meta object id for the '{@link mncModel.impl.ParameterImpl <em>Parameter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mncModel.impl.ParameterImpl
	 * @see mncModel.impl.MncModelPackageImpl#getParameter()
	 * @generated
	 */
	int PARAMETER = 42;

	/**
	 * The number of structural features of the '<em>Parameter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_FEATURE_COUNT = ABSTRACT_INTERFACE_ITEMS_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link mncModel.impl.CheckParameterConditionImpl <em>Check Parameter Condition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mncModel.impl.CheckParameterConditionImpl
	 * @see mncModel.impl.MncModelPackageImpl#getCheckParameterCondition()
	 * @generated
	 */
	int CHECK_PARAMETER_CONDITION = 43;

	/**
	 * The feature id for the '<em><b>Parameter</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHECK_PARAMETER_CONDITION__PARAMETER = 0;

	/**
	 * The feature id for the '<em><b>Check Values</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHECK_PARAMETER_CONDITION__CHECK_VALUES = 1;

	/**
	 * The feature id for the '<em><b>Check Max Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHECK_PARAMETER_CONDITION__CHECK_MAX_VALUE = 2;

	/**
	 * The feature id for the '<em><b>Check Min Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHECK_PARAMETER_CONDITION__CHECK_MIN_VALUE = 3;

	/**
	 * The number of structural features of the '<em>Check Parameter Condition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHECK_PARAMETER_CONDITION_FEATURE_COUNT = 4;

	/**
	 * The meta object id for the '{@link mncModel.impl.ArrayTypeImpl <em>Array Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mncModel.impl.ArrayTypeImpl
	 * @see mncModel.impl.MncModelPackageImpl#getArrayType()
	 * @generated
	 */
	int ARRAY_TYPE = 44;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARRAY_TYPE__TYPE = PARAMETER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARRAY_TYPE__NAME = PARAMETER_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Values</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARRAY_TYPE__VALUES = PARAMETER_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Array Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARRAY_TYPE_FEATURE_COUNT = PARAMETER_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link mncModel.impl.SimpleTypeImpl <em>Simple Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mncModel.impl.SimpleTypeImpl
	 * @see mncModel.impl.MncModelPackageImpl#getSimpleType()
	 * @generated
	 */
	int SIMPLE_TYPE = 45;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_TYPE__NAME = PARAMETER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_TYPE__TYPE = PARAMETER_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_TYPE__VALUE = PARAMETER_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Simple Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_TYPE_FEATURE_COUNT = PARAMETER_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link mncModel.impl.PrimitiveValueImpl <em>Primitive Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mncModel.impl.PrimitiveValueImpl
	 * @see mncModel.impl.MncModelPackageImpl#getPrimitiveValue()
	 * @generated
	 */
	int PRIMITIVE_VALUE = 46;

	/**
	 * The number of structural features of the '<em>Primitive Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRIMITIVE_VALUE_FEATURE_COUNT = 0;

	/**
	 * The meta object id for the '{@link mncModel.impl.IntValueImpl <em>Int Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mncModel.impl.IntValueImpl
	 * @see mncModel.impl.MncModelPackageImpl#getIntValue()
	 * @generated
	 */
	int INT_VALUE = 47;

	/**
	 * The feature id for the '<em><b>Int Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INT_VALUE__INT_VALUE = PRIMITIVE_VALUE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Int Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INT_VALUE_FEATURE_COUNT = PRIMITIVE_VALUE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link mncModel.impl.BoolValueImpl <em>Bool Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mncModel.impl.BoolValueImpl
	 * @see mncModel.impl.MncModelPackageImpl#getBoolValue()
	 * @generated
	 */
	int BOOL_VALUE = 48;

	/**
	 * The feature id for the '<em><b>Bool Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOL_VALUE__BOOL_VALUE = PRIMITIVE_VALUE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Bool Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOL_VALUE_FEATURE_COUNT = PRIMITIVE_VALUE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link mncModel.impl.FloatValueImpl <em>Float Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mncModel.impl.FloatValueImpl
	 * @see mncModel.impl.MncModelPackageImpl#getFloatValue()
	 * @generated
	 */
	int FLOAT_VALUE = 49;

	/**
	 * The feature id for the '<em><b>Float Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLOAT_VALUE__FLOAT_VALUE = PRIMITIVE_VALUE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Float Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FLOAT_VALUE_FEATURE_COUNT = PRIMITIVE_VALUE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link mncModel.impl.StringValueImpl <em>String Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mncModel.impl.StringValueImpl
	 * @see mncModel.impl.MncModelPackageImpl#getStringValue()
	 * @generated
	 */
	int STRING_VALUE = 50;

	/**
	 * The feature id for the '<em><b>String Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_VALUE__STRING_VALUE = PRIMITIVE_VALUE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>String Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_VALUE_FEATURE_COUNT = PRIMITIVE_VALUE_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link mncModel.impl.OperationImpl <em>Operation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mncModel.impl.OperationImpl
	 * @see mncModel.impl.MncModelPackageImpl#getOperation()
	 * @generated
	 */
	int OPERATION = 51;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION__NAME = ABSTRACT_OPERATIONABLE_ITEMS_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Input Parameters</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION__INPUT_PARAMETERS = ABSTRACT_OPERATIONABLE_ITEMS_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Output Parameters</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION__OUTPUT_PARAMETERS = ABSTRACT_OPERATIONABLE_ITEMS_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Script</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION__SCRIPT = ABSTRACT_OPERATIONABLE_ITEMS_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION_FEATURE_COUNT = ABSTRACT_OPERATIONABLE_ITEMS_FEATURE_COUNT + 4;

	/**
	 * The meta object id for the '{@link mncModel.impl.AddressImpl <em>Address</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mncModel.impl.AddressImpl
	 * @see mncModel.impl.MncModelPackageImpl#getAddress()
	 * @generated
	 */
	int ADDRESS = 52;

	/**
	 * The feature id for the '<em><b>Ipaddress</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADDRESS__IPADDRESS = 0;

	/**
	 * The number of structural features of the '<em>Address</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADDRESS_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link mncModel.impl.ActionImpl <em>Action</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mncModel.impl.ActionImpl
	 * @see mncModel.impl.MncModelPackageImpl#getAction()
	 * @generated
	 */
	int ACTION = 53;

	/**
	 * The feature id for the '<em><b>Command</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION__COMMAND = 0;

	/**
	 * The feature id for the '<em><b>Event</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION__EVENT = 1;

	/**
	 * The feature id for the '<em><b>Alarm</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION__ALARM = 2;

	/**
	 * The feature id for the '<em><b>Operation</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION__OPERATION = 3;

	/**
	 * The number of structural features of the '<em>Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION_FEATURE_COUNT = 4;

	/**
	 * The meta object id for the '{@link mncModel.impl.PublishImpl <em>Publish</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mncModel.impl.PublishImpl
	 * @see mncModel.impl.MncModelPackageImpl#getPublish()
	 * @generated
	 */
	int PUBLISH = 54;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PUBLISH__NAME = OPERATION__NAME;

	/**
	 * The feature id for the '<em><b>Input Parameters</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PUBLISH__INPUT_PARAMETERS = OPERATION__INPUT_PARAMETERS;

	/**
	 * The feature id for the '<em><b>Output Parameters</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PUBLISH__OUTPUT_PARAMETERS = OPERATION__OUTPUT_PARAMETERS;

	/**
	 * The feature id for the '<em><b>Script</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PUBLISH__SCRIPT = OPERATION__SCRIPT;

	/**
	 * The number of structural features of the '<em>Publish</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PUBLISH_FEATURE_COUNT = OPERATION_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link mncModel.impl.SubscribableItemsImpl <em>Subscribable Items</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mncModel.impl.SubscribableItemsImpl
	 * @see mncModel.impl.MncModelPackageImpl#getSubscribableItems()
	 * @generated
	 */
	int SUBSCRIBABLE_ITEMS = 55;

	/**
	 * The number of structural features of the '<em>Subscribable Items</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBSCRIBABLE_ITEMS_FEATURE_COUNT = 0;

	/**
	 * The meta object id for the '{@link mncModel.impl.SubscribableItemListImpl <em>Subscribable Item List</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mncModel.impl.SubscribableItemListImpl
	 * @see mncModel.impl.MncModelPackageImpl#getSubscribableItemList()
	 * @generated
	 */
	int SUBSCRIBABLE_ITEM_LIST = 56;

	/**
	 * The feature id for the '<em><b>Subscribed Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBSCRIBABLE_ITEM_LIST__SUBSCRIBED_EVENTS = 0;

	/**
	 * The feature id for the '<em><b>Subscribed Alarms</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBSCRIBABLE_ITEM_LIST__SUBSCRIBED_ALARMS = 1;

	/**
	 * The feature id for the '<em><b>Subscribed Data Points</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBSCRIBABLE_ITEM_LIST__SUBSCRIBED_DATA_POINTS = 2;

	/**
	 * The number of structural features of the '<em>Subscribable Item List</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUBSCRIBABLE_ITEM_LIST_FEATURE_COUNT = 3;

	/**
	 * The meta object id for the '{@link mncModel.impl.ResponsibleItemListImpl <em>Responsible Item List</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mncModel.impl.ResponsibleItemListImpl
	 * @see mncModel.impl.MncModelPackageImpl#getResponsibleItemList()
	 * @generated
	 */
	int RESPONSIBLE_ITEM_LIST = 57;

	/**
	 * The feature id for the '<em><b>Responsible Commands</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESPONSIBLE_ITEM_LIST__RESPONSIBLE_COMMANDS = 0;

	/**
	 * The feature id for the '<em><b>Responsible Events</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESPONSIBLE_ITEM_LIST__RESPONSIBLE_EVENTS = 1;

	/**
	 * The feature id for the '<em><b>Responsible Data Points</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESPONSIBLE_ITEM_LIST__RESPONSIBLE_DATA_POINTS = 2;

	/**
	 * The feature id for the '<em><b>Responsible Alarms</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESPONSIBLE_ITEM_LIST__RESPONSIBLE_ALARMS = 3;

	/**
	 * The number of structural features of the '<em>Responsible Item List</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESPONSIBLE_ITEM_LIST_FEATURE_COUNT = 4;

	/**
	 * The meta object id for the '{@link mncModel.PrimitiveValueType <em>Primitive Value Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see mncModel.PrimitiveValueType
	 * @see mncModel.impl.MncModelPackageImpl#getPrimitiveValueType()
	 * @generated
	 */
	int PRIMITIVE_VALUE_TYPE = 58;


	/**
	 * Returns the meta object for class '{@link mncModel.Model <em>Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Model</em>'.
	 * @see mncModel.Model
	 * @generated
	 */
	EClass getModel();

	/**
	 * Returns the meta object for the containment reference list '{@link mncModel.Model#getSystems <em>Systems</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Systems</em>'.
	 * @see mncModel.Model#getSystems()
	 * @see #getModel()
	 * @generated
	 */
	EReference getModel_Systems();

	/**
	 * Returns the meta object for the attribute '{@link mncModel.Model#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see mncModel.Model#getName()
	 * @see #getModel()
	 * @generated
	 */
	EAttribute getModel_Name();

	/**
	 * Returns the meta object for the containment reference list '{@link mncModel.Model#getImportSection <em>Import Section</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Import Section</em>'.
	 * @see mncModel.Model#getImportSection()
	 * @see #getModel()
	 * @generated
	 */
	EReference getModel_ImportSection();

	/**
	 * Returns the meta object for the containment reference '{@link mncModel.Model#getSecurity <em>Security</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Security</em>'.
	 * @see mncModel.Model#getSecurity()
	 * @see #getModel()
	 * @generated
	 */
	EReference getModel_Security();

	/**
	 * Returns the meta object for the containment reference '{@link mncModel.Model#getStakeholder <em>Stakeholder</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Stakeholder</em>'.
	 * @see mncModel.Model#getStakeholder()
	 * @see #getModel()
	 * @generated
	 */
	EReference getModel_Stakeholder();

	/**
	 * Returns the meta object for class '{@link mncModel.Import <em>Import</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Import</em>'.
	 * @see mncModel.Import
	 * @generated
	 */
	EClass getImport();

	/**
	 * Returns the meta object for the attribute '{@link mncModel.Import#getImportedNamespace <em>Imported Namespace</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Imported Namespace</em>'.
	 * @see mncModel.Import#getImportedNamespace()
	 * @see #getImport()
	 * @generated
	 */
	EAttribute getImport_ImportedNamespace();

	/**
	 * Returns the meta object for class '{@link mncModel.System <em>System</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>System</em>'.
	 * @see mncModel.System
	 * @generated
	 */
	EClass getSystem();

	/**
	 * Returns the meta object for class '{@link mncModel.ControlNode <em>Control Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Control Node</em>'.
	 * @see mncModel.ControlNode
	 * @generated
	 */
	EClass getControlNode();

	/**
	 * Returns the meta object for the attribute '{@link mncModel.ControlNode#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see mncModel.ControlNode#getName()
	 * @see #getControlNode()
	 * @generated
	 */
	EAttribute getControlNode_Name();

	/**
	 * Returns the meta object for the reference '{@link mncModel.ControlNode#getInterfaceDescription <em>Interface Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Interface Description</em>'.
	 * @see mncModel.ControlNode#getInterfaceDescription()
	 * @see #getControlNode()
	 * @generated
	 */
	EReference getControlNode_InterfaceDescription();

	/**
	 * Returns the meta object for the reference list '{@link mncModel.ControlNode#getChildNodes <em>Child Nodes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Child Nodes</em>'.
	 * @see mncModel.ControlNode#getChildNodes()
	 * @see #getControlNode()
	 * @generated
	 */
	EReference getControlNode_ChildNodes();

	/**
	 * Returns the meta object for the reference '{@link mncModel.ControlNode#getParentNode <em>Parent Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Parent Node</em>'.
	 * @see mncModel.ControlNode#getParentNode()
	 * @see #getControlNode()
	 * @generated
	 */
	EReference getControlNode_ParentNode();

	/**
	 * Returns the meta object for the containment reference '{@link mncModel.ControlNode#getCommandResponseBlocks <em>Command Response Blocks</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Command Response Blocks</em>'.
	 * @see mncModel.ControlNode#getCommandResponseBlocks()
	 * @see #getControlNode()
	 * @generated
	 */
	EReference getControlNode_CommandResponseBlocks();

	/**
	 * Returns the meta object for the containment reference '{@link mncModel.ControlNode#getEventBlocks <em>Event Blocks</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Event Blocks</em>'.
	 * @see mncModel.ControlNode#getEventBlocks()
	 * @see #getControlNode()
	 * @generated
	 */
	EReference getControlNode_EventBlocks();

	/**
	 * Returns the meta object for the containment reference '{@link mncModel.ControlNode#getAlarmBlocks <em>Alarm Blocks</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Alarm Blocks</em>'.
	 * @see mncModel.ControlNode#getAlarmBlocks()
	 * @see #getControlNode()
	 * @generated
	 */
	EReference getControlNode_AlarmBlocks();

	/**
	 * Returns the meta object for the containment reference '{@link mncModel.ControlNode#getDataPointBlocks <em>Data Point Blocks</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Point Blocks</em>'.
	 * @see mncModel.ControlNode#getDataPointBlocks()
	 * @see #getControlNode()
	 * @generated
	 */
	EReference getControlNode_DataPointBlocks();

	/**
	 * Returns the meta object for class '{@link mncModel.InterfaceDescription <em>Interface Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Interface Description</em>'.
	 * @see mncModel.InterfaceDescription
	 * @generated
	 */
	EClass getInterfaceDescription();

	/**
	 * Returns the meta object for the attribute '{@link mncModel.InterfaceDescription#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see mncModel.InterfaceDescription#getName()
	 * @see #getInterfaceDescription()
	 * @generated
	 */
	EAttribute getInterfaceDescription_Name();

	/**
	 * Returns the meta object for the containment reference '{@link mncModel.InterfaceDescription#getIpaddress <em>Ipaddress</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Ipaddress</em>'.
	 * @see mncModel.InterfaceDescription#getIpaddress()
	 * @see #getInterfaceDescription()
	 * @generated
	 */
	EReference getInterfaceDescription_Ipaddress();

	/**
	 * Returns the meta object for the containment reference '{@link mncModel.InterfaceDescription#getPort <em>Port</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Port</em>'.
	 * @see mncModel.InterfaceDescription#getPort()
	 * @see #getInterfaceDescription()
	 * @generated
	 */
	EReference getInterfaceDescription_Port();

	/**
	 * Returns the meta object for the containment reference '{@link mncModel.InterfaceDescription#getDataPoints <em>Data Points</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Points</em>'.
	 * @see mncModel.InterfaceDescription#getDataPoints()
	 * @see #getInterfaceDescription()
	 * @generated
	 */
	EReference getInterfaceDescription_DataPoints();

	/**
	 * Returns the meta object for the containment reference '{@link mncModel.InterfaceDescription#getAlarms <em>Alarms</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Alarms</em>'.
	 * @see mncModel.InterfaceDescription#getAlarms()
	 * @see #getInterfaceDescription()
	 * @generated
	 */
	EReference getInterfaceDescription_Alarms();

	/**
	 * Returns the meta object for the containment reference '{@link mncModel.InterfaceDescription#getCommands <em>Commands</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Commands</em>'.
	 * @see mncModel.InterfaceDescription#getCommands()
	 * @see #getInterfaceDescription()
	 * @generated
	 */
	EReference getInterfaceDescription_Commands();

	/**
	 * Returns the meta object for the containment reference '{@link mncModel.InterfaceDescription#getEvents <em>Events</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Events</em>'.
	 * @see mncModel.InterfaceDescription#getEvents()
	 * @see #getInterfaceDescription()
	 * @generated
	 */
	EReference getInterfaceDescription_Events();

	/**
	 * Returns the meta object for the reference list '{@link mncModel.InterfaceDescription#getSubscribedEvents <em>Subscribed Events</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Subscribed Events</em>'.
	 * @see mncModel.InterfaceDescription#getSubscribedEvents()
	 * @see #getInterfaceDescription()
	 * @generated
	 */
	EReference getInterfaceDescription_SubscribedEvents();

	/**
	 * Returns the meta object for the containment reference '{@link mncModel.InterfaceDescription#getResponses <em>Responses</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Responses</em>'.
	 * @see mncModel.InterfaceDescription#getResponses()
	 * @see #getInterfaceDescription()
	 * @generated
	 */
	EReference getInterfaceDescription_Responses();

	/**
	 * Returns the meta object for the containment reference '{@link mncModel.InterfaceDescription#getOperatingStates <em>Operating States</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Operating States</em>'.
	 * @see mncModel.InterfaceDescription#getOperatingStates()
	 * @see #getInterfaceDescription()
	 * @generated
	 */
	EReference getInterfaceDescription_OperatingStates();

	/**
	 * Returns the meta object for the containment reference '{@link mncModel.InterfaceDescription#getSubscribedItems <em>Subscribed Items</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Subscribed Items</em>'.
	 * @see mncModel.InterfaceDescription#getSubscribedItems()
	 * @see #getInterfaceDescription()
	 * @generated
	 */
	EReference getInterfaceDescription_SubscribedItems();

	/**
	 * Returns the meta object for the reference list '{@link mncModel.InterfaceDescription#getUses <em>Uses</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Uses</em>'.
	 * @see mncModel.InterfaceDescription#getUses()
	 * @see #getInterfaceDescription()
	 * @generated
	 */
	EReference getInterfaceDescription_Uses();

	/**
	 * Returns the meta object for class '{@link mncModel.AbstractInterfaceItems <em>Abstract Interface Items</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Abstract Interface Items</em>'.
	 * @see mncModel.AbstractInterfaceItems
	 * @generated
	 */
	EClass getAbstractInterfaceItems();

	/**
	 * Returns the meta object for class '{@link mncModel.AbstractOperationableItems <em>Abstract Operationable Items</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Abstract Operationable Items</em>'.
	 * @see mncModel.AbstractOperationableItems
	 * @generated
	 */
	EClass getAbstractOperationableItems();

	/**
	 * Returns the meta object for class '{@link mncModel.OperatingState <em>Operating State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Operating State</em>'.
	 * @see mncModel.OperatingState
	 * @generated
	 */
	EClass getOperatingState();

	/**
	 * Returns the meta object for the attribute '{@link mncModel.OperatingState#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see mncModel.OperatingState#getName()
	 * @see #getOperatingState()
	 * @generated
	 */
	EAttribute getOperatingState_Name();

	/**
	 * Returns the meta object for the containment reference list '{@link mncModel.OperatingState#getParameters <em>Parameters</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Parameters</em>'.
	 * @see mncModel.OperatingState#getParameters()
	 * @see #getOperatingState()
	 * @generated
	 */
	EReference getOperatingState_Parameters();

	/**
	 * Returns the meta object for class '{@link mncModel.CommandResponseBlock <em>Command Response Block</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Command Response Block</em>'.
	 * @see mncModel.CommandResponseBlock
	 * @generated
	 */
	EClass getCommandResponseBlock();

	/**
	 * Returns the meta object for the reference '{@link mncModel.CommandResponseBlock#getCommand <em>Command</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Command</em>'.
	 * @see mncModel.CommandResponseBlock#getCommand()
	 * @see #getCommandResponseBlock()
	 * @generated
	 */
	EReference getCommandResponseBlock_Command();

	/**
	 * Returns the meta object for the containment reference '{@link mncModel.CommandResponseBlock#getCommandValidation <em>Command Validation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Command Validation</em>'.
	 * @see mncModel.CommandResponseBlock#getCommandValidation()
	 * @see #getCommandResponseBlock()
	 * @generated
	 */
	EReference getCommandResponseBlock_CommandValidation();

	/**
	 * Returns the meta object for the containment reference '{@link mncModel.CommandResponseBlock#getTransition <em>Transition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Transition</em>'.
	 * @see mncModel.CommandResponseBlock#getTransition()
	 * @see #getCommandResponseBlock()
	 * @generated
	 */
	EReference getCommandResponseBlock_Transition();

	/**
	 * Returns the meta object for the containment reference '{@link mncModel.CommandResponseBlock#getCommandTriggerCondition <em>Command Trigger Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Command Trigger Condition</em>'.
	 * @see mncModel.CommandResponseBlock#getCommandTriggerCondition()
	 * @see #getCommandResponseBlock()
	 * @generated
	 */
	EReference getCommandResponseBlock_CommandTriggerCondition();

	/**
	 * Returns the meta object for the containment reference '{@link mncModel.CommandResponseBlock#getCommandTranslation <em>Command Translation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Command Translation</em>'.
	 * @see mncModel.CommandResponseBlock#getCommandTranslation()
	 * @see #getCommandResponseBlock()
	 * @generated
	 */
	EReference getCommandResponseBlock_CommandTranslation();

	/**
	 * Returns the meta object for the containment reference '{@link mncModel.CommandResponseBlock#getCommandDistributions <em>Command Distributions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Command Distributions</em>'.
	 * @see mncModel.CommandResponseBlock#getCommandDistributions()
	 * @see #getCommandResponseBlock()
	 * @generated
	 */
	EReference getCommandResponseBlock_CommandDistributions();

	/**
	 * Returns the meta object for the containment reference '{@link mncModel.CommandResponseBlock#getResponseBlock <em>Response Block</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Response Block</em>'.
	 * @see mncModel.CommandResponseBlock#getResponseBlock()
	 * @see #getCommandResponseBlock()
	 * @generated
	 */
	EReference getCommandResponseBlock_ResponseBlock();

	/**
	 * Returns the meta object for class '{@link mncModel.Command <em>Command</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Command</em>'.
	 * @see mncModel.Command
	 * @generated
	 */
	EClass getCommand();

	/**
	 * Returns the meta object for the attribute '{@link mncModel.Command#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see mncModel.Command#getName()
	 * @see #getCommand()
	 * @generated
	 */
	EAttribute getCommand_Name();

	/**
	 * Returns the meta object for the containment reference list '{@link mncModel.Command#getParameters <em>Parameters</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Parameters</em>'.
	 * @see mncModel.Command#getParameters()
	 * @see #getCommand()
	 * @generated
	 */
	EReference getCommand_Parameters();

	/**
	 * Returns the meta object for class '{@link mncModel.CommandValidation <em>Command Validation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Command Validation</em>'.
	 * @see mncModel.CommandValidation
	 * @generated
	 */
	EClass getCommandValidation();

	/**
	 * Returns the meta object for the containment reference list '{@link mncModel.CommandValidation#getValidationRules <em>Validation Rules</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Validation Rules</em>'.
	 * @see mncModel.CommandValidation#getValidationRules()
	 * @see #getCommandValidation()
	 * @generated
	 */
	EReference getCommandValidation_ValidationRules();

	/**
	 * Returns the meta object for the containment reference '{@link mncModel.CommandValidation#getOperation <em>Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Operation</em>'.
	 * @see mncModel.CommandValidation#getOperation()
	 * @see #getCommandValidation()
	 * @generated
	 */
	EReference getCommandValidation_Operation();

	/**
	 * Returns the meta object for class '{@link mncModel.CommandDistribution <em>Command Distribution</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Command Distribution</em>'.
	 * @see mncModel.CommandDistribution
	 * @generated
	 */
	EClass getCommandDistribution();

	/**
	 * Returns the meta object for the reference '{@link mncModel.CommandDistribution#getCommand <em>Command</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Command</em>'.
	 * @see mncModel.CommandDistribution#getCommand()
	 * @see #getCommandDistribution()
	 * @generated
	 */
	EReference getCommandDistribution_Command();

	/**
	 * Returns the meta object for the reference list '{@link mncModel.CommandDistribution#getDestinationNodes <em>Destination Nodes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Destination Nodes</em>'.
	 * @see mncModel.CommandDistribution#getDestinationNodes()
	 * @see #getCommandDistribution()
	 * @generated
	 */
	EReference getCommandDistribution_DestinationNodes();

	/**
	 * Returns the meta object for class '{@link mncModel.CommandTranslation <em>Command Translation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Command Translation</em>'.
	 * @see mncModel.CommandTranslation
	 * @generated
	 */
	EClass getCommandTranslation();

	/**
	 * Returns the meta object for the reference '{@link mncModel.CommandTranslation#getInputCommand <em>Input Command</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Input Command</em>'.
	 * @see mncModel.CommandTranslation#getInputCommand()
	 * @see #getCommandTranslation()
	 * @generated
	 */
	EReference getCommandTranslation_InputCommand();

	/**
	 * Returns the meta object for the reference list '{@link mncModel.CommandTranslation#getTranslatedCommands <em>Translated Commands</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Translated Commands</em>'.
	 * @see mncModel.CommandTranslation#getTranslatedCommands()
	 * @see #getCommandTranslation()
	 * @generated
	 */
	EReference getCommandTranslation_TranslatedCommands();

	/**
	 * Returns the meta object for the containment reference list '{@link mncModel.CommandTranslation#getParameterTranslations <em>Parameter Translations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Parameter Translations</em>'.
	 * @see mncModel.CommandTranslation#getParameterTranslations()
	 * @see #getCommandTranslation()
	 * @generated
	 */
	EReference getCommandTranslation_ParameterTranslations();

	/**
	 * Returns the meta object for the reference '{@link mncModel.CommandTranslation#getOperation <em>Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Operation</em>'.
	 * @see mncModel.CommandTranslation#getOperation()
	 * @see #getCommandTranslation()
	 * @generated
	 */
	EReference getCommandTranslation_Operation();

	/**
	 * Returns the meta object for class '{@link mncModel.CommandTriggerCondition <em>Command Trigger Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Command Trigger Condition</em>'.
	 * @see mncModel.CommandTriggerCondition
	 * @generated
	 */
	EClass getCommandTriggerCondition();

	/**
	 * Returns the meta object for the containment reference '{@link mncModel.CommandTriggerCondition#getResponsibleItems <em>Responsible Items</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Responsible Items</em>'.
	 * @see mncModel.CommandTriggerCondition#getResponsibleItems()
	 * @see #getCommandTriggerCondition()
	 * @generated
	 */
	EReference getCommandTriggerCondition_ResponsibleItems();

	/**
	 * Returns the meta object for the containment reference '{@link mncModel.CommandTriggerCondition#getOperation <em>Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Operation</em>'.
	 * @see mncModel.CommandTriggerCondition#getOperation()
	 * @see #getCommandTriggerCondition()
	 * @generated
	 */
	EReference getCommandTriggerCondition_Operation();

	/**
	 * Returns the meta object for class '{@link mncModel.ParameterTranslation <em>Parameter Translation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Parameter Translation</em>'.
	 * @see mncModel.ParameterTranslation
	 * @generated
	 */
	EClass getParameterTranslation();

	/**
	 * Returns the meta object for the reference list '{@link mncModel.ParameterTranslation#getInputParameters <em>Input Parameters</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Input Parameters</em>'.
	 * @see mncModel.ParameterTranslation#getInputParameters()
	 * @see #getParameterTranslation()
	 * @generated
	 */
	EReference getParameterTranslation_InputParameters();

	/**
	 * Returns the meta object for the reference list '{@link mncModel.ParameterTranslation#getTranslatedParameters <em>Translated Parameters</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Translated Parameters</em>'.
	 * @see mncModel.ParameterTranslation#getTranslatedParameters()
	 * @see #getParameterTranslation()
	 * @generated
	 */
	EReference getParameterTranslation_TranslatedParameters();

	/**
	 * Returns the meta object for class '{@link mncModel.Transition <em>Transition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Transition</em>'.
	 * @see mncModel.Transition
	 * @generated
	 */
	EClass getTransition();

	/**
	 * Returns the meta object for the reference '{@link mncModel.Transition#getCurrentState <em>Current State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Current State</em>'.
	 * @see mncModel.Transition#getCurrentState()
	 * @see #getTransition()
	 * @generated
	 */
	EReference getTransition_CurrentState();

	/**
	 * Returns the meta object for the reference '{@link mncModel.Transition#getNextState <em>Next State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Next State</em>'.
	 * @see mncModel.Transition#getNextState()
	 * @see #getTransition()
	 * @generated
	 */
	EReference getTransition_NextState();

	/**
	 * Returns the meta object for the containment reference '{@link mncModel.Transition#getEntryAction <em>Entry Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Entry Action</em>'.
	 * @see mncModel.Transition#getEntryAction()
	 * @see #getTransition()
	 * @generated
	 */
	EReference getTransition_EntryAction();

	/**
	 * Returns the meta object for the containment reference '{@link mncModel.Transition#getExitAction <em>Exit Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Exit Action</em>'.
	 * @see mncModel.Transition#getExitAction()
	 * @see #getTransition()
	 * @generated
	 */
	EReference getTransition_ExitAction();

	/**
	 * Returns the meta object for class '{@link mncModel.Response <em>Response</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Response</em>'.
	 * @see mncModel.Response
	 * @generated
	 */
	EClass getResponse();

	/**
	 * Returns the meta object for the attribute '{@link mncModel.Response#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see mncModel.Response#getName()
	 * @see #getResponse()
	 * @generated
	 */
	EAttribute getResponse_Name();

	/**
	 * Returns the meta object for the containment reference list '{@link mncModel.Response#getParameters <em>Parameters</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Parameters</em>'.
	 * @see mncModel.Response#getParameters()
	 * @see #getResponse()
	 * @generated
	 */
	EReference getResponse_Parameters();

	/**
	 * Returns the meta object for the containment reference '{@link mncModel.Response#getPublish <em>Publish</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Publish</em>'.
	 * @see mncModel.Response#getPublish()
	 * @see #getResponse()
	 * @generated
	 */
	EReference getResponse_Publish();

	/**
	 * Returns the meta object for class '{@link mncModel.ResponseBlock <em>Response Block</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Response Block</em>'.
	 * @see mncModel.ResponseBlock
	 * @generated
	 */
	EClass getResponseBlock();

	/**
	 * Returns the meta object for the reference '{@link mncModel.ResponseBlock#getResponse <em>Response</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Response</em>'.
	 * @see mncModel.ResponseBlock#getResponse()
	 * @see #getResponseBlock()
	 * @generated
	 */
	EReference getResponseBlock_Response();

	/**
	 * Returns the meta object for the containment reference '{@link mncModel.ResponseBlock#getResponseValidation <em>Response Validation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Response Validation</em>'.
	 * @see mncModel.ResponseBlock#getResponseValidation()
	 * @see #getResponseBlock()
	 * @generated
	 */
	EReference getResponseBlock_ResponseValidation();

	/**
	 * Returns the meta object for the containment reference '{@link mncModel.ResponseBlock#getResponseTranslation <em>Response Translation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Response Translation</em>'.
	 * @see mncModel.ResponseBlock#getResponseTranslation()
	 * @see #getResponseBlock()
	 * @generated
	 */
	EReference getResponseBlock_ResponseTranslation();

	/**
	 * Returns the meta object for the reference list '{@link mncModel.ResponseBlock#getDestinationNodes <em>Destination Nodes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Destination Nodes</em>'.
	 * @see mncModel.ResponseBlock#getDestinationNodes()
	 * @see #getResponseBlock()
	 * @generated
	 */
	EReference getResponseBlock_DestinationNodes();

	/**
	 * Returns the meta object for class '{@link mncModel.ResponseValidation <em>Response Validation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Response Validation</em>'.
	 * @see mncModel.ResponseValidation
	 * @generated
	 */
	EClass getResponseValidation();

	/**
	 * Returns the meta object for the reference '{@link mncModel.ResponseValidation#getResponse <em>Response</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Response</em>'.
	 * @see mncModel.ResponseValidation#getResponse()
	 * @see #getResponseValidation()
	 * @generated
	 */
	EReference getResponseValidation_Response();

	/**
	 * Returns the meta object for the containment reference list '{@link mncModel.ResponseValidation#getValidationRules <em>Validation Rules</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Validation Rules</em>'.
	 * @see mncModel.ResponseValidation#getValidationRules()
	 * @see #getResponseValidation()
	 * @generated
	 */
	EReference getResponseValidation_ValidationRules();

	/**
	 * Returns the meta object for class '{@link mncModel.ResponseDistribution <em>Response Distribution</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Response Distribution</em>'.
	 * @see mncModel.ResponseDistribution
	 * @generated
	 */
	EClass getResponseDistribution();

	/**
	 * Returns the meta object for the reference '{@link mncModel.ResponseDistribution#getResponse <em>Response</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Response</em>'.
	 * @see mncModel.ResponseDistribution#getResponse()
	 * @see #getResponseDistribution()
	 * @generated
	 */
	EReference getResponseDistribution_Response();

	/**
	 * Returns the meta object for the reference list '{@link mncModel.ResponseDistribution#getDestinationNodes <em>Destination Nodes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Destination Nodes</em>'.
	 * @see mncModel.ResponseDistribution#getDestinationNodes()
	 * @see #getResponseDistribution()
	 * @generated
	 */
	EReference getResponseDistribution_DestinationNodes();

	/**
	 * Returns the meta object for class '{@link mncModel.ResponseTranslation <em>Response Translation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Response Translation</em>'.
	 * @see mncModel.ResponseTranslation
	 * @generated
	 */
	EClass getResponseTranslation();

	/**
	 * Returns the meta object for the containment reference list '{@link mncModel.ResponseTranslation#getResponseTranslationRules <em>Response Translation Rules</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Response Translation Rules</em>'.
	 * @see mncModel.ResponseTranslation#getResponseTranslationRules()
	 * @see #getResponseTranslation()
	 * @generated
	 */
	EReference getResponseTranslation_ResponseTranslationRules();

	/**
	 * Returns the meta object for class '{@link mncModel.ResponseTranslationRule <em>Response Translation Rule</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Response Translation Rule</em>'.
	 * @see mncModel.ResponseTranslationRule
	 * @generated
	 */
	EClass getResponseTranslationRule();

	/**
	 * Returns the meta object for the reference list '{@link mncModel.ResponseTranslationRule#getInputResponses <em>Input Responses</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Input Responses</em>'.
	 * @see mncModel.ResponseTranslationRule#getInputResponses()
	 * @see #getResponseTranslationRule()
	 * @generated
	 */
	EReference getResponseTranslationRule_InputResponses();

	/**
	 * Returns the meta object for the reference '{@link mncModel.ResponseTranslationRule#getTranslatedResponse <em>Translated Response</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Translated Response</em>'.
	 * @see mncModel.ResponseTranslationRule#getTranslatedResponse()
	 * @see #getResponseTranslationRule()
	 * @generated
	 */
	EReference getResponseTranslationRule_TranslatedResponse();

	/**
	 * Returns the meta object for the containment reference list '{@link mncModel.ResponseTranslationRule#getParameterTranslations <em>Parameter Translations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Parameter Translations</em>'.
	 * @see mncModel.ResponseTranslationRule#getParameterTranslations()
	 * @see #getResponseTranslationRule()
	 * @generated
	 */
	EReference getResponseTranslationRule_ParameterTranslations();

	/**
	 * Returns the meta object for the containment reference '{@link mncModel.ResponseTranslationRule#getOperation <em>Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Operation</em>'.
	 * @see mncModel.ResponseTranslationRule#getOperation()
	 * @see #getResponseTranslationRule()
	 * @generated
	 */
	EReference getResponseTranslationRule_Operation();

	/**
	 * Returns the meta object for class '{@link mncModel.Port <em>Port</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Port</em>'.
	 * @see mncModel.Port
	 * @generated
	 */
	EClass getPort();

	/**
	 * Returns the meta object for the attribute '{@link mncModel.Port#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see mncModel.Port#getName()
	 * @see #getPort()
	 * @generated
	 */
	EAttribute getPort_Name();

	/**
	 * Returns the meta object for the attribute '{@link mncModel.Port#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see mncModel.Port#getValue()
	 * @see #getPort()
	 * @generated
	 */
	EAttribute getPort_Value();

	/**
	 * Returns the meta object for class '{@link mncModel.EventBlock <em>Event Block</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Event Block</em>'.
	 * @see mncModel.EventBlock
	 * @generated
	 */
	EClass getEventBlock();

	/**
	 * Returns the meta object for the reference '{@link mncModel.EventBlock#getEvent <em>Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Event</em>'.
	 * @see mncModel.EventBlock#getEvent()
	 * @see #getEventBlock()
	 * @generated
	 */
	EReference getEventBlock_Event();

	/**
	 * Returns the meta object for the containment reference '{@link mncModel.EventBlock#getEventCondition <em>Event Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Event Condition</em>'.
	 * @see mncModel.EventBlock#getEventCondition()
	 * @see #getEventBlock()
	 * @generated
	 */
	EReference getEventBlock_EventCondition();

	/**
	 * Returns the meta object for the containment reference '{@link mncModel.EventBlock#getEventHandling <em>Event Handling</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Event Handling</em>'.
	 * @see mncModel.EventBlock#getEventHandling()
	 * @see #getEventBlock()
	 * @generated
	 */
	EReference getEventBlock_EventHandling();

	/**
	 * Returns the meta object for class '{@link mncModel.Event <em>Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Event</em>'.
	 * @see mncModel.Event
	 * @generated
	 */
	EClass getEvent();

	/**
	 * Returns the meta object for the attribute '{@link mncModel.Event#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see mncModel.Event#getName()
	 * @see #getEvent()
	 * @generated
	 */
	EAttribute getEvent_Name();

	/**
	 * Returns the meta object for the containment reference '{@link mncModel.Event#getPublish <em>Publish</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Publish</em>'.
	 * @see mncModel.Event#getPublish()
	 * @see #getEvent()
	 * @generated
	 */
	EReference getEvent_Publish();

	/**
	 * Returns the meta object for the containment reference list '{@link mncModel.Event#getParameters <em>Parameters</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Parameters</em>'.
	 * @see mncModel.Event#getParameters()
	 * @see #getEvent()
	 * @generated
	 */
	EReference getEvent_Parameters();

	/**
	 * Returns the meta object for class '{@link mncModel.EventTriggerCondition <em>Event Trigger Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Event Trigger Condition</em>'.
	 * @see mncModel.EventTriggerCondition
	 * @generated
	 */
	EClass getEventTriggerCondition();

	/**
	 * Returns the meta object for the containment reference '{@link mncModel.EventTriggerCondition#getEventTranslation <em>Event Translation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Event Translation</em>'.
	 * @see mncModel.EventTriggerCondition#getEventTranslation()
	 * @see #getEventTriggerCondition()
	 * @generated
	 */
	EReference getEventTriggerCondition_EventTranslation();

	/**
	 * Returns the meta object for the containment reference '{@link mncModel.EventTriggerCondition#getOperation <em>Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Operation</em>'.
	 * @see mncModel.EventTriggerCondition#getOperation()
	 * @see #getEventTriggerCondition()
	 * @generated
	 */
	EReference getEventTriggerCondition_Operation();

	/**
	 * Returns the meta object for the containment reference '{@link mncModel.EventTriggerCondition#getResponsibleItems <em>Responsible Items</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Responsible Items</em>'.
	 * @see mncModel.EventTriggerCondition#getResponsibleItems()
	 * @see #getEventTriggerCondition()
	 * @generated
	 */
	EReference getEventTriggerCondition_ResponsibleItems();

	/**
	 * Returns the meta object for class '{@link mncModel.EventTranslation <em>Event Translation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Event Translation</em>'.
	 * @see mncModel.EventTranslation
	 * @generated
	 */
	EClass getEventTranslation();

	/**
	 * Returns the meta object for the containment reference list '{@link mncModel.EventTranslation#getEventTranslationRules <em>Event Translation Rules</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Event Translation Rules</em>'.
	 * @see mncModel.EventTranslation#getEventTranslationRules()
	 * @see #getEventTranslation()
	 * @generated
	 */
	EReference getEventTranslation_EventTranslationRules();

	/**
	 * Returns the meta object for class '{@link mncModel.EventTranslationRule <em>Event Translation Rule</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Event Translation Rule</em>'.
	 * @see mncModel.EventTranslationRule
	 * @generated
	 */
	EClass getEventTranslationRule();

	/**
	 * Returns the meta object for the reference list '{@link mncModel.EventTranslationRule#getEvents <em>Events</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Events</em>'.
	 * @see mncModel.EventTranslationRule#getEvents()
	 * @see #getEventTranslationRule()
	 * @generated
	 */
	EReference getEventTranslationRule_Events();

	/**
	 * Returns the meta object for the reference '{@link mncModel.EventTranslationRule#getTranslatedEvent <em>Translated Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Translated Event</em>'.
	 * @see mncModel.EventTranslationRule#getTranslatedEvent()
	 * @see #getEventTranslationRule()
	 * @generated
	 */
	EReference getEventTranslationRule_TranslatedEvent();

	/**
	 * Returns the meta object for the containment reference '{@link mncModel.EventTranslationRule#getOperation <em>Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Operation</em>'.
	 * @see mncModel.EventTranslationRule#getOperation()
	 * @see #getEventTranslationRule()
	 * @generated
	 */
	EReference getEventTranslationRule_Operation();

	/**
	 * Returns the meta object for class '{@link mncModel.EventHandling <em>Event Handling</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Event Handling</em>'.
	 * @see mncModel.EventHandling
	 * @generated
	 */
	EClass getEventHandling();

	/**
	 * Returns the meta object for the containment reference '{@link mncModel.EventHandling#getTriggerAction <em>Trigger Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Trigger Action</em>'.
	 * @see mncModel.EventHandling#getTriggerAction()
	 * @see #getEventHandling()
	 * @generated
	 */
	EReference getEventHandling_TriggerAction();

	/**
	 * Returns the meta object for class '{@link mncModel.AlarmBlock <em>Alarm Block</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Alarm Block</em>'.
	 * @see mncModel.AlarmBlock
	 * @generated
	 */
	EClass getAlarmBlock();

	/**
	 * Returns the meta object for the reference '{@link mncModel.AlarmBlock#getAlarm <em>Alarm</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Alarm</em>'.
	 * @see mncModel.AlarmBlock#getAlarm()
	 * @see #getAlarmBlock()
	 * @generated
	 */
	EReference getAlarmBlock_Alarm();

	/**
	 * Returns the meta object for the containment reference '{@link mncModel.AlarmBlock#getAlarmCondition <em>Alarm Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Alarm Condition</em>'.
	 * @see mncModel.AlarmBlock#getAlarmCondition()
	 * @see #getAlarmBlock()
	 * @generated
	 */
	EReference getAlarmBlock_AlarmCondition();

	/**
	 * Returns the meta object for the containment reference '{@link mncModel.AlarmBlock#getAlarmHandling <em>Alarm Handling</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Alarm Handling</em>'.
	 * @see mncModel.AlarmBlock#getAlarmHandling()
	 * @see #getAlarmBlock()
	 * @generated
	 */
	EReference getAlarmBlock_AlarmHandling();

	/**
	 * Returns the meta object for class '{@link mncModel.Alarm <em>Alarm</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Alarm</em>'.
	 * @see mncModel.Alarm
	 * @generated
	 */
	EClass getAlarm();

	/**
	 * Returns the meta object for the attribute '{@link mncModel.Alarm#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see mncModel.Alarm#getName()
	 * @see #getAlarm()
	 * @generated
	 */
	EAttribute getAlarm_Name();

	/**
	 * Returns the meta object for the attribute '{@link mncModel.Alarm#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see mncModel.Alarm#getType()
	 * @see #getAlarm()
	 * @generated
	 */
	EAttribute getAlarm_Type();

	/**
	 * Returns the meta object for the attribute '{@link mncModel.Alarm#getLevel <em>Level</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Level</em>'.
	 * @see mncModel.Alarm#getLevel()
	 * @see #getAlarm()
	 * @generated
	 */
	EAttribute getAlarm_Level();

	/**
	 * Returns the meta object for the containment reference '{@link mncModel.Alarm#getPublish <em>Publish</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Publish</em>'.
	 * @see mncModel.Alarm#getPublish()
	 * @see #getAlarm()
	 * @generated
	 */
	EReference getAlarm_Publish();

	/**
	 * Returns the meta object for the containment reference list '{@link mncModel.Alarm#getParameters <em>Parameters</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Parameters</em>'.
	 * @see mncModel.Alarm#getParameters()
	 * @see #getAlarm()
	 * @generated
	 */
	EReference getAlarm_Parameters();

	/**
	 * Returns the meta object for class '{@link mncModel.AlarmTriggerCondition <em>Alarm Trigger Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Alarm Trigger Condition</em>'.
	 * @see mncModel.AlarmTriggerCondition
	 * @generated
	 */
	EClass getAlarmTriggerCondition();

	/**
	 * Returns the meta object for the containment reference '{@link mncModel.AlarmTriggerCondition#getAlarmTranslation <em>Alarm Translation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Alarm Translation</em>'.
	 * @see mncModel.AlarmTriggerCondition#getAlarmTranslation()
	 * @see #getAlarmTriggerCondition()
	 * @generated
	 */
	EReference getAlarmTriggerCondition_AlarmTranslation();

	/**
	 * Returns the meta object for the containment reference '{@link mncModel.AlarmTriggerCondition#getOperation <em>Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Operation</em>'.
	 * @see mncModel.AlarmTriggerCondition#getOperation()
	 * @see #getAlarmTriggerCondition()
	 * @generated
	 */
	EReference getAlarmTriggerCondition_Operation();

	/**
	 * Returns the meta object for the containment reference '{@link mncModel.AlarmTriggerCondition#getResponsibleItems <em>Responsible Items</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Responsible Items</em>'.
	 * @see mncModel.AlarmTriggerCondition#getResponsibleItems()
	 * @see #getAlarmTriggerCondition()
	 * @generated
	 */
	EReference getAlarmTriggerCondition_ResponsibleItems();

	/**
	 * Returns the meta object for class '{@link mncModel.AlarmTranslation <em>Alarm Translation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Alarm Translation</em>'.
	 * @see mncModel.AlarmTranslation
	 * @generated
	 */
	EClass getAlarmTranslation();

	/**
	 * Returns the meta object for the containment reference list '{@link mncModel.AlarmTranslation#getAlarmTranslationRules <em>Alarm Translation Rules</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Alarm Translation Rules</em>'.
	 * @see mncModel.AlarmTranslation#getAlarmTranslationRules()
	 * @see #getAlarmTranslation()
	 * @generated
	 */
	EReference getAlarmTranslation_AlarmTranslationRules();

	/**
	 * Returns the meta object for class '{@link mncModel.AlarmTranslationRule <em>Alarm Translation Rule</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Alarm Translation Rule</em>'.
	 * @see mncModel.AlarmTranslationRule
	 * @generated
	 */
	EClass getAlarmTranslationRule();

	/**
	 * Returns the meta object for the containment reference '{@link mncModel.AlarmTranslationRule#getOperation <em>Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Operation</em>'.
	 * @see mncModel.AlarmTranslationRule#getOperation()
	 * @see #getAlarmTranslationRule()
	 * @generated
	 */
	EReference getAlarmTranslationRule_Operation();

	/**
	 * Returns the meta object for class '{@link mncModel.AlarmHandling <em>Alarm Handling</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Alarm Handling</em>'.
	 * @see mncModel.AlarmHandling
	 * @generated
	 */
	EClass getAlarmHandling();

	/**
	 * Returns the meta object for the containment reference '{@link mncModel.AlarmHandling#getTriggerAction <em>Trigger Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Trigger Action</em>'.
	 * @see mncModel.AlarmHandling#getTriggerAction()
	 * @see #getAlarmHandling()
	 * @generated
	 */
	EReference getAlarmHandling_TriggerAction();

	/**
	 * Returns the meta object for class '{@link mncModel.DataPointBlock <em>Data Point Block</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Data Point Block</em>'.
	 * @see mncModel.DataPointBlock
	 * @generated
	 */
	EClass getDataPointBlock();

	/**
	 * Returns the meta object for the reference '{@link mncModel.DataPointBlock#getDataPoint <em>Data Point</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Data Point</em>'.
	 * @see mncModel.DataPointBlock#getDataPoint()
	 * @see #getDataPointBlock()
	 * @generated
	 */
	EReference getDataPointBlock_DataPoint();

	/**
	 * Returns the meta object for the containment reference '{@link mncModel.DataPointBlock#getDataPointCondition <em>Data Point Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Point Condition</em>'.
	 * @see mncModel.DataPointBlock#getDataPointCondition()
	 * @see #getDataPointBlock()
	 * @generated
	 */
	EReference getDataPointBlock_DataPointCondition();

	/**
	 * Returns the meta object for the containment reference '{@link mncModel.DataPointBlock#getDataPointHandling <em>Data Point Handling</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Point Handling</em>'.
	 * @see mncModel.DataPointBlock#getDataPointHandling()
	 * @see #getDataPointBlock()
	 * @generated
	 */
	EReference getDataPointBlock_DataPointHandling();

	/**
	 * Returns the meta object for class '{@link mncModel.DataPoint <em>Data Point</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Data Point</em>'.
	 * @see mncModel.DataPoint
	 * @generated
	 */
	EClass getDataPoint();

	/**
	 * Returns the meta object for the attribute '{@link mncModel.DataPoint#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see mncModel.DataPoint#getName()
	 * @see #getDataPoint()
	 * @generated
	 */
	EAttribute getDataPoint_Name();

	/**
	 * Returns the meta object for the attribute '{@link mncModel.DataPoint#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see mncModel.DataPoint#getType()
	 * @see #getDataPoint()
	 * @generated
	 */
	EAttribute getDataPoint_Type();

	/**
	 * Returns the meta object for the containment reference '{@link mncModel.DataPoint#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Value</em>'.
	 * @see mncModel.DataPoint#getValue()
	 * @see #getDataPoint()
	 * @generated
	 */
	EReference getDataPoint_Value();

	/**
	 * Returns the meta object for the containment reference '{@link mncModel.DataPoint#getPublish <em>Publish</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Publish</em>'.
	 * @see mncModel.DataPoint#getPublish()
	 * @see #getDataPoint()
	 * @generated
	 */
	EReference getDataPoint_Publish();

	/**
	 * Returns the meta object for the containment reference list '{@link mncModel.DataPoint#getParameters <em>Parameters</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Parameters</em>'.
	 * @see mncModel.DataPoint#getParameters()
	 * @see #getDataPoint()
	 * @generated
	 */
	EReference getDataPoint_Parameters();

	/**
	 * Returns the meta object for class '{@link mncModel.DataPointTriggerCondition <em>Data Point Trigger Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Data Point Trigger Condition</em>'.
	 * @see mncModel.DataPointTriggerCondition
	 * @generated
	 */
	EClass getDataPointTriggerCondition();

	/**
	 * Returns the meta object for the containment reference '{@link mncModel.DataPointTriggerCondition#getDataPointTranslation <em>Data Point Translation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Data Point Translation</em>'.
	 * @see mncModel.DataPointTriggerCondition#getDataPointTranslation()
	 * @see #getDataPointTriggerCondition()
	 * @generated
	 */
	EReference getDataPointTriggerCondition_DataPointTranslation();

	/**
	 * Returns the meta object for the containment reference '{@link mncModel.DataPointTriggerCondition#getResponsibleItems <em>Responsible Items</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Responsible Items</em>'.
	 * @see mncModel.DataPointTriggerCondition#getResponsibleItems()
	 * @see #getDataPointTriggerCondition()
	 * @generated
	 */
	EReference getDataPointTriggerCondition_ResponsibleItems();

	/**
	 * Returns the meta object for the containment reference '{@link mncModel.DataPointTriggerCondition#getOperation <em>Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Operation</em>'.
	 * @see mncModel.DataPointTriggerCondition#getOperation()
	 * @see #getDataPointTriggerCondition()
	 * @generated
	 */
	EReference getDataPointTriggerCondition_Operation();

	/**
	 * Returns the meta object for class '{@link mncModel.DataPointTranslation <em>Data Point Translation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Data Point Translation</em>'.
	 * @see mncModel.DataPointTranslation
	 * @generated
	 */
	EClass getDataPointTranslation();

	/**
	 * Returns the meta object for the containment reference list '{@link mncModel.DataPointTranslation#getDataPointTranslationRules <em>Data Point Translation Rules</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Data Point Translation Rules</em>'.
	 * @see mncModel.DataPointTranslation#getDataPointTranslationRules()
	 * @see #getDataPointTranslation()
	 * @generated
	 */
	EReference getDataPointTranslation_DataPointTranslationRules();

	/**
	 * Returns the meta object for class '{@link mncModel.DataPointTranslationRule <em>Data Point Translation Rule</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Data Point Translation Rule</em>'.
	 * @see mncModel.DataPointTranslationRule
	 * @generated
	 */
	EClass getDataPointTranslationRule();

	/**
	 * Returns the meta object for the reference list '{@link mncModel.DataPointTranslationRule#getDataPoints <em>Data Points</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Data Points</em>'.
	 * @see mncModel.DataPointTranslationRule#getDataPoints()
	 * @see #getDataPointTranslationRule()
	 * @generated
	 */
	EReference getDataPointTranslationRule_DataPoints();

	/**
	 * Returns the meta object for the containment reference '{@link mncModel.DataPointTranslationRule#getOperation <em>Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Operation</em>'.
	 * @see mncModel.DataPointTranslationRule#getOperation()
	 * @see #getDataPointTranslationRule()
	 * @generated
	 */
	EReference getDataPointTranslationRule_Operation();

	/**
	 * Returns the meta object for class '{@link mncModel.DataPointHandling <em>Data Point Handling</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Data Point Handling</em>'.
	 * @see mncModel.DataPointHandling
	 * @generated
	 */
	EClass getDataPointHandling();

	/**
	 * Returns the meta object for the containment reference '{@link mncModel.DataPointHandling#getCheckDataPoint <em>Check Data Point</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Check Data Point</em>'.
	 * @see mncModel.DataPointHandling#getCheckDataPoint()
	 * @see #getDataPointHandling()
	 * @generated
	 */
	EReference getDataPointHandling_CheckDataPoint();

	/**
	 * Returns the meta object for the containment reference '{@link mncModel.DataPointHandling#getAction <em>Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Action</em>'.
	 * @see mncModel.DataPointHandling#getAction()
	 * @see #getDataPointHandling()
	 * @generated
	 */
	EReference getDataPointHandling_Action();

	/**
	 * Returns the meta object for class '{@link mncModel.DataPointValidCondition <em>Data Point Valid Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Data Point Valid Condition</em>'.
	 * @see mncModel.DataPointValidCondition
	 * @generated
	 */
	EClass getDataPointValidCondition();

	/**
	 * Returns the meta object for the reference '{@link mncModel.DataPointValidCondition#getDataPoint <em>Data Point</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Data Point</em>'.
	 * @see mncModel.DataPointValidCondition#getDataPoint()
	 * @see #getDataPointValidCondition()
	 * @generated
	 */
	EReference getDataPointValidCondition_DataPoint();

	/**
	 * Returns the meta object for the containment reference list '{@link mncModel.DataPointValidCondition#getCheckValues <em>Check Values</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Check Values</em>'.
	 * @see mncModel.DataPointValidCondition#getCheckValues()
	 * @see #getDataPointValidCondition()
	 * @generated
	 */
	EReference getDataPointValidCondition_CheckValues();

	/**
	 * Returns the meta object for the containment reference '{@link mncModel.DataPointValidCondition#getCheckMaxValue <em>Check Max Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Check Max Value</em>'.
	 * @see mncModel.DataPointValidCondition#getCheckMaxValue()
	 * @see #getDataPointValidCondition()
	 * @generated
	 */
	EReference getDataPointValidCondition_CheckMaxValue();

	/**
	 * Returns the meta object for the containment reference '{@link mncModel.DataPointValidCondition#getCheckMinValue <em>Check Min Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Check Min Value</em>'.
	 * @see mncModel.DataPointValidCondition#getCheckMinValue()
	 * @see #getDataPointValidCondition()
	 * @generated
	 */
	EReference getDataPointValidCondition_CheckMinValue();

	/**
	 * Returns the meta object for class '{@link mncModel.Parameter <em>Parameter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Parameter</em>'.
	 * @see mncModel.Parameter
	 * @generated
	 */
	EClass getParameter();

	/**
	 * Returns the meta object for class '{@link mncModel.CheckParameterCondition <em>Check Parameter Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Check Parameter Condition</em>'.
	 * @see mncModel.CheckParameterCondition
	 * @generated
	 */
	EClass getCheckParameterCondition();

	/**
	 * Returns the meta object for the reference '{@link mncModel.CheckParameterCondition#getParameter <em>Parameter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Parameter</em>'.
	 * @see mncModel.CheckParameterCondition#getParameter()
	 * @see #getCheckParameterCondition()
	 * @generated
	 */
	EReference getCheckParameterCondition_Parameter();

	/**
	 * Returns the meta object for the containment reference list '{@link mncModel.CheckParameterCondition#getCheckValues <em>Check Values</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Check Values</em>'.
	 * @see mncModel.CheckParameterCondition#getCheckValues()
	 * @see #getCheckParameterCondition()
	 * @generated
	 */
	EReference getCheckParameterCondition_CheckValues();

	/**
	 * Returns the meta object for the containment reference '{@link mncModel.CheckParameterCondition#getCheckMaxValue <em>Check Max Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Check Max Value</em>'.
	 * @see mncModel.CheckParameterCondition#getCheckMaxValue()
	 * @see #getCheckParameterCondition()
	 * @generated
	 */
	EReference getCheckParameterCondition_CheckMaxValue();

	/**
	 * Returns the meta object for the containment reference '{@link mncModel.CheckParameterCondition#getCheckMinValue <em>Check Min Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Check Min Value</em>'.
	 * @see mncModel.CheckParameterCondition#getCheckMinValue()
	 * @see #getCheckParameterCondition()
	 * @generated
	 */
	EReference getCheckParameterCondition_CheckMinValue();

	/**
	 * Returns the meta object for class '{@link mncModel.ArrayType <em>Array Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Array Type</em>'.
	 * @see mncModel.ArrayType
	 * @generated
	 */
	EClass getArrayType();

	/**
	 * Returns the meta object for the attribute '{@link mncModel.ArrayType#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see mncModel.ArrayType#getType()
	 * @see #getArrayType()
	 * @generated
	 */
	EAttribute getArrayType_Type();

	/**
	 * Returns the meta object for the attribute '{@link mncModel.ArrayType#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see mncModel.ArrayType#getName()
	 * @see #getArrayType()
	 * @generated
	 */
	EAttribute getArrayType_Name();

	/**
	 * Returns the meta object for the containment reference list '{@link mncModel.ArrayType#getValues <em>Values</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Values</em>'.
	 * @see mncModel.ArrayType#getValues()
	 * @see #getArrayType()
	 * @generated
	 */
	EReference getArrayType_Values();

	/**
	 * Returns the meta object for class '{@link mncModel.SimpleType <em>Simple Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Simple Type</em>'.
	 * @see mncModel.SimpleType
	 * @generated
	 */
	EClass getSimpleType();

	/**
	 * Returns the meta object for the attribute '{@link mncModel.SimpleType#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see mncModel.SimpleType#getName()
	 * @see #getSimpleType()
	 * @generated
	 */
	EAttribute getSimpleType_Name();

	/**
	 * Returns the meta object for the attribute '{@link mncModel.SimpleType#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see mncModel.SimpleType#getType()
	 * @see #getSimpleType()
	 * @generated
	 */
	EAttribute getSimpleType_Type();

	/**
	 * Returns the meta object for the containment reference '{@link mncModel.SimpleType#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Value</em>'.
	 * @see mncModel.SimpleType#getValue()
	 * @see #getSimpleType()
	 * @generated
	 */
	EReference getSimpleType_Value();

	/**
	 * Returns the meta object for class '{@link mncModel.PrimitiveValue <em>Primitive Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Primitive Value</em>'.
	 * @see mncModel.PrimitiveValue
	 * @generated
	 */
	EClass getPrimitiveValue();

	/**
	 * Returns the meta object for class '{@link mncModel.IntValue <em>Int Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Int Value</em>'.
	 * @see mncModel.IntValue
	 * @generated
	 */
	EClass getIntValue();

	/**
	 * Returns the meta object for the attribute '{@link mncModel.IntValue#getIntValue <em>Int Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Int Value</em>'.
	 * @see mncModel.IntValue#getIntValue()
	 * @see #getIntValue()
	 * @generated
	 */
	EAttribute getIntValue_IntValue();

	/**
	 * Returns the meta object for class '{@link mncModel.BoolValue <em>Bool Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Bool Value</em>'.
	 * @see mncModel.BoolValue
	 * @generated
	 */
	EClass getBoolValue();

	/**
	 * Returns the meta object for the attribute '{@link mncModel.BoolValue#isBoolValue <em>Bool Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Bool Value</em>'.
	 * @see mncModel.BoolValue#isBoolValue()
	 * @see #getBoolValue()
	 * @generated
	 */
	EAttribute getBoolValue_BoolValue();

	/**
	 * Returns the meta object for class '{@link mncModel.FloatValue <em>Float Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Float Value</em>'.
	 * @see mncModel.FloatValue
	 * @generated
	 */
	EClass getFloatValue();

	/**
	 * Returns the meta object for the attribute '{@link mncModel.FloatValue#getFloatValue <em>Float Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Float Value</em>'.
	 * @see mncModel.FloatValue#getFloatValue()
	 * @see #getFloatValue()
	 * @generated
	 */
	EAttribute getFloatValue_FloatValue();

	/**
	 * Returns the meta object for class '{@link mncModel.StringValue <em>String Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>String Value</em>'.
	 * @see mncModel.StringValue
	 * @generated
	 */
	EClass getStringValue();

	/**
	 * Returns the meta object for the attribute '{@link mncModel.StringValue#getStringValue <em>String Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>String Value</em>'.
	 * @see mncModel.StringValue#getStringValue()
	 * @see #getStringValue()
	 * @generated
	 */
	EAttribute getStringValue_StringValue();

	/**
	 * Returns the meta object for class '{@link mncModel.Operation <em>Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Operation</em>'.
	 * @see mncModel.Operation
	 * @generated
	 */
	EClass getOperation();

	/**
	 * Returns the meta object for the attribute '{@link mncModel.Operation#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see mncModel.Operation#getName()
	 * @see #getOperation()
	 * @generated
	 */
	EAttribute getOperation_Name();

	/**
	 * Returns the meta object for the reference list '{@link mncModel.Operation#getInputParameters <em>Input Parameters</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Input Parameters</em>'.
	 * @see mncModel.Operation#getInputParameters()
	 * @see #getOperation()
	 * @generated
	 */
	EReference getOperation_InputParameters();

	/**
	 * Returns the meta object for the reference list '{@link mncModel.Operation#getOutputParameters <em>Output Parameters</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Output Parameters</em>'.
	 * @see mncModel.Operation#getOutputParameters()
	 * @see #getOperation()
	 * @generated
	 */
	EReference getOperation_OutputParameters();

	/**
	 * Returns the meta object for the attribute '{@link mncModel.Operation#getScript <em>Script</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Script</em>'.
	 * @see mncModel.Operation#getScript()
	 * @see #getOperation()
	 * @generated
	 */
	EAttribute getOperation_Script();

	/**
	 * Returns the meta object for class '{@link mncModel.Address <em>Address</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Address</em>'.
	 * @see mncModel.Address
	 * @generated
	 */
	EClass getAddress();

	/**
	 * Returns the meta object for the attribute '{@link mncModel.Address#getIpaddress <em>Ipaddress</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Ipaddress</em>'.
	 * @see mncModel.Address#getIpaddress()
	 * @see #getAddress()
	 * @generated
	 */
	EAttribute getAddress_Ipaddress();

	/**
	 * Returns the meta object for class '{@link mncModel.Action <em>Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Action</em>'.
	 * @see mncModel.Action
	 * @generated
	 */
	EClass getAction();

	/**
	 * Returns the meta object for the reference list '{@link mncModel.Action#getCommand <em>Command</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Command</em>'.
	 * @see mncModel.Action#getCommand()
	 * @see #getAction()
	 * @generated
	 */
	EReference getAction_Command();

	/**
	 * Returns the meta object for the reference list '{@link mncModel.Action#getEvent <em>Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Event</em>'.
	 * @see mncModel.Action#getEvent()
	 * @see #getAction()
	 * @generated
	 */
	EReference getAction_Event();

	/**
	 * Returns the meta object for the reference list '{@link mncModel.Action#getAlarm <em>Alarm</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Alarm</em>'.
	 * @see mncModel.Action#getAlarm()
	 * @see #getAction()
	 * @generated
	 */
	EReference getAction_Alarm();

	/**
	 * Returns the meta object for the containment reference '{@link mncModel.Action#getOperation <em>Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Operation</em>'.
	 * @see mncModel.Action#getOperation()
	 * @see #getAction()
	 * @generated
	 */
	EReference getAction_Operation();

	/**
	 * Returns the meta object for class '{@link mncModel.Publish <em>Publish</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Publish</em>'.
	 * @see mncModel.Publish
	 * @generated
	 */
	EClass getPublish();

	/**
	 * Returns the meta object for class '{@link mncModel.SubscribableItems <em>Subscribable Items</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Subscribable Items</em>'.
	 * @see mncModel.SubscribableItems
	 * @generated
	 */
	EClass getSubscribableItems();

	/**
	 * Returns the meta object for class '{@link mncModel.SubscribableItemList <em>Subscribable Item List</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Subscribable Item List</em>'.
	 * @see mncModel.SubscribableItemList
	 * @generated
	 */
	EClass getSubscribableItemList();

	/**
	 * Returns the meta object for the reference list '{@link mncModel.SubscribableItemList#getSubscribedEvents <em>Subscribed Events</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Subscribed Events</em>'.
	 * @see mncModel.SubscribableItemList#getSubscribedEvents()
	 * @see #getSubscribableItemList()
	 * @generated
	 */
	EReference getSubscribableItemList_SubscribedEvents();

	/**
	 * Returns the meta object for the reference list '{@link mncModel.SubscribableItemList#getSubscribedAlarms <em>Subscribed Alarms</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Subscribed Alarms</em>'.
	 * @see mncModel.SubscribableItemList#getSubscribedAlarms()
	 * @see #getSubscribableItemList()
	 * @generated
	 */
	EReference getSubscribableItemList_SubscribedAlarms();

	/**
	 * Returns the meta object for the reference list '{@link mncModel.SubscribableItemList#getSubscribedDataPoints <em>Subscribed Data Points</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Subscribed Data Points</em>'.
	 * @see mncModel.SubscribableItemList#getSubscribedDataPoints()
	 * @see #getSubscribableItemList()
	 * @generated
	 */
	EReference getSubscribableItemList_SubscribedDataPoints();

	/**
	 * Returns the meta object for class '{@link mncModel.ResponsibleItemList <em>Responsible Item List</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Responsible Item List</em>'.
	 * @see mncModel.ResponsibleItemList
	 * @generated
	 */
	EClass getResponsibleItemList();

	/**
	 * Returns the meta object for the reference list '{@link mncModel.ResponsibleItemList#getResponsibleCommands <em>Responsible Commands</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Responsible Commands</em>'.
	 * @see mncModel.ResponsibleItemList#getResponsibleCommands()
	 * @see #getResponsibleItemList()
	 * @generated
	 */
	EReference getResponsibleItemList_ResponsibleCommands();

	/**
	 * Returns the meta object for the reference list '{@link mncModel.ResponsibleItemList#getResponsibleEvents <em>Responsible Events</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Responsible Events</em>'.
	 * @see mncModel.ResponsibleItemList#getResponsibleEvents()
	 * @see #getResponsibleItemList()
	 * @generated
	 */
	EReference getResponsibleItemList_ResponsibleEvents();

	/**
	 * Returns the meta object for the reference list '{@link mncModel.ResponsibleItemList#getResponsibleDataPoints <em>Responsible Data Points</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Responsible Data Points</em>'.
	 * @see mncModel.ResponsibleItemList#getResponsibleDataPoints()
	 * @see #getResponsibleItemList()
	 * @generated
	 */
	EReference getResponsibleItemList_ResponsibleDataPoints();

	/**
	 * Returns the meta object for the reference list '{@link mncModel.ResponsibleItemList#getResponsibleAlarms <em>Responsible Alarms</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Responsible Alarms</em>'.
	 * @see mncModel.ResponsibleItemList#getResponsibleAlarms()
	 * @see #getResponsibleItemList()
	 * @generated
	 */
	EReference getResponsibleItemList_ResponsibleAlarms();

	/**
	 * Returns the meta object for enum '{@link mncModel.PrimitiveValueType <em>Primitive Value Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Primitive Value Type</em>'.
	 * @see mncModel.PrimitiveValueType
	 * @generated
	 */
	EEnum getPrimitiveValueType();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	MncModelFactory getMncModelFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link mncModel.impl.ModelImpl <em>Model</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mncModel.impl.ModelImpl
		 * @see mncModel.impl.MncModelPackageImpl#getModel()
		 * @generated
		 */
		EClass MODEL = eINSTANCE.getModel();

		/**
		 * The meta object literal for the '<em><b>Systems</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MODEL__SYSTEMS = eINSTANCE.getModel_Systems();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MODEL__NAME = eINSTANCE.getModel_Name();

		/**
		 * The meta object literal for the '<em><b>Import Section</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MODEL__IMPORT_SECTION = eINSTANCE.getModel_ImportSection();

		/**
		 * The meta object literal for the '<em><b>Security</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MODEL__SECURITY = eINSTANCE.getModel_Security();

		/**
		 * The meta object literal for the '<em><b>Stakeholder</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MODEL__STAKEHOLDER = eINSTANCE.getModel_Stakeholder();

		/**
		 * The meta object literal for the '{@link mncModel.impl.ImportImpl <em>Import</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mncModel.impl.ImportImpl
		 * @see mncModel.impl.MncModelPackageImpl#getImport()
		 * @generated
		 */
		EClass IMPORT = eINSTANCE.getImport();

		/**
		 * The meta object literal for the '<em><b>Imported Namespace</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IMPORT__IMPORTED_NAMESPACE = eINSTANCE.getImport_ImportedNamespace();

		/**
		 * The meta object literal for the '{@link mncModel.impl.SystemImpl <em>System</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mncModel.impl.SystemImpl
		 * @see mncModel.impl.MncModelPackageImpl#getSystem()
		 * @generated
		 */
		EClass SYSTEM = eINSTANCE.getSystem();

		/**
		 * The meta object literal for the '{@link mncModel.impl.ControlNodeImpl <em>Control Node</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mncModel.impl.ControlNodeImpl
		 * @see mncModel.impl.MncModelPackageImpl#getControlNode()
		 * @generated
		 */
		EClass CONTROL_NODE = eINSTANCE.getControlNode();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CONTROL_NODE__NAME = eINSTANCE.getControlNode_Name();

		/**
		 * The meta object literal for the '<em><b>Interface Description</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONTROL_NODE__INTERFACE_DESCRIPTION = eINSTANCE.getControlNode_InterfaceDescription();

		/**
		 * The meta object literal for the '<em><b>Child Nodes</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONTROL_NODE__CHILD_NODES = eINSTANCE.getControlNode_ChildNodes();

		/**
		 * The meta object literal for the '<em><b>Parent Node</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONTROL_NODE__PARENT_NODE = eINSTANCE.getControlNode_ParentNode();

		/**
		 * The meta object literal for the '<em><b>Command Response Blocks</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONTROL_NODE__COMMAND_RESPONSE_BLOCKS = eINSTANCE.getControlNode_CommandResponseBlocks();

		/**
		 * The meta object literal for the '<em><b>Event Blocks</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONTROL_NODE__EVENT_BLOCKS = eINSTANCE.getControlNode_EventBlocks();

		/**
		 * The meta object literal for the '<em><b>Alarm Blocks</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONTROL_NODE__ALARM_BLOCKS = eINSTANCE.getControlNode_AlarmBlocks();

		/**
		 * The meta object literal for the '<em><b>Data Point Blocks</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONTROL_NODE__DATA_POINT_BLOCKS = eINSTANCE.getControlNode_DataPointBlocks();

		/**
		 * The meta object literal for the '{@link mncModel.impl.InterfaceDescriptionImpl <em>Interface Description</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mncModel.impl.InterfaceDescriptionImpl
		 * @see mncModel.impl.MncModelPackageImpl#getInterfaceDescription()
		 * @generated
		 */
		EClass INTERFACE_DESCRIPTION = eINSTANCE.getInterfaceDescription();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute INTERFACE_DESCRIPTION__NAME = eINSTANCE.getInterfaceDescription_Name();

		/**
		 * The meta object literal for the '<em><b>Ipaddress</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INTERFACE_DESCRIPTION__IPADDRESS = eINSTANCE.getInterfaceDescription_Ipaddress();

		/**
		 * The meta object literal for the '<em><b>Port</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INTERFACE_DESCRIPTION__PORT = eINSTANCE.getInterfaceDescription_Port();

		/**
		 * The meta object literal for the '<em><b>Data Points</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INTERFACE_DESCRIPTION__DATA_POINTS = eINSTANCE.getInterfaceDescription_DataPoints();

		/**
		 * The meta object literal for the '<em><b>Alarms</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INTERFACE_DESCRIPTION__ALARMS = eINSTANCE.getInterfaceDescription_Alarms();

		/**
		 * The meta object literal for the '<em><b>Commands</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INTERFACE_DESCRIPTION__COMMANDS = eINSTANCE.getInterfaceDescription_Commands();

		/**
		 * The meta object literal for the '<em><b>Events</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INTERFACE_DESCRIPTION__EVENTS = eINSTANCE.getInterfaceDescription_Events();

		/**
		 * The meta object literal for the '<em><b>Subscribed Events</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INTERFACE_DESCRIPTION__SUBSCRIBED_EVENTS = eINSTANCE.getInterfaceDescription_SubscribedEvents();

		/**
		 * The meta object literal for the '<em><b>Responses</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INTERFACE_DESCRIPTION__RESPONSES = eINSTANCE.getInterfaceDescription_Responses();

		/**
		 * The meta object literal for the '<em><b>Operating States</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INTERFACE_DESCRIPTION__OPERATING_STATES = eINSTANCE.getInterfaceDescription_OperatingStates();

		/**
		 * The meta object literal for the '<em><b>Subscribed Items</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INTERFACE_DESCRIPTION__SUBSCRIBED_ITEMS = eINSTANCE.getInterfaceDescription_SubscribedItems();

		/**
		 * The meta object literal for the '<em><b>Uses</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INTERFACE_DESCRIPTION__USES = eINSTANCE.getInterfaceDescription_Uses();

		/**
		 * The meta object literal for the '{@link mncModel.impl.AbstractInterfaceItemsImpl <em>Abstract Interface Items</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mncModel.impl.AbstractInterfaceItemsImpl
		 * @see mncModel.impl.MncModelPackageImpl#getAbstractInterfaceItems()
		 * @generated
		 */
		EClass ABSTRACT_INTERFACE_ITEMS = eINSTANCE.getAbstractInterfaceItems();

		/**
		 * The meta object literal for the '{@link mncModel.impl.AbstractOperationableItemsImpl <em>Abstract Operationable Items</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mncModel.impl.AbstractOperationableItemsImpl
		 * @see mncModel.impl.MncModelPackageImpl#getAbstractOperationableItems()
		 * @generated
		 */
		EClass ABSTRACT_OPERATIONABLE_ITEMS = eINSTANCE.getAbstractOperationableItems();

		/**
		 * The meta object literal for the '{@link mncModel.impl.OperatingStateImpl <em>Operating State</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mncModel.impl.OperatingStateImpl
		 * @see mncModel.impl.MncModelPackageImpl#getOperatingState()
		 * @generated
		 */
		EClass OPERATING_STATE = eINSTANCE.getOperatingState();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OPERATING_STATE__NAME = eINSTANCE.getOperatingState_Name();

		/**
		 * The meta object literal for the '<em><b>Parameters</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OPERATING_STATE__PARAMETERS = eINSTANCE.getOperatingState_Parameters();

		/**
		 * The meta object literal for the '{@link mncModel.impl.CommandResponseBlockImpl <em>Command Response Block</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mncModel.impl.CommandResponseBlockImpl
		 * @see mncModel.impl.MncModelPackageImpl#getCommandResponseBlock()
		 * @generated
		 */
		EClass COMMAND_RESPONSE_BLOCK = eINSTANCE.getCommandResponseBlock();

		/**
		 * The meta object literal for the '<em><b>Command</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMMAND_RESPONSE_BLOCK__COMMAND = eINSTANCE.getCommandResponseBlock_Command();

		/**
		 * The meta object literal for the '<em><b>Command Validation</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMMAND_RESPONSE_BLOCK__COMMAND_VALIDATION = eINSTANCE.getCommandResponseBlock_CommandValidation();

		/**
		 * The meta object literal for the '<em><b>Transition</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMMAND_RESPONSE_BLOCK__TRANSITION = eINSTANCE.getCommandResponseBlock_Transition();

		/**
		 * The meta object literal for the '<em><b>Command Trigger Condition</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMMAND_RESPONSE_BLOCK__COMMAND_TRIGGER_CONDITION = eINSTANCE.getCommandResponseBlock_CommandTriggerCondition();

		/**
		 * The meta object literal for the '<em><b>Command Translation</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMMAND_RESPONSE_BLOCK__COMMAND_TRANSLATION = eINSTANCE.getCommandResponseBlock_CommandTranslation();

		/**
		 * The meta object literal for the '<em><b>Command Distributions</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMMAND_RESPONSE_BLOCK__COMMAND_DISTRIBUTIONS = eINSTANCE.getCommandResponseBlock_CommandDistributions();

		/**
		 * The meta object literal for the '<em><b>Response Block</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMMAND_RESPONSE_BLOCK__RESPONSE_BLOCK = eINSTANCE.getCommandResponseBlock_ResponseBlock();

		/**
		 * The meta object literal for the '{@link mncModel.impl.CommandImpl <em>Command</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mncModel.impl.CommandImpl
		 * @see mncModel.impl.MncModelPackageImpl#getCommand()
		 * @generated
		 */
		EClass COMMAND = eINSTANCE.getCommand();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMMAND__NAME = eINSTANCE.getCommand_Name();

		/**
		 * The meta object literal for the '<em><b>Parameters</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMMAND__PARAMETERS = eINSTANCE.getCommand_Parameters();

		/**
		 * The meta object literal for the '{@link mncModel.impl.CommandValidationImpl <em>Command Validation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mncModel.impl.CommandValidationImpl
		 * @see mncModel.impl.MncModelPackageImpl#getCommandValidation()
		 * @generated
		 */
		EClass COMMAND_VALIDATION = eINSTANCE.getCommandValidation();

		/**
		 * The meta object literal for the '<em><b>Validation Rules</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMMAND_VALIDATION__VALIDATION_RULES = eINSTANCE.getCommandValidation_ValidationRules();

		/**
		 * The meta object literal for the '<em><b>Operation</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMMAND_VALIDATION__OPERATION = eINSTANCE.getCommandValidation_Operation();

		/**
		 * The meta object literal for the '{@link mncModel.impl.CommandDistributionImpl <em>Command Distribution</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mncModel.impl.CommandDistributionImpl
		 * @see mncModel.impl.MncModelPackageImpl#getCommandDistribution()
		 * @generated
		 */
		EClass COMMAND_DISTRIBUTION = eINSTANCE.getCommandDistribution();

		/**
		 * The meta object literal for the '<em><b>Command</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMMAND_DISTRIBUTION__COMMAND = eINSTANCE.getCommandDistribution_Command();

		/**
		 * The meta object literal for the '<em><b>Destination Nodes</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMMAND_DISTRIBUTION__DESTINATION_NODES = eINSTANCE.getCommandDistribution_DestinationNodes();

		/**
		 * The meta object literal for the '{@link mncModel.impl.CommandTranslationImpl <em>Command Translation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mncModel.impl.CommandTranslationImpl
		 * @see mncModel.impl.MncModelPackageImpl#getCommandTranslation()
		 * @generated
		 */
		EClass COMMAND_TRANSLATION = eINSTANCE.getCommandTranslation();

		/**
		 * The meta object literal for the '<em><b>Input Command</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMMAND_TRANSLATION__INPUT_COMMAND = eINSTANCE.getCommandTranslation_InputCommand();

		/**
		 * The meta object literal for the '<em><b>Translated Commands</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMMAND_TRANSLATION__TRANSLATED_COMMANDS = eINSTANCE.getCommandTranslation_TranslatedCommands();

		/**
		 * The meta object literal for the '<em><b>Parameter Translations</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMMAND_TRANSLATION__PARAMETER_TRANSLATIONS = eINSTANCE.getCommandTranslation_ParameterTranslations();

		/**
		 * The meta object literal for the '<em><b>Operation</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMMAND_TRANSLATION__OPERATION = eINSTANCE.getCommandTranslation_Operation();

		/**
		 * The meta object literal for the '{@link mncModel.impl.CommandTriggerConditionImpl <em>Command Trigger Condition</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mncModel.impl.CommandTriggerConditionImpl
		 * @see mncModel.impl.MncModelPackageImpl#getCommandTriggerCondition()
		 * @generated
		 */
		EClass COMMAND_TRIGGER_CONDITION = eINSTANCE.getCommandTriggerCondition();

		/**
		 * The meta object literal for the '<em><b>Responsible Items</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMMAND_TRIGGER_CONDITION__RESPONSIBLE_ITEMS = eINSTANCE.getCommandTriggerCondition_ResponsibleItems();

		/**
		 * The meta object literal for the '<em><b>Operation</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMMAND_TRIGGER_CONDITION__OPERATION = eINSTANCE.getCommandTriggerCondition_Operation();

		/**
		 * The meta object literal for the '{@link mncModel.impl.ParameterTranslationImpl <em>Parameter Translation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mncModel.impl.ParameterTranslationImpl
		 * @see mncModel.impl.MncModelPackageImpl#getParameterTranslation()
		 * @generated
		 */
		EClass PARAMETER_TRANSLATION = eINSTANCE.getParameterTranslation();

		/**
		 * The meta object literal for the '<em><b>Input Parameters</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PARAMETER_TRANSLATION__INPUT_PARAMETERS = eINSTANCE.getParameterTranslation_InputParameters();

		/**
		 * The meta object literal for the '<em><b>Translated Parameters</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PARAMETER_TRANSLATION__TRANSLATED_PARAMETERS = eINSTANCE.getParameterTranslation_TranslatedParameters();

		/**
		 * The meta object literal for the '{@link mncModel.impl.TransitionImpl <em>Transition</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mncModel.impl.TransitionImpl
		 * @see mncModel.impl.MncModelPackageImpl#getTransition()
		 * @generated
		 */
		EClass TRANSITION = eINSTANCE.getTransition();

		/**
		 * The meta object literal for the '<em><b>Current State</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TRANSITION__CURRENT_STATE = eINSTANCE.getTransition_CurrentState();

		/**
		 * The meta object literal for the '<em><b>Next State</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TRANSITION__NEXT_STATE = eINSTANCE.getTransition_NextState();

		/**
		 * The meta object literal for the '<em><b>Entry Action</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TRANSITION__ENTRY_ACTION = eINSTANCE.getTransition_EntryAction();

		/**
		 * The meta object literal for the '<em><b>Exit Action</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TRANSITION__EXIT_ACTION = eINSTANCE.getTransition_ExitAction();

		/**
		 * The meta object literal for the '{@link mncModel.impl.ResponseImpl <em>Response</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mncModel.impl.ResponseImpl
		 * @see mncModel.impl.MncModelPackageImpl#getResponse()
		 * @generated
		 */
		EClass RESPONSE = eINSTANCE.getResponse();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RESPONSE__NAME = eINSTANCE.getResponse_Name();

		/**
		 * The meta object literal for the '<em><b>Parameters</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RESPONSE__PARAMETERS = eINSTANCE.getResponse_Parameters();

		/**
		 * The meta object literal for the '<em><b>Publish</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RESPONSE__PUBLISH = eINSTANCE.getResponse_Publish();

		/**
		 * The meta object literal for the '{@link mncModel.impl.ResponseBlockImpl <em>Response Block</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mncModel.impl.ResponseBlockImpl
		 * @see mncModel.impl.MncModelPackageImpl#getResponseBlock()
		 * @generated
		 */
		EClass RESPONSE_BLOCK = eINSTANCE.getResponseBlock();

		/**
		 * The meta object literal for the '<em><b>Response</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RESPONSE_BLOCK__RESPONSE = eINSTANCE.getResponseBlock_Response();

		/**
		 * The meta object literal for the '<em><b>Response Validation</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RESPONSE_BLOCK__RESPONSE_VALIDATION = eINSTANCE.getResponseBlock_ResponseValidation();

		/**
		 * The meta object literal for the '<em><b>Response Translation</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RESPONSE_BLOCK__RESPONSE_TRANSLATION = eINSTANCE.getResponseBlock_ResponseTranslation();

		/**
		 * The meta object literal for the '<em><b>Destination Nodes</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RESPONSE_BLOCK__DESTINATION_NODES = eINSTANCE.getResponseBlock_DestinationNodes();

		/**
		 * The meta object literal for the '{@link mncModel.impl.ResponseValidationImpl <em>Response Validation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mncModel.impl.ResponseValidationImpl
		 * @see mncModel.impl.MncModelPackageImpl#getResponseValidation()
		 * @generated
		 */
		EClass RESPONSE_VALIDATION = eINSTANCE.getResponseValidation();

		/**
		 * The meta object literal for the '<em><b>Response</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RESPONSE_VALIDATION__RESPONSE = eINSTANCE.getResponseValidation_Response();

		/**
		 * The meta object literal for the '<em><b>Validation Rules</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RESPONSE_VALIDATION__VALIDATION_RULES = eINSTANCE.getResponseValidation_ValidationRules();

		/**
		 * The meta object literal for the '{@link mncModel.impl.ResponseDistributionImpl <em>Response Distribution</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mncModel.impl.ResponseDistributionImpl
		 * @see mncModel.impl.MncModelPackageImpl#getResponseDistribution()
		 * @generated
		 */
		EClass RESPONSE_DISTRIBUTION = eINSTANCE.getResponseDistribution();

		/**
		 * The meta object literal for the '<em><b>Response</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RESPONSE_DISTRIBUTION__RESPONSE = eINSTANCE.getResponseDistribution_Response();

		/**
		 * The meta object literal for the '<em><b>Destination Nodes</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RESPONSE_DISTRIBUTION__DESTINATION_NODES = eINSTANCE.getResponseDistribution_DestinationNodes();

		/**
		 * The meta object literal for the '{@link mncModel.impl.ResponseTranslationImpl <em>Response Translation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mncModel.impl.ResponseTranslationImpl
		 * @see mncModel.impl.MncModelPackageImpl#getResponseTranslation()
		 * @generated
		 */
		EClass RESPONSE_TRANSLATION = eINSTANCE.getResponseTranslation();

		/**
		 * The meta object literal for the '<em><b>Response Translation Rules</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RESPONSE_TRANSLATION__RESPONSE_TRANSLATION_RULES = eINSTANCE.getResponseTranslation_ResponseTranslationRules();

		/**
		 * The meta object literal for the '{@link mncModel.impl.ResponseTranslationRuleImpl <em>Response Translation Rule</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mncModel.impl.ResponseTranslationRuleImpl
		 * @see mncModel.impl.MncModelPackageImpl#getResponseTranslationRule()
		 * @generated
		 */
		EClass RESPONSE_TRANSLATION_RULE = eINSTANCE.getResponseTranslationRule();

		/**
		 * The meta object literal for the '<em><b>Input Responses</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RESPONSE_TRANSLATION_RULE__INPUT_RESPONSES = eINSTANCE.getResponseTranslationRule_InputResponses();

		/**
		 * The meta object literal for the '<em><b>Translated Response</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RESPONSE_TRANSLATION_RULE__TRANSLATED_RESPONSE = eINSTANCE.getResponseTranslationRule_TranslatedResponse();

		/**
		 * The meta object literal for the '<em><b>Parameter Translations</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RESPONSE_TRANSLATION_RULE__PARAMETER_TRANSLATIONS = eINSTANCE.getResponseTranslationRule_ParameterTranslations();

		/**
		 * The meta object literal for the '<em><b>Operation</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RESPONSE_TRANSLATION_RULE__OPERATION = eINSTANCE.getResponseTranslationRule_Operation();

		/**
		 * The meta object literal for the '{@link mncModel.impl.PortImpl <em>Port</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mncModel.impl.PortImpl
		 * @see mncModel.impl.MncModelPackageImpl#getPort()
		 * @generated
		 */
		EClass PORT = eINSTANCE.getPort();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PORT__NAME = eINSTANCE.getPort_Name();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PORT__VALUE = eINSTANCE.getPort_Value();

		/**
		 * The meta object literal for the '{@link mncModel.impl.EventBlockImpl <em>Event Block</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mncModel.impl.EventBlockImpl
		 * @see mncModel.impl.MncModelPackageImpl#getEventBlock()
		 * @generated
		 */
		EClass EVENT_BLOCK = eINSTANCE.getEventBlock();

		/**
		 * The meta object literal for the '<em><b>Event</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EVENT_BLOCK__EVENT = eINSTANCE.getEventBlock_Event();

		/**
		 * The meta object literal for the '<em><b>Event Condition</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EVENT_BLOCK__EVENT_CONDITION = eINSTANCE.getEventBlock_EventCondition();

		/**
		 * The meta object literal for the '<em><b>Event Handling</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EVENT_BLOCK__EVENT_HANDLING = eINSTANCE.getEventBlock_EventHandling();

		/**
		 * The meta object literal for the '{@link mncModel.impl.EventImpl <em>Event</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mncModel.impl.EventImpl
		 * @see mncModel.impl.MncModelPackageImpl#getEvent()
		 * @generated
		 */
		EClass EVENT = eINSTANCE.getEvent();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EVENT__NAME = eINSTANCE.getEvent_Name();

		/**
		 * The meta object literal for the '<em><b>Publish</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EVENT__PUBLISH = eINSTANCE.getEvent_Publish();

		/**
		 * The meta object literal for the '<em><b>Parameters</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EVENT__PARAMETERS = eINSTANCE.getEvent_Parameters();

		/**
		 * The meta object literal for the '{@link mncModel.impl.EventTriggerConditionImpl <em>Event Trigger Condition</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mncModel.impl.EventTriggerConditionImpl
		 * @see mncModel.impl.MncModelPackageImpl#getEventTriggerCondition()
		 * @generated
		 */
		EClass EVENT_TRIGGER_CONDITION = eINSTANCE.getEventTriggerCondition();

		/**
		 * The meta object literal for the '<em><b>Event Translation</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EVENT_TRIGGER_CONDITION__EVENT_TRANSLATION = eINSTANCE.getEventTriggerCondition_EventTranslation();

		/**
		 * The meta object literal for the '<em><b>Operation</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EVENT_TRIGGER_CONDITION__OPERATION = eINSTANCE.getEventTriggerCondition_Operation();

		/**
		 * The meta object literal for the '<em><b>Responsible Items</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EVENT_TRIGGER_CONDITION__RESPONSIBLE_ITEMS = eINSTANCE.getEventTriggerCondition_ResponsibleItems();

		/**
		 * The meta object literal for the '{@link mncModel.impl.EventTranslationImpl <em>Event Translation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mncModel.impl.EventTranslationImpl
		 * @see mncModel.impl.MncModelPackageImpl#getEventTranslation()
		 * @generated
		 */
		EClass EVENT_TRANSLATION = eINSTANCE.getEventTranslation();

		/**
		 * The meta object literal for the '<em><b>Event Translation Rules</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EVENT_TRANSLATION__EVENT_TRANSLATION_RULES = eINSTANCE.getEventTranslation_EventTranslationRules();

		/**
		 * The meta object literal for the '{@link mncModel.impl.EventTranslationRuleImpl <em>Event Translation Rule</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mncModel.impl.EventTranslationRuleImpl
		 * @see mncModel.impl.MncModelPackageImpl#getEventTranslationRule()
		 * @generated
		 */
		EClass EVENT_TRANSLATION_RULE = eINSTANCE.getEventTranslationRule();

		/**
		 * The meta object literal for the '<em><b>Events</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EVENT_TRANSLATION_RULE__EVENTS = eINSTANCE.getEventTranslationRule_Events();

		/**
		 * The meta object literal for the '<em><b>Translated Event</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EVENT_TRANSLATION_RULE__TRANSLATED_EVENT = eINSTANCE.getEventTranslationRule_TranslatedEvent();

		/**
		 * The meta object literal for the '<em><b>Operation</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EVENT_TRANSLATION_RULE__OPERATION = eINSTANCE.getEventTranslationRule_Operation();

		/**
		 * The meta object literal for the '{@link mncModel.impl.EventHandlingImpl <em>Event Handling</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mncModel.impl.EventHandlingImpl
		 * @see mncModel.impl.MncModelPackageImpl#getEventHandling()
		 * @generated
		 */
		EClass EVENT_HANDLING = eINSTANCE.getEventHandling();

		/**
		 * The meta object literal for the '<em><b>Trigger Action</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EVENT_HANDLING__TRIGGER_ACTION = eINSTANCE.getEventHandling_TriggerAction();

		/**
		 * The meta object literal for the '{@link mncModel.impl.AlarmBlockImpl <em>Alarm Block</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mncModel.impl.AlarmBlockImpl
		 * @see mncModel.impl.MncModelPackageImpl#getAlarmBlock()
		 * @generated
		 */
		EClass ALARM_BLOCK = eINSTANCE.getAlarmBlock();

		/**
		 * The meta object literal for the '<em><b>Alarm</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ALARM_BLOCK__ALARM = eINSTANCE.getAlarmBlock_Alarm();

		/**
		 * The meta object literal for the '<em><b>Alarm Condition</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ALARM_BLOCK__ALARM_CONDITION = eINSTANCE.getAlarmBlock_AlarmCondition();

		/**
		 * The meta object literal for the '<em><b>Alarm Handling</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ALARM_BLOCK__ALARM_HANDLING = eINSTANCE.getAlarmBlock_AlarmHandling();

		/**
		 * The meta object literal for the '{@link mncModel.impl.AlarmImpl <em>Alarm</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mncModel.impl.AlarmImpl
		 * @see mncModel.impl.MncModelPackageImpl#getAlarm()
		 * @generated
		 */
		EClass ALARM = eINSTANCE.getAlarm();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ALARM__NAME = eINSTANCE.getAlarm_Name();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ALARM__TYPE = eINSTANCE.getAlarm_Type();

		/**
		 * The meta object literal for the '<em><b>Level</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ALARM__LEVEL = eINSTANCE.getAlarm_Level();

		/**
		 * The meta object literal for the '<em><b>Publish</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ALARM__PUBLISH = eINSTANCE.getAlarm_Publish();

		/**
		 * The meta object literal for the '<em><b>Parameters</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ALARM__PARAMETERS = eINSTANCE.getAlarm_Parameters();

		/**
		 * The meta object literal for the '{@link mncModel.impl.AlarmTriggerConditionImpl <em>Alarm Trigger Condition</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mncModel.impl.AlarmTriggerConditionImpl
		 * @see mncModel.impl.MncModelPackageImpl#getAlarmTriggerCondition()
		 * @generated
		 */
		EClass ALARM_TRIGGER_CONDITION = eINSTANCE.getAlarmTriggerCondition();

		/**
		 * The meta object literal for the '<em><b>Alarm Translation</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ALARM_TRIGGER_CONDITION__ALARM_TRANSLATION = eINSTANCE.getAlarmTriggerCondition_AlarmTranslation();

		/**
		 * The meta object literal for the '<em><b>Operation</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ALARM_TRIGGER_CONDITION__OPERATION = eINSTANCE.getAlarmTriggerCondition_Operation();

		/**
		 * The meta object literal for the '<em><b>Responsible Items</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ALARM_TRIGGER_CONDITION__RESPONSIBLE_ITEMS = eINSTANCE.getAlarmTriggerCondition_ResponsibleItems();

		/**
		 * The meta object literal for the '{@link mncModel.impl.AlarmTranslationImpl <em>Alarm Translation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mncModel.impl.AlarmTranslationImpl
		 * @see mncModel.impl.MncModelPackageImpl#getAlarmTranslation()
		 * @generated
		 */
		EClass ALARM_TRANSLATION = eINSTANCE.getAlarmTranslation();

		/**
		 * The meta object literal for the '<em><b>Alarm Translation Rules</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ALARM_TRANSLATION__ALARM_TRANSLATION_RULES = eINSTANCE.getAlarmTranslation_AlarmTranslationRules();

		/**
		 * The meta object literal for the '{@link mncModel.impl.AlarmTranslationRuleImpl <em>Alarm Translation Rule</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mncModel.impl.AlarmTranslationRuleImpl
		 * @see mncModel.impl.MncModelPackageImpl#getAlarmTranslationRule()
		 * @generated
		 */
		EClass ALARM_TRANSLATION_RULE = eINSTANCE.getAlarmTranslationRule();

		/**
		 * The meta object literal for the '<em><b>Operation</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ALARM_TRANSLATION_RULE__OPERATION = eINSTANCE.getAlarmTranslationRule_Operation();

		/**
		 * The meta object literal for the '{@link mncModel.impl.AlarmHandlingImpl <em>Alarm Handling</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mncModel.impl.AlarmHandlingImpl
		 * @see mncModel.impl.MncModelPackageImpl#getAlarmHandling()
		 * @generated
		 */
		EClass ALARM_HANDLING = eINSTANCE.getAlarmHandling();

		/**
		 * The meta object literal for the '<em><b>Trigger Action</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ALARM_HANDLING__TRIGGER_ACTION = eINSTANCE.getAlarmHandling_TriggerAction();

		/**
		 * The meta object literal for the '{@link mncModel.impl.DataPointBlockImpl <em>Data Point Block</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mncModel.impl.DataPointBlockImpl
		 * @see mncModel.impl.MncModelPackageImpl#getDataPointBlock()
		 * @generated
		 */
		EClass DATA_POINT_BLOCK = eINSTANCE.getDataPointBlock();

		/**
		 * The meta object literal for the '<em><b>Data Point</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DATA_POINT_BLOCK__DATA_POINT = eINSTANCE.getDataPointBlock_DataPoint();

		/**
		 * The meta object literal for the '<em><b>Data Point Condition</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DATA_POINT_BLOCK__DATA_POINT_CONDITION = eINSTANCE.getDataPointBlock_DataPointCondition();

		/**
		 * The meta object literal for the '<em><b>Data Point Handling</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DATA_POINT_BLOCK__DATA_POINT_HANDLING = eINSTANCE.getDataPointBlock_DataPointHandling();

		/**
		 * The meta object literal for the '{@link mncModel.impl.DataPointImpl <em>Data Point</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mncModel.impl.DataPointImpl
		 * @see mncModel.impl.MncModelPackageImpl#getDataPoint()
		 * @generated
		 */
		EClass DATA_POINT = eINSTANCE.getDataPoint();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DATA_POINT__NAME = eINSTANCE.getDataPoint_Name();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DATA_POINT__TYPE = eINSTANCE.getDataPoint_Type();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DATA_POINT__VALUE = eINSTANCE.getDataPoint_Value();

		/**
		 * The meta object literal for the '<em><b>Publish</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DATA_POINT__PUBLISH = eINSTANCE.getDataPoint_Publish();

		/**
		 * The meta object literal for the '<em><b>Parameters</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DATA_POINT__PARAMETERS = eINSTANCE.getDataPoint_Parameters();

		/**
		 * The meta object literal for the '{@link mncModel.impl.DataPointTriggerConditionImpl <em>Data Point Trigger Condition</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mncModel.impl.DataPointTriggerConditionImpl
		 * @see mncModel.impl.MncModelPackageImpl#getDataPointTriggerCondition()
		 * @generated
		 */
		EClass DATA_POINT_TRIGGER_CONDITION = eINSTANCE.getDataPointTriggerCondition();

		/**
		 * The meta object literal for the '<em><b>Data Point Translation</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DATA_POINT_TRIGGER_CONDITION__DATA_POINT_TRANSLATION = eINSTANCE.getDataPointTriggerCondition_DataPointTranslation();

		/**
		 * The meta object literal for the '<em><b>Responsible Items</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DATA_POINT_TRIGGER_CONDITION__RESPONSIBLE_ITEMS = eINSTANCE.getDataPointTriggerCondition_ResponsibleItems();

		/**
		 * The meta object literal for the '<em><b>Operation</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DATA_POINT_TRIGGER_CONDITION__OPERATION = eINSTANCE.getDataPointTriggerCondition_Operation();

		/**
		 * The meta object literal for the '{@link mncModel.impl.DataPointTranslationImpl <em>Data Point Translation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mncModel.impl.DataPointTranslationImpl
		 * @see mncModel.impl.MncModelPackageImpl#getDataPointTranslation()
		 * @generated
		 */
		EClass DATA_POINT_TRANSLATION = eINSTANCE.getDataPointTranslation();

		/**
		 * The meta object literal for the '<em><b>Data Point Translation Rules</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DATA_POINT_TRANSLATION__DATA_POINT_TRANSLATION_RULES = eINSTANCE.getDataPointTranslation_DataPointTranslationRules();

		/**
		 * The meta object literal for the '{@link mncModel.impl.DataPointTranslationRuleImpl <em>Data Point Translation Rule</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mncModel.impl.DataPointTranslationRuleImpl
		 * @see mncModel.impl.MncModelPackageImpl#getDataPointTranslationRule()
		 * @generated
		 */
		EClass DATA_POINT_TRANSLATION_RULE = eINSTANCE.getDataPointTranslationRule();

		/**
		 * The meta object literal for the '<em><b>Data Points</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DATA_POINT_TRANSLATION_RULE__DATA_POINTS = eINSTANCE.getDataPointTranslationRule_DataPoints();

		/**
		 * The meta object literal for the '<em><b>Operation</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DATA_POINT_TRANSLATION_RULE__OPERATION = eINSTANCE.getDataPointTranslationRule_Operation();

		/**
		 * The meta object literal for the '{@link mncModel.impl.DataPointHandlingImpl <em>Data Point Handling</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mncModel.impl.DataPointHandlingImpl
		 * @see mncModel.impl.MncModelPackageImpl#getDataPointHandling()
		 * @generated
		 */
		EClass DATA_POINT_HANDLING = eINSTANCE.getDataPointHandling();

		/**
		 * The meta object literal for the '<em><b>Check Data Point</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DATA_POINT_HANDLING__CHECK_DATA_POINT = eINSTANCE.getDataPointHandling_CheckDataPoint();

		/**
		 * The meta object literal for the '<em><b>Action</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DATA_POINT_HANDLING__ACTION = eINSTANCE.getDataPointHandling_Action();

		/**
		 * The meta object literal for the '{@link mncModel.impl.DataPointValidConditionImpl <em>Data Point Valid Condition</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mncModel.impl.DataPointValidConditionImpl
		 * @see mncModel.impl.MncModelPackageImpl#getDataPointValidCondition()
		 * @generated
		 */
		EClass DATA_POINT_VALID_CONDITION = eINSTANCE.getDataPointValidCondition();

		/**
		 * The meta object literal for the '<em><b>Data Point</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DATA_POINT_VALID_CONDITION__DATA_POINT = eINSTANCE.getDataPointValidCondition_DataPoint();

		/**
		 * The meta object literal for the '<em><b>Check Values</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DATA_POINT_VALID_CONDITION__CHECK_VALUES = eINSTANCE.getDataPointValidCondition_CheckValues();

		/**
		 * The meta object literal for the '<em><b>Check Max Value</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DATA_POINT_VALID_CONDITION__CHECK_MAX_VALUE = eINSTANCE.getDataPointValidCondition_CheckMaxValue();

		/**
		 * The meta object literal for the '<em><b>Check Min Value</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DATA_POINT_VALID_CONDITION__CHECK_MIN_VALUE = eINSTANCE.getDataPointValidCondition_CheckMinValue();

		/**
		 * The meta object literal for the '{@link mncModel.impl.ParameterImpl <em>Parameter</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mncModel.impl.ParameterImpl
		 * @see mncModel.impl.MncModelPackageImpl#getParameter()
		 * @generated
		 */
		EClass PARAMETER = eINSTANCE.getParameter();

		/**
		 * The meta object literal for the '{@link mncModel.impl.CheckParameterConditionImpl <em>Check Parameter Condition</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mncModel.impl.CheckParameterConditionImpl
		 * @see mncModel.impl.MncModelPackageImpl#getCheckParameterCondition()
		 * @generated
		 */
		EClass CHECK_PARAMETER_CONDITION = eINSTANCE.getCheckParameterCondition();

		/**
		 * The meta object literal for the '<em><b>Parameter</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CHECK_PARAMETER_CONDITION__PARAMETER = eINSTANCE.getCheckParameterCondition_Parameter();

		/**
		 * The meta object literal for the '<em><b>Check Values</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CHECK_PARAMETER_CONDITION__CHECK_VALUES = eINSTANCE.getCheckParameterCondition_CheckValues();

		/**
		 * The meta object literal for the '<em><b>Check Max Value</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CHECK_PARAMETER_CONDITION__CHECK_MAX_VALUE = eINSTANCE.getCheckParameterCondition_CheckMaxValue();

		/**
		 * The meta object literal for the '<em><b>Check Min Value</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CHECK_PARAMETER_CONDITION__CHECK_MIN_VALUE = eINSTANCE.getCheckParameterCondition_CheckMinValue();

		/**
		 * The meta object literal for the '{@link mncModel.impl.ArrayTypeImpl <em>Array Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mncModel.impl.ArrayTypeImpl
		 * @see mncModel.impl.MncModelPackageImpl#getArrayType()
		 * @generated
		 */
		EClass ARRAY_TYPE = eINSTANCE.getArrayType();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ARRAY_TYPE__TYPE = eINSTANCE.getArrayType_Type();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ARRAY_TYPE__NAME = eINSTANCE.getArrayType_Name();

		/**
		 * The meta object literal for the '<em><b>Values</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ARRAY_TYPE__VALUES = eINSTANCE.getArrayType_Values();

		/**
		 * The meta object literal for the '{@link mncModel.impl.SimpleTypeImpl <em>Simple Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mncModel.impl.SimpleTypeImpl
		 * @see mncModel.impl.MncModelPackageImpl#getSimpleType()
		 * @generated
		 */
		EClass SIMPLE_TYPE = eINSTANCE.getSimpleType();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SIMPLE_TYPE__NAME = eINSTANCE.getSimpleType_Name();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SIMPLE_TYPE__TYPE = eINSTANCE.getSimpleType_Type();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SIMPLE_TYPE__VALUE = eINSTANCE.getSimpleType_Value();

		/**
		 * The meta object literal for the '{@link mncModel.impl.PrimitiveValueImpl <em>Primitive Value</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mncModel.impl.PrimitiveValueImpl
		 * @see mncModel.impl.MncModelPackageImpl#getPrimitiveValue()
		 * @generated
		 */
		EClass PRIMITIVE_VALUE = eINSTANCE.getPrimitiveValue();

		/**
		 * The meta object literal for the '{@link mncModel.impl.IntValueImpl <em>Int Value</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mncModel.impl.IntValueImpl
		 * @see mncModel.impl.MncModelPackageImpl#getIntValue()
		 * @generated
		 */
		EClass INT_VALUE = eINSTANCE.getIntValue();

		/**
		 * The meta object literal for the '<em><b>Int Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute INT_VALUE__INT_VALUE = eINSTANCE.getIntValue_IntValue();

		/**
		 * The meta object literal for the '{@link mncModel.impl.BoolValueImpl <em>Bool Value</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mncModel.impl.BoolValueImpl
		 * @see mncModel.impl.MncModelPackageImpl#getBoolValue()
		 * @generated
		 */
		EClass BOOL_VALUE = eINSTANCE.getBoolValue();

		/**
		 * The meta object literal for the '<em><b>Bool Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BOOL_VALUE__BOOL_VALUE = eINSTANCE.getBoolValue_BoolValue();

		/**
		 * The meta object literal for the '{@link mncModel.impl.FloatValueImpl <em>Float Value</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mncModel.impl.FloatValueImpl
		 * @see mncModel.impl.MncModelPackageImpl#getFloatValue()
		 * @generated
		 */
		EClass FLOAT_VALUE = eINSTANCE.getFloatValue();

		/**
		 * The meta object literal for the '<em><b>Float Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FLOAT_VALUE__FLOAT_VALUE = eINSTANCE.getFloatValue_FloatValue();

		/**
		 * The meta object literal for the '{@link mncModel.impl.StringValueImpl <em>String Value</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mncModel.impl.StringValueImpl
		 * @see mncModel.impl.MncModelPackageImpl#getStringValue()
		 * @generated
		 */
		EClass STRING_VALUE = eINSTANCE.getStringValue();

		/**
		 * The meta object literal for the '<em><b>String Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute STRING_VALUE__STRING_VALUE = eINSTANCE.getStringValue_StringValue();

		/**
		 * The meta object literal for the '{@link mncModel.impl.OperationImpl <em>Operation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mncModel.impl.OperationImpl
		 * @see mncModel.impl.MncModelPackageImpl#getOperation()
		 * @generated
		 */
		EClass OPERATION = eINSTANCE.getOperation();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OPERATION__NAME = eINSTANCE.getOperation_Name();

		/**
		 * The meta object literal for the '<em><b>Input Parameters</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OPERATION__INPUT_PARAMETERS = eINSTANCE.getOperation_InputParameters();

		/**
		 * The meta object literal for the '<em><b>Output Parameters</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OPERATION__OUTPUT_PARAMETERS = eINSTANCE.getOperation_OutputParameters();

		/**
		 * The meta object literal for the '<em><b>Script</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OPERATION__SCRIPT = eINSTANCE.getOperation_Script();

		/**
		 * The meta object literal for the '{@link mncModel.impl.AddressImpl <em>Address</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mncModel.impl.AddressImpl
		 * @see mncModel.impl.MncModelPackageImpl#getAddress()
		 * @generated
		 */
		EClass ADDRESS = eINSTANCE.getAddress();

		/**
		 * The meta object literal for the '<em><b>Ipaddress</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ADDRESS__IPADDRESS = eINSTANCE.getAddress_Ipaddress();

		/**
		 * The meta object literal for the '{@link mncModel.impl.ActionImpl <em>Action</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mncModel.impl.ActionImpl
		 * @see mncModel.impl.MncModelPackageImpl#getAction()
		 * @generated
		 */
		EClass ACTION = eINSTANCE.getAction();

		/**
		 * The meta object literal for the '<em><b>Command</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTION__COMMAND = eINSTANCE.getAction_Command();

		/**
		 * The meta object literal for the '<em><b>Event</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTION__EVENT = eINSTANCE.getAction_Event();

		/**
		 * The meta object literal for the '<em><b>Alarm</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTION__ALARM = eINSTANCE.getAction_Alarm();

		/**
		 * The meta object literal for the '<em><b>Operation</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTION__OPERATION = eINSTANCE.getAction_Operation();

		/**
		 * The meta object literal for the '{@link mncModel.impl.PublishImpl <em>Publish</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mncModel.impl.PublishImpl
		 * @see mncModel.impl.MncModelPackageImpl#getPublish()
		 * @generated
		 */
		EClass PUBLISH = eINSTANCE.getPublish();

		/**
		 * The meta object literal for the '{@link mncModel.impl.SubscribableItemsImpl <em>Subscribable Items</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mncModel.impl.SubscribableItemsImpl
		 * @see mncModel.impl.MncModelPackageImpl#getSubscribableItems()
		 * @generated
		 */
		EClass SUBSCRIBABLE_ITEMS = eINSTANCE.getSubscribableItems();

		/**
		 * The meta object literal for the '{@link mncModel.impl.SubscribableItemListImpl <em>Subscribable Item List</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mncModel.impl.SubscribableItemListImpl
		 * @see mncModel.impl.MncModelPackageImpl#getSubscribableItemList()
		 * @generated
		 */
		EClass SUBSCRIBABLE_ITEM_LIST = eINSTANCE.getSubscribableItemList();

		/**
		 * The meta object literal for the '<em><b>Subscribed Events</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SUBSCRIBABLE_ITEM_LIST__SUBSCRIBED_EVENTS = eINSTANCE.getSubscribableItemList_SubscribedEvents();

		/**
		 * The meta object literal for the '<em><b>Subscribed Alarms</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SUBSCRIBABLE_ITEM_LIST__SUBSCRIBED_ALARMS = eINSTANCE.getSubscribableItemList_SubscribedAlarms();

		/**
		 * The meta object literal for the '<em><b>Subscribed Data Points</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SUBSCRIBABLE_ITEM_LIST__SUBSCRIBED_DATA_POINTS = eINSTANCE.getSubscribableItemList_SubscribedDataPoints();

		/**
		 * The meta object literal for the '{@link mncModel.impl.ResponsibleItemListImpl <em>Responsible Item List</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mncModel.impl.ResponsibleItemListImpl
		 * @see mncModel.impl.MncModelPackageImpl#getResponsibleItemList()
		 * @generated
		 */
		EClass RESPONSIBLE_ITEM_LIST = eINSTANCE.getResponsibleItemList();

		/**
		 * The meta object literal for the '<em><b>Responsible Commands</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RESPONSIBLE_ITEM_LIST__RESPONSIBLE_COMMANDS = eINSTANCE.getResponsibleItemList_ResponsibleCommands();

		/**
		 * The meta object literal for the '<em><b>Responsible Events</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RESPONSIBLE_ITEM_LIST__RESPONSIBLE_EVENTS = eINSTANCE.getResponsibleItemList_ResponsibleEvents();

		/**
		 * The meta object literal for the '<em><b>Responsible Data Points</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RESPONSIBLE_ITEM_LIST__RESPONSIBLE_DATA_POINTS = eINSTANCE.getResponsibleItemList_ResponsibleDataPoints();

		/**
		 * The meta object literal for the '<em><b>Responsible Alarms</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RESPONSIBLE_ITEM_LIST__RESPONSIBLE_ALARMS = eINSTANCE.getResponsibleItemList_ResponsibleAlarms();

		/**
		 * The meta object literal for the '{@link mncModel.PrimitiveValueType <em>Primitive Value Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see mncModel.PrimitiveValueType
		 * @see mncModel.impl.MncModelPackageImpl#getPrimitiveValueType()
		 * @generated
		 */
		EEnum PRIMITIVE_VALUE_TYPE = eINSTANCE.getPrimitiveValueType();

	}

} //MncModelPackage
