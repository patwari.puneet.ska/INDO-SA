/**
 */
package mncModel.impl;

import java.util.Collection;

import mncModel.MncModelPackage;
import mncModel.ResponseTranslation;
import mncModel.ResponseTranslationRule;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Response Translation</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link mncModel.impl.ResponseTranslationImpl#getResponseTranslationRules <em>Response Translation Rules</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ResponseTranslationImpl extends MinimalEObjectImpl.Container implements ResponseTranslation {
	/**
	 * The cached value of the '{@link #getResponseTranslationRules() <em>Response Translation Rules</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getResponseTranslationRules()
	 * @generated
	 * @ordered
	 */
	protected EList<ResponseTranslationRule> responseTranslationRules;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ResponseTranslationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MncModelPackage.Literals.RESPONSE_TRANSLATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ResponseTranslationRule> getResponseTranslationRules() {
		if (responseTranslationRules == null) {
			responseTranslationRules = new EObjectContainmentEList<ResponseTranslationRule>(ResponseTranslationRule.class, this, MncModelPackage.RESPONSE_TRANSLATION__RESPONSE_TRANSLATION_RULES);
		}
		return responseTranslationRules;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MncModelPackage.RESPONSE_TRANSLATION__RESPONSE_TRANSLATION_RULES:
				return ((InternalEList<?>)getResponseTranslationRules()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MncModelPackage.RESPONSE_TRANSLATION__RESPONSE_TRANSLATION_RULES:
				return getResponseTranslationRules();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MncModelPackage.RESPONSE_TRANSLATION__RESPONSE_TRANSLATION_RULES:
				getResponseTranslationRules().clear();
				getResponseTranslationRules().addAll((Collection<? extends ResponseTranslationRule>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MncModelPackage.RESPONSE_TRANSLATION__RESPONSE_TRANSLATION_RULES:
				getResponseTranslationRules().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MncModelPackage.RESPONSE_TRANSLATION__RESPONSE_TRANSLATION_RULES:
				return responseTranslationRules != null && !responseTranslationRules.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //ResponseTranslationImpl
