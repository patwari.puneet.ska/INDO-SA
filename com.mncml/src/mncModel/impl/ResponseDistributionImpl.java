/**
 */
package mncModel.impl;

import java.util.Collection;

import mncModel.ControlNode;
import mncModel.MncModelPackage;
import mncModel.Response;
import mncModel.ResponseDistribution;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Response Distribution</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link mncModel.impl.ResponseDistributionImpl#getResponse <em>Response</em>}</li>
 *   <li>{@link mncModel.impl.ResponseDistributionImpl#getDestinationNodes <em>Destination Nodes</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ResponseDistributionImpl extends MinimalEObjectImpl.Container implements ResponseDistribution {
	/**
	 * The cached value of the '{@link #getResponse() <em>Response</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getResponse()
	 * @generated
	 * @ordered
	 */
	protected Response response;

	/**
	 * The cached value of the '{@link #getDestinationNodes() <em>Destination Nodes</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDestinationNodes()
	 * @generated
	 * @ordered
	 */
	protected EList<ControlNode> destinationNodes;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ResponseDistributionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MncModelPackage.Literals.RESPONSE_DISTRIBUTION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Response getResponse() {
		if (response != null && response.eIsProxy()) {
			InternalEObject oldResponse = (InternalEObject)response;
			response = (Response)eResolveProxy(oldResponse);
			if (response != oldResponse) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, MncModelPackage.RESPONSE_DISTRIBUTION__RESPONSE, oldResponse, response));
			}
		}
		return response;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Response basicGetResponse() {
		return response;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setResponse(Response newResponse) {
		Response oldResponse = response;
		response = newResponse;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MncModelPackage.RESPONSE_DISTRIBUTION__RESPONSE, oldResponse, response));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ControlNode> getDestinationNodes() {
		if (destinationNodes == null) {
			destinationNodes = new EObjectResolvingEList<ControlNode>(ControlNode.class, this, MncModelPackage.RESPONSE_DISTRIBUTION__DESTINATION_NODES);
		}
		return destinationNodes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MncModelPackage.RESPONSE_DISTRIBUTION__RESPONSE:
				if (resolve) return getResponse();
				return basicGetResponse();
			case MncModelPackage.RESPONSE_DISTRIBUTION__DESTINATION_NODES:
				return getDestinationNodes();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MncModelPackage.RESPONSE_DISTRIBUTION__RESPONSE:
				setResponse((Response)newValue);
				return;
			case MncModelPackage.RESPONSE_DISTRIBUTION__DESTINATION_NODES:
				getDestinationNodes().clear();
				getDestinationNodes().addAll((Collection<? extends ControlNode>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MncModelPackage.RESPONSE_DISTRIBUTION__RESPONSE:
				setResponse((Response)null);
				return;
			case MncModelPackage.RESPONSE_DISTRIBUTION__DESTINATION_NODES:
				getDestinationNodes().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MncModelPackage.RESPONSE_DISTRIBUTION__RESPONSE:
				return response != null;
			case MncModelPackage.RESPONSE_DISTRIBUTION__DESTINATION_NODES:
				return destinationNodes != null && !destinationNodes.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //ResponseDistributionImpl
