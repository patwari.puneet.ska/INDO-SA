/**
 */
package mncModel.impl;

import mncModel.Command;
import mncModel.CommandResponseBlock;
import mncModel.CommandTranslation;
import mncModel.CommandTriggerCondition;
import mncModel.CommandValidation;
import mncModel.MncModelPackage;

import mncModel.utility.CommandDistributionUtility;
import mncModel.utility.ResponseBlockUtility;
import mncModel.utility.TransitionUtility;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Command Response Block</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link mncModel.impl.CommandResponseBlockImpl#getCommand <em>Command</em>}</li>
 *   <li>{@link mncModel.impl.CommandResponseBlockImpl#getCommandValidation <em>Command Validation</em>}</li>
 *   <li>{@link mncModel.impl.CommandResponseBlockImpl#getTransition <em>Transition</em>}</li>
 *   <li>{@link mncModel.impl.CommandResponseBlockImpl#getCommandTriggerCondition <em>Command Trigger Condition</em>}</li>
 *   <li>{@link mncModel.impl.CommandResponseBlockImpl#getCommandTranslation <em>Command Translation</em>}</li>
 *   <li>{@link mncModel.impl.CommandResponseBlockImpl#getCommandDistributions <em>Command Distributions</em>}</li>
 *   <li>{@link mncModel.impl.CommandResponseBlockImpl#getResponseBlock <em>Response Block</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CommandResponseBlockImpl extends MinimalEObjectImpl.Container implements CommandResponseBlock {
	/**
	 * The cached value of the '{@link #getCommand() <em>Command</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCommand()
	 * @generated
	 * @ordered
	 */
	protected Command command;

	/**
	 * The cached value of the '{@link #getCommandValidation() <em>Command Validation</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCommandValidation()
	 * @generated
	 * @ordered
	 */
	protected CommandValidation commandValidation;

	/**
	 * The cached value of the '{@link #getTransition() <em>Transition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTransition()
	 * @generated
	 * @ordered
	 */
	protected TransitionUtility transition;

	/**
	 * The cached value of the '{@link #getCommandTriggerCondition() <em>Command Trigger Condition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCommandTriggerCondition()
	 * @generated
	 * @ordered
	 */
	protected CommandTriggerCondition commandTriggerCondition;

	/**
	 * The cached value of the '{@link #getCommandTranslation() <em>Command Translation</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCommandTranslation()
	 * @generated
	 * @ordered
	 */
	protected CommandTranslation commandTranslation;

	/**
	 * The cached value of the '{@link #getCommandDistributions() <em>Command Distributions</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCommandDistributions()
	 * @generated
	 * @ordered
	 */
	protected CommandDistributionUtility commandDistributions;

	/**
	 * The cached value of the '{@link #getResponseBlock() <em>Response Block</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getResponseBlock()
	 * @generated
	 * @ordered
	 */
	protected ResponseBlockUtility responseBlock;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CommandResponseBlockImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MncModelPackage.Literals.COMMAND_RESPONSE_BLOCK;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Command getCommand() {
		if (command != null && command.eIsProxy()) {
			InternalEObject oldCommand = (InternalEObject)command;
			command = (Command)eResolveProxy(oldCommand);
			if (command != oldCommand) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, MncModelPackage.COMMAND_RESPONSE_BLOCK__COMMAND, oldCommand, command));
			}
		}
		return command;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Command basicGetCommand() {
		return command;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCommand(Command newCommand) {
		Command oldCommand = command;
		command = newCommand;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MncModelPackage.COMMAND_RESPONSE_BLOCK__COMMAND, oldCommand, command));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CommandValidation getCommandValidation() {
		return commandValidation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCommandValidation(CommandValidation newCommandValidation, NotificationChain msgs) {
		CommandValidation oldCommandValidation = commandValidation;
		commandValidation = newCommandValidation;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MncModelPackage.COMMAND_RESPONSE_BLOCK__COMMAND_VALIDATION, oldCommandValidation, newCommandValidation);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCommandValidation(CommandValidation newCommandValidation) {
		if (newCommandValidation != commandValidation) {
			NotificationChain msgs = null;
			if (commandValidation != null)
				msgs = ((InternalEObject)commandValidation).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - MncModelPackage.COMMAND_RESPONSE_BLOCK__COMMAND_VALIDATION, null, msgs);
			if (newCommandValidation != null)
				msgs = ((InternalEObject)newCommandValidation).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - MncModelPackage.COMMAND_RESPONSE_BLOCK__COMMAND_VALIDATION, null, msgs);
			msgs = basicSetCommandValidation(newCommandValidation, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MncModelPackage.COMMAND_RESPONSE_BLOCK__COMMAND_VALIDATION, newCommandValidation, newCommandValidation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TransitionUtility getTransition() {
		return transition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTransition(TransitionUtility newTransition, NotificationChain msgs) {
		TransitionUtility oldTransition = transition;
		transition = newTransition;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MncModelPackage.COMMAND_RESPONSE_BLOCK__TRANSITION, oldTransition, newTransition);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTransition(TransitionUtility newTransition) {
		if (newTransition != transition) {
			NotificationChain msgs = null;
			if (transition != null)
				msgs = ((InternalEObject)transition).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - MncModelPackage.COMMAND_RESPONSE_BLOCK__TRANSITION, null, msgs);
			if (newTransition != null)
				msgs = ((InternalEObject)newTransition).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - MncModelPackage.COMMAND_RESPONSE_BLOCK__TRANSITION, null, msgs);
			msgs = basicSetTransition(newTransition, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MncModelPackage.COMMAND_RESPONSE_BLOCK__TRANSITION, newTransition, newTransition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CommandTriggerCondition getCommandTriggerCondition() {
		return commandTriggerCondition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCommandTriggerCondition(CommandTriggerCondition newCommandTriggerCondition, NotificationChain msgs) {
		CommandTriggerCondition oldCommandTriggerCondition = commandTriggerCondition;
		commandTriggerCondition = newCommandTriggerCondition;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MncModelPackage.COMMAND_RESPONSE_BLOCK__COMMAND_TRIGGER_CONDITION, oldCommandTriggerCondition, newCommandTriggerCondition);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCommandTriggerCondition(CommandTriggerCondition newCommandTriggerCondition) {
		if (newCommandTriggerCondition != commandTriggerCondition) {
			NotificationChain msgs = null;
			if (commandTriggerCondition != null)
				msgs = ((InternalEObject)commandTriggerCondition).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - MncModelPackage.COMMAND_RESPONSE_BLOCK__COMMAND_TRIGGER_CONDITION, null, msgs);
			if (newCommandTriggerCondition != null)
				msgs = ((InternalEObject)newCommandTriggerCondition).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - MncModelPackage.COMMAND_RESPONSE_BLOCK__COMMAND_TRIGGER_CONDITION, null, msgs);
			msgs = basicSetCommandTriggerCondition(newCommandTriggerCondition, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MncModelPackage.COMMAND_RESPONSE_BLOCK__COMMAND_TRIGGER_CONDITION, newCommandTriggerCondition, newCommandTriggerCondition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CommandTranslation getCommandTranslation() {
		return commandTranslation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCommandTranslation(CommandTranslation newCommandTranslation, NotificationChain msgs) {
		CommandTranslation oldCommandTranslation = commandTranslation;
		commandTranslation = newCommandTranslation;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MncModelPackage.COMMAND_RESPONSE_BLOCK__COMMAND_TRANSLATION, oldCommandTranslation, newCommandTranslation);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCommandTranslation(CommandTranslation newCommandTranslation) {
		if (newCommandTranslation != commandTranslation) {
			NotificationChain msgs = null;
			if (commandTranslation != null)
				msgs = ((InternalEObject)commandTranslation).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - MncModelPackage.COMMAND_RESPONSE_BLOCK__COMMAND_TRANSLATION, null, msgs);
			if (newCommandTranslation != null)
				msgs = ((InternalEObject)newCommandTranslation).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - MncModelPackage.COMMAND_RESPONSE_BLOCK__COMMAND_TRANSLATION, null, msgs);
			msgs = basicSetCommandTranslation(newCommandTranslation, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MncModelPackage.COMMAND_RESPONSE_BLOCK__COMMAND_TRANSLATION, newCommandTranslation, newCommandTranslation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CommandDistributionUtility getCommandDistributions() {
		return commandDistributions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCommandDistributions(CommandDistributionUtility newCommandDistributions, NotificationChain msgs) {
		CommandDistributionUtility oldCommandDistributions = commandDistributions;
		commandDistributions = newCommandDistributions;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MncModelPackage.COMMAND_RESPONSE_BLOCK__COMMAND_DISTRIBUTIONS, oldCommandDistributions, newCommandDistributions);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCommandDistributions(CommandDistributionUtility newCommandDistributions) {
		if (newCommandDistributions != commandDistributions) {
			NotificationChain msgs = null;
			if (commandDistributions != null)
				msgs = ((InternalEObject)commandDistributions).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - MncModelPackage.COMMAND_RESPONSE_BLOCK__COMMAND_DISTRIBUTIONS, null, msgs);
			if (newCommandDistributions != null)
				msgs = ((InternalEObject)newCommandDistributions).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - MncModelPackage.COMMAND_RESPONSE_BLOCK__COMMAND_DISTRIBUTIONS, null, msgs);
			msgs = basicSetCommandDistributions(newCommandDistributions, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MncModelPackage.COMMAND_RESPONSE_BLOCK__COMMAND_DISTRIBUTIONS, newCommandDistributions, newCommandDistributions));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ResponseBlockUtility getResponseBlock() {
		return responseBlock;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetResponseBlock(ResponseBlockUtility newResponseBlock, NotificationChain msgs) {
		ResponseBlockUtility oldResponseBlock = responseBlock;
		responseBlock = newResponseBlock;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MncModelPackage.COMMAND_RESPONSE_BLOCK__RESPONSE_BLOCK, oldResponseBlock, newResponseBlock);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setResponseBlock(ResponseBlockUtility newResponseBlock) {
		if (newResponseBlock != responseBlock) {
			NotificationChain msgs = null;
			if (responseBlock != null)
				msgs = ((InternalEObject)responseBlock).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - MncModelPackage.COMMAND_RESPONSE_BLOCK__RESPONSE_BLOCK, null, msgs);
			if (newResponseBlock != null)
				msgs = ((InternalEObject)newResponseBlock).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - MncModelPackage.COMMAND_RESPONSE_BLOCK__RESPONSE_BLOCK, null, msgs);
			msgs = basicSetResponseBlock(newResponseBlock, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MncModelPackage.COMMAND_RESPONSE_BLOCK__RESPONSE_BLOCK, newResponseBlock, newResponseBlock));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MncModelPackage.COMMAND_RESPONSE_BLOCK__COMMAND_VALIDATION:
				return basicSetCommandValidation(null, msgs);
			case MncModelPackage.COMMAND_RESPONSE_BLOCK__TRANSITION:
				return basicSetTransition(null, msgs);
			case MncModelPackage.COMMAND_RESPONSE_BLOCK__COMMAND_TRIGGER_CONDITION:
				return basicSetCommandTriggerCondition(null, msgs);
			case MncModelPackage.COMMAND_RESPONSE_BLOCK__COMMAND_TRANSLATION:
				return basicSetCommandTranslation(null, msgs);
			case MncModelPackage.COMMAND_RESPONSE_BLOCK__COMMAND_DISTRIBUTIONS:
				return basicSetCommandDistributions(null, msgs);
			case MncModelPackage.COMMAND_RESPONSE_BLOCK__RESPONSE_BLOCK:
				return basicSetResponseBlock(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MncModelPackage.COMMAND_RESPONSE_BLOCK__COMMAND:
				if (resolve) return getCommand();
				return basicGetCommand();
			case MncModelPackage.COMMAND_RESPONSE_BLOCK__COMMAND_VALIDATION:
				return getCommandValidation();
			case MncModelPackage.COMMAND_RESPONSE_BLOCK__TRANSITION:
				return getTransition();
			case MncModelPackage.COMMAND_RESPONSE_BLOCK__COMMAND_TRIGGER_CONDITION:
				return getCommandTriggerCondition();
			case MncModelPackage.COMMAND_RESPONSE_BLOCK__COMMAND_TRANSLATION:
				return getCommandTranslation();
			case MncModelPackage.COMMAND_RESPONSE_BLOCK__COMMAND_DISTRIBUTIONS:
				return getCommandDistributions();
			case MncModelPackage.COMMAND_RESPONSE_BLOCK__RESPONSE_BLOCK:
				return getResponseBlock();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MncModelPackage.COMMAND_RESPONSE_BLOCK__COMMAND:
				setCommand((Command)newValue);
				return;
			case MncModelPackage.COMMAND_RESPONSE_BLOCK__COMMAND_VALIDATION:
				setCommandValidation((CommandValidation)newValue);
				return;
			case MncModelPackage.COMMAND_RESPONSE_BLOCK__TRANSITION:
				setTransition((TransitionUtility)newValue);
				return;
			case MncModelPackage.COMMAND_RESPONSE_BLOCK__COMMAND_TRIGGER_CONDITION:
				setCommandTriggerCondition((CommandTriggerCondition)newValue);
				return;
			case MncModelPackage.COMMAND_RESPONSE_BLOCK__COMMAND_TRANSLATION:
				setCommandTranslation((CommandTranslation)newValue);
				return;
			case MncModelPackage.COMMAND_RESPONSE_BLOCK__COMMAND_DISTRIBUTIONS:
				setCommandDistributions((CommandDistributionUtility)newValue);
				return;
			case MncModelPackage.COMMAND_RESPONSE_BLOCK__RESPONSE_BLOCK:
				setResponseBlock((ResponseBlockUtility)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MncModelPackage.COMMAND_RESPONSE_BLOCK__COMMAND:
				setCommand((Command)null);
				return;
			case MncModelPackage.COMMAND_RESPONSE_BLOCK__COMMAND_VALIDATION:
				setCommandValidation((CommandValidation)null);
				return;
			case MncModelPackage.COMMAND_RESPONSE_BLOCK__TRANSITION:
				setTransition((TransitionUtility)null);
				return;
			case MncModelPackage.COMMAND_RESPONSE_BLOCK__COMMAND_TRIGGER_CONDITION:
				setCommandTriggerCondition((CommandTriggerCondition)null);
				return;
			case MncModelPackage.COMMAND_RESPONSE_BLOCK__COMMAND_TRANSLATION:
				setCommandTranslation((CommandTranslation)null);
				return;
			case MncModelPackage.COMMAND_RESPONSE_BLOCK__COMMAND_DISTRIBUTIONS:
				setCommandDistributions((CommandDistributionUtility)null);
				return;
			case MncModelPackage.COMMAND_RESPONSE_BLOCK__RESPONSE_BLOCK:
				setResponseBlock((ResponseBlockUtility)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MncModelPackage.COMMAND_RESPONSE_BLOCK__COMMAND:
				return command != null;
			case MncModelPackage.COMMAND_RESPONSE_BLOCK__COMMAND_VALIDATION:
				return commandValidation != null;
			case MncModelPackage.COMMAND_RESPONSE_BLOCK__TRANSITION:
				return transition != null;
			case MncModelPackage.COMMAND_RESPONSE_BLOCK__COMMAND_TRIGGER_CONDITION:
				return commandTriggerCondition != null;
			case MncModelPackage.COMMAND_RESPONSE_BLOCK__COMMAND_TRANSLATION:
				return commandTranslation != null;
			case MncModelPackage.COMMAND_RESPONSE_BLOCK__COMMAND_DISTRIBUTIONS:
				return commandDistributions != null;
			case MncModelPackage.COMMAND_RESPONSE_BLOCK__RESPONSE_BLOCK:
				return responseBlock != null;
		}
		return super.eIsSet(featureID);
	}

} //CommandResponseBlockImpl
