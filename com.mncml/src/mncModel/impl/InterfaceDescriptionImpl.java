/**
 */
package mncModel.impl;

import java.util.Collection;

import mncModel.Address;
import mncModel.Event;
import mncModel.InterfaceDescription;
import mncModel.MncModelPackage;
import mncModel.Port;
import mncModel.SubscribableItemList;

import mncModel.utility.AlarmUtility;
import mncModel.utility.CommandUtility;
import mncModel.utility.DataPointUtility;
import mncModel.utility.EventUtility;
import mncModel.utility.ResponseUtility;
import mncModel.utility.StateUtility;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Interface Description</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link mncModel.impl.InterfaceDescriptionImpl#getName <em>Name</em>}</li>
 *   <li>{@link mncModel.impl.InterfaceDescriptionImpl#getIpaddress <em>Ipaddress</em>}</li>
 *   <li>{@link mncModel.impl.InterfaceDescriptionImpl#getPort <em>Port</em>}</li>
 *   <li>{@link mncModel.impl.InterfaceDescriptionImpl#getDataPoints <em>Data Points</em>}</li>
 *   <li>{@link mncModel.impl.InterfaceDescriptionImpl#getAlarms <em>Alarms</em>}</li>
 *   <li>{@link mncModel.impl.InterfaceDescriptionImpl#getCommands <em>Commands</em>}</li>
 *   <li>{@link mncModel.impl.InterfaceDescriptionImpl#getEvents <em>Events</em>}</li>
 *   <li>{@link mncModel.impl.InterfaceDescriptionImpl#getSubscribedEvents <em>Subscribed Events</em>}</li>
 *   <li>{@link mncModel.impl.InterfaceDescriptionImpl#getResponses <em>Responses</em>}</li>
 *   <li>{@link mncModel.impl.InterfaceDescriptionImpl#getOperatingStates <em>Operating States</em>}</li>
 *   <li>{@link mncModel.impl.InterfaceDescriptionImpl#getSubscribedItems <em>Subscribed Items</em>}</li>
 *   <li>{@link mncModel.impl.InterfaceDescriptionImpl#getUses <em>Uses</em>}</li>
 * </ul>
 *
 * @generated
 */
public class InterfaceDescriptionImpl extends SystemImpl implements InterfaceDescription {
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getIpaddress() <em>Ipaddress</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIpaddress()
	 * @generated
	 * @ordered
	 */
	protected Address ipaddress;

	/**
	 * The cached value of the '{@link #getPort() <em>Port</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPort()
	 * @generated
	 * @ordered
	 */
	protected Port port;

	/**
	 * The cached value of the '{@link #getDataPoints() <em>Data Points</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDataPoints()
	 * @generated
	 * @ordered
	 */
	protected DataPointUtility dataPoints;

	/**
	 * The cached value of the '{@link #getAlarms() <em>Alarms</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAlarms()
	 * @generated
	 * @ordered
	 */
	protected AlarmUtility alarms;

	/**
	 * The cached value of the '{@link #getCommands() <em>Commands</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCommands()
	 * @generated
	 * @ordered
	 */
	protected CommandUtility commands;

	/**
	 * The cached value of the '{@link #getEvents() <em>Events</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEvents()
	 * @generated
	 * @ordered
	 */
	protected EventUtility events;

	/**
	 * The cached value of the '{@link #getSubscribedEvents() <em>Subscribed Events</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSubscribedEvents()
	 * @generated
	 * @ordered
	 */
	protected EList<Event> subscribedEvents;

	/**
	 * The cached value of the '{@link #getResponses() <em>Responses</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getResponses()
	 * @generated
	 * @ordered
	 */
	protected ResponseUtility responses;

	/**
	 * The cached value of the '{@link #getOperatingStates() <em>Operating States</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperatingStates()
	 * @generated
	 * @ordered
	 */
	protected StateUtility operatingStates;

	/**
	 * The cached value of the '{@link #getSubscribedItems() <em>Subscribed Items</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSubscribedItems()
	 * @generated
	 * @ordered
	 */
	protected SubscribableItemList subscribedItems;

	/**
	 * The cached value of the '{@link #getUses() <em>Uses</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUses()
	 * @generated
	 * @ordered
	 */
	protected EList<InterfaceDescription> uses;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected InterfaceDescriptionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MncModelPackage.Literals.INTERFACE_DESCRIPTION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MncModelPackage.INTERFACE_DESCRIPTION__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Address getIpaddress() {
		return ipaddress;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetIpaddress(Address newIpaddress, NotificationChain msgs) {
		Address oldIpaddress = ipaddress;
		ipaddress = newIpaddress;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MncModelPackage.INTERFACE_DESCRIPTION__IPADDRESS, oldIpaddress, newIpaddress);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIpaddress(Address newIpaddress) {
		if (newIpaddress != ipaddress) {
			NotificationChain msgs = null;
			if (ipaddress != null)
				msgs = ((InternalEObject)ipaddress).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - MncModelPackage.INTERFACE_DESCRIPTION__IPADDRESS, null, msgs);
			if (newIpaddress != null)
				msgs = ((InternalEObject)newIpaddress).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - MncModelPackage.INTERFACE_DESCRIPTION__IPADDRESS, null, msgs);
			msgs = basicSetIpaddress(newIpaddress, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MncModelPackage.INTERFACE_DESCRIPTION__IPADDRESS, newIpaddress, newIpaddress));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Port getPort() {
		return port;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPort(Port newPort, NotificationChain msgs) {
		Port oldPort = port;
		port = newPort;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MncModelPackage.INTERFACE_DESCRIPTION__PORT, oldPort, newPort);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPort(Port newPort) {
		if (newPort != port) {
			NotificationChain msgs = null;
			if (port != null)
				msgs = ((InternalEObject)port).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - MncModelPackage.INTERFACE_DESCRIPTION__PORT, null, msgs);
			if (newPort != null)
				msgs = ((InternalEObject)newPort).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - MncModelPackage.INTERFACE_DESCRIPTION__PORT, null, msgs);
			msgs = basicSetPort(newPort, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MncModelPackage.INTERFACE_DESCRIPTION__PORT, newPort, newPort));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataPointUtility getDataPoints() {
		return dataPoints;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDataPoints(DataPointUtility newDataPoints, NotificationChain msgs) {
		DataPointUtility oldDataPoints = dataPoints;
		dataPoints = newDataPoints;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MncModelPackage.INTERFACE_DESCRIPTION__DATA_POINTS, oldDataPoints, newDataPoints);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDataPoints(DataPointUtility newDataPoints) {
		if (newDataPoints != dataPoints) {
			NotificationChain msgs = null;
			if (dataPoints != null)
				msgs = ((InternalEObject)dataPoints).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - MncModelPackage.INTERFACE_DESCRIPTION__DATA_POINTS, null, msgs);
			if (newDataPoints != null)
				msgs = ((InternalEObject)newDataPoints).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - MncModelPackage.INTERFACE_DESCRIPTION__DATA_POINTS, null, msgs);
			msgs = basicSetDataPoints(newDataPoints, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MncModelPackage.INTERFACE_DESCRIPTION__DATA_POINTS, newDataPoints, newDataPoints));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AlarmUtility getAlarms() {
		return alarms;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAlarms(AlarmUtility newAlarms, NotificationChain msgs) {
		AlarmUtility oldAlarms = alarms;
		alarms = newAlarms;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MncModelPackage.INTERFACE_DESCRIPTION__ALARMS, oldAlarms, newAlarms);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAlarms(AlarmUtility newAlarms) {
		if (newAlarms != alarms) {
			NotificationChain msgs = null;
			if (alarms != null)
				msgs = ((InternalEObject)alarms).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - MncModelPackage.INTERFACE_DESCRIPTION__ALARMS, null, msgs);
			if (newAlarms != null)
				msgs = ((InternalEObject)newAlarms).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - MncModelPackage.INTERFACE_DESCRIPTION__ALARMS, null, msgs);
			msgs = basicSetAlarms(newAlarms, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MncModelPackage.INTERFACE_DESCRIPTION__ALARMS, newAlarms, newAlarms));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CommandUtility getCommands() {
		return commands;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCommands(CommandUtility newCommands, NotificationChain msgs) {
		CommandUtility oldCommands = commands;
		commands = newCommands;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MncModelPackage.INTERFACE_DESCRIPTION__COMMANDS, oldCommands, newCommands);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCommands(CommandUtility newCommands) {
		if (newCommands != commands) {
			NotificationChain msgs = null;
			if (commands != null)
				msgs = ((InternalEObject)commands).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - MncModelPackage.INTERFACE_DESCRIPTION__COMMANDS, null, msgs);
			if (newCommands != null)
				msgs = ((InternalEObject)newCommands).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - MncModelPackage.INTERFACE_DESCRIPTION__COMMANDS, null, msgs);
			msgs = basicSetCommands(newCommands, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MncModelPackage.INTERFACE_DESCRIPTION__COMMANDS, newCommands, newCommands));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EventUtility getEvents() {
		return events;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetEvents(EventUtility newEvents, NotificationChain msgs) {
		EventUtility oldEvents = events;
		events = newEvents;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MncModelPackage.INTERFACE_DESCRIPTION__EVENTS, oldEvents, newEvents);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEvents(EventUtility newEvents) {
		if (newEvents != events) {
			NotificationChain msgs = null;
			if (events != null)
				msgs = ((InternalEObject)events).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - MncModelPackage.INTERFACE_DESCRIPTION__EVENTS, null, msgs);
			if (newEvents != null)
				msgs = ((InternalEObject)newEvents).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - MncModelPackage.INTERFACE_DESCRIPTION__EVENTS, null, msgs);
			msgs = basicSetEvents(newEvents, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MncModelPackage.INTERFACE_DESCRIPTION__EVENTS, newEvents, newEvents));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Event> getSubscribedEvents() {
		if (subscribedEvents == null) {
			subscribedEvents = new EObjectResolvingEList<Event>(Event.class, this, MncModelPackage.INTERFACE_DESCRIPTION__SUBSCRIBED_EVENTS);
		}
		return subscribedEvents;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ResponseUtility getResponses() {
		return responses;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetResponses(ResponseUtility newResponses, NotificationChain msgs) {
		ResponseUtility oldResponses = responses;
		responses = newResponses;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MncModelPackage.INTERFACE_DESCRIPTION__RESPONSES, oldResponses, newResponses);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setResponses(ResponseUtility newResponses) {
		if (newResponses != responses) {
			NotificationChain msgs = null;
			if (responses != null)
				msgs = ((InternalEObject)responses).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - MncModelPackage.INTERFACE_DESCRIPTION__RESPONSES, null, msgs);
			if (newResponses != null)
				msgs = ((InternalEObject)newResponses).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - MncModelPackage.INTERFACE_DESCRIPTION__RESPONSES, null, msgs);
			msgs = basicSetResponses(newResponses, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MncModelPackage.INTERFACE_DESCRIPTION__RESPONSES, newResponses, newResponses));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StateUtility getOperatingStates() {
		return operatingStates;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetOperatingStates(StateUtility newOperatingStates, NotificationChain msgs) {
		StateUtility oldOperatingStates = operatingStates;
		operatingStates = newOperatingStates;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MncModelPackage.INTERFACE_DESCRIPTION__OPERATING_STATES, oldOperatingStates, newOperatingStates);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOperatingStates(StateUtility newOperatingStates) {
		if (newOperatingStates != operatingStates) {
			NotificationChain msgs = null;
			if (operatingStates != null)
				msgs = ((InternalEObject)operatingStates).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - MncModelPackage.INTERFACE_DESCRIPTION__OPERATING_STATES, null, msgs);
			if (newOperatingStates != null)
				msgs = ((InternalEObject)newOperatingStates).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - MncModelPackage.INTERFACE_DESCRIPTION__OPERATING_STATES, null, msgs);
			msgs = basicSetOperatingStates(newOperatingStates, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MncModelPackage.INTERFACE_DESCRIPTION__OPERATING_STATES, newOperatingStates, newOperatingStates));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SubscribableItemList getSubscribedItems() {
		return subscribedItems;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSubscribedItems(SubscribableItemList newSubscribedItems, NotificationChain msgs) {
		SubscribableItemList oldSubscribedItems = subscribedItems;
		subscribedItems = newSubscribedItems;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MncModelPackage.INTERFACE_DESCRIPTION__SUBSCRIBED_ITEMS, oldSubscribedItems, newSubscribedItems);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSubscribedItems(SubscribableItemList newSubscribedItems) {
		if (newSubscribedItems != subscribedItems) {
			NotificationChain msgs = null;
			if (subscribedItems != null)
				msgs = ((InternalEObject)subscribedItems).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - MncModelPackage.INTERFACE_DESCRIPTION__SUBSCRIBED_ITEMS, null, msgs);
			if (newSubscribedItems != null)
				msgs = ((InternalEObject)newSubscribedItems).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - MncModelPackage.INTERFACE_DESCRIPTION__SUBSCRIBED_ITEMS, null, msgs);
			msgs = basicSetSubscribedItems(newSubscribedItems, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MncModelPackage.INTERFACE_DESCRIPTION__SUBSCRIBED_ITEMS, newSubscribedItems, newSubscribedItems));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<InterfaceDescription> getUses() {
		if (uses == null) {
			uses = new EObjectResolvingEList<InterfaceDescription>(InterfaceDescription.class, this, MncModelPackage.INTERFACE_DESCRIPTION__USES);
		}
		return uses;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MncModelPackage.INTERFACE_DESCRIPTION__IPADDRESS:
				return basicSetIpaddress(null, msgs);
			case MncModelPackage.INTERFACE_DESCRIPTION__PORT:
				return basicSetPort(null, msgs);
			case MncModelPackage.INTERFACE_DESCRIPTION__DATA_POINTS:
				return basicSetDataPoints(null, msgs);
			case MncModelPackage.INTERFACE_DESCRIPTION__ALARMS:
				return basicSetAlarms(null, msgs);
			case MncModelPackage.INTERFACE_DESCRIPTION__COMMANDS:
				return basicSetCommands(null, msgs);
			case MncModelPackage.INTERFACE_DESCRIPTION__EVENTS:
				return basicSetEvents(null, msgs);
			case MncModelPackage.INTERFACE_DESCRIPTION__RESPONSES:
				return basicSetResponses(null, msgs);
			case MncModelPackage.INTERFACE_DESCRIPTION__OPERATING_STATES:
				return basicSetOperatingStates(null, msgs);
			case MncModelPackage.INTERFACE_DESCRIPTION__SUBSCRIBED_ITEMS:
				return basicSetSubscribedItems(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MncModelPackage.INTERFACE_DESCRIPTION__NAME:
				return getName();
			case MncModelPackage.INTERFACE_DESCRIPTION__IPADDRESS:
				return getIpaddress();
			case MncModelPackage.INTERFACE_DESCRIPTION__PORT:
				return getPort();
			case MncModelPackage.INTERFACE_DESCRIPTION__DATA_POINTS:
				return getDataPoints();
			case MncModelPackage.INTERFACE_DESCRIPTION__ALARMS:
				return getAlarms();
			case MncModelPackage.INTERFACE_DESCRIPTION__COMMANDS:
				return getCommands();
			case MncModelPackage.INTERFACE_DESCRIPTION__EVENTS:
				return getEvents();
			case MncModelPackage.INTERFACE_DESCRIPTION__SUBSCRIBED_EVENTS:
				return getSubscribedEvents();
			case MncModelPackage.INTERFACE_DESCRIPTION__RESPONSES:
				return getResponses();
			case MncModelPackage.INTERFACE_DESCRIPTION__OPERATING_STATES:
				return getOperatingStates();
			case MncModelPackage.INTERFACE_DESCRIPTION__SUBSCRIBED_ITEMS:
				return getSubscribedItems();
			case MncModelPackage.INTERFACE_DESCRIPTION__USES:
				return getUses();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MncModelPackage.INTERFACE_DESCRIPTION__NAME:
				setName((String)newValue);
				return;
			case MncModelPackage.INTERFACE_DESCRIPTION__IPADDRESS:
				setIpaddress((Address)newValue);
				return;
			case MncModelPackage.INTERFACE_DESCRIPTION__PORT:
				setPort((Port)newValue);
				return;
			case MncModelPackage.INTERFACE_DESCRIPTION__DATA_POINTS:
				setDataPoints((DataPointUtility)newValue);
				return;
			case MncModelPackage.INTERFACE_DESCRIPTION__ALARMS:
				setAlarms((AlarmUtility)newValue);
				return;
			case MncModelPackage.INTERFACE_DESCRIPTION__COMMANDS:
				setCommands((CommandUtility)newValue);
				return;
			case MncModelPackage.INTERFACE_DESCRIPTION__EVENTS:
				setEvents((EventUtility)newValue);
				return;
			case MncModelPackage.INTERFACE_DESCRIPTION__SUBSCRIBED_EVENTS:
				getSubscribedEvents().clear();
				getSubscribedEvents().addAll((Collection<? extends Event>)newValue);
				return;
			case MncModelPackage.INTERFACE_DESCRIPTION__RESPONSES:
				setResponses((ResponseUtility)newValue);
				return;
			case MncModelPackage.INTERFACE_DESCRIPTION__OPERATING_STATES:
				setOperatingStates((StateUtility)newValue);
				return;
			case MncModelPackage.INTERFACE_DESCRIPTION__SUBSCRIBED_ITEMS:
				setSubscribedItems((SubscribableItemList)newValue);
				return;
			case MncModelPackage.INTERFACE_DESCRIPTION__USES:
				getUses().clear();
				getUses().addAll((Collection<? extends InterfaceDescription>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MncModelPackage.INTERFACE_DESCRIPTION__NAME:
				setName(NAME_EDEFAULT);
				return;
			case MncModelPackage.INTERFACE_DESCRIPTION__IPADDRESS:
				setIpaddress((Address)null);
				return;
			case MncModelPackage.INTERFACE_DESCRIPTION__PORT:
				setPort((Port)null);
				return;
			case MncModelPackage.INTERFACE_DESCRIPTION__DATA_POINTS:
				setDataPoints((DataPointUtility)null);
				return;
			case MncModelPackage.INTERFACE_DESCRIPTION__ALARMS:
				setAlarms((AlarmUtility)null);
				return;
			case MncModelPackage.INTERFACE_DESCRIPTION__COMMANDS:
				setCommands((CommandUtility)null);
				return;
			case MncModelPackage.INTERFACE_DESCRIPTION__EVENTS:
				setEvents((EventUtility)null);
				return;
			case MncModelPackage.INTERFACE_DESCRIPTION__SUBSCRIBED_EVENTS:
				getSubscribedEvents().clear();
				return;
			case MncModelPackage.INTERFACE_DESCRIPTION__RESPONSES:
				setResponses((ResponseUtility)null);
				return;
			case MncModelPackage.INTERFACE_DESCRIPTION__OPERATING_STATES:
				setOperatingStates((StateUtility)null);
				return;
			case MncModelPackage.INTERFACE_DESCRIPTION__SUBSCRIBED_ITEMS:
				setSubscribedItems((SubscribableItemList)null);
				return;
			case MncModelPackage.INTERFACE_DESCRIPTION__USES:
				getUses().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MncModelPackage.INTERFACE_DESCRIPTION__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case MncModelPackage.INTERFACE_DESCRIPTION__IPADDRESS:
				return ipaddress != null;
			case MncModelPackage.INTERFACE_DESCRIPTION__PORT:
				return port != null;
			case MncModelPackage.INTERFACE_DESCRIPTION__DATA_POINTS:
				return dataPoints != null;
			case MncModelPackage.INTERFACE_DESCRIPTION__ALARMS:
				return alarms != null;
			case MncModelPackage.INTERFACE_DESCRIPTION__COMMANDS:
				return commands != null;
			case MncModelPackage.INTERFACE_DESCRIPTION__EVENTS:
				return events != null;
			case MncModelPackage.INTERFACE_DESCRIPTION__SUBSCRIBED_EVENTS:
				return subscribedEvents != null && !subscribedEvents.isEmpty();
			case MncModelPackage.INTERFACE_DESCRIPTION__RESPONSES:
				return responses != null;
			case MncModelPackage.INTERFACE_DESCRIPTION__OPERATING_STATES:
				return operatingStates != null;
			case MncModelPackage.INTERFACE_DESCRIPTION__SUBSCRIBED_ITEMS:
				return subscribedItems != null;
			case MncModelPackage.INTERFACE_DESCRIPTION__USES:
				return uses != null && !uses.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(')');
		return result.toString();
	}

} //InterfaceDescriptionImpl
