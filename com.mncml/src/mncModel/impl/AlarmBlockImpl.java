/**
 */
package mncModel.impl;

import mncModel.Alarm;
import mncModel.AlarmBlock;
import mncModel.AlarmHandling;
import mncModel.AlarmTriggerCondition;
import mncModel.MncModelPackage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Alarm Block</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link mncModel.impl.AlarmBlockImpl#getAlarm <em>Alarm</em>}</li>
 *   <li>{@link mncModel.impl.AlarmBlockImpl#getAlarmCondition <em>Alarm Condition</em>}</li>
 *   <li>{@link mncModel.impl.AlarmBlockImpl#getAlarmHandling <em>Alarm Handling</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AlarmBlockImpl extends MinimalEObjectImpl.Container implements AlarmBlock {
	/**
	 * The cached value of the '{@link #getAlarm() <em>Alarm</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAlarm()
	 * @generated
	 * @ordered
	 */
	protected Alarm alarm;

	/**
	 * The cached value of the '{@link #getAlarmCondition() <em>Alarm Condition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAlarmCondition()
	 * @generated
	 * @ordered
	 */
	protected AlarmTriggerCondition alarmCondition;

	/**
	 * The cached value of the '{@link #getAlarmHandling() <em>Alarm Handling</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAlarmHandling()
	 * @generated
	 * @ordered
	 */
	protected AlarmHandling alarmHandling;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AlarmBlockImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MncModelPackage.Literals.ALARM_BLOCK;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Alarm getAlarm() {
		if (alarm != null && alarm.eIsProxy()) {
			InternalEObject oldAlarm = (InternalEObject)alarm;
			alarm = (Alarm)eResolveProxy(oldAlarm);
			if (alarm != oldAlarm) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, MncModelPackage.ALARM_BLOCK__ALARM, oldAlarm, alarm));
			}
		}
		return alarm;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Alarm basicGetAlarm() {
		return alarm;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAlarm(Alarm newAlarm) {
		Alarm oldAlarm = alarm;
		alarm = newAlarm;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MncModelPackage.ALARM_BLOCK__ALARM, oldAlarm, alarm));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AlarmTriggerCondition getAlarmCondition() {
		return alarmCondition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAlarmCondition(AlarmTriggerCondition newAlarmCondition, NotificationChain msgs) {
		AlarmTriggerCondition oldAlarmCondition = alarmCondition;
		alarmCondition = newAlarmCondition;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MncModelPackage.ALARM_BLOCK__ALARM_CONDITION, oldAlarmCondition, newAlarmCondition);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAlarmCondition(AlarmTriggerCondition newAlarmCondition) {
		if (newAlarmCondition != alarmCondition) {
			NotificationChain msgs = null;
			if (alarmCondition != null)
				msgs = ((InternalEObject)alarmCondition).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - MncModelPackage.ALARM_BLOCK__ALARM_CONDITION, null, msgs);
			if (newAlarmCondition != null)
				msgs = ((InternalEObject)newAlarmCondition).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - MncModelPackage.ALARM_BLOCK__ALARM_CONDITION, null, msgs);
			msgs = basicSetAlarmCondition(newAlarmCondition, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MncModelPackage.ALARM_BLOCK__ALARM_CONDITION, newAlarmCondition, newAlarmCondition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AlarmHandling getAlarmHandling() {
		return alarmHandling;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAlarmHandling(AlarmHandling newAlarmHandling, NotificationChain msgs) {
		AlarmHandling oldAlarmHandling = alarmHandling;
		alarmHandling = newAlarmHandling;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MncModelPackage.ALARM_BLOCK__ALARM_HANDLING, oldAlarmHandling, newAlarmHandling);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAlarmHandling(AlarmHandling newAlarmHandling) {
		if (newAlarmHandling != alarmHandling) {
			NotificationChain msgs = null;
			if (alarmHandling != null)
				msgs = ((InternalEObject)alarmHandling).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - MncModelPackage.ALARM_BLOCK__ALARM_HANDLING, null, msgs);
			if (newAlarmHandling != null)
				msgs = ((InternalEObject)newAlarmHandling).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - MncModelPackage.ALARM_BLOCK__ALARM_HANDLING, null, msgs);
			msgs = basicSetAlarmHandling(newAlarmHandling, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MncModelPackage.ALARM_BLOCK__ALARM_HANDLING, newAlarmHandling, newAlarmHandling));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MncModelPackage.ALARM_BLOCK__ALARM_CONDITION:
				return basicSetAlarmCondition(null, msgs);
			case MncModelPackage.ALARM_BLOCK__ALARM_HANDLING:
				return basicSetAlarmHandling(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MncModelPackage.ALARM_BLOCK__ALARM:
				if (resolve) return getAlarm();
				return basicGetAlarm();
			case MncModelPackage.ALARM_BLOCK__ALARM_CONDITION:
				return getAlarmCondition();
			case MncModelPackage.ALARM_BLOCK__ALARM_HANDLING:
				return getAlarmHandling();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MncModelPackage.ALARM_BLOCK__ALARM:
				setAlarm((Alarm)newValue);
				return;
			case MncModelPackage.ALARM_BLOCK__ALARM_CONDITION:
				setAlarmCondition((AlarmTriggerCondition)newValue);
				return;
			case MncModelPackage.ALARM_BLOCK__ALARM_HANDLING:
				setAlarmHandling((AlarmHandling)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MncModelPackage.ALARM_BLOCK__ALARM:
				setAlarm((Alarm)null);
				return;
			case MncModelPackage.ALARM_BLOCK__ALARM_CONDITION:
				setAlarmCondition((AlarmTriggerCondition)null);
				return;
			case MncModelPackage.ALARM_BLOCK__ALARM_HANDLING:
				setAlarmHandling((AlarmHandling)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MncModelPackage.ALARM_BLOCK__ALARM:
				return alarm != null;
			case MncModelPackage.ALARM_BLOCK__ALARM_CONDITION:
				return alarmCondition != null;
			case MncModelPackage.ALARM_BLOCK__ALARM_HANDLING:
				return alarmHandling != null;
		}
		return super.eIsSet(featureID);
	}

} //AlarmBlockImpl
