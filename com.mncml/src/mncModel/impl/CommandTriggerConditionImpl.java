/**
 */
package mncModel.impl;

import mncModel.AbstractOperationableItems;
import mncModel.CommandTriggerCondition;
import mncModel.MncModelPackage;
import mncModel.ResponsibleItemList;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Command Trigger Condition</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link mncModel.impl.CommandTriggerConditionImpl#getResponsibleItems <em>Responsible Items</em>}</li>
 *   <li>{@link mncModel.impl.CommandTriggerConditionImpl#getOperation <em>Operation</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CommandTriggerConditionImpl extends MinimalEObjectImpl.Container implements CommandTriggerCondition {
	/**
	 * The cached value of the '{@link #getResponsibleItems() <em>Responsible Items</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getResponsibleItems()
	 * @generated
	 * @ordered
	 */
	protected ResponsibleItemList responsibleItems;

	/**
	 * The cached value of the '{@link #getOperation() <em>Operation</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperation()
	 * @generated
	 * @ordered
	 */
	protected AbstractOperationableItems operation;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CommandTriggerConditionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MncModelPackage.Literals.COMMAND_TRIGGER_CONDITION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ResponsibleItemList getResponsibleItems() {
		return responsibleItems;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetResponsibleItems(ResponsibleItemList newResponsibleItems, NotificationChain msgs) {
		ResponsibleItemList oldResponsibleItems = responsibleItems;
		responsibleItems = newResponsibleItems;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MncModelPackage.COMMAND_TRIGGER_CONDITION__RESPONSIBLE_ITEMS, oldResponsibleItems, newResponsibleItems);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setResponsibleItems(ResponsibleItemList newResponsibleItems) {
		if (newResponsibleItems != responsibleItems) {
			NotificationChain msgs = null;
			if (responsibleItems != null)
				msgs = ((InternalEObject)responsibleItems).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - MncModelPackage.COMMAND_TRIGGER_CONDITION__RESPONSIBLE_ITEMS, null, msgs);
			if (newResponsibleItems != null)
				msgs = ((InternalEObject)newResponsibleItems).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - MncModelPackage.COMMAND_TRIGGER_CONDITION__RESPONSIBLE_ITEMS, null, msgs);
			msgs = basicSetResponsibleItems(newResponsibleItems, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MncModelPackage.COMMAND_TRIGGER_CONDITION__RESPONSIBLE_ITEMS, newResponsibleItems, newResponsibleItems));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AbstractOperationableItems getOperation() {
		return operation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetOperation(AbstractOperationableItems newOperation, NotificationChain msgs) {
		AbstractOperationableItems oldOperation = operation;
		operation = newOperation;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MncModelPackage.COMMAND_TRIGGER_CONDITION__OPERATION, oldOperation, newOperation);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOperation(AbstractOperationableItems newOperation) {
		if (newOperation != operation) {
			NotificationChain msgs = null;
			if (operation != null)
				msgs = ((InternalEObject)operation).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - MncModelPackage.COMMAND_TRIGGER_CONDITION__OPERATION, null, msgs);
			if (newOperation != null)
				msgs = ((InternalEObject)newOperation).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - MncModelPackage.COMMAND_TRIGGER_CONDITION__OPERATION, null, msgs);
			msgs = basicSetOperation(newOperation, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MncModelPackage.COMMAND_TRIGGER_CONDITION__OPERATION, newOperation, newOperation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MncModelPackage.COMMAND_TRIGGER_CONDITION__RESPONSIBLE_ITEMS:
				return basicSetResponsibleItems(null, msgs);
			case MncModelPackage.COMMAND_TRIGGER_CONDITION__OPERATION:
				return basicSetOperation(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MncModelPackage.COMMAND_TRIGGER_CONDITION__RESPONSIBLE_ITEMS:
				return getResponsibleItems();
			case MncModelPackage.COMMAND_TRIGGER_CONDITION__OPERATION:
				return getOperation();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MncModelPackage.COMMAND_TRIGGER_CONDITION__RESPONSIBLE_ITEMS:
				setResponsibleItems((ResponsibleItemList)newValue);
				return;
			case MncModelPackage.COMMAND_TRIGGER_CONDITION__OPERATION:
				setOperation((AbstractOperationableItems)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MncModelPackage.COMMAND_TRIGGER_CONDITION__RESPONSIBLE_ITEMS:
				setResponsibleItems((ResponsibleItemList)null);
				return;
			case MncModelPackage.COMMAND_TRIGGER_CONDITION__OPERATION:
				setOperation((AbstractOperationableItems)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MncModelPackage.COMMAND_TRIGGER_CONDITION__RESPONSIBLE_ITEMS:
				return responsibleItems != null;
			case MncModelPackage.COMMAND_TRIGGER_CONDITION__OPERATION:
				return operation != null;
		}
		return super.eIsSet(featureID);
	}

} //CommandTriggerConditionImpl
