/**
 */
package mncModel.impl;

import java.util.Collection;

import mncModel.Import;
import mncModel.MncModelPackage;
import mncModel.Model;

import mncModel.security.Security;

import mncModel.stakeholder.Stakeholder;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Model</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link mncModel.impl.ModelImpl#getSystems <em>Systems</em>}</li>
 *   <li>{@link mncModel.impl.ModelImpl#getName <em>Name</em>}</li>
 *   <li>{@link mncModel.impl.ModelImpl#getImportSection <em>Import Section</em>}</li>
 *   <li>{@link mncModel.impl.ModelImpl#getSecurity <em>Security</em>}</li>
 *   <li>{@link mncModel.impl.ModelImpl#getStakeholder <em>Stakeholder</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ModelImpl extends MinimalEObjectImpl.Container implements Model {
	/**
	 * The cached value of the '{@link #getSystems() <em>Systems</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSystems()
	 * @generated
	 * @ordered
	 */
	protected EList<mncModel.System> systems;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getImportSection() <em>Import Section</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getImportSection()
	 * @generated
	 * @ordered
	 */
	protected EList<Import> importSection;

	/**
	 * The cached value of the '{@link #getSecurity() <em>Security</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSecurity()
	 * @generated
	 * @ordered
	 */
	protected Security security;

	/**
	 * The cached value of the '{@link #getStakeholder() <em>Stakeholder</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStakeholder()
	 * @generated
	 * @ordered
	 */
	protected Stakeholder stakeholder;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ModelImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MncModelPackage.Literals.MODEL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<mncModel.System> getSystems() {
		if (systems == null) {
			systems = new EObjectContainmentEList<mncModel.System>(mncModel.System.class, this, MncModelPackage.MODEL__SYSTEMS);
		}
		return systems;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MncModelPackage.MODEL__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Import> getImportSection() {
		if (importSection == null) {
			importSection = new EObjectContainmentEList<Import>(Import.class, this, MncModelPackage.MODEL__IMPORT_SECTION);
		}
		return importSection;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Security getSecurity() {
		return security;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSecurity(Security newSecurity, NotificationChain msgs) {
		Security oldSecurity = security;
		security = newSecurity;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MncModelPackage.MODEL__SECURITY, oldSecurity, newSecurity);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSecurity(Security newSecurity) {
		if (newSecurity != security) {
			NotificationChain msgs = null;
			if (security != null)
				msgs = ((InternalEObject)security).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - MncModelPackage.MODEL__SECURITY, null, msgs);
			if (newSecurity != null)
				msgs = ((InternalEObject)newSecurity).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - MncModelPackage.MODEL__SECURITY, null, msgs);
			msgs = basicSetSecurity(newSecurity, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MncModelPackage.MODEL__SECURITY, newSecurity, newSecurity));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Stakeholder getStakeholder() {
		return stakeholder;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetStakeholder(Stakeholder newStakeholder, NotificationChain msgs) {
		Stakeholder oldStakeholder = stakeholder;
		stakeholder = newStakeholder;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MncModelPackage.MODEL__STAKEHOLDER, oldStakeholder, newStakeholder);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStakeholder(Stakeholder newStakeholder) {
		if (newStakeholder != stakeholder) {
			NotificationChain msgs = null;
			if (stakeholder != null)
				msgs = ((InternalEObject)stakeholder).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - MncModelPackage.MODEL__STAKEHOLDER, null, msgs);
			if (newStakeholder != null)
				msgs = ((InternalEObject)newStakeholder).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - MncModelPackage.MODEL__STAKEHOLDER, null, msgs);
			msgs = basicSetStakeholder(newStakeholder, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MncModelPackage.MODEL__STAKEHOLDER, newStakeholder, newStakeholder));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MncModelPackage.MODEL__SYSTEMS:
				return ((InternalEList<?>)getSystems()).basicRemove(otherEnd, msgs);
			case MncModelPackage.MODEL__IMPORT_SECTION:
				return ((InternalEList<?>)getImportSection()).basicRemove(otherEnd, msgs);
			case MncModelPackage.MODEL__SECURITY:
				return basicSetSecurity(null, msgs);
			case MncModelPackage.MODEL__STAKEHOLDER:
				return basicSetStakeholder(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MncModelPackage.MODEL__SYSTEMS:
				return getSystems();
			case MncModelPackage.MODEL__NAME:
				return getName();
			case MncModelPackage.MODEL__IMPORT_SECTION:
				return getImportSection();
			case MncModelPackage.MODEL__SECURITY:
				return getSecurity();
			case MncModelPackage.MODEL__STAKEHOLDER:
				return getStakeholder();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MncModelPackage.MODEL__SYSTEMS:
				getSystems().clear();
				getSystems().addAll((Collection<? extends mncModel.System>)newValue);
				return;
			case MncModelPackage.MODEL__NAME:
				setName((String)newValue);
				return;
			case MncModelPackage.MODEL__IMPORT_SECTION:
				getImportSection().clear();
				getImportSection().addAll((Collection<? extends Import>)newValue);
				return;
			case MncModelPackage.MODEL__SECURITY:
				setSecurity((Security)newValue);
				return;
			case MncModelPackage.MODEL__STAKEHOLDER:
				setStakeholder((Stakeholder)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MncModelPackage.MODEL__SYSTEMS:
				getSystems().clear();
				return;
			case MncModelPackage.MODEL__NAME:
				setName(NAME_EDEFAULT);
				return;
			case MncModelPackage.MODEL__IMPORT_SECTION:
				getImportSection().clear();
				return;
			case MncModelPackage.MODEL__SECURITY:
				setSecurity((Security)null);
				return;
			case MncModelPackage.MODEL__STAKEHOLDER:
				setStakeholder((Stakeholder)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MncModelPackage.MODEL__SYSTEMS:
				return systems != null && !systems.isEmpty();
			case MncModelPackage.MODEL__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case MncModelPackage.MODEL__IMPORT_SECTION:
				return importSection != null && !importSection.isEmpty();
			case MncModelPackage.MODEL__SECURITY:
				return security != null;
			case MncModelPackage.MODEL__STAKEHOLDER:
				return stakeholder != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(')');
		return result.toString();
	}

} //ModelImpl
