/**
 */
package mncModel.impl;

import java.util.Collection;

import mncModel.DataPointTranslation;
import mncModel.DataPointTranslationRule;
import mncModel.MncModelPackage;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Data Point Translation</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link mncModel.impl.DataPointTranslationImpl#getDataPointTranslationRules <em>Data Point Translation Rules</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DataPointTranslationImpl extends MinimalEObjectImpl.Container implements DataPointTranslation {
	/**
	 * The cached value of the '{@link #getDataPointTranslationRules() <em>Data Point Translation Rules</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDataPointTranslationRules()
	 * @generated
	 * @ordered
	 */
	protected EList<DataPointTranslationRule> dataPointTranslationRules;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DataPointTranslationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MncModelPackage.Literals.DATA_POINT_TRANSLATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DataPointTranslationRule> getDataPointTranslationRules() {
		if (dataPointTranslationRules == null) {
			dataPointTranslationRules = new EObjectContainmentEList<DataPointTranslationRule>(DataPointTranslationRule.class, this, MncModelPackage.DATA_POINT_TRANSLATION__DATA_POINT_TRANSLATION_RULES);
		}
		return dataPointTranslationRules;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MncModelPackage.DATA_POINT_TRANSLATION__DATA_POINT_TRANSLATION_RULES:
				return ((InternalEList<?>)getDataPointTranslationRules()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MncModelPackage.DATA_POINT_TRANSLATION__DATA_POINT_TRANSLATION_RULES:
				return getDataPointTranslationRules();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MncModelPackage.DATA_POINT_TRANSLATION__DATA_POINT_TRANSLATION_RULES:
				getDataPointTranslationRules().clear();
				getDataPointTranslationRules().addAll((Collection<? extends DataPointTranslationRule>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MncModelPackage.DATA_POINT_TRANSLATION__DATA_POINT_TRANSLATION_RULES:
				getDataPointTranslationRules().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MncModelPackage.DATA_POINT_TRANSLATION__DATA_POINT_TRANSLATION_RULES:
				return dataPointTranslationRules != null && !dataPointTranslationRules.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //DataPointTranslationImpl
