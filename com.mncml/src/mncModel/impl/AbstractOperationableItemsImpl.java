/**
 */
package mncModel.impl;

import mncModel.AbstractOperationableItems;
import mncModel.MncModelPackage;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Abstract Operationable Items</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class AbstractOperationableItemsImpl extends MinimalEObjectImpl.Container implements AbstractOperationableItems {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AbstractOperationableItemsImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MncModelPackage.Literals.ABSTRACT_OPERATIONABLE_ITEMS;
	}

} //AbstractOperationableItemsImpl
