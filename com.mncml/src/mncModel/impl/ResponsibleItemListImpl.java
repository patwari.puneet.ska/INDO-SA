/**
 */
package mncModel.impl;

import java.util.Collection;

import mncModel.Alarm;
import mncModel.Command;
import mncModel.DataPoint;
import mncModel.Event;
import mncModel.MncModelPackage;
import mncModel.ResponsibleItemList;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Responsible Item List</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link mncModel.impl.ResponsibleItemListImpl#getResponsibleCommands <em>Responsible Commands</em>}</li>
 *   <li>{@link mncModel.impl.ResponsibleItemListImpl#getResponsibleEvents <em>Responsible Events</em>}</li>
 *   <li>{@link mncModel.impl.ResponsibleItemListImpl#getResponsibleDataPoints <em>Responsible Data Points</em>}</li>
 *   <li>{@link mncModel.impl.ResponsibleItemListImpl#getResponsibleAlarms <em>Responsible Alarms</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ResponsibleItemListImpl extends MinimalEObjectImpl.Container implements ResponsibleItemList {
	/**
	 * The cached value of the '{@link #getResponsibleCommands() <em>Responsible Commands</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getResponsibleCommands()
	 * @generated
	 * @ordered
	 */
	protected EList<Command> responsibleCommands;

	/**
	 * The cached value of the '{@link #getResponsibleEvents() <em>Responsible Events</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getResponsibleEvents()
	 * @generated
	 * @ordered
	 */
	protected EList<Event> responsibleEvents;

	/**
	 * The cached value of the '{@link #getResponsibleDataPoints() <em>Responsible Data Points</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getResponsibleDataPoints()
	 * @generated
	 * @ordered
	 */
	protected EList<DataPoint> responsibleDataPoints;

	/**
	 * The cached value of the '{@link #getResponsibleAlarms() <em>Responsible Alarms</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getResponsibleAlarms()
	 * @generated
	 * @ordered
	 */
	protected EList<Alarm> responsibleAlarms;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ResponsibleItemListImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MncModelPackage.Literals.RESPONSIBLE_ITEM_LIST;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Command> getResponsibleCommands() {
		if (responsibleCommands == null) {
			responsibleCommands = new EObjectResolvingEList<Command>(Command.class, this, MncModelPackage.RESPONSIBLE_ITEM_LIST__RESPONSIBLE_COMMANDS);
		}
		return responsibleCommands;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Event> getResponsibleEvents() {
		if (responsibleEvents == null) {
			responsibleEvents = new EObjectResolvingEList<Event>(Event.class, this, MncModelPackage.RESPONSIBLE_ITEM_LIST__RESPONSIBLE_EVENTS);
		}
		return responsibleEvents;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DataPoint> getResponsibleDataPoints() {
		if (responsibleDataPoints == null) {
			responsibleDataPoints = new EObjectResolvingEList<DataPoint>(DataPoint.class, this, MncModelPackage.RESPONSIBLE_ITEM_LIST__RESPONSIBLE_DATA_POINTS);
		}
		return responsibleDataPoints;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Alarm> getResponsibleAlarms() {
		if (responsibleAlarms == null) {
			responsibleAlarms = new EObjectResolvingEList<Alarm>(Alarm.class, this, MncModelPackage.RESPONSIBLE_ITEM_LIST__RESPONSIBLE_ALARMS);
		}
		return responsibleAlarms;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MncModelPackage.RESPONSIBLE_ITEM_LIST__RESPONSIBLE_COMMANDS:
				return getResponsibleCommands();
			case MncModelPackage.RESPONSIBLE_ITEM_LIST__RESPONSIBLE_EVENTS:
				return getResponsibleEvents();
			case MncModelPackage.RESPONSIBLE_ITEM_LIST__RESPONSIBLE_DATA_POINTS:
				return getResponsibleDataPoints();
			case MncModelPackage.RESPONSIBLE_ITEM_LIST__RESPONSIBLE_ALARMS:
				return getResponsibleAlarms();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MncModelPackage.RESPONSIBLE_ITEM_LIST__RESPONSIBLE_COMMANDS:
				getResponsibleCommands().clear();
				getResponsibleCommands().addAll((Collection<? extends Command>)newValue);
				return;
			case MncModelPackage.RESPONSIBLE_ITEM_LIST__RESPONSIBLE_EVENTS:
				getResponsibleEvents().clear();
				getResponsibleEvents().addAll((Collection<? extends Event>)newValue);
				return;
			case MncModelPackage.RESPONSIBLE_ITEM_LIST__RESPONSIBLE_DATA_POINTS:
				getResponsibleDataPoints().clear();
				getResponsibleDataPoints().addAll((Collection<? extends DataPoint>)newValue);
				return;
			case MncModelPackage.RESPONSIBLE_ITEM_LIST__RESPONSIBLE_ALARMS:
				getResponsibleAlarms().clear();
				getResponsibleAlarms().addAll((Collection<? extends Alarm>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MncModelPackage.RESPONSIBLE_ITEM_LIST__RESPONSIBLE_COMMANDS:
				getResponsibleCommands().clear();
				return;
			case MncModelPackage.RESPONSIBLE_ITEM_LIST__RESPONSIBLE_EVENTS:
				getResponsibleEvents().clear();
				return;
			case MncModelPackage.RESPONSIBLE_ITEM_LIST__RESPONSIBLE_DATA_POINTS:
				getResponsibleDataPoints().clear();
				return;
			case MncModelPackage.RESPONSIBLE_ITEM_LIST__RESPONSIBLE_ALARMS:
				getResponsibleAlarms().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MncModelPackage.RESPONSIBLE_ITEM_LIST__RESPONSIBLE_COMMANDS:
				return responsibleCommands != null && !responsibleCommands.isEmpty();
			case MncModelPackage.RESPONSIBLE_ITEM_LIST__RESPONSIBLE_EVENTS:
				return responsibleEvents != null && !responsibleEvents.isEmpty();
			case MncModelPackage.RESPONSIBLE_ITEM_LIST__RESPONSIBLE_DATA_POINTS:
				return responsibleDataPoints != null && !responsibleDataPoints.isEmpty();
			case MncModelPackage.RESPONSIBLE_ITEM_LIST__RESPONSIBLE_ALARMS:
				return responsibleAlarms != null && !responsibleAlarms.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //ResponsibleItemListImpl
