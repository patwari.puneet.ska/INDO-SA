/**
 */
package mncModel.impl;

import mncModel.Action;
import mncModel.DataPointHandling;
import mncModel.DataPointValidCondition;
import mncModel.MncModelPackage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Data Point Handling</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link mncModel.impl.DataPointHandlingImpl#getCheckDataPoint <em>Check Data Point</em>}</li>
 *   <li>{@link mncModel.impl.DataPointHandlingImpl#getAction <em>Action</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DataPointHandlingImpl extends MinimalEObjectImpl.Container implements DataPointHandling {
	/**
	 * The cached value of the '{@link #getCheckDataPoint() <em>Check Data Point</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCheckDataPoint()
	 * @generated
	 * @ordered
	 */
	protected DataPointValidCondition checkDataPoint;

	/**
	 * The cached value of the '{@link #getAction() <em>Action</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAction()
	 * @generated
	 * @ordered
	 */
	protected Action action;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DataPointHandlingImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MncModelPackage.Literals.DATA_POINT_HANDLING;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataPointValidCondition getCheckDataPoint() {
		return checkDataPoint;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCheckDataPoint(DataPointValidCondition newCheckDataPoint, NotificationChain msgs) {
		DataPointValidCondition oldCheckDataPoint = checkDataPoint;
		checkDataPoint = newCheckDataPoint;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MncModelPackage.DATA_POINT_HANDLING__CHECK_DATA_POINT, oldCheckDataPoint, newCheckDataPoint);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCheckDataPoint(DataPointValidCondition newCheckDataPoint) {
		if (newCheckDataPoint != checkDataPoint) {
			NotificationChain msgs = null;
			if (checkDataPoint != null)
				msgs = ((InternalEObject)checkDataPoint).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - MncModelPackage.DATA_POINT_HANDLING__CHECK_DATA_POINT, null, msgs);
			if (newCheckDataPoint != null)
				msgs = ((InternalEObject)newCheckDataPoint).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - MncModelPackage.DATA_POINT_HANDLING__CHECK_DATA_POINT, null, msgs);
			msgs = basicSetCheckDataPoint(newCheckDataPoint, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MncModelPackage.DATA_POINT_HANDLING__CHECK_DATA_POINT, newCheckDataPoint, newCheckDataPoint));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Action getAction() {
		return action;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAction(Action newAction, NotificationChain msgs) {
		Action oldAction = action;
		action = newAction;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MncModelPackage.DATA_POINT_HANDLING__ACTION, oldAction, newAction);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAction(Action newAction) {
		if (newAction != action) {
			NotificationChain msgs = null;
			if (action != null)
				msgs = ((InternalEObject)action).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - MncModelPackage.DATA_POINT_HANDLING__ACTION, null, msgs);
			if (newAction != null)
				msgs = ((InternalEObject)newAction).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - MncModelPackage.DATA_POINT_HANDLING__ACTION, null, msgs);
			msgs = basicSetAction(newAction, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MncModelPackage.DATA_POINT_HANDLING__ACTION, newAction, newAction));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MncModelPackage.DATA_POINT_HANDLING__CHECK_DATA_POINT:
				return basicSetCheckDataPoint(null, msgs);
			case MncModelPackage.DATA_POINT_HANDLING__ACTION:
				return basicSetAction(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MncModelPackage.DATA_POINT_HANDLING__CHECK_DATA_POINT:
				return getCheckDataPoint();
			case MncModelPackage.DATA_POINT_HANDLING__ACTION:
				return getAction();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MncModelPackage.DATA_POINT_HANDLING__CHECK_DATA_POINT:
				setCheckDataPoint((DataPointValidCondition)newValue);
				return;
			case MncModelPackage.DATA_POINT_HANDLING__ACTION:
				setAction((Action)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MncModelPackage.DATA_POINT_HANDLING__CHECK_DATA_POINT:
				setCheckDataPoint((DataPointValidCondition)null);
				return;
			case MncModelPackage.DATA_POINT_HANDLING__ACTION:
				setAction((Action)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MncModelPackage.DATA_POINT_HANDLING__CHECK_DATA_POINT:
				return checkDataPoint != null;
			case MncModelPackage.DATA_POINT_HANDLING__ACTION:
				return action != null;
		}
		return super.eIsSet(featureID);
	}

} //DataPointHandlingImpl
