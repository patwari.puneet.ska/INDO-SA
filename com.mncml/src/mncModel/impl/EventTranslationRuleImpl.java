/**
 */
package mncModel.impl;

import java.util.Collection;

import mncModel.Event;
import mncModel.EventTranslationRule;
import mncModel.MncModelPackage;
import mncModel.Operation;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Event Translation Rule</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link mncModel.impl.EventTranslationRuleImpl#getEvents <em>Events</em>}</li>
 *   <li>{@link mncModel.impl.EventTranslationRuleImpl#getTranslatedEvent <em>Translated Event</em>}</li>
 *   <li>{@link mncModel.impl.EventTranslationRuleImpl#getOperation <em>Operation</em>}</li>
 * </ul>
 *
 * @generated
 */
public class EventTranslationRuleImpl extends MinimalEObjectImpl.Container implements EventTranslationRule {
	/**
	 * The cached value of the '{@link #getEvents() <em>Events</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEvents()
	 * @generated
	 * @ordered
	 */
	protected EList<Event> events;

	/**
	 * The cached value of the '{@link #getTranslatedEvent() <em>Translated Event</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTranslatedEvent()
	 * @generated
	 * @ordered
	 */
	protected Event translatedEvent;

	/**
	 * The cached value of the '{@link #getOperation() <em>Operation</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperation()
	 * @generated
	 * @ordered
	 */
	protected Operation operation;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EventTranslationRuleImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MncModelPackage.Literals.EVENT_TRANSLATION_RULE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Event> getEvents() {
		if (events == null) {
			events = new EObjectResolvingEList<Event>(Event.class, this, MncModelPackage.EVENT_TRANSLATION_RULE__EVENTS);
		}
		return events;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Event getTranslatedEvent() {
		if (translatedEvent != null && translatedEvent.eIsProxy()) {
			InternalEObject oldTranslatedEvent = (InternalEObject)translatedEvent;
			translatedEvent = (Event)eResolveProxy(oldTranslatedEvent);
			if (translatedEvent != oldTranslatedEvent) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, MncModelPackage.EVENT_TRANSLATION_RULE__TRANSLATED_EVENT, oldTranslatedEvent, translatedEvent));
			}
		}
		return translatedEvent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Event basicGetTranslatedEvent() {
		return translatedEvent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTranslatedEvent(Event newTranslatedEvent) {
		Event oldTranslatedEvent = translatedEvent;
		translatedEvent = newTranslatedEvent;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MncModelPackage.EVENT_TRANSLATION_RULE__TRANSLATED_EVENT, oldTranslatedEvent, translatedEvent));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Operation getOperation() {
		return operation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetOperation(Operation newOperation, NotificationChain msgs) {
		Operation oldOperation = operation;
		operation = newOperation;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MncModelPackage.EVENT_TRANSLATION_RULE__OPERATION, oldOperation, newOperation);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOperation(Operation newOperation) {
		if (newOperation != operation) {
			NotificationChain msgs = null;
			if (operation != null)
				msgs = ((InternalEObject)operation).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - MncModelPackage.EVENT_TRANSLATION_RULE__OPERATION, null, msgs);
			if (newOperation != null)
				msgs = ((InternalEObject)newOperation).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - MncModelPackage.EVENT_TRANSLATION_RULE__OPERATION, null, msgs);
			msgs = basicSetOperation(newOperation, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MncModelPackage.EVENT_TRANSLATION_RULE__OPERATION, newOperation, newOperation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MncModelPackage.EVENT_TRANSLATION_RULE__OPERATION:
				return basicSetOperation(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MncModelPackage.EVENT_TRANSLATION_RULE__EVENTS:
				return getEvents();
			case MncModelPackage.EVENT_TRANSLATION_RULE__TRANSLATED_EVENT:
				if (resolve) return getTranslatedEvent();
				return basicGetTranslatedEvent();
			case MncModelPackage.EVENT_TRANSLATION_RULE__OPERATION:
				return getOperation();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MncModelPackage.EVENT_TRANSLATION_RULE__EVENTS:
				getEvents().clear();
				getEvents().addAll((Collection<? extends Event>)newValue);
				return;
			case MncModelPackage.EVENT_TRANSLATION_RULE__TRANSLATED_EVENT:
				setTranslatedEvent((Event)newValue);
				return;
			case MncModelPackage.EVENT_TRANSLATION_RULE__OPERATION:
				setOperation((Operation)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MncModelPackage.EVENT_TRANSLATION_RULE__EVENTS:
				getEvents().clear();
				return;
			case MncModelPackage.EVENT_TRANSLATION_RULE__TRANSLATED_EVENT:
				setTranslatedEvent((Event)null);
				return;
			case MncModelPackage.EVENT_TRANSLATION_RULE__OPERATION:
				setOperation((Operation)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MncModelPackage.EVENT_TRANSLATION_RULE__EVENTS:
				return events != null && !events.isEmpty();
			case MncModelPackage.EVENT_TRANSLATION_RULE__TRANSLATED_EVENT:
				return translatedEvent != null;
			case MncModelPackage.EVENT_TRANSLATION_RULE__OPERATION:
				return operation != null;
		}
		return super.eIsSet(featureID);
	}

} //EventTranslationRuleImpl
