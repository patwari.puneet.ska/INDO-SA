/**
 */
package mncModel.utility;

import mncModel.AlarmTriggerCondition;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Alarm Condition Utility</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link mncModel.utility.AlarmConditionUtility#getAlarmConditions <em>Alarm Conditions</em>}</li>
 * </ul>
 *
 * @see mncModel.utility.UtilityPackage#getAlarmConditionUtility()
 * @model
 * @generated
 */
public interface AlarmConditionUtility extends EObject {
	/**
	 * Returns the value of the '<em><b>Alarm Conditions</b></em>' containment reference list.
	 * The list contents are of type {@link mncModel.AlarmTriggerCondition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Alarm Conditions</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Alarm Conditions</em>' containment reference list.
	 * @see mncModel.utility.UtilityPackage#getAlarmConditionUtility_AlarmConditions()
	 * @model containment="true"
	 * @generated
	 */
	EList<AlarmTriggerCondition> getAlarmConditions();

} // AlarmConditionUtility
