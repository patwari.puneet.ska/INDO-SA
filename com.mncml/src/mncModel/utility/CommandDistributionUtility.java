/**
 */
package mncModel.utility;

import mncModel.CommandDistribution;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Command Distribution Utility</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link mncModel.utility.CommandDistributionUtility#getCommandDistributions <em>Command Distributions</em>}</li>
 * </ul>
 *
 * @see mncModel.utility.UtilityPackage#getCommandDistributionUtility()
 * @model
 * @generated
 */
public interface CommandDistributionUtility extends EObject {
	/**
	 * Returns the value of the '<em><b>Command Distributions</b></em>' containment reference list.
	 * The list contents are of type {@link mncModel.CommandDistribution}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Command Distributions</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Command Distributions</em>' containment reference list.
	 * @see mncModel.utility.UtilityPackage#getCommandDistributionUtility_CommandDistributions()
	 * @model containment="true"
	 * @generated
	 */
	EList<CommandDistribution> getCommandDistributions();

} // CommandDistributionUtility
