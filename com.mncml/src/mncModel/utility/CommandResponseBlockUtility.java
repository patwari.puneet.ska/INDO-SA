/**
 */
package mncModel.utility;

import mncModel.CommandResponseBlock;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Command Response Block Utility</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link mncModel.utility.CommandResponseBlockUtility#getCommandResponseBlocks <em>Command Response Blocks</em>}</li>
 * </ul>
 *
 * @see mncModel.utility.UtilityPackage#getCommandResponseBlockUtility()
 * @model
 * @generated
 */
public interface CommandResponseBlockUtility extends EObject {
	/**
	 * Returns the value of the '<em><b>Command Response Blocks</b></em>' containment reference list.
	 * The list contents are of type {@link mncModel.CommandResponseBlock}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Command Response Blocks</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Command Response Blocks</em>' containment reference list.
	 * @see mncModel.utility.UtilityPackage#getCommandResponseBlockUtility_CommandResponseBlocks()
	 * @model containment="true"
	 * @generated
	 */
	EList<CommandResponseBlock> getCommandResponseBlocks();

} // CommandResponseBlockUtility
