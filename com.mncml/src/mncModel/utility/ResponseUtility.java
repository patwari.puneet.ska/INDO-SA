/**
 */
package mncModel.utility;

import mncModel.Response;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Response Utility</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link mncModel.utility.ResponseUtility#getResponses <em>Responses</em>}</li>
 * </ul>
 *
 * @see mncModel.utility.UtilityPackage#getResponseUtility()
 * @model
 * @generated
 */
public interface ResponseUtility extends EObject {
	/**
	 * Returns the value of the '<em><b>Responses</b></em>' containment reference list.
	 * The list contents are of type {@link mncModel.Response}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Responses</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Responses</em>' containment reference list.
	 * @see mncModel.utility.UtilityPackage#getResponseUtility_Responses()
	 * @model containment="true"
	 * @generated
	 */
	EList<Response> getResponses();

} // ResponseUtility
