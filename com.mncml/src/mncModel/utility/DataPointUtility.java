/**
 */
package mncModel.utility;

import mncModel.DataPoint;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Data Point Utility</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link mncModel.utility.DataPointUtility#getDataPoints <em>Data Points</em>}</li>
 * </ul>
 *
 * @see mncModel.utility.UtilityPackage#getDataPointUtility()
 * @model
 * @generated
 */
public interface DataPointUtility extends EObject {
	/**
	 * Returns the value of the '<em><b>Data Points</b></em>' containment reference list.
	 * The list contents are of type {@link mncModel.DataPoint}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Data Points</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data Points</em>' containment reference list.
	 * @see mncModel.utility.UtilityPackage#getDataPointUtility_DataPoints()
	 * @model containment="true"
	 * @generated
	 */
	EList<DataPoint> getDataPoints();

} // DataPointUtility
