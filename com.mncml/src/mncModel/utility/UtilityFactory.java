/**
 */
package mncModel.utility;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see mncModel.utility.UtilityPackage
 * @generated
 */
public interface UtilityFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	UtilityFactory eINSTANCE = mncModel.utility.impl.UtilityFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Command Validation Utility</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Command Validation Utility</em>'.
	 * @generated
	 */
	CommandValidationUtility createCommandValidationUtility();

	/**
	 * Returns a new object of class '<em>Alarm Handling Utility</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Alarm Handling Utility</em>'.
	 * @generated
	 */
	AlarmHandlingUtility createAlarmHandlingUtility();

	/**
	 * Returns a new object of class '<em>Data Point Handling Utility</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Point Handling Utility</em>'.
	 * @generated
	 */
	DataPointHandlingUtility createDataPointHandlingUtility();

	/**
	 * Returns a new object of class '<em>Operation Utility</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Operation Utility</em>'.
	 * @generated
	 */
	OperationUtility createOperationUtility();

	/**
	 * Returns a new object of class '<em>Command Utility</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Command Utility</em>'.
	 * @generated
	 */
	CommandUtility createCommandUtility();

	/**
	 * Returns a new object of class '<em>Response Validation Utility</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Response Validation Utility</em>'.
	 * @generated
	 */
	ResponseValidationUtility createResponseValidationUtility();

	/**
	 * Returns a new object of class '<em>Alarm Condition Utility</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Alarm Condition Utility</em>'.
	 * @generated
	 */
	AlarmConditionUtility createAlarmConditionUtility();

	/**
	 * Returns a new object of class '<em>Data Point Utility</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Point Utility</em>'.
	 * @generated
	 */
	DataPointUtility createDataPointUtility();

	/**
	 * Returns a new object of class '<em>Alarm Utility</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Alarm Utility</em>'.
	 * @generated
	 */
	AlarmUtility createAlarmUtility();

	/**
	 * Returns a new object of class '<em>Event Utility</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Event Utility</em>'.
	 * @generated
	 */
	EventUtility createEventUtility();

	/**
	 * Returns a new object of class '<em>Response Utility</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Response Utility</em>'.
	 * @generated
	 */
	ResponseUtility createResponseUtility();

	/**
	 * Returns a new object of class '<em>State Utility</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>State Utility</em>'.
	 * @generated
	 */
	StateUtility createStateUtility();

	/**
	 * Returns a new object of class '<em>Authorisation Utility</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Authorisation Utility</em>'.
	 * @generated
	 */
	AuthorisationUtility createAuthorisationUtility();

	/**
	 * Returns a new object of class '<em>Role Utility</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Role Utility</em>'.
	 * @generated
	 */
	RoleUtility createRoleUtility();

	/**
	 * Returns a new object of class '<em>User Utility</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>User Utility</em>'.
	 * @generated
	 */
	UserUtility createUserUtility();

	/**
	 * Returns a new object of class '<em>Transition Utility</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Transition Utility</em>'.
	 * @generated
	 */
	TransitionUtility createTransitionUtility();

	/**
	 * Returns a new object of class '<em>Command Response Block Utility</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Command Response Block Utility</em>'.
	 * @generated
	 */
	CommandResponseBlockUtility createCommandResponseBlockUtility();

	/**
	 * Returns a new object of class '<em>Response Block Utility</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Response Block Utility</em>'.
	 * @generated
	 */
	ResponseBlockUtility createResponseBlockUtility();

	/**
	 * Returns a new object of class '<em>Event Block Utility</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Event Block Utility</em>'.
	 * @generated
	 */
	EventBlockUtility createEventBlockUtility();

	/**
	 * Returns a new object of class '<em>Alarm Block Utility</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Alarm Block Utility</em>'.
	 * @generated
	 */
	AlarmBlockUtility createAlarmBlockUtility();

	/**
	 * Returns a new object of class '<em>Data Point Block Utility</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Point Block Utility</em>'.
	 * @generated
	 */
	DataPointBlockUtility createDataPointBlockUtility();

	/**
	 * Returns a new object of class '<em>Command Distribution Utility</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Command Distribution Utility</em>'.
	 * @generated
	 */
	CommandDistributionUtility createCommandDistributionUtility();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	UtilityPackage getUtilityPackage();

} //UtilityFactory
