/**
 */
package mncModel.utility;

import mncModel.Event;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Event Utility</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link mncModel.utility.EventUtility#getEvents <em>Events</em>}</li>
 * </ul>
 *
 * @see mncModel.utility.UtilityPackage#getEventUtility()
 * @model
 * @generated
 */
public interface EventUtility extends EObject {
	/**
	 * Returns the value of the '<em><b>Events</b></em>' containment reference list.
	 * The list contents are of type {@link mncModel.Event}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Events</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Events</em>' containment reference list.
	 * @see mncModel.utility.UtilityPackage#getEventUtility_Events()
	 * @model containment="true"
	 * @generated
	 */
	EList<Event> getEvents();

} // EventUtility
