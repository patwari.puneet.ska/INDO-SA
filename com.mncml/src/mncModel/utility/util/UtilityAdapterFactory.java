/**
 */
package mncModel.utility.util;

import mncModel.utility.*;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see mncModel.utility.UtilityPackage
 * @generated
 */
public class UtilityAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static UtilityPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UtilityAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = UtilityPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UtilitySwitch<Adapter> modelSwitch =
		new UtilitySwitch<Adapter>() {
			@Override
			public Adapter caseCommandValidationUtility(CommandValidationUtility object) {
				return createCommandValidationUtilityAdapter();
			}
			@Override
			public Adapter caseAlarmHandlingUtility(AlarmHandlingUtility object) {
				return createAlarmHandlingUtilityAdapter();
			}
			@Override
			public Adapter caseDataPointHandlingUtility(DataPointHandlingUtility object) {
				return createDataPointHandlingUtilityAdapter();
			}
			@Override
			public Adapter caseOperationUtility(OperationUtility object) {
				return createOperationUtilityAdapter();
			}
			@Override
			public Adapter caseCommandUtility(CommandUtility object) {
				return createCommandUtilityAdapter();
			}
			@Override
			public Adapter caseResponseValidationUtility(ResponseValidationUtility object) {
				return createResponseValidationUtilityAdapter();
			}
			@Override
			public Adapter caseAlarmConditionUtility(AlarmConditionUtility object) {
				return createAlarmConditionUtilityAdapter();
			}
			@Override
			public Adapter caseDataPointUtility(DataPointUtility object) {
				return createDataPointUtilityAdapter();
			}
			@Override
			public Adapter caseAlarmUtility(AlarmUtility object) {
				return createAlarmUtilityAdapter();
			}
			@Override
			public Adapter caseEventUtility(EventUtility object) {
				return createEventUtilityAdapter();
			}
			@Override
			public Adapter caseResponseUtility(ResponseUtility object) {
				return createResponseUtilityAdapter();
			}
			@Override
			public Adapter caseStateUtility(StateUtility object) {
				return createStateUtilityAdapter();
			}
			@Override
			public Adapter caseAuthorisationUtility(AuthorisationUtility object) {
				return createAuthorisationUtilityAdapter();
			}
			@Override
			public Adapter caseRoleUtility(RoleUtility object) {
				return createRoleUtilityAdapter();
			}
			@Override
			public Adapter caseUserUtility(UserUtility object) {
				return createUserUtilityAdapter();
			}
			@Override
			public Adapter caseTransitionUtility(TransitionUtility object) {
				return createTransitionUtilityAdapter();
			}
			@Override
			public Adapter caseCommandResponseBlockUtility(CommandResponseBlockUtility object) {
				return createCommandResponseBlockUtilityAdapter();
			}
			@Override
			public Adapter caseResponseBlockUtility(ResponseBlockUtility object) {
				return createResponseBlockUtilityAdapter();
			}
			@Override
			public Adapter caseEventBlockUtility(EventBlockUtility object) {
				return createEventBlockUtilityAdapter();
			}
			@Override
			public Adapter caseAlarmBlockUtility(AlarmBlockUtility object) {
				return createAlarmBlockUtilityAdapter();
			}
			@Override
			public Adapter caseDataPointBlockUtility(DataPointBlockUtility object) {
				return createDataPointBlockUtilityAdapter();
			}
			@Override
			public Adapter caseCommandDistributionUtility(CommandDistributionUtility object) {
				return createCommandDistributionUtilityAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link mncModel.utility.CommandValidationUtility <em>Command Validation Utility</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see mncModel.utility.CommandValidationUtility
	 * @generated
	 */
	public Adapter createCommandValidationUtilityAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link mncModel.utility.AlarmHandlingUtility <em>Alarm Handling Utility</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see mncModel.utility.AlarmHandlingUtility
	 * @generated
	 */
	public Adapter createAlarmHandlingUtilityAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link mncModel.utility.DataPointHandlingUtility <em>Data Point Handling Utility</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see mncModel.utility.DataPointHandlingUtility
	 * @generated
	 */
	public Adapter createDataPointHandlingUtilityAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link mncModel.utility.OperationUtility <em>Operation Utility</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see mncModel.utility.OperationUtility
	 * @generated
	 */
	public Adapter createOperationUtilityAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link mncModel.utility.CommandUtility <em>Command Utility</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see mncModel.utility.CommandUtility
	 * @generated
	 */
	public Adapter createCommandUtilityAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link mncModel.utility.ResponseValidationUtility <em>Response Validation Utility</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see mncModel.utility.ResponseValidationUtility
	 * @generated
	 */
	public Adapter createResponseValidationUtilityAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link mncModel.utility.AlarmConditionUtility <em>Alarm Condition Utility</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see mncModel.utility.AlarmConditionUtility
	 * @generated
	 */
	public Adapter createAlarmConditionUtilityAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link mncModel.utility.DataPointUtility <em>Data Point Utility</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see mncModel.utility.DataPointUtility
	 * @generated
	 */
	public Adapter createDataPointUtilityAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link mncModel.utility.AlarmUtility <em>Alarm Utility</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see mncModel.utility.AlarmUtility
	 * @generated
	 */
	public Adapter createAlarmUtilityAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link mncModel.utility.EventUtility <em>Event Utility</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see mncModel.utility.EventUtility
	 * @generated
	 */
	public Adapter createEventUtilityAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link mncModel.utility.ResponseUtility <em>Response Utility</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see mncModel.utility.ResponseUtility
	 * @generated
	 */
	public Adapter createResponseUtilityAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link mncModel.utility.StateUtility <em>State Utility</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see mncModel.utility.StateUtility
	 * @generated
	 */
	public Adapter createStateUtilityAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link mncModel.utility.AuthorisationUtility <em>Authorisation Utility</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see mncModel.utility.AuthorisationUtility
	 * @generated
	 */
	public Adapter createAuthorisationUtilityAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link mncModel.utility.RoleUtility <em>Role Utility</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see mncModel.utility.RoleUtility
	 * @generated
	 */
	public Adapter createRoleUtilityAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link mncModel.utility.UserUtility <em>User Utility</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see mncModel.utility.UserUtility
	 * @generated
	 */
	public Adapter createUserUtilityAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link mncModel.utility.TransitionUtility <em>Transition Utility</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see mncModel.utility.TransitionUtility
	 * @generated
	 */
	public Adapter createTransitionUtilityAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link mncModel.utility.CommandResponseBlockUtility <em>Command Response Block Utility</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see mncModel.utility.CommandResponseBlockUtility
	 * @generated
	 */
	public Adapter createCommandResponseBlockUtilityAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link mncModel.utility.ResponseBlockUtility <em>Response Block Utility</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see mncModel.utility.ResponseBlockUtility
	 * @generated
	 */
	public Adapter createResponseBlockUtilityAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link mncModel.utility.EventBlockUtility <em>Event Block Utility</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see mncModel.utility.EventBlockUtility
	 * @generated
	 */
	public Adapter createEventBlockUtilityAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link mncModel.utility.AlarmBlockUtility <em>Alarm Block Utility</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see mncModel.utility.AlarmBlockUtility
	 * @generated
	 */
	public Adapter createAlarmBlockUtilityAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link mncModel.utility.DataPointBlockUtility <em>Data Point Block Utility</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see mncModel.utility.DataPointBlockUtility
	 * @generated
	 */
	public Adapter createDataPointBlockUtilityAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link mncModel.utility.CommandDistributionUtility <em>Command Distribution Utility</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see mncModel.utility.CommandDistributionUtility
	 * @generated
	 */
	public Adapter createCommandDistributionUtilityAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //UtilityAdapterFactory
