/**
 */
package mncModel.utility;

import mncModel.OperatingState;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>State Utility</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link mncModel.utility.StateUtility#getOperatingStates <em>Operating States</em>}</li>
 *   <li>{@link mncModel.utility.StateUtility#getStartState <em>Start State</em>}</li>
 *   <li>{@link mncModel.utility.StateUtility#getEndState <em>End State</em>}</li>
 * </ul>
 *
 * @see mncModel.utility.UtilityPackage#getStateUtility()
 * @model
 * @generated
 */
public interface StateUtility extends EObject {
	/**
	 * Returns the value of the '<em><b>Operating States</b></em>' containment reference list.
	 * The list contents are of type {@link mncModel.OperatingState}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operating States</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operating States</em>' containment reference list.
	 * @see mncModel.utility.UtilityPackage#getStateUtility_OperatingStates()
	 * @model containment="true"
	 * @generated
	 */
	EList<OperatingState> getOperatingStates();

	/**
	 * Returns the value of the '<em><b>Start State</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Start State</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Start State</em>' reference.
	 * @see #setStartState(OperatingState)
	 * @see mncModel.utility.UtilityPackage#getStateUtility_StartState()
	 * @model
	 * @generated
	 */
	OperatingState getStartState();

	/**
	 * Sets the value of the '{@link mncModel.utility.StateUtility#getStartState <em>Start State</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Start State</em>' reference.
	 * @see #getStartState()
	 * @generated
	 */
	void setStartState(OperatingState value);

	/**
	 * Returns the value of the '<em><b>End State</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>End State</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>End State</em>' reference.
	 * @see #setEndState(OperatingState)
	 * @see mncModel.utility.UtilityPackage#getStateUtility_EndState()
	 * @model
	 * @generated
	 */
	OperatingState getEndState();

	/**
	 * Sets the value of the '{@link mncModel.utility.StateUtility#getEndState <em>End State</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>End State</em>' reference.
	 * @see #getEndState()
	 * @generated
	 */
	void setEndState(OperatingState value);

} // StateUtility
