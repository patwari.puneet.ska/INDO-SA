/**
 */
package mncModel.utility.impl;

import java.util.Collection;

import mncModel.CommandDistribution;

import mncModel.utility.CommandDistributionUtility;
import mncModel.utility.UtilityPackage;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Command Distribution Utility</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link mncModel.utility.impl.CommandDistributionUtilityImpl#getCommandDistributions <em>Command Distributions</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CommandDistributionUtilityImpl extends MinimalEObjectImpl.Container implements CommandDistributionUtility {
	/**
	 * The cached value of the '{@link #getCommandDistributions() <em>Command Distributions</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCommandDistributions()
	 * @generated
	 * @ordered
	 */
	protected EList<CommandDistribution> commandDistributions;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CommandDistributionUtilityImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return UtilityPackage.Literals.COMMAND_DISTRIBUTION_UTILITY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<CommandDistribution> getCommandDistributions() {
		if (commandDistributions == null) {
			commandDistributions = new EObjectContainmentEList<CommandDistribution>(CommandDistribution.class, this, UtilityPackage.COMMAND_DISTRIBUTION_UTILITY__COMMAND_DISTRIBUTIONS);
		}
		return commandDistributions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case UtilityPackage.COMMAND_DISTRIBUTION_UTILITY__COMMAND_DISTRIBUTIONS:
				return ((InternalEList<?>)getCommandDistributions()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case UtilityPackage.COMMAND_DISTRIBUTION_UTILITY__COMMAND_DISTRIBUTIONS:
				return getCommandDistributions();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case UtilityPackage.COMMAND_DISTRIBUTION_UTILITY__COMMAND_DISTRIBUTIONS:
				getCommandDistributions().clear();
				getCommandDistributions().addAll((Collection<? extends CommandDistribution>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case UtilityPackage.COMMAND_DISTRIBUTION_UTILITY__COMMAND_DISTRIBUTIONS:
				getCommandDistributions().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case UtilityPackage.COMMAND_DISTRIBUTION_UTILITY__COMMAND_DISTRIBUTIONS:
				return commandDistributions != null && !commandDistributions.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //CommandDistributionUtilityImpl
