/**
 */
package mncModel.utility.impl;

import java.util.Collection;

import mncModel.CommandResponseBlock;

import mncModel.utility.CommandResponseBlockUtility;
import mncModel.utility.UtilityPackage;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Command Response Block Utility</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link mncModel.utility.impl.CommandResponseBlockUtilityImpl#getCommandResponseBlocks <em>Command Response Blocks</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CommandResponseBlockUtilityImpl extends MinimalEObjectImpl.Container implements CommandResponseBlockUtility {
	/**
	 * The cached value of the '{@link #getCommandResponseBlocks() <em>Command Response Blocks</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCommandResponseBlocks()
	 * @generated
	 * @ordered
	 */
	protected EList<CommandResponseBlock> commandResponseBlocks;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CommandResponseBlockUtilityImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return UtilityPackage.Literals.COMMAND_RESPONSE_BLOCK_UTILITY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<CommandResponseBlock> getCommandResponseBlocks() {
		if (commandResponseBlocks == null) {
			commandResponseBlocks = new EObjectContainmentEList<CommandResponseBlock>(CommandResponseBlock.class, this, UtilityPackage.COMMAND_RESPONSE_BLOCK_UTILITY__COMMAND_RESPONSE_BLOCKS);
		}
		return commandResponseBlocks;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case UtilityPackage.COMMAND_RESPONSE_BLOCK_UTILITY__COMMAND_RESPONSE_BLOCKS:
				return ((InternalEList<?>)getCommandResponseBlocks()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case UtilityPackage.COMMAND_RESPONSE_BLOCK_UTILITY__COMMAND_RESPONSE_BLOCKS:
				return getCommandResponseBlocks();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case UtilityPackage.COMMAND_RESPONSE_BLOCK_UTILITY__COMMAND_RESPONSE_BLOCKS:
				getCommandResponseBlocks().clear();
				getCommandResponseBlocks().addAll((Collection<? extends CommandResponseBlock>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case UtilityPackage.COMMAND_RESPONSE_BLOCK_UTILITY__COMMAND_RESPONSE_BLOCKS:
				getCommandResponseBlocks().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case UtilityPackage.COMMAND_RESPONSE_BLOCK_UTILITY__COMMAND_RESPONSE_BLOCKS:
				return commandResponseBlocks != null && !commandResponseBlocks.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //CommandResponseBlockUtilityImpl
