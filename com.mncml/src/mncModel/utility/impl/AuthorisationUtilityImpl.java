/**
 */
package mncModel.utility.impl;

import java.util.Collection;

import mncModel.security.Authorisation;

import mncModel.utility.AuthorisationUtility;
import mncModel.utility.UtilityPackage;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Authorisation Utility</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link mncModel.utility.impl.AuthorisationUtilityImpl#getAuthorisations <em>Authorisations</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AuthorisationUtilityImpl extends MinimalEObjectImpl.Container implements AuthorisationUtility {
	/**
	 * The cached value of the '{@link #getAuthorisations() <em>Authorisations</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAuthorisations()
	 * @generated
	 * @ordered
	 */
	protected EList<Authorisation> authorisations;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AuthorisationUtilityImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return UtilityPackage.Literals.AUTHORISATION_UTILITY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Authorisation> getAuthorisations() {
		if (authorisations == null) {
			authorisations = new EObjectContainmentEList<Authorisation>(Authorisation.class, this, UtilityPackage.AUTHORISATION_UTILITY__AUTHORISATIONS);
		}
		return authorisations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case UtilityPackage.AUTHORISATION_UTILITY__AUTHORISATIONS:
				return ((InternalEList<?>)getAuthorisations()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case UtilityPackage.AUTHORISATION_UTILITY__AUTHORISATIONS:
				return getAuthorisations();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case UtilityPackage.AUTHORISATION_UTILITY__AUTHORISATIONS:
				getAuthorisations().clear();
				getAuthorisations().addAll((Collection<? extends Authorisation>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case UtilityPackage.AUTHORISATION_UTILITY__AUTHORISATIONS:
				getAuthorisations().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case UtilityPackage.AUTHORISATION_UTILITY__AUTHORISATIONS:
				return authorisations != null && !authorisations.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //AuthorisationUtilityImpl
