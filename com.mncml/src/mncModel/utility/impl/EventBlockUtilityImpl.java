/**
 */
package mncModel.utility.impl;

import java.util.Collection;

import mncModel.EventBlock;

import mncModel.utility.EventBlockUtility;
import mncModel.utility.UtilityPackage;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Event Block Utility</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link mncModel.utility.impl.EventBlockUtilityImpl#getEventBlocks <em>Event Blocks</em>}</li>
 * </ul>
 *
 * @generated
 */
public class EventBlockUtilityImpl extends MinimalEObjectImpl.Container implements EventBlockUtility {
	/**
	 * The cached value of the '{@link #getEventBlocks() <em>Event Blocks</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEventBlocks()
	 * @generated
	 * @ordered
	 */
	protected EList<EventBlock> eventBlocks;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EventBlockUtilityImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return UtilityPackage.Literals.EVENT_BLOCK_UTILITY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EventBlock> getEventBlocks() {
		if (eventBlocks == null) {
			eventBlocks = new EObjectContainmentEList<EventBlock>(EventBlock.class, this, UtilityPackage.EVENT_BLOCK_UTILITY__EVENT_BLOCKS);
		}
		return eventBlocks;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case UtilityPackage.EVENT_BLOCK_UTILITY__EVENT_BLOCKS:
				return ((InternalEList<?>)getEventBlocks()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case UtilityPackage.EVENT_BLOCK_UTILITY__EVENT_BLOCKS:
				return getEventBlocks();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case UtilityPackage.EVENT_BLOCK_UTILITY__EVENT_BLOCKS:
				getEventBlocks().clear();
				getEventBlocks().addAll((Collection<? extends EventBlock>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case UtilityPackage.EVENT_BLOCK_UTILITY__EVENT_BLOCKS:
				getEventBlocks().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case UtilityPackage.EVENT_BLOCK_UTILITY__EVENT_BLOCKS:
				return eventBlocks != null && !eventBlocks.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //EventBlockUtilityImpl
