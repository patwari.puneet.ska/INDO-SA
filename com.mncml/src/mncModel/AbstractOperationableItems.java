/**
 */
package mncModel;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Abstract Operationable Items</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see mncModel.MncModelPackage#getAbstractOperationableItems()
 * @model
 * @generated
 */
public interface AbstractOperationableItems extends EObject {
} // AbstractOperationableItems
