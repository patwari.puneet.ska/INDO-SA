/**
 */
package mncModel.util;

import mncModel.AbstractInterfaceItems;
import mncModel.AbstractOperationableItems;
import mncModel.Action;
import mncModel.Address;
import mncModel.Alarm;
import mncModel.AlarmBlock;
import mncModel.AlarmHandling;
import mncModel.AlarmTranslation;
import mncModel.AlarmTranslationRule;
import mncModel.AlarmTriggerCondition;
import mncModel.ArrayType;
import mncModel.BoolValue;
import mncModel.CheckParameterCondition;
import mncModel.Command;
import mncModel.CommandDistribution;
import mncModel.CommandResponseBlock;
import mncModel.CommandTranslation;
import mncModel.CommandTriggerCondition;
import mncModel.CommandValidation;
import mncModel.ControlNode;
import mncModel.DataPoint;
import mncModel.DataPointBlock;
import mncModel.DataPointHandling;
import mncModel.DataPointTranslation;
import mncModel.DataPointTranslationRule;
import mncModel.DataPointTriggerCondition;
import mncModel.DataPointValidCondition;
import mncModel.Event;
import mncModel.EventBlock;
import mncModel.EventHandling;
import mncModel.EventTranslation;
import mncModel.EventTranslationRule;
import mncModel.EventTriggerCondition;
import mncModel.FloatValue;
import mncModel.Import;
import mncModel.IntValue;
import mncModel.InterfaceDescription;
import mncModel.MncModelPackage;
import mncModel.Model;
import mncModel.OperatingState;
import mncModel.Operation;
import mncModel.Parameter;
import mncModel.ParameterTranslation;
import mncModel.Port;
import mncModel.PrimitiveValue;
import mncModel.Publish;
import mncModel.Response;
import mncModel.ResponseBlock;
import mncModel.ResponseDistribution;
import mncModel.ResponseTranslation;
import mncModel.ResponseTranslationRule;
import mncModel.ResponseValidation;
import mncModel.ResponsibleItemList;
import mncModel.SimpleType;
import mncModel.StringValue;
import mncModel.SubscribableItemList;
import mncModel.SubscribableItems;
import mncModel.Transition;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see mncModel.MncModelPackage
 * @generated
 */
public class MncModelSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static MncModelPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MncModelSwitch() {
		if (modelPackage == null) {
			modelPackage = MncModelPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case MncModelPackage.MODEL: {
				Model model = (Model)theEObject;
				T result = caseModel(model);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MncModelPackage.IMPORT: {
				Import import_ = (Import)theEObject;
				T result = caseImport(import_);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MncModelPackage.SYSTEM: {
				mncModel.System system = (mncModel.System)theEObject;
				T result = caseSystem(system);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MncModelPackage.CONTROL_NODE: {
				ControlNode controlNode = (ControlNode)theEObject;
				T result = caseControlNode(controlNode);
				if (result == null) result = caseSystem(controlNode);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MncModelPackage.INTERFACE_DESCRIPTION: {
				InterfaceDescription interfaceDescription = (InterfaceDescription)theEObject;
				T result = caseInterfaceDescription(interfaceDescription);
				if (result == null) result = caseSystem(interfaceDescription);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MncModelPackage.ABSTRACT_INTERFACE_ITEMS: {
				AbstractInterfaceItems abstractInterfaceItems = (AbstractInterfaceItems)theEObject;
				T result = caseAbstractInterfaceItems(abstractInterfaceItems);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MncModelPackage.ABSTRACT_OPERATIONABLE_ITEMS: {
				AbstractOperationableItems abstractOperationableItems = (AbstractOperationableItems)theEObject;
				T result = caseAbstractOperationableItems(abstractOperationableItems);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MncModelPackage.OPERATING_STATE: {
				OperatingState operatingState = (OperatingState)theEObject;
				T result = caseOperatingState(operatingState);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MncModelPackage.COMMAND_RESPONSE_BLOCK: {
				CommandResponseBlock commandResponseBlock = (CommandResponseBlock)theEObject;
				T result = caseCommandResponseBlock(commandResponseBlock);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MncModelPackage.COMMAND: {
				Command command = (Command)theEObject;
				T result = caseCommand(command);
				if (result == null) result = caseAbstractInterfaceItems(command);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MncModelPackage.COMMAND_VALIDATION: {
				CommandValidation commandValidation = (CommandValidation)theEObject;
				T result = caseCommandValidation(commandValidation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MncModelPackage.COMMAND_DISTRIBUTION: {
				CommandDistribution commandDistribution = (CommandDistribution)theEObject;
				T result = caseCommandDistribution(commandDistribution);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MncModelPackage.COMMAND_TRANSLATION: {
				CommandTranslation commandTranslation = (CommandTranslation)theEObject;
				T result = caseCommandTranslation(commandTranslation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MncModelPackage.COMMAND_TRIGGER_CONDITION: {
				CommandTriggerCondition commandTriggerCondition = (CommandTriggerCondition)theEObject;
				T result = caseCommandTriggerCondition(commandTriggerCondition);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MncModelPackage.PARAMETER_TRANSLATION: {
				ParameterTranslation parameterTranslation = (ParameterTranslation)theEObject;
				T result = caseParameterTranslation(parameterTranslation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MncModelPackage.TRANSITION: {
				Transition transition = (Transition)theEObject;
				T result = caseTransition(transition);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MncModelPackage.RESPONSE: {
				Response response = (Response)theEObject;
				T result = caseResponse(response);
				if (result == null) result = caseAbstractInterfaceItems(response);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MncModelPackage.RESPONSE_BLOCK: {
				ResponseBlock responseBlock = (ResponseBlock)theEObject;
				T result = caseResponseBlock(responseBlock);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MncModelPackage.RESPONSE_VALIDATION: {
				ResponseValidation responseValidation = (ResponseValidation)theEObject;
				T result = caseResponseValidation(responseValidation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MncModelPackage.RESPONSE_DISTRIBUTION: {
				ResponseDistribution responseDistribution = (ResponseDistribution)theEObject;
				T result = caseResponseDistribution(responseDistribution);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MncModelPackage.RESPONSE_TRANSLATION: {
				ResponseTranslation responseTranslation = (ResponseTranslation)theEObject;
				T result = caseResponseTranslation(responseTranslation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MncModelPackage.RESPONSE_TRANSLATION_RULE: {
				ResponseTranslationRule responseTranslationRule = (ResponseTranslationRule)theEObject;
				T result = caseResponseTranslationRule(responseTranslationRule);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MncModelPackage.PORT: {
				Port port = (Port)theEObject;
				T result = casePort(port);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MncModelPackage.EVENT_BLOCK: {
				EventBlock eventBlock = (EventBlock)theEObject;
				T result = caseEventBlock(eventBlock);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MncModelPackage.EVENT: {
				Event event = (Event)theEObject;
				T result = caseEvent(event);
				if (result == null) result = caseAbstractInterfaceItems(event);
				if (result == null) result = caseSubscribableItems(event);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MncModelPackage.EVENT_TRIGGER_CONDITION: {
				EventTriggerCondition eventTriggerCondition = (EventTriggerCondition)theEObject;
				T result = caseEventTriggerCondition(eventTriggerCondition);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MncModelPackage.EVENT_TRANSLATION: {
				EventTranslation eventTranslation = (EventTranslation)theEObject;
				T result = caseEventTranslation(eventTranslation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MncModelPackage.EVENT_TRANSLATION_RULE: {
				EventTranslationRule eventTranslationRule = (EventTranslationRule)theEObject;
				T result = caseEventTranslationRule(eventTranslationRule);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MncModelPackage.EVENT_HANDLING: {
				EventHandling eventHandling = (EventHandling)theEObject;
				T result = caseEventHandling(eventHandling);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MncModelPackage.ALARM_BLOCK: {
				AlarmBlock alarmBlock = (AlarmBlock)theEObject;
				T result = caseAlarmBlock(alarmBlock);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MncModelPackage.ALARM: {
				Alarm alarm = (Alarm)theEObject;
				T result = caseAlarm(alarm);
				if (result == null) result = caseAbstractInterfaceItems(alarm);
				if (result == null) result = caseSubscribableItems(alarm);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MncModelPackage.ALARM_TRIGGER_CONDITION: {
				AlarmTriggerCondition alarmTriggerCondition = (AlarmTriggerCondition)theEObject;
				T result = caseAlarmTriggerCondition(alarmTriggerCondition);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MncModelPackage.ALARM_TRANSLATION: {
				AlarmTranslation alarmTranslation = (AlarmTranslation)theEObject;
				T result = caseAlarmTranslation(alarmTranslation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MncModelPackage.ALARM_TRANSLATION_RULE: {
				AlarmTranslationRule alarmTranslationRule = (AlarmTranslationRule)theEObject;
				T result = caseAlarmTranslationRule(alarmTranslationRule);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MncModelPackage.ALARM_HANDLING: {
				AlarmHandling alarmHandling = (AlarmHandling)theEObject;
				T result = caseAlarmHandling(alarmHandling);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MncModelPackage.DATA_POINT_BLOCK: {
				DataPointBlock dataPointBlock = (DataPointBlock)theEObject;
				T result = caseDataPointBlock(dataPointBlock);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MncModelPackage.DATA_POINT: {
				DataPoint dataPoint = (DataPoint)theEObject;
				T result = caseDataPoint(dataPoint);
				if (result == null) result = caseAbstractInterfaceItems(dataPoint);
				if (result == null) result = caseSubscribableItems(dataPoint);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MncModelPackage.DATA_POINT_TRIGGER_CONDITION: {
				DataPointTriggerCondition dataPointTriggerCondition = (DataPointTriggerCondition)theEObject;
				T result = caseDataPointTriggerCondition(dataPointTriggerCondition);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MncModelPackage.DATA_POINT_TRANSLATION: {
				DataPointTranslation dataPointTranslation = (DataPointTranslation)theEObject;
				T result = caseDataPointTranslation(dataPointTranslation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MncModelPackage.DATA_POINT_TRANSLATION_RULE: {
				DataPointTranslationRule dataPointTranslationRule = (DataPointTranslationRule)theEObject;
				T result = caseDataPointTranslationRule(dataPointTranslationRule);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MncModelPackage.DATA_POINT_HANDLING: {
				DataPointHandling dataPointHandling = (DataPointHandling)theEObject;
				T result = caseDataPointHandling(dataPointHandling);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MncModelPackage.DATA_POINT_VALID_CONDITION: {
				DataPointValidCondition dataPointValidCondition = (DataPointValidCondition)theEObject;
				T result = caseDataPointValidCondition(dataPointValidCondition);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MncModelPackage.PARAMETER: {
				Parameter parameter = (Parameter)theEObject;
				T result = caseParameter(parameter);
				if (result == null) result = caseAbstractInterfaceItems(parameter);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MncModelPackage.CHECK_PARAMETER_CONDITION: {
				CheckParameterCondition checkParameterCondition = (CheckParameterCondition)theEObject;
				T result = caseCheckParameterCondition(checkParameterCondition);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MncModelPackage.ARRAY_TYPE: {
				ArrayType arrayType = (ArrayType)theEObject;
				T result = caseArrayType(arrayType);
				if (result == null) result = caseParameter(arrayType);
				if (result == null) result = caseAbstractInterfaceItems(arrayType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MncModelPackage.SIMPLE_TYPE: {
				SimpleType simpleType = (SimpleType)theEObject;
				T result = caseSimpleType(simpleType);
				if (result == null) result = caseParameter(simpleType);
				if (result == null) result = caseAbstractInterfaceItems(simpleType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MncModelPackage.PRIMITIVE_VALUE: {
				PrimitiveValue primitiveValue = (PrimitiveValue)theEObject;
				T result = casePrimitiveValue(primitiveValue);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MncModelPackage.INT_VALUE: {
				IntValue intValue = (IntValue)theEObject;
				T result = caseIntValue(intValue);
				if (result == null) result = casePrimitiveValue(intValue);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MncModelPackage.BOOL_VALUE: {
				BoolValue boolValue = (BoolValue)theEObject;
				T result = caseBoolValue(boolValue);
				if (result == null) result = casePrimitiveValue(boolValue);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MncModelPackage.FLOAT_VALUE: {
				FloatValue floatValue = (FloatValue)theEObject;
				T result = caseFloatValue(floatValue);
				if (result == null) result = casePrimitiveValue(floatValue);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MncModelPackage.STRING_VALUE: {
				StringValue stringValue = (StringValue)theEObject;
				T result = caseStringValue(stringValue);
				if (result == null) result = casePrimitiveValue(stringValue);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MncModelPackage.OPERATION: {
				Operation operation = (Operation)theEObject;
				T result = caseOperation(operation);
				if (result == null) result = caseAbstractOperationableItems(operation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MncModelPackage.ADDRESS: {
				Address address = (Address)theEObject;
				T result = caseAddress(address);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MncModelPackage.ACTION: {
				Action action = (Action)theEObject;
				T result = caseAction(action);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MncModelPackage.PUBLISH: {
				Publish publish = (Publish)theEObject;
				T result = casePublish(publish);
				if (result == null) result = caseOperation(publish);
				if (result == null) result = caseAbstractOperationableItems(publish);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MncModelPackage.SUBSCRIBABLE_ITEMS: {
				SubscribableItems subscribableItems = (SubscribableItems)theEObject;
				T result = caseSubscribableItems(subscribableItems);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MncModelPackage.SUBSCRIBABLE_ITEM_LIST: {
				SubscribableItemList subscribableItemList = (SubscribableItemList)theEObject;
				T result = caseSubscribableItemList(subscribableItemList);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MncModelPackage.RESPONSIBLE_ITEM_LIST: {
				ResponsibleItemList responsibleItemList = (ResponsibleItemList)theEObject;
				T result = caseResponsibleItemList(responsibleItemList);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Model</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Model</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseModel(Model object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Import</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Import</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseImport(Import object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>System</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>System</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSystem(mncModel.System object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Control Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Control Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseControlNode(ControlNode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Interface Description</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Interface Description</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseInterfaceDescription(InterfaceDescription object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Abstract Interface Items</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Abstract Interface Items</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAbstractInterfaceItems(AbstractInterfaceItems object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Abstract Operationable Items</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Abstract Operationable Items</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAbstractOperationableItems(AbstractOperationableItems object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Operating State</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Operating State</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOperatingState(OperatingState object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Command Response Block</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Command Response Block</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCommandResponseBlock(CommandResponseBlock object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Command</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Command</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCommand(Command object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Command Validation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Command Validation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCommandValidation(CommandValidation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Command Distribution</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Command Distribution</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCommandDistribution(CommandDistribution object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Command Translation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Command Translation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCommandTranslation(CommandTranslation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Command Trigger Condition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Command Trigger Condition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCommandTriggerCondition(CommandTriggerCondition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Parameter Translation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Parameter Translation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseParameterTranslation(ParameterTranslation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Transition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Transition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTransition(Transition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Response</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Response</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseResponse(Response object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Response Block</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Response Block</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseResponseBlock(ResponseBlock object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Response Validation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Response Validation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseResponseValidation(ResponseValidation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Response Distribution</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Response Distribution</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseResponseDistribution(ResponseDistribution object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Response Translation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Response Translation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseResponseTranslation(ResponseTranslation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Response Translation Rule</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Response Translation Rule</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseResponseTranslationRule(ResponseTranslationRule object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Port</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Port</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePort(Port object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Event Block</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Event Block</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEventBlock(EventBlock object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Event</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Event</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEvent(Event object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Event Trigger Condition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Event Trigger Condition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEventTriggerCondition(EventTriggerCondition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Event Translation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Event Translation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEventTranslation(EventTranslation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Event Translation Rule</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Event Translation Rule</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEventTranslationRule(EventTranslationRule object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Event Handling</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Event Handling</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEventHandling(EventHandling object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Alarm Block</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Alarm Block</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAlarmBlock(AlarmBlock object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Alarm</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Alarm</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAlarm(Alarm object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Alarm Trigger Condition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Alarm Trigger Condition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAlarmTriggerCondition(AlarmTriggerCondition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Alarm Translation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Alarm Translation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAlarmTranslation(AlarmTranslation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Alarm Translation Rule</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Alarm Translation Rule</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAlarmTranslationRule(AlarmTranslationRule object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Alarm Handling</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Alarm Handling</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAlarmHandling(AlarmHandling object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Point Block</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Point Block</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataPointBlock(DataPointBlock object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Point</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Point</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataPoint(DataPoint object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Point Trigger Condition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Point Trigger Condition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataPointTriggerCondition(DataPointTriggerCondition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Point Translation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Point Translation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataPointTranslation(DataPointTranslation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Point Translation Rule</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Point Translation Rule</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataPointTranslationRule(DataPointTranslationRule object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Point Handling</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Point Handling</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataPointHandling(DataPointHandling object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Point Valid Condition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Point Valid Condition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataPointValidCondition(DataPointValidCondition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Parameter</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Parameter</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseParameter(Parameter object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Check Parameter Condition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Check Parameter Condition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCheckParameterCondition(CheckParameterCondition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Array Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Array Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseArrayType(ArrayType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Simple Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Simple Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSimpleType(SimpleType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Primitive Value</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Primitive Value</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePrimitiveValue(PrimitiveValue object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Int Value</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Int Value</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIntValue(IntValue object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Bool Value</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Bool Value</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBoolValue(BoolValue object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Float Value</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Float Value</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFloatValue(FloatValue object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>String Value</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>String Value</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseStringValue(StringValue object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Operation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Operation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOperation(Operation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Address</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Address</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAddress(Address object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Action</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Action</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAction(Action object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Publish</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Publish</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePublish(Publish object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Subscribable Items</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Subscribable Items</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSubscribableItems(SubscribableItems object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Subscribable Item List</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Subscribable Item List</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSubscribableItemList(SubscribableItemList object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Responsible Item List</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Responsible Item List</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseResponsibleItemList(ResponsibleItemList object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //MncModelSwitch
