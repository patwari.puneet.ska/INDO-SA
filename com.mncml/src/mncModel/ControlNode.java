/**
 */
package mncModel;

import mncModel.utility.AlarmBlockUtility;
import mncModel.utility.CommandResponseBlockUtility;
import mncModel.utility.DataPointBlockUtility;
import mncModel.utility.EventBlockUtility;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Control Node</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * Control Node
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link mncModel.ControlNode#getName <em>Name</em>}</li>
 *   <li>{@link mncModel.ControlNode#getInterfaceDescription <em>Interface Description</em>}</li>
 *   <li>{@link mncModel.ControlNode#getChildNodes <em>Child Nodes</em>}</li>
 *   <li>{@link mncModel.ControlNode#getParentNode <em>Parent Node</em>}</li>
 *   <li>{@link mncModel.ControlNode#getCommandResponseBlocks <em>Command Response Blocks</em>}</li>
 *   <li>{@link mncModel.ControlNode#getEventBlocks <em>Event Blocks</em>}</li>
 *   <li>{@link mncModel.ControlNode#getAlarmBlocks <em>Alarm Blocks</em>}</li>
 *   <li>{@link mncModel.ControlNode#getDataPointBlocks <em>Data Point Blocks</em>}</li>
 * </ul>
 *
 * @see mncModel.MncModelPackage#getControlNode()
 * @model
 * @generated
 */
public interface ControlNode extends mncModel.System {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see mncModel.MncModelPackage#getControlNode_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link mncModel.ControlNode#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Interface Description</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Interface Description</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Interface Description</em>' reference.
	 * @see #setInterfaceDescription(InterfaceDescription)
	 * @see mncModel.MncModelPackage#getControlNode_InterfaceDescription()
	 * @model
	 * @generated
	 */
	InterfaceDescription getInterfaceDescription();

	/**
	 * Sets the value of the '{@link mncModel.ControlNode#getInterfaceDescription <em>Interface Description</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Interface Description</em>' reference.
	 * @see #getInterfaceDescription()
	 * @generated
	 */
	void setInterfaceDescription(InterfaceDescription value);

	/**
	 * Returns the value of the '<em><b>Child Nodes</b></em>' reference list.
	 * The list contents are of type {@link mncModel.ControlNode}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Child Nodes</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Child Nodes</em>' reference list.
	 * @see mncModel.MncModelPackage#getControlNode_ChildNodes()
	 * @model resolveProxies="false"
	 * @generated
	 */
	EList<ControlNode> getChildNodes();

	/**
	 * Returns the value of the '<em><b>Parent Node</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parent Node</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parent Node</em>' reference.
	 * @see #setParentNode(ControlNode)
	 * @see mncModel.MncModelPackage#getControlNode_ParentNode()
	 * @model
	 * @generated
	 */
	ControlNode getParentNode();

	/**
	 * Sets the value of the '{@link mncModel.ControlNode#getParentNode <em>Parent Node</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Parent Node</em>' reference.
	 * @see #getParentNode()
	 * @generated
	 */
	void setParentNode(ControlNode value);

	/**
	 * Returns the value of the '<em><b>Command Response Blocks</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Command Response Blocks</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Command Response Blocks</em>' containment reference.
	 * @see #setCommandResponseBlocks(CommandResponseBlockUtility)
	 * @see mncModel.MncModelPackage#getControlNode_CommandResponseBlocks()
	 * @model containment="true"
	 * @generated
	 */
	CommandResponseBlockUtility getCommandResponseBlocks();

	/**
	 * Sets the value of the '{@link mncModel.ControlNode#getCommandResponseBlocks <em>Command Response Blocks</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Command Response Blocks</em>' containment reference.
	 * @see #getCommandResponseBlocks()
	 * @generated
	 */
	void setCommandResponseBlocks(CommandResponseBlockUtility value);

	/**
	 * Returns the value of the '<em><b>Event Blocks</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Event Blocks</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Event Blocks</em>' containment reference.
	 * @see #setEventBlocks(EventBlockUtility)
	 * @see mncModel.MncModelPackage#getControlNode_EventBlocks()
	 * @model containment="true"
	 * @generated
	 */
	EventBlockUtility getEventBlocks();

	/**
	 * Sets the value of the '{@link mncModel.ControlNode#getEventBlocks <em>Event Blocks</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Event Blocks</em>' containment reference.
	 * @see #getEventBlocks()
	 * @generated
	 */
	void setEventBlocks(EventBlockUtility value);

	/**
	 * Returns the value of the '<em><b>Alarm Blocks</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Alarm Blocks</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Alarm Blocks</em>' containment reference.
	 * @see #setAlarmBlocks(AlarmBlockUtility)
	 * @see mncModel.MncModelPackage#getControlNode_AlarmBlocks()
	 * @model containment="true"
	 * @generated
	 */
	AlarmBlockUtility getAlarmBlocks();

	/**
	 * Sets the value of the '{@link mncModel.ControlNode#getAlarmBlocks <em>Alarm Blocks</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Alarm Blocks</em>' containment reference.
	 * @see #getAlarmBlocks()
	 * @generated
	 */
	void setAlarmBlocks(AlarmBlockUtility value);

	/**
	 * Returns the value of the '<em><b>Data Point Blocks</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Data Point Blocks</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data Point Blocks</em>' containment reference.
	 * @see #setDataPointBlocks(DataPointBlockUtility)
	 * @see mncModel.MncModelPackage#getControlNode_DataPointBlocks()
	 * @model containment="true"
	 * @generated
	 */
	DataPointBlockUtility getDataPointBlocks();

	/**
	 * Sets the value of the '{@link mncModel.ControlNode#getDataPointBlocks <em>Data Point Blocks</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Data Point Blocks</em>' containment reference.
	 * @see #getDataPointBlocks()
	 * @generated
	 */
	void setDataPointBlocks(DataPointBlockUtility value);

} // ControlNode
