/**
 */
package pogoDsl.impl;

import org.eclipse.emf.ecore.EClass;

import pogoDsl.PogoDslPackage;
import pogoDsl.ULongType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>ULong Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ULongTypeImpl extends TypeImpl implements ULongType {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ULongTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return PogoDslPackage.Literals.ULONG_TYPE;
	}

} //ULongTypeImpl
