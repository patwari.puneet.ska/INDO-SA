/**
 */
package pogoDsl.impl;

import org.eclipse.emf.ecore.EClass;

import pogoDsl.DoubleVectorType;
import pogoDsl.PogoDslPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Double Vector Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class DoubleVectorTypeImpl extends VectorTypeImpl implements DoubleVectorType {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DoubleVectorTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return PogoDslPackage.Literals.DOUBLE_VECTOR_TYPE;
	}

} //DoubleVectorTypeImpl
