/**
 */
package pogoDsl.impl;

import org.eclipse.emf.ecore.EClass;

import pogoDsl.PogoDslPackage;
import pogoDsl.UCharType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>UChar Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class UCharTypeImpl extends TypeImpl implements UCharType {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UCharTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return PogoDslPackage.Literals.UCHAR_TYPE;
	}

} //UCharTypeImpl
