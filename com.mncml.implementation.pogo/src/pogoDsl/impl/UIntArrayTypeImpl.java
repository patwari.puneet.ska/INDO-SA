/**
 */
package pogoDsl.impl;

import org.eclipse.emf.ecore.EClass;

import pogoDsl.PogoDslPackage;
import pogoDsl.UIntArrayType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>UInt Array Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class UIntArrayTypeImpl extends TypeImpl implements UIntArrayType {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UIntArrayTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return PogoDslPackage.Literals.UINT_ARRAY_TYPE;
	}

} //UIntArrayTypeImpl
