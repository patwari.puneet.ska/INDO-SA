/**
 */
package pogoDsl.impl;

import org.eclipse.emf.ecore.EClass;

import pogoDsl.PogoDslPackage;
import pogoDsl.ShortVectorType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Short Vector Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ShortVectorTypeImpl extends VectorTypeImpl implements ShortVectorType {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ShortVectorTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return PogoDslPackage.Literals.SHORT_VECTOR_TYPE;
	}

} //ShortVectorTypeImpl
