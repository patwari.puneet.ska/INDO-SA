/**
 */
package pogoDsl.impl;

import org.eclipse.emf.ecore.EClass;

import pogoDsl.PogoDslPackage;
import pogoDsl.ShortArrayType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Short Array Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ShortArrayTypeImpl extends TypeImpl implements ShortArrayType {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ShortArrayTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return PogoDslPackage.Literals.SHORT_ARRAY_TYPE;
	}

} //ShortArrayTypeImpl
