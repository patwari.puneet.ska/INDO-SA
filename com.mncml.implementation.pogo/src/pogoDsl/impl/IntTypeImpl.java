/**
 */
package pogoDsl.impl;

import org.eclipse.emf.ecore.EClass;

import pogoDsl.IntType;
import pogoDsl.PogoDslPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Int Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class IntTypeImpl extends SimpleTypeImpl implements IntType {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IntTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return PogoDslPackage.Literals.INT_TYPE;
	}

} //IntTypeImpl
