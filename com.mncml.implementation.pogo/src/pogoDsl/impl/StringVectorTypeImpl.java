/**
 */
package pogoDsl.impl;

import org.eclipse.emf.ecore.EClass;

import pogoDsl.PogoDslPackage;
import pogoDsl.StringVectorType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>String Vector Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class StringVectorTypeImpl extends VectorTypeImpl implements StringVectorType {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected StringVectorTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return PogoDslPackage.Literals.STRING_VECTOR_TYPE;
	}

} //StringVectorTypeImpl
