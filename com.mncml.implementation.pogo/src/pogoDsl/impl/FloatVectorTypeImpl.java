/**
 */
package pogoDsl.impl;

import org.eclipse.emf.ecore.EClass;

import pogoDsl.FloatVectorType;
import pogoDsl.PogoDslPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Float Vector Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class FloatVectorTypeImpl extends VectorTypeImpl implements FloatVectorType {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FloatVectorTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return PogoDslPackage.Literals.FLOAT_VECTOR_TYPE;
	}

} //FloatVectorTypeImpl
