/**
 */
package pogoDsl.impl;

import org.eclipse.emf.ecore.EClass;

import pogoDsl.PogoDslPackage;
import pogoDsl.UIntType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>UInt Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class UIntTypeImpl extends SimpleTypeImpl implements UIntType {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UIntTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return PogoDslPackage.Literals.UINT_TYPE;
	}

} //UIntTypeImpl
