/**
 */
package pogoDsl.impl;

import org.eclipse.emf.ecore.EClass;

import pogoDsl.PogoDslPackage;
import pogoDsl.UShortArrayType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>UShort Array Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class UShortArrayTypeImpl extends TypeImpl implements UShortArrayType {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UShortArrayTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return PogoDslPackage.Literals.USHORT_ARRAY_TYPE;
	}

} //UShortArrayTypeImpl
