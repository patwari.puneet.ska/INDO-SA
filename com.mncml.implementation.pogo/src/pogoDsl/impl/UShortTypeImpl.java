/**
 */
package pogoDsl.impl;

import org.eclipse.emf.ecore.EClass;

import pogoDsl.PogoDslPackage;
import pogoDsl.UShortType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>UShort Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class UShortTypeImpl extends SimpleTypeImpl implements UShortType {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UShortTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return PogoDslPackage.Literals.USHORT_TYPE;
	}

} //UShortTypeImpl
