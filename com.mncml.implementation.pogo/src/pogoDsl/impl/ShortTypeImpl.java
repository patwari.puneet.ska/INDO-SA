/**
 */
package pogoDsl.impl;

import org.eclipse.emf.ecore.EClass;

import pogoDsl.PogoDslPackage;
import pogoDsl.ShortType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Short Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ShortTypeImpl extends SimpleTypeImpl implements ShortType {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ShortTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return PogoDslPackage.Literals.SHORT_TYPE;
	}

} //ShortTypeImpl
