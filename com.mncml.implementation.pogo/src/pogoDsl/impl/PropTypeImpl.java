/**
 */
package pogoDsl.impl;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import pogoDsl.PogoDslPackage;
import pogoDsl.PropType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Prop Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class PropTypeImpl extends MinimalEObjectImpl.Container implements PropType {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PropTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return PogoDslPackage.Literals.PROP_TYPE;
	}

} //PropTypeImpl
