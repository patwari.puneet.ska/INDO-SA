/**
 */
package pogoDsl.impl;

import org.eclipse.emf.ecore.EClass;

import pogoDsl.PogoDslPackage;
import pogoDsl.ULongArrayType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>ULong Array Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ULongArrayTypeImpl extends TypeImpl implements ULongArrayType {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ULongArrayTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return PogoDslPackage.Literals.ULONG_ARRAY_TYPE;
	}

} //ULongArrayTypeImpl
