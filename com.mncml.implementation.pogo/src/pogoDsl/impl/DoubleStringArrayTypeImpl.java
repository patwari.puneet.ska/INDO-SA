/**
 */
package pogoDsl.impl;

import org.eclipse.emf.ecore.EClass;

import pogoDsl.DoubleStringArrayType;
import pogoDsl.PogoDslPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Double String Array Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class DoubleStringArrayTypeImpl extends TypeImpl implements DoubleStringArrayType {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DoubleStringArrayTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return PogoDslPackage.Literals.DOUBLE_STRING_ARRAY_TYPE;
	}

} //DoubleStringArrayTypeImpl
