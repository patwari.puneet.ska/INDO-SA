/**
 */
package pogoDsl.impl;

import org.eclipse.emf.ecore.EClass;

import pogoDsl.LongStringArrayType;
import pogoDsl.PogoDslPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Long String Array Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class LongStringArrayTypeImpl extends TypeImpl implements LongStringArrayType {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected LongStringArrayTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return PogoDslPackage.Literals.LONG_STRING_ARRAY_TYPE;
	}

} //LongStringArrayTypeImpl
