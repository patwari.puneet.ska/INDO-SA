/**
 */
package pogoDsl.impl;

import org.eclipse.emf.ecore.EClass;

import pogoDsl.IntVectorType;
import pogoDsl.PogoDslPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Int Vector Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class IntVectorTypeImpl extends VectorTypeImpl implements IntVectorType {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IntVectorTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return PogoDslPackage.Literals.INT_VECTOR_TYPE;
	}

} //IntVectorTypeImpl
