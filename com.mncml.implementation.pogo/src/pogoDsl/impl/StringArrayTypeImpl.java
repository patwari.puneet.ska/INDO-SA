/**
 */
package pogoDsl.impl;

import org.eclipse.emf.ecore.EClass;

import pogoDsl.PogoDslPackage;
import pogoDsl.StringArrayType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>String Array Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class StringArrayTypeImpl extends TypeImpl implements StringArrayType {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected StringArrayTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return PogoDslPackage.Literals.STRING_ARRAY_TYPE;
	}

} //StringArrayTypeImpl
