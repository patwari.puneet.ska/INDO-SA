/**
 */
package pogoDsl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Encoded Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see pogoDsl.PogoDslPackage#getEncodedType()
 * @model
 * @generated
 */
public interface EncodedType extends Type {
} // EncodedType
