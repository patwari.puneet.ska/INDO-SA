/**
 */
package pogoDsl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Dev Int Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see pogoDsl.PogoDslPackage#getDevIntType()
 * @model
 * @generated
 */
public interface DevIntType extends Type {
} // DevIntType
