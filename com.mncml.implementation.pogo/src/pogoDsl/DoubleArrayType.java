/**
 */
package pogoDsl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Double Array Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see pogoDsl.PogoDslPackage#getDoubleArrayType()
 * @model
 * @generated
 */
public interface DoubleArrayType extends Type {
} // DoubleArrayType
