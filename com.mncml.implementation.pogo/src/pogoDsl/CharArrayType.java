/**
 */
package pogoDsl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Char Array Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see pogoDsl.PogoDslPackage#getCharArrayType()
 * @model
 * @generated
 */
public interface CharArrayType extends Type {
} // CharArrayType
