/**
 */
package pogoDsl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Long String Array Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see pogoDsl.PogoDslPackage#getLongStringArrayType()
 * @model
 * @generated
 */
public interface LongStringArrayType extends Type {
} // LongStringArrayType
