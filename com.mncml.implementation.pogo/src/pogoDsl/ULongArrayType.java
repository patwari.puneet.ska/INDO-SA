/**
 */
package pogoDsl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ULong Array Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see pogoDsl.PogoDslPackage#getULongArrayType()
 * @model
 * @generated
 */
public interface ULongArrayType extends Type {
} // ULongArrayType
