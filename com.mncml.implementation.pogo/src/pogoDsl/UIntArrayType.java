/**
 */
package pogoDsl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>UInt Array Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see pogoDsl.PogoDslPackage#getUIntArrayType()
 * @model
 * @generated
 */
public interface UIntArrayType extends Type {
} // UIntArrayType
