/**
 */
package pogoDsl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Const String Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see pogoDsl.PogoDslPackage#getConstStringType()
 * @model
 * @generated
 */
public interface ConstStringType extends Type {
} // ConstStringType
