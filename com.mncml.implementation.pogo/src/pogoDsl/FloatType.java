/**
 */
package pogoDsl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Float Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see pogoDsl.PogoDslPackage#getFloatType()
 * @model
 * @generated
 */
public interface FloatType extends SimpleType, Type {
} // FloatType
