/**
 */
package pogoDsl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Attr Properties</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link pogoDsl.AttrProperties#getDescription <em>Description</em>}</li>
 *   <li>{@link pogoDsl.AttrProperties#getLabel <em>Label</em>}</li>
 *   <li>{@link pogoDsl.AttrProperties#getUnit <em>Unit</em>}</li>
 *   <li>{@link pogoDsl.AttrProperties#getStandardUnit <em>Standard Unit</em>}</li>
 *   <li>{@link pogoDsl.AttrProperties#getDisplayUnit <em>Display Unit</em>}</li>
 *   <li>{@link pogoDsl.AttrProperties#getFormat <em>Format</em>}</li>
 *   <li>{@link pogoDsl.AttrProperties#getMaxValue <em>Max Value</em>}</li>
 *   <li>{@link pogoDsl.AttrProperties#getMinValue <em>Min Value</em>}</li>
 *   <li>{@link pogoDsl.AttrProperties#getMaxAlarm <em>Max Alarm</em>}</li>
 *   <li>{@link pogoDsl.AttrProperties#getMinAlarm <em>Min Alarm</em>}</li>
 *   <li>{@link pogoDsl.AttrProperties#getMaxWarning <em>Max Warning</em>}</li>
 *   <li>{@link pogoDsl.AttrProperties#getMinWarning <em>Min Warning</em>}</li>
 *   <li>{@link pogoDsl.AttrProperties#getDeltaTime <em>Delta Time</em>}</li>
 *   <li>{@link pogoDsl.AttrProperties#getDeltaValue <em>Delta Value</em>}</li>
 * </ul>
 *
 * @see pogoDsl.PogoDslPackage#getAttrProperties()
 * @model
 * @generated
 */
public interface AttrProperties extends EObject {
	/**
	 * Returns the value of the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Description</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description</em>' attribute.
	 * @see #setDescription(String)
	 * @see pogoDsl.PogoDslPackage#getAttrProperties_Description()
	 * @model
	 * @generated
	 */
	String getDescription();

	/**
	 * Sets the value of the '{@link pogoDsl.AttrProperties#getDescription <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Description</em>' attribute.
	 * @see #getDescription()
	 * @generated
	 */
	void setDescription(String value);

	/**
	 * Returns the value of the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Label</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Label</em>' attribute.
	 * @see #setLabel(String)
	 * @see pogoDsl.PogoDslPackage#getAttrProperties_Label()
	 * @model
	 * @generated
	 */
	String getLabel();

	/**
	 * Sets the value of the '{@link pogoDsl.AttrProperties#getLabel <em>Label</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Label</em>' attribute.
	 * @see #getLabel()
	 * @generated
	 */
	void setLabel(String value);

	/**
	 * Returns the value of the '<em><b>Unit</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unit</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unit</em>' attribute.
	 * @see #setUnit(String)
	 * @see pogoDsl.PogoDslPackage#getAttrProperties_Unit()
	 * @model
	 * @generated
	 */
	String getUnit();

	/**
	 * Sets the value of the '{@link pogoDsl.AttrProperties#getUnit <em>Unit</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Unit</em>' attribute.
	 * @see #getUnit()
	 * @generated
	 */
	void setUnit(String value);

	/**
	 * Returns the value of the '<em><b>Standard Unit</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Standard Unit</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Standard Unit</em>' attribute.
	 * @see #setStandardUnit(String)
	 * @see pogoDsl.PogoDslPackage#getAttrProperties_StandardUnit()
	 * @model
	 * @generated
	 */
	String getStandardUnit();

	/**
	 * Sets the value of the '{@link pogoDsl.AttrProperties#getStandardUnit <em>Standard Unit</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Standard Unit</em>' attribute.
	 * @see #getStandardUnit()
	 * @generated
	 */
	void setStandardUnit(String value);

	/**
	 * Returns the value of the '<em><b>Display Unit</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Display Unit</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Display Unit</em>' attribute.
	 * @see #setDisplayUnit(String)
	 * @see pogoDsl.PogoDslPackage#getAttrProperties_DisplayUnit()
	 * @model
	 * @generated
	 */
	String getDisplayUnit();

	/**
	 * Sets the value of the '{@link pogoDsl.AttrProperties#getDisplayUnit <em>Display Unit</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Display Unit</em>' attribute.
	 * @see #getDisplayUnit()
	 * @generated
	 */
	void setDisplayUnit(String value);

	/**
	 * Returns the value of the '<em><b>Format</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Format</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Format</em>' attribute.
	 * @see #setFormat(String)
	 * @see pogoDsl.PogoDslPackage#getAttrProperties_Format()
	 * @model
	 * @generated
	 */
	String getFormat();

	/**
	 * Sets the value of the '{@link pogoDsl.AttrProperties#getFormat <em>Format</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Format</em>' attribute.
	 * @see #getFormat()
	 * @generated
	 */
	void setFormat(String value);

	/**
	 * Returns the value of the '<em><b>Max Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Max Value</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Max Value</em>' attribute.
	 * @see #setMaxValue(String)
	 * @see pogoDsl.PogoDslPackage#getAttrProperties_MaxValue()
	 * @model
	 * @generated
	 */
	String getMaxValue();

	/**
	 * Sets the value of the '{@link pogoDsl.AttrProperties#getMaxValue <em>Max Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Max Value</em>' attribute.
	 * @see #getMaxValue()
	 * @generated
	 */
	void setMaxValue(String value);

	/**
	 * Returns the value of the '<em><b>Min Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Min Value</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Min Value</em>' attribute.
	 * @see #setMinValue(String)
	 * @see pogoDsl.PogoDslPackage#getAttrProperties_MinValue()
	 * @model
	 * @generated
	 */
	String getMinValue();

	/**
	 * Sets the value of the '{@link pogoDsl.AttrProperties#getMinValue <em>Min Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Min Value</em>' attribute.
	 * @see #getMinValue()
	 * @generated
	 */
	void setMinValue(String value);

	/**
	 * Returns the value of the '<em><b>Max Alarm</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Max Alarm</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Max Alarm</em>' attribute.
	 * @see #setMaxAlarm(String)
	 * @see pogoDsl.PogoDslPackage#getAttrProperties_MaxAlarm()
	 * @model
	 * @generated
	 */
	String getMaxAlarm();

	/**
	 * Sets the value of the '{@link pogoDsl.AttrProperties#getMaxAlarm <em>Max Alarm</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Max Alarm</em>' attribute.
	 * @see #getMaxAlarm()
	 * @generated
	 */
	void setMaxAlarm(String value);

	/**
	 * Returns the value of the '<em><b>Min Alarm</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Min Alarm</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Min Alarm</em>' attribute.
	 * @see #setMinAlarm(String)
	 * @see pogoDsl.PogoDslPackage#getAttrProperties_MinAlarm()
	 * @model
	 * @generated
	 */
	String getMinAlarm();

	/**
	 * Sets the value of the '{@link pogoDsl.AttrProperties#getMinAlarm <em>Min Alarm</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Min Alarm</em>' attribute.
	 * @see #getMinAlarm()
	 * @generated
	 */
	void setMinAlarm(String value);

	/**
	 * Returns the value of the '<em><b>Max Warning</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Max Warning</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Max Warning</em>' attribute.
	 * @see #setMaxWarning(String)
	 * @see pogoDsl.PogoDslPackage#getAttrProperties_MaxWarning()
	 * @model
	 * @generated
	 */
	String getMaxWarning();

	/**
	 * Sets the value of the '{@link pogoDsl.AttrProperties#getMaxWarning <em>Max Warning</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Max Warning</em>' attribute.
	 * @see #getMaxWarning()
	 * @generated
	 */
	void setMaxWarning(String value);

	/**
	 * Returns the value of the '<em><b>Min Warning</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Min Warning</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Min Warning</em>' attribute.
	 * @see #setMinWarning(String)
	 * @see pogoDsl.PogoDslPackage#getAttrProperties_MinWarning()
	 * @model
	 * @generated
	 */
	String getMinWarning();

	/**
	 * Sets the value of the '{@link pogoDsl.AttrProperties#getMinWarning <em>Min Warning</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Min Warning</em>' attribute.
	 * @see #getMinWarning()
	 * @generated
	 */
	void setMinWarning(String value);

	/**
	 * Returns the value of the '<em><b>Delta Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Delta Time</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Delta Time</em>' attribute.
	 * @see #setDeltaTime(String)
	 * @see pogoDsl.PogoDslPackage#getAttrProperties_DeltaTime()
	 * @model
	 * @generated
	 */
	String getDeltaTime();

	/**
	 * Sets the value of the '{@link pogoDsl.AttrProperties#getDeltaTime <em>Delta Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Delta Time</em>' attribute.
	 * @see #getDeltaTime()
	 * @generated
	 */
	void setDeltaTime(String value);

	/**
	 * Returns the value of the '<em><b>Delta Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Delta Value</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Delta Value</em>' attribute.
	 * @see #setDeltaValue(String)
	 * @see pogoDsl.PogoDslPackage#getAttrProperties_DeltaValue()
	 * @model
	 * @generated
	 */
	String getDeltaValue();

	/**
	 * Sets the value of the '{@link pogoDsl.AttrProperties#getDeltaValue <em>Delta Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Delta Value</em>' attribute.
	 * @see #getDeltaValue()
	 * @generated
	 */
	void setDeltaValue(String value);

} // AttrProperties
