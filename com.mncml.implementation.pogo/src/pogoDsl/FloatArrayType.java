/**
 */
package pogoDsl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Float Array Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see pogoDsl.PogoDslPackage#getFloatArrayType()
 * @model
 * @generated
 */
public interface FloatArrayType extends Type {
} // FloatArrayType
