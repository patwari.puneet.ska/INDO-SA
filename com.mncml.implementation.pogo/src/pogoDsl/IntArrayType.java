/**
 */
package pogoDsl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Int Array Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see pogoDsl.PogoDslPackage#getIntArrayType()
 * @model
 * @generated
 */
public interface IntArrayType extends Type {
} // IntArrayType
