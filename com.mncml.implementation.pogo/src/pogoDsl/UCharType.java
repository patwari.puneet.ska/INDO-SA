/**
 */
package pogoDsl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>UChar Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see pogoDsl.PogoDslPackage#getUCharType()
 * @model
 * @generated
 */
public interface UCharType extends Type {
} // UCharType
