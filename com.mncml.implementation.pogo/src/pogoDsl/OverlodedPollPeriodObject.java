/**
 */
package pogoDsl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Overloded Poll Period Object</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link pogoDsl.OverlodedPollPeriodObject#getName <em>Name</em>}</li>
 *   <li>{@link pogoDsl.OverlodedPollPeriodObject#getType <em>Type</em>}</li>
 *   <li>{@link pogoDsl.OverlodedPollPeriodObject#getPollPeriod <em>Poll Period</em>}</li>
 * </ul>
 *
 * @see pogoDsl.PogoDslPackage#getOverlodedPollPeriodObject()
 * @model
 * @generated
 */
public interface OverlodedPollPeriodObject extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see pogoDsl.PogoDslPackage#getOverlodedPollPeriodObject_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link pogoDsl.OverlodedPollPeriodObject#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' attribute.
	 * @see #setType(String)
	 * @see pogoDsl.PogoDslPackage#getOverlodedPollPeriodObject_Type()
	 * @model
	 * @generated
	 */
	String getType();

	/**
	 * Sets the value of the '{@link pogoDsl.OverlodedPollPeriodObject#getType <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' attribute.
	 * @see #getType()
	 * @generated
	 */
	void setType(String value);

	/**
	 * Returns the value of the '<em><b>Poll Period</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Poll Period</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Poll Period</em>' attribute.
	 * @see #setPollPeriod(String)
	 * @see pogoDsl.PogoDslPackage#getOverlodedPollPeriodObject_PollPeriod()
	 * @model
	 * @generated
	 */
	String getPollPeriod();

	/**
	 * Sets the value of the '{@link pogoDsl.OverlodedPollPeriodObject#getPollPeriod <em>Poll Period</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Poll Period</em>' attribute.
	 * @see #getPollPeriod()
	 * @generated
	 */
	void setPollPeriod(String value);

} // OverlodedPollPeriodObject
