/**
 */
package pogoDsl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>UShort Array Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see pogoDsl.PogoDslPackage#getUShortArrayType()
 * @model
 * @generated
 */
public interface UShortArrayType extends Type {
} // UShortArrayType
