/**
 */
package pogoDsl;

import mncModel.ControlNode;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Pogo Device Class</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link pogoDsl.PogoDeviceClass#getName <em>Name</em>}</li>
 *   <li>{@link pogoDsl.PogoDeviceClass#getRefferedControlNode <em>Reffered Control Node</em>}</li>
 *   <li>{@link pogoDsl.PogoDeviceClass#isIsAbstract <em>Is Abstract</em>}</li>
 *   <li>{@link pogoDsl.PogoDeviceClass#getBaseClass <em>Base Class</em>}</li>
 *   <li>{@link pogoDsl.PogoDeviceClass#getPogoRevision <em>Pogo Revision</em>}</li>
 *   <li>{@link pogoDsl.PogoDeviceClass#getInstitute <em>Institute</em>}</li>
 *   <li>{@link pogoDsl.PogoDeviceClass#getDescription <em>Description</em>}</li>
 *   <li>{@link pogoDsl.PogoDeviceClass#getClassProperties <em>Class Properties</em>}</li>
 *   <li>{@link pogoDsl.PogoDeviceClass#getDeviceProperties <em>Device Properties</em>}</li>
 *   <li>{@link pogoDsl.PogoDeviceClass#getCommands <em>Commands</em>}</li>
 *   <li>{@link pogoDsl.PogoDeviceClass#getDynamicCommands <em>Dynamic Commands</em>}</li>
 *   <li>{@link pogoDsl.PogoDeviceClass#getAttributes <em>Attributes</em>}</li>
 *   <li>{@link pogoDsl.PogoDeviceClass#getDynamicAttributes <em>Dynamic Attributes</em>}</li>
 *   <li>{@link pogoDsl.PogoDeviceClass#getForwardedAttributes <em>Forwarded Attributes</em>}</li>
 *   <li>{@link pogoDsl.PogoDeviceClass#getPipes <em>Pipes</em>}</li>
 *   <li>{@link pogoDsl.PogoDeviceClass#getStates <em>States</em>}</li>
 *   <li>{@link pogoDsl.PogoDeviceClass#getPreferences <em>Preferences</em>}</li>
 *   <li>{@link pogoDsl.PogoDeviceClass#getAdditionalFiles <em>Additional Files</em>}</li>
 *   <li>{@link pogoDsl.PogoDeviceClass#getOverlodedPollPeriodObject <em>Overloded Poll Period Object</em>}</li>
 * </ul>
 *
 * @see pogoDsl.PogoDslPackage#getPogoDeviceClass()
 * @model
 * @generated
 */
public interface PogoDeviceClass extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see pogoDsl.PogoDslPackage#getPogoDeviceClass_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link pogoDsl.PogoDeviceClass#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Reffered Control Node</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Reffered Control Node</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Reffered Control Node</em>' reference.
	 * @see #setRefferedControlNode(ControlNode)
	 * @see pogoDsl.PogoDslPackage#getPogoDeviceClass_RefferedControlNode()
	 * @model
	 * @generated
	 */
	ControlNode getRefferedControlNode();

	/**
	 * Sets the value of the '{@link pogoDsl.PogoDeviceClass#getRefferedControlNode <em>Reffered Control Node</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Reffered Control Node</em>' reference.
	 * @see #getRefferedControlNode()
	 * @generated
	 */
	void setRefferedControlNode(ControlNode value);

	/**
	 * Returns the value of the '<em><b>Is Abstract</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is Abstract</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is Abstract</em>' attribute.
	 * @see #setIsAbstract(boolean)
	 * @see pogoDsl.PogoDslPackage#getPogoDeviceClass_IsAbstract()
	 * @model
	 * @generated
	 */
	boolean isIsAbstract();

	/**
	 * Sets the value of the '{@link pogoDsl.PogoDeviceClass#isIsAbstract <em>Is Abstract</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Is Abstract</em>' attribute.
	 * @see #isIsAbstract()
	 * @generated
	 */
	void setIsAbstract(boolean value);

	/**
	 * Returns the value of the '<em><b>Base Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Base Class</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Base Class</em>' reference.
	 * @see #setBaseClass(PogoDeviceClass)
	 * @see pogoDsl.PogoDslPackage#getPogoDeviceClass_BaseClass()
	 * @model
	 * @generated
	 */
	PogoDeviceClass getBaseClass();

	/**
	 * Sets the value of the '{@link pogoDsl.PogoDeviceClass#getBaseClass <em>Base Class</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Base Class</em>' reference.
	 * @see #getBaseClass()
	 * @generated
	 */
	void setBaseClass(PogoDeviceClass value);

	/**
	 * Returns the value of the '<em><b>Pogo Revision</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Pogo Revision</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Pogo Revision</em>' attribute.
	 * @see #setPogoRevision(String)
	 * @see pogoDsl.PogoDslPackage#getPogoDeviceClass_PogoRevision()
	 * @model
	 * @generated
	 */
	String getPogoRevision();

	/**
	 * Sets the value of the '{@link pogoDsl.PogoDeviceClass#getPogoRevision <em>Pogo Revision</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Pogo Revision</em>' attribute.
	 * @see #getPogoRevision()
	 * @generated
	 */
	void setPogoRevision(String value);

	/**
	 * Returns the value of the '<em><b>Institute</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Institute</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Institute</em>' attribute.
	 * @see #setInstitute(String)
	 * @see pogoDsl.PogoDslPackage#getPogoDeviceClass_Institute()
	 * @model
	 * @generated
	 */
	String getInstitute();

	/**
	 * Sets the value of the '{@link pogoDsl.PogoDeviceClass#getInstitute <em>Institute</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Institute</em>' attribute.
	 * @see #getInstitute()
	 * @generated
	 */
	void setInstitute(String value);

	/**
	 * Returns the value of the '<em><b>Description</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Description</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description</em>' containment reference.
	 * @see #setDescription(ClassDescription)
	 * @see pogoDsl.PogoDslPackage#getPogoDeviceClass_Description()
	 * @model containment="true"
	 * @generated
	 */
	ClassDescription getDescription();

	/**
	 * Sets the value of the '{@link pogoDsl.PogoDeviceClass#getDescription <em>Description</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Description</em>' containment reference.
	 * @see #getDescription()
	 * @generated
	 */
	void setDescription(ClassDescription value);

	/**
	 * Returns the value of the '<em><b>Class Properties</b></em>' containment reference list.
	 * The list contents are of type {@link pogoDsl.Property}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Class Properties</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Class Properties</em>' containment reference list.
	 * @see pogoDsl.PogoDslPackage#getPogoDeviceClass_ClassProperties()
	 * @model containment="true"
	 * @generated
	 */
	EList<Property> getClassProperties();

	/**
	 * Returns the value of the '<em><b>Device Properties</b></em>' containment reference list.
	 * The list contents are of type {@link pogoDsl.Property}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Device Properties</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Device Properties</em>' containment reference list.
	 * @see pogoDsl.PogoDslPackage#getPogoDeviceClass_DeviceProperties()
	 * @model containment="true"
	 * @generated
	 */
	EList<Property> getDeviceProperties();

	/**
	 * Returns the value of the '<em><b>Commands</b></em>' containment reference list.
	 * The list contents are of type {@link pogoDsl.Command}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Commands</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Commands</em>' containment reference list.
	 * @see pogoDsl.PogoDslPackage#getPogoDeviceClass_Commands()
	 * @model containment="true"
	 * @generated
	 */
	EList<Command> getCommands();

	/**
	 * Returns the value of the '<em><b>Dynamic Commands</b></em>' containment reference list.
	 * The list contents are of type {@link pogoDsl.Command}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Dynamic Commands</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Dynamic Commands</em>' containment reference list.
	 * @see pogoDsl.PogoDslPackage#getPogoDeviceClass_DynamicCommands()
	 * @model containment="true"
	 * @generated
	 */
	EList<Command> getDynamicCommands();

	/**
	 * Returns the value of the '<em><b>Attributes</b></em>' containment reference list.
	 * The list contents are of type {@link pogoDsl.Attribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Attributes</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Attributes</em>' containment reference list.
	 * @see pogoDsl.PogoDslPackage#getPogoDeviceClass_Attributes()
	 * @model containment="true"
	 * @generated
	 */
	EList<Attribute> getAttributes();

	/**
	 * Returns the value of the '<em><b>Dynamic Attributes</b></em>' containment reference list.
	 * The list contents are of type {@link pogoDsl.Attribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Dynamic Attributes</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Dynamic Attributes</em>' containment reference list.
	 * @see pogoDsl.PogoDslPackage#getPogoDeviceClass_DynamicAttributes()
	 * @model containment="true"
	 * @generated
	 */
	EList<Attribute> getDynamicAttributes();

	/**
	 * Returns the value of the '<em><b>Forwarded Attributes</b></em>' containment reference list.
	 * The list contents are of type {@link pogoDsl.ForwardedAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Forwarded Attributes</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Forwarded Attributes</em>' containment reference list.
	 * @see pogoDsl.PogoDslPackage#getPogoDeviceClass_ForwardedAttributes()
	 * @model containment="true"
	 * @generated
	 */
	EList<ForwardedAttribute> getForwardedAttributes();

	/**
	 * Returns the value of the '<em><b>Pipes</b></em>' containment reference list.
	 * The list contents are of type {@link pogoDsl.Pipe}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Pipes</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Pipes</em>' containment reference list.
	 * @see pogoDsl.PogoDslPackage#getPogoDeviceClass_Pipes()
	 * @model containment="true"
	 * @generated
	 */
	EList<Pipe> getPipes();

	/**
	 * Returns the value of the '<em><b>States</b></em>' containment reference list.
	 * The list contents are of type {@link pogoDsl.State}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>States</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>States</em>' containment reference list.
	 * @see pogoDsl.PogoDslPackage#getPogoDeviceClass_States()
	 * @model containment="true"
	 * @generated
	 */
	EList<State> getStates();

	/**
	 * Returns the value of the '<em><b>Preferences</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Preferences</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Preferences</em>' containment reference.
	 * @see #setPreferences(Preferences)
	 * @see pogoDsl.PogoDslPackage#getPogoDeviceClass_Preferences()
	 * @model containment="true"
	 * @generated
	 */
	Preferences getPreferences();

	/**
	 * Sets the value of the '{@link pogoDsl.PogoDeviceClass#getPreferences <em>Preferences</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Preferences</em>' containment reference.
	 * @see #getPreferences()
	 * @generated
	 */
	void setPreferences(Preferences value);

	/**
	 * Returns the value of the '<em><b>Additional Files</b></em>' containment reference list.
	 * The list contents are of type {@link pogoDsl.AdditionalFile}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Additional Files</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Additional Files</em>' containment reference list.
	 * @see pogoDsl.PogoDslPackage#getPogoDeviceClass_AdditionalFiles()
	 * @model containment="true"
	 * @generated
	 */
	EList<AdditionalFile> getAdditionalFiles();

	/**
	 * Returns the value of the '<em><b>Overloded Poll Period Object</b></em>' containment reference list.
	 * The list contents are of type {@link pogoDsl.OverlodedPollPeriodObject}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Overloded Poll Period Object</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Overloded Poll Period Object</em>' containment reference list.
	 * @see pogoDsl.PogoDslPackage#getPogoDeviceClass_OverlodedPollPeriodObject()
	 * @model containment="true"
	 * @generated
	 */
	EList<OverlodedPollPeriodObject> getOverlodedPollPeriodObject();

} // PogoDeviceClass
