/**
 */
package pogoDsl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Long Array Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see pogoDsl.PogoDslPackage#getLongArrayType()
 * @model
 * @generated
 */
public interface LongArrayType extends Type {
} // LongArrayType
