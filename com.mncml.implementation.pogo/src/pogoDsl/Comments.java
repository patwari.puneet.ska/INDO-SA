/**
 */
package pogoDsl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Comments</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link pogoDsl.Comments#getCommandsTable <em>Commands Table</em>}</li>
 * </ul>
 *
 * @see pogoDsl.PogoDslPackage#getComments()
 * @model
 * @generated
 */
public interface Comments extends EObject {
	/**
	 * Returns the value of the '<em><b>Commands Table</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Commands Table</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Commands Table</em>' attribute.
	 * @see #setCommandsTable(String)
	 * @see pogoDsl.PogoDslPackage#getComments_CommandsTable()
	 * @model
	 * @generated
	 */
	String getCommandsTable();

	/**
	 * Sets the value of the '{@link pogoDsl.Comments#getCommandsTable <em>Commands Table</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Commands Table</em>' attribute.
	 * @see #getCommandsTable()
	 * @generated
	 */
	void setCommandsTable(String value);

} // Comments
