/**
 */
package pogoDsl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Double Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see pogoDsl.PogoDslPackage#getDoubleType()
 * @model
 * @generated
 */
public interface DoubleType extends SimpleType, Type {
} // DoubleType
