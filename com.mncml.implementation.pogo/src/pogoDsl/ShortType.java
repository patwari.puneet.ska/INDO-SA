/**
 */
package pogoDsl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Short Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see pogoDsl.PogoDslPackage#getShortType()
 * @model
 * @generated
 */
public interface ShortType extends SimpleType, Type {
} // ShortType
