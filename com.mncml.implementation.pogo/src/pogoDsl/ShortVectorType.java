/**
 */
package pogoDsl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Short Vector Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see pogoDsl.PogoDslPackage#getShortVectorType()
 * @model
 * @generated
 */
public interface ShortVectorType extends VectorType {
} // ShortVectorType
