/**
 */
package pogoDsl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Boolean Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see pogoDsl.PogoDslPackage#getBooleanType()
 * @model
 * @generated
 */
public interface BooleanType extends SimpleType, Type {
} // BooleanType
