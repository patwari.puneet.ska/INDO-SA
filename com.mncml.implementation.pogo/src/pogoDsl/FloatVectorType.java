/**
 */
package pogoDsl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Float Vector Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see pogoDsl.PogoDslPackage#getFloatVectorType()
 * @model
 * @generated
 */
public interface FloatVectorType extends VectorType {
} // FloatVectorType
