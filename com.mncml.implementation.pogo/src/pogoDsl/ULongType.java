/**
 */
package pogoDsl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ULong Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see pogoDsl.PogoDslPackage#getULongType()
 * @model
 * @generated
 */
public interface ULongType extends Type {
} // ULongType
