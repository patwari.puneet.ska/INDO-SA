/**
 */
package pogoDsl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>State Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see pogoDsl.PogoDslPackage#getStateType()
 * @model
 * @generated
 */
public interface StateType extends Type {
} // StateType
