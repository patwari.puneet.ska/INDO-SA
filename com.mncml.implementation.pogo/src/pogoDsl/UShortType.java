/**
 */
package pogoDsl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>UShort Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see pogoDsl.PogoDslPackage#getUShortType()
 * @model
 * @generated
 */
public interface UShortType extends SimpleType, Type {
} // UShortType
