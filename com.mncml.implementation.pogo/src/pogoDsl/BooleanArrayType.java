/**
 */
package pogoDsl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Boolean Array Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see pogoDsl.PogoDslPackage#getBooleanArrayType()
 * @model
 * @generated
 */
public interface BooleanArrayType extends Type {
} // BooleanArrayType
