/**
 */
package pogoDsl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Int Vector Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see pogoDsl.PogoDslPackage#getIntVectorType()
 * @model
 * @generated
 */
public interface IntVectorType extends VectorType {
} // IntVectorType
