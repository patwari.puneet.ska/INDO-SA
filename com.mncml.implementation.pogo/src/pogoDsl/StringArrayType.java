/**
 */
package pogoDsl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>String Array Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see pogoDsl.PogoDslPackage#getStringArrayType()
 * @model
 * @generated
 */
public interface StringArrayType extends Type {
} // StringArrayType
