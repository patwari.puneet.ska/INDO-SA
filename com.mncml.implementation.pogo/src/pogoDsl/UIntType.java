/**
 */
package pogoDsl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>UInt Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see pogoDsl.PogoDslPackage#getUIntType()
 * @model
 * @generated
 */
public interface UIntType extends SimpleType, Type {
} // UIntType
