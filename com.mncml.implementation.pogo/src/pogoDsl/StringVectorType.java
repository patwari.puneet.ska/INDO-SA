/**
 */
package pogoDsl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>String Vector Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see pogoDsl.PogoDslPackage#getStringVectorType()
 * @model
 * @generated
 */
public interface StringVectorType extends VectorType {
} // StringVectorType
