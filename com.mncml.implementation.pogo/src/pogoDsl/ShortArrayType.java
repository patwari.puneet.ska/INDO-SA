/**
 */
package pogoDsl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Short Array Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see pogoDsl.PogoDslPackage#getShortArrayType()
 * @model
 * @generated
 */
public interface ShortArrayType extends Type {
} // ShortArrayType
