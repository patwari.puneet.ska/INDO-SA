/**
 */
package pogoDsl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Double Vector Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see pogoDsl.PogoDslPackage#getDoubleVectorType()
 * @model
 * @generated
 */
public interface DoubleVectorType extends VectorType {
} // DoubleVectorType
