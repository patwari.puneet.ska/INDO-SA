/**
 */
package pogoDsl;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Double String Array Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see pogoDsl.PogoDslPackage#getDoubleStringArrayType()
 * @model
 * @generated
 */
public interface DoubleStringArrayType extends Type {
} // DoubleStringArrayType
