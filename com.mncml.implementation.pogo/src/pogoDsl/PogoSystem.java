/**
 */
package pogoDsl;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Pogo System</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link pogoDsl.PogoSystem#getImports <em>Imports</em>}</li>
 *   <li>{@link pogoDsl.PogoSystem#getClasses <em>Classes</em>}</li>
 *   <li>{@link pogoDsl.PogoSystem#getMultiClasses <em>Multi Classes</em>}</li>
 * </ul>
 *
 * @see pogoDsl.PogoDslPackage#getPogoSystem()
 * @model
 * @generated
 */
public interface PogoSystem extends EObject {
	/**
	 * Returns the value of the '<em><b>Imports</b></em>' containment reference list.
	 * The list contents are of type {@link pogoDsl.Import}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Imports</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Imports</em>' containment reference list.
	 * @see pogoDsl.PogoDslPackage#getPogoSystem_Imports()
	 * @model containment="true"
	 * @generated
	 */
	EList<Import> getImports();

	/**
	 * Returns the value of the '<em><b>Classes</b></em>' containment reference list.
	 * The list contents are of type {@link pogoDsl.PogoDeviceClass}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Classes</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Classes</em>' containment reference list.
	 * @see pogoDsl.PogoDslPackage#getPogoSystem_Classes()
	 * @model containment="true"
	 * @generated
	 */
	EList<PogoDeviceClass> getClasses();

	/**
	 * Returns the value of the '<em><b>Multi Classes</b></em>' containment reference list.
	 * The list contents are of type {@link pogoDsl.PogoMultiClasses}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Multi Classes</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Multi Classes</em>' containment reference list.
	 * @see pogoDsl.PogoDslPackage#getPogoSystem_MultiClasses()
	 * @model containment="true"
	 * @generated
	 */
	EList<PogoMultiClasses> getMultiClasses();

} // PogoSystem
