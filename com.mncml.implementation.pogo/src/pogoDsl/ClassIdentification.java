/**
 */
package pogoDsl;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Class Identification</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link pogoDsl.ClassIdentification#getContact <em>Contact</em>}</li>
 *   <li>{@link pogoDsl.ClassIdentification#getAuthor <em>Author</em>}</li>
 *   <li>{@link pogoDsl.ClassIdentification#getEmailDomain <em>Email Domain</em>}</li>
 *   <li>{@link pogoDsl.ClassIdentification#getClassFamily <em>Class Family</em>}</li>
 *   <li>{@link pogoDsl.ClassIdentification#getSiteSpecific <em>Site Specific</em>}</li>
 *   <li>{@link pogoDsl.ClassIdentification#getPlatform <em>Platform</em>}</li>
 *   <li>{@link pogoDsl.ClassIdentification#getBus <em>Bus</em>}</li>
 *   <li>{@link pogoDsl.ClassIdentification#getManufacturer <em>Manufacturer</em>}</li>
 *   <li>{@link pogoDsl.ClassIdentification#getReference <em>Reference</em>}</li>
 *   <li>{@link pogoDsl.ClassIdentification#getKeyWords <em>Key Words</em>}</li>
 * </ul>
 *
 * @see pogoDsl.PogoDslPackage#getClassIdentification()
 * @model
 * @generated
 */
public interface ClassIdentification extends EObject {
	/**
	 * Returns the value of the '<em><b>Contact</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Contact</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Contact</em>' attribute.
	 * @see #setContact(String)
	 * @see pogoDsl.PogoDslPackage#getClassIdentification_Contact()
	 * @model
	 * @generated
	 */
	String getContact();

	/**
	 * Sets the value of the '{@link pogoDsl.ClassIdentification#getContact <em>Contact</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Contact</em>' attribute.
	 * @see #getContact()
	 * @generated
	 */
	void setContact(String value);

	/**
	 * Returns the value of the '<em><b>Author</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Author</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Author</em>' attribute.
	 * @see #setAuthor(String)
	 * @see pogoDsl.PogoDslPackage#getClassIdentification_Author()
	 * @model
	 * @generated
	 */
	String getAuthor();

	/**
	 * Sets the value of the '{@link pogoDsl.ClassIdentification#getAuthor <em>Author</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Author</em>' attribute.
	 * @see #getAuthor()
	 * @generated
	 */
	void setAuthor(String value);

	/**
	 * Returns the value of the '<em><b>Email Domain</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Email Domain</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Email Domain</em>' attribute.
	 * @see #setEmailDomain(String)
	 * @see pogoDsl.PogoDslPackage#getClassIdentification_EmailDomain()
	 * @model
	 * @generated
	 */
	String getEmailDomain();

	/**
	 * Sets the value of the '{@link pogoDsl.ClassIdentification#getEmailDomain <em>Email Domain</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Email Domain</em>' attribute.
	 * @see #getEmailDomain()
	 * @generated
	 */
	void setEmailDomain(String value);

	/**
	 * Returns the value of the '<em><b>Class Family</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Class Family</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Class Family</em>' attribute.
	 * @see #setClassFamily(String)
	 * @see pogoDsl.PogoDslPackage#getClassIdentification_ClassFamily()
	 * @model
	 * @generated
	 */
	String getClassFamily();

	/**
	 * Sets the value of the '{@link pogoDsl.ClassIdentification#getClassFamily <em>Class Family</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Class Family</em>' attribute.
	 * @see #getClassFamily()
	 * @generated
	 */
	void setClassFamily(String value);

	/**
	 * Returns the value of the '<em><b>Site Specific</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Site Specific</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Site Specific</em>' attribute.
	 * @see #setSiteSpecific(String)
	 * @see pogoDsl.PogoDslPackage#getClassIdentification_SiteSpecific()
	 * @model
	 * @generated
	 */
	String getSiteSpecific();

	/**
	 * Sets the value of the '{@link pogoDsl.ClassIdentification#getSiteSpecific <em>Site Specific</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Site Specific</em>' attribute.
	 * @see #getSiteSpecific()
	 * @generated
	 */
	void setSiteSpecific(String value);

	/**
	 * Returns the value of the '<em><b>Platform</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Platform</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Platform</em>' attribute.
	 * @see #setPlatform(String)
	 * @see pogoDsl.PogoDslPackage#getClassIdentification_Platform()
	 * @model
	 * @generated
	 */
	String getPlatform();

	/**
	 * Sets the value of the '{@link pogoDsl.ClassIdentification#getPlatform <em>Platform</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Platform</em>' attribute.
	 * @see #getPlatform()
	 * @generated
	 */
	void setPlatform(String value);

	/**
	 * Returns the value of the '<em><b>Bus</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Bus</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Bus</em>' attribute.
	 * @see #setBus(String)
	 * @see pogoDsl.PogoDslPackage#getClassIdentification_Bus()
	 * @model
	 * @generated
	 */
	String getBus();

	/**
	 * Sets the value of the '{@link pogoDsl.ClassIdentification#getBus <em>Bus</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Bus</em>' attribute.
	 * @see #getBus()
	 * @generated
	 */
	void setBus(String value);

	/**
	 * Returns the value of the '<em><b>Manufacturer</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Manufacturer</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Manufacturer</em>' attribute.
	 * @see #setManufacturer(String)
	 * @see pogoDsl.PogoDslPackage#getClassIdentification_Manufacturer()
	 * @model
	 * @generated
	 */
	String getManufacturer();

	/**
	 * Sets the value of the '{@link pogoDsl.ClassIdentification#getManufacturer <em>Manufacturer</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Manufacturer</em>' attribute.
	 * @see #getManufacturer()
	 * @generated
	 */
	void setManufacturer(String value);

	/**
	 * Returns the value of the '<em><b>Reference</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Reference</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Reference</em>' attribute.
	 * @see #setReference(String)
	 * @see pogoDsl.PogoDslPackage#getClassIdentification_Reference()
	 * @model
	 * @generated
	 */
	String getReference();

	/**
	 * Sets the value of the '{@link pogoDsl.ClassIdentification#getReference <em>Reference</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Reference</em>' attribute.
	 * @see #getReference()
	 * @generated
	 */
	void setReference(String value);

	/**
	 * Returns the value of the '<em><b>Key Words</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.String}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Key Words</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Key Words</em>' attribute list.
	 * @see pogoDsl.PogoDslPackage#getClassIdentification_KeyWords()
	 * @model unique="false"
	 * @generated
	 */
	EList<String> getKeyWords();

} // ClassIdentification
