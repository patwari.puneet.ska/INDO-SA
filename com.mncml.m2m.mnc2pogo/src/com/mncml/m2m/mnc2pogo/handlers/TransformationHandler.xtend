package com.mncml.m2m.mnc2pogo.handlers

import com.mncml.mnc2pogo.transformation.Mnc2PogoTransformation
import mncModel.ControlNode
import mncModel.Model
import org.eclipse.core.commands.AbstractHandler
import org.eclipse.core.commands.ExecutionEvent
import org.eclipse.core.commands.ExecutionException
import org.eclipse.core.commands.IHandler
import org.eclipse.core.resources.IFile
import org.eclipse.core.resources.IResource
import org.eclipse.core.runtime.CoreException
import org.eclipse.core.runtime.NullProgressMonitor
import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.jdt.core.JavaCore
import org.eclipse.jface.dialogs.MessageDialog
import org.eclipse.ui.PartInitException
import org.eclipse.ui.PlatformUI
import org.eclipse.ui.handlers.HandlerUtil
import org.eclipse.ui.part.FileEditorInput
import org.eclipse.xtext.builder.EclipseResourceFileSystemAccess
import org.eclipse.xtext.resource.XtextResource
import org.eclipse.xtext.ui.editor.XtextEditor
import org.eclipse.xtext.util.concurrent.IUnitOfWork
import pogoDsl.PogoDslFactory
import pogoDsl.PogoSystem
import org.eclipse.jdt.core.IPackageFragment
import org.eclipse.ui.ide.IDE

/** 
 * Our sample handler extends AbstractHandler, an IHandler base class.
 * @see IHandler
 * @see AbstractHandler
 */
class TransformationHandler extends AbstractHandler {

	/** 
	 * The constructor.
	 */
	new() {
	}

	/** 
	 * the command has been executed, so extract extract the needed information
	 * from the application context.
	 */
	override Object execute(ExecutionEvent event) throws ExecutionException {

		val generate = MessageDialog.openConfirm(HandlerUtil.getActiveShell(event), "Generate Tango View",
			"Do you want to Tango View for the MnC Specifications ?");
		if (!generate)
			return null;
		val activeEditor = HandlerUtil.getActiveEditor(event);
		val file = activeEditor.getEditorInput().getAdapter(IFile) as IFile;
		if (file !== null) {
			val project = file.getProject();
			val srcGenFolder = project.getFolder("src-gen");
			var javaProject = JavaCore.create(project)
			var srcRoot = javaProject.getPackageFragmentRoot(srcGenFolder);
			if (!srcGenFolder.exists()) {
				try {
					srcGenFolder.create(true, true, new NullProgressMonitor());

				} catch (CoreException e) {
					e.printStackTrace
				}
			}
			val tangoSrcFolder = srcRoot.createPackageFragment("com.mnc.implementation.pogo", true, null)
			val fsa = new EclipseResourceFileSystemAccess
			try {
				fsa.root = (project.getWorkspace().getRoot());
//		fsa.project = project
				fsa.setOutputPath(tangoSrcFolder.path.toString());
				var teste = fsa.getOutputConfigurations();
				fsa.root = project.workspace.root
				var it = teste.entrySet().iterator();

				// make a new Outputconfiguration <- needed
				while (it.hasNext()) {

					var next = it.next();
					var out = next.getValue();
					out.isOverrideExistingResources();
					out.setCreateOutputDirectory(true); // <--- do not touch this
				}

				if (activeEditor instanceof XtextEditor) {
					(activeEditor as XtextEditor).getDocument().readOnly(new IUnitOfWork<Boolean, XtextResource>() {

						override exec(XtextResource state) throws Exception {
							val res = state as Resource;
							project.refreshLocal(IResource.DEPTH_INFINITE, null);
							if (res !== null) {

								val pogoSystem = new Mnc2PogoTransformation().transform(res) as PogoSystem
								var mncSystem = res.contents.get(0) as Model
								if (mncSystem.systems.get(1) !== null) {
									val controlNode = mncSystem.eContents.filter(ControlNode).get(0)

									if (pogoSystem !== null && pogoSystem instanceof PogoSystem) {

										var pogoDeviceClassName = pogoSystem.classes.head.name
										pogoSystem.classes.head.refferedControlNode = controlNode
										var pogoSystemModel = PogoDslFactory.eINSTANCE.createPogoSystem => [
											imports.addAll(pogoSystem.imports)
											classes.addAll(pogoSystem.classes)
											multiClasses.addAll(pogoSystem.multiClasses)
										]

										fsa.generateFile(pogoDeviceClassName + ".pogo", "")

										var pogoRes = res.resourceSet.createResource(
											URI.createFileURI(
												tangoSrcFolder.path.toString() + "/" + pogoDeviceClassName + ".pogo"));
										pogoRes.contents += pogoSystemModel
										pogoRes.save(null)

										var consideredFolder = project.getFolder("src-gen/com/mnc/implementation/pogo/")
										var xtextGeneratedFile = consideredFolder.getFile(pogoDeviceClassName + ".pogo");

//										var wb = PlatformUI.getWorkbench();
//										var win = wb.getActiveWorkbenchWindow();
//										var page = win.getActivePage();
//										var part = IDE.openEditor(page, xtextGeneratedFile, true);

                                      openFileInEditor(xtextGeneratedFile,event)

									}
								}
								return Boolean.TRUE;
							}

						}

					})
				}
			} catch (Exception e) {
				e.printStackTrace
			}
			return null;
		} 

	}

	def openFileInEditor(IFile file, ExecutionEvent event) {

		try { // Get the active page.
			var wb = PlatformUI.getWorkbench();
			var win = wb.getActiveWorkbenchWindow();

			// on new versions it may need to be changed to:
			var page = win.getActivePage();
			// Figure out the default editor for the file type based on extension.
			var desc = PlatformUI.getWorkbench().getEditorRegistry().getDefaultEditor(file.getName());
			if (desc === null) {
				MessageDialog.openError(HandlerUtil.getActiveShell(event), "Editor open error",
					"Unable to find a suitable editor to open file.");
			} else {
				// Try opening the page in the editor.
				page.openEditor(new FileEditorInput(file), desc.getId());

			}

		} catch (PartInitException e) {
			e.printStackTrace
		}
	}
}
