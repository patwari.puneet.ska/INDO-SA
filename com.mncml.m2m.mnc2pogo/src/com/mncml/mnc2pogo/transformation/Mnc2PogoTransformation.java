package com.mncml.mnc2pogo.transformation;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.xmi.XMIResource;
import org.eclipse.emf.ecore.xmi.impl.EcoreResourceFactoryImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceImpl;
import org.eclipse.m2m.atl.core.ATLCoreException;
import org.eclipse.m2m.atl.core.IReferenceModel;
import org.eclipse.m2m.atl.core.ModelFactory;
import org.eclipse.m2m.atl.core.emf.EMFInjector;
import org.eclipse.m2m.atl.core.emf.EMFModel;
import org.eclipse.m2m.atl.core.emf.EMFModelFactory;
import org.eclipse.m2m.atl.core.launch.ILauncher;
import org.eclipse.m2m.atl.engine.emfvm.launch.EMFVMLauncher;
import org.eclipse.xtext.resource.XtextResource;
import org.eclipse.xtext.resource.XtextResourceSet;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Module;
import com.mncml.dsl.MncRuntimeModule;
import com.mncml.dsl.MncStandaloneSetup;
import com.mncml.implementation.pogo.dsl.PogoDslRuntimeModule;
import com.mncml.implementation.pogo.dsl.PogoDslStandaloneSetup;

import mncModel.MncModelPackage;
import pogoDsl.PogoDslPackage;

public class Mnc2PogoTransformation {

	/**
	 * @param args
	 * @throws ATLCoreException
	 * @throws IOException
	 */

	private static Properties properties;

	private EMFModel inModel;

	private EMFModel outModel;

	String concernedPaths;

	String mncMetaModelUri;

	String pogoMetaModelUri;

	// PogoDslStandaloneSetup pogoStandAlone;

	// static MncStandaloneSetup mncStandAlone;

	public Mnc2PogoTransformation() throws IOException {
		properties = new Properties();
		properties.load(getFileURL("Mnc2pogo.properties").openStream());
		Resource.Factory.Registry.INSTANCE.getExtensionToFactoryMap().put("ecore", new EcoreResourceFactoryImpl());

		concernedPaths = Mnc2PogoTransformation.class.getProtectionDomain().getCodeSource().getLocation().getPath()
				.toString();
		concernedPaths = concernedPaths.replace("/com.mncml.m2m.mnc2pogo", "");
		mncMetaModelUri = org.eclipse.emf.common.util.URI.createFileURI(concernedPaths + getMetamodelUri("Mnc"))
				.toString();

		pogoMetaModelUri = org.eclipse.emf.common.util.URI.createFileURI(concernedPaths + getMetamodelUri("Pogo"))
				.toString();

	}

	protected static URL getFileURL(String fileName) throws IOException {
		final URL fileURL;
		if (isEclipseRunning()) {
			URL resourceURL = Mnc2PogoTransformation.class.getResource(fileName);
			if (resourceURL != null) {
				fileURL = FileLocator.toFileURL(resourceURL);
			} else {
				fileURL = null;
			}
		} else {
			fileURL = Mnc2PogoTransformation.class.getResource(fileName);
		}
		if (fileURL == null) {
			throw new IOException("'" + fileName + "' not found");
		} else {
			return fileURL;
		}
	}

	public static boolean isEclipseRunning() {
		try {
			return Platform.isRunning();
		} catch (Throwable exception) {
			// Assume that we aren't running.
		}
		return false;
	}

	public static void main(String[] args) throws ATLCoreException, IOException {
		// mncStandAlone = new MncStandaloneSetup();
		// Injector xtextinjector =
		// mncStandAlone.createInjectorAndDoEMFRegistration();
		// Injector xtextinjector = Guice.createInjector(new
		// MncRuntimeModule());
		// XtextResourceSet mncResourceSet =
		// xtextinjector.getInstance(XtextResourceSet.class);
		// mncResourceSet.addLoadOption(XtextResource.OPTION_RESOLVE_ALL,
		// Boolean.TRUE);
		// mncResourceSet.getPackageRegistry().put(MncModelPackage.eINSTANCE.getNsURI(),
		// MncModelPackage.eINSTANCE);
		// Resource resource = mncResourceSet
		// .getResource(URI.createURI("source/LMC.mncspec"), true);
		// new Mnc2PogoTransformation().transform(resource);
	}

	public void loadModels(Resource res) throws ATLCoreException, IOException {
		// Replace DSL by your Language name

		// Injector xtextinjector =
		// mncStandAlone.createInjectorAndDoEMFRegistration();
		Injector xtextinjector = Guice.createInjector((Module) new MncRuntimeModule());
		XtextResourceSet mncResourceSet = xtextinjector.getInstance(XtextResourceSet.class);
		mncResourceSet.addLoadOption(XtextResource.OPTION_RESOLVE_ALL, Boolean.TRUE);
		mncResourceSet.getPackageRegistry().put(MncModelPackage.eINSTANCE.getNsURI(), MncModelPackage.eINSTANCE);
		Resource resource = mncResourceSet.getResource(res.getURI(), true);

		XMIResourceFactoryImpl resFactory = new XMIResourceFactoryImpl();
		URI xmiuri = URI.createFileURI("generated/Pogo123.xmi");
		XMIResource xmiresource = (XMIResource) (resFactory.createResource(xmiuri));
		xmiresource.getContents().addAll(resource.getContents());
		xmiresource.save(new HashMap());
		InputStream[] is = new InputStream[1];
		{
			final InputStream[] _wrVal_is = is;
			FileInputStream _fileInputStream = new FileInputStream("generated/Pogo123.xmi");
			_wrVal_is[0] = _fileInputStream;
		}
		Map<String, Object> options = new HashMap<String, Object>();

		// Load the DSL metamodel
		ModelFactory factory = new EMFModelFactory();
		EMFInjector injector = new EMFInjector();

		IReferenceModel mncMetamodel = factory.newReferenceModel();
		injector.inject(mncMetamodel, mncMetaModelUri);
		IReferenceModel pogoMetamodel = factory.newReferenceModel();
		injector.inject(pogoMetamodel, pogoMetaModelUri);

		// Load the XtextResource as an EMFModel
		this.inModel = (EMFModel) factory.newModel(mncMetamodel);
		injector.inject(inModel, is[0], options);

		this.outModel = (EMFModel) factory.newModel(pogoMetamodel);

		// Resource pogoresource = new
		// ResourceFactoryImpl().createResource(org.eclipse.emf.common.util.URI.createFileURI("bin/com/mncml/m2m/mnc2pogo/files/Pogo2.xmi"));
		// emfinjector.inject(outMetamodel, pogoresource);
	}

	public EObject transform(Resource res) throws ATLCoreException, IOException {

		loadModels(res);
		doMnc2Pogo();
		return saveModel();

	}

	public void doMnc2Pogo() throws IOException {

		ILauncher launcher = new EMFVMLauncher();
		Map<String, Object> launcherOptions = getOptions();
		launcher.initialize(launcherOptions);
		launcher.addInModel(inModel, "IN", "Mnc");
		launcher.addOutModel(outModel, "OUT", "Pogo");
		launcher.launch("run", new NullProgressMonitor(), launcherOptions, (Object[]) getModulesList());

	}

	public EObject saveModel() throws IOException {
		File file = new File("generated/Pogo123.xmi");
		FileOutputStream fop = new FileOutputStream(file);
		// if file doesnt exists, then create it
		if (!file.exists()) {
			file.createNewFile();
		}
		Map<String, Object> launcherOptions = getOptions();
		this.outModel.getResource().save(fop, launcherOptions);
		Injector xtextInjector = Guice.createInjector(new PogoDslRuntimeModule());
		XtextResourceSet pogoResourceSet = xtextInjector.getInstance(XtextResourceSet.class);
		pogoResourceSet.getPackageRegistry().put(PogoDslPackage.eINSTANCE.getNsURI(), PogoDslPackage.eINSTANCE);
		XMIResourceImpl resource = new XMIResourceImpl();

		resource.load(new FileInputStream(file), new HashMap<Object, Object>());
		EObject data = (EObject) resource.getContents().get(0);
		return data;

	}

	public static Map<String, Object> getOptions() {
		Map<String, Object> options = new HashMap<String, Object>();
		for (Entry<Object, Object> entry : properties.entrySet()) {
			if (entry.getKey().toString().startsWith("Mnc2pogo.options.")) {
				options.put(entry.getKey().toString().replaceFirst("Mnc2pogo.options.", ""),
						entry.getValue().toString());
			}
		}
		return options;
	}

	protected String getMetamodelUri(String metamodelName) {
		return properties.getProperty("Mnc2pogo.metamodels." + metamodelName);
	}

	public InputStream[] getModulesList() throws IOException {
		InputStream[] modules = null;
		String modulesList = properties.getProperty("Mnc2pogo.modules");
		if (modulesList != null) {
			String[] moduleNames = modulesList.split(",");
			modules = new InputStream[moduleNames.length];
			for (int i = 0; i < moduleNames.length; i++) {
				String asmModulePath = new Path(moduleNames[i].trim()).removeFileExtension().addFileExtension("asm")
						.toString();
				modules[i] = getFileURL(asmModulePath).openStream();
			}
		}
		return modules;
	}

}
