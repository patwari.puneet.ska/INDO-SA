package com.mncml.implementation.pogo.emulator.handlers

import com.google.inject.Guice
import fr.esrf.tango.pogo.generator.java.JavaDevice
import java.io.BufferedInputStream
import java.io.File
import java.io.FileInputStream
import java.net.URI
import java.util.List
import java.util.Set
import org.eclipse.core.commands.AbstractHandler
import org.eclipse.core.commands.ExecutionEvent
import org.eclipse.core.commands.ExecutionException
import org.eclipse.core.commands.IHandler
import org.eclipse.core.resources.IFile
import org.eclipse.core.runtime.CoreException
import org.eclipse.core.runtime.FileLocator
import org.eclipse.core.runtime.NullProgressMonitor
import org.eclipse.core.runtime.Platform
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.jdt.core.IClasspathEntry
import org.eclipse.jdt.core.JavaCore
import org.eclipse.jface.dialogs.MessageDialog
import org.eclipse.ui.handlers.HandlerUtil
import org.eclipse.xtext.builder.EclipseResourceFileSystemAccess
import org.eclipse.xtext.resource.XtextResource
import org.eclipse.xtext.service.AbstractGenericModule
import org.eclipse.xtext.ui.editor.XtextEditor
import org.eclipse.xtext.util.concurrent.IUnitOfWork

/** 
 * Our sample handler extends AbstractHandler, an IHandler base class.
 * @see IHandler
 * @see AbstractHandler
 */
class GenerateTangoCodeHandler extends AbstractHandler {

	override execute(ExecutionEvent event) throws ExecutionException {

		var is = null as BufferedInputStream
		val generate = MessageDialog.openConfirm(HandlerUtil.getActiveShell(event), "Generate JAVA",
			"Do you want to generate Mnc powered JAVA code?");
		if (!generate) {
			return null
		} else {
			var activeEditor = HandlerUtil.getActiveEditor(event);
			var file = activeEditor.getEditorInput().getAdapter(IFile) as IFile
			if (file !== null) {
				var project = file.getProject();
				var javaProject = JavaCore.create(project)
				var srcGenFolder = project.getFolder("src-gen");

				if (!srcGenFolder.exists())
						srcGenFolder.create(true, true, new NullProgressMonitor)
 
	 			var srcRoot = javaProject.getPackageFragmentRoot(srcGenFolder);
		 		val tangoSrcFolder = srcRoot.createPackageFragment("", true, null)
				var oldEntries = javaProject.getRawClasspath();
				var List<IClasspathEntry> newEntries = newArrayList(oldEntries)

				// Check if the newENtry is already present in oldEntries of sources
				var newEntry = JavaCore.newSourceEntry(srcRoot.getPath)
				newEntries.add(newEntry);
				for (ce : oldEntries)
					if (ce.equals(newEntry))
						newEntries.remove(newEntry)

				try {
					// Putting libraries in classpath of runtime java project
					var jarFolder = project.getFolder("jars")
					if (!jarFolder.exists())
						jarFolder.create(true, true, null)

					var fileURL = Platform.getBundle("com.mncml.dsl.libs").getEntry("libs")
					var resolvedFileLibsURL = FileLocator.toFileURL(fileURL)
					var fileLibs = new File(new URI(resolvedFileLibsURL.protocol, resolvedFileLibsURL.path, null))
					var jars = fileLibs.listFiles.toList
					for (f : jars) {
						if (f.name.contains("JTango") || f.name.contains("SKA_Dependencies") ||
							f.name.contains("json") || f.name.contains("testng")) {
							is = new BufferedInputStream(new FileInputStream(f))
							var jarFile = project.getFile("jars/" + f.name)
							if (!jarFile.exists)
								jarFile.create(is, true, null)
							var jarPath = jarFile.fullPath
							newEntries.add(JavaCore.newLibraryEntry(jarPath, null, null))
							is.close
						}
					}

					var Set<IClasspathEntry> setOfEntries = newHashSet(newEntries)
					javaProject.setRawClasspath(setOfEntries, new NullProgressMonitor)
				} catch (CoreException e) {
					e.printStackTrace
					return null;
				} finally {
//						is.close
				}
 
				val fsa = new EclipseResourceFileSystemAccess
				fsa.setOutputPath(tangoSrcFolder.path.toString());
				var teste = fsa.getOutputConfigurations();
				fsa.root = project.workspace.root
				var it = teste.entrySet().iterator();

				// make a new Outputconfiguration <- needed
				while (it.hasNext()) {

					var next = it.next();
					var out = next.getValue();
					out.isOverrideExistingResources();
					out.setCreateOutputDirectory(true); // <--- do not touch this
				}

				if (activeEditor instanceof XtextEditor) {
					(activeEditor as XtextEditor).getDocument().readOnly(new IUnitOfWork<Boolean, XtextResource>() {

						override public Boolean exec(XtextResource state) throws Exception {

							Guice.createInjector(new AbstractGenericModule() {
							}).getInstance(JavaDevice).doGenerate(state as Resource, fsa)
							return Boolean.TRUE;
						}
					});

				}
			}
			return null;

		}
	}
}
