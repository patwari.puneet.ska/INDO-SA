/*
 * generated by Xtext 2.11.0
 */
package com.mncml.implementation.pogo.dsl.ui

import org.eclipse.xtend.lib.annotations.FinalFieldsConstructor

/**
 * Use this class to register components to be used within the Eclipse IDE.
 */
@FinalFieldsConstructor
class PogoDslUiModule extends AbstractPogoDslUiModule {
}
