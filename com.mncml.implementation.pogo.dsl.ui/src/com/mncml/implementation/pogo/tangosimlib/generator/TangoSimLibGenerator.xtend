package com.mncml.implementation.pogo.tangosimlib.generator

import com.google.inject.Guice
import com.mncml.implementation.pogo.SimulatorDslRuntimeModule
import org.eclipse.core.commands.AbstractHandler
import org.eclipse.core.commands.ExecutionEvent
import org.eclipse.core.commands.ExecutionException
import org.eclipse.core.resources.IFile
import org.eclipse.core.resources.IResource
import org.eclipse.core.runtime.CoreException
import org.eclipse.core.runtime.NullProgressMonitor
import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.emf.ecore.resource.ResourceSet
import org.eclipse.jdt.core.IPackageFragment
import org.eclipse.jdt.core.JavaCore
import org.eclipse.jface.dialogs.MessageDialog
import org.eclipse.ui.PartInitException
import org.eclipse.ui.PlatformUI
import org.eclipse.ui.handlers.HandlerUtil
import org.eclipse.ui.part.FileEditorInput
import org.eclipse.xtext.builder.EclipseResourceFileSystemAccess
import org.eclipse.xtext.resource.XtextResource
import org.eclipse.xtext.ui.editor.XtextEditor
import org.eclipse.xtext.util.concurrent.IUnitOfWork
import pogoDsl.PogoSystem
import tangoSimLibSimulator.TangoSimLibSimulatorFactory

class TangoSimLibGenerator extends AbstractHandler {

	new() {
	}

	override execute(ExecutionEvent event) throws ExecutionException {
		val generate = MessageDialog.openConfirm(HandlerUtil.getActiveShell(event), "Create TangoSimLib Specs ?",
			"Do you want to create TangoSimLib specifications file ?");
		if (!generate)
			return null;
		val activeEditor = HandlerUtil.getActiveEditor(event);
		val file = activeEditor.getEditorInput().getAdapter(IFile) as IFile;
		if (file !== null) {
			val project = file.getProject();
			val srcGenFolder = project.getFolder("src-gen");
			var javaProject = JavaCore.create(project)
			var srcRoot = javaProject.getPackageFragmentRoot(srcGenFolder)
			if (!srcGenFolder.exists()) {
				try {
					srcGenFolder.create(true, true, new NullProgressMonitor());

				} catch (CoreException e) {
					e.printStackTrace
				}
			}
			val tangoSrcFolder = srcRoot.createPackageFragment("com.mnc.implementation.pogo.simulator", true, null)
			val fsa = new EclipseResourceFileSystemAccess

			fsa.root = (project.getWorkspace().getRoot());
//		fsa.project = project
			fsa.setOutputPath(tangoSrcFolder.path.toString());
			var teste = fsa.getOutputConfigurations();
			fsa.root = project.workspace.root
			var it = teste.entrySet().iterator();

			// make a new Outputconfiguration <- needed
			while (it.hasNext()) {

				var next = it.next();
				var out = next.getValue();
				out.isOverrideExistingResources();
				out.setCreateOutputDirectory(true); // <--- do not touch this
			}

			if (activeEditor instanceof XtextEditor) {
				(activeEditor as XtextEditor).getDocument().readOnly(new IUnitOfWork<Boolean, XtextResource>() {

					override exec(XtextResource state) throws Exception {
						val res = state as Resource;
						project.refreshLocal(IResource.DEPTH_INFINITE, null)
						if (res !== null && res.contents !== null && res.contents.length !== 0) {

							val pogoSystem = res.contents.head as PogoSystem

							if (pogoSystem !== null && pogoSystem instanceof PogoSystem) {
								try {
									val pogoDeviceClass = pogoSystem.classes.head
									var injector = Guice.createInjector(new SimulatorDslRuntimeModule);
//									var serializer = injector.getInstance(Serializer);
									var simDsl = TangoSimLibSimulatorFactory.eINSTANCE.createTangoSimLib => [
										refferedPogoDeviceClass = pogoDeviceClass

										for (attribute : pogoDeviceClass.attributes) {
											dataSimulations.add(
												TangoSimLibSimulatorFactory.eINSTANCE.createDataSimulation => [
													pogoAttr = attribute

												])

										}
										for (command : pogoDeviceClass.commands) {
											commandSimulations.add(
												TangoSimLibSimulatorFactory.eINSTANCE.createCommandSimulation => [
													pogoCommand = command
												])
										}
									]

									var rs = injector.getInstance(ResourceSet)
									var resource = rs.createResource(
										URI.createURI(srcGenFolder.projectRelativePath.toString))
									resource.contents += simDsl
//								var simDslText = serializer.serialize(simDsl);
									fsa.generateFile(pogoDeviceClass.name + ".tsl", "")
//									var resSet = new ResourceSetImpl();
									var simLibRes = res.resourceSet.getResource(
										URI.createFileURI(
											tangoSrcFolder.path.toString() + "/" + pogoDeviceClass.name + ".tsl"),
										true);
										simLibRes.contents.set(0, simDsl)

										simLibRes.save(null)
										var consideredFolder = project.getFolder(
											"src-gen/com/mnc/implementation/pogo/simulator/")
										var xtextGeneratedFile = consideredFolder.getFile(pogoDeviceClass.name +
											".tsl");
										openFileInEditor(xtextGeneratedFile, event)
									} catch (Exception e) {
										e.printStackTrace
									}
									return Boolean.TRUE;

								}

							}

						}

					})
					return null;

				}

			}

		}

		def openFileInEditor(IFile file, ExecutionEvent event) {

			// Get the active page.
			var wb = PlatformUI.getWorkbench();
			var win = wb.getActiveWorkbenchWindow();

			// on new versions it may need to be changed to:
			var page = win.getActivePage();
			// Figure out the default editor for the file type based on extension.
			var desc = PlatformUI.getWorkbench().getEditorRegistry().getDefaultEditor(file.getName());
			if (desc === null) {
				MessageDialog.openError(HandlerUtil.getActiveShell(event), "Editor open error",
					"Unable to find a suitable editor to open file.");
			} else {
				// Try opening the page in the editor.
				try {
					page.openEditor(new FileEditorInput(file), desc.getId());
				} catch (PartInitException e) {
					e.printStackTrace
				}
			}
		}
	}
	