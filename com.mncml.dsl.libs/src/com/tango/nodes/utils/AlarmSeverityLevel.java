package com.tango.nodes.utils;

/**
 * Created by admin on 03-10-2015.
 */
public enum AlarmSeverityLevel {
    INDETERMINATE,
    CRITICAL,
    MAJOR,
    MINOR,
    WARNING,
    INFO,
    INITIALISED;


    public AlarmSeverityLevel fromInt(int x)
    {
        switch(x){
            case 0:
                return INDETERMINATE;
            case 1:
                return CRITICAL;
            case 2:
                return MAJOR;
            case 3:
                return MINOR;
            case 4:
                return WARNING;
            case 5:
                return INFO;
            case 6:
                return INITIALISED;
            default:
                return null;
        }
    }

    }