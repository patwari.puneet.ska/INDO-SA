package com.tango.nodes.utils;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 * Created by user on 9/8/15.
 */

public class Capability
{
    private String capabilityName;
    private int maxInstance;
    private int unUtilizedInstance;
    private boolean isAvailable;
    private String healthStatus;
    private Map<Integer, Integer> subArrayInstanceMap;

    public Capability(String capabilityName, int maxInstance, boolean isAvailable, String healthStatus) {
        this.capabilityName = capabilityName;
        this.maxInstance = maxInstance;
        this.isAvailable = isAvailable;
        this.healthStatus = healthStatus;
        subArrayInstanceMap = new HashMap<Integer, Integer>();
        setUnUtilizedInstance();
    }

    public String getCapabilityName() {
        return capabilityName;
    }

    public void setCapabilityName(String capabilityName) {
        this.capabilityName = capabilityName;
    }

    public int getMaxInstance() {
        return maxInstance;
    }

    public void setMaxInstance(int maxInstance) {
        this.maxInstance = maxInstance;
    }

    public int getUnUtilizedInstance() {
        return unUtilizedInstance;
    }

    public void setUnUtilizedInstance() {
        // write logic
        Set subArrayIdSet = subArrayInstanceMap.keySet();
        Iterator<Integer> itr = subArrayIdSet.iterator();
        int usedInstance = 0;
        while(itr.hasNext())
        {
            int currentSbArrId = itr.next();
            usedInstance = usedInstance + subArrayInstanceMap.get(currentSbArrId);
        }
        this.unUtilizedInstance = this.maxInstance - usedInstance;
    }

    public boolean isAvailable() {
        return isAvailable;
    }

    public void setIsAvailable(boolean isAvailable) {
        this.isAvailable = isAvailable;
    }

    public String getHealthStatus() {
        return healthStatus;
    }

    public void setHealthStatus(String healthStatus) {
        this.healthStatus = healthStatus;
    }

    public boolean addUsageInfo(int subArrId, int numInstance)
    {
        boolean retVal = true;
        if(numInstance > unUtilizedInstance)
            retVal = false;
        else
        {
            subArrayInstanceMap.put(subArrId, numInstance);
            setUnUtilizedInstance();
        }
        return retVal;
    }

    public boolean removeUsageInfo(int subArrId, int numInstance)
    {
        if(subArrayInstanceMap.containsKey(subArrId)) {
            int currentVal = subArrayInstanceMap.get(subArrId);
            if (numInstance > currentVal)
                return false;
            else {
                subArrayInstanceMap.put(subArrId, currentVal - numInstance);
                setUnUtilizedInstance();
            }
            return true;
        } else {
            return false;
        }
    }

    public boolean removeUsageInfo(int subArrId)
    {
        if(subArrayInstanceMap.containsKey(subArrId)) {
            subArrayInstanceMap.remove(subArrId);
            setUnUtilizedInstance();
            return true;
        }
        else return false;
    }

    public void removeUsageInfo()
    {
        subArrayInstanceMap.clear();
        setUnUtilizedInstance();
    }

    @Override
    public String toString()
    {
        return "CapabilityName='" + getCapabilityName() + '\'' +
                ",MaxInstance=" + getMaxInstance() +
                ",UnUtilizedInstance=" + getUnUtilizedInstance() +
                ",IsAvailable=" + isAvailable() +
                ",HealthStatus='" + getHealthStatus() + '\'' +
                ",SubArrayInstanceMap=" + subArrayInstanceMap;
    }
}
