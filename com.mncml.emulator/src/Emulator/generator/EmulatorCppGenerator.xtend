package Emulator.generator

import Emulator.util.GeneratorUtils
import com.google.inject.Inject
import mncModel.AlarmBlock
import mncModel.CommandResponseBlock
import mncModel.ControlNode
import mncModel.DataPointBlock
import mncModel.EventBlock
import mncModel.InterfaceDescription
import mncModel.Model
import mncModel.SimpleType
import mncModel.impl.ControlNodeImpl
import mncModel.impl.InterfaceDescriptionImpl
import org.eclipse.emf.common.util.EList
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.xtext.generator.IFileSystemAccess
import org.eclipse.xtext.generator.IGenerator

class EmulatorCppGenerator implements IGenerator {
@Inject 
EList<DataPointBlock> dataPointBlockList	
@Inject  
EList<AlarmBlock> alarmBlockList
@Inject
EList<EventBlock> eventBlockList
@Inject
EList<CommandResponseBlock> commandResponseBlockList

var controlNode = null as ControlNodeImpl
	override doGenerate(Resource resource, IFileSystemAccess fsa) {
		val instance = resource.contents.head as  Model
		val interface = instance.eContents.filter(InterfaceDescriptionImpl).get(0)
		controlNode = instance.eContents.filter(ControlNodeImpl).get(0)
		dataPointBlockList = new GeneratorUtils().getDataPointBlocks(controlNode)
		eventBlockList = new GeneratorUtils().getEventBlocks(controlNode)
		alarmBlockList = new  GeneratorUtils().getAlarmBlocks(controlNode)
		commandResponseBlockList = new  GeneratorUtils().getCommandResponseBlocks(controlNode)
		val className = controlNode.name

		if (instance != null) { 
			
			fsa.generateFile("ClassFactory.cpp",
				toClassFactory(controlNode.name))
				
			fsa.generateFile(className+".h",
			toCppServerHeaderCode(interface,controlNode))
			
			fsa.generateFile(className+".cpp",
			toCppServerSourceCode(interface,controlNode))
			
			fsa.generateFile(className+"Class.cpp",
				toCppClassSource(interface,controlNode)
			)
			
	 		fsa.generateFile(className+"Class.h",
				toCppClassHeader(interface,controlNode)
			)
			
			fsa.generateFile(className+"DynAttrUtils.cpp",
				toCppDynamicAttribUtils(interface,controlNode)
			)
			
			fsa.generateFile("main.cpp",
				toCppMainMethod(controlNode.name)
			)
		/*else if (interface.operatingStates == null && controlNode.scmStateMachine != null) {
				fsa.generateFile("SCM" + ".java", toJavaCodeSCMStateMachine(controlNode))
			}*/
		}
	} 
	
	def toCppClassHeader(InterfaceDescriptionImpl ides, ControlNodeImpl cn) {
		'''
		#ifndef �cn.name�Class_H
#define �cn.name�Class_H

#include <tango.h>
#include <�cn.name�.h>


/*----- PROTECTED REGION END -----*/	//	�cn.name�Class.h


namespace �cn.name�_ns
{
/*----- PROTECTED REGION ID(�cn.name�Class::classes for dynamic creation) ENABLED START -----*/


/*----- PROTECTED REGION END -----*/	//	�cn.name�Class::classes for dynamic creation

//=========================================
//	Define classes for attributes
//=========================================
�IF cn.dataPointBlocks!=null�
�IF cn.dataPointBlocks.dataPointBlocks!=null�
�FOR dataPointBlock : cn.dataPointBlocks.dataPointBlocks�
�IF !dataPointBlock.dataPoint.name.contains("dynamic")�
//	Attribute �dataPointBlock.dataPoint.name� class definition
class �dataPointBlock.dataPoint.name�Attrib: public Tango::Attr
{
public:
	�dataPointBlock.dataPoint.name�Attrib():Attr("�dataPointBlock.dataPoint.name�",
			Tango::DEV_DOUBLE, Tango::READ) {};
	~�dataPointBlock.dataPoint.name�Attrib() {};
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
		{(static_cast<�cn.name� *>(dev))->read_�dataPointBlock.dataPoint.name�(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
		{return (static_cast<�cn.name� *>(dev))->is_�dataPointBlock.dataPoint.name�_allowed(ty);}
};
�ENDIF�
�IF dataPointBlock.dataPoint.name.contains("dynamic")�
//=========================================
//	Define classes for dynamic attributes
//=========================================
//	Attribute �dataPointBlock.dataPoint.name� class definition
class �dataPointBlock.dataPoint.name�Attrib: public Tango::Attr
{
public:
	�dataPointBlock.dataPoint.name�Attrib(const string &att_name):Attr(att_name.c_str(), 
			Tango::DEV_DOUBLE, Tango::READ) {};
	~�dataPointBlock.dataPoint.name�Attrib() {};
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
		{(static_cast<�cn.name� *>(dev))->read_�dataPointBlock.dataPoint.name�(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
		{return (static_cast<�cn.name� *>(dev))->is_�dataPointBlock.dataPoint.name�_allowed(ty);}
};
�ENDIF�
�ENDFOR�
�ENDIF�
�ENDIF�

//=========================================
//	Define classes for commands
//=========================================
�IF cn.commandResponseBlocks!=null�
�IF cn.commandResponseBlocks.commandResponseBlocks!=null�
�FOR commandResponseBlock : cn.commandResponseBlocks.commandResponseBlocks�
//	Command �commandResponseBlock.command.name� class definition
class �commandResponseBlock.command.name�Class : public Tango::Command
{
public:
	�commandResponseBlock.command.name�Class(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out,
				   const char        *in_desc,
				   const char        *out_desc,
				   Tango::DispLevel  level)
	:Command(name,in,out,in_desc,out_desc, level)	{};

	�commandResponseBlock.command.name�Class(const char   *name,
	               Tango::CmdArgType in,
				   Tango::CmdArgType out)
	:Command(name,in,out)	{};
	~�commandResponseBlock.command.name�Class() {};
	
	virtual CORBA::Any *execute (Tango::DeviceImpl *dev, const CORBA::Any &any);
	virtual bool is_allowed (Tango::DeviceImpl *dev, const CORBA::Any &any)
	{return (static_cast<�cn.name� *>(dev))->is_�commandResponseBlock.command.name�_allowed(any);}
};

�ENDFOR�
�ENDIF�
�ENDIF�

/**
 *	The �cn.name�Class singleton definition
 */

#ifdef _TG_WINDOWS_
class __declspec(dllexport)  �cn.name�Class : public Tango::DeviceClass
#else
class �cn.name�Class : public Tango::DeviceClass
#endif
{
	/*----- PROTECTED REGION ID(�cn.name�Class::Additionnal DServer data members) ENABLED START -----*/
	
	
	/*----- PROTECTED REGION END -----*/	//	�cn.name�Class::Additionnal DServer data members

	public:
		//	write class properties data members
		Tango::DbData	cl_prop;
		Tango::DbData	cl_def_prop;
		Tango::DbData	dev_def_prop;
	
		//	Method prototypes
		static �cn.name�Class *init(const char *);
		static �cn.name�Class *instance();
		~�cn.name�Class();
		Tango::DbDatum	get_class_property(string &);
		Tango::DbDatum	get_default_device_property(string &);
		Tango::DbDatum	get_default_class_property(string &);
	
	protected:
		�cn.name�Class(string &);
		static �cn.name�Class *_instance;
		void command_factory();
		void attribute_factory(vector<Tango::Attr *> &);
		void write_class_property();
		void set_default_property();
		void get_class_property();
		string get_cvstag();
		string get_cvsroot();
	
	private:
		void device_factory(const Tango::DevVarStringArray *);
		void create_static_attribute_list(vector<Tango::Attr *> &);
		void erase_dynamic_attributes(const Tango::DevVarStringArray *,vector<Tango::Attr *> &);
		vector<string>	defaultAttList;
		Tango::Attr *get_attr_object_by_name(vector<Tango::Attr *> &att_list, string attname);
};

}	//	End of namespace

#endif   //	�cn.name�_H
		
		'''
	}
	
	def toCppClassSource(InterfaceDescriptionImpl ides, ControlNodeImpl cn) {
		'''
		#include <�cn.name.toFirstUpper�Class.h>

/*----- PROTECTED REGION END -----*/	//	�cn.name.toFirstUpper�Class.cpp

//-------------------------------------------------------------------
/**
 *	Create �cn.name.toFirstUpper�Class singleton and
 *	return it in a C function for Python usage
 */
//-------------------------------------------------------------------
extern "C" {
#ifdef _TG_WINDOWS_

__declspec(dllexport)

#endif

	Tango::DeviceClass *_create_�cn.name.toFirstUpper�_class(const char *name) {
		return �cn.name.toFirstUpper�_ns::�cn.name.toFirstUpper�Class::init(name);
	}
}

namespace �cn.name.toFirstUpper�_ns
{
//===================================================================
//	Initialize pointer for singleton pattern
//===================================================================
�cn.name.toFirstUpper�Class *�cn.name.toFirstUpper�Class::_instance = NULL;

//--------------------------------------------------------
/**
 * method : 		�cn.name.toFirstUpper�Class::�cn.name.toFirstUpper�Class(string &s)
 * description : 	constructor for the �cn.name.toFirstUpper�Class
 *
 * @param s	The class name
 */
//--------------------------------------------------------
�cn.name.toFirstUpper�Class::�cn.name.toFirstUpper�Class(string &s):Tango::DeviceClass(s)
{
	cout2 << "Entering �cn.name.toFirstUpper�Class constructor" << endl;
	set_default_property();
	write_class_property();

	/*----- PROTECTED REGION ID(�cn.name.toFirstUpper�Class::constructor) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	�cn.name.toFirstUpper�Class::constructor

	cout2 << "Leaving �cn.name.toFirstUpper�Class constructor" << endl;
}

//--------------------------------------------------------
/**
 * method : 		�cn.name.toFirstUpper�Class::~�cn.name.toFirstUpper�Class()
 * description : 	destructor for the �cn.name.toFirstUpper�Class
 */
//--------------------------------------------------------
�cn.name.toFirstUpper�Class::~�cn.name.toFirstUpper�Class()
{
	/*----- PROTECTED REGION ID(�cn.name.toFirstUpper�Class::destructor) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	�cn.name.toFirstUpper�Class::destructor

	_instance = NULL;
}


//--------------------------------------------------------
/**
 * method : 		�cn.name.toFirstUpper�Class::init
 * description : 	Create the object if not already done.
 *                  Otherwise, just return a pointer to the object
 *
 * @param	name	The class name
 */
//--------------------------------------------------------
�cn.name.toFirstUpper�Class *�cn.name.toFirstUpper�Class::init(const char *name)
{
	if (_instance == NULL)
	{
		try
		{
			string s(name);
			_instance = new �cn.name.toFirstUpper�Class(s);
		}
		catch (bad_alloc &)
		{
			throw;
		}		
	}		
	return _instance;
}

//--------------------------------------------------------
/**
 * method : 		�cn.name.toFirstUpper�Class::instance
 * description : 	Check if object already created,
 *                  and return a pointer to the object
 */
//--------------------------------------------------------
�cn.name.toFirstUpper�Class *�cn.name.toFirstUpper�Class::instance()
{
	if (_instance == NULL)
	{
		cerr << "Class is not initialised !!" << endl;
		exit(-1);
	}
	return _instance;
}



//===================================================================
//	Command execution method calls
//===================================================================
//--------------------------------------------------------
�IF cn.commandResponseBlocks!=null�
�IF cn.commandResponseBlocks.commandResponseBlocks!=null�
�FOR commandResponseBlock : cn.commandResponseBlocks.commandResponseBlocks�
/**
 * method : 		�cn.name.toFirstUpper��commandResponseBlock.command.name�Class::execute()
 * description : 	method to trigger the execution of the command.
 *
 * @param	device	The device on which the command must be executed
 * @param	in_any	The command input data
 *
 *	returns The command output data (packed in the Any object)
 */
//--------------------------------------------------------
CORBA::Any *�cn.name.toFirstUpper��commandResponseBlock.command.name�Class::execute(Tango::DeviceImpl *device, TANGO_UNUSED(const CORBA::Any &in_any))
{
	cout2 << "�cn.name.toFirstUpper��commandResponseBlock.command.name�Class::execute(): arrived" << endl;
	((static_cast<�cn.name.toFirstUpper� *>(device))->�cn.name.toFirstUpper�_�commandResponseBlock.command.name�());
	return new CORBA::Any();
}
�ENDFOR�
�ENDIF�
�ENDIF�


//===================================================================
//	Properties management
//===================================================================
//--------------------------------------------------------
/**
 *	Method      : �cn.name.toFirstUpper�Class::get_class_property()
 *	Description : Get the class property for specified name.
 */
//--------------------------------------------------------
Tango::DbDatum �cn.name.toFirstUpper�Class::get_class_property(string &prop_name)
{
	for (unsigned int i=0 ; i<cl_prop.size() ; i++)
		if (cl_prop[i].name == prop_name)
			return cl_prop[i];
	//	if not found, returns  an empty DbDatum
	return Tango::DbDatum(prop_name);
}

//--------------------------------------------------------
/**
 *	Method      : �cn.name.toFirstUpper�Class::get_default_device_property()
 *	Description : Return the default value for device property.
 */
//--------------------------------------------------------
Tango::DbDatum �cn.name.toFirstUpper�Class::get_default_device_property(string &prop_name)
{
	for (unsigned int i=0 ; i<dev_def_prop.size() ; i++)
		if (dev_def_prop[i].name == prop_name)
			return dev_def_prop[i];
	//	if not found, return  an empty DbDatum
	return Tango::DbDatum(prop_name);
}

//--------------------------------------------------------
/**
 *	Method      : �cn.name.toFirstUpper�Class::get_default_class_property()
 *	Description : Return the default value for class property.
 */
//--------------------------------------------------------
Tango::DbDatum �cn.name.toFirstUpper�Class::get_default_class_property(string &prop_name)
{
	for (unsigned int i=0 ; i<cl_def_prop.size() ; i++)
		if (cl_def_prop[i].name == prop_name)
			return cl_def_prop[i];
	//	if not found, return  an empty DbDatum
	return Tango::DbDatum(prop_name);
}


//--------------------------------------------------------
/**
 *	Method      : �cn.name.toFirstUpper�Class::set_default_property()
 *	Description : Set default property (class and device) for wizard.
 *                For each property, add to wizard property name and description.
 *                If default value has been set, add it to wizard property and
 *                store it in a DbDatum.
 */
//--------------------------------------------------------
void �cn.name.toFirstUpper�Class::set_default_property()
{
	string	prop_name;
	string	prop_desc;
	string	prop_def;
	vector<string>	vect_data;

	//	Set Default Class Properties

	//	Set Default device Properties
}

//--------------------------------------------------------
/**
 *	Method      : �cn.name.toFirstUpper�Class::write_class_property()
 *	Description : Set class description fields as property in database
 */
//--------------------------------------------------------
void �cn.name.toFirstUpper�Class::write_class_property()
{
	//	First time, check if database used
	if (Tango::Util::_UseDb == false)
		return;

	Tango::DbData	data;
	string	classname = get_name();
	string	header;
	string::size_type	start, end;

	//	Put title
	Tango::DbDatum	title("ProjectTitle");
	string	str_title("");
	title << str_title;
	data.push_back(title);

	//	Put Description
	Tango::DbDatum	description("Description");
	vector<string>	str_desc;
	str_desc.push_back("");
	description << str_desc;
	data.push_back(description);

	//	put cvs or svn location
	string	filename("�cn.name.toFirstUpper�");
	filename += "Class.cpp";

	// check for cvs information
	string	src_path(CvsPath);
	start = src_path.find("/");
	if (start!=string::npos)
	{
		end   = src_path.find(filename);
		if (end>start)
		{
			string	strloc = src_path.substr(start, end-start);
			//	Check if specific repository
			start = strloc.find("/cvsroot/");
			if (start!=string::npos && start>0)
			{
				string	repository = strloc.substr(0, start);
				if (repository.find("/segfs/")!=string::npos)
					strloc = "ESRF:" + strloc.substr(start, strloc.length()-start);
			}
			Tango::DbDatum	cvs_loc("cvs_location");
			cvs_loc << strloc;
			data.push_back(cvs_loc);
		}
	}

	// check for svn information
	else
	{
		string	src_path(SvnPath);
		start = src_path.find("://");
		if (start!=string::npos)
		{
			end = src_path.find(filename);
			if (end>start)
			{
				header = "$HeadURL: ";
				start = header.length();
				string	strloc = src_path.substr(start, (end-start));
				
				Tango::DbDatum	svn_loc("svn_location");
				svn_loc << strloc;
				data.push_back(svn_loc);
			}
		}
	}

	//	Get CVS or SVN revision tag
	
	// CVS tag
	string	tagname(TagName);
	header = "$Name: ";
	start = header.length();
	string	endstr(" $");
	
	end   = tagname.find(endstr);
	if (end!=string::npos && end>start)
	{
		string	strtag = tagname.substr(start, end-start);
		Tango::DbDatum	cvs_tag("cvs_tag");
		cvs_tag << strtag;
		data.push_back(cvs_tag);
	}
	
	// SVN tag
	string	svnpath(SvnPath);
	header = "$HeadURL: ";
	start = header.length();
	
	end   = svnpath.find(endstr);
	if (end!=string::npos && end>start)
	{
		string	strloc = svnpath.substr(start, end-start);
		
		string tagstr ("/tags/");
		start = strloc.find(tagstr);
		if ( start!=string::npos )
		{
			start = start + tagstr.length();
			end   = strloc.find(filename);
			string	strtag = strloc.substr(start, end-start-1);
			
			Tango::DbDatum	svn_tag("svn_tag");
			svn_tag << strtag;
			data.push_back(svn_tag);
		}
	}

	//	Get URL location
	string	httpServ(HttpServer);
	if (httpServ.length()>0)
	{
		Tango::DbDatum	db_doc_url("doc_url");
		db_doc_url << httpServ;
		data.push_back(db_doc_url);
	}

	//  Put inheritance
	Tango::DbDatum	inher_datum("InheritedFrom");
	vector<string> inheritance;
	inheritance.push_back("TANGO_BASE_CLASS");
	inher_datum << inheritance;
	data.push_back(inher_datum);

	//	Call database and and values
	get_db_class()->put_property(data);
}

//===================================================================
//	Factory methods
//===================================================================

//--------------------------------------------------------
/**
 *	Method      : �cn.name.toFirstUpper�Class::device_factory()
 *	Description : Create the device object(s)
 *                and store them in the device list
 */
//--------------------------------------------------------
void �cn.name.toFirstUpper�Class::device_factory(const Tango::DevVarStringArray *devlist_ptr)
{
	/*----- PROTECTED REGION ID(�cn.name.toFirstUpper�Class::device_factory_before) ENABLED START -----*/
	
	//	Add your own code
	
	/*----- PROTECTED REGION END -----*/	//	�cn.name.toFirstUpper�Class::device_factory_before

	//	Create devices and add it into the device list
	for (unsigned long i=0 ; i<devlist_ptr->length() ; i++)
	{
		cout4 << "Device name : " << (*devlist_ptr)[i].in() << endl;
		device_list.push_back(new �cn.name.toFirstUpper�(this, (*devlist_ptr)[i]));							 
	}

	//	Manage dynamic attributes if any
	erase_dynamic_attributes(devlist_ptr, get_class_attr()->get_attr_list());

	//	Export devices to the outside world
	for (unsigned long i=1 ; i<=devlist_ptr->length() ; i++)
	{
		//	Add dynamic attributes if any
		�cn.name.toFirstUpper� *dev = static_cast<�cn.name.toFirstUpper� *>(device_list[device_list.size()-i]);
		dev->add_dynamic_attributes();

		//	Check before if database used.
		if ((Tango::Util::_UseDb == true) && (Tango::Util::_FileDb == false))
			export_device(dev);
		else
			export_device(dev, dev->get_name().c_str());
	}

	/*----- PROTECTED REGION ID(�cn.name.toFirstUpper�Class::device_factory_after) ENABLED START -----*/
	
	//	Add your own code
	
	/*----- PROTECTED REGION END -----*/	//	�cn.name.toFirstUpper�Class::device_factory_after
}
//--------------------------------------------------------
/**
 *	Method      : �cn.name.toFirstUpper�Class::attribute_factory()
 *	Description : Create the attribute object(s)
 *                and store them in the attribute list
 */
//--------------------------------------------------------
void �cn.name.toFirstUpper�Class::attribute_factory(vector<Tango::Attr *> &att_list)
{
	/*----- PROTECTED REGION ID(�cn.name.toFirstUpper�Class::attribute_factory_before) ENABLED START -----*/
	
	//	Add your own code


�IF cn.dataPointBlocks!=null�	
�IF cn.dataPointBlocks.dataPointBlocks!=null�
�FOR dataPointBlock :cn.dataPointBlocks.dataPointBlocks�
	/*----- PROTECTED REGION END -----*/	//	�cn.name.toFirstUpper�Class::attribute_factory_before
	//	Attribute : �cn.name.toFirstUpper�Attr1
	�cn.name.toFirstUpper�Attr1Attrib	*�cn.name.toFirstUpper�attr1 = new �cn.name.toFirstUpper�Attr1Attrib();
	Tango::UserDefaultAttrProp	�cn.name.toFirstUpper��dataPointBlock.dataPoint.name�_prop;
	�IF dataPointBlock.dataPoint.parameters!=null�
	�FOR param : dataPointBlock.dataPoint.parameters�
	�var simpleType = param as SimpleType�
	
	�simpleType.name�_prop.set_description("�ParameterValueFinder.findValue(simpleType.value)�");
	�ENDFOR�
	�ENDIF�
	�cn.name.toFirstUpper��dataPointBlock.dataPoint.name�->set_default_properties(�cn.name.toFirstUpper�attr1_prop);
	//	Not Polled
	�cn.name.toFirstUpper��dataPointBlock.dataPoint.name�->set_disp_level(Tango::OPERATOR);
	//	Not Memorized
	att_list.push_back(�cn.name.toFirstUpper�attr1);
�ENDFOR�
�ENDIF�
�ENDIF�




	//	Create a list of static attributes
	create_static_attribute_list(get_class_attr()->get_attr_list());
	/*----- PROTECTED REGION ID(�cn.name.toFirstUpper�Class::attribute_factory_after) ENABLED START -----*/
	
	//	Add your own code
	
	/*----- PROTECTED REGION END -----*/	//	�cn.name.toFirstUpper�Class::attribute_factory_after
}
//--------------------------------------------------------
/**
 *	Method      : �cn.name.toFirstUpper�Class::command_factory()
 *	Description : Create the command object(s)
 *                and store them in the command list
 */
//--------------------------------------------------------
void �cn.name.toFirstUpper�Class::command_factory()
{
	/*----- PROTECTED REGION ID(�cn.name.toFirstUpper�Class::command_factory_before) ENABLED START -----*/
	
	//	Add your own code
	
	/*----- PROTECTED REGION END -----*/	//	�cn.name.toFirstUpper�Class::command_factory_before

�IF cn.commandResponseBlocks!=null�
�IF cn.commandResponseBlocks.commandResponseBlocks!=null�
�FOR commandResponseBlock  : cn.commandResponseBlocks.commandResponseBlocks�
	//	Command �cn.name.toFirstUpper��commandResponseBlock.command.name�
	�cn.name.toFirstUpper��commandResponseBlock.command.name�Class	*p�cn.name.toFirstUpper��commandResponseBlock.command.name�Cmd =
		new �cn.name.toFirstUpper��commandResponseBlock.command.name�Class("�cn.name.toFirstUpper��commandResponseBlock.command.name�",
			Tango::DEV_VOID, Tango::DEV_VOID,
			"",
			"",
			Tango::OPERATOR);
	command_list.push_back(p�cn.name.toFirstUpper��commandResponseBlock.command.name�Cmd);
	�ENDFOR�
�ENDIF�
�ENDIF�


	/*----- PROTECTED REGION ID(�cn.name.toFirstUpper�Class::command_factory_after) ENABLED START -----*/
	
	//	Add your own code
	
	/*----- PROTECTED REGION END -----*/	//	�cn.name.toFirstUpper�Class::command_factory_after
}

//===================================================================
//	Dynamic attributes related methods
//===================================================================

//--------------------------------------------------------
/**
 * method : 		�cn.name.toFirstUpper�Class::create_static_attribute_list
 * description : 	Create the a list of static attributes
 *
 * @param	att_list	the ceated attribute list 
 */
//--------------------------------------------------------
void �cn.name.toFirstUpper�Class::create_static_attribute_list(vector<Tango::Attr *> &att_list)
{
	for (unsigned long i=0 ; i<att_list.size() ; i++)
	{
		string att_name(att_list[i]->get_name());
		transform(att_name.begin(), att_name.end(), att_name.begin(), ::tolower);
		defaultAttList.push_back(att_name);
	}

	cout2 << defaultAttList.size() << " attributes in default list" << endl;

	/*----- PROTECTED REGION ID(�cn.name.toFirstUpper�Class::create_static_att_list) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	�cn.name.toFirstUpper�Class::create_static_att_list
}


//--------------------------------------------------------
/**
 * method : 		�cn.name.toFirstUpper�Class::erase_dynamic_attributes
 * description : 	delete the dynamic attributes if any.
 *
 * @param	devlist_ptr	the device list pointer
 * @param	list of all attributes
 */
//--------------------------------------------------------
void �cn.name.toFirstUpper�Class::erase_dynamic_attributes(const Tango::DevVarStringArray *devlist_ptr, vector<Tango::Attr *> &att_list)
{
	Tango::Util *tg = Tango::Util::instance();

	for (unsigned long i=0 ; i<devlist_ptr->length() ; i++)
	{	
		Tango::DeviceImpl *dev_impl = tg->get_device_by_name(((string)(*devlist_ptr)[i]).c_str());
		�cn.name.toFirstUpper� *dev = static_cast<�cn.name.toFirstUpper� *> (dev_impl);
		
		vector<Tango::Attribute *> &dev_att_list = dev->get_device_attr()->get_attribute_list();
		vector<Tango::Attribute *>::iterator ite_att;
		for (ite_att=dev_att_list.begin() ; ite_att != dev_att_list.end() ; ++ite_att)
		{
			string att_name((*ite_att)->get_name_lower());
			if ((att_name == "state") || (att_name == "status"))
				continue;
			vector<string>::iterator ite_str = find(defaultAttList.begin(), defaultAttList.end(), att_name);
			if (ite_str == defaultAttList.end())
			{
				cout2 << att_name << " is a UNWANTED dynamic attribute for device " << (*devlist_ptr)[i] << endl;
				Tango::Attribute &att = dev->get_device_attr()->get_attr_by_name(att_name.c_str());
				dev->remove_attribute(att_list[att.get_attr_idx()], true, false);
				--ite_att;
			}
		}
	}
	/*----- PROTECTED REGION ID(�cn.name.toFirstUpper�Class::erase_dynamic_attributes) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	�cn.name.toFirstUpper�Class::erase_dynamic_attributes
}

//--------------------------------------------------------
/**
 *	Method      : �cn.name.toFirstUpper�Class::get_attr_by_name()
 *	Description : returns Tango::Attr * object found by name
 */
//--------------------------------------------------------
Tango::Attr *�cn.name.toFirstUpper�Class::get_attr_object_by_name(vector<Tango::Attr *> &att_list, string attname)
{
	vector<Tango::Attr *>::iterator it;
	for (it=att_list.begin() ; it<att_list.end() ; it++)
		if ((*it)->get_name()==attname)
			return (*it);
	//	Attr does not exist
	return NULL;
}


/*----- PROTECTED REGION ID(�cn.name.toFirstUpper�Class::Additional Methods) ENABLED START -----*/

/*----- PROTECTED REGION END -----*/	//	�cn.name.toFirstUpper�Class::Additional Methods
} //	namespace
		'''
	}
	
	def toCppMainMethod(String controlNodeName) {
	'''
	/*----- PROTECTED REGION ID(�controlNodeName�::main.cpp) ENABLED START -----*/
static const char *RcsId = "$Id:  $";

#include <tango.h>

int main(int argc,char *argv[])
{
	try
	{
		// Initialise the device server
		//----------------------------------------
		Tango::Util *tg = Tango::Util::init(argc,argv);

		// Create the device server singleton 
		//	which will create everything
		//----------------------------------------
		tg->server_init(false);

		// Run the endless loop
		//----------------------------------------
		cout << "Ready to accept request" << endl;
		tg->server_run();
	}
	catch (bad_alloc &)
	{
		cout << "Can't allocate memory to store device object !!!" << endl;
		cout << "Exiting" << endl;
	}
	catch (CORBA::Exception &e)
	{
		Tango::Except::print_exception(e);
		
		cout << "Received a CORBA_Exception" << endl;
		cout << "Exiting" << endl;
	}
	Tango::Util::instance()->server_cleanup();
	return(0);
}

/*----- PROTECTED REGION END -----*/	//	�controlNodeName�::main.cpp
	
	'''
	}
	
	def toCppDynamicAttribUtils(InterfaceDescription ides, ControlNode cn) {
'''
static const char *RcsId = "$Id:  $";
#include <�cn.name�.h>
#include <�cn.name�.h>

namespace �cn.name�_ns
{
//=============================================================
//	Add/Remove dynamic attribute methods
//=============================================================

//--------------------------------------------------------
�IF ides.dataPoints!= null�
 �FOR dataPoint: ides.dataPoints.dataPoints� 
 �IF(dataPoint.name.contains("dynamic"))�
 �var dynamicAttributeName = dataPoint.name.split("_").get(1)�
/**
 *	Add a �dynamicAttributeName� dynamic attribute.
 *
 *  parameter attname: attribute name to be cretated and added.
 */

//--------------------------------------------------------
void �cn.name�::add_�dynamicAttributeName�_dynamic_attribute(string attname)
{
	//	Attribute : �dynamicAttributeName�
	�dynamicAttributeName�Attrib	*�dynamicAttributeName� = new �dynamicAttributeName�Attrib(attname);
	Tango::UserDefaultAttrProp	dynamicattribute_prop;
	
	/*----- PROTECTED REGION ID(DemoCPPServer::att_�dynamicAttributeName�_dynamic_attribute) ENABLED START -----*/
	
	
	/*----- PROTECTED REGION END -----*/	//	DemoCPPServer::att_dynamicAttribute_dynamic_attribute
	�dynamicAttributeName�->set_default_properties(�dynamicAttributeName�_prop);
	//	Not Polled
	�dynamicAttributeName�->set_disp_level(Tango::OPERATOR);
	//	Not Memorized
	�dynamicAttributeName�_data.insert(make_pair(attname, 0.0));
	add_attribute(dynamicattribute);
}
//--------------------------------------------------------
/**
 *	remove a �dynamicAttributeName� dynamic attribute.
 *
 *  parameter attname: attribute name to be removed and added.
 */
//--------------------------------------------------------
void DemoCPPServer::remove_�dynamicAttributeName�_dynamic_attribute(string attname)
{
	remove_attribute(attname, true);
	map<string,Tango::DevDouble>::iterator ite;
    if ((ite=�dynamicAttributeName�_data.find(attname))!=�dynamicAttributeName�_data.end())
    {
    	/*----- PROTECTED REGION ID(DemoCPPServer::remove_�dynamicAttributeName�_dynamic_attribute) ENABLED START -----*/
    	
    	/*----- PROTECTED REGION END -----*/	//	DemoCPPServer::remove_�dynamicAttributeName�_dynamic_attribute
		�dynamicAttributeName�_data.erase(ite);
	}
}


//============================================================
//	Tool methods to get pointer on attribute data buffer 
//============================================================
//--------------------------------------------------------
/**
 *	Return a pointer on �dynamicAttributeName� data.
 *
 *  parameter attname: the specified attribute name.
 */
//--------------------------------------------------------
Tango::DevDouble *DemoCPPServer::get_�dynamicAttributeName�_data_ptr(string &name)
{
	map<string,Tango::DevDouble>::iterator ite;
    if ((ite=�dynamicAttributeName�_data.find(name))==�dynamicAttributeName�_data.end())
    {
		TangoSys_OMemStream	tms;
		tms << "Dynamic attribute " << name << " has not been created";
		Tango::Except::throw_exception(
					(const char *)"ATTRIBUTE_NOT_FOUND",
					tms.str().c_str(),
					(const char *)"DemoCPPServer::get_�dynamicAttributeName�_data_ptr()");
    }
	return  &(ite->second);
}
�ENDIF�
�ENDFOR�
�ENDIF� ���Closing IF check for NULL DataPoint List��
//--------------------------------------------------------

} //	namespace

'''
	}

	def toClassFactory(String className) {
		'''
		static const char *RcsId = "$Id:  $";
		#include <tango.h>
		#include <�className�Class.h>
		void Tango::DServer::class_factory()
		{
		//	Add method class init if needed
		add_class(className_ns::�className�Class::init("className"));
		}
		'''
	}	
	
// Cpp Server Code	
	def toCppServerSourceCode(InterfaceDescriptionImpl ides, ControlNodeImpl cn) {
'''
static const char *RcsId = "$Id:  $";
#include <�cn.name�.h>
#include <�cn.name�Class.h>


//================================================================

namespace �cn.name�_ns
{
/*----- PROTECTED REGION ID(�cn.name�::namespace_starting) ENABLED START -----*/

//	static initializations

/*----- PROTECTED REGION END -----*/	//	�cn.name�::namespace_starting

//--------------------------------------------------------
/**
 *	Method      : �cn.name�::�cn.name�()
 *	Description : Constructors for a Tango device
 *                implementing the class�cn.name�
 */
//--------------------------------------------------------
�cn.name�::�cn.name�(Tango::DeviceClass *cl, string &s)
 : TANGO_BASE_CLASS(cl, s.c_str())
{
	/*----- PROTECTED REGION ID(�cn.name�::constructor_1) ENABLED START -----*/
	init_device();
	
	/*----- PROTECTED REGION END -----*/	//	�cn.name�::constructor_1
}
//--------------------------------------------------------
�cn.name�::�cn.name�(Tango::DeviceClass *cl, const char *s)
 : TANGO_BASE_CLASS(cl, s)
{
	/*----- PROTECTED REGION ID(�cn.name�::constructor_2) ENABLED START -----*/
	init_device();
	
	/*----- PROTECTED REGION END -----*/	//	�cn.name�::constructor_2
}
//--------------------------------------------------------
�cn.name�::�cn.name�(Tango::DeviceClass *cl, const char *s, const char *d)
 : TANGO_BASE_CLASS(cl, s, d)
{
	/*----- PROTECTED REGION ID(�cn.name�::constructor_3) ENABLED START -----*/
	init_device();
	
	/*----- PROTECTED REGION END -----*/	//	�cn.name�::constructor_3
}

//--------------------------------------------------------
/**
 *	Method      : �cn.name�::delete_device()
 *	Description : will be called at device destruction or at init command
 */
//--------------------------------------------------------
void �cn.name�::delete_device()
{ 
	DEBUG_STREAM << "�cn.name�::delete_device() " << device_name << endl;
	/*----- PROTECTED REGION ID(�cn.name�::delete_device) ENABLED START -----*/
	
	//	Delete device allocated objects
	
	/*----- PROTECTED REGION END -----*/	//	�cn.name�::delete_device
	�IF cn.dataPointBlocks!=null�
	�FOR dataPointBlock: cn.dataPointBlocks.dataPointBlocks�
	delete[] attr_�dataPointBlock.dataPoint.name�_read;
	�ENDFOR�
	�ENDIF�
}

//--------------------------------------------------------
/**
 *	Method      : �cn.name�::init_device()
 *	Description : will be called at device initialization.
 */
//--------------------------------------------------------
void �cn.name�::init_device()
{
	DEBUG_STREAM << "�cn.name�::init_device() create device " << device_name << endl;
	/*----- PROTECTED REGION ID(�cn.name�::init_device_before) ENABLED START -----*/
	
	//	Initialization before get_device_property() call
	
	/*----- PROTECTED REGION END -----*/	//	�cn.name�::init_device_before
	

	//	Get the device properties from database
	get_device_property();
	if (mandatoryNotDefined)
		return;
	�IF cn.dataPointBlocks!=null�	
	�FOR dataPointBlock: cn.dataPointBlocks.dataPointBlocks�
	attr_�dataPointBlock.dataPoint.name�_read = new Tango::DevDouble[1];
	�ENDFOR�
	�ENDIF�
	/*----- PROTECTED REGION ID(�cn.name�::init_device) ENABLED START -----*/
	
	//	Initialize device
	
	/*----- PROTECTED REGION END -----*/	//	�cn.name�::init_device
}

//--------------------------------------------------------
/**
 *	Method      : �cn.name�::get_device_property()
 *	Description : Read database to initialize property data members.
 */
//--------------------------------------------------------
void �cn.name�::get_device_property()
{
	/*----- PROTECTED REGION ID(�cn.name�::get_device_property_before) ENABLED START -----*/
	
	//	Initialize property data members
	
	/*----- PROTECTED REGION END -----*/	//	�cn.name�::get_device_property_before

	mandatoryNotDefined = false;

	//	Read device properties from database.
	Tango::DbData	dev_prop;
	�IF cn.dataPointBlocks!=null�	
	�FOR dataPointBlock: cn.dataPointBlocks.dataPointBlocks�
	�IF(dataPointBlock.dataPoint.name.contains("deviceProperty"))�
	dev_prop.push_back(Tango::DbDatum("�dataPointBlock.dataPoint.name.split("_").get(1)�"));
	�ENDIF�
	�ENDFOR�
	�ENDIF�

	//	is there at least one property to be read ?
	if (dev_prop.size()>0)
	{
		//	Call database and extract values
		if (Tango::Util::instance()->_UseDb==true)
			get_db_device()->get_property(dev_prop);
	
		//	get instance on �cn.name�Class to get class property
		Tango::DbDatum	def_prop, cl_prop;
		�cn.name�Class	*ds_class =
			(static_cast<�cn.name�Class *>(get_device_class()));
		int	i = -1;

�IF cn.dataPointBlocks!=null�	
�FOR dataPointBlock: cn.dataPointBlocks.dataPointBlocks�
	�IF(dataPointBlock.dataPoint.name.contains("deviceProperty"))�
	�var attributeName = dataPointBlock.dataPoint.name.split("/").get(1)�
		//	Try to initialize �attributeName� from class property
		cl_prop = ds_class->get_class_property(dev_prop[++i].name);
		if (cl_prop.is_empty()==false)	cl_prop  >>  �attributeName�;
		else {
			//	Try to initialize �attributeName� from default device value
			def_prop = ds_class->get_default_device_property(dev_prop[i].name);
			if (def_prop.is_empty()==false)	def_prop  >>  �attributeName�;
		}
		//	And try to extract newDeviceProperty value from database
		if (dev_prop[i].is_empty()==false)	dev_prop[i]  >>  �attributeName�;
		//	Property StartDsPath is mandatory, check if has been defined in database.
		check_mandatory_property(cl_prop, dev_prop[i]);
�ENDIF�
�ENDFOR�
�ENDIF�
	}

	/*----- PROTECTED REGION ID(�cn.name�::get_device_property_after) ENABLED START -----*/
	
	//	Check device property data members init
	
	/*----- PROTECTED REGION END -----*/	//	�cn.name�::get_device_property_after
}
//--------------------------------------------------------
/**
 *	Method      : �cn.name�::check_mandatory_property()
 *	Description : For mandatory properties check if defined in database.
 */
//--------------------------------------------------------
void �cn.name�::check_mandatory_property(Tango::DbDatum &class_prop, Tango::DbDatum &dev_prop)
{
	//	Check if all properties are empty
	if (class_prop.is_empty() && dev_prop.is_empty())
	{
		TangoSys_OMemStream	tms;
		tms << endl <<"Property \'" << dev_prop.name;
		if (Tango::Util::instance()->_UseDb==true)
			tms << "\' is mandatory but not defined in database";
		else
			tms << "\' is mandatory but cannot be defined without database";
		string	status(get_status());
		status += tms.str();
		set_status(status);
		mandatoryNotDefined = true;
		/*----- PROTECTED REGION ID(�cn.name�::check_mandatory_property) ENABLED START -----*/
		cerr << tms.str() << " for " << device_name << endl;
		
		/*----- PROTECTED REGION END -----*/	//	�cn.name�::check_mandatory_property
	}
}


//--------------------------------------------------------
/**
 *	Method      : �cn.name�::always_executed_hook()
 *	Description : method always executed before any command is executed
 */
//--------------------------------------------------------
void �cn.name�::always_executed_hook()
{
	INFO_STREAM << "�cn.name�::always_executed_hook()  " << device_name << endl;
	if (mandatoryNotDefined)
	{
		string	status(get_status());
		Tango::Except::throw_exception(
					(const char *)"PROPERTY_NOT_SET",
					status.c_str(),
					(const char *)"�cn.name�::always_executed_hook()");
	}
	/*----- PROTECTED REGION ID(�cn.name�::always_executed_hook) ENABLED START -----*/
	
	//	code always executed before all requests
	
	/*----- PROTECTED REGION END -----*/	//	�cn.name�::always_executed_hook
}

//--------------------------------------------------------
/**
 *	Method      : �cn.name�::read_attr_hardware()
 *	Description : Hardware acquisition for attributes
 */
//--------------------------------------------------------
void �cn.name�::read_attr_hardware(TANGO_UNUSED(vector<long> &attr_list))
{
	DEBUG_STREAM << "�cn.name�::read_attr_hardware(vector<long> &attr_list) entering... " << endl;
	/*----- PROTECTED REGION ID(�cn.name�::read_attr_hardware) ENABLED START -----*/
	
	//	Add your own code
	
	/*----- PROTECTED REGION END -----*/	//	�cn.name�::read_attr_hardware
}


//--------------------------------------------------------


void �controlNode.name�::read_attr_hardware(TANGO_UNUSED(vector<long> &attr_list))
{
	DEBUG_STREAM << "�controlNode.name�::read_attr_hardware(vector<long> &attr_list) entering... " << endl;
	/*----- PROTECTED REGION ID(�controlNode.name�::read_attr_hardware) ENABLED START -----*/
	
	//	Add your own code
	
	/*----- PROTECTED REGION END -----*/	//	�controlNode.name�::read_attr_hardware
}
//--------------------------------------------------------
/**
 *	Method      : �controlNode.name�::write_attr_hardware()
 *	Description : Hardware writing for attributes
 */
//--------------------------------------------------------
void �controlNode.name�::write_attr_hardware(TANGO_UNUSED(vector<long> &attr_list))
{
	DEBUG_STREAM << "�controlNode.name�::write_attr_hardware(vector<long> &attr_list) entering... " << endl;
	/*----- PROTECTED REGION ID(�controlNode.name�::write_attr_hardware) ENABLED START -----*/
	
	//	Add your own code
	
	/*----- PROTECTED REGION END -----*/	//	�controlNode.name�::write_attr_hardware
}
�IF cn.dataPointBlocks!=null�	
�FOR dataPointBlock: cn.dataPointBlocks.dataPointBlocks�
�IF !dataPointBlock.dataPoint.name.contains("deviceName")&&!dataPointBlock.dataPoint.name.contains("subSystemId")&& !dataPointBlock.dataPoint.name.contains("deviceProperty")�

�IF(!dataPointBlock.dataPoint.name.contains("dynamic"))�
�var attributeName = dataPointBlock.dataPoint.name�
//--------------------------------------------------------
void �cn.name�::read_�attributeName�(Tango::Attribute &attr)
{
	DEBUG_STREAM << "�controlNode.name�::read_�attributeName�(Tango::Attribute &attr) entering... " << endl;
	/*----- PROTECTED REGION ID(�controlNode.name�::read_�attributeName�) ENABLED START -----*/
	//	Set the attribute value
	attr.set_value(attr_�attributeName�_read);
	
	/*----- PROTECTED REGION END -----*/	//	�controlNode.name�::read_�attributeName�
}
//--------------------------------------------------------
void �controlNode.name�::write_�attributeName�(Tango::WAttribute &attr)
{
	DEBUG_STREAM << "�controlNode.name�::write_�attributeName�(Tango::WAttribute &attr) entering... " << endl;
	//	Retrieve write value
	Tango::DevDouble	w_val;
	attr.get_write_value(w_val);
	/*----- PROTECTED REGION ID(�controlNode.name�::write_�attributeName�) ENABLED START -----*/
	
	
	/*----- PROTECTED REGION END -----*/	//	�controlNode.name�::write_�attributeName�
}
�ENDIF�
�IF(dataPointBlock.dataPoint.name.contains("dynamic"))�
�var dynamicAttributeName =   dataPointBlock.dataPoint.name.split("_").get(1)�
//--------------------------------------------------------
void �controlNode.name�::read_�dynamicAttributeName�(Tango::Attribute &attr)
{
	DEBUG_STREAM << "�controlNode.name�::read_�dynamicAttributeName�(Tango::Attribute &attr) entering... " << endl;
	Tango::DevDouble	*att_value = get_�dynamicAttributeName�_data_ptr(attr.get_name());
	/*----- PROTECTED REGION ID(�controlNode.name�::read_�dynamicAttributeName�) ENABLED START -----*/
	//	Set the attribute value
	attr.set_value(att_value);
	
	/*----- PROTECTED REGION END -----*/	//	�controlNode.name�::read_dynamicAttribute
}
//--------------------------------------------------------
void �controlNode.name�::write_�dynamicAttributeName�(Tango::WAttribute &attr)
{
	DEBUG_STREAM << "�controlNode.name�::write_�dynamicAttributeName�(Tango::WAttribute &attr) entering... " << endl;
	//	Retrieve write value
	Tango::DevDouble	w_val;
	attr.get_write_value(w_val);
	/*----- PROTECTED REGION ID(�controlNode.name�::write_�dynamicAttributeName�) ENABLED START -----*/
	
	
	/*----- PROTECTED REGION END -----*/	//	�controlNode.name�::write_�dynamicAttributeName�
}
�ENDIF�
�ENDIF�
�ENDFOR�
�ENDIF�
//--------------------------------------------------------
void �cn.name�::add_dynamic_attributes()
{
	//	Example to add dynamic attribute:
	//	add_dynamicAttribute_dynamic_attribute("MydynamicAttributeAttribute");
	
	/*----- PROTECTED REGION ID(�cn.name�::add_dynamic_attributes) ENABLED START -----*/
	
	//	Add your own code to create and add dynamic attributes if any
	
	/*----- PROTECTED REGION END -----*/	//	�cn.name�::add_dynamic_attributes
}

//--------------------------------------------------------
�IF cn.commandResponseBlocks!=null�	
�FOR commandBlock: cn.commandResponseBlocks.commandResponseBlocks�
Tango::DevLong �cn.name�::�commandBlock.command.name.toLowerCase�(Tango::DevLong argin)
{
	Tango::DevLong argout;
	DEBUG_STREAM << "�cn.name�::�commandBlock.command.name�()  - " << device_name << endl;
	/*----- PROTECTED REGION ID(�cn.name�::�commandBlock.command.name.toLowerCase�) ENABLED START -----*/
	
	//	Add your own code
	
	/*----- PROTECTED REGION END -----*/	//	�cn.name�::�commandBlock.command.name.toLowerCase�
	return argout;
} 
�ENDFOR�
�ENDIF�
/*----- PROTECTED REGION ID(�cn.name�::namespace_ending) ENABLED START -----*/

//	Additional Methods

/*----- PROTECTED REGION END -----*/	//	�cn.name�::namespace_ending
} //	namespace

'''
	}
// Cpp Server Header File	
	def toCppServerHeaderCode(InterfaceDescription interfaceDesc,ControlNode controlNode){
'''
#ifndef �controlNode.name�_H
#define �controlNode.name�_H

#include <tango.h>


/*----- PROTECTED REGION END -----*/	//	�controlNode.name�.h

/**
 *  �controlNode.name� 
 *   
 */

namespace �controlNode.name�_ns
{
/*----- PROTECTED REGION ID(�controlNode.name�::Additional Class Declarations) ENABLED START -----*/

//	Additional Class Declarations

/*----- PROTECTED REGION END -----*/	//	�controlNode.name�::Additional Class Declarations

class �controlNode.name� : public TANGO_BASE_CLASS
{

/*----- PROTECTED REGION ID(�controlNode.name�::Data Members) ENABLED START -----*/

//	Add your own data members

/*----- PROTECTED REGION END -----*/	//	�controlNode.name�::Data Members

//	Device property data members
public:
�IF interfaceDesc.dataPoints!=null�	
�FOR dataPoint: interfaceDesc.dataPoints.dataPoints�
�IF dataPoint.name.contains("deviceProperty")�
�var devicePropertyName= dataPoint.name.split("deviceProperty").get(1)�
	
	string	�devicePropertyName�;

	string	�devicePropertyName�;
�ENDIF�
bool	mandatoryNotDefined;
�IF !dataPoint.name.contains("deviceProperty")&& !dataPoint.name.contains("deviceName") && !dataPoint.name.contains("subSystemId")�
//	Attribute data members
public:
	Tango::Dev�dataPoint.type.toString.toFirstUpper�	*attr_�dataPoint.name�_read;
	Tango::Dev�dataPoint.type.toString.toFirstUpper�	*attr_�dataPoint.name�_read;
�ENDIF�
�ENDFOR�
�ENDIF�
//	Constructors and destructors
public:
	/**
	 * Constructs a newly device object.
	 *
	 *	@param cl	Class.
	 *	@param s 	Device Name
	 */
	�controlNode.name�(Tango::DeviceClass *cl,string &s);
	/**
	 * Constructs a newly device object.
	 *
	 *	@param cl	Class.
	 *	@param s 	Device Name
	 */
	�controlNode.name�(Tango::DeviceClass *cl,const char *s);
	/**
	 * Constructs a newly device object.
	 *
	 *	@param cl	Class.
	 *	@param s 	Device name
	 *	@param d	Device description.
	 */
	�controlNode.name�(Tango::DeviceClass *cl,const char *s,const char *d);
	/**
	 * The device object destructor.
	 */	
	~�controlNode.name�() {delete_device();};


//	Miscellaneous methods
public:
	/*
	 *	will be called at device destruction or at init command.
	 */
	void delete_device();
	/*
	 *	Initialize the device
	 */
	virtual void init_device();
	/*
	 *	Read the device properties from database
	 */
	void get_device_property();
	/*
	 *	Always executed method before execution command method.
	 */
	virtual void always_executed_hook();

	/*
	 *	Check if mandatory property has been set
	 */
	 void check_mandatory_property(Tango::DbDatum &class_prop, Tango::DbDatum &dev_prop);

//	Attribute methods
public:
	//--------------------------------------------------------
	/*
	 *	Method      : �controlNode.name�::read_attr_hardware()
	 *	Description : Hardware acquisition for attributes.
	 */
	//--------------------------------------------------------
	virtual void read_attr_hardware(vector<long> &attr_list);
	//--------------------------------------------------------
	/*
	 *	Method      : �controlNode.name�::write_attr_hardware()
	 *	Description : Hardware writing for attributes.
	 */
	//--------------------------------------------------------
	virtual void write_attr_hardware(vector<long> &attr_list);

�IF controlNode.dataPointBlocks!=null�
 �FOR dataPointBlock: controlNode.dataPointBlocks.dataPointBlocks�
 �IF !dataPointBlock.dataPoint.name.contains("deviceProperty")&& !dataPointBlock.dataPoint.name.contains("deviceName") && !dataPointBlock.dataPoint.name.contains("subSystemId") && !dataPointBlock.dataPoint.name.contains("dynamic")�
 /**
 *	Attribute �dataPointBlock.dataPoint.name� related methods
 *	Description: SPeed
 *
 *	Data type:	Tango::Dev�dataPointBlock.dataPoint.type.toString.toFirstUpper�
 *	Attr type:	Scalar
 */
 virtual void read_�dataPointBlock.dataPoint.name�(Tango::Attribute &attr);
 virtual void write_�dataPointBlock.dataPoint.name�(Tango::WAttribute &attr);
 virtual bool is_�dataPointBlock.dataPoint.name�_allowed(Tango::AttReqType type)
�ENDIF�
�IF dataPointBlock.dataPoint.name.contains("dynamic")�
//	Dynamic attribute methods
public:
	/**
	 *	Attribute �dataPointBlock.dataPoint.name� related methods
	 *	Description: 
	 *
	 *	Data type:	Tango::Dev�dataPointBlock.dataPoint.type.toString.toFirstUpper�
	 *	Attr type:	Scalar
	 */
	virtual void read_�dataPointBlock.dataPoint.name�(Tango::Attribute &attr);
	virtual void write_�dataPointBlock.dataPoint.name�(Tango::WAttribute &attr);
	virtual bool is_�dataPointBlock.dataPoint.name�_allowed(Tango::AttReqType type);
	void add_�dataPointBlock.dataPoint.name�_dynamic_attribute(string attname);
	void remove_�dataPointBlock.dataPoint.name�_dynamic_attribute(string attname);
	Tango::DevDouble *get_�dataPointBlock.dataPoint.name�_data_ptr(string &name);
	map<string,Tango::DevDouble>	   �dataPointBlock.dataPoint.name�_data;
�ENDIF�
�ENDFOR�
�ENDIF�
	//--------------------------------------------------------
	/**
	 *	Method      : �controlNode.name�::add_dynamic_attributes()
	 *	Description : Add dynamic attributes if any.
	 */
	//--------------------------------------------------------
	void add_dynamic_attributes();



//	Command related methods
�IF controlNode.commandResponseBlocks!=null�
�FOR commandBlocks : controlNode.commandResponseBlocks.commandResponseBlocks�
	virtual Tango::DevString �commandBlocks.command.name.toLowerCase�(Tango::DevString argin);
	virtual bool is_�commandBlocks.command.name�_allowed(const CORBA::Any &any);
�ENDFOR�
�ENDIF�
/*----- PROTECTED REGION ID(�controlNode.name�::Additional Method prototypes) ENABLED START -----*/

//	Additional Method prototypes

/*----- PROTECTED REGION END -----*/	//	�controlNode.name�::Additional Method prototypes
};

/*----- PROTECTED REGION ID(�controlNode.name�::Additional Classes Definitions) ENABLED START -----*/

//	Additional Classes Definitions

/*----- PROTECTED REGION END -----*/	//	�controlNode.name�::Additional Classes Definitions

}	//	End of namespace

#endif   //	�controlNode.name�_H
'''
	}
	
	
	
	
}