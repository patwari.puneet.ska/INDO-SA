package Emulator.generator

import mncModel.PrimitiveValue
import mncModel.impl.IntValueImpl
import mncModel.impl.FloatValueImpl
import mncModel.impl.StringValueImpl
import org.eclipse.emf.common.util.EList
import java.util.ArrayList
import mncModel.impl.BoolValueImpl
import mncModel.DataPoint
import mncModel.PrimitiveValueType

class ParameterValueFinder {
	
	static def String findValueType(DataPoint dp) {
	  	if (dp !== null) { 
 	 		if (dp.type.equals(PrimitiveValueType.INT)) {
 			 	return "int"
			} else if (dp.type.equals(PrimitiveValueType.FLOAT)) {
				return "float"
			} else if (dp.type.equals(PrimitiveValueType.STRING)) {
				return "String"
			}else if(dp.type.equals(PrimitiveValueType.BOOLEAN)) {
				return "boolean"
			}
		}
	}
	
	static def String findValue(PrimitiveValue value) {
	 	if (value !== null || value!="") { 
	 		if (value instanceof IntValueImpl) {
 			 	return value.intValue.toString
			} else if (value instanceof FloatValueImpl) {
				return value.floatValue.toString
			} else if (value instanceof StringValueImpl) {
				var hh = value.stringValue.toString
				if(!hh.equals(""))
				return value.stringValue.toString
				else return "0"
			}else if(value instanceof BoolValueImpl){
				return value.boolValue.toString
			}
		}else return "0"
	}

	static def String findValue(EList<PrimitiveValue> value) {
		if (value != null) {
			if (value.size > 0 && value.get(0) instanceof IntValueImpl) {
				var str = ""
				for (valu : value) {
					var v = valu as IntValueImpl
					str = str + v.intValue.toString + ","
				}
				return str
			} else if (value.size > 0 && value.get(0) instanceof FloatValueImpl) {
				var str = ""
				for (valu : value) {
					var v = valu as FloatValueImpl
					str = str + v.floatValue.toString + ","
				}
				return str
			} else if (value.size > 0 && value.get(0) instanceof StringValueImpl) {
				var str = ""
				for (valu : value) {
					var v = valu as StringValueImpl
					str = str + v.stringValue.toString + ","
				}
				return str
			} else {
				return ""
			}

		}
	}

	def static convertIntoArrayList(EList<PrimitiveValue> list) {
		var arrayList = null as ArrayList<Integer>
		if (list != null && list.size > 0) {
			if (list.get(0) instanceof IntValueImpl) {
				arrayList = new ArrayList<Integer>();
				for (value : list) {
					var simpleVal = value as IntValueImpl
					arrayList.add(simpleVal.intValue)
				}
				return arrayList
			}
			if (list.get(0) instanceof FloatValueImpl) { 
				arrayList = new ArrayList<Integer>();
				for (value : list) {
					var simpleVal = value as FloatValueImpl
					arrayList.add(simpleVal.floatValue as int)
				}
				return arrayList
			}
			if (list.get(0) instanceof StringValueImpl) {
				arrayList = new ArrayList<Integer>();
				for (value : list) {
					var simpleVal = value as StringValueImpl
					arrayList.add(Integer.parseInt(simpleVal.stringValue))
				}
				return arrayList
			}

		}
	}

}
