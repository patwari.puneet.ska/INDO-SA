package Emulator.generator

import com.google.inject.Inject
import mncModel.AlarmBlock
import mncModel.CommandResponseBlock
import mncModel.ControlNode
import mncModel.DataPointBlock
import mncModel.EventBlock
import mncModel.Model
import mncModel.OperatingState
import mncModel.PrimitiveValue
import mncModel.impl.BoolValueImpl
import mncModel.impl.ControlNodeImpl
import mncModel.impl.FloatValueImpl
import mncModel.impl.IntValueImpl
import mncModel.impl.InterfaceDescriptionImpl
import mncModel.impl.StringValueImpl
import org.eclipse.emf.common.util.EList 
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.xtext.generator.IFileSystemAccess
import org.eclipse.xtext.generator.IGenerator
import Emulator.util.GeneratorUtils
import mncModel.SimpleType
import org.eclipse.xtext.EcoreUtil2
import mncModel.PrimitiveValueType
import mncModel.InterfaceDescription
import mncModel.DataPoint
import java.util.List
import java.util.HashSet
import java.util.ArrayList
import mncModel.Alarm
import com.ibm.icu.text.ChineseDateFormat.Field

class EmulatorJavaGenerator implements IGenerator {
@Inject 
static EList<DataPointBlock> dataPointBlockList	 
@Inject  
static EList<AlarmBlock> alarmBlockList
@Inject
static EList<EventBlock> eventBlockList
@Inject 
static EList<CommandResponseBlock> commandResponseBlockList
IFileSystemAccess fsa
	//	@Inject
	//	ArrayList<String> nodeLists
	var controlNode = null as ControlNodeImpl
	boolean connect = false;
	def void setConnect(boolean connect) { 
		this.connect = connect 
	}

	override doGenerate(Resource resource, IFileSystemAccess fsa) {
 		this.fsa = fsa
		val instance = resource.contents.head as  Model
		val interface = instance.eContents.filter(InterfaceDescriptionImpl).get(0)
		initializeTheComponants(instance)
if (instance != null) { 
			fsa.generateFile(controlNode.name + ".java",
				toJavaCode(interface as InterfaceDescriptionImpl, controlNode as ControlNodeImpl,fsa,null,null))
			}
	}
	
	def initializeTheComponants(Model instance){
		controlNode = instance.eContents.filter(ControlNodeImpl).get(0)
		dataPointBlockList = new GeneratorUtils().getDataPointBlocks(controlNode)
		eventBlockList = new GeneratorUtils().getEventBlocks(controlNode)
		alarmBlockList = new  GeneratorUtils().getAlarmBlocks(controlNode)
		commandResponseBlockList = new  GeneratorUtils().getCommandResponseBlocks(controlNode)
	} 
 
	 
def toJavaCode(InterfaceDescription ides, ControlNode cn,IFileSystemAccess fsa,List<String> childDevices,String childDeviceProxy) {
'''
/*----- PROTECTED REGION END -----*///	�cn.name.toFirstUpper�.java


package com.mnc.nodes.java;

/*----- PROTECTED REGION ENABLED START -----*/

import com.tango.nodes.utils.*;
import fr.esrf.Tango.DevFailed;
import fr.esrf.Tango.DevState; 
import fr.esrf.Tango.DispLevel;
import fr.esrf.TangoApi.DevicePipe;
import fr.esrf.TangoApi.PipeBlob;
import fr.esrf.TangoApi.PipeDataElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.ext.XLogger;
import org.slf4j.ext.XLoggerFactory; 
import org.tango.DeviceState;
import org.tango.server.InvocationContext;
import org.tango.server.ServerManager;
import org.tango.server.annotation.*;
import org.tango.server.attribute.AttributeValue;
import org.tango.server.device.DeviceManager;
import org.tango.server.dynamic.DynamicManager;
import org.tango.server.events.EventType;
import org.tango.server.pipe.PipeValue;
import org.tango.logging.LoggingManager;
import fr.esrf.TangoApi.events.TangoUser;
import fr.esrf.TangoApi.DeviceProxy;
import fr.esrf.TangoApi.DeviceData;
//	Import Tango IDL types
// additional imported packages

/*----- PROTECTED REGION END -----*///	�cn.name.toFirstUpper�.imports
  
@Device
public class �cn.name.toFirstUpper� {

	public static   Logger logger = (Logger) LoggerFactory.getLogger(�cn.name.toFirstUpper�.class);
	private static final XLogger xlogger = XLoggerFactory.getXLogger(�cn.name.toFirstUpper�.class);
	private static String deviceName = "generatedNode/�cn.name.toFirstUpper�/test";
	DeviceProxy selfDeviceProxy;
	�IF this.connect�
	private Simulator sim = new Simulator(deviceName);
	�ENDIF�
	�IF(childDevices!=null &&  !childDevices.isEmpty)�
	String[] childDevices = new String[]{�FOR childDevice : childDevices�
	"�childDevice�"�IF(!childDevice.equalsIgnoreCase(childDevices.last))�,�ENDIF�
	�ENDFOR�};
	�ENDIF�
	�IF(childDeviceProxy!=null)�
	DeviceProxy childDeviceProxy;
	�ENDIF�
	// ========================================================
	// Programmer's data members 
	// ========================================================
	/*----- PROTECTED REGION ID(�cn.name.toFirstUpper�.variables) ENABLED START -----*/

	// Put static variables here

	/*----- PROTECTED REGION END -----*/// �cn.name.toFirstUpper�.variables
	/*----- PROTECTED REGION ID(�cn.name.toFirstUpper�.private) ENABLED START -----*/


	// Put private variables here
		@Pipe(name = "Mnc_EVENT", label = "EVENT", displayLevel = DispLevel._OPERATOR)
		private PipeValue Mnc_EVENT = new PipeValue();
	
		public void setMnc_EVENT(PipeValue Mnc_EVENT) throws DevFailed {
			xlogger.entry();
			this.Mnc_EVENT = Mnc_EVENT;
			deviceManager.pushPipeEvent("Mnc_EVENT", Mnc_EVENT);
			xlogger.info("EVENT::"+Mnc_EVENT.getValue().getName());
			xlogger.exit();  
		}  
	
		public PipeValue getMnc_EVENT() {
			xlogger.entry();
			// Write programmer code
			xlogger.exit();
			return this.Mnc_EVENT;
		}
	
		@Pipe(name = "Mnc_ALARM", label = "ALARM", displayLevel = DispLevel._OPERATOR)
		private PipeValue Mnc_ALARM;
	
		public PipeValue getMnc_ALARM() {
			xlogger.entry();
			// Write programmer code
			xlogger.exit();
			return this.Mnc_ALARM;
		} 
	
		public void setMnc_ALARM(PipeValue Mnc_ALARM) throws DevFailed {
			xlogger.entry();
			deviceManager.pushPipeEvent("Mnc_ALARM", Mnc_ALARM);
			setStatus("Alarm Raised :: "+Mnc_ALARM.getValue().getName());
			this.Mnc_ALARM = Mnc_ALARM;
			xlogger.info("ALARM::"+Mnc_ALARM.getValue().getName());
			xlogger.exit();
		}
		
		@Pipe(name = "Mnc_STATE", label = "STATE", displayLevel = DispLevel._OPERATOR)
		private PipeValue Mnc_STATE;
		public PipeValue getMnc_STATE() {
			xlogger.entry();
			// Write programmer code
			xlogger.info("CURRENT_STATE::"+Mnc_STATE.getValue().getName());
			xlogger.exit();
			return Mnc_STATE;
		}
		public void setMnc_STATE(PipeValue STATE) {
			xlogger.entry();
			xlogger.info("NEXT_STATE::"+STATE.getValue().getName());
			Mnc_STATE = STATE;
			xlogger.exit();
		}	
	//========================================================
	//	Property data members and related methods
	//========================================================

	//========================================================
	//	Miscellaneous methods
	//========================================================
	/**
	 * Initialize the device.
	 *  
	 * @throws DevFailed if something fails during the device initialization.
	 */
	@Init(lazyLoading = true)
		public void initDevice() throws DevFailed {
			xlogger.entry();
			/*----- PROTECTED REGION ID(LeafNode.initDevice) ENABLED START -----*/
			PipeValue alarmValue = new PipeValue();
			PipeValue eventValue = new PipeValue();
			PipeValue stateValue = new PipeValue();
			alarmValue.setValue(new PipeBlob("NO_ALARM"));
			eventValue.setValue(new PipeBlob("NO_EVENT"));
			stateValue.setValue(new PipeBlob("START"));
			setMnc_ALARM(alarmValue);
			setMnc_STATE(stateValue);
			setMnc_EVENT(eventValue);
			xlogger.info("INIT_STATE::"+stateValue.getValue().getName());
			xlogger.info("INIT_EVENT::"+eventValue.getValue().getName());
			xlogger.info("INIT_ALARM::"+alarmValue.getValue().getName());
			selfDeviceProxy = new DeviceProxy(deviceName);
			�IF(childDevices!=null && !childDevices.isEmpty)�
			UserEventCallBack callback = new UserEventCallBack(selfDeviceProxy,this);
			
			for(String child : childDevices) {
							DeviceProxy devPro = new DeviceProxy(child);
							 
							try {
								devPro.subscribe_event("Mnc_ALARM",TangoUser.PIPE_EVENT, callback, new String[0]);
							} catch (DevFailed e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						};
			�ENDIF� 
			�IF(childDeviceProxy!=null)�
			childDeviceProxy = new DeviceProxy("�childDeviceProxy�");
			�ENDIF�
			// Put your device initialization code here
			/*----- PROTECTED REGION END -----*/ // LeafNode.initDevice
			xlogger.exit();
		}
	
	
        //	Put your code here
		
		/*----- PROTECTED REGION END -----*/	//	LeafNode.setDynamicManager

	
	/**
	 * Device management. Will be injected by the framework.
	 */
		@DeviceManagement
		DeviceManager deviceManager;
	
		public void setDeviceManager(DeviceManager deviceManager) {
			
			this.deviceManager = deviceManager;
	
		}
	
	// ========================================================
	// Attribute data members and related methods
	// ========================================================
	�declaireAttributes(cn)�
	
	@Attribute(name="HasControl",pollingPeriod = 1000)
	private boolean hasControl ;
	public synchronized boolean getHasControl() throws DevFailed{
		return this.hasControl;
		}
	public synchronized void setHasControl(boolean hasControl) throws DevFailed{
		this.hasControl  = hasControl;
	}  
	//========================================================
	//	Command data members and related methods
	//========================================================
	/**
	 * The state of the device
	*/
	@State
	private DevState state = DevState.UNKNOWN;
	/**
	 * Execute command "State".
	 * description: This command gets the device state (stored in its 'state' data member) and returns it to the caller.
	 * @return Device state
	 * @throws DevFailed if command execution failed.
	 */
	public final DevState getState() throws DevFailed {
		/*----- PROTECTED REGION ID(LeafNode.getState) ENABLED START -----*/

        //	Put state code here
		
		/*----- PROTECTED REGION END -----*/	//	LeafNode.getState
		return state;
	}
	/**
	 * Set the device state
	 * @param state the new device state
	 */
	public void setState(final DevState state) {
		this.state = state;
	}
	/**
	 * The status of the device
	 */
	@Status(isPolled=true,pollingPeriod=1000)
	private String status = "Server is starting. The device state is unknown";
	/**
	 * Execute command "Status".
	 * description: This command gets the device status (stored in its 'status' data member) and returns it to the caller.
	 * @return Device status
	 * @throws DevFailed if command execution failed.
	 */
	public final String getStatus() throws DevFailed {
		/*----- PROTECTED REGION ID(LeafNode.getStatus) ENABLED START -----*/

        //	Put status code here
		
		/*----- PROTECTED REGION END -----*/	//	LeafNode.getStatus
		return status;
	}
	/**
	 * Set the device status
	 * @param status the new device status
	 */
	public void setStatus(final String status) {
		this.status = status;
	}
// Set Response Pipe to receive Responses from Commands	
	@Pipe(name = "ResponsePipe")
		PipeValue responsePipe = new PipeValue();
	
		public PipeValue getResponsePipe() {
			xlogger.entry();
			xlogger.exit();
			return responsePipe;
		}
	
		public void setResponsePipe(PipeValue pipe) {
			xlogger.entry();
			this.responsePipe = pipe;
			// System.out.println(this.responsePipe);
			xlogger.exit();
		}
		
	�declareCommand(cn,childDevices,childDeviceProxy)�
	
	// Declaire Events
	�declareEvent(cn)�
	
	//Declaire Alarms
	�declareAlarm(cn)�
	
	// Declare States
	�declareStates(cn)�

	
	/*----- PROTECTED REGION END -----*/// �controlNode.name.toFirstUpper�.methods

	/**
	 * Starts the server.
	 * 
	 * @param args
	 *            program arguments (instance_name [-v[trace level]] [-nodb
	 *            [-dlist <device name list>] [-file=fileName]])
	 */
	public static void main(final String[] args) throws DevFailed {
		DataBaseHandler.addDevice(deviceName); // Uncomment this to enter the device into the database
		LoggingManager.getInstance().setLoggingLevel(4,�controlNode.name.toFirstUpper�.class);
		LoggingManager.getInstance().addFileAppender("Logging.txt", deviceName);
		LoggingManager.getInstance().startAll(); 
		ServerManager.getInstance().start(args, �controlNode.name.toFirstUpper�.class);
		logger.info("------- Started -------------");
 
	}

}

'''
	}
	
def declareStates(ControlNode controlNode)
{
var statesUsedInControlNode = new HashSet<OperatingState>()
if(commandResponseBlockList!=null){
	for(commandRespBlock : commandResponseBlockList){
	var transUtil = commandRespBlock.transition
	if(transUtil!=null){
		var transitions = transUtil.transitions
		for(trans : transitions){
			statesUsedInControlNode.add(trans.currentState)
			statesUsedInControlNode.add(trans.nextState)
		}
	}
	
	}
}
'''
�IF statesUsedInControlNode!=null�
�FOR statesinCn  : statesUsedInControlNode�
public void �statesinCn.name.toUpperCase�_STATE()
{
	xlogger.entry();
	PipeValue stateValue = new PipeValue();
	stateValue.setValue(new PipeBlob("�statesinCn.name.toUpperCase�"));
	setMnc_STATE(stateValue);
	// Write State Transition Specific Code here
	xlogger.exit();
}
�ENDFOR�
�ENDIF�
'''
}

def declareEvent(ControlNode controlNode){
	if(eventBlockList!=null){
		'''
		�FOR eventBlock : eventBlockList�
public void �eventBlock.event.name.toUpperCase�_EVENT() throws DevFailed
{
	xlogger.entry();
	PipeValue eventValue = new PipeValue();
	eventValue.setValue(new PipeBlob("�eventBlock.event.name.toUpperCase�"));
	setMnc_EVENT(eventValue);
	// Write Event Handling Specific Code here
	xlogger.exit();
}
		�ENDFOR�
		'''
	}
}

def declareAlarm(ControlNode controlNode){
	if(alarmBlockList!=null){
		'''
		�FOR alarmBlock : alarmBlockList�
public void �alarmBlock.alarm.name.toUpperCase�_ALARM() throws DevFailed
{
	xlogger.entry();
	PipeValue alarmValue = new PipeValue();
	PipeBlob pipeBlob = new PipeBlob("�alarmBlock.alarm.name.toUpperCase�");
	�IF alarmBlock.alarmHandling==null�
	PipeDataElement pde = new PipeDataElement("hasControl", false);
	pipeBlob.add(pde);
	�ENDIF�
	alarmValue.setValue(pipeBlob);
	setMnc_ALARM(alarmValue);
	setStatus("Alarm �alarmBlock.alarm.name.toUpperCase� Being Handled");
	�IF(alarmBlock.alarmHandling!=null && alarmBlock.alarmHandling.triggerAction!=null)�
	�IF(alarmBlock.alarmHandling.triggerAction.command!=null)�
	if(hasControl)
	{ 
	�FOR comm : alarmBlock.alarmHandling.triggerAction.command�
	�comm.name�("");
	�ENDFOR�   
	setHasControl(false);
	initDevice();
	}
	�ENDIF� 
	�ENDIF�
	// Write Alarm Handling Specific Code here		
	xlogger.exit();
}
		
		�ENDFOR�
		'''
	} 
}
	
 
def declareCommand(ControlNode controlNode,List<String> childDevice,String childDeviceProxy){
 var List<String> commandPropertiesAsPerTango = newArrayList()
 commandPropertiesAsPerTango.add("inTypeDesc")
  commandPropertiesAsPerTango.add("outTypeDesc")
 '''
 �IF controlNode!=null�
 �var commandBlockUtil = controlNode.commandResponseBlocks�
 �IF commandBlockUtil!=null�
 �var commandBlockList = controlNode.commandResponseBlocks.commandResponseBlocks�
 �IF commandBlockList!=null�
 �FOR commandBlock : commandBlockList�
 �var command = commandBlock.command�
 �var commandParameters = command.parameters�
@Command(name="�command.name�"
�IF commandParameters!=null� 
�FOR param : commandParameters�
�var simpleType = param as SimpleType�
�IF commandPropertiesAsPerTango.contains(simpleType.name)�
�IF !param.equals(commandParameters.last)�,�ENDIF�
�simpleType.name�=�IF simpleType.type.equals(PrimitiveValueType.STRING)�"�ParameterValueFinder.findValue(simpleType.value)�"
�ELSE��ParameterValueFinder.findValue(simpleType.value)�
�ENDIF�
�IF !param.equals(commandParameters.last)�,�ENDIF�
�ENDIF� 
�ENDFOR�
 �ENDIF�)  
 public synchronized String �command.name�(String �command.name�Params) throws DevFailed {
		xlogger.entry();
		xlogger.info("COMMAND::"+"�command.name.toUpperCase�"); 
		/*----- PROTECTED REGION ID(�controlNode.name.toFirstUpper�.�command.name�) ENABLED START -----*/
    setStatus("Executing Command �command.name.toUpperCase�");
      �IF(childDevice!=null)�
      DeviceData dd = new DeviceData();
      dd.insert("22.0");
      �IF(childDeviceProxy!=null)�
      childDeviceProxy.command_inout("�command.name�", dd);
      �ENDIF�
      �ENDIF�
      �IF commandBlock.commandTranslation!=null�
      �IF commandBlock.commandTranslation.translatedCommands!=null� 
      �FOR translatedCom : commandBlock.commandTranslation.translatedCommands�
      �var childDeviceName = (EcoreUtil2.getContainerOfType(translatedCom,Model).systems.get(1) as ControlNode).name.toFirstUpper�
      DeviceProxy dp_�translatedCom.name.toLowerCase��childDeviceName� = new DeviceProxy("generatedNode/�childDeviceName�/test");
      dp_�translatedCom.name.toLowerCase��childDeviceName�.command_inout("�translatedCom.name.toUpperCase�", dd);
      �ENDFOR� 
      �ENDIF�
      �ENDIF�
       �IF commandBlock.transition!=null && commandBlock.transition.transitions!=null�
        �FOR stateTrans : commandBlock.transition.transitions�
        if("�stateTrans.currentState.name.toUpperCase�".equals(getMnc_STATE().getValue().getName())){
���        	xlogger.info("TRANSITION::BEGIN");
        	// Exit Action 
         	�IF stateTrans.exitAction!=null�
        	�IF stateTrans.exitAction.alarm!=null�
         	�FOR alarm : stateTrans.exitAction.alarm�
         	�IF alarmBlockList!=null�
        	�FOR alarmBlock:alarmBlockList�
        	�IF alarmBlock.alarm.name.equalsIgnoreCase(alarm.name)�
        	�alarmBlock.alarm.name.toUpperCase�_ALARM();
        	�ENDIF��ENDFOR�
        	�ENDIF�
        	�ENDFOR�  
        	�ENDIF�
        	�IF stateTrans.exitAction.event!=null�
        	�FOR event : stateTrans.exitAction.event�
         	�IF eventBlockList!=null�
        	�FOR eventBlock:eventBlockList�
        	�IF eventBlock.event.name.equalsIgnoreCase(event.name)�
        	�eventBlock.event.name.toUpperCase�_EVENT();
        	�ENDIF�
        	�ENDFOR�
        	�ENDIF�
        	�ENDFOR�
        	�ENDIF�
        	�IF stateTrans.exitAction.command!=null�
        	�FOR actionCommand : stateTrans.exitAction.command�
        	�actionCommand.name.toUpperCase�("");
        	�ENDFOR�
        	�ENDIF�
        	�ENDIF�
        	// State Transition
        	�stateTrans.nextState.name.toUpperCase�_STATE();
        	// Entry Action 
        	�IF stateTrans.entryAction!=null�
        	�IF stateTrans.entryAction.alarm!=null�
        	�FOR alarm : stateTrans.entryAction.alarm�
        	�IF alarmBlockList!=null�
        	�FOR alarmBlock:alarmBlockList�
        	�IF alarmBlock.alarm.name.equalsIgnoreCase(alarm.name)�
        	�alarmBlock.alarm.name.toUpperCase�_ALARM();
        	�ENDIF�
        	�ENDFOR�
        	�ENDIF�
        	�ENDFOR� 
        	�ENDIF� 
        	�IF stateTrans.exitAction !=null  && stateTrans.exitAction.event!=null�
        	�FOR event : stateTrans.entryAction.event� 
        	�IF eventBlockList!=null�
        	�FOR eventBlock:eventBlockList�
        	�IF eventBlock.event.name.equalsIgnoreCase(event.name)�
        	�eventBlock.event.name.toUpperCase�_EVENT();
        	�ENDIF�
        	�ENDFOR�
        	�ENDIF� 
         	�ENDFOR�
        	�ENDIF�
        	�IF stateTrans.entryAction.command!=null && stateTrans.exitAction!=null && stateTrans.exitAction.command!=null�
        	�FOR actionCommand : stateTrans.exitAction.command�
        	�actionCommand.name.toUpperCase�("");
        	�ENDFOR�
        	�ENDIF�
        	�ENDIF�
���        	xlogger.info("TRANSITION::END");
        	}
         �ENDFOR�
        �ENDIF�
         
        //	Put command code here
        �IF this.connect�
        		sim.responseGenerator("�command.name.toUpperCase�", �command.name�Params);
        		String �command.name�response = responsePipe.getValue().getName().toString();
        		
���       	String �command.name�response =responsePipe.getValue().getName().toString();
        xlogger.info("RESPONSE::"+�command.name�response);
        �ENDIF�
		xlogger.exit();
		
		�IF this.connect�
		return �command.name�response;
		�ELSE�
		return "�command.name�_Response Received";
		�ENDIF� 
	}   
 �ENDFOR�
 �ENDIF�
 �ENDIF�  
 �ENDIF�
 
	'''
}
	def declaireAttributes(ControlNode controlNode) 
	'''  
	�IF controlNode!==null�
	�IF controlNode.dataPointBlocks!==null�
	�IF controlNode.dataPointBlocks.dataPointBlocks!==null�
	�var EList<DataPointBlock> dataPointList = controlNode.dataPointBlocks.dataPointBlocks� 
	�IF dataPointList !== null� 
			�FOR dataPoint : dataPointList� 
			�var dataPointName = dataPoint.dataPoint.name�
			�var minValue = null as Object�
			�var maxValue = null as Object�
				@Attribute(name="�dataPointName.toFirstUpper�",pollingPeriod = 1000)
				�IF dataPoint.dataPointHandling!==null�
				
		 		�IF dataPoint.dataPointHandling.checkDataPoint!==null�
		 		// min value =	� minValue = dataPoint.dataPointHandling.checkDataPoint.checkMinValue.dataPointValue�
		 	// max value =	� maxValue = dataPoint.dataPointHandling.checkDataPoint.checkMaxValue.dataPointValue�
		 	@AttributeProperties(minAlarm="�minValue�",maxAlarm="�maxValue�")
				�ENDIF�
				�ENDIF� 
			private �dataPoint.dataPoint.type� �dataPointName� �IF dataPoint.dataPoint.value!==null�= �dataPoint.dataPoint.value.getDataPointValue��IF ParameterValueFinder.findValueType(dataPoint.dataPoint).equals("float")�F�ENDIF��ENDIF�;
				public synchronized �dataPoint.dataPoint.type� get�dataPointName.toFirstUpper�() throws DevFailed{
					�IF dataPoint.dataPointHandling!==null�
					�IF(dataPoint.dataPointHandling.action!==null && dataPoint.dataPointHandling.action.alarm!==null)�
					�IF(minValue!==null)�
					if(�dataPointName�<�minValue�){
						�FOR alrm : dataPoint.dataPointHandling.action.alarm�
���						�IF(alrm.name.contains("MIN"))�
						�alrm.name.toUpperCase�_ALARM();
���						�ENDIF�
						�ENDFOR�
					}�ENDIF�	 
					�IF(maxValue!==null)�
					if(�dataPointName�>�maxValue�){
					�FOR alrm : dataPoint.dataPointHandling.action.alarm�
���						�IF(alrm.name.contains("MAX"))�
						�alrm.name.toUpperCase�_ALARM();
���						�ENDIF�
						�ENDFOR�						
					}
					�ENDIF�	
					�ENDIF�
					�ENDIF�
					return this.�dataPointName�;
				}
				public synchronized void set�dataPoint.dataPoint.name.toFirstUpper�(�dataPoint.dataPoint.type� �dataPoint.dataPoint.name�) throws DevFailed{
					this.�dataPoint.dataPoint.name�  = �dataPoint.dataPoint.name�;
				}  
			�ENDFOR�
		�ENDIF�	 
		�ENDIF�	
		�ENDIF�	
		�ENDIF�		 
	'''
	
	def getDataPointValue(PrimitiveValue value) {
		if(value !==null){
		if (value instanceof IntValueImpl) {
			return value.intValue 
	 	}
		if (value instanceof FloatValueImpl) {
			return value.floatValue
		}
		if (value instanceof StringValueImpl) {
			return value.stringValue
		} 
		if (value instanceof BoolValueImpl) {
			return value.boolValue
		}
		 
		}else {
			return null;
		}
	}
	
	def toJavaCodeCallback(mncModel.ControlNode node, ArrayList<String> strings) {
		'''
		package com.tango.generatedDeviceServers;
		
		import java.lang.reflect.Method;
		import java.util.HashMap;
		import java.util.HashSet;
		import java.util.Map;
		import java.util.Set;
		import java.util.Timer;
		import java.util.TimerTask;
		import fr.esrf.Tango.DevFailed;
		import fr.esrf.TangoApi.CallBack;
		import fr.esrf.TangoApi.DeviceAttribute;
		import fr.esrf.TangoApi.DeviceProxy;
		import fr.esrf.TangoApi.events.EventData;
		import fr.esrf.TangoApi.events.EventQueue;
		import fr.esrf.TangoApi.PipeDataElement;
		public class UserEventCallBack extends CallBack {
		 
			/**
			 * 
			 */
		
			private static final long serialVersionUID = 1L;
			static int interval = 10;
			static Timer timer = new Timer();
			static int counter = 10;
			int delay = 0;
			int period = 1000;
			static DeviceProxy childDeviceProxy;
			static DeviceProxy selfDeviceProxy;
			static Map<DeviceProxy, String> eventMap = new HashMap<DeviceProxy, String>();
			EventQueue eq = new EventQueue();
			SupervisoryController_CN selfObject;
			Set<Map<Map<String, String>, String>> alarmRuleMap = new HashSet<Map<Map<String, String>, String>>();
			public UserEventCallBack(DeviceProxy selfDeviceProxy, SupervisoryController_CN supervisoryController_CN) {
				// TODO Auto-generated constructor stub
				this.selfDeviceProxy = selfDeviceProxy;
				this.selfObject = supervisoryController_CN;
				
				
				�IF(alarmBlockList!=null)�	
				�FOR alarmBlock : alarmBlockList� 
				Map<String, String> m_�alarmBlock.alarm.name.toLowerCase� = new HashMap<String, String>();
				Map<Map<String,String>,String> n_�alarmBlock.alarm.name.toLowerCase� = new HashMap<Map<String,String>,String>();
				�IF alarmBlock.alarmCondition!=null && alarmBlock.alarmCondition.responsibleItems!=null && alarmBlock.alarmCondition.responsibleItems.responsibleAlarms!=null�
				�FOR responsibleAlarm : alarmBlock.alarmCondition.responsibleItems.responsibleAlarms�
				�var conNod = EcoreUtil2.getContainerOfType(EcoreUtil2.getContainerOfType(responsibleAlarm, InterfaceDescription),Model).systems.get(1) as ControlNode�
				m_�alarmBlock.alarm.name.toLowerCase�.put("generatedNode/�conNod.name.toFirstUpper�/test", "�responsibleAlarm.name.toUpperCase�");
				�ENDFOR�	
				�ELSE�
				�var conNod = EcoreUtil2.getContainerOfType(EcoreUtil2.getContainerOfType(alarmBlock.alarm, InterfaceDescription),Model).systems.get(1) as ControlNode�
				m_�alarmBlock.alarm.name.toLowerCase�.put("generatedNode/�conNod.name.toFirstUpper�/test", "�alarmBlock.alarm.name.toUpperCase�");
				�ENDIF�				
				
				n_�alarmBlock.alarm.name.toLowerCase�.put(m_�alarmBlock.alarm.name.toLowerCase�, "�alarmBlock.alarm.name.toUpperCase�_ALARM");
				alarmRuleMap.add(n_�alarmBlock.alarm.name.toLowerCase�);
				
				�ENDFOR�
				�ENDIF�
			}
		
			
				@Override
				public void push_event(EventData event) {
					
					
					if(eventMap.get(event.device)==null)
					{
					eventMap.put(event.device, event.devicePipe.getPipeBlob().getName());
					}
					if(event.devicePipe.getPipeBlob().size()>0)
					{
					PipeDataElement control = event.devicePipe.getPipeBlob().get(0);
					if(control.extractBooleanArray()[0]==false) {
						sendControlToTheSupervisoryDevice();
						return;
					}
					}
					if(eventMap.size()==1 && eventMap.get(event.device)!=null && interval ==10 )
					{
					timer.schedule(new TimerTask() {
						public void run() {
							System.out.println(interval--);
							if (interval < 0 && eventMap.size() == 1) {
								timer.cancel();
							sendControlToTheChildDevice(event);
							}
							
						}
					}, 0, 1000);
					}else if (interval > 0 && eventMap.size() > 1) {
						timer.cancel();
						sendControlToTheSupervisoryDevice();
					}
				}
			
				private void sendControlToTheChildDevice(EventData event) {
					interval = 10;
					System.out.println("Control to child device");
					try {
						event.device.write_attribute(new DeviceAttribute("hasControl", true));
						selfDeviceProxy.write_attribute(new DeviceAttribute("hasControl", false));
					} catch (DevFailed e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					eventMap.clear();
					timer = new Timer();
				}
				private void sendControlToTheSupervisoryDevice() {
					Map<String, String> eventRaidsedMap = new HashMap<String, String>();
					interval = 10;
					System.out.println("Control to supervisory device");
					try {
						eventMap.forEach((k, v) -> {
							try {
								k.write_attribute(new DeviceAttribute("hasControl", false));
								eventRaidsedMap.put(k.name(), v);
							} catch (DevFailed e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						});
						selfDeviceProxy.write_attribute(new DeviceAttribute("hasControl", true));
			
						raiseSupervisoryControllerAlarmAsPerTheGeneratedAlarms(eventRaidsedMap);
					} catch (DevFailed e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					eventMap.clear();
					timer = new Timer();
				}
			
				private void raiseSupervisoryControllerAlarmAsPerTheGeneratedAlarms(Map<String, String> eventRaidsedMap) {
					// TODO Auto-generated method stub
					alarmRuleMap.forEach(setItem -> {
						setItem.forEach((K, V) -> {
							System.out.println(K);
							if (K.equals(eventRaidsedMap)) {
								try {
									Method method = selfObject.getClass().getMethod(V);
									method.invoke(selfObject, null);
									return;
								} catch (Exception e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}
						});
					});
			
				}
			
			}
		'''
	}


	
	

}
