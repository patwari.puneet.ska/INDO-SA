package Emulator.generator

import org.eclipse.core.commands.AbstractHandler
import org.eclipse.core.commands.ExecutionEvent
import org.eclipse.core.commands.ExecutionException
import org.eclipse.core.resources.IFile
import org.eclipse.core.runtime.CoreException
import org.eclipse.core.runtime.NullProgressMonitor
import org.eclipse.jface.dialogs.MessageDialog
import org.eclipse.ui.handlers.HandlerUtil
import org.eclipse.xtext.builder.EclipseResourceFileSystemAccess
import org.eclipse.xtext.resource.XtextResource
import org.eclipse.xtext.ui.editor.XtextEditor
import org.eclipse.xtext.util.concurrent.IUnitOfWork
import org.eclipse.jdt.core.JavaCore
import org.eclipse.jdt.core.IPackageFragment
import org.eclipse.jdt.core.IClasspathEntry
import java.util.List

class EmulatorGeneratorCPP extends AbstractHandler {

	

	override execute(ExecutionEvent event) throws ExecutionException {
		var tangoSrcFolder = null as IPackageFragment
		val generate = MessageDialog.openConfirm(HandlerUtil.getActiveShell(event), "Generate JAVA",
			"Do you want to generate CPP code?");
		if (!generate) {
			return null
		} else {

			var activeEditor = HandlerUtil.getActiveEditor(event);
			var file = activeEditor.getEditorInput().getAdapter(IFile) as IFile
			if (file !== null) {
				var project = file.getProject();
				var javaProject = JavaCore.create(project)
				var srcGenFolder = project.getFolder("src-gen");
				
				try {
					if (!srcGenFolder.exists()) 
						srcGenFolder.create(true, true, new NullProgressMonitor)
					var srcRoot = javaProject.getPackageFragmentRoot(srcGenFolder);
					var oldEntries = javaProject.getRawClasspath();
					var List<IClasspathEntry> newEntries = newArrayList(oldEntries)
						
				//Check if the newENtry is already present in oldEntries of sources
					var newEntry = JavaCore.newSourceEntry(srcRoot.getPath)
				    newEntries.add(newEntry);
				    for(ce : oldEntries)
				    	if(ce.equals(newEntry))
				    		newEntries.remove(newEntry)
				    javaProject.setRawClasspath(newEntries, null)
					tangoSrcFolder = srcRoot.createPackageFragment("com.tango.nodes.cpp",true, null)
				} catch (CoreException e) {
					e.printStackTrace
					return null;
				}
				  

 				val fsa = new EclipseResourceFileSystemAccess
				fsa.setOutputPath(tangoSrcFolder.path.toString());
				var teste = fsa.getOutputConfigurations();
					fsa.root = project.workspace.root
				var it = teste.entrySet().iterator();

				//make a new Outputconfiguration <- needed
				while (it.hasNext()) {

					var next = it.next();
					var out = next.getValue();
					out.isOverrideExistingResources();
		 			out.setCreateOutputDirectory(true); // <--- do not touch this
				}

				if (activeEditor instanceof XtextEditor) {
					(activeEditor as XtextEditor).getDocument().readOnly(
						new IUnitOfWork<Boolean, XtextResource>() {

							override public Boolean exec(XtextResource state) throws Exception {
								new EmulatorCppGenerator().doGenerate(state, fsa);
								return Boolean.TRUE;
							}
						});

				}
			}
			return null;

		}
	}

}
