package com.mncml.implementation.pogo.simulator.generator

import com.google.gson.JsonObject
import tangoSimLibSimulator.ConstantQuantity
import tangoSimLibSimulator.DataSimulation
import tangoSimLibSimulator.DeterministicSignal
import tangoSimLibSimulator.GaussianSlewLimited
import tangoSimLibSimulator.RuntimeSpecifiedWaveform
import tangoSimLibSimulator.Simulation
import org.apache.commons.lang3.math.NumberUtils

class SIMDDAttributeCreator {

	def JsonObject createSIMDDDynamicAttributes(DataSimulation attribute) {
		var basicAttributeData = new JsonObject
		try {
			if (attribute.pogoAttr !== null) {
				var attributeJsonObject = new JsonObject
				var pogoAttr = attribute.pogoAttr
				// Attribute fields
				attributeJsonObject.addProperty("name", pogoAttr.name)
				if (pogoAttr.dataType !== null) {
					attributeJsonObject.addProperty("data_type", pogoAttr.dataType.class.simpleName.replace("TypeImpl",""))
				} 
				if (pogoAttr.attType !== null) {
					attributeJsonObject.addProperty("data_format", pogoAttr.attType)
				}

				// Data Shape
				var dataShape =  new JsonObject
				if (pogoAttr.maxX !== null) {
					dataShape.addProperty("max_dim_x",numberUtilsBlankStringRemover(pogoAttr.maxX))
				}
 				if (pogoAttr.maxY !== null) {
					dataShape.addProperty("max_dim_y", numberUtilsBlankStringRemover(pogoAttr.maxY))
				}
				if (dataShape.entrySet.size>0) {
					attributeJsonObject.add("data_shape", dataShape)
				}
				// Data Shape Ends
				// RW
				if (pogoAttr.rwType !== null) {
					var rw = new JsonObject
					rw.addProperty("writable", pogoAttr.rwType)
					attributeJsonObject.add("attributeInterlocks", rw)
				}

				// Device Properties	
				if (pogoAttr.properties !== null) {
					var property = pogoAttr.properties
					if (property.unit !== null) {
						attributeJsonObject.addProperty("unit", property.unit)
					}
					if (property.label !== null) {
						attributeJsonObject.addProperty("label", property.label)
					}
					if (property.description !== null) {
						attributeJsonObject.addProperty("description", property.description)
					}
					if (property.format !== null) {
						attributeJsonObject.addProperty("format", property.format)
					}
					if (property.deltaTime !== null) {
						attributeJsonObject.addProperty("delta_t", numberUtilsBlankStringRemover(property.deltaTime))
					}
					if (property.deltaValue !== null) {
						attributeJsonObject.addProperty("delta_val", numberUtilsBlankStringRemover(property.deltaValue))
					}

					// Properties end
					// Attribute error checking
					var attrErrorChecking = new JsonObject

					if (property.minValue !== null) {
						attrErrorChecking.addProperty("min_value", numberUtilsBlankStringRemover(property.minValue))
					}
					if (property.maxValue !== null) {
						attrErrorChecking.addProperty("max_value", numberUtilsBlankStringRemover(property.maxValue))
					}
					if (property.minAlarm !== null) {
						attrErrorChecking.addProperty("min_alarm", numberUtilsBlankStringRemover(property.minAlarm))
					}
					if (property.maxAlarm !== null) {
						attrErrorChecking.addProperty("max_alarm", numberUtilsBlankStringRemover(property.maxAlarm))
					}
					if (property.minWarning !== null) {
						attrErrorChecking.addProperty("min_warning", numberUtilsBlankStringRemover(property.minWarning))
					}
					if (property.maxWarning !== null) {
						attrErrorChecking.addProperty("max_warning", numberUtilsBlankStringRemover(property.maxWarning))
					}
					if (attrErrorChecking.entrySet.size > 0) {
						attributeJsonObject.add("attributeErrorChecking", attrErrorChecking)
					}

				}

				// Attriibute details over
				// Simulation details		
				if (attribute.simulationType !== null) {
					var dataSimulationParameters = getSimulationJsonObject(attribute.simulationType)
					if (dataSimulationParameters.entrySet.size > 0) {
						attributeJsonObject.add("dataSimulationParameters", dataSimulationParameters)
					}
				}
				// Simulation Ends
				// Attribute control system
				var attrCntrlSys = new JsonObject
				var eventSettings = new JsonObject

				if (pogoAttr.displayLevel !== null) {
					attrCntrlSys.addProperty("display_level", pogoAttr.displayLevel)
				}
				if (pogoAttr.polledPeriod !== null) {
					attrCntrlSys.addProperty("polled_period", numberUtilsBlankStringRemover(pogoAttr.polledPeriod))
				}


				if (pogoAttr.evArchiveCriteria !== null) {
					var eventArchCriteria = new JsonObject
					eventArchCriteria.addProperty("archive_abs_change", numberUtilsBlankStringRemover(pogoAttr.evArchiveCriteria.absChange))
					eventArchCriteria.addProperty("archive_period", numberUtilsBlankStringRemover(pogoAttr.evArchiveCriteria.period))
					eventArchCriteria.addProperty("archive_rel_change",numberUtilsBlankStringRemover(pogoAttr.evArchiveCriteria.relChange))
					eventSettings.add("eventArchiveCriteria", eventArchCriteria)
					
				}

				if (pogoAttr.eventCriteria !== null) {
					var eventCriteria = new JsonObject
					eventCriteria.addProperty("abs_change", numberUtilsBlankStringRemover(pogoAttr.eventCriteria.absChange))
					eventCriteria.addProperty("period", numberUtilsBlankStringRemover(pogoAttr.eventCriteria.period))
					eventCriteria.addProperty("rel_change", numberUtilsBlankStringRemover(pogoAttr.eventCriteria.relChange))
					eventSettings.add("eventCriteria", eventCriteria)
				}
				
				if(eventSettings.entrySet.size>0){
					attrCntrlSys.add("EventSettings",eventSettings)
				}
				
				
				if (attrCntrlSys.entrySet.size > 0) {
					attributeJsonObject.add("attributeControlSystem", attrCntrlSys)
				}
				
				if(attributeJsonObject.entrySet.size>0){
					basicAttributeData.add("basicAttributeData",attributeJsonObject)
				}

			}

		} catch (Exception e) {
			e.printStackTrace
		}
		
		
		return basicAttributeData
	}
	 
	def Number numberUtilsBlankStringRemover(String str){
		if(str.empty)
		return 0
		else
		return NumberUtils.createNumber(str)
	}

	def getSimulationJsonObject(Simulation simulation) {
		var dataSimulation = new JsonObject
		try {
			dataSimulation.addProperty("quantity_simulation_type", simulation.class.simpleName.replace("Impl",""))
			if (simulation instanceof GaussianSlewLimited) {
				if (!simulation.minBound.equals(null)) {
					dataSimulation.addProperty("min_bound", numberUtilsBlankStringRemover(simulation.minBound.toString))
				}
				if (!simulation.maxBound.equals(null)) {
					dataSimulation.addProperty("max_bound", numberUtilsBlankStringRemover(simulation.maxBound.toString))
				}
				if (!simulation.mean.equals(null)) {
					dataSimulation.addProperty("mean", numberUtilsBlankStringRemover(simulation.mean.toString))
				}
				if (!simulation.slewRate.equals(null)) {
					dataSimulation.addProperty("max_slew_rate", numberUtilsBlankStringRemover(simulation.slewRate.toString))
				}
				if (!simulation.updatePeriod.equals(null)) {
					dataSimulation.addProperty("update_period", numberUtilsBlankStringRemover(simulation.updatePeriod.toString))
				}

			}
			if (simulation instanceof ConstantQuantity) {
				if (!simulation.initialValue.equals(null)) {
					dataSimulation.addProperty("initial_value", numberUtilsBlankStringRemover(simulation.initialValue.toString))
				}
				if (!simulation.quality.equals(null)) {
					dataSimulation.addProperty("quality", numberUtilsBlankStringRemover(simulation.quality.toString))
				}
			}
			if (simulation instanceof DeterministicSignal) {
				if (!simulation.type.equals(null)) {
					dataSimulation.addProperty("type", numberUtilsBlankStringRemover(simulation.type.class.simpleName))
				}
				if (!simulation.amplitude.equals(null)) {
					dataSimulation.addProperty("amplitude", numberUtilsBlankStringRemover(simulation.amplitude.toString))
				}
				if (!simulation.period.equals(null)) {
					dataSimulation.addProperty("period", numberUtilsBlankStringRemover(simulation.period.toString))
				}
				if (!simulation.offset.equals(null)) {
					dataSimulation.addProperty("offset", numberUtilsBlankStringRemover(simulation.offset.toString))
				}

			}
			if (simulation instanceof RuntimeSpecifiedWaveform) {
				if (!simulation.defaultValue.equals(null)) {
					dataSimulation.addProperty("default_values", numberUtilsBlankStringRemover(simulation.defaultValue.toString))
				}
				if (!simulation.timestamp.equals(null)) {
					dataSimulation.addProperty("timestamp", simulation.timestamp.toString)
				}
				if (!simulation.attribute_qualities.equals(null)) {
					dataSimulation.addProperty("attribute_qualities", numberUtilsBlankStringRemover(simulation.attribute_qualities.toString))
				}
			}
		} catch (Exception e) {
			e.printStackTrace
		}
		return dataSimulation
	}

}
