package com.mncml.implementation.pogo.simulator.generator

import com.google.gson.JsonObject
import tangoSimLibSimulator.OverrideClass

class SIMDDOverrideClassCreator {
	def JsonObject createSIMDDClassOverrides(OverrideClass classOverride) {
		var  overrideClassJsonObject = new JsonObject
		overrideClassJsonObject.addProperty("name",classOverride.name)
		overrideClassJsonObject.addProperty("module_directory",classOverride.module_directory)
		overrideClassJsonObject.addProperty("module_name",classOverride.module_name)
		overrideClassJsonObject.addProperty("class_name",classOverride.class_name)
		
		var overrideClass = new JsonObject
		if(overrideClassJsonObject.entrySet.size>0){
			overrideClass.add("override_class",overrideClassJsonObject)
		}
	return overrideClass	
	}
}