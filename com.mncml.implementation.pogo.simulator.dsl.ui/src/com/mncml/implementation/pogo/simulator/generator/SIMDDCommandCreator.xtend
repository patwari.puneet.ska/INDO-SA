package com.mncml.implementation.pogo.simulator.generator

import com.google.gson.JsonObject
import tangoSimLibSimulator.CommandSimulation
import tangoSimLibSimulator.Behaviour
import tangoSimLibSimulator.InputTransform
import tangoSimLibSimulator.SideEffects
import tangoSimLibSimulator.OutputReturn
import com.google.gson.JsonArray

class SIMDDCommandCreator {
	def JsonObject createSIMDDCommands(CommandSimulation command) {
		var commandJsonObject = new JsonObject
		if (command.pogoCommand !== null) {
			var pogoCommand = command.pogoCommand
			commandJsonObject.addProperty("name", pogoCommand.name)
			commandJsonObject.addProperty("description", pogoCommand.description)
			if (command.behaviorType !== null) {
				var behaviour = command.behaviorType
				commandJsonObject.add("actions",createBehaviourJsonObject(behaviour))
			}
			
			if(pogoCommand.argin!==null){
				var inputParams = new JsonObject
				inputParams.addProperty("dtype_in",pogoCommand.argin.type.class.simpleName.replace("TypeImpl",""))
				inputParams.addProperty("doc_in",pogoCommand.argin.description)
				inputParams.addProperty("dformat_in","Scalar")
				
				commandJsonObject.add("input_parameters",inputParams)
			}
			if(pogoCommand.argout!==null){
				var outPutParams = new JsonObject
				outPutParams.addProperty("dtype_out",pogoCommand.argout.type.class.simpleName.replace("TypeImpl",""))
				outPutParams.addProperty("doc_out",pogoCommand.argout.description)
				outPutParams.addProperty("dformat_out","Scalar")
				
				commandJsonObject.add("output_parameters",outPutParams)
			}
		}

		var basicCommandData = new JsonObject
		if(commandJsonObject.entrySet.size>0)
		{
		basicCommandData.add("basicCommandData",commandJsonObject)
		}
		return basicCommandData
	}

	def createBehaviourJsonObject(Behaviour behaviour) {
		var actionsJsonArray = new JsonArray
		var behaviorJsonObject = new JsonObject

		if (behaviour instanceof InputTransform) {
			behaviorJsonObject.addProperty("behaviour", "input_transform")
			behaviorJsonObject.addProperty("destination_variable", behaviour.destinationVariableName)
		}
		if (behaviour instanceof SideEffects) {
			behaviorJsonObject.addProperty("behaviour", "side_effects")
			behaviorJsonObject.addProperty("source_variable", behaviour.source_variable)
			behaviorJsonObject.addProperty("destination_quanntity", behaviour.destination_quantity)
		}
		if (behaviour instanceof OutputReturn) {
			behaviorJsonObject.addProperty("behaviour", "output_return")
			behaviorJsonObject.addProperty("source_variable", behaviour.source_variable)
			behaviorJsonObject.addProperty("source_quantity", behaviour.source_quantity)
		}
		if(behaviorJsonObject.entrySet.size>0)
		{
		actionsJsonArray.add(behaviorJsonObject)
		}
		return actionsJsonArray
	}

}
