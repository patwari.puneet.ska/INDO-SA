package com.mncml.implementation.pogo.simulator.generator

import com.google.gson.JsonObject
import pogoDsl.PogoDeviceClass
import com.google.gson.JsonArray
import tangoSimLibSimulator.TangoSimLib

class SIMDDJsonSpecsCreator {
	def JsonObject createSIMDDSpecifications(TangoSimLib simSpecs) {
		var simddjsonObject = new JsonObject
		try {

			var deviceClass = simSpecs.refferedPogoDeviceClass as PogoDeviceClass
			var deviceClassName = deviceClass.name
	 		simddjsonObject.addProperty("class_name", deviceClassName)
			if (simSpecs.dataSimulations !== null) {
				var attributes = simSpecs.dataSimulations
				var dynamicAttributes = new JsonArray
				for (attribute : attributes) {
					var dynamAttr = new SIMDDAttributeCreator().createSIMDDDynamicAttributes(attribute)
					if(dynamAttr.entrySet.size>0)
					{ 
						dynamicAttributes.add(dynamAttr)
					}
					
				}
				simddjsonObject.add("dynamicAttributes", dynamicAttributes)
			}
			if (simSpecs.commandSimulations !== null) {
				var commands = simSpecs.commandSimulations
				var commandArray = new JsonArray
				for (command : commands) {
					commandArray.add(new SIMDDCommandCreator().createSIMDDCommands(command))
				}
				simddjsonObject.add("commands", commandArray)
			}
			if (deviceClass.deviceProperties !== null) {
				var deviceProoperties = deviceClass.deviceProperties
				var propertiesArray = new JsonArray
				for (property : deviceProoperties) {
					propertiesArray.add(new SIMDDDevicePropertiesCreator().createSIMDDDeviceProperties(property))
				}
				simddjsonObject.add("deviceProperties", propertiesArray)
			}

			if (simSpecs.overrideClass !== null) {
				var classOverrides = simSpecs.overrideClass
				var classOverArray = new JsonArray
				for (classOverride : classOverrides) {
					classOverArray.add(new SIMDDOverrideClassCreator().createSIMDDClassOverrides(classOverride))
				}
				simddjsonObject.add("class_overrides", classOverArray)
			}
		} catch (Exception e) {
			e.printStackTrace
		}
		return simddjsonObject
	}
}
