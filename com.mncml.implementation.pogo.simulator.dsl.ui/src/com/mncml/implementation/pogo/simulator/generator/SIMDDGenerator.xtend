package com.mncml.implementation.pogo.simulator.generator

import org.eclipse.core.commands.AbstractHandler
import org.eclipse.core.commands.ExecutionEvent
import org.eclipse.core.commands.ExecutionException
import org.eclipse.core.resources.IFile
import org.eclipse.core.resources.IResource
import org.eclipse.core.runtime.CoreException
import org.eclipse.core.runtime.NullProgressMonitor
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.jdt.core.JavaCore
import org.eclipse.jface.dialogs.MessageDialog
import org.eclipse.ui.PlatformUI
import org.eclipse.ui.handlers.HandlerUtil
import org.eclipse.ui.ide.IDE
import org.eclipse.xtext.builder.EclipseResourceFileSystemAccess
import org.eclipse.xtext.resource.XtextResource
import org.eclipse.xtext.ui.editor.XtextEditor
import org.eclipse.xtext.util.concurrent.IUnitOfWork
import org.sweetlemonade.eclipse.json.editor.JsonEditor
import tangoSimLibSimulator.TangoSimLib

class SIMDDGenerator extends AbstractHandler {

	override execute(ExecutionEvent event) throws ExecutionException {
		val generate = MessageDialog.openConfirm(HandlerUtil.getActiveShell(event), "Generate SIMDD File",
			"Do you want to generate SIMDD specifications file ?");
		if (!generate)
			return null;
		val activeEditor = HandlerUtil.getActiveEditor(event);
		val file = activeEditor.getEditorInput().getAdapter(IFile) as IFile;
		if (file !== null) {
			val project = file.getProject();
			val srcGenFolder = project.getFolder("src-gen");
			var javaProject = JavaCore.create(project)
			var srcRoot = javaProject.getPackageFragmentRoot(srcGenFolder);
			if (!srcGenFolder.exists()) {
				try {
					srcGenFolder.create(true, true, new NullProgressMonitor());
					
				} catch (CoreException e) {
 					e.printStackTrace
				}
			}
val tangoSrcFolder = srcRoot.createPackageFragment("com.mnc.simulation.tango.simlib", true, null)
			val fsa = new EclipseResourceFileSystemAccess

			fsa.root = (project.getWorkspace().getRoot());
//		fsa.project = project
			fsa.setOutputPath(tangoSrcFolder.path.toString());
			var teste = fsa.getOutputConfigurations();
					fsa.root = project.workspace.root
				var it = teste.entrySet().iterator();

				//make a new Outputconfiguration <- needed
				while (it.hasNext()) {

					var next = it.next();
					var out = next.getValue();
					out.isOverrideExistingResources();
					out.setCreateOutputDirectory(true); // <--- do not touch this
				}

			if (activeEditor instanceof XtextEditor) {
				(activeEditor as XtextEditor).getDocument().readOnly(new IUnitOfWork<Boolean, XtextResource>() {

					override exec(XtextResource state) throws Exception {
						val res = state as Resource;
						project.refreshLocal(IResource.DEPTH_INFINITE, null);
						if (res !== null && res.contents !== null && res.contents.length !== 0) {

							val simSpecs = res.contents.head as TangoSimLib

							if (simSpecs !== null && simSpecs instanceof TangoSimLib) {
 
								var pogoDeviceClass = simSpecs.refferedPogoDeviceClass
								try {
									var generatedSIMDDText = ""
									try {
										generatedSIMDDText = new SIMDDJsonSpecsCreator().
											createSIMDDSpecifications(simSpecs).toString
									} catch (Exception e) {
										e.printStackTrace
		 				 			}
									fsa.generateFile(pogoDeviceClass.name + "_SIMDD" + ".json", generatedSIMDDText)
									var consideredFolder = project.getFolder("src-gen/com/mnc/simulation/tango/simlib/")
										var jsonGeneratedFile = consideredFolder.getFile(simSpecs.refferedPogoDeviceClass.name + "_SIMDD" + ".json");
										
									var wb = PlatformUI.getWorkbench();
									var win = wb.getActiveWorkbenchWindow();
		  						 	var page = win.getActivePage();
									var part = IDE.openEditor(page, jsonGeneratedFile,true) as JsonEditor;
			   		 				part.doSave(new NullProgressMonitor)
								} catch (Exception e) {
									e.printStackTrace
								}
								return Boolean.TRUE;
							}

						}

					}

				})

			}
			return null;

		}

	}

}
