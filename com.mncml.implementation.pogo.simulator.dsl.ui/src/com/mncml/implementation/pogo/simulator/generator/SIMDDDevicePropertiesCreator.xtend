package com.mncml.implementation.pogo.simulator.generator

import com.google.gson.JsonObject
import pogoDsl.Property
import org.eclipse.emf.common.util.EList
import org.apache.commons.lang3.StringUtils


class SIMDDDevicePropertiesCreator {
	def JsonObject createSIMDDDeviceProperties(Property deviceProopertie) {
		var devicePropertyJsonObject = new JsonObject
		devicePropertyJsonObject.addProperty("name",deviceProopertie.name)
		devicePropertyJsonObject.addProperty("type",deviceProopertie.type.class.simpleName.replace("TypeImpl",""))
		if(deviceProopertie.defaultPropValue!==null){
		devicePropertyJsonObject.addProperty("DefaultPropValue",deviceProopertie.defaultPropValue.getDefaultPropValueSeperatedByCommas)
		}
		devicePropertyJsonObject.addProperty("description",deviceProopertie.description)
		
		var proprtyData = new JsonObject
		if(devicePropertyJsonObject.entrySet.size>0){
		proprtyData.add("propertyData",devicePropertyJsonObject)	
		}
		
		return proprtyData
	}
	
	def String getDefaultPropValueSeperatedByCommas(EList<String> list)
	{
		return StringUtils.join(list, ',');
	}
}