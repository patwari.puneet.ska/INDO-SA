package com.mncml.implementation.pogo.dsl.parser.antlr.internal;

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import com.mncml.implementation.pogo.dsl.services.PogoDslGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalPogoDslParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_STRING", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'PogoSystem'", "'{'", "'imports'", "','", "'}'", "'classes'", "'multiClasses'", "'Import'", "'importURI'", "'isAbstract'", "'PogoDeviceClass'", "'for'", "'controlNode'", "'pogoRevision'", "'institute'", "'baseClass'", "'description'", "'classProperties'", "'deviceProperties'", "'commands'", "'dynamicCommands'", "'attributes'", "'dynamicAttributes'", "'forwardedAttributes'", "'pipes'", "'states'", "'preferences'", "'additionalFiles'", "'overlodedPollPeriodObject'", "'.'", "'PogoMultiClasses'", "'sourcePath'", "'title'", "'license'", "'filestogenerate'", "'ClassDescription'", "'language'", "'hasMandatoryProperty'", "'hasConcreteProperty'", "'hasAbstractCommand'", "'hasAbstractAttribute'", "'descriptionHtmlExists'", "'inheritances'", "'identification'", "'comments'", "'Property'", "'mandatory'", "'DefaultPropValue'", "'type'", "'status'", "'Command'", "'execMethod'", "'displayLevel'", "'polledPeriod'", "'isDynamic'", "'excludedStates'", "'argin'", "'argout'", "'Attribute'", "'attType'", "'rwType'", "'maxX'", "'maxY'", "'associatedAttr'", "'memorized'", "'memorizedAtInit'", "'allocReadMember'", "'enumLabels'", "'readExcludedStates'", "'writeExcludedStates'", "'dataType'", "'changeEvent'", "'archiveEvent'", "'dataReadyEvent'", "'properties'", "'eventCriteria'", "'evArchiveCriteria'", "'ForwardedAttribute'", "'label'", "'Pipe'", "'State'", "'Preferences'", "'docHome'", "'makefileHome'", "'installHome'", "'htmlVersion'", "'AdditionalFile'", "'path'", "'OverlodedPollPeriodObject'", "'pollPeriod'", "'Inheritance'", "'classname'", "'ClassIdentification'", "'contact'", "'author'", "'emailDomain'", "'classFamily'", "'siteSpecific'", "'platform'", "'bus'", "'manufacturer'", "'reference'", "'keyWords'", "'Comments'", "'commandsTable'", "'PropType'", "'InheritanceStatus'", "'abstract'", "'inherited'", "'concrete'", "'concreteHere'", "'hasChanged'", "'SimpleType'", "'VectorType'", "'BooleanType'", "'ShortType'", "'UShortType'", "'IntType'", "'UIntType'", "'FloatType'", "'DoubleType'", "'StringType'", "'ShortVectorType'", "'IntVectorType'", "'FloatVectorType'", "'DoubleVectorType'", "'StringVectorType'", "'Argument'", "'Type'", "'VoidType'", "'CharArrayType'", "'ShortArrayType'", "'UShortArrayType'", "'IntArrayType'", "'UIntArrayType'", "'FloatArrayType'", "'DoubleArrayType'", "'StringArrayType'", "'LongStringArrayType'", "'DoubleStringArrayType'", "'StateType'", "'ConstStringType'", "'BooleanArrayType'", "'UCharType'", "'LongType'", "'ULongType'", "'LongArrayType'", "'ULongArrayType'", "'DevIntType'", "'EncodedType'", "'EnumType'", "'FireEvents'", "'fire'", "'libCheckCriteria'", "'AttrProperties'", "'unit'", "'standardUnit'", "'displayUnit'", "'format'", "'maxValue'", "'minValue'", "'maxAlarm'", "'minAlarm'", "'maxWarning'", "'minWarning'", "'deltaTime'", "'deltaValue'", "'Cpp'", "'Java'", "'Python'", "'OPERATOR'", "'EXPERT'", "'Scalar'", "'Spectrum'", "'Image'", "'READ'", "'WRITE'", "'READ_WRITE'", "'READ_WITH_WRITE'", "'true'", "'false'", "'EventCriteria'", "'relChange'", "'absChange'", "'period'", "'OneClassSimpleDef'", "'hasDynamic'", "'pogo6'", "'parentClasses'"
    };
    public static final int T__144=144;
    public static final int T__143=143;
    public static final int T__146=146;
    public static final int T__50=50;
    public static final int T__145=145;
    public static final int T__140=140;
    public static final int T__142=142;
    public static final int T__141=141;
    public static final int T__59=59;
    public static final int T__55=55;
    public static final int T__56=56;
    public static final int T__57=57;
    public static final int T__58=58;
    public static final int T__51=51;
    public static final int T__137=137;
    public static final int T__52=52;
    public static final int T__136=136;
    public static final int T__53=53;
    public static final int T__139=139;
    public static final int T__54=54;
    public static final int T__138=138;
    public static final int T__133=133;
    public static final int T__132=132;
    public static final int T__60=60;
    public static final int T__135=135;
    public static final int T__61=61;
    public static final int T__134=134;
    public static final int RULE_ID=4;
    public static final int T__131=131;
    public static final int T__130=130;
    public static final int RULE_INT=6;
    public static final int T__66=66;
    public static final int T__67=67;
    public static final int T__129=129;
    public static final int T__68=68;
    public static final int T__69=69;
    public static final int T__62=62;
    public static final int T__126=126;
    public static final int T__63=63;
    public static final int T__125=125;
    public static final int T__64=64;
    public static final int T__128=128;
    public static final int T__65=65;
    public static final int T__127=127;
    public static final int T__166=166;
    public static final int T__165=165;
    public static final int T__168=168;
    public static final int T__167=167;
    public static final int T__162=162;
    public static final int T__161=161;
    public static final int T__164=164;
    public static final int T__163=163;
    public static final int T__160=160;
    public static final int T__37=37;
    public static final int T__38=38;
    public static final int T__39=39;
    public static final int T__33=33;
    public static final int T__34=34;
    public static final int T__35=35;
    public static final int T__36=36;
    public static final int T__159=159;
    public static final int T__30=30;
    public static final int T__158=158;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int T__155=155;
    public static final int T__154=154;
    public static final int T__157=157;
    public static final int T__156=156;
    public static final int T__151=151;
    public static final int T__150=150;
    public static final int T__153=153;
    public static final int T__152=152;
    public static final int T__48=48;
    public static final int T__49=49;
    public static final int T__44=44;
    public static final int T__45=45;
    public static final int T__46=46;
    public static final int T__47=47;
    public static final int T__40=40;
    public static final int T__148=148;
    public static final int T__41=41;
    public static final int T__147=147;
    public static final int T__42=42;
    public static final int T__43=43;
    public static final int T__149=149;
    public static final int T__100=100;
    public static final int T__102=102;
    public static final int T__101=101;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;
    public static final int T__122=122;
    public static final int T__121=121;
    public static final int T__124=124;
    public static final int T__123=123;
    public static final int T__120=120;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__119=119;
    public static final int T__118=118;
    public static final int T__115=115;
    public static final int EOF=-1;
    public static final int T__114=114;
    public static final int T__117=117;
    public static final int T__116=116;
    public static final int T__111=111;
    public static final int T__110=110;
    public static final int T__113=113;
    public static final int T__112=112;
    public static final int T__108=108;
    public static final int T__107=107;
    public static final int T__109=109;
    public static final int T__104=104;
    public static final int T__103=103;
    public static final int T__106=106;
    public static final int T__105=105;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__91=91;
    public static final int T__188=188;
    public static final int T__92=92;
    public static final int T__187=187;
    public static final int T__93=93;
    public static final int T__94=94;
    public static final int T__189=189;
    public static final int T__184=184;
    public static final int T__183=183;
    public static final int T__186=186;
    public static final int T__90=90;
    public static final int T__185=185;
    public static final int T__180=180;
    public static final int T__182=182;
    public static final int T__181=181;
    public static final int T__99=99;
    public static final int T__95=95;
    public static final int T__96=96;
    public static final int T__97=97;
    public static final int T__98=98;
    public static final int T__177=177;
    public static final int T__176=176;
    public static final int T__179=179;
    public static final int T__178=178;
    public static final int T__173=173;
    public static final int T__172=172;
    public static final int T__175=175;
    public static final int T__174=174;
    public static final int T__171=171;
    public static final int T__170=170;
    public static final int T__169=169;
    public static final int T__70=70;
    public static final int T__71=71;
    public static final int T__72=72;
    public static final int RULE_STRING=5;
    public static final int T__77=77;
    public static final int T__78=78;
    public static final int T__79=79;
    public static final int T__73=73;
    public static final int T__74=74;
    public static final int T__75=75;
    public static final int T__76=76;
    public static final int T__80=80;
    public static final int T__199=199;
    public static final int T__81=81;
    public static final int T__198=198;
    public static final int T__82=82;
    public static final int T__83=83;
    public static final int T__195=195;
    public static final int T__194=194;
    public static final int RULE_WS=9;
    public static final int T__197=197;
    public static final int T__196=196;
    public static final int T__191=191;
    public static final int T__190=190;
    public static final int T__193=193;
    public static final int T__192=192;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__88=88;
    public static final int T__89=89;
    public static final int T__84=84;
    public static final int T__85=85;
    public static final int T__86=86;
    public static final int T__87=87;

    // delegates
    // delegators


        public InternalPogoDslParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalPogoDslParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalPogoDslParser.tokenNames; }
    public String getGrammarFileName() { return "InternalPogoDsl.g"; }



     	private PogoDslGrammarAccess grammarAccess;

        public InternalPogoDslParser(TokenStream input, PogoDslGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }

        @Override
        protected String getFirstRuleName() {
        	return "PogoSystem";
       	}

       	@Override
       	protected PogoDslGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}




    // $ANTLR start "entryRulePogoSystem"
    // InternalPogoDsl.g:64:1: entryRulePogoSystem returns [EObject current=null] : iv_rulePogoSystem= rulePogoSystem EOF ;
    public final EObject entryRulePogoSystem() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePogoSystem = null;


        try {
            // InternalPogoDsl.g:64:51: (iv_rulePogoSystem= rulePogoSystem EOF )
            // InternalPogoDsl.g:65:2: iv_rulePogoSystem= rulePogoSystem EOF
            {
             newCompositeNode(grammarAccess.getPogoSystemRule()); 
            pushFollow(FOLLOW_1);
            iv_rulePogoSystem=rulePogoSystem();

            state._fsp--;

             current =iv_rulePogoSystem; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePogoSystem"


    // $ANTLR start "rulePogoSystem"
    // InternalPogoDsl.g:71:1: rulePogoSystem returns [EObject current=null] : ( () otherlv_1= 'PogoSystem' otherlv_2= '{' (otherlv_3= 'imports' otherlv_4= '{' ( (lv_imports_5_0= ruleImport ) ) (otherlv_6= ',' ( (lv_imports_7_0= ruleImport ) ) )* otherlv_8= '}' )? (otherlv_9= 'classes' otherlv_10= '{' ( (lv_classes_11_0= rulePogoDeviceClass ) ) (otherlv_12= ',' ( (lv_classes_13_0= rulePogoDeviceClass ) ) )* otherlv_14= '}' )? (otherlv_15= 'multiClasses' otherlv_16= '{' ( (lv_multiClasses_17_0= rulePogoMultiClasses ) ) (otherlv_18= ',' ( (lv_multiClasses_19_0= rulePogoMultiClasses ) ) )* otherlv_20= '}' )? otherlv_21= '}' ) ;
    public final EObject rulePogoSystem() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        Token otherlv_9=null;
        Token otherlv_10=null;
        Token otherlv_12=null;
        Token otherlv_14=null;
        Token otherlv_15=null;
        Token otherlv_16=null;
        Token otherlv_18=null;
        Token otherlv_20=null;
        Token otherlv_21=null;
        EObject lv_imports_5_0 = null;

        EObject lv_imports_7_0 = null;

        EObject lv_classes_11_0 = null;

        EObject lv_classes_13_0 = null;

        EObject lv_multiClasses_17_0 = null;

        EObject lv_multiClasses_19_0 = null;



        	enterRule();

        try {
            // InternalPogoDsl.g:77:2: ( ( () otherlv_1= 'PogoSystem' otherlv_2= '{' (otherlv_3= 'imports' otherlv_4= '{' ( (lv_imports_5_0= ruleImport ) ) (otherlv_6= ',' ( (lv_imports_7_0= ruleImport ) ) )* otherlv_8= '}' )? (otherlv_9= 'classes' otherlv_10= '{' ( (lv_classes_11_0= rulePogoDeviceClass ) ) (otherlv_12= ',' ( (lv_classes_13_0= rulePogoDeviceClass ) ) )* otherlv_14= '}' )? (otherlv_15= 'multiClasses' otherlv_16= '{' ( (lv_multiClasses_17_0= rulePogoMultiClasses ) ) (otherlv_18= ',' ( (lv_multiClasses_19_0= rulePogoMultiClasses ) ) )* otherlv_20= '}' )? otherlv_21= '}' ) )
            // InternalPogoDsl.g:78:2: ( () otherlv_1= 'PogoSystem' otherlv_2= '{' (otherlv_3= 'imports' otherlv_4= '{' ( (lv_imports_5_0= ruleImport ) ) (otherlv_6= ',' ( (lv_imports_7_0= ruleImport ) ) )* otherlv_8= '}' )? (otherlv_9= 'classes' otherlv_10= '{' ( (lv_classes_11_0= rulePogoDeviceClass ) ) (otherlv_12= ',' ( (lv_classes_13_0= rulePogoDeviceClass ) ) )* otherlv_14= '}' )? (otherlv_15= 'multiClasses' otherlv_16= '{' ( (lv_multiClasses_17_0= rulePogoMultiClasses ) ) (otherlv_18= ',' ( (lv_multiClasses_19_0= rulePogoMultiClasses ) ) )* otherlv_20= '}' )? otherlv_21= '}' )
            {
            // InternalPogoDsl.g:78:2: ( () otherlv_1= 'PogoSystem' otherlv_2= '{' (otherlv_3= 'imports' otherlv_4= '{' ( (lv_imports_5_0= ruleImport ) ) (otherlv_6= ',' ( (lv_imports_7_0= ruleImport ) ) )* otherlv_8= '}' )? (otherlv_9= 'classes' otherlv_10= '{' ( (lv_classes_11_0= rulePogoDeviceClass ) ) (otherlv_12= ',' ( (lv_classes_13_0= rulePogoDeviceClass ) ) )* otherlv_14= '}' )? (otherlv_15= 'multiClasses' otherlv_16= '{' ( (lv_multiClasses_17_0= rulePogoMultiClasses ) ) (otherlv_18= ',' ( (lv_multiClasses_19_0= rulePogoMultiClasses ) ) )* otherlv_20= '}' )? otherlv_21= '}' )
            // InternalPogoDsl.g:79:3: () otherlv_1= 'PogoSystem' otherlv_2= '{' (otherlv_3= 'imports' otherlv_4= '{' ( (lv_imports_5_0= ruleImport ) ) (otherlv_6= ',' ( (lv_imports_7_0= ruleImport ) ) )* otherlv_8= '}' )? (otherlv_9= 'classes' otherlv_10= '{' ( (lv_classes_11_0= rulePogoDeviceClass ) ) (otherlv_12= ',' ( (lv_classes_13_0= rulePogoDeviceClass ) ) )* otherlv_14= '}' )? (otherlv_15= 'multiClasses' otherlv_16= '{' ( (lv_multiClasses_17_0= rulePogoMultiClasses ) ) (otherlv_18= ',' ( (lv_multiClasses_19_0= rulePogoMultiClasses ) ) )* otherlv_20= '}' )? otherlv_21= '}'
            {
            // InternalPogoDsl.g:79:3: ()
            // InternalPogoDsl.g:80:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getPogoSystemAccess().getPogoSystemAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,11,FOLLOW_3); 

            			newLeafNode(otherlv_1, grammarAccess.getPogoSystemAccess().getPogoSystemKeyword_1());
            		
            otherlv_2=(Token)match(input,12,FOLLOW_4); 

            			newLeafNode(otherlv_2, grammarAccess.getPogoSystemAccess().getLeftCurlyBracketKeyword_2());
            		
            // InternalPogoDsl.g:94:3: (otherlv_3= 'imports' otherlv_4= '{' ( (lv_imports_5_0= ruleImport ) ) (otherlv_6= ',' ( (lv_imports_7_0= ruleImport ) ) )* otherlv_8= '}' )?
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0==13) ) {
                alt2=1;
            }
            switch (alt2) {
                case 1 :
                    // InternalPogoDsl.g:95:4: otherlv_3= 'imports' otherlv_4= '{' ( (lv_imports_5_0= ruleImport ) ) (otherlv_6= ',' ( (lv_imports_7_0= ruleImport ) ) )* otherlv_8= '}'
                    {
                    otherlv_3=(Token)match(input,13,FOLLOW_3); 

                    				newLeafNode(otherlv_3, grammarAccess.getPogoSystemAccess().getImportsKeyword_3_0());
                    			
                    otherlv_4=(Token)match(input,12,FOLLOW_5); 

                    				newLeafNode(otherlv_4, grammarAccess.getPogoSystemAccess().getLeftCurlyBracketKeyword_3_1());
                    			
                    // InternalPogoDsl.g:103:4: ( (lv_imports_5_0= ruleImport ) )
                    // InternalPogoDsl.g:104:5: (lv_imports_5_0= ruleImport )
                    {
                    // InternalPogoDsl.g:104:5: (lv_imports_5_0= ruleImport )
                    // InternalPogoDsl.g:105:6: lv_imports_5_0= ruleImport
                    {

                    						newCompositeNode(grammarAccess.getPogoSystemAccess().getImportsImportParserRuleCall_3_2_0());
                    					
                    pushFollow(FOLLOW_6);
                    lv_imports_5_0=ruleImport();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getPogoSystemRule());
                    						}
                    						add(
                    							current,
                    							"imports",
                    							lv_imports_5_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.Import");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalPogoDsl.g:122:4: (otherlv_6= ',' ( (lv_imports_7_0= ruleImport ) ) )*
                    loop1:
                    do {
                        int alt1=2;
                        int LA1_0 = input.LA(1);

                        if ( (LA1_0==14) ) {
                            alt1=1;
                        }


                        switch (alt1) {
                    	case 1 :
                    	    // InternalPogoDsl.g:123:5: otherlv_6= ',' ( (lv_imports_7_0= ruleImport ) )
                    	    {
                    	    otherlv_6=(Token)match(input,14,FOLLOW_5); 

                    	    					newLeafNode(otherlv_6, grammarAccess.getPogoSystemAccess().getCommaKeyword_3_3_0());
                    	    				
                    	    // InternalPogoDsl.g:127:5: ( (lv_imports_7_0= ruleImport ) )
                    	    // InternalPogoDsl.g:128:6: (lv_imports_7_0= ruleImport )
                    	    {
                    	    // InternalPogoDsl.g:128:6: (lv_imports_7_0= ruleImport )
                    	    // InternalPogoDsl.g:129:7: lv_imports_7_0= ruleImport
                    	    {

                    	    							newCompositeNode(grammarAccess.getPogoSystemAccess().getImportsImportParserRuleCall_3_3_1_0());
                    	    						
                    	    pushFollow(FOLLOW_6);
                    	    lv_imports_7_0=ruleImport();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getPogoSystemRule());
                    	    							}
                    	    							add(
                    	    								current,
                    	    								"imports",
                    	    								lv_imports_7_0,
                    	    								"com.mncml.implementation.pogo.dsl.PogoDsl.Import");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop1;
                        }
                    } while (true);

                    otherlv_8=(Token)match(input,15,FOLLOW_7); 

                    				newLeafNode(otherlv_8, grammarAccess.getPogoSystemAccess().getRightCurlyBracketKeyword_3_4());
                    			

                    }
                    break;

            }

            // InternalPogoDsl.g:152:3: (otherlv_9= 'classes' otherlv_10= '{' ( (lv_classes_11_0= rulePogoDeviceClass ) ) (otherlv_12= ',' ( (lv_classes_13_0= rulePogoDeviceClass ) ) )* otherlv_14= '}' )?
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==16) ) {
                alt4=1;
            }
            switch (alt4) {
                case 1 :
                    // InternalPogoDsl.g:153:4: otherlv_9= 'classes' otherlv_10= '{' ( (lv_classes_11_0= rulePogoDeviceClass ) ) (otherlv_12= ',' ( (lv_classes_13_0= rulePogoDeviceClass ) ) )* otherlv_14= '}'
                    {
                    otherlv_9=(Token)match(input,16,FOLLOW_3); 

                    				newLeafNode(otherlv_9, grammarAccess.getPogoSystemAccess().getClassesKeyword_4_0());
                    			
                    otherlv_10=(Token)match(input,12,FOLLOW_8); 

                    				newLeafNode(otherlv_10, grammarAccess.getPogoSystemAccess().getLeftCurlyBracketKeyword_4_1());
                    			
                    // InternalPogoDsl.g:161:4: ( (lv_classes_11_0= rulePogoDeviceClass ) )
                    // InternalPogoDsl.g:162:5: (lv_classes_11_0= rulePogoDeviceClass )
                    {
                    // InternalPogoDsl.g:162:5: (lv_classes_11_0= rulePogoDeviceClass )
                    // InternalPogoDsl.g:163:6: lv_classes_11_0= rulePogoDeviceClass
                    {

                    						newCompositeNode(grammarAccess.getPogoSystemAccess().getClassesPogoDeviceClassParserRuleCall_4_2_0());
                    					
                    pushFollow(FOLLOW_6);
                    lv_classes_11_0=rulePogoDeviceClass();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getPogoSystemRule());
                    						}
                    						add(
                    							current,
                    							"classes",
                    							lv_classes_11_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.PogoDeviceClass");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalPogoDsl.g:180:4: (otherlv_12= ',' ( (lv_classes_13_0= rulePogoDeviceClass ) ) )*
                    loop3:
                    do {
                        int alt3=2;
                        int LA3_0 = input.LA(1);

                        if ( (LA3_0==14) ) {
                            alt3=1;
                        }


                        switch (alt3) {
                    	case 1 :
                    	    // InternalPogoDsl.g:181:5: otherlv_12= ',' ( (lv_classes_13_0= rulePogoDeviceClass ) )
                    	    {
                    	    otherlv_12=(Token)match(input,14,FOLLOW_8); 

                    	    					newLeafNode(otherlv_12, grammarAccess.getPogoSystemAccess().getCommaKeyword_4_3_0());
                    	    				
                    	    // InternalPogoDsl.g:185:5: ( (lv_classes_13_0= rulePogoDeviceClass ) )
                    	    // InternalPogoDsl.g:186:6: (lv_classes_13_0= rulePogoDeviceClass )
                    	    {
                    	    // InternalPogoDsl.g:186:6: (lv_classes_13_0= rulePogoDeviceClass )
                    	    // InternalPogoDsl.g:187:7: lv_classes_13_0= rulePogoDeviceClass
                    	    {

                    	    							newCompositeNode(grammarAccess.getPogoSystemAccess().getClassesPogoDeviceClassParserRuleCall_4_3_1_0());
                    	    						
                    	    pushFollow(FOLLOW_6);
                    	    lv_classes_13_0=rulePogoDeviceClass();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getPogoSystemRule());
                    	    							}
                    	    							add(
                    	    								current,
                    	    								"classes",
                    	    								lv_classes_13_0,
                    	    								"com.mncml.implementation.pogo.dsl.PogoDsl.PogoDeviceClass");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop3;
                        }
                    } while (true);

                    otherlv_14=(Token)match(input,15,FOLLOW_9); 

                    				newLeafNode(otherlv_14, grammarAccess.getPogoSystemAccess().getRightCurlyBracketKeyword_4_4());
                    			

                    }
                    break;

            }

            // InternalPogoDsl.g:210:3: (otherlv_15= 'multiClasses' otherlv_16= '{' ( (lv_multiClasses_17_0= rulePogoMultiClasses ) ) (otherlv_18= ',' ( (lv_multiClasses_19_0= rulePogoMultiClasses ) ) )* otherlv_20= '}' )?
            int alt6=2;
            int LA6_0 = input.LA(1);

            if ( (LA6_0==17) ) {
                alt6=1;
            }
            switch (alt6) {
                case 1 :
                    // InternalPogoDsl.g:211:4: otherlv_15= 'multiClasses' otherlv_16= '{' ( (lv_multiClasses_17_0= rulePogoMultiClasses ) ) (otherlv_18= ',' ( (lv_multiClasses_19_0= rulePogoMultiClasses ) ) )* otherlv_20= '}'
                    {
                    otherlv_15=(Token)match(input,17,FOLLOW_3); 

                    				newLeafNode(otherlv_15, grammarAccess.getPogoSystemAccess().getMultiClassesKeyword_5_0());
                    			
                    otherlv_16=(Token)match(input,12,FOLLOW_10); 

                    				newLeafNode(otherlv_16, grammarAccess.getPogoSystemAccess().getLeftCurlyBracketKeyword_5_1());
                    			
                    // InternalPogoDsl.g:219:4: ( (lv_multiClasses_17_0= rulePogoMultiClasses ) )
                    // InternalPogoDsl.g:220:5: (lv_multiClasses_17_0= rulePogoMultiClasses )
                    {
                    // InternalPogoDsl.g:220:5: (lv_multiClasses_17_0= rulePogoMultiClasses )
                    // InternalPogoDsl.g:221:6: lv_multiClasses_17_0= rulePogoMultiClasses
                    {

                    						newCompositeNode(grammarAccess.getPogoSystemAccess().getMultiClassesPogoMultiClassesParserRuleCall_5_2_0());
                    					
                    pushFollow(FOLLOW_6);
                    lv_multiClasses_17_0=rulePogoMultiClasses();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getPogoSystemRule());
                    						}
                    						add(
                    							current,
                    							"multiClasses",
                    							lv_multiClasses_17_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.PogoMultiClasses");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalPogoDsl.g:238:4: (otherlv_18= ',' ( (lv_multiClasses_19_0= rulePogoMultiClasses ) ) )*
                    loop5:
                    do {
                        int alt5=2;
                        int LA5_0 = input.LA(1);

                        if ( (LA5_0==14) ) {
                            alt5=1;
                        }


                        switch (alt5) {
                    	case 1 :
                    	    // InternalPogoDsl.g:239:5: otherlv_18= ',' ( (lv_multiClasses_19_0= rulePogoMultiClasses ) )
                    	    {
                    	    otherlv_18=(Token)match(input,14,FOLLOW_10); 

                    	    					newLeafNode(otherlv_18, grammarAccess.getPogoSystemAccess().getCommaKeyword_5_3_0());
                    	    				
                    	    // InternalPogoDsl.g:243:5: ( (lv_multiClasses_19_0= rulePogoMultiClasses ) )
                    	    // InternalPogoDsl.g:244:6: (lv_multiClasses_19_0= rulePogoMultiClasses )
                    	    {
                    	    // InternalPogoDsl.g:244:6: (lv_multiClasses_19_0= rulePogoMultiClasses )
                    	    // InternalPogoDsl.g:245:7: lv_multiClasses_19_0= rulePogoMultiClasses
                    	    {

                    	    							newCompositeNode(grammarAccess.getPogoSystemAccess().getMultiClassesPogoMultiClassesParserRuleCall_5_3_1_0());
                    	    						
                    	    pushFollow(FOLLOW_6);
                    	    lv_multiClasses_19_0=rulePogoMultiClasses();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getPogoSystemRule());
                    	    							}
                    	    							add(
                    	    								current,
                    	    								"multiClasses",
                    	    								lv_multiClasses_19_0,
                    	    								"com.mncml.implementation.pogo.dsl.PogoDsl.PogoMultiClasses");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop5;
                        }
                    } while (true);

                    otherlv_20=(Token)match(input,15,FOLLOW_11); 

                    				newLeafNode(otherlv_20, grammarAccess.getPogoSystemAccess().getRightCurlyBracketKeyword_5_4());
                    			

                    }
                    break;

            }

            otherlv_21=(Token)match(input,15,FOLLOW_2); 

            			newLeafNode(otherlv_21, grammarAccess.getPogoSystemAccess().getRightCurlyBracketKeyword_6());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePogoSystem"


    // $ANTLR start "entryRulePropType"
    // InternalPogoDsl.g:276:1: entryRulePropType returns [EObject current=null] : iv_rulePropType= rulePropType EOF ;
    public final EObject entryRulePropType() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePropType = null;


        try {
            // InternalPogoDsl.g:276:49: (iv_rulePropType= rulePropType EOF )
            // InternalPogoDsl.g:277:2: iv_rulePropType= rulePropType EOF
            {
             newCompositeNode(grammarAccess.getPropTypeRule()); 
            pushFollow(FOLLOW_1);
            iv_rulePropType=rulePropType();

            state._fsp--;

             current =iv_rulePropType; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePropType"


    // $ANTLR start "rulePropType"
    // InternalPogoDsl.g:283:1: rulePropType returns [EObject current=null] : (this_PropType_Impl_0= rulePropType_Impl | this_SimpleType_Impl_1= ruleSimpleType_Impl | this_VectorType_Impl_2= ruleVectorType_Impl | this_BooleanType_3= ruleBooleanType | this_ShortType_4= ruleShortType | this_UShortType_5= ruleUShortType | this_IntType_6= ruleIntType | this_UIntType_7= ruleUIntType | this_FloatType_8= ruleFloatType | this_DoubleType_9= ruleDoubleType | this_StringType_10= ruleStringType | this_ShortVectorType_11= ruleShortVectorType | this_IntVectorType_12= ruleIntVectorType | this_FloatVectorType_13= ruleFloatVectorType | this_DoubleVectorType_14= ruleDoubleVectorType | this_StringVectorType_15= ruleStringVectorType ) ;
    public final EObject rulePropType() throws RecognitionException {
        EObject current = null;

        EObject this_PropType_Impl_0 = null;

        EObject this_SimpleType_Impl_1 = null;

        EObject this_VectorType_Impl_2 = null;

        EObject this_BooleanType_3 = null;

        EObject this_ShortType_4 = null;

        EObject this_UShortType_5 = null;

        EObject this_IntType_6 = null;

        EObject this_UIntType_7 = null;

        EObject this_FloatType_8 = null;

        EObject this_DoubleType_9 = null;

        EObject this_StringType_10 = null;

        EObject this_ShortVectorType_11 = null;

        EObject this_IntVectorType_12 = null;

        EObject this_FloatVectorType_13 = null;

        EObject this_DoubleVectorType_14 = null;

        EObject this_StringVectorType_15 = null;



        	enterRule();

        try {
            // InternalPogoDsl.g:289:2: ( (this_PropType_Impl_0= rulePropType_Impl | this_SimpleType_Impl_1= ruleSimpleType_Impl | this_VectorType_Impl_2= ruleVectorType_Impl | this_BooleanType_3= ruleBooleanType | this_ShortType_4= ruleShortType | this_UShortType_5= ruleUShortType | this_IntType_6= ruleIntType | this_UIntType_7= ruleUIntType | this_FloatType_8= ruleFloatType | this_DoubleType_9= ruleDoubleType | this_StringType_10= ruleStringType | this_ShortVectorType_11= ruleShortVectorType | this_IntVectorType_12= ruleIntVectorType | this_FloatVectorType_13= ruleFloatVectorType | this_DoubleVectorType_14= ruleDoubleVectorType | this_StringVectorType_15= ruleStringVectorType ) )
            // InternalPogoDsl.g:290:2: (this_PropType_Impl_0= rulePropType_Impl | this_SimpleType_Impl_1= ruleSimpleType_Impl | this_VectorType_Impl_2= ruleVectorType_Impl | this_BooleanType_3= ruleBooleanType | this_ShortType_4= ruleShortType | this_UShortType_5= ruleUShortType | this_IntType_6= ruleIntType | this_UIntType_7= ruleUIntType | this_FloatType_8= ruleFloatType | this_DoubleType_9= ruleDoubleType | this_StringType_10= ruleStringType | this_ShortVectorType_11= ruleShortVectorType | this_IntVectorType_12= ruleIntVectorType | this_FloatVectorType_13= ruleFloatVectorType | this_DoubleVectorType_14= ruleDoubleVectorType | this_StringVectorType_15= ruleStringVectorType )
            {
            // InternalPogoDsl.g:290:2: (this_PropType_Impl_0= rulePropType_Impl | this_SimpleType_Impl_1= ruleSimpleType_Impl | this_VectorType_Impl_2= ruleVectorType_Impl | this_BooleanType_3= ruleBooleanType | this_ShortType_4= ruleShortType | this_UShortType_5= ruleUShortType | this_IntType_6= ruleIntType | this_UIntType_7= ruleUIntType | this_FloatType_8= ruleFloatType | this_DoubleType_9= ruleDoubleType | this_StringType_10= ruleStringType | this_ShortVectorType_11= ruleShortVectorType | this_IntVectorType_12= ruleIntVectorType | this_FloatVectorType_13= ruleFloatVectorType | this_DoubleVectorType_14= ruleDoubleVectorType | this_StringVectorType_15= ruleStringVectorType )
            int alt7=16;
            switch ( input.LA(1) ) {
            case 116:
                {
                alt7=1;
                }
                break;
            case 123:
                {
                alt7=2;
                }
                break;
            case 124:
                {
                alt7=3;
                }
                break;
            case 125:
                {
                alt7=4;
                }
                break;
            case 126:
                {
                alt7=5;
                }
                break;
            case 127:
                {
                alt7=6;
                }
                break;
            case 128:
                {
                alt7=7;
                }
                break;
            case 129:
                {
                alt7=8;
                }
                break;
            case 130:
                {
                alt7=9;
                }
                break;
            case 131:
                {
                alt7=10;
                }
                break;
            case 132:
                {
                alt7=11;
                }
                break;
            case 133:
                {
                alt7=12;
                }
                break;
            case 134:
                {
                alt7=13;
                }
                break;
            case 135:
                {
                alt7=14;
                }
                break;
            case 136:
                {
                alt7=15;
                }
                break;
            case 137:
                {
                alt7=16;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 7, 0, input);

                throw nvae;
            }

            switch (alt7) {
                case 1 :
                    // InternalPogoDsl.g:291:3: this_PropType_Impl_0= rulePropType_Impl
                    {

                    			newCompositeNode(grammarAccess.getPropTypeAccess().getPropType_ImplParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_PropType_Impl_0=rulePropType_Impl();

                    state._fsp--;


                    			current = this_PropType_Impl_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalPogoDsl.g:300:3: this_SimpleType_Impl_1= ruleSimpleType_Impl
                    {

                    			newCompositeNode(grammarAccess.getPropTypeAccess().getSimpleType_ImplParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_SimpleType_Impl_1=ruleSimpleType_Impl();

                    state._fsp--;


                    			current = this_SimpleType_Impl_1;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 3 :
                    // InternalPogoDsl.g:309:3: this_VectorType_Impl_2= ruleVectorType_Impl
                    {

                    			newCompositeNode(grammarAccess.getPropTypeAccess().getVectorType_ImplParserRuleCall_2());
                    		
                    pushFollow(FOLLOW_2);
                    this_VectorType_Impl_2=ruleVectorType_Impl();

                    state._fsp--;


                    			current = this_VectorType_Impl_2;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 4 :
                    // InternalPogoDsl.g:318:3: this_BooleanType_3= ruleBooleanType
                    {

                    			newCompositeNode(grammarAccess.getPropTypeAccess().getBooleanTypeParserRuleCall_3());
                    		
                    pushFollow(FOLLOW_2);
                    this_BooleanType_3=ruleBooleanType();

                    state._fsp--;


                    			current = this_BooleanType_3;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 5 :
                    // InternalPogoDsl.g:327:3: this_ShortType_4= ruleShortType
                    {

                    			newCompositeNode(grammarAccess.getPropTypeAccess().getShortTypeParserRuleCall_4());
                    		
                    pushFollow(FOLLOW_2);
                    this_ShortType_4=ruleShortType();

                    state._fsp--;


                    			current = this_ShortType_4;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 6 :
                    // InternalPogoDsl.g:336:3: this_UShortType_5= ruleUShortType
                    {

                    			newCompositeNode(grammarAccess.getPropTypeAccess().getUShortTypeParserRuleCall_5());
                    		
                    pushFollow(FOLLOW_2);
                    this_UShortType_5=ruleUShortType();

                    state._fsp--;


                    			current = this_UShortType_5;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 7 :
                    // InternalPogoDsl.g:345:3: this_IntType_6= ruleIntType
                    {

                    			newCompositeNode(grammarAccess.getPropTypeAccess().getIntTypeParserRuleCall_6());
                    		
                    pushFollow(FOLLOW_2);
                    this_IntType_6=ruleIntType();

                    state._fsp--;


                    			current = this_IntType_6;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 8 :
                    // InternalPogoDsl.g:354:3: this_UIntType_7= ruleUIntType
                    {

                    			newCompositeNode(grammarAccess.getPropTypeAccess().getUIntTypeParserRuleCall_7());
                    		
                    pushFollow(FOLLOW_2);
                    this_UIntType_7=ruleUIntType();

                    state._fsp--;


                    			current = this_UIntType_7;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 9 :
                    // InternalPogoDsl.g:363:3: this_FloatType_8= ruleFloatType
                    {

                    			newCompositeNode(grammarAccess.getPropTypeAccess().getFloatTypeParserRuleCall_8());
                    		
                    pushFollow(FOLLOW_2);
                    this_FloatType_8=ruleFloatType();

                    state._fsp--;


                    			current = this_FloatType_8;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 10 :
                    // InternalPogoDsl.g:372:3: this_DoubleType_9= ruleDoubleType
                    {

                    			newCompositeNode(grammarAccess.getPropTypeAccess().getDoubleTypeParserRuleCall_9());
                    		
                    pushFollow(FOLLOW_2);
                    this_DoubleType_9=ruleDoubleType();

                    state._fsp--;


                    			current = this_DoubleType_9;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 11 :
                    // InternalPogoDsl.g:381:3: this_StringType_10= ruleStringType
                    {

                    			newCompositeNode(grammarAccess.getPropTypeAccess().getStringTypeParserRuleCall_10());
                    		
                    pushFollow(FOLLOW_2);
                    this_StringType_10=ruleStringType();

                    state._fsp--;


                    			current = this_StringType_10;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 12 :
                    // InternalPogoDsl.g:390:3: this_ShortVectorType_11= ruleShortVectorType
                    {

                    			newCompositeNode(grammarAccess.getPropTypeAccess().getShortVectorTypeParserRuleCall_11());
                    		
                    pushFollow(FOLLOW_2);
                    this_ShortVectorType_11=ruleShortVectorType();

                    state._fsp--;


                    			current = this_ShortVectorType_11;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 13 :
                    // InternalPogoDsl.g:399:3: this_IntVectorType_12= ruleIntVectorType
                    {

                    			newCompositeNode(grammarAccess.getPropTypeAccess().getIntVectorTypeParserRuleCall_12());
                    		
                    pushFollow(FOLLOW_2);
                    this_IntVectorType_12=ruleIntVectorType();

                    state._fsp--;


                    			current = this_IntVectorType_12;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 14 :
                    // InternalPogoDsl.g:408:3: this_FloatVectorType_13= ruleFloatVectorType
                    {

                    			newCompositeNode(grammarAccess.getPropTypeAccess().getFloatVectorTypeParserRuleCall_13());
                    		
                    pushFollow(FOLLOW_2);
                    this_FloatVectorType_13=ruleFloatVectorType();

                    state._fsp--;


                    			current = this_FloatVectorType_13;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 15 :
                    // InternalPogoDsl.g:417:3: this_DoubleVectorType_14= ruleDoubleVectorType
                    {

                    			newCompositeNode(grammarAccess.getPropTypeAccess().getDoubleVectorTypeParserRuleCall_14());
                    		
                    pushFollow(FOLLOW_2);
                    this_DoubleVectorType_14=ruleDoubleVectorType();

                    state._fsp--;


                    			current = this_DoubleVectorType_14;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 16 :
                    // InternalPogoDsl.g:426:3: this_StringVectorType_15= ruleStringVectorType
                    {

                    			newCompositeNode(grammarAccess.getPropTypeAccess().getStringVectorTypeParserRuleCall_15());
                    		
                    pushFollow(FOLLOW_2);
                    this_StringVectorType_15=ruleStringVectorType();

                    state._fsp--;


                    			current = this_StringVectorType_15;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePropType"


    // $ANTLR start "entryRuleType"
    // InternalPogoDsl.g:438:1: entryRuleType returns [EObject current=null] : iv_ruleType= ruleType EOF ;
    public final EObject entryRuleType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleType = null;


        try {
            // InternalPogoDsl.g:438:45: (iv_ruleType= ruleType EOF )
            // InternalPogoDsl.g:439:2: iv_ruleType= ruleType EOF
            {
             newCompositeNode(grammarAccess.getTypeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleType=ruleType();

            state._fsp--;

             current =iv_ruleType; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleType"


    // $ANTLR start "ruleType"
    // InternalPogoDsl.g:445:1: ruleType returns [EObject current=null] : (this_Type_Impl_0= ruleType_Impl | this_VoidType_1= ruleVoidType | this_BooleanType_2= ruleBooleanType | this_ShortType_3= ruleShortType | this_UShortType_4= ruleUShortType | this_IntType_5= ruleIntType | this_UIntType_6= ruleUIntType | this_FloatType_7= ruleFloatType | this_DoubleType_8= ruleDoubleType | this_StringType_9= ruleStringType | this_CharArrayType_10= ruleCharArrayType | this_ShortArrayType_11= ruleShortArrayType | this_UShortArrayType_12= ruleUShortArrayType | this_IntArrayType_13= ruleIntArrayType | this_UIntArrayType_14= ruleUIntArrayType | this_FloatArrayType_15= ruleFloatArrayType | this_DoubleArrayType_16= ruleDoubleArrayType | this_StringArrayType_17= ruleStringArrayType | this_LongStringArrayType_18= ruleLongStringArrayType | this_DoubleStringArrayType_19= ruleDoubleStringArrayType | this_StateType_20= ruleStateType | this_ConstStringType_21= ruleConstStringType | this_BooleanArrayType_22= ruleBooleanArrayType | this_UCharType_23= ruleUCharType | this_LongType_24= ruleLongType | this_ULongType_25= ruleULongType | this_LongArrayType_26= ruleLongArrayType | this_ULongArrayType_27= ruleULongArrayType | this_DevIntType_28= ruleDevIntType | this_EncodedType_29= ruleEncodedType | this_EnumType_30= ruleEnumType ) ;
    public final EObject ruleType() throws RecognitionException {
        EObject current = null;

        EObject this_Type_Impl_0 = null;

        EObject this_VoidType_1 = null;

        EObject this_BooleanType_2 = null;

        EObject this_ShortType_3 = null;

        EObject this_UShortType_4 = null;

        EObject this_IntType_5 = null;

        EObject this_UIntType_6 = null;

        EObject this_FloatType_7 = null;

        EObject this_DoubleType_8 = null;

        EObject this_StringType_9 = null;

        EObject this_CharArrayType_10 = null;

        EObject this_ShortArrayType_11 = null;

        EObject this_UShortArrayType_12 = null;

        EObject this_IntArrayType_13 = null;

        EObject this_UIntArrayType_14 = null;

        EObject this_FloatArrayType_15 = null;

        EObject this_DoubleArrayType_16 = null;

        EObject this_StringArrayType_17 = null;

        EObject this_LongStringArrayType_18 = null;

        EObject this_DoubleStringArrayType_19 = null;

        EObject this_StateType_20 = null;

        EObject this_ConstStringType_21 = null;

        EObject this_BooleanArrayType_22 = null;

        EObject this_UCharType_23 = null;

        EObject this_LongType_24 = null;

        EObject this_ULongType_25 = null;

        EObject this_LongArrayType_26 = null;

        EObject this_ULongArrayType_27 = null;

        EObject this_DevIntType_28 = null;

        EObject this_EncodedType_29 = null;

        EObject this_EnumType_30 = null;



        	enterRule();

        try {
            // InternalPogoDsl.g:451:2: ( (this_Type_Impl_0= ruleType_Impl | this_VoidType_1= ruleVoidType | this_BooleanType_2= ruleBooleanType | this_ShortType_3= ruleShortType | this_UShortType_4= ruleUShortType | this_IntType_5= ruleIntType | this_UIntType_6= ruleUIntType | this_FloatType_7= ruleFloatType | this_DoubleType_8= ruleDoubleType | this_StringType_9= ruleStringType | this_CharArrayType_10= ruleCharArrayType | this_ShortArrayType_11= ruleShortArrayType | this_UShortArrayType_12= ruleUShortArrayType | this_IntArrayType_13= ruleIntArrayType | this_UIntArrayType_14= ruleUIntArrayType | this_FloatArrayType_15= ruleFloatArrayType | this_DoubleArrayType_16= ruleDoubleArrayType | this_StringArrayType_17= ruleStringArrayType | this_LongStringArrayType_18= ruleLongStringArrayType | this_DoubleStringArrayType_19= ruleDoubleStringArrayType | this_StateType_20= ruleStateType | this_ConstStringType_21= ruleConstStringType | this_BooleanArrayType_22= ruleBooleanArrayType | this_UCharType_23= ruleUCharType | this_LongType_24= ruleLongType | this_ULongType_25= ruleULongType | this_LongArrayType_26= ruleLongArrayType | this_ULongArrayType_27= ruleULongArrayType | this_DevIntType_28= ruleDevIntType | this_EncodedType_29= ruleEncodedType | this_EnumType_30= ruleEnumType ) )
            // InternalPogoDsl.g:452:2: (this_Type_Impl_0= ruleType_Impl | this_VoidType_1= ruleVoidType | this_BooleanType_2= ruleBooleanType | this_ShortType_3= ruleShortType | this_UShortType_4= ruleUShortType | this_IntType_5= ruleIntType | this_UIntType_6= ruleUIntType | this_FloatType_7= ruleFloatType | this_DoubleType_8= ruleDoubleType | this_StringType_9= ruleStringType | this_CharArrayType_10= ruleCharArrayType | this_ShortArrayType_11= ruleShortArrayType | this_UShortArrayType_12= ruleUShortArrayType | this_IntArrayType_13= ruleIntArrayType | this_UIntArrayType_14= ruleUIntArrayType | this_FloatArrayType_15= ruleFloatArrayType | this_DoubleArrayType_16= ruleDoubleArrayType | this_StringArrayType_17= ruleStringArrayType | this_LongStringArrayType_18= ruleLongStringArrayType | this_DoubleStringArrayType_19= ruleDoubleStringArrayType | this_StateType_20= ruleStateType | this_ConstStringType_21= ruleConstStringType | this_BooleanArrayType_22= ruleBooleanArrayType | this_UCharType_23= ruleUCharType | this_LongType_24= ruleLongType | this_ULongType_25= ruleULongType | this_LongArrayType_26= ruleLongArrayType | this_ULongArrayType_27= ruleULongArrayType | this_DevIntType_28= ruleDevIntType | this_EncodedType_29= ruleEncodedType | this_EnumType_30= ruleEnumType )
            {
            // InternalPogoDsl.g:452:2: (this_Type_Impl_0= ruleType_Impl | this_VoidType_1= ruleVoidType | this_BooleanType_2= ruleBooleanType | this_ShortType_3= ruleShortType | this_UShortType_4= ruleUShortType | this_IntType_5= ruleIntType | this_UIntType_6= ruleUIntType | this_FloatType_7= ruleFloatType | this_DoubleType_8= ruleDoubleType | this_StringType_9= ruleStringType | this_CharArrayType_10= ruleCharArrayType | this_ShortArrayType_11= ruleShortArrayType | this_UShortArrayType_12= ruleUShortArrayType | this_IntArrayType_13= ruleIntArrayType | this_UIntArrayType_14= ruleUIntArrayType | this_FloatArrayType_15= ruleFloatArrayType | this_DoubleArrayType_16= ruleDoubleArrayType | this_StringArrayType_17= ruleStringArrayType | this_LongStringArrayType_18= ruleLongStringArrayType | this_DoubleStringArrayType_19= ruleDoubleStringArrayType | this_StateType_20= ruleStateType | this_ConstStringType_21= ruleConstStringType | this_BooleanArrayType_22= ruleBooleanArrayType | this_UCharType_23= ruleUCharType | this_LongType_24= ruleLongType | this_ULongType_25= ruleULongType | this_LongArrayType_26= ruleLongArrayType | this_ULongArrayType_27= ruleULongArrayType | this_DevIntType_28= ruleDevIntType | this_EncodedType_29= ruleEncodedType | this_EnumType_30= ruleEnumType )
            int alt8=31;
            switch ( input.LA(1) ) {
            case 139:
                {
                alt8=1;
                }
                break;
            case 140:
                {
                alt8=2;
                }
                break;
            case 125:
                {
                alt8=3;
                }
                break;
            case 126:
                {
                alt8=4;
                }
                break;
            case 127:
                {
                alt8=5;
                }
                break;
            case 128:
                {
                alt8=6;
                }
                break;
            case 129:
                {
                alt8=7;
                }
                break;
            case 130:
                {
                alt8=8;
                }
                break;
            case 131:
                {
                alt8=9;
                }
                break;
            case 132:
                {
                alt8=10;
                }
                break;
            case 141:
                {
                alt8=11;
                }
                break;
            case 142:
                {
                alt8=12;
                }
                break;
            case 143:
                {
                alt8=13;
                }
                break;
            case 144:
                {
                alt8=14;
                }
                break;
            case 145:
                {
                alt8=15;
                }
                break;
            case 146:
                {
                alt8=16;
                }
                break;
            case 147:
                {
                alt8=17;
                }
                break;
            case 148:
                {
                alt8=18;
                }
                break;
            case 149:
                {
                alt8=19;
                }
                break;
            case 150:
                {
                alt8=20;
                }
                break;
            case 151:
                {
                alt8=21;
                }
                break;
            case 152:
                {
                alt8=22;
                }
                break;
            case 153:
                {
                alt8=23;
                }
                break;
            case 154:
                {
                alt8=24;
                }
                break;
            case 155:
                {
                alt8=25;
                }
                break;
            case 156:
                {
                alt8=26;
                }
                break;
            case 157:
                {
                alt8=27;
                }
                break;
            case 158:
                {
                alt8=28;
                }
                break;
            case 159:
                {
                alt8=29;
                }
                break;
            case 160:
                {
                alt8=30;
                }
                break;
            case 161:
                {
                alt8=31;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 8, 0, input);

                throw nvae;
            }

            switch (alt8) {
                case 1 :
                    // InternalPogoDsl.g:453:3: this_Type_Impl_0= ruleType_Impl
                    {

                    			newCompositeNode(grammarAccess.getTypeAccess().getType_ImplParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_Type_Impl_0=ruleType_Impl();

                    state._fsp--;


                    			current = this_Type_Impl_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalPogoDsl.g:462:3: this_VoidType_1= ruleVoidType
                    {

                    			newCompositeNode(grammarAccess.getTypeAccess().getVoidTypeParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_VoidType_1=ruleVoidType();

                    state._fsp--;


                    			current = this_VoidType_1;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 3 :
                    // InternalPogoDsl.g:471:3: this_BooleanType_2= ruleBooleanType
                    {

                    			newCompositeNode(grammarAccess.getTypeAccess().getBooleanTypeParserRuleCall_2());
                    		
                    pushFollow(FOLLOW_2);
                    this_BooleanType_2=ruleBooleanType();

                    state._fsp--;


                    			current = this_BooleanType_2;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 4 :
                    // InternalPogoDsl.g:480:3: this_ShortType_3= ruleShortType
                    {

                    			newCompositeNode(grammarAccess.getTypeAccess().getShortTypeParserRuleCall_3());
                    		
                    pushFollow(FOLLOW_2);
                    this_ShortType_3=ruleShortType();

                    state._fsp--;


                    			current = this_ShortType_3;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 5 :
                    // InternalPogoDsl.g:489:3: this_UShortType_4= ruleUShortType
                    {

                    			newCompositeNode(grammarAccess.getTypeAccess().getUShortTypeParserRuleCall_4());
                    		
                    pushFollow(FOLLOW_2);
                    this_UShortType_4=ruleUShortType();

                    state._fsp--;


                    			current = this_UShortType_4;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 6 :
                    // InternalPogoDsl.g:498:3: this_IntType_5= ruleIntType
                    {

                    			newCompositeNode(grammarAccess.getTypeAccess().getIntTypeParserRuleCall_5());
                    		
                    pushFollow(FOLLOW_2);
                    this_IntType_5=ruleIntType();

                    state._fsp--;


                    			current = this_IntType_5;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 7 :
                    // InternalPogoDsl.g:507:3: this_UIntType_6= ruleUIntType
                    {

                    			newCompositeNode(grammarAccess.getTypeAccess().getUIntTypeParserRuleCall_6());
                    		
                    pushFollow(FOLLOW_2);
                    this_UIntType_6=ruleUIntType();

                    state._fsp--;


                    			current = this_UIntType_6;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 8 :
                    // InternalPogoDsl.g:516:3: this_FloatType_7= ruleFloatType
                    {

                    			newCompositeNode(grammarAccess.getTypeAccess().getFloatTypeParserRuleCall_7());
                    		
                    pushFollow(FOLLOW_2);
                    this_FloatType_7=ruleFloatType();

                    state._fsp--;


                    			current = this_FloatType_7;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 9 :
                    // InternalPogoDsl.g:525:3: this_DoubleType_8= ruleDoubleType
                    {

                    			newCompositeNode(grammarAccess.getTypeAccess().getDoubleTypeParserRuleCall_8());
                    		
                    pushFollow(FOLLOW_2);
                    this_DoubleType_8=ruleDoubleType();

                    state._fsp--;


                    			current = this_DoubleType_8;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 10 :
                    // InternalPogoDsl.g:534:3: this_StringType_9= ruleStringType
                    {

                    			newCompositeNode(grammarAccess.getTypeAccess().getStringTypeParserRuleCall_9());
                    		
                    pushFollow(FOLLOW_2);
                    this_StringType_9=ruleStringType();

                    state._fsp--;


                    			current = this_StringType_9;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 11 :
                    // InternalPogoDsl.g:543:3: this_CharArrayType_10= ruleCharArrayType
                    {

                    			newCompositeNode(grammarAccess.getTypeAccess().getCharArrayTypeParserRuleCall_10());
                    		
                    pushFollow(FOLLOW_2);
                    this_CharArrayType_10=ruleCharArrayType();

                    state._fsp--;


                    			current = this_CharArrayType_10;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 12 :
                    // InternalPogoDsl.g:552:3: this_ShortArrayType_11= ruleShortArrayType
                    {

                    			newCompositeNode(grammarAccess.getTypeAccess().getShortArrayTypeParserRuleCall_11());
                    		
                    pushFollow(FOLLOW_2);
                    this_ShortArrayType_11=ruleShortArrayType();

                    state._fsp--;


                    			current = this_ShortArrayType_11;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 13 :
                    // InternalPogoDsl.g:561:3: this_UShortArrayType_12= ruleUShortArrayType
                    {

                    			newCompositeNode(grammarAccess.getTypeAccess().getUShortArrayTypeParserRuleCall_12());
                    		
                    pushFollow(FOLLOW_2);
                    this_UShortArrayType_12=ruleUShortArrayType();

                    state._fsp--;


                    			current = this_UShortArrayType_12;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 14 :
                    // InternalPogoDsl.g:570:3: this_IntArrayType_13= ruleIntArrayType
                    {

                    			newCompositeNode(grammarAccess.getTypeAccess().getIntArrayTypeParserRuleCall_13());
                    		
                    pushFollow(FOLLOW_2);
                    this_IntArrayType_13=ruleIntArrayType();

                    state._fsp--;


                    			current = this_IntArrayType_13;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 15 :
                    // InternalPogoDsl.g:579:3: this_UIntArrayType_14= ruleUIntArrayType
                    {

                    			newCompositeNode(grammarAccess.getTypeAccess().getUIntArrayTypeParserRuleCall_14());
                    		
                    pushFollow(FOLLOW_2);
                    this_UIntArrayType_14=ruleUIntArrayType();

                    state._fsp--;


                    			current = this_UIntArrayType_14;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 16 :
                    // InternalPogoDsl.g:588:3: this_FloatArrayType_15= ruleFloatArrayType
                    {

                    			newCompositeNode(grammarAccess.getTypeAccess().getFloatArrayTypeParserRuleCall_15());
                    		
                    pushFollow(FOLLOW_2);
                    this_FloatArrayType_15=ruleFloatArrayType();

                    state._fsp--;


                    			current = this_FloatArrayType_15;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 17 :
                    // InternalPogoDsl.g:597:3: this_DoubleArrayType_16= ruleDoubleArrayType
                    {

                    			newCompositeNode(grammarAccess.getTypeAccess().getDoubleArrayTypeParserRuleCall_16());
                    		
                    pushFollow(FOLLOW_2);
                    this_DoubleArrayType_16=ruleDoubleArrayType();

                    state._fsp--;


                    			current = this_DoubleArrayType_16;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 18 :
                    // InternalPogoDsl.g:606:3: this_StringArrayType_17= ruleStringArrayType
                    {

                    			newCompositeNode(grammarAccess.getTypeAccess().getStringArrayTypeParserRuleCall_17());
                    		
                    pushFollow(FOLLOW_2);
                    this_StringArrayType_17=ruleStringArrayType();

                    state._fsp--;


                    			current = this_StringArrayType_17;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 19 :
                    // InternalPogoDsl.g:615:3: this_LongStringArrayType_18= ruleLongStringArrayType
                    {

                    			newCompositeNode(grammarAccess.getTypeAccess().getLongStringArrayTypeParserRuleCall_18());
                    		
                    pushFollow(FOLLOW_2);
                    this_LongStringArrayType_18=ruleLongStringArrayType();

                    state._fsp--;


                    			current = this_LongStringArrayType_18;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 20 :
                    // InternalPogoDsl.g:624:3: this_DoubleStringArrayType_19= ruleDoubleStringArrayType
                    {

                    			newCompositeNode(grammarAccess.getTypeAccess().getDoubleStringArrayTypeParserRuleCall_19());
                    		
                    pushFollow(FOLLOW_2);
                    this_DoubleStringArrayType_19=ruleDoubleStringArrayType();

                    state._fsp--;


                    			current = this_DoubleStringArrayType_19;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 21 :
                    // InternalPogoDsl.g:633:3: this_StateType_20= ruleStateType
                    {

                    			newCompositeNode(grammarAccess.getTypeAccess().getStateTypeParserRuleCall_20());
                    		
                    pushFollow(FOLLOW_2);
                    this_StateType_20=ruleStateType();

                    state._fsp--;


                    			current = this_StateType_20;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 22 :
                    // InternalPogoDsl.g:642:3: this_ConstStringType_21= ruleConstStringType
                    {

                    			newCompositeNode(grammarAccess.getTypeAccess().getConstStringTypeParserRuleCall_21());
                    		
                    pushFollow(FOLLOW_2);
                    this_ConstStringType_21=ruleConstStringType();

                    state._fsp--;


                    			current = this_ConstStringType_21;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 23 :
                    // InternalPogoDsl.g:651:3: this_BooleanArrayType_22= ruleBooleanArrayType
                    {

                    			newCompositeNode(grammarAccess.getTypeAccess().getBooleanArrayTypeParserRuleCall_22());
                    		
                    pushFollow(FOLLOW_2);
                    this_BooleanArrayType_22=ruleBooleanArrayType();

                    state._fsp--;


                    			current = this_BooleanArrayType_22;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 24 :
                    // InternalPogoDsl.g:660:3: this_UCharType_23= ruleUCharType
                    {

                    			newCompositeNode(grammarAccess.getTypeAccess().getUCharTypeParserRuleCall_23());
                    		
                    pushFollow(FOLLOW_2);
                    this_UCharType_23=ruleUCharType();

                    state._fsp--;


                    			current = this_UCharType_23;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 25 :
                    // InternalPogoDsl.g:669:3: this_LongType_24= ruleLongType
                    {

                    			newCompositeNode(grammarAccess.getTypeAccess().getLongTypeParserRuleCall_24());
                    		
                    pushFollow(FOLLOW_2);
                    this_LongType_24=ruleLongType();

                    state._fsp--;


                    			current = this_LongType_24;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 26 :
                    // InternalPogoDsl.g:678:3: this_ULongType_25= ruleULongType
                    {

                    			newCompositeNode(grammarAccess.getTypeAccess().getULongTypeParserRuleCall_25());
                    		
                    pushFollow(FOLLOW_2);
                    this_ULongType_25=ruleULongType();

                    state._fsp--;


                    			current = this_ULongType_25;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 27 :
                    // InternalPogoDsl.g:687:3: this_LongArrayType_26= ruleLongArrayType
                    {

                    			newCompositeNode(grammarAccess.getTypeAccess().getLongArrayTypeParserRuleCall_26());
                    		
                    pushFollow(FOLLOW_2);
                    this_LongArrayType_26=ruleLongArrayType();

                    state._fsp--;


                    			current = this_LongArrayType_26;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 28 :
                    // InternalPogoDsl.g:696:3: this_ULongArrayType_27= ruleULongArrayType
                    {

                    			newCompositeNode(grammarAccess.getTypeAccess().getULongArrayTypeParserRuleCall_27());
                    		
                    pushFollow(FOLLOW_2);
                    this_ULongArrayType_27=ruleULongArrayType();

                    state._fsp--;


                    			current = this_ULongArrayType_27;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 29 :
                    // InternalPogoDsl.g:705:3: this_DevIntType_28= ruleDevIntType
                    {

                    			newCompositeNode(grammarAccess.getTypeAccess().getDevIntTypeParserRuleCall_28());
                    		
                    pushFollow(FOLLOW_2);
                    this_DevIntType_28=ruleDevIntType();

                    state._fsp--;


                    			current = this_DevIntType_28;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 30 :
                    // InternalPogoDsl.g:714:3: this_EncodedType_29= ruleEncodedType
                    {

                    			newCompositeNode(grammarAccess.getTypeAccess().getEncodedTypeParserRuleCall_29());
                    		
                    pushFollow(FOLLOW_2);
                    this_EncodedType_29=ruleEncodedType();

                    state._fsp--;


                    			current = this_EncodedType_29;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 31 :
                    // InternalPogoDsl.g:723:3: this_EnumType_30= ruleEnumType
                    {

                    			newCompositeNode(grammarAccess.getTypeAccess().getEnumTypeParserRuleCall_30());
                    		
                    pushFollow(FOLLOW_2);
                    this_EnumType_30=ruleEnumType();

                    state._fsp--;


                    			current = this_EnumType_30;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleType"


    // $ANTLR start "entryRuleImport"
    // InternalPogoDsl.g:735:1: entryRuleImport returns [EObject current=null] : iv_ruleImport= ruleImport EOF ;
    public final EObject entryRuleImport() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleImport = null;


        try {
            // InternalPogoDsl.g:735:47: (iv_ruleImport= ruleImport EOF )
            // InternalPogoDsl.g:736:2: iv_ruleImport= ruleImport EOF
            {
             newCompositeNode(grammarAccess.getImportRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleImport=ruleImport();

            state._fsp--;

             current =iv_ruleImport; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleImport"


    // $ANTLR start "ruleImport"
    // InternalPogoDsl.g:742:1: ruleImport returns [EObject current=null] : ( () otherlv_1= 'Import' otherlv_2= '{' (otherlv_3= 'importURI' ( (lv_importURI_4_0= ruleEString ) ) )? otherlv_5= '}' ) ;
    public final EObject ruleImport() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        AntlrDatatypeRuleToken lv_importURI_4_0 = null;



        	enterRule();

        try {
            // InternalPogoDsl.g:748:2: ( ( () otherlv_1= 'Import' otherlv_2= '{' (otherlv_3= 'importURI' ( (lv_importURI_4_0= ruleEString ) ) )? otherlv_5= '}' ) )
            // InternalPogoDsl.g:749:2: ( () otherlv_1= 'Import' otherlv_2= '{' (otherlv_3= 'importURI' ( (lv_importURI_4_0= ruleEString ) ) )? otherlv_5= '}' )
            {
            // InternalPogoDsl.g:749:2: ( () otherlv_1= 'Import' otherlv_2= '{' (otherlv_3= 'importURI' ( (lv_importURI_4_0= ruleEString ) ) )? otherlv_5= '}' )
            // InternalPogoDsl.g:750:3: () otherlv_1= 'Import' otherlv_2= '{' (otherlv_3= 'importURI' ( (lv_importURI_4_0= ruleEString ) ) )? otherlv_5= '}'
            {
            // InternalPogoDsl.g:750:3: ()
            // InternalPogoDsl.g:751:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getImportAccess().getImportAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,18,FOLLOW_3); 

            			newLeafNode(otherlv_1, grammarAccess.getImportAccess().getImportKeyword_1());
            		
            otherlv_2=(Token)match(input,12,FOLLOW_12); 

            			newLeafNode(otherlv_2, grammarAccess.getImportAccess().getLeftCurlyBracketKeyword_2());
            		
            // InternalPogoDsl.g:765:3: (otherlv_3= 'importURI' ( (lv_importURI_4_0= ruleEString ) ) )?
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( (LA9_0==19) ) {
                alt9=1;
            }
            switch (alt9) {
                case 1 :
                    // InternalPogoDsl.g:766:4: otherlv_3= 'importURI' ( (lv_importURI_4_0= ruleEString ) )
                    {
                    otherlv_3=(Token)match(input,19,FOLLOW_13); 

                    				newLeafNode(otherlv_3, grammarAccess.getImportAccess().getImportURIKeyword_3_0());
                    			
                    // InternalPogoDsl.g:770:4: ( (lv_importURI_4_0= ruleEString ) )
                    // InternalPogoDsl.g:771:5: (lv_importURI_4_0= ruleEString )
                    {
                    // InternalPogoDsl.g:771:5: (lv_importURI_4_0= ruleEString )
                    // InternalPogoDsl.g:772:6: lv_importURI_4_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getImportAccess().getImportURIEStringParserRuleCall_3_1_0());
                    					
                    pushFollow(FOLLOW_11);
                    lv_importURI_4_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getImportRule());
                    						}
                    						set(
                    							current,
                    							"importURI",
                    							lv_importURI_4_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            otherlv_5=(Token)match(input,15,FOLLOW_2); 

            			newLeafNode(otherlv_5, grammarAccess.getImportAccess().getRightCurlyBracketKeyword_4());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleImport"


    // $ANTLR start "entryRulePogoDeviceClass"
    // InternalPogoDsl.g:798:1: entryRulePogoDeviceClass returns [EObject current=null] : iv_rulePogoDeviceClass= rulePogoDeviceClass EOF ;
    public final EObject entryRulePogoDeviceClass() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePogoDeviceClass = null;


        try {
            // InternalPogoDsl.g:798:56: (iv_rulePogoDeviceClass= rulePogoDeviceClass EOF )
            // InternalPogoDsl.g:799:2: iv_rulePogoDeviceClass= rulePogoDeviceClass EOF
            {
             newCompositeNode(grammarAccess.getPogoDeviceClassRule()); 
            pushFollow(FOLLOW_1);
            iv_rulePogoDeviceClass=rulePogoDeviceClass();

            state._fsp--;

             current =iv_rulePogoDeviceClass; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePogoDeviceClass"


    // $ANTLR start "rulePogoDeviceClass"
    // InternalPogoDsl.g:805:1: rulePogoDeviceClass returns [EObject current=null] : ( () ( (lv_isAbstract_1_0= 'isAbstract' ) )? otherlv_2= 'PogoDeviceClass' otherlv_3= 'for' ( (lv_name_4_0= ruleEString ) ) otherlv_5= '{' (otherlv_6= 'controlNode' ( ( ruleQualifiedName ) ) )? (otherlv_8= 'pogoRevision' ( (lv_pogoRevision_9_0= ruleEString ) ) )? (otherlv_10= 'institute' ( (lv_institute_11_0= ruleEString ) ) )? (otherlv_12= 'baseClass' ( ( ruleEString ) ) )? (otherlv_14= 'description' ( (lv_description_15_0= ruleClassDescription ) ) )? (otherlv_16= 'classProperties' otherlv_17= '{' ( (lv_classProperties_18_0= ruleProperty ) ) (otherlv_19= ',' ( (lv_classProperties_20_0= ruleProperty ) ) )* otherlv_21= '}' )? (otherlv_22= 'deviceProperties' otherlv_23= '{' ( (lv_deviceProperties_24_0= ruleProperty ) ) (otherlv_25= ',' ( (lv_deviceProperties_26_0= ruleProperty ) ) )* otherlv_27= '}' )? (otherlv_28= 'commands' otherlv_29= '{' ( (lv_commands_30_0= ruleCommand ) ) (otherlv_31= ',' ( (lv_commands_32_0= ruleCommand ) ) )* otherlv_33= '}' )? (otherlv_34= 'dynamicCommands' otherlv_35= '{' ( (lv_dynamicCommands_36_0= ruleCommand ) ) (otherlv_37= ',' ( (lv_dynamicCommands_38_0= ruleCommand ) ) )* otherlv_39= '}' )? (otherlv_40= 'attributes' otherlv_41= '{' ( (lv_attributes_42_0= ruleAttribute ) ) (otherlv_43= ',' ( (lv_attributes_44_0= ruleAttribute ) ) )* otherlv_45= '}' )? (otherlv_46= 'dynamicAttributes' otherlv_47= '{' ( (lv_dynamicAttributes_48_0= ruleAttribute ) ) (otherlv_49= ',' ( (lv_dynamicAttributes_50_0= ruleAttribute ) ) )* otherlv_51= '}' )? (otherlv_52= 'forwardedAttributes' otherlv_53= '{' ( (lv_forwardedAttributes_54_0= ruleForwardedAttribute ) ) (otherlv_55= ',' ( (lv_forwardedAttributes_56_0= ruleForwardedAttribute ) ) )* otherlv_57= '}' )? (otherlv_58= 'pipes' otherlv_59= '{' ( (lv_pipes_60_0= rulePipe ) ) (otherlv_61= ',' ( (lv_pipes_62_0= rulePipe ) ) )* otherlv_63= '}' )? (otherlv_64= 'states' otherlv_65= '{' ( (lv_states_66_0= ruleState ) ) (otherlv_67= ',' ( (lv_states_68_0= ruleState ) ) )* otherlv_69= '}' )? (otherlv_70= 'preferences' ( (lv_preferences_71_0= rulePreferences ) ) )? (otherlv_72= 'additionalFiles' otherlv_73= '{' ( (lv_additionalFiles_74_0= ruleAdditionalFile ) ) (otherlv_75= ',' ( (lv_additionalFiles_76_0= ruleAdditionalFile ) ) )* otherlv_77= '}' )? (otherlv_78= 'overlodedPollPeriodObject' otherlv_79= '{' ( (lv_overlodedPollPeriodObject_80_0= ruleOverlodedPollPeriodObject ) ) (otherlv_81= ',' ( (lv_overlodedPollPeriodObject_82_0= ruleOverlodedPollPeriodObject ) ) )* otherlv_83= '}' )? otherlv_84= '}' ) ;
    public final EObject rulePogoDeviceClass() throws RecognitionException {
        EObject current = null;

        Token lv_isAbstract_1_0=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        Token otherlv_10=null;
        Token otherlv_12=null;
        Token otherlv_14=null;
        Token otherlv_16=null;
        Token otherlv_17=null;
        Token otherlv_19=null;
        Token otherlv_21=null;
        Token otherlv_22=null;
        Token otherlv_23=null;
        Token otherlv_25=null;
        Token otherlv_27=null;
        Token otherlv_28=null;
        Token otherlv_29=null;
        Token otherlv_31=null;
        Token otherlv_33=null;
        Token otherlv_34=null;
        Token otherlv_35=null;
        Token otherlv_37=null;
        Token otherlv_39=null;
        Token otherlv_40=null;
        Token otherlv_41=null;
        Token otherlv_43=null;
        Token otherlv_45=null;
        Token otherlv_46=null;
        Token otherlv_47=null;
        Token otherlv_49=null;
        Token otherlv_51=null;
        Token otherlv_52=null;
        Token otherlv_53=null;
        Token otherlv_55=null;
        Token otherlv_57=null;
        Token otherlv_58=null;
        Token otherlv_59=null;
        Token otherlv_61=null;
        Token otherlv_63=null;
        Token otherlv_64=null;
        Token otherlv_65=null;
        Token otherlv_67=null;
        Token otherlv_69=null;
        Token otherlv_70=null;
        Token otherlv_72=null;
        Token otherlv_73=null;
        Token otherlv_75=null;
        Token otherlv_77=null;
        Token otherlv_78=null;
        Token otherlv_79=null;
        Token otherlv_81=null;
        Token otherlv_83=null;
        Token otherlv_84=null;
        AntlrDatatypeRuleToken lv_name_4_0 = null;

        AntlrDatatypeRuleToken lv_pogoRevision_9_0 = null;

        AntlrDatatypeRuleToken lv_institute_11_0 = null;

        EObject lv_description_15_0 = null;

        EObject lv_classProperties_18_0 = null;

        EObject lv_classProperties_20_0 = null;

        EObject lv_deviceProperties_24_0 = null;

        EObject lv_deviceProperties_26_0 = null;

        EObject lv_commands_30_0 = null;

        EObject lv_commands_32_0 = null;

        EObject lv_dynamicCommands_36_0 = null;

        EObject lv_dynamicCommands_38_0 = null;

        EObject lv_attributes_42_0 = null;

        EObject lv_attributes_44_0 = null;

        EObject lv_dynamicAttributes_48_0 = null;

        EObject lv_dynamicAttributes_50_0 = null;

        EObject lv_forwardedAttributes_54_0 = null;

        EObject lv_forwardedAttributes_56_0 = null;

        EObject lv_pipes_60_0 = null;

        EObject lv_pipes_62_0 = null;

        EObject lv_states_66_0 = null;

        EObject lv_states_68_0 = null;

        EObject lv_preferences_71_0 = null;

        EObject lv_additionalFiles_74_0 = null;

        EObject lv_additionalFiles_76_0 = null;

        EObject lv_overlodedPollPeriodObject_80_0 = null;

        EObject lv_overlodedPollPeriodObject_82_0 = null;



        	enterRule();

        try {
            // InternalPogoDsl.g:811:2: ( ( () ( (lv_isAbstract_1_0= 'isAbstract' ) )? otherlv_2= 'PogoDeviceClass' otherlv_3= 'for' ( (lv_name_4_0= ruleEString ) ) otherlv_5= '{' (otherlv_6= 'controlNode' ( ( ruleQualifiedName ) ) )? (otherlv_8= 'pogoRevision' ( (lv_pogoRevision_9_0= ruleEString ) ) )? (otherlv_10= 'institute' ( (lv_institute_11_0= ruleEString ) ) )? (otherlv_12= 'baseClass' ( ( ruleEString ) ) )? (otherlv_14= 'description' ( (lv_description_15_0= ruleClassDescription ) ) )? (otherlv_16= 'classProperties' otherlv_17= '{' ( (lv_classProperties_18_0= ruleProperty ) ) (otherlv_19= ',' ( (lv_classProperties_20_0= ruleProperty ) ) )* otherlv_21= '}' )? (otherlv_22= 'deviceProperties' otherlv_23= '{' ( (lv_deviceProperties_24_0= ruleProperty ) ) (otherlv_25= ',' ( (lv_deviceProperties_26_0= ruleProperty ) ) )* otherlv_27= '}' )? (otherlv_28= 'commands' otherlv_29= '{' ( (lv_commands_30_0= ruleCommand ) ) (otherlv_31= ',' ( (lv_commands_32_0= ruleCommand ) ) )* otherlv_33= '}' )? (otherlv_34= 'dynamicCommands' otherlv_35= '{' ( (lv_dynamicCommands_36_0= ruleCommand ) ) (otherlv_37= ',' ( (lv_dynamicCommands_38_0= ruleCommand ) ) )* otherlv_39= '}' )? (otherlv_40= 'attributes' otherlv_41= '{' ( (lv_attributes_42_0= ruleAttribute ) ) (otherlv_43= ',' ( (lv_attributes_44_0= ruleAttribute ) ) )* otherlv_45= '}' )? (otherlv_46= 'dynamicAttributes' otherlv_47= '{' ( (lv_dynamicAttributes_48_0= ruleAttribute ) ) (otherlv_49= ',' ( (lv_dynamicAttributes_50_0= ruleAttribute ) ) )* otherlv_51= '}' )? (otherlv_52= 'forwardedAttributes' otherlv_53= '{' ( (lv_forwardedAttributes_54_0= ruleForwardedAttribute ) ) (otherlv_55= ',' ( (lv_forwardedAttributes_56_0= ruleForwardedAttribute ) ) )* otherlv_57= '}' )? (otherlv_58= 'pipes' otherlv_59= '{' ( (lv_pipes_60_0= rulePipe ) ) (otherlv_61= ',' ( (lv_pipes_62_0= rulePipe ) ) )* otherlv_63= '}' )? (otherlv_64= 'states' otherlv_65= '{' ( (lv_states_66_0= ruleState ) ) (otherlv_67= ',' ( (lv_states_68_0= ruleState ) ) )* otherlv_69= '}' )? (otherlv_70= 'preferences' ( (lv_preferences_71_0= rulePreferences ) ) )? (otherlv_72= 'additionalFiles' otherlv_73= '{' ( (lv_additionalFiles_74_0= ruleAdditionalFile ) ) (otherlv_75= ',' ( (lv_additionalFiles_76_0= ruleAdditionalFile ) ) )* otherlv_77= '}' )? (otherlv_78= 'overlodedPollPeriodObject' otherlv_79= '{' ( (lv_overlodedPollPeriodObject_80_0= ruleOverlodedPollPeriodObject ) ) (otherlv_81= ',' ( (lv_overlodedPollPeriodObject_82_0= ruleOverlodedPollPeriodObject ) ) )* otherlv_83= '}' )? otherlv_84= '}' ) )
            // InternalPogoDsl.g:812:2: ( () ( (lv_isAbstract_1_0= 'isAbstract' ) )? otherlv_2= 'PogoDeviceClass' otherlv_3= 'for' ( (lv_name_4_0= ruleEString ) ) otherlv_5= '{' (otherlv_6= 'controlNode' ( ( ruleQualifiedName ) ) )? (otherlv_8= 'pogoRevision' ( (lv_pogoRevision_9_0= ruleEString ) ) )? (otherlv_10= 'institute' ( (lv_institute_11_0= ruleEString ) ) )? (otherlv_12= 'baseClass' ( ( ruleEString ) ) )? (otherlv_14= 'description' ( (lv_description_15_0= ruleClassDescription ) ) )? (otherlv_16= 'classProperties' otherlv_17= '{' ( (lv_classProperties_18_0= ruleProperty ) ) (otherlv_19= ',' ( (lv_classProperties_20_0= ruleProperty ) ) )* otherlv_21= '}' )? (otherlv_22= 'deviceProperties' otherlv_23= '{' ( (lv_deviceProperties_24_0= ruleProperty ) ) (otherlv_25= ',' ( (lv_deviceProperties_26_0= ruleProperty ) ) )* otherlv_27= '}' )? (otherlv_28= 'commands' otherlv_29= '{' ( (lv_commands_30_0= ruleCommand ) ) (otherlv_31= ',' ( (lv_commands_32_0= ruleCommand ) ) )* otherlv_33= '}' )? (otherlv_34= 'dynamicCommands' otherlv_35= '{' ( (lv_dynamicCommands_36_0= ruleCommand ) ) (otherlv_37= ',' ( (lv_dynamicCommands_38_0= ruleCommand ) ) )* otherlv_39= '}' )? (otherlv_40= 'attributes' otherlv_41= '{' ( (lv_attributes_42_0= ruleAttribute ) ) (otherlv_43= ',' ( (lv_attributes_44_0= ruleAttribute ) ) )* otherlv_45= '}' )? (otherlv_46= 'dynamicAttributes' otherlv_47= '{' ( (lv_dynamicAttributes_48_0= ruleAttribute ) ) (otherlv_49= ',' ( (lv_dynamicAttributes_50_0= ruleAttribute ) ) )* otherlv_51= '}' )? (otherlv_52= 'forwardedAttributes' otherlv_53= '{' ( (lv_forwardedAttributes_54_0= ruleForwardedAttribute ) ) (otherlv_55= ',' ( (lv_forwardedAttributes_56_0= ruleForwardedAttribute ) ) )* otherlv_57= '}' )? (otherlv_58= 'pipes' otherlv_59= '{' ( (lv_pipes_60_0= rulePipe ) ) (otherlv_61= ',' ( (lv_pipes_62_0= rulePipe ) ) )* otherlv_63= '}' )? (otherlv_64= 'states' otherlv_65= '{' ( (lv_states_66_0= ruleState ) ) (otherlv_67= ',' ( (lv_states_68_0= ruleState ) ) )* otherlv_69= '}' )? (otherlv_70= 'preferences' ( (lv_preferences_71_0= rulePreferences ) ) )? (otherlv_72= 'additionalFiles' otherlv_73= '{' ( (lv_additionalFiles_74_0= ruleAdditionalFile ) ) (otherlv_75= ',' ( (lv_additionalFiles_76_0= ruleAdditionalFile ) ) )* otherlv_77= '}' )? (otherlv_78= 'overlodedPollPeriodObject' otherlv_79= '{' ( (lv_overlodedPollPeriodObject_80_0= ruleOverlodedPollPeriodObject ) ) (otherlv_81= ',' ( (lv_overlodedPollPeriodObject_82_0= ruleOverlodedPollPeriodObject ) ) )* otherlv_83= '}' )? otherlv_84= '}' )
            {
            // InternalPogoDsl.g:812:2: ( () ( (lv_isAbstract_1_0= 'isAbstract' ) )? otherlv_2= 'PogoDeviceClass' otherlv_3= 'for' ( (lv_name_4_0= ruleEString ) ) otherlv_5= '{' (otherlv_6= 'controlNode' ( ( ruleQualifiedName ) ) )? (otherlv_8= 'pogoRevision' ( (lv_pogoRevision_9_0= ruleEString ) ) )? (otherlv_10= 'institute' ( (lv_institute_11_0= ruleEString ) ) )? (otherlv_12= 'baseClass' ( ( ruleEString ) ) )? (otherlv_14= 'description' ( (lv_description_15_0= ruleClassDescription ) ) )? (otherlv_16= 'classProperties' otherlv_17= '{' ( (lv_classProperties_18_0= ruleProperty ) ) (otherlv_19= ',' ( (lv_classProperties_20_0= ruleProperty ) ) )* otherlv_21= '}' )? (otherlv_22= 'deviceProperties' otherlv_23= '{' ( (lv_deviceProperties_24_0= ruleProperty ) ) (otherlv_25= ',' ( (lv_deviceProperties_26_0= ruleProperty ) ) )* otherlv_27= '}' )? (otherlv_28= 'commands' otherlv_29= '{' ( (lv_commands_30_0= ruleCommand ) ) (otherlv_31= ',' ( (lv_commands_32_0= ruleCommand ) ) )* otherlv_33= '}' )? (otherlv_34= 'dynamicCommands' otherlv_35= '{' ( (lv_dynamicCommands_36_0= ruleCommand ) ) (otherlv_37= ',' ( (lv_dynamicCommands_38_0= ruleCommand ) ) )* otherlv_39= '}' )? (otherlv_40= 'attributes' otherlv_41= '{' ( (lv_attributes_42_0= ruleAttribute ) ) (otherlv_43= ',' ( (lv_attributes_44_0= ruleAttribute ) ) )* otherlv_45= '}' )? (otherlv_46= 'dynamicAttributes' otherlv_47= '{' ( (lv_dynamicAttributes_48_0= ruleAttribute ) ) (otherlv_49= ',' ( (lv_dynamicAttributes_50_0= ruleAttribute ) ) )* otherlv_51= '}' )? (otherlv_52= 'forwardedAttributes' otherlv_53= '{' ( (lv_forwardedAttributes_54_0= ruleForwardedAttribute ) ) (otherlv_55= ',' ( (lv_forwardedAttributes_56_0= ruleForwardedAttribute ) ) )* otherlv_57= '}' )? (otherlv_58= 'pipes' otherlv_59= '{' ( (lv_pipes_60_0= rulePipe ) ) (otherlv_61= ',' ( (lv_pipes_62_0= rulePipe ) ) )* otherlv_63= '}' )? (otherlv_64= 'states' otherlv_65= '{' ( (lv_states_66_0= ruleState ) ) (otherlv_67= ',' ( (lv_states_68_0= ruleState ) ) )* otherlv_69= '}' )? (otherlv_70= 'preferences' ( (lv_preferences_71_0= rulePreferences ) ) )? (otherlv_72= 'additionalFiles' otherlv_73= '{' ( (lv_additionalFiles_74_0= ruleAdditionalFile ) ) (otherlv_75= ',' ( (lv_additionalFiles_76_0= ruleAdditionalFile ) ) )* otherlv_77= '}' )? (otherlv_78= 'overlodedPollPeriodObject' otherlv_79= '{' ( (lv_overlodedPollPeriodObject_80_0= ruleOverlodedPollPeriodObject ) ) (otherlv_81= ',' ( (lv_overlodedPollPeriodObject_82_0= ruleOverlodedPollPeriodObject ) ) )* otherlv_83= '}' )? otherlv_84= '}' )
            // InternalPogoDsl.g:813:3: () ( (lv_isAbstract_1_0= 'isAbstract' ) )? otherlv_2= 'PogoDeviceClass' otherlv_3= 'for' ( (lv_name_4_0= ruleEString ) ) otherlv_5= '{' (otherlv_6= 'controlNode' ( ( ruleQualifiedName ) ) )? (otherlv_8= 'pogoRevision' ( (lv_pogoRevision_9_0= ruleEString ) ) )? (otherlv_10= 'institute' ( (lv_institute_11_0= ruleEString ) ) )? (otherlv_12= 'baseClass' ( ( ruleEString ) ) )? (otherlv_14= 'description' ( (lv_description_15_0= ruleClassDescription ) ) )? (otherlv_16= 'classProperties' otherlv_17= '{' ( (lv_classProperties_18_0= ruleProperty ) ) (otherlv_19= ',' ( (lv_classProperties_20_0= ruleProperty ) ) )* otherlv_21= '}' )? (otherlv_22= 'deviceProperties' otherlv_23= '{' ( (lv_deviceProperties_24_0= ruleProperty ) ) (otherlv_25= ',' ( (lv_deviceProperties_26_0= ruleProperty ) ) )* otherlv_27= '}' )? (otherlv_28= 'commands' otherlv_29= '{' ( (lv_commands_30_0= ruleCommand ) ) (otherlv_31= ',' ( (lv_commands_32_0= ruleCommand ) ) )* otherlv_33= '}' )? (otherlv_34= 'dynamicCommands' otherlv_35= '{' ( (lv_dynamicCommands_36_0= ruleCommand ) ) (otherlv_37= ',' ( (lv_dynamicCommands_38_0= ruleCommand ) ) )* otherlv_39= '}' )? (otherlv_40= 'attributes' otherlv_41= '{' ( (lv_attributes_42_0= ruleAttribute ) ) (otherlv_43= ',' ( (lv_attributes_44_0= ruleAttribute ) ) )* otherlv_45= '}' )? (otherlv_46= 'dynamicAttributes' otherlv_47= '{' ( (lv_dynamicAttributes_48_0= ruleAttribute ) ) (otherlv_49= ',' ( (lv_dynamicAttributes_50_0= ruleAttribute ) ) )* otherlv_51= '}' )? (otherlv_52= 'forwardedAttributes' otherlv_53= '{' ( (lv_forwardedAttributes_54_0= ruleForwardedAttribute ) ) (otherlv_55= ',' ( (lv_forwardedAttributes_56_0= ruleForwardedAttribute ) ) )* otherlv_57= '}' )? (otherlv_58= 'pipes' otherlv_59= '{' ( (lv_pipes_60_0= rulePipe ) ) (otherlv_61= ',' ( (lv_pipes_62_0= rulePipe ) ) )* otherlv_63= '}' )? (otherlv_64= 'states' otherlv_65= '{' ( (lv_states_66_0= ruleState ) ) (otherlv_67= ',' ( (lv_states_68_0= ruleState ) ) )* otherlv_69= '}' )? (otherlv_70= 'preferences' ( (lv_preferences_71_0= rulePreferences ) ) )? (otherlv_72= 'additionalFiles' otherlv_73= '{' ( (lv_additionalFiles_74_0= ruleAdditionalFile ) ) (otherlv_75= ',' ( (lv_additionalFiles_76_0= ruleAdditionalFile ) ) )* otherlv_77= '}' )? (otherlv_78= 'overlodedPollPeriodObject' otherlv_79= '{' ( (lv_overlodedPollPeriodObject_80_0= ruleOverlodedPollPeriodObject ) ) (otherlv_81= ',' ( (lv_overlodedPollPeriodObject_82_0= ruleOverlodedPollPeriodObject ) ) )* otherlv_83= '}' )? otherlv_84= '}'
            {
            // InternalPogoDsl.g:813:3: ()
            // InternalPogoDsl.g:814:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getPogoDeviceClassAccess().getPogoDeviceClassAction_0(),
            					current);
            			

            }

            // InternalPogoDsl.g:820:3: ( (lv_isAbstract_1_0= 'isAbstract' ) )?
            int alt10=2;
            int LA10_0 = input.LA(1);

            if ( (LA10_0==20) ) {
                alt10=1;
            }
            switch (alt10) {
                case 1 :
                    // InternalPogoDsl.g:821:4: (lv_isAbstract_1_0= 'isAbstract' )
                    {
                    // InternalPogoDsl.g:821:4: (lv_isAbstract_1_0= 'isAbstract' )
                    // InternalPogoDsl.g:822:5: lv_isAbstract_1_0= 'isAbstract'
                    {
                    lv_isAbstract_1_0=(Token)match(input,20,FOLLOW_14); 

                    					newLeafNode(lv_isAbstract_1_0, grammarAccess.getPogoDeviceClassAccess().getIsAbstractIsAbstractKeyword_1_0());
                    				

                    					if (current==null) {
                    						current = createModelElement(grammarAccess.getPogoDeviceClassRule());
                    					}
                    					setWithLastConsumed(current, "isAbstract", true, "isAbstract");
                    				

                    }


                    }
                    break;

            }

            otherlv_2=(Token)match(input,21,FOLLOW_15); 

            			newLeafNode(otherlv_2, grammarAccess.getPogoDeviceClassAccess().getPogoDeviceClassKeyword_2());
            		
            otherlv_3=(Token)match(input,22,FOLLOW_13); 

            			newLeafNode(otherlv_3, grammarAccess.getPogoDeviceClassAccess().getForKeyword_3());
            		
            // InternalPogoDsl.g:842:3: ( (lv_name_4_0= ruleEString ) )
            // InternalPogoDsl.g:843:4: (lv_name_4_0= ruleEString )
            {
            // InternalPogoDsl.g:843:4: (lv_name_4_0= ruleEString )
            // InternalPogoDsl.g:844:5: lv_name_4_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getPogoDeviceClassAccess().getNameEStringParserRuleCall_4_0());
            				
            pushFollow(FOLLOW_3);
            lv_name_4_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getPogoDeviceClassRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_4_0,
            						"com.mncml.implementation.pogo.dsl.PogoDsl.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_5=(Token)match(input,12,FOLLOW_16); 

            			newLeafNode(otherlv_5, grammarAccess.getPogoDeviceClassAccess().getLeftCurlyBracketKeyword_5());
            		
            // InternalPogoDsl.g:865:3: (otherlv_6= 'controlNode' ( ( ruleQualifiedName ) ) )?
            int alt11=2;
            int LA11_0 = input.LA(1);

            if ( (LA11_0==23) ) {
                alt11=1;
            }
            switch (alt11) {
                case 1 :
                    // InternalPogoDsl.g:866:4: otherlv_6= 'controlNode' ( ( ruleQualifiedName ) )
                    {
                    otherlv_6=(Token)match(input,23,FOLLOW_17); 

                    				newLeafNode(otherlv_6, grammarAccess.getPogoDeviceClassAccess().getControlNodeKeyword_6_0());
                    			
                    // InternalPogoDsl.g:870:4: ( ( ruleQualifiedName ) )
                    // InternalPogoDsl.g:871:5: ( ruleQualifiedName )
                    {
                    // InternalPogoDsl.g:871:5: ( ruleQualifiedName )
                    // InternalPogoDsl.g:872:6: ruleQualifiedName
                    {

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getPogoDeviceClassRule());
                    						}
                    					

                    						newCompositeNode(grammarAccess.getPogoDeviceClassAccess().getRefferedControlNodeControlNodeCrossReference_6_1_0());
                    					
                    pushFollow(FOLLOW_18);
                    ruleQualifiedName();

                    state._fsp--;


                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalPogoDsl.g:887:3: (otherlv_8= 'pogoRevision' ( (lv_pogoRevision_9_0= ruleEString ) ) )?
            int alt12=2;
            int LA12_0 = input.LA(1);

            if ( (LA12_0==24) ) {
                alt12=1;
            }
            switch (alt12) {
                case 1 :
                    // InternalPogoDsl.g:888:4: otherlv_8= 'pogoRevision' ( (lv_pogoRevision_9_0= ruleEString ) )
                    {
                    otherlv_8=(Token)match(input,24,FOLLOW_13); 

                    				newLeafNode(otherlv_8, grammarAccess.getPogoDeviceClassAccess().getPogoRevisionKeyword_7_0());
                    			
                    // InternalPogoDsl.g:892:4: ( (lv_pogoRevision_9_0= ruleEString ) )
                    // InternalPogoDsl.g:893:5: (lv_pogoRevision_9_0= ruleEString )
                    {
                    // InternalPogoDsl.g:893:5: (lv_pogoRevision_9_0= ruleEString )
                    // InternalPogoDsl.g:894:6: lv_pogoRevision_9_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getPogoDeviceClassAccess().getPogoRevisionEStringParserRuleCall_7_1_0());
                    					
                    pushFollow(FOLLOW_19);
                    lv_pogoRevision_9_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getPogoDeviceClassRule());
                    						}
                    						set(
                    							current,
                    							"pogoRevision",
                    							lv_pogoRevision_9_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalPogoDsl.g:912:3: (otherlv_10= 'institute' ( (lv_institute_11_0= ruleEString ) ) )?
            int alt13=2;
            int LA13_0 = input.LA(1);

            if ( (LA13_0==25) ) {
                alt13=1;
            }
            switch (alt13) {
                case 1 :
                    // InternalPogoDsl.g:913:4: otherlv_10= 'institute' ( (lv_institute_11_0= ruleEString ) )
                    {
                    otherlv_10=(Token)match(input,25,FOLLOW_13); 

                    				newLeafNode(otherlv_10, grammarAccess.getPogoDeviceClassAccess().getInstituteKeyword_8_0());
                    			
                    // InternalPogoDsl.g:917:4: ( (lv_institute_11_0= ruleEString ) )
                    // InternalPogoDsl.g:918:5: (lv_institute_11_0= ruleEString )
                    {
                    // InternalPogoDsl.g:918:5: (lv_institute_11_0= ruleEString )
                    // InternalPogoDsl.g:919:6: lv_institute_11_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getPogoDeviceClassAccess().getInstituteEStringParserRuleCall_8_1_0());
                    					
                    pushFollow(FOLLOW_20);
                    lv_institute_11_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getPogoDeviceClassRule());
                    						}
                    						set(
                    							current,
                    							"institute",
                    							lv_institute_11_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalPogoDsl.g:937:3: (otherlv_12= 'baseClass' ( ( ruleEString ) ) )?
            int alt14=2;
            int LA14_0 = input.LA(1);

            if ( (LA14_0==26) ) {
                alt14=1;
            }
            switch (alt14) {
                case 1 :
                    // InternalPogoDsl.g:938:4: otherlv_12= 'baseClass' ( ( ruleEString ) )
                    {
                    otherlv_12=(Token)match(input,26,FOLLOW_13); 

                    				newLeafNode(otherlv_12, grammarAccess.getPogoDeviceClassAccess().getBaseClassKeyword_9_0());
                    			
                    // InternalPogoDsl.g:942:4: ( ( ruleEString ) )
                    // InternalPogoDsl.g:943:5: ( ruleEString )
                    {
                    // InternalPogoDsl.g:943:5: ( ruleEString )
                    // InternalPogoDsl.g:944:6: ruleEString
                    {

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getPogoDeviceClassRule());
                    						}
                    					

                    						newCompositeNode(grammarAccess.getPogoDeviceClassAccess().getBaseClassPogoDeviceClassCrossReference_9_1_0());
                    					
                    pushFollow(FOLLOW_21);
                    ruleEString();

                    state._fsp--;


                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalPogoDsl.g:959:3: (otherlv_14= 'description' ( (lv_description_15_0= ruleClassDescription ) ) )?
            int alt15=2;
            int LA15_0 = input.LA(1);

            if ( (LA15_0==27) ) {
                alt15=1;
            }
            switch (alt15) {
                case 1 :
                    // InternalPogoDsl.g:960:4: otherlv_14= 'description' ( (lv_description_15_0= ruleClassDescription ) )
                    {
                    otherlv_14=(Token)match(input,27,FOLLOW_22); 

                    				newLeafNode(otherlv_14, grammarAccess.getPogoDeviceClassAccess().getDescriptionKeyword_10_0());
                    			
                    // InternalPogoDsl.g:964:4: ( (lv_description_15_0= ruleClassDescription ) )
                    // InternalPogoDsl.g:965:5: (lv_description_15_0= ruleClassDescription )
                    {
                    // InternalPogoDsl.g:965:5: (lv_description_15_0= ruleClassDescription )
                    // InternalPogoDsl.g:966:6: lv_description_15_0= ruleClassDescription
                    {

                    						newCompositeNode(grammarAccess.getPogoDeviceClassAccess().getDescriptionClassDescriptionParserRuleCall_10_1_0());
                    					
                    pushFollow(FOLLOW_23);
                    lv_description_15_0=ruleClassDescription();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getPogoDeviceClassRule());
                    						}
                    						set(
                    							current,
                    							"description",
                    							lv_description_15_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.ClassDescription");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalPogoDsl.g:984:3: (otherlv_16= 'classProperties' otherlv_17= '{' ( (lv_classProperties_18_0= ruleProperty ) ) (otherlv_19= ',' ( (lv_classProperties_20_0= ruleProperty ) ) )* otherlv_21= '}' )?
            int alt17=2;
            int LA17_0 = input.LA(1);

            if ( (LA17_0==28) ) {
                alt17=1;
            }
            switch (alt17) {
                case 1 :
                    // InternalPogoDsl.g:985:4: otherlv_16= 'classProperties' otherlv_17= '{' ( (lv_classProperties_18_0= ruleProperty ) ) (otherlv_19= ',' ( (lv_classProperties_20_0= ruleProperty ) ) )* otherlv_21= '}'
                    {
                    otherlv_16=(Token)match(input,28,FOLLOW_3); 

                    				newLeafNode(otherlv_16, grammarAccess.getPogoDeviceClassAccess().getClassPropertiesKeyword_11_0());
                    			
                    otherlv_17=(Token)match(input,12,FOLLOW_24); 

                    				newLeafNode(otherlv_17, grammarAccess.getPogoDeviceClassAccess().getLeftCurlyBracketKeyword_11_1());
                    			
                    // InternalPogoDsl.g:993:4: ( (lv_classProperties_18_0= ruleProperty ) )
                    // InternalPogoDsl.g:994:5: (lv_classProperties_18_0= ruleProperty )
                    {
                    // InternalPogoDsl.g:994:5: (lv_classProperties_18_0= ruleProperty )
                    // InternalPogoDsl.g:995:6: lv_classProperties_18_0= ruleProperty
                    {

                    						newCompositeNode(grammarAccess.getPogoDeviceClassAccess().getClassPropertiesPropertyParserRuleCall_11_2_0());
                    					
                    pushFollow(FOLLOW_6);
                    lv_classProperties_18_0=ruleProperty();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getPogoDeviceClassRule());
                    						}
                    						add(
                    							current,
                    							"classProperties",
                    							lv_classProperties_18_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.Property");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalPogoDsl.g:1012:4: (otherlv_19= ',' ( (lv_classProperties_20_0= ruleProperty ) ) )*
                    loop16:
                    do {
                        int alt16=2;
                        int LA16_0 = input.LA(1);

                        if ( (LA16_0==14) ) {
                            alt16=1;
                        }


                        switch (alt16) {
                    	case 1 :
                    	    // InternalPogoDsl.g:1013:5: otherlv_19= ',' ( (lv_classProperties_20_0= ruleProperty ) )
                    	    {
                    	    otherlv_19=(Token)match(input,14,FOLLOW_24); 

                    	    					newLeafNode(otherlv_19, grammarAccess.getPogoDeviceClassAccess().getCommaKeyword_11_3_0());
                    	    				
                    	    // InternalPogoDsl.g:1017:5: ( (lv_classProperties_20_0= ruleProperty ) )
                    	    // InternalPogoDsl.g:1018:6: (lv_classProperties_20_0= ruleProperty )
                    	    {
                    	    // InternalPogoDsl.g:1018:6: (lv_classProperties_20_0= ruleProperty )
                    	    // InternalPogoDsl.g:1019:7: lv_classProperties_20_0= ruleProperty
                    	    {

                    	    							newCompositeNode(grammarAccess.getPogoDeviceClassAccess().getClassPropertiesPropertyParserRuleCall_11_3_1_0());
                    	    						
                    	    pushFollow(FOLLOW_6);
                    	    lv_classProperties_20_0=ruleProperty();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getPogoDeviceClassRule());
                    	    							}
                    	    							add(
                    	    								current,
                    	    								"classProperties",
                    	    								lv_classProperties_20_0,
                    	    								"com.mncml.implementation.pogo.dsl.PogoDsl.Property");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop16;
                        }
                    } while (true);

                    otherlv_21=(Token)match(input,15,FOLLOW_25); 

                    				newLeafNode(otherlv_21, grammarAccess.getPogoDeviceClassAccess().getRightCurlyBracketKeyword_11_4());
                    			

                    }
                    break;

            }

            // InternalPogoDsl.g:1042:3: (otherlv_22= 'deviceProperties' otherlv_23= '{' ( (lv_deviceProperties_24_0= ruleProperty ) ) (otherlv_25= ',' ( (lv_deviceProperties_26_0= ruleProperty ) ) )* otherlv_27= '}' )?
            int alt19=2;
            int LA19_0 = input.LA(1);

            if ( (LA19_0==29) ) {
                alt19=1;
            }
            switch (alt19) {
                case 1 :
                    // InternalPogoDsl.g:1043:4: otherlv_22= 'deviceProperties' otherlv_23= '{' ( (lv_deviceProperties_24_0= ruleProperty ) ) (otherlv_25= ',' ( (lv_deviceProperties_26_0= ruleProperty ) ) )* otherlv_27= '}'
                    {
                    otherlv_22=(Token)match(input,29,FOLLOW_3); 

                    				newLeafNode(otherlv_22, grammarAccess.getPogoDeviceClassAccess().getDevicePropertiesKeyword_12_0());
                    			
                    otherlv_23=(Token)match(input,12,FOLLOW_24); 

                    				newLeafNode(otherlv_23, grammarAccess.getPogoDeviceClassAccess().getLeftCurlyBracketKeyword_12_1());
                    			
                    // InternalPogoDsl.g:1051:4: ( (lv_deviceProperties_24_0= ruleProperty ) )
                    // InternalPogoDsl.g:1052:5: (lv_deviceProperties_24_0= ruleProperty )
                    {
                    // InternalPogoDsl.g:1052:5: (lv_deviceProperties_24_0= ruleProperty )
                    // InternalPogoDsl.g:1053:6: lv_deviceProperties_24_0= ruleProperty
                    {

                    						newCompositeNode(grammarAccess.getPogoDeviceClassAccess().getDevicePropertiesPropertyParserRuleCall_12_2_0());
                    					
                    pushFollow(FOLLOW_6);
                    lv_deviceProperties_24_0=ruleProperty();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getPogoDeviceClassRule());
                    						}
                    						add(
                    							current,
                    							"deviceProperties",
                    							lv_deviceProperties_24_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.Property");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalPogoDsl.g:1070:4: (otherlv_25= ',' ( (lv_deviceProperties_26_0= ruleProperty ) ) )*
                    loop18:
                    do {
                        int alt18=2;
                        int LA18_0 = input.LA(1);

                        if ( (LA18_0==14) ) {
                            alt18=1;
                        }


                        switch (alt18) {
                    	case 1 :
                    	    // InternalPogoDsl.g:1071:5: otherlv_25= ',' ( (lv_deviceProperties_26_0= ruleProperty ) )
                    	    {
                    	    otherlv_25=(Token)match(input,14,FOLLOW_24); 

                    	    					newLeafNode(otherlv_25, grammarAccess.getPogoDeviceClassAccess().getCommaKeyword_12_3_0());
                    	    				
                    	    // InternalPogoDsl.g:1075:5: ( (lv_deviceProperties_26_0= ruleProperty ) )
                    	    // InternalPogoDsl.g:1076:6: (lv_deviceProperties_26_0= ruleProperty )
                    	    {
                    	    // InternalPogoDsl.g:1076:6: (lv_deviceProperties_26_0= ruleProperty )
                    	    // InternalPogoDsl.g:1077:7: lv_deviceProperties_26_0= ruleProperty
                    	    {

                    	    							newCompositeNode(grammarAccess.getPogoDeviceClassAccess().getDevicePropertiesPropertyParserRuleCall_12_3_1_0());
                    	    						
                    	    pushFollow(FOLLOW_6);
                    	    lv_deviceProperties_26_0=ruleProperty();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getPogoDeviceClassRule());
                    	    							}
                    	    							add(
                    	    								current,
                    	    								"deviceProperties",
                    	    								lv_deviceProperties_26_0,
                    	    								"com.mncml.implementation.pogo.dsl.PogoDsl.Property");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop18;
                        }
                    } while (true);

                    otherlv_27=(Token)match(input,15,FOLLOW_26); 

                    				newLeafNode(otherlv_27, grammarAccess.getPogoDeviceClassAccess().getRightCurlyBracketKeyword_12_4());
                    			

                    }
                    break;

            }

            // InternalPogoDsl.g:1100:3: (otherlv_28= 'commands' otherlv_29= '{' ( (lv_commands_30_0= ruleCommand ) ) (otherlv_31= ',' ( (lv_commands_32_0= ruleCommand ) ) )* otherlv_33= '}' )?
            int alt21=2;
            int LA21_0 = input.LA(1);

            if ( (LA21_0==30) ) {
                alt21=1;
            }
            switch (alt21) {
                case 1 :
                    // InternalPogoDsl.g:1101:4: otherlv_28= 'commands' otherlv_29= '{' ( (lv_commands_30_0= ruleCommand ) ) (otherlv_31= ',' ( (lv_commands_32_0= ruleCommand ) ) )* otherlv_33= '}'
                    {
                    otherlv_28=(Token)match(input,30,FOLLOW_3); 

                    				newLeafNode(otherlv_28, grammarAccess.getPogoDeviceClassAccess().getCommandsKeyword_13_0());
                    			
                    otherlv_29=(Token)match(input,12,FOLLOW_27); 

                    				newLeafNode(otherlv_29, grammarAccess.getPogoDeviceClassAccess().getLeftCurlyBracketKeyword_13_1());
                    			
                    // InternalPogoDsl.g:1109:4: ( (lv_commands_30_0= ruleCommand ) )
                    // InternalPogoDsl.g:1110:5: (lv_commands_30_0= ruleCommand )
                    {
                    // InternalPogoDsl.g:1110:5: (lv_commands_30_0= ruleCommand )
                    // InternalPogoDsl.g:1111:6: lv_commands_30_0= ruleCommand
                    {

                    						newCompositeNode(grammarAccess.getPogoDeviceClassAccess().getCommandsCommandParserRuleCall_13_2_0());
                    					
                    pushFollow(FOLLOW_6);
                    lv_commands_30_0=ruleCommand();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getPogoDeviceClassRule());
                    						}
                    						add(
                    							current,
                    							"commands",
                    							lv_commands_30_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.Command");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalPogoDsl.g:1128:4: (otherlv_31= ',' ( (lv_commands_32_0= ruleCommand ) ) )*
                    loop20:
                    do {
                        int alt20=2;
                        int LA20_0 = input.LA(1);

                        if ( (LA20_0==14) ) {
                            alt20=1;
                        }


                        switch (alt20) {
                    	case 1 :
                    	    // InternalPogoDsl.g:1129:5: otherlv_31= ',' ( (lv_commands_32_0= ruleCommand ) )
                    	    {
                    	    otherlv_31=(Token)match(input,14,FOLLOW_27); 

                    	    					newLeafNode(otherlv_31, grammarAccess.getPogoDeviceClassAccess().getCommaKeyword_13_3_0());
                    	    				
                    	    // InternalPogoDsl.g:1133:5: ( (lv_commands_32_0= ruleCommand ) )
                    	    // InternalPogoDsl.g:1134:6: (lv_commands_32_0= ruleCommand )
                    	    {
                    	    // InternalPogoDsl.g:1134:6: (lv_commands_32_0= ruleCommand )
                    	    // InternalPogoDsl.g:1135:7: lv_commands_32_0= ruleCommand
                    	    {

                    	    							newCompositeNode(grammarAccess.getPogoDeviceClassAccess().getCommandsCommandParserRuleCall_13_3_1_0());
                    	    						
                    	    pushFollow(FOLLOW_6);
                    	    lv_commands_32_0=ruleCommand();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getPogoDeviceClassRule());
                    	    							}
                    	    							add(
                    	    								current,
                    	    								"commands",
                    	    								lv_commands_32_0,
                    	    								"com.mncml.implementation.pogo.dsl.PogoDsl.Command");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop20;
                        }
                    } while (true);

                    otherlv_33=(Token)match(input,15,FOLLOW_28); 

                    				newLeafNode(otherlv_33, grammarAccess.getPogoDeviceClassAccess().getRightCurlyBracketKeyword_13_4());
                    			

                    }
                    break;

            }

            // InternalPogoDsl.g:1158:3: (otherlv_34= 'dynamicCommands' otherlv_35= '{' ( (lv_dynamicCommands_36_0= ruleCommand ) ) (otherlv_37= ',' ( (lv_dynamicCommands_38_0= ruleCommand ) ) )* otherlv_39= '}' )?
            int alt23=2;
            int LA23_0 = input.LA(1);

            if ( (LA23_0==31) ) {
                alt23=1;
            }
            switch (alt23) {
                case 1 :
                    // InternalPogoDsl.g:1159:4: otherlv_34= 'dynamicCommands' otherlv_35= '{' ( (lv_dynamicCommands_36_0= ruleCommand ) ) (otherlv_37= ',' ( (lv_dynamicCommands_38_0= ruleCommand ) ) )* otherlv_39= '}'
                    {
                    otherlv_34=(Token)match(input,31,FOLLOW_3); 

                    				newLeafNode(otherlv_34, grammarAccess.getPogoDeviceClassAccess().getDynamicCommandsKeyword_14_0());
                    			
                    otherlv_35=(Token)match(input,12,FOLLOW_27); 

                    				newLeafNode(otherlv_35, grammarAccess.getPogoDeviceClassAccess().getLeftCurlyBracketKeyword_14_1());
                    			
                    // InternalPogoDsl.g:1167:4: ( (lv_dynamicCommands_36_0= ruleCommand ) )
                    // InternalPogoDsl.g:1168:5: (lv_dynamicCommands_36_0= ruleCommand )
                    {
                    // InternalPogoDsl.g:1168:5: (lv_dynamicCommands_36_0= ruleCommand )
                    // InternalPogoDsl.g:1169:6: lv_dynamicCommands_36_0= ruleCommand
                    {

                    						newCompositeNode(grammarAccess.getPogoDeviceClassAccess().getDynamicCommandsCommandParserRuleCall_14_2_0());
                    					
                    pushFollow(FOLLOW_6);
                    lv_dynamicCommands_36_0=ruleCommand();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getPogoDeviceClassRule());
                    						}
                    						add(
                    							current,
                    							"dynamicCommands",
                    							lv_dynamicCommands_36_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.Command");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalPogoDsl.g:1186:4: (otherlv_37= ',' ( (lv_dynamicCommands_38_0= ruleCommand ) ) )*
                    loop22:
                    do {
                        int alt22=2;
                        int LA22_0 = input.LA(1);

                        if ( (LA22_0==14) ) {
                            alt22=1;
                        }


                        switch (alt22) {
                    	case 1 :
                    	    // InternalPogoDsl.g:1187:5: otherlv_37= ',' ( (lv_dynamicCommands_38_0= ruleCommand ) )
                    	    {
                    	    otherlv_37=(Token)match(input,14,FOLLOW_27); 

                    	    					newLeafNode(otherlv_37, grammarAccess.getPogoDeviceClassAccess().getCommaKeyword_14_3_0());
                    	    				
                    	    // InternalPogoDsl.g:1191:5: ( (lv_dynamicCommands_38_0= ruleCommand ) )
                    	    // InternalPogoDsl.g:1192:6: (lv_dynamicCommands_38_0= ruleCommand )
                    	    {
                    	    // InternalPogoDsl.g:1192:6: (lv_dynamicCommands_38_0= ruleCommand )
                    	    // InternalPogoDsl.g:1193:7: lv_dynamicCommands_38_0= ruleCommand
                    	    {

                    	    							newCompositeNode(grammarAccess.getPogoDeviceClassAccess().getDynamicCommandsCommandParserRuleCall_14_3_1_0());
                    	    						
                    	    pushFollow(FOLLOW_6);
                    	    lv_dynamicCommands_38_0=ruleCommand();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getPogoDeviceClassRule());
                    	    							}
                    	    							add(
                    	    								current,
                    	    								"dynamicCommands",
                    	    								lv_dynamicCommands_38_0,
                    	    								"com.mncml.implementation.pogo.dsl.PogoDsl.Command");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop22;
                        }
                    } while (true);

                    otherlv_39=(Token)match(input,15,FOLLOW_29); 

                    				newLeafNode(otherlv_39, grammarAccess.getPogoDeviceClassAccess().getRightCurlyBracketKeyword_14_4());
                    			

                    }
                    break;

            }

            // InternalPogoDsl.g:1216:3: (otherlv_40= 'attributes' otherlv_41= '{' ( (lv_attributes_42_0= ruleAttribute ) ) (otherlv_43= ',' ( (lv_attributes_44_0= ruleAttribute ) ) )* otherlv_45= '}' )?
            int alt25=2;
            int LA25_0 = input.LA(1);

            if ( (LA25_0==32) ) {
                alt25=1;
            }
            switch (alt25) {
                case 1 :
                    // InternalPogoDsl.g:1217:4: otherlv_40= 'attributes' otherlv_41= '{' ( (lv_attributes_42_0= ruleAttribute ) ) (otherlv_43= ',' ( (lv_attributes_44_0= ruleAttribute ) ) )* otherlv_45= '}'
                    {
                    otherlv_40=(Token)match(input,32,FOLLOW_3); 

                    				newLeafNode(otherlv_40, grammarAccess.getPogoDeviceClassAccess().getAttributesKeyword_15_0());
                    			
                    otherlv_41=(Token)match(input,12,FOLLOW_30); 

                    				newLeafNode(otherlv_41, grammarAccess.getPogoDeviceClassAccess().getLeftCurlyBracketKeyword_15_1());
                    			
                    // InternalPogoDsl.g:1225:4: ( (lv_attributes_42_0= ruleAttribute ) )
                    // InternalPogoDsl.g:1226:5: (lv_attributes_42_0= ruleAttribute )
                    {
                    // InternalPogoDsl.g:1226:5: (lv_attributes_42_0= ruleAttribute )
                    // InternalPogoDsl.g:1227:6: lv_attributes_42_0= ruleAttribute
                    {

                    						newCompositeNode(grammarAccess.getPogoDeviceClassAccess().getAttributesAttributeParserRuleCall_15_2_0());
                    					
                    pushFollow(FOLLOW_6);
                    lv_attributes_42_0=ruleAttribute();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getPogoDeviceClassRule());
                    						}
                    						add(
                    							current,
                    							"attributes",
                    							lv_attributes_42_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.Attribute");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalPogoDsl.g:1244:4: (otherlv_43= ',' ( (lv_attributes_44_0= ruleAttribute ) ) )*
                    loop24:
                    do {
                        int alt24=2;
                        int LA24_0 = input.LA(1);

                        if ( (LA24_0==14) ) {
                            alt24=1;
                        }


                        switch (alt24) {
                    	case 1 :
                    	    // InternalPogoDsl.g:1245:5: otherlv_43= ',' ( (lv_attributes_44_0= ruleAttribute ) )
                    	    {
                    	    otherlv_43=(Token)match(input,14,FOLLOW_30); 

                    	    					newLeafNode(otherlv_43, grammarAccess.getPogoDeviceClassAccess().getCommaKeyword_15_3_0());
                    	    				
                    	    // InternalPogoDsl.g:1249:5: ( (lv_attributes_44_0= ruleAttribute ) )
                    	    // InternalPogoDsl.g:1250:6: (lv_attributes_44_0= ruleAttribute )
                    	    {
                    	    // InternalPogoDsl.g:1250:6: (lv_attributes_44_0= ruleAttribute )
                    	    // InternalPogoDsl.g:1251:7: lv_attributes_44_0= ruleAttribute
                    	    {

                    	    							newCompositeNode(grammarAccess.getPogoDeviceClassAccess().getAttributesAttributeParserRuleCall_15_3_1_0());
                    	    						
                    	    pushFollow(FOLLOW_6);
                    	    lv_attributes_44_0=ruleAttribute();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getPogoDeviceClassRule());
                    	    							}
                    	    							add(
                    	    								current,
                    	    								"attributes",
                    	    								lv_attributes_44_0,
                    	    								"com.mncml.implementation.pogo.dsl.PogoDsl.Attribute");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop24;
                        }
                    } while (true);

                    otherlv_45=(Token)match(input,15,FOLLOW_31); 

                    				newLeafNode(otherlv_45, grammarAccess.getPogoDeviceClassAccess().getRightCurlyBracketKeyword_15_4());
                    			

                    }
                    break;

            }

            // InternalPogoDsl.g:1274:3: (otherlv_46= 'dynamicAttributes' otherlv_47= '{' ( (lv_dynamicAttributes_48_0= ruleAttribute ) ) (otherlv_49= ',' ( (lv_dynamicAttributes_50_0= ruleAttribute ) ) )* otherlv_51= '}' )?
            int alt27=2;
            int LA27_0 = input.LA(1);

            if ( (LA27_0==33) ) {
                alt27=1;
            }
            switch (alt27) {
                case 1 :
                    // InternalPogoDsl.g:1275:4: otherlv_46= 'dynamicAttributes' otherlv_47= '{' ( (lv_dynamicAttributes_48_0= ruleAttribute ) ) (otherlv_49= ',' ( (lv_dynamicAttributes_50_0= ruleAttribute ) ) )* otherlv_51= '}'
                    {
                    otherlv_46=(Token)match(input,33,FOLLOW_3); 

                    				newLeafNode(otherlv_46, grammarAccess.getPogoDeviceClassAccess().getDynamicAttributesKeyword_16_0());
                    			
                    otherlv_47=(Token)match(input,12,FOLLOW_30); 

                    				newLeafNode(otherlv_47, grammarAccess.getPogoDeviceClassAccess().getLeftCurlyBracketKeyword_16_1());
                    			
                    // InternalPogoDsl.g:1283:4: ( (lv_dynamicAttributes_48_0= ruleAttribute ) )
                    // InternalPogoDsl.g:1284:5: (lv_dynamicAttributes_48_0= ruleAttribute )
                    {
                    // InternalPogoDsl.g:1284:5: (lv_dynamicAttributes_48_0= ruleAttribute )
                    // InternalPogoDsl.g:1285:6: lv_dynamicAttributes_48_0= ruleAttribute
                    {

                    						newCompositeNode(grammarAccess.getPogoDeviceClassAccess().getDynamicAttributesAttributeParserRuleCall_16_2_0());
                    					
                    pushFollow(FOLLOW_6);
                    lv_dynamicAttributes_48_0=ruleAttribute();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getPogoDeviceClassRule());
                    						}
                    						add(
                    							current,
                    							"dynamicAttributes",
                    							lv_dynamicAttributes_48_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.Attribute");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalPogoDsl.g:1302:4: (otherlv_49= ',' ( (lv_dynamicAttributes_50_0= ruleAttribute ) ) )*
                    loop26:
                    do {
                        int alt26=2;
                        int LA26_0 = input.LA(1);

                        if ( (LA26_0==14) ) {
                            alt26=1;
                        }


                        switch (alt26) {
                    	case 1 :
                    	    // InternalPogoDsl.g:1303:5: otherlv_49= ',' ( (lv_dynamicAttributes_50_0= ruleAttribute ) )
                    	    {
                    	    otherlv_49=(Token)match(input,14,FOLLOW_30); 

                    	    					newLeafNode(otherlv_49, grammarAccess.getPogoDeviceClassAccess().getCommaKeyword_16_3_0());
                    	    				
                    	    // InternalPogoDsl.g:1307:5: ( (lv_dynamicAttributes_50_0= ruleAttribute ) )
                    	    // InternalPogoDsl.g:1308:6: (lv_dynamicAttributes_50_0= ruleAttribute )
                    	    {
                    	    // InternalPogoDsl.g:1308:6: (lv_dynamicAttributes_50_0= ruleAttribute )
                    	    // InternalPogoDsl.g:1309:7: lv_dynamicAttributes_50_0= ruleAttribute
                    	    {

                    	    							newCompositeNode(grammarAccess.getPogoDeviceClassAccess().getDynamicAttributesAttributeParserRuleCall_16_3_1_0());
                    	    						
                    	    pushFollow(FOLLOW_6);
                    	    lv_dynamicAttributes_50_0=ruleAttribute();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getPogoDeviceClassRule());
                    	    							}
                    	    							add(
                    	    								current,
                    	    								"dynamicAttributes",
                    	    								lv_dynamicAttributes_50_0,
                    	    								"com.mncml.implementation.pogo.dsl.PogoDsl.Attribute");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop26;
                        }
                    } while (true);

                    otherlv_51=(Token)match(input,15,FOLLOW_32); 

                    				newLeafNode(otherlv_51, grammarAccess.getPogoDeviceClassAccess().getRightCurlyBracketKeyword_16_4());
                    			

                    }
                    break;

            }

            // InternalPogoDsl.g:1332:3: (otherlv_52= 'forwardedAttributes' otherlv_53= '{' ( (lv_forwardedAttributes_54_0= ruleForwardedAttribute ) ) (otherlv_55= ',' ( (lv_forwardedAttributes_56_0= ruleForwardedAttribute ) ) )* otherlv_57= '}' )?
            int alt29=2;
            int LA29_0 = input.LA(1);

            if ( (LA29_0==34) ) {
                alt29=1;
            }
            switch (alt29) {
                case 1 :
                    // InternalPogoDsl.g:1333:4: otherlv_52= 'forwardedAttributes' otherlv_53= '{' ( (lv_forwardedAttributes_54_0= ruleForwardedAttribute ) ) (otherlv_55= ',' ( (lv_forwardedAttributes_56_0= ruleForwardedAttribute ) ) )* otherlv_57= '}'
                    {
                    otherlv_52=(Token)match(input,34,FOLLOW_3); 

                    				newLeafNode(otherlv_52, grammarAccess.getPogoDeviceClassAccess().getForwardedAttributesKeyword_17_0());
                    			
                    otherlv_53=(Token)match(input,12,FOLLOW_33); 

                    				newLeafNode(otherlv_53, grammarAccess.getPogoDeviceClassAccess().getLeftCurlyBracketKeyword_17_1());
                    			
                    // InternalPogoDsl.g:1341:4: ( (lv_forwardedAttributes_54_0= ruleForwardedAttribute ) )
                    // InternalPogoDsl.g:1342:5: (lv_forwardedAttributes_54_0= ruleForwardedAttribute )
                    {
                    // InternalPogoDsl.g:1342:5: (lv_forwardedAttributes_54_0= ruleForwardedAttribute )
                    // InternalPogoDsl.g:1343:6: lv_forwardedAttributes_54_0= ruleForwardedAttribute
                    {

                    						newCompositeNode(grammarAccess.getPogoDeviceClassAccess().getForwardedAttributesForwardedAttributeParserRuleCall_17_2_0());
                    					
                    pushFollow(FOLLOW_6);
                    lv_forwardedAttributes_54_0=ruleForwardedAttribute();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getPogoDeviceClassRule());
                    						}
                    						add(
                    							current,
                    							"forwardedAttributes",
                    							lv_forwardedAttributes_54_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.ForwardedAttribute");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalPogoDsl.g:1360:4: (otherlv_55= ',' ( (lv_forwardedAttributes_56_0= ruleForwardedAttribute ) ) )*
                    loop28:
                    do {
                        int alt28=2;
                        int LA28_0 = input.LA(1);

                        if ( (LA28_0==14) ) {
                            alt28=1;
                        }


                        switch (alt28) {
                    	case 1 :
                    	    // InternalPogoDsl.g:1361:5: otherlv_55= ',' ( (lv_forwardedAttributes_56_0= ruleForwardedAttribute ) )
                    	    {
                    	    otherlv_55=(Token)match(input,14,FOLLOW_33); 

                    	    					newLeafNode(otherlv_55, grammarAccess.getPogoDeviceClassAccess().getCommaKeyword_17_3_0());
                    	    				
                    	    // InternalPogoDsl.g:1365:5: ( (lv_forwardedAttributes_56_0= ruleForwardedAttribute ) )
                    	    // InternalPogoDsl.g:1366:6: (lv_forwardedAttributes_56_0= ruleForwardedAttribute )
                    	    {
                    	    // InternalPogoDsl.g:1366:6: (lv_forwardedAttributes_56_0= ruleForwardedAttribute )
                    	    // InternalPogoDsl.g:1367:7: lv_forwardedAttributes_56_0= ruleForwardedAttribute
                    	    {

                    	    							newCompositeNode(grammarAccess.getPogoDeviceClassAccess().getForwardedAttributesForwardedAttributeParserRuleCall_17_3_1_0());
                    	    						
                    	    pushFollow(FOLLOW_6);
                    	    lv_forwardedAttributes_56_0=ruleForwardedAttribute();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getPogoDeviceClassRule());
                    	    							}
                    	    							add(
                    	    								current,
                    	    								"forwardedAttributes",
                    	    								lv_forwardedAttributes_56_0,
                    	    								"com.mncml.implementation.pogo.dsl.PogoDsl.ForwardedAttribute");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop28;
                        }
                    } while (true);

                    otherlv_57=(Token)match(input,15,FOLLOW_34); 

                    				newLeafNode(otherlv_57, grammarAccess.getPogoDeviceClassAccess().getRightCurlyBracketKeyword_17_4());
                    			

                    }
                    break;

            }

            // InternalPogoDsl.g:1390:3: (otherlv_58= 'pipes' otherlv_59= '{' ( (lv_pipes_60_0= rulePipe ) ) (otherlv_61= ',' ( (lv_pipes_62_0= rulePipe ) ) )* otherlv_63= '}' )?
            int alt31=2;
            int LA31_0 = input.LA(1);

            if ( (LA31_0==35) ) {
                alt31=1;
            }
            switch (alt31) {
                case 1 :
                    // InternalPogoDsl.g:1391:4: otherlv_58= 'pipes' otherlv_59= '{' ( (lv_pipes_60_0= rulePipe ) ) (otherlv_61= ',' ( (lv_pipes_62_0= rulePipe ) ) )* otherlv_63= '}'
                    {
                    otherlv_58=(Token)match(input,35,FOLLOW_3); 

                    				newLeafNode(otherlv_58, grammarAccess.getPogoDeviceClassAccess().getPipesKeyword_18_0());
                    			
                    otherlv_59=(Token)match(input,12,FOLLOW_35); 

                    				newLeafNode(otherlv_59, grammarAccess.getPogoDeviceClassAccess().getLeftCurlyBracketKeyword_18_1());
                    			
                    // InternalPogoDsl.g:1399:4: ( (lv_pipes_60_0= rulePipe ) )
                    // InternalPogoDsl.g:1400:5: (lv_pipes_60_0= rulePipe )
                    {
                    // InternalPogoDsl.g:1400:5: (lv_pipes_60_0= rulePipe )
                    // InternalPogoDsl.g:1401:6: lv_pipes_60_0= rulePipe
                    {

                    						newCompositeNode(grammarAccess.getPogoDeviceClassAccess().getPipesPipeParserRuleCall_18_2_0());
                    					
                    pushFollow(FOLLOW_6);
                    lv_pipes_60_0=rulePipe();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getPogoDeviceClassRule());
                    						}
                    						add(
                    							current,
                    							"pipes",
                    							lv_pipes_60_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.Pipe");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalPogoDsl.g:1418:4: (otherlv_61= ',' ( (lv_pipes_62_0= rulePipe ) ) )*
                    loop30:
                    do {
                        int alt30=2;
                        int LA30_0 = input.LA(1);

                        if ( (LA30_0==14) ) {
                            alt30=1;
                        }


                        switch (alt30) {
                    	case 1 :
                    	    // InternalPogoDsl.g:1419:5: otherlv_61= ',' ( (lv_pipes_62_0= rulePipe ) )
                    	    {
                    	    otherlv_61=(Token)match(input,14,FOLLOW_35); 

                    	    					newLeafNode(otherlv_61, grammarAccess.getPogoDeviceClassAccess().getCommaKeyword_18_3_0());
                    	    				
                    	    // InternalPogoDsl.g:1423:5: ( (lv_pipes_62_0= rulePipe ) )
                    	    // InternalPogoDsl.g:1424:6: (lv_pipes_62_0= rulePipe )
                    	    {
                    	    // InternalPogoDsl.g:1424:6: (lv_pipes_62_0= rulePipe )
                    	    // InternalPogoDsl.g:1425:7: lv_pipes_62_0= rulePipe
                    	    {

                    	    							newCompositeNode(grammarAccess.getPogoDeviceClassAccess().getPipesPipeParserRuleCall_18_3_1_0());
                    	    						
                    	    pushFollow(FOLLOW_6);
                    	    lv_pipes_62_0=rulePipe();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getPogoDeviceClassRule());
                    	    							}
                    	    							add(
                    	    								current,
                    	    								"pipes",
                    	    								lv_pipes_62_0,
                    	    								"com.mncml.implementation.pogo.dsl.PogoDsl.Pipe");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop30;
                        }
                    } while (true);

                    otherlv_63=(Token)match(input,15,FOLLOW_36); 

                    				newLeafNode(otherlv_63, grammarAccess.getPogoDeviceClassAccess().getRightCurlyBracketKeyword_18_4());
                    			

                    }
                    break;

            }

            // InternalPogoDsl.g:1448:3: (otherlv_64= 'states' otherlv_65= '{' ( (lv_states_66_0= ruleState ) ) (otherlv_67= ',' ( (lv_states_68_0= ruleState ) ) )* otherlv_69= '}' )?
            int alt33=2;
            int LA33_0 = input.LA(1);

            if ( (LA33_0==36) ) {
                alt33=1;
            }
            switch (alt33) {
                case 1 :
                    // InternalPogoDsl.g:1449:4: otherlv_64= 'states' otherlv_65= '{' ( (lv_states_66_0= ruleState ) ) (otherlv_67= ',' ( (lv_states_68_0= ruleState ) ) )* otherlv_69= '}'
                    {
                    otherlv_64=(Token)match(input,36,FOLLOW_3); 

                    				newLeafNode(otherlv_64, grammarAccess.getPogoDeviceClassAccess().getStatesKeyword_19_0());
                    			
                    otherlv_65=(Token)match(input,12,FOLLOW_37); 

                    				newLeafNode(otherlv_65, grammarAccess.getPogoDeviceClassAccess().getLeftCurlyBracketKeyword_19_1());
                    			
                    // InternalPogoDsl.g:1457:4: ( (lv_states_66_0= ruleState ) )
                    // InternalPogoDsl.g:1458:5: (lv_states_66_0= ruleState )
                    {
                    // InternalPogoDsl.g:1458:5: (lv_states_66_0= ruleState )
                    // InternalPogoDsl.g:1459:6: lv_states_66_0= ruleState
                    {

                    						newCompositeNode(grammarAccess.getPogoDeviceClassAccess().getStatesStateParserRuleCall_19_2_0());
                    					
                    pushFollow(FOLLOW_6);
                    lv_states_66_0=ruleState();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getPogoDeviceClassRule());
                    						}
                    						add(
                    							current,
                    							"states",
                    							lv_states_66_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.State");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalPogoDsl.g:1476:4: (otherlv_67= ',' ( (lv_states_68_0= ruleState ) ) )*
                    loop32:
                    do {
                        int alt32=2;
                        int LA32_0 = input.LA(1);

                        if ( (LA32_0==14) ) {
                            alt32=1;
                        }


                        switch (alt32) {
                    	case 1 :
                    	    // InternalPogoDsl.g:1477:5: otherlv_67= ',' ( (lv_states_68_0= ruleState ) )
                    	    {
                    	    otherlv_67=(Token)match(input,14,FOLLOW_37); 

                    	    					newLeafNode(otherlv_67, grammarAccess.getPogoDeviceClassAccess().getCommaKeyword_19_3_0());
                    	    				
                    	    // InternalPogoDsl.g:1481:5: ( (lv_states_68_0= ruleState ) )
                    	    // InternalPogoDsl.g:1482:6: (lv_states_68_0= ruleState )
                    	    {
                    	    // InternalPogoDsl.g:1482:6: (lv_states_68_0= ruleState )
                    	    // InternalPogoDsl.g:1483:7: lv_states_68_0= ruleState
                    	    {

                    	    							newCompositeNode(grammarAccess.getPogoDeviceClassAccess().getStatesStateParserRuleCall_19_3_1_0());
                    	    						
                    	    pushFollow(FOLLOW_6);
                    	    lv_states_68_0=ruleState();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getPogoDeviceClassRule());
                    	    							}
                    	    							add(
                    	    								current,
                    	    								"states",
                    	    								lv_states_68_0,
                    	    								"com.mncml.implementation.pogo.dsl.PogoDsl.State");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop32;
                        }
                    } while (true);

                    otherlv_69=(Token)match(input,15,FOLLOW_38); 

                    				newLeafNode(otherlv_69, grammarAccess.getPogoDeviceClassAccess().getRightCurlyBracketKeyword_19_4());
                    			

                    }
                    break;

            }

            // InternalPogoDsl.g:1506:3: (otherlv_70= 'preferences' ( (lv_preferences_71_0= rulePreferences ) ) )?
            int alt34=2;
            int LA34_0 = input.LA(1);

            if ( (LA34_0==37) ) {
                alt34=1;
            }
            switch (alt34) {
                case 1 :
                    // InternalPogoDsl.g:1507:4: otherlv_70= 'preferences' ( (lv_preferences_71_0= rulePreferences ) )
                    {
                    otherlv_70=(Token)match(input,37,FOLLOW_39); 

                    				newLeafNode(otherlv_70, grammarAccess.getPogoDeviceClassAccess().getPreferencesKeyword_20_0());
                    			
                    // InternalPogoDsl.g:1511:4: ( (lv_preferences_71_0= rulePreferences ) )
                    // InternalPogoDsl.g:1512:5: (lv_preferences_71_0= rulePreferences )
                    {
                    // InternalPogoDsl.g:1512:5: (lv_preferences_71_0= rulePreferences )
                    // InternalPogoDsl.g:1513:6: lv_preferences_71_0= rulePreferences
                    {

                    						newCompositeNode(grammarAccess.getPogoDeviceClassAccess().getPreferencesPreferencesParserRuleCall_20_1_0());
                    					
                    pushFollow(FOLLOW_40);
                    lv_preferences_71_0=rulePreferences();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getPogoDeviceClassRule());
                    						}
                    						set(
                    							current,
                    							"preferences",
                    							lv_preferences_71_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.Preferences");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalPogoDsl.g:1531:3: (otherlv_72= 'additionalFiles' otherlv_73= '{' ( (lv_additionalFiles_74_0= ruleAdditionalFile ) ) (otherlv_75= ',' ( (lv_additionalFiles_76_0= ruleAdditionalFile ) ) )* otherlv_77= '}' )?
            int alt36=2;
            int LA36_0 = input.LA(1);

            if ( (LA36_0==38) ) {
                alt36=1;
            }
            switch (alt36) {
                case 1 :
                    // InternalPogoDsl.g:1532:4: otherlv_72= 'additionalFiles' otherlv_73= '{' ( (lv_additionalFiles_74_0= ruleAdditionalFile ) ) (otherlv_75= ',' ( (lv_additionalFiles_76_0= ruleAdditionalFile ) ) )* otherlv_77= '}'
                    {
                    otherlv_72=(Token)match(input,38,FOLLOW_3); 

                    				newLeafNode(otherlv_72, grammarAccess.getPogoDeviceClassAccess().getAdditionalFilesKeyword_21_0());
                    			
                    otherlv_73=(Token)match(input,12,FOLLOW_41); 

                    				newLeafNode(otherlv_73, grammarAccess.getPogoDeviceClassAccess().getLeftCurlyBracketKeyword_21_1());
                    			
                    // InternalPogoDsl.g:1540:4: ( (lv_additionalFiles_74_0= ruleAdditionalFile ) )
                    // InternalPogoDsl.g:1541:5: (lv_additionalFiles_74_0= ruleAdditionalFile )
                    {
                    // InternalPogoDsl.g:1541:5: (lv_additionalFiles_74_0= ruleAdditionalFile )
                    // InternalPogoDsl.g:1542:6: lv_additionalFiles_74_0= ruleAdditionalFile
                    {

                    						newCompositeNode(grammarAccess.getPogoDeviceClassAccess().getAdditionalFilesAdditionalFileParserRuleCall_21_2_0());
                    					
                    pushFollow(FOLLOW_6);
                    lv_additionalFiles_74_0=ruleAdditionalFile();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getPogoDeviceClassRule());
                    						}
                    						add(
                    							current,
                    							"additionalFiles",
                    							lv_additionalFiles_74_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.AdditionalFile");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalPogoDsl.g:1559:4: (otherlv_75= ',' ( (lv_additionalFiles_76_0= ruleAdditionalFile ) ) )*
                    loop35:
                    do {
                        int alt35=2;
                        int LA35_0 = input.LA(1);

                        if ( (LA35_0==14) ) {
                            alt35=1;
                        }


                        switch (alt35) {
                    	case 1 :
                    	    // InternalPogoDsl.g:1560:5: otherlv_75= ',' ( (lv_additionalFiles_76_0= ruleAdditionalFile ) )
                    	    {
                    	    otherlv_75=(Token)match(input,14,FOLLOW_41); 

                    	    					newLeafNode(otherlv_75, grammarAccess.getPogoDeviceClassAccess().getCommaKeyword_21_3_0());
                    	    				
                    	    // InternalPogoDsl.g:1564:5: ( (lv_additionalFiles_76_0= ruleAdditionalFile ) )
                    	    // InternalPogoDsl.g:1565:6: (lv_additionalFiles_76_0= ruleAdditionalFile )
                    	    {
                    	    // InternalPogoDsl.g:1565:6: (lv_additionalFiles_76_0= ruleAdditionalFile )
                    	    // InternalPogoDsl.g:1566:7: lv_additionalFiles_76_0= ruleAdditionalFile
                    	    {

                    	    							newCompositeNode(grammarAccess.getPogoDeviceClassAccess().getAdditionalFilesAdditionalFileParserRuleCall_21_3_1_0());
                    	    						
                    	    pushFollow(FOLLOW_6);
                    	    lv_additionalFiles_76_0=ruleAdditionalFile();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getPogoDeviceClassRule());
                    	    							}
                    	    							add(
                    	    								current,
                    	    								"additionalFiles",
                    	    								lv_additionalFiles_76_0,
                    	    								"com.mncml.implementation.pogo.dsl.PogoDsl.AdditionalFile");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop35;
                        }
                    } while (true);

                    otherlv_77=(Token)match(input,15,FOLLOW_42); 

                    				newLeafNode(otherlv_77, grammarAccess.getPogoDeviceClassAccess().getRightCurlyBracketKeyword_21_4());
                    			

                    }
                    break;

            }

            // InternalPogoDsl.g:1589:3: (otherlv_78= 'overlodedPollPeriodObject' otherlv_79= '{' ( (lv_overlodedPollPeriodObject_80_0= ruleOverlodedPollPeriodObject ) ) (otherlv_81= ',' ( (lv_overlodedPollPeriodObject_82_0= ruleOverlodedPollPeriodObject ) ) )* otherlv_83= '}' )?
            int alt38=2;
            int LA38_0 = input.LA(1);

            if ( (LA38_0==39) ) {
                alt38=1;
            }
            switch (alt38) {
                case 1 :
                    // InternalPogoDsl.g:1590:4: otherlv_78= 'overlodedPollPeriodObject' otherlv_79= '{' ( (lv_overlodedPollPeriodObject_80_0= ruleOverlodedPollPeriodObject ) ) (otherlv_81= ',' ( (lv_overlodedPollPeriodObject_82_0= ruleOverlodedPollPeriodObject ) ) )* otherlv_83= '}'
                    {
                    otherlv_78=(Token)match(input,39,FOLLOW_3); 

                    				newLeafNode(otherlv_78, grammarAccess.getPogoDeviceClassAccess().getOverlodedPollPeriodObjectKeyword_22_0());
                    			
                    otherlv_79=(Token)match(input,12,FOLLOW_43); 

                    				newLeafNode(otherlv_79, grammarAccess.getPogoDeviceClassAccess().getLeftCurlyBracketKeyword_22_1());
                    			
                    // InternalPogoDsl.g:1598:4: ( (lv_overlodedPollPeriodObject_80_0= ruleOverlodedPollPeriodObject ) )
                    // InternalPogoDsl.g:1599:5: (lv_overlodedPollPeriodObject_80_0= ruleOverlodedPollPeriodObject )
                    {
                    // InternalPogoDsl.g:1599:5: (lv_overlodedPollPeriodObject_80_0= ruleOverlodedPollPeriodObject )
                    // InternalPogoDsl.g:1600:6: lv_overlodedPollPeriodObject_80_0= ruleOverlodedPollPeriodObject
                    {

                    						newCompositeNode(grammarAccess.getPogoDeviceClassAccess().getOverlodedPollPeriodObjectOverlodedPollPeriodObjectParserRuleCall_22_2_0());
                    					
                    pushFollow(FOLLOW_6);
                    lv_overlodedPollPeriodObject_80_0=ruleOverlodedPollPeriodObject();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getPogoDeviceClassRule());
                    						}
                    						add(
                    							current,
                    							"overlodedPollPeriodObject",
                    							lv_overlodedPollPeriodObject_80_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.OverlodedPollPeriodObject");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalPogoDsl.g:1617:4: (otherlv_81= ',' ( (lv_overlodedPollPeriodObject_82_0= ruleOverlodedPollPeriodObject ) ) )*
                    loop37:
                    do {
                        int alt37=2;
                        int LA37_0 = input.LA(1);

                        if ( (LA37_0==14) ) {
                            alt37=1;
                        }


                        switch (alt37) {
                    	case 1 :
                    	    // InternalPogoDsl.g:1618:5: otherlv_81= ',' ( (lv_overlodedPollPeriodObject_82_0= ruleOverlodedPollPeriodObject ) )
                    	    {
                    	    otherlv_81=(Token)match(input,14,FOLLOW_43); 

                    	    					newLeafNode(otherlv_81, grammarAccess.getPogoDeviceClassAccess().getCommaKeyword_22_3_0());
                    	    				
                    	    // InternalPogoDsl.g:1622:5: ( (lv_overlodedPollPeriodObject_82_0= ruleOverlodedPollPeriodObject ) )
                    	    // InternalPogoDsl.g:1623:6: (lv_overlodedPollPeriodObject_82_0= ruleOverlodedPollPeriodObject )
                    	    {
                    	    // InternalPogoDsl.g:1623:6: (lv_overlodedPollPeriodObject_82_0= ruleOverlodedPollPeriodObject )
                    	    // InternalPogoDsl.g:1624:7: lv_overlodedPollPeriodObject_82_0= ruleOverlodedPollPeriodObject
                    	    {

                    	    							newCompositeNode(grammarAccess.getPogoDeviceClassAccess().getOverlodedPollPeriodObjectOverlodedPollPeriodObjectParserRuleCall_22_3_1_0());
                    	    						
                    	    pushFollow(FOLLOW_6);
                    	    lv_overlodedPollPeriodObject_82_0=ruleOverlodedPollPeriodObject();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getPogoDeviceClassRule());
                    	    							}
                    	    							add(
                    	    								current,
                    	    								"overlodedPollPeriodObject",
                    	    								lv_overlodedPollPeriodObject_82_0,
                    	    								"com.mncml.implementation.pogo.dsl.PogoDsl.OverlodedPollPeriodObject");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop37;
                        }
                    } while (true);

                    otherlv_83=(Token)match(input,15,FOLLOW_11); 

                    				newLeafNode(otherlv_83, grammarAccess.getPogoDeviceClassAccess().getRightCurlyBracketKeyword_22_4());
                    			

                    }
                    break;

            }

            otherlv_84=(Token)match(input,15,FOLLOW_2); 

            			newLeafNode(otherlv_84, grammarAccess.getPogoDeviceClassAccess().getRightCurlyBracketKeyword_23());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePogoDeviceClass"


    // $ANTLR start "entryRuleQualifiedName"
    // InternalPogoDsl.g:1655:1: entryRuleQualifiedName returns [String current=null] : iv_ruleQualifiedName= ruleQualifiedName EOF ;
    public final String entryRuleQualifiedName() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleQualifiedName = null;


        try {
            // InternalPogoDsl.g:1655:53: (iv_ruleQualifiedName= ruleQualifiedName EOF )
            // InternalPogoDsl.g:1656:2: iv_ruleQualifiedName= ruleQualifiedName EOF
            {
             newCompositeNode(grammarAccess.getQualifiedNameRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleQualifiedName=ruleQualifiedName();

            state._fsp--;

             current =iv_ruleQualifiedName.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleQualifiedName"


    // $ANTLR start "ruleQualifiedName"
    // InternalPogoDsl.g:1662:1: ruleQualifiedName returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* ) ;
    public final AntlrDatatypeRuleToken ruleQualifiedName() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_ID_0=null;
        Token kw=null;
        Token this_ID_2=null;


        	enterRule();

        try {
            // InternalPogoDsl.g:1668:2: ( (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* ) )
            // InternalPogoDsl.g:1669:2: (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* )
            {
            // InternalPogoDsl.g:1669:2: (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* )
            // InternalPogoDsl.g:1670:3: this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )*
            {
            this_ID_0=(Token)match(input,RULE_ID,FOLLOW_44); 

            			current.merge(this_ID_0);
            		

            			newLeafNode(this_ID_0, grammarAccess.getQualifiedNameAccess().getIDTerminalRuleCall_0());
            		
            // InternalPogoDsl.g:1677:3: (kw= '.' this_ID_2= RULE_ID )*
            loop39:
            do {
                int alt39=2;
                int LA39_0 = input.LA(1);

                if ( (LA39_0==40) ) {
                    alt39=1;
                }


                switch (alt39) {
            	case 1 :
            	    // InternalPogoDsl.g:1678:4: kw= '.' this_ID_2= RULE_ID
            	    {
            	    kw=(Token)match(input,40,FOLLOW_17); 

            	    				current.merge(kw);
            	    				newLeafNode(kw, grammarAccess.getQualifiedNameAccess().getFullStopKeyword_1_0());
            	    			
            	    this_ID_2=(Token)match(input,RULE_ID,FOLLOW_44); 

            	    				current.merge(this_ID_2);
            	    			

            	    				newLeafNode(this_ID_2, grammarAccess.getQualifiedNameAccess().getIDTerminalRuleCall_1_1());
            	    			

            	    }
            	    break;

            	default :
            	    break loop39;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleQualifiedName"


    // $ANTLR start "entryRulePogoMultiClasses"
    // InternalPogoDsl.g:1695:1: entryRulePogoMultiClasses returns [EObject current=null] : iv_rulePogoMultiClasses= rulePogoMultiClasses EOF ;
    public final EObject entryRulePogoMultiClasses() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePogoMultiClasses = null;


        try {
            // InternalPogoDsl.g:1695:57: (iv_rulePogoMultiClasses= rulePogoMultiClasses EOF )
            // InternalPogoDsl.g:1696:2: iv_rulePogoMultiClasses= rulePogoMultiClasses EOF
            {
             newCompositeNode(grammarAccess.getPogoMultiClassesRule()); 
            pushFollow(FOLLOW_1);
            iv_rulePogoMultiClasses=rulePogoMultiClasses();

            state._fsp--;

             current =iv_rulePogoMultiClasses; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePogoMultiClasses"


    // $ANTLR start "rulePogoMultiClasses"
    // InternalPogoDsl.g:1702:1: rulePogoMultiClasses returns [EObject current=null] : ( () otherlv_1= 'PogoMultiClasses' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'pogoRevision' ( (lv_pogoRevision_5_0= ruleEString ) ) )? (otherlv_6= 'sourcePath' ( (lv_sourcePath_7_0= ruleEString ) ) )? (otherlv_8= 'description' ( (lv_description_9_0= ruleEString ) ) )? (otherlv_10= 'title' ( (lv_title_11_0= ruleEString ) ) )? (otherlv_12= 'license' ( (lv_license_13_0= ruleEString ) ) )? (otherlv_14= 'filestogenerate' ( (lv_filestogenerate_15_0= ruleEString ) ) )? (otherlv_16= 'classes' otherlv_17= '{' ( (lv_classes_18_0= ruleOneClassSimpleDef ) ) (otherlv_19= ',' ( (lv_classes_20_0= ruleOneClassSimpleDef ) ) )* otherlv_21= '}' )? (otherlv_22= 'preferences' ( (lv_preferences_23_0= rulePreferences ) ) )? otherlv_24= '}' ) ;
    public final EObject rulePogoMultiClasses() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        Token otherlv_10=null;
        Token otherlv_12=null;
        Token otherlv_14=null;
        Token otherlv_16=null;
        Token otherlv_17=null;
        Token otherlv_19=null;
        Token otherlv_21=null;
        Token otherlv_22=null;
        Token otherlv_24=null;
        AntlrDatatypeRuleToken lv_name_2_0 = null;

        AntlrDatatypeRuleToken lv_pogoRevision_5_0 = null;

        AntlrDatatypeRuleToken lv_sourcePath_7_0 = null;

        AntlrDatatypeRuleToken lv_description_9_0 = null;

        AntlrDatatypeRuleToken lv_title_11_0 = null;

        AntlrDatatypeRuleToken lv_license_13_0 = null;

        AntlrDatatypeRuleToken lv_filestogenerate_15_0 = null;

        EObject lv_classes_18_0 = null;

        EObject lv_classes_20_0 = null;

        EObject lv_preferences_23_0 = null;



        	enterRule();

        try {
            // InternalPogoDsl.g:1708:2: ( ( () otherlv_1= 'PogoMultiClasses' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'pogoRevision' ( (lv_pogoRevision_5_0= ruleEString ) ) )? (otherlv_6= 'sourcePath' ( (lv_sourcePath_7_0= ruleEString ) ) )? (otherlv_8= 'description' ( (lv_description_9_0= ruleEString ) ) )? (otherlv_10= 'title' ( (lv_title_11_0= ruleEString ) ) )? (otherlv_12= 'license' ( (lv_license_13_0= ruleEString ) ) )? (otherlv_14= 'filestogenerate' ( (lv_filestogenerate_15_0= ruleEString ) ) )? (otherlv_16= 'classes' otherlv_17= '{' ( (lv_classes_18_0= ruleOneClassSimpleDef ) ) (otherlv_19= ',' ( (lv_classes_20_0= ruleOneClassSimpleDef ) ) )* otherlv_21= '}' )? (otherlv_22= 'preferences' ( (lv_preferences_23_0= rulePreferences ) ) )? otherlv_24= '}' ) )
            // InternalPogoDsl.g:1709:2: ( () otherlv_1= 'PogoMultiClasses' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'pogoRevision' ( (lv_pogoRevision_5_0= ruleEString ) ) )? (otherlv_6= 'sourcePath' ( (lv_sourcePath_7_0= ruleEString ) ) )? (otherlv_8= 'description' ( (lv_description_9_0= ruleEString ) ) )? (otherlv_10= 'title' ( (lv_title_11_0= ruleEString ) ) )? (otherlv_12= 'license' ( (lv_license_13_0= ruleEString ) ) )? (otherlv_14= 'filestogenerate' ( (lv_filestogenerate_15_0= ruleEString ) ) )? (otherlv_16= 'classes' otherlv_17= '{' ( (lv_classes_18_0= ruleOneClassSimpleDef ) ) (otherlv_19= ',' ( (lv_classes_20_0= ruleOneClassSimpleDef ) ) )* otherlv_21= '}' )? (otherlv_22= 'preferences' ( (lv_preferences_23_0= rulePreferences ) ) )? otherlv_24= '}' )
            {
            // InternalPogoDsl.g:1709:2: ( () otherlv_1= 'PogoMultiClasses' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'pogoRevision' ( (lv_pogoRevision_5_0= ruleEString ) ) )? (otherlv_6= 'sourcePath' ( (lv_sourcePath_7_0= ruleEString ) ) )? (otherlv_8= 'description' ( (lv_description_9_0= ruleEString ) ) )? (otherlv_10= 'title' ( (lv_title_11_0= ruleEString ) ) )? (otherlv_12= 'license' ( (lv_license_13_0= ruleEString ) ) )? (otherlv_14= 'filestogenerate' ( (lv_filestogenerate_15_0= ruleEString ) ) )? (otherlv_16= 'classes' otherlv_17= '{' ( (lv_classes_18_0= ruleOneClassSimpleDef ) ) (otherlv_19= ',' ( (lv_classes_20_0= ruleOneClassSimpleDef ) ) )* otherlv_21= '}' )? (otherlv_22= 'preferences' ( (lv_preferences_23_0= rulePreferences ) ) )? otherlv_24= '}' )
            // InternalPogoDsl.g:1710:3: () otherlv_1= 'PogoMultiClasses' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'pogoRevision' ( (lv_pogoRevision_5_0= ruleEString ) ) )? (otherlv_6= 'sourcePath' ( (lv_sourcePath_7_0= ruleEString ) ) )? (otherlv_8= 'description' ( (lv_description_9_0= ruleEString ) ) )? (otherlv_10= 'title' ( (lv_title_11_0= ruleEString ) ) )? (otherlv_12= 'license' ( (lv_license_13_0= ruleEString ) ) )? (otherlv_14= 'filestogenerate' ( (lv_filestogenerate_15_0= ruleEString ) ) )? (otherlv_16= 'classes' otherlv_17= '{' ( (lv_classes_18_0= ruleOneClassSimpleDef ) ) (otherlv_19= ',' ( (lv_classes_20_0= ruleOneClassSimpleDef ) ) )* otherlv_21= '}' )? (otherlv_22= 'preferences' ( (lv_preferences_23_0= rulePreferences ) ) )? otherlv_24= '}'
            {
            // InternalPogoDsl.g:1710:3: ()
            // InternalPogoDsl.g:1711:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getPogoMultiClassesAccess().getPogoMultiClassesAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,41,FOLLOW_13); 

            			newLeafNode(otherlv_1, grammarAccess.getPogoMultiClassesAccess().getPogoMultiClassesKeyword_1());
            		
            // InternalPogoDsl.g:1721:3: ( (lv_name_2_0= ruleEString ) )
            // InternalPogoDsl.g:1722:4: (lv_name_2_0= ruleEString )
            {
            // InternalPogoDsl.g:1722:4: (lv_name_2_0= ruleEString )
            // InternalPogoDsl.g:1723:5: lv_name_2_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getPogoMultiClassesAccess().getNameEStringParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_3);
            lv_name_2_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getPogoMultiClassesRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_2_0,
            						"com.mncml.implementation.pogo.dsl.PogoDsl.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_3=(Token)match(input,12,FOLLOW_45); 

            			newLeafNode(otherlv_3, grammarAccess.getPogoMultiClassesAccess().getLeftCurlyBracketKeyword_3());
            		
            // InternalPogoDsl.g:1744:3: (otherlv_4= 'pogoRevision' ( (lv_pogoRevision_5_0= ruleEString ) ) )?
            int alt40=2;
            int LA40_0 = input.LA(1);

            if ( (LA40_0==24) ) {
                alt40=1;
            }
            switch (alt40) {
                case 1 :
                    // InternalPogoDsl.g:1745:4: otherlv_4= 'pogoRevision' ( (lv_pogoRevision_5_0= ruleEString ) )
                    {
                    otherlv_4=(Token)match(input,24,FOLLOW_13); 

                    				newLeafNode(otherlv_4, grammarAccess.getPogoMultiClassesAccess().getPogoRevisionKeyword_4_0());
                    			
                    // InternalPogoDsl.g:1749:4: ( (lv_pogoRevision_5_0= ruleEString ) )
                    // InternalPogoDsl.g:1750:5: (lv_pogoRevision_5_0= ruleEString )
                    {
                    // InternalPogoDsl.g:1750:5: (lv_pogoRevision_5_0= ruleEString )
                    // InternalPogoDsl.g:1751:6: lv_pogoRevision_5_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getPogoMultiClassesAccess().getPogoRevisionEStringParserRuleCall_4_1_0());
                    					
                    pushFollow(FOLLOW_46);
                    lv_pogoRevision_5_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getPogoMultiClassesRule());
                    						}
                    						set(
                    							current,
                    							"pogoRevision",
                    							lv_pogoRevision_5_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalPogoDsl.g:1769:3: (otherlv_6= 'sourcePath' ( (lv_sourcePath_7_0= ruleEString ) ) )?
            int alt41=2;
            int LA41_0 = input.LA(1);

            if ( (LA41_0==42) ) {
                alt41=1;
            }
            switch (alt41) {
                case 1 :
                    // InternalPogoDsl.g:1770:4: otherlv_6= 'sourcePath' ( (lv_sourcePath_7_0= ruleEString ) )
                    {
                    otherlv_6=(Token)match(input,42,FOLLOW_13); 

                    				newLeafNode(otherlv_6, grammarAccess.getPogoMultiClassesAccess().getSourcePathKeyword_5_0());
                    			
                    // InternalPogoDsl.g:1774:4: ( (lv_sourcePath_7_0= ruleEString ) )
                    // InternalPogoDsl.g:1775:5: (lv_sourcePath_7_0= ruleEString )
                    {
                    // InternalPogoDsl.g:1775:5: (lv_sourcePath_7_0= ruleEString )
                    // InternalPogoDsl.g:1776:6: lv_sourcePath_7_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getPogoMultiClassesAccess().getSourcePathEStringParserRuleCall_5_1_0());
                    					
                    pushFollow(FOLLOW_47);
                    lv_sourcePath_7_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getPogoMultiClassesRule());
                    						}
                    						set(
                    							current,
                    							"sourcePath",
                    							lv_sourcePath_7_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalPogoDsl.g:1794:3: (otherlv_8= 'description' ( (lv_description_9_0= ruleEString ) ) )?
            int alt42=2;
            int LA42_0 = input.LA(1);

            if ( (LA42_0==27) ) {
                alt42=1;
            }
            switch (alt42) {
                case 1 :
                    // InternalPogoDsl.g:1795:4: otherlv_8= 'description' ( (lv_description_9_0= ruleEString ) )
                    {
                    otherlv_8=(Token)match(input,27,FOLLOW_13); 

                    				newLeafNode(otherlv_8, grammarAccess.getPogoMultiClassesAccess().getDescriptionKeyword_6_0());
                    			
                    // InternalPogoDsl.g:1799:4: ( (lv_description_9_0= ruleEString ) )
                    // InternalPogoDsl.g:1800:5: (lv_description_9_0= ruleEString )
                    {
                    // InternalPogoDsl.g:1800:5: (lv_description_9_0= ruleEString )
                    // InternalPogoDsl.g:1801:6: lv_description_9_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getPogoMultiClassesAccess().getDescriptionEStringParserRuleCall_6_1_0());
                    					
                    pushFollow(FOLLOW_48);
                    lv_description_9_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getPogoMultiClassesRule());
                    						}
                    						set(
                    							current,
                    							"description",
                    							lv_description_9_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalPogoDsl.g:1819:3: (otherlv_10= 'title' ( (lv_title_11_0= ruleEString ) ) )?
            int alt43=2;
            int LA43_0 = input.LA(1);

            if ( (LA43_0==43) ) {
                alt43=1;
            }
            switch (alt43) {
                case 1 :
                    // InternalPogoDsl.g:1820:4: otherlv_10= 'title' ( (lv_title_11_0= ruleEString ) )
                    {
                    otherlv_10=(Token)match(input,43,FOLLOW_13); 

                    				newLeafNode(otherlv_10, grammarAccess.getPogoMultiClassesAccess().getTitleKeyword_7_0());
                    			
                    // InternalPogoDsl.g:1824:4: ( (lv_title_11_0= ruleEString ) )
                    // InternalPogoDsl.g:1825:5: (lv_title_11_0= ruleEString )
                    {
                    // InternalPogoDsl.g:1825:5: (lv_title_11_0= ruleEString )
                    // InternalPogoDsl.g:1826:6: lv_title_11_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getPogoMultiClassesAccess().getTitleEStringParserRuleCall_7_1_0());
                    					
                    pushFollow(FOLLOW_49);
                    lv_title_11_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getPogoMultiClassesRule());
                    						}
                    						set(
                    							current,
                    							"title",
                    							lv_title_11_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalPogoDsl.g:1844:3: (otherlv_12= 'license' ( (lv_license_13_0= ruleEString ) ) )?
            int alt44=2;
            int LA44_0 = input.LA(1);

            if ( (LA44_0==44) ) {
                alt44=1;
            }
            switch (alt44) {
                case 1 :
                    // InternalPogoDsl.g:1845:4: otherlv_12= 'license' ( (lv_license_13_0= ruleEString ) )
                    {
                    otherlv_12=(Token)match(input,44,FOLLOW_13); 

                    				newLeafNode(otherlv_12, grammarAccess.getPogoMultiClassesAccess().getLicenseKeyword_8_0());
                    			
                    // InternalPogoDsl.g:1849:4: ( (lv_license_13_0= ruleEString ) )
                    // InternalPogoDsl.g:1850:5: (lv_license_13_0= ruleEString )
                    {
                    // InternalPogoDsl.g:1850:5: (lv_license_13_0= ruleEString )
                    // InternalPogoDsl.g:1851:6: lv_license_13_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getPogoMultiClassesAccess().getLicenseEStringParserRuleCall_8_1_0());
                    					
                    pushFollow(FOLLOW_50);
                    lv_license_13_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getPogoMultiClassesRule());
                    						}
                    						set(
                    							current,
                    							"license",
                    							lv_license_13_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalPogoDsl.g:1869:3: (otherlv_14= 'filestogenerate' ( (lv_filestogenerate_15_0= ruleEString ) ) )?
            int alt45=2;
            int LA45_0 = input.LA(1);

            if ( (LA45_0==45) ) {
                alt45=1;
            }
            switch (alt45) {
                case 1 :
                    // InternalPogoDsl.g:1870:4: otherlv_14= 'filestogenerate' ( (lv_filestogenerate_15_0= ruleEString ) )
                    {
                    otherlv_14=(Token)match(input,45,FOLLOW_13); 

                    				newLeafNode(otherlv_14, grammarAccess.getPogoMultiClassesAccess().getFilestogenerateKeyword_9_0());
                    			
                    // InternalPogoDsl.g:1874:4: ( (lv_filestogenerate_15_0= ruleEString ) )
                    // InternalPogoDsl.g:1875:5: (lv_filestogenerate_15_0= ruleEString )
                    {
                    // InternalPogoDsl.g:1875:5: (lv_filestogenerate_15_0= ruleEString )
                    // InternalPogoDsl.g:1876:6: lv_filestogenerate_15_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getPogoMultiClassesAccess().getFilestogenerateEStringParserRuleCall_9_1_0());
                    					
                    pushFollow(FOLLOW_51);
                    lv_filestogenerate_15_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getPogoMultiClassesRule());
                    						}
                    						set(
                    							current,
                    							"filestogenerate",
                    							lv_filestogenerate_15_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalPogoDsl.g:1894:3: (otherlv_16= 'classes' otherlv_17= '{' ( (lv_classes_18_0= ruleOneClassSimpleDef ) ) (otherlv_19= ',' ( (lv_classes_20_0= ruleOneClassSimpleDef ) ) )* otherlv_21= '}' )?
            int alt47=2;
            int LA47_0 = input.LA(1);

            if ( (LA47_0==16) ) {
                alt47=1;
            }
            switch (alt47) {
                case 1 :
                    // InternalPogoDsl.g:1895:4: otherlv_16= 'classes' otherlv_17= '{' ( (lv_classes_18_0= ruleOneClassSimpleDef ) ) (otherlv_19= ',' ( (lv_classes_20_0= ruleOneClassSimpleDef ) ) )* otherlv_21= '}'
                    {
                    otherlv_16=(Token)match(input,16,FOLLOW_3); 

                    				newLeafNode(otherlv_16, grammarAccess.getPogoMultiClassesAccess().getClassesKeyword_10_0());
                    			
                    otherlv_17=(Token)match(input,12,FOLLOW_52); 

                    				newLeafNode(otherlv_17, grammarAccess.getPogoMultiClassesAccess().getLeftCurlyBracketKeyword_10_1());
                    			
                    // InternalPogoDsl.g:1903:4: ( (lv_classes_18_0= ruleOneClassSimpleDef ) )
                    // InternalPogoDsl.g:1904:5: (lv_classes_18_0= ruleOneClassSimpleDef )
                    {
                    // InternalPogoDsl.g:1904:5: (lv_classes_18_0= ruleOneClassSimpleDef )
                    // InternalPogoDsl.g:1905:6: lv_classes_18_0= ruleOneClassSimpleDef
                    {

                    						newCompositeNode(grammarAccess.getPogoMultiClassesAccess().getClassesOneClassSimpleDefParserRuleCall_10_2_0());
                    					
                    pushFollow(FOLLOW_6);
                    lv_classes_18_0=ruleOneClassSimpleDef();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getPogoMultiClassesRule());
                    						}
                    						add(
                    							current,
                    							"classes",
                    							lv_classes_18_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.OneClassSimpleDef");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalPogoDsl.g:1922:4: (otherlv_19= ',' ( (lv_classes_20_0= ruleOneClassSimpleDef ) ) )*
                    loop46:
                    do {
                        int alt46=2;
                        int LA46_0 = input.LA(1);

                        if ( (LA46_0==14) ) {
                            alt46=1;
                        }


                        switch (alt46) {
                    	case 1 :
                    	    // InternalPogoDsl.g:1923:5: otherlv_19= ',' ( (lv_classes_20_0= ruleOneClassSimpleDef ) )
                    	    {
                    	    otherlv_19=(Token)match(input,14,FOLLOW_52); 

                    	    					newLeafNode(otherlv_19, grammarAccess.getPogoMultiClassesAccess().getCommaKeyword_10_3_0());
                    	    				
                    	    // InternalPogoDsl.g:1927:5: ( (lv_classes_20_0= ruleOneClassSimpleDef ) )
                    	    // InternalPogoDsl.g:1928:6: (lv_classes_20_0= ruleOneClassSimpleDef )
                    	    {
                    	    // InternalPogoDsl.g:1928:6: (lv_classes_20_0= ruleOneClassSimpleDef )
                    	    // InternalPogoDsl.g:1929:7: lv_classes_20_0= ruleOneClassSimpleDef
                    	    {

                    	    							newCompositeNode(grammarAccess.getPogoMultiClassesAccess().getClassesOneClassSimpleDefParserRuleCall_10_3_1_0());
                    	    						
                    	    pushFollow(FOLLOW_6);
                    	    lv_classes_20_0=ruleOneClassSimpleDef();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getPogoMultiClassesRule());
                    	    							}
                    	    							add(
                    	    								current,
                    	    								"classes",
                    	    								lv_classes_20_0,
                    	    								"com.mncml.implementation.pogo.dsl.PogoDsl.OneClassSimpleDef");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop46;
                        }
                    } while (true);

                    otherlv_21=(Token)match(input,15,FOLLOW_53); 

                    				newLeafNode(otherlv_21, grammarAccess.getPogoMultiClassesAccess().getRightCurlyBracketKeyword_10_4());
                    			

                    }
                    break;

            }

            // InternalPogoDsl.g:1952:3: (otherlv_22= 'preferences' ( (lv_preferences_23_0= rulePreferences ) ) )?
            int alt48=2;
            int LA48_0 = input.LA(1);

            if ( (LA48_0==37) ) {
                alt48=1;
            }
            switch (alt48) {
                case 1 :
                    // InternalPogoDsl.g:1953:4: otherlv_22= 'preferences' ( (lv_preferences_23_0= rulePreferences ) )
                    {
                    otherlv_22=(Token)match(input,37,FOLLOW_39); 

                    				newLeafNode(otherlv_22, grammarAccess.getPogoMultiClassesAccess().getPreferencesKeyword_11_0());
                    			
                    // InternalPogoDsl.g:1957:4: ( (lv_preferences_23_0= rulePreferences ) )
                    // InternalPogoDsl.g:1958:5: (lv_preferences_23_0= rulePreferences )
                    {
                    // InternalPogoDsl.g:1958:5: (lv_preferences_23_0= rulePreferences )
                    // InternalPogoDsl.g:1959:6: lv_preferences_23_0= rulePreferences
                    {

                    						newCompositeNode(grammarAccess.getPogoMultiClassesAccess().getPreferencesPreferencesParserRuleCall_11_1_0());
                    					
                    pushFollow(FOLLOW_11);
                    lv_preferences_23_0=rulePreferences();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getPogoMultiClassesRule());
                    						}
                    						set(
                    							current,
                    							"preferences",
                    							lv_preferences_23_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.Preferences");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            otherlv_24=(Token)match(input,15,FOLLOW_2); 

            			newLeafNode(otherlv_24, grammarAccess.getPogoMultiClassesAccess().getRightCurlyBracketKeyword_12());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePogoMultiClasses"


    // $ANTLR start "entryRuleEString"
    // InternalPogoDsl.g:1985:1: entryRuleEString returns [String current=null] : iv_ruleEString= ruleEString EOF ;
    public final String entryRuleEString() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleEString = null;


        try {
            // InternalPogoDsl.g:1985:47: (iv_ruleEString= ruleEString EOF )
            // InternalPogoDsl.g:1986:2: iv_ruleEString= ruleEString EOF
            {
             newCompositeNode(grammarAccess.getEStringRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleEString=ruleEString();

            state._fsp--;

             current =iv_ruleEString.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEString"


    // $ANTLR start "ruleEString"
    // InternalPogoDsl.g:1992:1: ruleEString returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID ) ;
    public final AntlrDatatypeRuleToken ruleEString() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_STRING_0=null;
        Token this_ID_1=null;


        	enterRule();

        try {
            // InternalPogoDsl.g:1998:2: ( (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID ) )
            // InternalPogoDsl.g:1999:2: (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID )
            {
            // InternalPogoDsl.g:1999:2: (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID )
            int alt49=2;
            int LA49_0 = input.LA(1);

            if ( (LA49_0==RULE_STRING) ) {
                alt49=1;
            }
            else if ( (LA49_0==RULE_ID) ) {
                alt49=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 49, 0, input);

                throw nvae;
            }
            switch (alt49) {
                case 1 :
                    // InternalPogoDsl.g:2000:3: this_STRING_0= RULE_STRING
                    {
                    this_STRING_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

                    			current.merge(this_STRING_0);
                    		

                    			newLeafNode(this_STRING_0, grammarAccess.getEStringAccess().getSTRINGTerminalRuleCall_0());
                    		

                    }
                    break;
                case 2 :
                    // InternalPogoDsl.g:2008:3: this_ID_1= RULE_ID
                    {
                    this_ID_1=(Token)match(input,RULE_ID,FOLLOW_2); 

                    			current.merge(this_ID_1);
                    		

                    			newLeafNode(this_ID_1, grammarAccess.getEStringAccess().getIDTerminalRuleCall_1());
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEString"


    // $ANTLR start "entryRuleClassDescription"
    // InternalPogoDsl.g:2019:1: entryRuleClassDescription returns [EObject current=null] : iv_ruleClassDescription= ruleClassDescription EOF ;
    public final EObject entryRuleClassDescription() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleClassDescription = null;


        try {
            // InternalPogoDsl.g:2019:57: (iv_ruleClassDescription= ruleClassDescription EOF )
            // InternalPogoDsl.g:2020:2: iv_ruleClassDescription= ruleClassDescription EOF
            {
             newCompositeNode(grammarAccess.getClassDescriptionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleClassDescription=ruleClassDescription();

            state._fsp--;

             current =iv_ruleClassDescription; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleClassDescription"


    // $ANTLR start "ruleClassDescription"
    // InternalPogoDsl.g:2026:1: ruleClassDescription returns [EObject current=null] : ( () otherlv_1= 'ClassDescription' otherlv_2= '{' (otherlv_3= 'description' ( (lv_description_4_0= ruleEString ) ) )? (otherlv_5= 'title' ( (lv_title_6_0= ruleEString ) ) )? (otherlv_7= 'sourcePath' ( (lv_sourcePath_8_0= ruleEString ) ) )? (otherlv_9= 'language' ( (lv_language_10_0= ruleLanguage ) ) )? (otherlv_11= 'filestogenerate' ( (lv_filestogenerate_12_0= ruleEString ) ) )? (otherlv_13= 'license' ( (lv_license_14_0= ruleEString ) ) )? ( (otherlv_15= 'hasMandatoryProperty' ( (lv_hasMandatoryProperty_16_0= ruleBoolean ) ) ) | ruleQualifiedName )? ( (otherlv_18= 'hasConcreteProperty' ( (lv_hasConcreteProperty_19_0= ruleBoolean ) ) ) | ruleQualifiedName )? ( (otherlv_21= 'hasAbstractCommand' ( (lv_hasAbstractCommand_22_0= ruleBoolean ) ) ) | ruleQualifiedName )? ( (otherlv_24= 'hasAbstractAttribute' ( (lv_hasAbstractAttribute_25_0= ruleBoolean ) ) ) | ruleQualifiedName )? ( (otherlv_27= 'descriptionHtmlExists' ( (lv_descriptionHtmlExists_28_0= ruleBoolean ) ) ) | ruleQualifiedName )? (otherlv_30= 'inheritances' otherlv_31= '{' ( (lv_inheritances_32_0= ruleInheritance ) ) (otherlv_33= ',' ( (lv_inheritances_34_0= ruleInheritance ) ) )* otherlv_35= '}' )? (otherlv_36= 'identification' ( (lv_identification_37_0= ruleClassIdentification ) ) )? (otherlv_38= 'comments' ( (lv_comments_39_0= ruleComments ) ) )? otherlv_40= '}' ) ;
    public final EObject ruleClassDescription() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        Token otherlv_11=null;
        Token otherlv_13=null;
        Token otherlv_15=null;
        Token otherlv_18=null;
        Token otherlv_21=null;
        Token otherlv_24=null;
        Token otherlv_27=null;
        Token otherlv_30=null;
        Token otherlv_31=null;
        Token otherlv_33=null;
        Token otherlv_35=null;
        Token otherlv_36=null;
        Token otherlv_38=null;
        Token otherlv_40=null;
        AntlrDatatypeRuleToken lv_description_4_0 = null;

        AntlrDatatypeRuleToken lv_title_6_0 = null;

        AntlrDatatypeRuleToken lv_sourcePath_8_0 = null;

        AntlrDatatypeRuleToken lv_language_10_0 = null;

        AntlrDatatypeRuleToken lv_filestogenerate_12_0 = null;

        AntlrDatatypeRuleToken lv_license_14_0 = null;

        AntlrDatatypeRuleToken lv_hasMandatoryProperty_16_0 = null;

        AntlrDatatypeRuleToken lv_hasConcreteProperty_19_0 = null;

        AntlrDatatypeRuleToken lv_hasAbstractCommand_22_0 = null;

        AntlrDatatypeRuleToken lv_hasAbstractAttribute_25_0 = null;

        AntlrDatatypeRuleToken lv_descriptionHtmlExists_28_0 = null;

        EObject lv_inheritances_32_0 = null;

        EObject lv_inheritances_34_0 = null;

        EObject lv_identification_37_0 = null;

        EObject lv_comments_39_0 = null;



        	enterRule();

        try {
            // InternalPogoDsl.g:2032:2: ( ( () otherlv_1= 'ClassDescription' otherlv_2= '{' (otherlv_3= 'description' ( (lv_description_4_0= ruleEString ) ) )? (otherlv_5= 'title' ( (lv_title_6_0= ruleEString ) ) )? (otherlv_7= 'sourcePath' ( (lv_sourcePath_8_0= ruleEString ) ) )? (otherlv_9= 'language' ( (lv_language_10_0= ruleLanguage ) ) )? (otherlv_11= 'filestogenerate' ( (lv_filestogenerate_12_0= ruleEString ) ) )? (otherlv_13= 'license' ( (lv_license_14_0= ruleEString ) ) )? ( (otherlv_15= 'hasMandatoryProperty' ( (lv_hasMandatoryProperty_16_0= ruleBoolean ) ) ) | ruleQualifiedName )? ( (otherlv_18= 'hasConcreteProperty' ( (lv_hasConcreteProperty_19_0= ruleBoolean ) ) ) | ruleQualifiedName )? ( (otherlv_21= 'hasAbstractCommand' ( (lv_hasAbstractCommand_22_0= ruleBoolean ) ) ) | ruleQualifiedName )? ( (otherlv_24= 'hasAbstractAttribute' ( (lv_hasAbstractAttribute_25_0= ruleBoolean ) ) ) | ruleQualifiedName )? ( (otherlv_27= 'descriptionHtmlExists' ( (lv_descriptionHtmlExists_28_0= ruleBoolean ) ) ) | ruleQualifiedName )? (otherlv_30= 'inheritances' otherlv_31= '{' ( (lv_inheritances_32_0= ruleInheritance ) ) (otherlv_33= ',' ( (lv_inheritances_34_0= ruleInheritance ) ) )* otherlv_35= '}' )? (otherlv_36= 'identification' ( (lv_identification_37_0= ruleClassIdentification ) ) )? (otherlv_38= 'comments' ( (lv_comments_39_0= ruleComments ) ) )? otherlv_40= '}' ) )
            // InternalPogoDsl.g:2033:2: ( () otherlv_1= 'ClassDescription' otherlv_2= '{' (otherlv_3= 'description' ( (lv_description_4_0= ruleEString ) ) )? (otherlv_5= 'title' ( (lv_title_6_0= ruleEString ) ) )? (otherlv_7= 'sourcePath' ( (lv_sourcePath_8_0= ruleEString ) ) )? (otherlv_9= 'language' ( (lv_language_10_0= ruleLanguage ) ) )? (otherlv_11= 'filestogenerate' ( (lv_filestogenerate_12_0= ruleEString ) ) )? (otherlv_13= 'license' ( (lv_license_14_0= ruleEString ) ) )? ( (otherlv_15= 'hasMandatoryProperty' ( (lv_hasMandatoryProperty_16_0= ruleBoolean ) ) ) | ruleQualifiedName )? ( (otherlv_18= 'hasConcreteProperty' ( (lv_hasConcreteProperty_19_0= ruleBoolean ) ) ) | ruleQualifiedName )? ( (otherlv_21= 'hasAbstractCommand' ( (lv_hasAbstractCommand_22_0= ruleBoolean ) ) ) | ruleQualifiedName )? ( (otherlv_24= 'hasAbstractAttribute' ( (lv_hasAbstractAttribute_25_0= ruleBoolean ) ) ) | ruleQualifiedName )? ( (otherlv_27= 'descriptionHtmlExists' ( (lv_descriptionHtmlExists_28_0= ruleBoolean ) ) ) | ruleQualifiedName )? (otherlv_30= 'inheritances' otherlv_31= '{' ( (lv_inheritances_32_0= ruleInheritance ) ) (otherlv_33= ',' ( (lv_inheritances_34_0= ruleInheritance ) ) )* otherlv_35= '}' )? (otherlv_36= 'identification' ( (lv_identification_37_0= ruleClassIdentification ) ) )? (otherlv_38= 'comments' ( (lv_comments_39_0= ruleComments ) ) )? otherlv_40= '}' )
            {
            // InternalPogoDsl.g:2033:2: ( () otherlv_1= 'ClassDescription' otherlv_2= '{' (otherlv_3= 'description' ( (lv_description_4_0= ruleEString ) ) )? (otherlv_5= 'title' ( (lv_title_6_0= ruleEString ) ) )? (otherlv_7= 'sourcePath' ( (lv_sourcePath_8_0= ruleEString ) ) )? (otherlv_9= 'language' ( (lv_language_10_0= ruleLanguage ) ) )? (otherlv_11= 'filestogenerate' ( (lv_filestogenerate_12_0= ruleEString ) ) )? (otherlv_13= 'license' ( (lv_license_14_0= ruleEString ) ) )? ( (otherlv_15= 'hasMandatoryProperty' ( (lv_hasMandatoryProperty_16_0= ruleBoolean ) ) ) | ruleQualifiedName )? ( (otherlv_18= 'hasConcreteProperty' ( (lv_hasConcreteProperty_19_0= ruleBoolean ) ) ) | ruleQualifiedName )? ( (otherlv_21= 'hasAbstractCommand' ( (lv_hasAbstractCommand_22_0= ruleBoolean ) ) ) | ruleQualifiedName )? ( (otherlv_24= 'hasAbstractAttribute' ( (lv_hasAbstractAttribute_25_0= ruleBoolean ) ) ) | ruleQualifiedName )? ( (otherlv_27= 'descriptionHtmlExists' ( (lv_descriptionHtmlExists_28_0= ruleBoolean ) ) ) | ruleQualifiedName )? (otherlv_30= 'inheritances' otherlv_31= '{' ( (lv_inheritances_32_0= ruleInheritance ) ) (otherlv_33= ',' ( (lv_inheritances_34_0= ruleInheritance ) ) )* otherlv_35= '}' )? (otherlv_36= 'identification' ( (lv_identification_37_0= ruleClassIdentification ) ) )? (otherlv_38= 'comments' ( (lv_comments_39_0= ruleComments ) ) )? otherlv_40= '}' )
            // InternalPogoDsl.g:2034:3: () otherlv_1= 'ClassDescription' otherlv_2= '{' (otherlv_3= 'description' ( (lv_description_4_0= ruleEString ) ) )? (otherlv_5= 'title' ( (lv_title_6_0= ruleEString ) ) )? (otherlv_7= 'sourcePath' ( (lv_sourcePath_8_0= ruleEString ) ) )? (otherlv_9= 'language' ( (lv_language_10_0= ruleLanguage ) ) )? (otherlv_11= 'filestogenerate' ( (lv_filestogenerate_12_0= ruleEString ) ) )? (otherlv_13= 'license' ( (lv_license_14_0= ruleEString ) ) )? ( (otherlv_15= 'hasMandatoryProperty' ( (lv_hasMandatoryProperty_16_0= ruleBoolean ) ) ) | ruleQualifiedName )? ( (otherlv_18= 'hasConcreteProperty' ( (lv_hasConcreteProperty_19_0= ruleBoolean ) ) ) | ruleQualifiedName )? ( (otherlv_21= 'hasAbstractCommand' ( (lv_hasAbstractCommand_22_0= ruleBoolean ) ) ) | ruleQualifiedName )? ( (otherlv_24= 'hasAbstractAttribute' ( (lv_hasAbstractAttribute_25_0= ruleBoolean ) ) ) | ruleQualifiedName )? ( (otherlv_27= 'descriptionHtmlExists' ( (lv_descriptionHtmlExists_28_0= ruleBoolean ) ) ) | ruleQualifiedName )? (otherlv_30= 'inheritances' otherlv_31= '{' ( (lv_inheritances_32_0= ruleInheritance ) ) (otherlv_33= ',' ( (lv_inheritances_34_0= ruleInheritance ) ) )* otherlv_35= '}' )? (otherlv_36= 'identification' ( (lv_identification_37_0= ruleClassIdentification ) ) )? (otherlv_38= 'comments' ( (lv_comments_39_0= ruleComments ) ) )? otherlv_40= '}'
            {
            // InternalPogoDsl.g:2034:3: ()
            // InternalPogoDsl.g:2035:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getClassDescriptionAccess().getClassDescriptionAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,46,FOLLOW_3); 

            			newLeafNode(otherlv_1, grammarAccess.getClassDescriptionAccess().getClassDescriptionKeyword_1());
            		
            otherlv_2=(Token)match(input,12,FOLLOW_54); 

            			newLeafNode(otherlv_2, grammarAccess.getClassDescriptionAccess().getLeftCurlyBracketKeyword_2());
            		
            // InternalPogoDsl.g:2049:3: (otherlv_3= 'description' ( (lv_description_4_0= ruleEString ) ) )?
            int alt50=2;
            int LA50_0 = input.LA(1);

            if ( (LA50_0==27) ) {
                alt50=1;
            }
            switch (alt50) {
                case 1 :
                    // InternalPogoDsl.g:2050:4: otherlv_3= 'description' ( (lv_description_4_0= ruleEString ) )
                    {
                    otherlv_3=(Token)match(input,27,FOLLOW_13); 

                    				newLeafNode(otherlv_3, grammarAccess.getClassDescriptionAccess().getDescriptionKeyword_3_0());
                    			
                    // InternalPogoDsl.g:2054:4: ( (lv_description_4_0= ruleEString ) )
                    // InternalPogoDsl.g:2055:5: (lv_description_4_0= ruleEString )
                    {
                    // InternalPogoDsl.g:2055:5: (lv_description_4_0= ruleEString )
                    // InternalPogoDsl.g:2056:6: lv_description_4_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getClassDescriptionAccess().getDescriptionEStringParserRuleCall_3_1_0());
                    					
                    pushFollow(FOLLOW_55);
                    lv_description_4_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getClassDescriptionRule());
                    						}
                    						set(
                    							current,
                    							"description",
                    							lv_description_4_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalPogoDsl.g:2074:3: (otherlv_5= 'title' ( (lv_title_6_0= ruleEString ) ) )?
            int alt51=2;
            int LA51_0 = input.LA(1);

            if ( (LA51_0==43) ) {
                alt51=1;
            }
            switch (alt51) {
                case 1 :
                    // InternalPogoDsl.g:2075:4: otherlv_5= 'title' ( (lv_title_6_0= ruleEString ) )
                    {
                    otherlv_5=(Token)match(input,43,FOLLOW_13); 

                    				newLeafNode(otherlv_5, grammarAccess.getClassDescriptionAccess().getTitleKeyword_4_0());
                    			
                    // InternalPogoDsl.g:2079:4: ( (lv_title_6_0= ruleEString ) )
                    // InternalPogoDsl.g:2080:5: (lv_title_6_0= ruleEString )
                    {
                    // InternalPogoDsl.g:2080:5: (lv_title_6_0= ruleEString )
                    // InternalPogoDsl.g:2081:6: lv_title_6_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getClassDescriptionAccess().getTitleEStringParserRuleCall_4_1_0());
                    					
                    pushFollow(FOLLOW_56);
                    lv_title_6_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getClassDescriptionRule());
                    						}
                    						set(
                    							current,
                    							"title",
                    							lv_title_6_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalPogoDsl.g:2099:3: (otherlv_7= 'sourcePath' ( (lv_sourcePath_8_0= ruleEString ) ) )?
            int alt52=2;
            int LA52_0 = input.LA(1);

            if ( (LA52_0==42) ) {
                alt52=1;
            }
            switch (alt52) {
                case 1 :
                    // InternalPogoDsl.g:2100:4: otherlv_7= 'sourcePath' ( (lv_sourcePath_8_0= ruleEString ) )
                    {
                    otherlv_7=(Token)match(input,42,FOLLOW_13); 

                    				newLeafNode(otherlv_7, grammarAccess.getClassDescriptionAccess().getSourcePathKeyword_5_0());
                    			
                    // InternalPogoDsl.g:2104:4: ( (lv_sourcePath_8_0= ruleEString ) )
                    // InternalPogoDsl.g:2105:5: (lv_sourcePath_8_0= ruleEString )
                    {
                    // InternalPogoDsl.g:2105:5: (lv_sourcePath_8_0= ruleEString )
                    // InternalPogoDsl.g:2106:6: lv_sourcePath_8_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getClassDescriptionAccess().getSourcePathEStringParserRuleCall_5_1_0());
                    					
                    pushFollow(FOLLOW_57);
                    lv_sourcePath_8_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getClassDescriptionRule());
                    						}
                    						set(
                    							current,
                    							"sourcePath",
                    							lv_sourcePath_8_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalPogoDsl.g:2124:3: (otherlv_9= 'language' ( (lv_language_10_0= ruleLanguage ) ) )?
            int alt53=2;
            int LA53_0 = input.LA(1);

            if ( (LA53_0==47) ) {
                alt53=1;
            }
            switch (alt53) {
                case 1 :
                    // InternalPogoDsl.g:2125:4: otherlv_9= 'language' ( (lv_language_10_0= ruleLanguage ) )
                    {
                    otherlv_9=(Token)match(input,47,FOLLOW_58); 

                    				newLeafNode(otherlv_9, grammarAccess.getClassDescriptionAccess().getLanguageKeyword_6_0());
                    			
                    // InternalPogoDsl.g:2129:4: ( (lv_language_10_0= ruleLanguage ) )
                    // InternalPogoDsl.g:2130:5: (lv_language_10_0= ruleLanguage )
                    {
                    // InternalPogoDsl.g:2130:5: (lv_language_10_0= ruleLanguage )
                    // InternalPogoDsl.g:2131:6: lv_language_10_0= ruleLanguage
                    {

                    						newCompositeNode(grammarAccess.getClassDescriptionAccess().getLanguageLanguageParserRuleCall_6_1_0());
                    					
                    pushFollow(FOLLOW_59);
                    lv_language_10_0=ruleLanguage();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getClassDescriptionRule());
                    						}
                    						set(
                    							current,
                    							"language",
                    							lv_language_10_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.Language");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalPogoDsl.g:2149:3: (otherlv_11= 'filestogenerate' ( (lv_filestogenerate_12_0= ruleEString ) ) )?
            int alt54=2;
            int LA54_0 = input.LA(1);

            if ( (LA54_0==45) ) {
                alt54=1;
            }
            switch (alt54) {
                case 1 :
                    // InternalPogoDsl.g:2150:4: otherlv_11= 'filestogenerate' ( (lv_filestogenerate_12_0= ruleEString ) )
                    {
                    otherlv_11=(Token)match(input,45,FOLLOW_13); 

                    				newLeafNode(otherlv_11, grammarAccess.getClassDescriptionAccess().getFilestogenerateKeyword_7_0());
                    			
                    // InternalPogoDsl.g:2154:4: ( (lv_filestogenerate_12_0= ruleEString ) )
                    // InternalPogoDsl.g:2155:5: (lv_filestogenerate_12_0= ruleEString )
                    {
                    // InternalPogoDsl.g:2155:5: (lv_filestogenerate_12_0= ruleEString )
                    // InternalPogoDsl.g:2156:6: lv_filestogenerate_12_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getClassDescriptionAccess().getFilestogenerateEStringParserRuleCall_7_1_0());
                    					
                    pushFollow(FOLLOW_60);
                    lv_filestogenerate_12_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getClassDescriptionRule());
                    						}
                    						set(
                    							current,
                    							"filestogenerate",
                    							lv_filestogenerate_12_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalPogoDsl.g:2174:3: (otherlv_13= 'license' ( (lv_license_14_0= ruleEString ) ) )?
            int alt55=2;
            int LA55_0 = input.LA(1);

            if ( (LA55_0==44) ) {
                alt55=1;
            }
            switch (alt55) {
                case 1 :
                    // InternalPogoDsl.g:2175:4: otherlv_13= 'license' ( (lv_license_14_0= ruleEString ) )
                    {
                    otherlv_13=(Token)match(input,44,FOLLOW_13); 

                    				newLeafNode(otherlv_13, grammarAccess.getClassDescriptionAccess().getLicenseKeyword_8_0());
                    			
                    // InternalPogoDsl.g:2179:4: ( (lv_license_14_0= ruleEString ) )
                    // InternalPogoDsl.g:2180:5: (lv_license_14_0= ruleEString )
                    {
                    // InternalPogoDsl.g:2180:5: (lv_license_14_0= ruleEString )
                    // InternalPogoDsl.g:2181:6: lv_license_14_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getClassDescriptionAccess().getLicenseEStringParserRuleCall_8_1_0());
                    					
                    pushFollow(FOLLOW_61);
                    lv_license_14_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getClassDescriptionRule());
                    						}
                    						set(
                    							current,
                    							"license",
                    							lv_license_14_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalPogoDsl.g:2199:3: ( (otherlv_15= 'hasMandatoryProperty' ( (lv_hasMandatoryProperty_16_0= ruleBoolean ) ) ) | ruleQualifiedName )?
            int alt56=3;
            int LA56_0 = input.LA(1);

            if ( (LA56_0==48) ) {
                alt56=1;
            }
            else if ( (LA56_0==RULE_ID) ) {
                alt56=2;
            }
            switch (alt56) {
                case 1 :
                    // InternalPogoDsl.g:2200:4: (otherlv_15= 'hasMandatoryProperty' ( (lv_hasMandatoryProperty_16_0= ruleBoolean ) ) )
                    {
                    // InternalPogoDsl.g:2200:4: (otherlv_15= 'hasMandatoryProperty' ( (lv_hasMandatoryProperty_16_0= ruleBoolean ) ) )
                    // InternalPogoDsl.g:2201:5: otherlv_15= 'hasMandatoryProperty' ( (lv_hasMandatoryProperty_16_0= ruleBoolean ) )
                    {
                    otherlv_15=(Token)match(input,48,FOLLOW_62); 

                    					newLeafNode(otherlv_15, grammarAccess.getClassDescriptionAccess().getHasMandatoryPropertyKeyword_9_0_0());
                    				
                    // InternalPogoDsl.g:2205:5: ( (lv_hasMandatoryProperty_16_0= ruleBoolean ) )
                    // InternalPogoDsl.g:2206:6: (lv_hasMandatoryProperty_16_0= ruleBoolean )
                    {
                    // InternalPogoDsl.g:2206:6: (lv_hasMandatoryProperty_16_0= ruleBoolean )
                    // InternalPogoDsl.g:2207:7: lv_hasMandatoryProperty_16_0= ruleBoolean
                    {

                    							newCompositeNode(grammarAccess.getClassDescriptionAccess().getHasMandatoryPropertyBooleanParserRuleCall_9_0_1_0());
                    						
                    pushFollow(FOLLOW_63);
                    lv_hasMandatoryProperty_16_0=ruleBoolean();

                    state._fsp--;


                    							if (current==null) {
                    								current = createModelElementForParent(grammarAccess.getClassDescriptionRule());
                    							}
                    							set(
                    								current,
                    								"hasMandatoryProperty",
                    								lv_hasMandatoryProperty_16_0,
                    								"com.mncml.implementation.pogo.dsl.PogoDsl.Boolean");
                    							afterParserOrEnumRuleCall();
                    						

                    }


                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalPogoDsl.g:2226:4: ruleQualifiedName
                    {

                    				newCompositeNode(grammarAccess.getClassDescriptionAccess().getQualifiedNameParserRuleCall_9_1());
                    			
                    pushFollow(FOLLOW_63);
                    ruleQualifiedName();

                    state._fsp--;


                    				afterParserOrEnumRuleCall();
                    			

                    }
                    break;

            }

            // InternalPogoDsl.g:2234:3: ( (otherlv_18= 'hasConcreteProperty' ( (lv_hasConcreteProperty_19_0= ruleBoolean ) ) ) | ruleQualifiedName )?
            int alt57=3;
            int LA57_0 = input.LA(1);

            if ( (LA57_0==49) ) {
                alt57=1;
            }
            else if ( (LA57_0==RULE_ID) ) {
                alt57=2;
            }
            switch (alt57) {
                case 1 :
                    // InternalPogoDsl.g:2235:4: (otherlv_18= 'hasConcreteProperty' ( (lv_hasConcreteProperty_19_0= ruleBoolean ) ) )
                    {
                    // InternalPogoDsl.g:2235:4: (otherlv_18= 'hasConcreteProperty' ( (lv_hasConcreteProperty_19_0= ruleBoolean ) ) )
                    // InternalPogoDsl.g:2236:5: otherlv_18= 'hasConcreteProperty' ( (lv_hasConcreteProperty_19_0= ruleBoolean ) )
                    {
                    otherlv_18=(Token)match(input,49,FOLLOW_62); 

                    					newLeafNode(otherlv_18, grammarAccess.getClassDescriptionAccess().getHasConcretePropertyKeyword_10_0_0());
                    				
                    // InternalPogoDsl.g:2240:5: ( (lv_hasConcreteProperty_19_0= ruleBoolean ) )
                    // InternalPogoDsl.g:2241:6: (lv_hasConcreteProperty_19_0= ruleBoolean )
                    {
                    // InternalPogoDsl.g:2241:6: (lv_hasConcreteProperty_19_0= ruleBoolean )
                    // InternalPogoDsl.g:2242:7: lv_hasConcreteProperty_19_0= ruleBoolean
                    {

                    							newCompositeNode(grammarAccess.getClassDescriptionAccess().getHasConcretePropertyBooleanParserRuleCall_10_0_1_0());
                    						
                    pushFollow(FOLLOW_64);
                    lv_hasConcreteProperty_19_0=ruleBoolean();

                    state._fsp--;


                    							if (current==null) {
                    								current = createModelElementForParent(grammarAccess.getClassDescriptionRule());
                    							}
                    							set(
                    								current,
                    								"hasConcreteProperty",
                    								lv_hasConcreteProperty_19_0,
                    								"com.mncml.implementation.pogo.dsl.PogoDsl.Boolean");
                    							afterParserOrEnumRuleCall();
                    						

                    }


                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalPogoDsl.g:2261:4: ruleQualifiedName
                    {

                    				newCompositeNode(grammarAccess.getClassDescriptionAccess().getQualifiedNameParserRuleCall_10_1());
                    			
                    pushFollow(FOLLOW_64);
                    ruleQualifiedName();

                    state._fsp--;


                    				afterParserOrEnumRuleCall();
                    			

                    }
                    break;

            }

            // InternalPogoDsl.g:2269:3: ( (otherlv_21= 'hasAbstractCommand' ( (lv_hasAbstractCommand_22_0= ruleBoolean ) ) ) | ruleQualifiedName )?
            int alt58=3;
            int LA58_0 = input.LA(1);

            if ( (LA58_0==50) ) {
                alt58=1;
            }
            else if ( (LA58_0==RULE_ID) ) {
                alt58=2;
            }
            switch (alt58) {
                case 1 :
                    // InternalPogoDsl.g:2270:4: (otherlv_21= 'hasAbstractCommand' ( (lv_hasAbstractCommand_22_0= ruleBoolean ) ) )
                    {
                    // InternalPogoDsl.g:2270:4: (otherlv_21= 'hasAbstractCommand' ( (lv_hasAbstractCommand_22_0= ruleBoolean ) ) )
                    // InternalPogoDsl.g:2271:5: otherlv_21= 'hasAbstractCommand' ( (lv_hasAbstractCommand_22_0= ruleBoolean ) )
                    {
                    otherlv_21=(Token)match(input,50,FOLLOW_62); 

                    					newLeafNode(otherlv_21, grammarAccess.getClassDescriptionAccess().getHasAbstractCommandKeyword_11_0_0());
                    				
                    // InternalPogoDsl.g:2275:5: ( (lv_hasAbstractCommand_22_0= ruleBoolean ) )
                    // InternalPogoDsl.g:2276:6: (lv_hasAbstractCommand_22_0= ruleBoolean )
                    {
                    // InternalPogoDsl.g:2276:6: (lv_hasAbstractCommand_22_0= ruleBoolean )
                    // InternalPogoDsl.g:2277:7: lv_hasAbstractCommand_22_0= ruleBoolean
                    {

                    							newCompositeNode(grammarAccess.getClassDescriptionAccess().getHasAbstractCommandBooleanParserRuleCall_11_0_1_0());
                    						
                    pushFollow(FOLLOW_65);
                    lv_hasAbstractCommand_22_0=ruleBoolean();

                    state._fsp--;


                    							if (current==null) {
                    								current = createModelElementForParent(grammarAccess.getClassDescriptionRule());
                    							}
                    							set(
                    								current,
                    								"hasAbstractCommand",
                    								lv_hasAbstractCommand_22_0,
                    								"com.mncml.implementation.pogo.dsl.PogoDsl.Boolean");
                    							afterParserOrEnumRuleCall();
                    						

                    }


                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalPogoDsl.g:2296:4: ruleQualifiedName
                    {

                    				newCompositeNode(grammarAccess.getClassDescriptionAccess().getQualifiedNameParserRuleCall_11_1());
                    			
                    pushFollow(FOLLOW_65);
                    ruleQualifiedName();

                    state._fsp--;


                    				afterParserOrEnumRuleCall();
                    			

                    }
                    break;

            }

            // InternalPogoDsl.g:2304:3: ( (otherlv_24= 'hasAbstractAttribute' ( (lv_hasAbstractAttribute_25_0= ruleBoolean ) ) ) | ruleQualifiedName )?
            int alt59=3;
            int LA59_0 = input.LA(1);

            if ( (LA59_0==51) ) {
                alt59=1;
            }
            else if ( (LA59_0==RULE_ID) ) {
                alt59=2;
            }
            switch (alt59) {
                case 1 :
                    // InternalPogoDsl.g:2305:4: (otherlv_24= 'hasAbstractAttribute' ( (lv_hasAbstractAttribute_25_0= ruleBoolean ) ) )
                    {
                    // InternalPogoDsl.g:2305:4: (otherlv_24= 'hasAbstractAttribute' ( (lv_hasAbstractAttribute_25_0= ruleBoolean ) ) )
                    // InternalPogoDsl.g:2306:5: otherlv_24= 'hasAbstractAttribute' ( (lv_hasAbstractAttribute_25_0= ruleBoolean ) )
                    {
                    otherlv_24=(Token)match(input,51,FOLLOW_62); 

                    					newLeafNode(otherlv_24, grammarAccess.getClassDescriptionAccess().getHasAbstractAttributeKeyword_12_0_0());
                    				
                    // InternalPogoDsl.g:2310:5: ( (lv_hasAbstractAttribute_25_0= ruleBoolean ) )
                    // InternalPogoDsl.g:2311:6: (lv_hasAbstractAttribute_25_0= ruleBoolean )
                    {
                    // InternalPogoDsl.g:2311:6: (lv_hasAbstractAttribute_25_0= ruleBoolean )
                    // InternalPogoDsl.g:2312:7: lv_hasAbstractAttribute_25_0= ruleBoolean
                    {

                    							newCompositeNode(grammarAccess.getClassDescriptionAccess().getHasAbstractAttributeBooleanParserRuleCall_12_0_1_0());
                    						
                    pushFollow(FOLLOW_66);
                    lv_hasAbstractAttribute_25_0=ruleBoolean();

                    state._fsp--;


                    							if (current==null) {
                    								current = createModelElementForParent(grammarAccess.getClassDescriptionRule());
                    							}
                    							set(
                    								current,
                    								"hasAbstractAttribute",
                    								lv_hasAbstractAttribute_25_0,
                    								"com.mncml.implementation.pogo.dsl.PogoDsl.Boolean");
                    							afterParserOrEnumRuleCall();
                    						

                    }


                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalPogoDsl.g:2331:4: ruleQualifiedName
                    {

                    				newCompositeNode(grammarAccess.getClassDescriptionAccess().getQualifiedNameParserRuleCall_12_1());
                    			
                    pushFollow(FOLLOW_66);
                    ruleQualifiedName();

                    state._fsp--;


                    				afterParserOrEnumRuleCall();
                    			

                    }
                    break;

            }

            // InternalPogoDsl.g:2339:3: ( (otherlv_27= 'descriptionHtmlExists' ( (lv_descriptionHtmlExists_28_0= ruleBoolean ) ) ) | ruleQualifiedName )?
            int alt60=3;
            int LA60_0 = input.LA(1);

            if ( (LA60_0==52) ) {
                alt60=1;
            }
            else if ( (LA60_0==RULE_ID) ) {
                alt60=2;
            }
            switch (alt60) {
                case 1 :
                    // InternalPogoDsl.g:2340:4: (otherlv_27= 'descriptionHtmlExists' ( (lv_descriptionHtmlExists_28_0= ruleBoolean ) ) )
                    {
                    // InternalPogoDsl.g:2340:4: (otherlv_27= 'descriptionHtmlExists' ( (lv_descriptionHtmlExists_28_0= ruleBoolean ) ) )
                    // InternalPogoDsl.g:2341:5: otherlv_27= 'descriptionHtmlExists' ( (lv_descriptionHtmlExists_28_0= ruleBoolean ) )
                    {
                    otherlv_27=(Token)match(input,52,FOLLOW_62); 

                    					newLeafNode(otherlv_27, grammarAccess.getClassDescriptionAccess().getDescriptionHtmlExistsKeyword_13_0_0());
                    				
                    // InternalPogoDsl.g:2345:5: ( (lv_descriptionHtmlExists_28_0= ruleBoolean ) )
                    // InternalPogoDsl.g:2346:6: (lv_descriptionHtmlExists_28_0= ruleBoolean )
                    {
                    // InternalPogoDsl.g:2346:6: (lv_descriptionHtmlExists_28_0= ruleBoolean )
                    // InternalPogoDsl.g:2347:7: lv_descriptionHtmlExists_28_0= ruleBoolean
                    {

                    							newCompositeNode(grammarAccess.getClassDescriptionAccess().getDescriptionHtmlExistsBooleanParserRuleCall_13_0_1_0());
                    						
                    pushFollow(FOLLOW_67);
                    lv_descriptionHtmlExists_28_0=ruleBoolean();

                    state._fsp--;


                    							if (current==null) {
                    								current = createModelElementForParent(grammarAccess.getClassDescriptionRule());
                    							}
                    							set(
                    								current,
                    								"descriptionHtmlExists",
                    								lv_descriptionHtmlExists_28_0,
                    								"com.mncml.implementation.pogo.dsl.PogoDsl.Boolean");
                    							afterParserOrEnumRuleCall();
                    						

                    }


                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalPogoDsl.g:2366:4: ruleQualifiedName
                    {

                    				newCompositeNode(grammarAccess.getClassDescriptionAccess().getQualifiedNameParserRuleCall_13_1());
                    			
                    pushFollow(FOLLOW_67);
                    ruleQualifiedName();

                    state._fsp--;


                    				afterParserOrEnumRuleCall();
                    			

                    }
                    break;

            }

            // InternalPogoDsl.g:2374:3: (otherlv_30= 'inheritances' otherlv_31= '{' ( (lv_inheritances_32_0= ruleInheritance ) ) (otherlv_33= ',' ( (lv_inheritances_34_0= ruleInheritance ) ) )* otherlv_35= '}' )?
            int alt62=2;
            int LA62_0 = input.LA(1);

            if ( (LA62_0==53) ) {
                alt62=1;
            }
            switch (alt62) {
                case 1 :
                    // InternalPogoDsl.g:2375:4: otherlv_30= 'inheritances' otherlv_31= '{' ( (lv_inheritances_32_0= ruleInheritance ) ) (otherlv_33= ',' ( (lv_inheritances_34_0= ruleInheritance ) ) )* otherlv_35= '}'
                    {
                    otherlv_30=(Token)match(input,53,FOLLOW_3); 

                    				newLeafNode(otherlv_30, grammarAccess.getClassDescriptionAccess().getInheritancesKeyword_14_0());
                    			
                    otherlv_31=(Token)match(input,12,FOLLOW_68); 

                    				newLeafNode(otherlv_31, grammarAccess.getClassDescriptionAccess().getLeftCurlyBracketKeyword_14_1());
                    			
                    // InternalPogoDsl.g:2383:4: ( (lv_inheritances_32_0= ruleInheritance ) )
                    // InternalPogoDsl.g:2384:5: (lv_inheritances_32_0= ruleInheritance )
                    {
                    // InternalPogoDsl.g:2384:5: (lv_inheritances_32_0= ruleInheritance )
                    // InternalPogoDsl.g:2385:6: lv_inheritances_32_0= ruleInheritance
                    {

                    						newCompositeNode(grammarAccess.getClassDescriptionAccess().getInheritancesInheritanceParserRuleCall_14_2_0());
                    					
                    pushFollow(FOLLOW_6);
                    lv_inheritances_32_0=ruleInheritance();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getClassDescriptionRule());
                    						}
                    						add(
                    							current,
                    							"inheritances",
                    							lv_inheritances_32_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.Inheritance");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalPogoDsl.g:2402:4: (otherlv_33= ',' ( (lv_inheritances_34_0= ruleInheritance ) ) )*
                    loop61:
                    do {
                        int alt61=2;
                        int LA61_0 = input.LA(1);

                        if ( (LA61_0==14) ) {
                            alt61=1;
                        }


                        switch (alt61) {
                    	case 1 :
                    	    // InternalPogoDsl.g:2403:5: otherlv_33= ',' ( (lv_inheritances_34_0= ruleInheritance ) )
                    	    {
                    	    otherlv_33=(Token)match(input,14,FOLLOW_68); 

                    	    					newLeafNode(otherlv_33, grammarAccess.getClassDescriptionAccess().getCommaKeyword_14_3_0());
                    	    				
                    	    // InternalPogoDsl.g:2407:5: ( (lv_inheritances_34_0= ruleInheritance ) )
                    	    // InternalPogoDsl.g:2408:6: (lv_inheritances_34_0= ruleInheritance )
                    	    {
                    	    // InternalPogoDsl.g:2408:6: (lv_inheritances_34_0= ruleInheritance )
                    	    // InternalPogoDsl.g:2409:7: lv_inheritances_34_0= ruleInheritance
                    	    {

                    	    							newCompositeNode(grammarAccess.getClassDescriptionAccess().getInheritancesInheritanceParserRuleCall_14_3_1_0());
                    	    						
                    	    pushFollow(FOLLOW_6);
                    	    lv_inheritances_34_0=ruleInheritance();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getClassDescriptionRule());
                    	    							}
                    	    							add(
                    	    								current,
                    	    								"inheritances",
                    	    								lv_inheritances_34_0,
                    	    								"com.mncml.implementation.pogo.dsl.PogoDsl.Inheritance");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop61;
                        }
                    } while (true);

                    otherlv_35=(Token)match(input,15,FOLLOW_69); 

                    				newLeafNode(otherlv_35, grammarAccess.getClassDescriptionAccess().getRightCurlyBracketKeyword_14_4());
                    			

                    }
                    break;

            }

            // InternalPogoDsl.g:2432:3: (otherlv_36= 'identification' ( (lv_identification_37_0= ruleClassIdentification ) ) )?
            int alt63=2;
            int LA63_0 = input.LA(1);

            if ( (LA63_0==54) ) {
                alt63=1;
            }
            switch (alt63) {
                case 1 :
                    // InternalPogoDsl.g:2433:4: otherlv_36= 'identification' ( (lv_identification_37_0= ruleClassIdentification ) )
                    {
                    otherlv_36=(Token)match(input,54,FOLLOW_70); 

                    				newLeafNode(otherlv_36, grammarAccess.getClassDescriptionAccess().getIdentificationKeyword_15_0());
                    			
                    // InternalPogoDsl.g:2437:4: ( (lv_identification_37_0= ruleClassIdentification ) )
                    // InternalPogoDsl.g:2438:5: (lv_identification_37_0= ruleClassIdentification )
                    {
                    // InternalPogoDsl.g:2438:5: (lv_identification_37_0= ruleClassIdentification )
                    // InternalPogoDsl.g:2439:6: lv_identification_37_0= ruleClassIdentification
                    {

                    						newCompositeNode(grammarAccess.getClassDescriptionAccess().getIdentificationClassIdentificationParserRuleCall_15_1_0());
                    					
                    pushFollow(FOLLOW_71);
                    lv_identification_37_0=ruleClassIdentification();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getClassDescriptionRule());
                    						}
                    						set(
                    							current,
                    							"identification",
                    							lv_identification_37_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.ClassIdentification");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalPogoDsl.g:2457:3: (otherlv_38= 'comments' ( (lv_comments_39_0= ruleComments ) ) )?
            int alt64=2;
            int LA64_0 = input.LA(1);

            if ( (LA64_0==55) ) {
                alt64=1;
            }
            switch (alt64) {
                case 1 :
                    // InternalPogoDsl.g:2458:4: otherlv_38= 'comments' ( (lv_comments_39_0= ruleComments ) )
                    {
                    otherlv_38=(Token)match(input,55,FOLLOW_72); 

                    				newLeafNode(otherlv_38, grammarAccess.getClassDescriptionAccess().getCommentsKeyword_16_0());
                    			
                    // InternalPogoDsl.g:2462:4: ( (lv_comments_39_0= ruleComments ) )
                    // InternalPogoDsl.g:2463:5: (lv_comments_39_0= ruleComments )
                    {
                    // InternalPogoDsl.g:2463:5: (lv_comments_39_0= ruleComments )
                    // InternalPogoDsl.g:2464:6: lv_comments_39_0= ruleComments
                    {

                    						newCompositeNode(grammarAccess.getClassDescriptionAccess().getCommentsCommentsParserRuleCall_16_1_0());
                    					
                    pushFollow(FOLLOW_11);
                    lv_comments_39_0=ruleComments();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getClassDescriptionRule());
                    						}
                    						set(
                    							current,
                    							"comments",
                    							lv_comments_39_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.Comments");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            otherlv_40=(Token)match(input,15,FOLLOW_2); 

            			newLeafNode(otherlv_40, grammarAccess.getClassDescriptionAccess().getRightCurlyBracketKeyword_17());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleClassDescription"


    // $ANTLR start "entryRuleProperty"
    // InternalPogoDsl.g:2490:1: entryRuleProperty returns [EObject current=null] : iv_ruleProperty= ruleProperty EOF ;
    public final EObject entryRuleProperty() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleProperty = null;


        try {
            // InternalPogoDsl.g:2490:49: (iv_ruleProperty= ruleProperty EOF )
            // InternalPogoDsl.g:2491:2: iv_ruleProperty= ruleProperty EOF
            {
             newCompositeNode(grammarAccess.getPropertyRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleProperty=ruleProperty();

            state._fsp--;

             current =iv_ruleProperty; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleProperty"


    // $ANTLR start "ruleProperty"
    // InternalPogoDsl.g:2497:1: ruleProperty returns [EObject current=null] : ( () otherlv_1= 'Property' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'mandatory' ( (lv_mandatory_5_0= ruleEString ) ) )? (otherlv_6= 'description' ( (lv_description_7_0= ruleEString ) ) )? (otherlv_8= 'DefaultPropValue' otherlv_9= '{' ( (lv_DefaultPropValue_10_0= ruleEString ) ) (otherlv_11= ',' ( (lv_DefaultPropValue_12_0= ruleEString ) ) )* otherlv_13= '}' )? (otherlv_14= 'type' ( (lv_type_15_0= rulePropType ) ) )? (otherlv_16= 'status' ( (lv_status_17_0= ruleInheritanceStatus ) ) )? otherlv_18= '}' ) ;
    public final EObject ruleProperty() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        Token otherlv_9=null;
        Token otherlv_11=null;
        Token otherlv_13=null;
        Token otherlv_14=null;
        Token otherlv_16=null;
        Token otherlv_18=null;
        AntlrDatatypeRuleToken lv_name_2_0 = null;

        AntlrDatatypeRuleToken lv_mandatory_5_0 = null;

        AntlrDatatypeRuleToken lv_description_7_0 = null;

        AntlrDatatypeRuleToken lv_DefaultPropValue_10_0 = null;

        AntlrDatatypeRuleToken lv_DefaultPropValue_12_0 = null;

        EObject lv_type_15_0 = null;

        EObject lv_status_17_0 = null;



        	enterRule();

        try {
            // InternalPogoDsl.g:2503:2: ( ( () otherlv_1= 'Property' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'mandatory' ( (lv_mandatory_5_0= ruleEString ) ) )? (otherlv_6= 'description' ( (lv_description_7_0= ruleEString ) ) )? (otherlv_8= 'DefaultPropValue' otherlv_9= '{' ( (lv_DefaultPropValue_10_0= ruleEString ) ) (otherlv_11= ',' ( (lv_DefaultPropValue_12_0= ruleEString ) ) )* otherlv_13= '}' )? (otherlv_14= 'type' ( (lv_type_15_0= rulePropType ) ) )? (otherlv_16= 'status' ( (lv_status_17_0= ruleInheritanceStatus ) ) )? otherlv_18= '}' ) )
            // InternalPogoDsl.g:2504:2: ( () otherlv_1= 'Property' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'mandatory' ( (lv_mandatory_5_0= ruleEString ) ) )? (otherlv_6= 'description' ( (lv_description_7_0= ruleEString ) ) )? (otherlv_8= 'DefaultPropValue' otherlv_9= '{' ( (lv_DefaultPropValue_10_0= ruleEString ) ) (otherlv_11= ',' ( (lv_DefaultPropValue_12_0= ruleEString ) ) )* otherlv_13= '}' )? (otherlv_14= 'type' ( (lv_type_15_0= rulePropType ) ) )? (otherlv_16= 'status' ( (lv_status_17_0= ruleInheritanceStatus ) ) )? otherlv_18= '}' )
            {
            // InternalPogoDsl.g:2504:2: ( () otherlv_1= 'Property' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'mandatory' ( (lv_mandatory_5_0= ruleEString ) ) )? (otherlv_6= 'description' ( (lv_description_7_0= ruleEString ) ) )? (otherlv_8= 'DefaultPropValue' otherlv_9= '{' ( (lv_DefaultPropValue_10_0= ruleEString ) ) (otherlv_11= ',' ( (lv_DefaultPropValue_12_0= ruleEString ) ) )* otherlv_13= '}' )? (otherlv_14= 'type' ( (lv_type_15_0= rulePropType ) ) )? (otherlv_16= 'status' ( (lv_status_17_0= ruleInheritanceStatus ) ) )? otherlv_18= '}' )
            // InternalPogoDsl.g:2505:3: () otherlv_1= 'Property' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'mandatory' ( (lv_mandatory_5_0= ruleEString ) ) )? (otherlv_6= 'description' ( (lv_description_7_0= ruleEString ) ) )? (otherlv_8= 'DefaultPropValue' otherlv_9= '{' ( (lv_DefaultPropValue_10_0= ruleEString ) ) (otherlv_11= ',' ( (lv_DefaultPropValue_12_0= ruleEString ) ) )* otherlv_13= '}' )? (otherlv_14= 'type' ( (lv_type_15_0= rulePropType ) ) )? (otherlv_16= 'status' ( (lv_status_17_0= ruleInheritanceStatus ) ) )? otherlv_18= '}'
            {
            // InternalPogoDsl.g:2505:3: ()
            // InternalPogoDsl.g:2506:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getPropertyAccess().getPropertyAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,56,FOLLOW_13); 

            			newLeafNode(otherlv_1, grammarAccess.getPropertyAccess().getPropertyKeyword_1());
            		
            // InternalPogoDsl.g:2516:3: ( (lv_name_2_0= ruleEString ) )
            // InternalPogoDsl.g:2517:4: (lv_name_2_0= ruleEString )
            {
            // InternalPogoDsl.g:2517:4: (lv_name_2_0= ruleEString )
            // InternalPogoDsl.g:2518:5: lv_name_2_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getPropertyAccess().getNameEStringParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_3);
            lv_name_2_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getPropertyRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_2_0,
            						"com.mncml.implementation.pogo.dsl.PogoDsl.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_3=(Token)match(input,12,FOLLOW_73); 

            			newLeafNode(otherlv_3, grammarAccess.getPropertyAccess().getLeftCurlyBracketKeyword_3());
            		
            // InternalPogoDsl.g:2539:3: (otherlv_4= 'mandatory' ( (lv_mandatory_5_0= ruleEString ) ) )?
            int alt65=2;
            int LA65_0 = input.LA(1);

            if ( (LA65_0==57) ) {
                alt65=1;
            }
            switch (alt65) {
                case 1 :
                    // InternalPogoDsl.g:2540:4: otherlv_4= 'mandatory' ( (lv_mandatory_5_0= ruleEString ) )
                    {
                    otherlv_4=(Token)match(input,57,FOLLOW_13); 

                    				newLeafNode(otherlv_4, grammarAccess.getPropertyAccess().getMandatoryKeyword_4_0());
                    			
                    // InternalPogoDsl.g:2544:4: ( (lv_mandatory_5_0= ruleEString ) )
                    // InternalPogoDsl.g:2545:5: (lv_mandatory_5_0= ruleEString )
                    {
                    // InternalPogoDsl.g:2545:5: (lv_mandatory_5_0= ruleEString )
                    // InternalPogoDsl.g:2546:6: lv_mandatory_5_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getPropertyAccess().getMandatoryEStringParserRuleCall_4_1_0());
                    					
                    pushFollow(FOLLOW_74);
                    lv_mandatory_5_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getPropertyRule());
                    						}
                    						set(
                    							current,
                    							"mandatory",
                    							lv_mandatory_5_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalPogoDsl.g:2564:3: (otherlv_6= 'description' ( (lv_description_7_0= ruleEString ) ) )?
            int alt66=2;
            int LA66_0 = input.LA(1);

            if ( (LA66_0==27) ) {
                alt66=1;
            }
            switch (alt66) {
                case 1 :
                    // InternalPogoDsl.g:2565:4: otherlv_6= 'description' ( (lv_description_7_0= ruleEString ) )
                    {
                    otherlv_6=(Token)match(input,27,FOLLOW_13); 

                    				newLeafNode(otherlv_6, grammarAccess.getPropertyAccess().getDescriptionKeyword_5_0());
                    			
                    // InternalPogoDsl.g:2569:4: ( (lv_description_7_0= ruleEString ) )
                    // InternalPogoDsl.g:2570:5: (lv_description_7_0= ruleEString )
                    {
                    // InternalPogoDsl.g:2570:5: (lv_description_7_0= ruleEString )
                    // InternalPogoDsl.g:2571:6: lv_description_7_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getPropertyAccess().getDescriptionEStringParserRuleCall_5_1_0());
                    					
                    pushFollow(FOLLOW_75);
                    lv_description_7_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getPropertyRule());
                    						}
                    						set(
                    							current,
                    							"description",
                    							lv_description_7_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalPogoDsl.g:2589:3: (otherlv_8= 'DefaultPropValue' otherlv_9= '{' ( (lv_DefaultPropValue_10_0= ruleEString ) ) (otherlv_11= ',' ( (lv_DefaultPropValue_12_0= ruleEString ) ) )* otherlv_13= '}' )?
            int alt68=2;
            int LA68_0 = input.LA(1);

            if ( (LA68_0==58) ) {
                alt68=1;
            }
            switch (alt68) {
                case 1 :
                    // InternalPogoDsl.g:2590:4: otherlv_8= 'DefaultPropValue' otherlv_9= '{' ( (lv_DefaultPropValue_10_0= ruleEString ) ) (otherlv_11= ',' ( (lv_DefaultPropValue_12_0= ruleEString ) ) )* otherlv_13= '}'
                    {
                    otherlv_8=(Token)match(input,58,FOLLOW_3); 

                    				newLeafNode(otherlv_8, grammarAccess.getPropertyAccess().getDefaultPropValueKeyword_6_0());
                    			
                    otherlv_9=(Token)match(input,12,FOLLOW_13); 

                    				newLeafNode(otherlv_9, grammarAccess.getPropertyAccess().getLeftCurlyBracketKeyword_6_1());
                    			
                    // InternalPogoDsl.g:2598:4: ( (lv_DefaultPropValue_10_0= ruleEString ) )
                    // InternalPogoDsl.g:2599:5: (lv_DefaultPropValue_10_0= ruleEString )
                    {
                    // InternalPogoDsl.g:2599:5: (lv_DefaultPropValue_10_0= ruleEString )
                    // InternalPogoDsl.g:2600:6: lv_DefaultPropValue_10_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getPropertyAccess().getDefaultPropValueEStringParserRuleCall_6_2_0());
                    					
                    pushFollow(FOLLOW_6);
                    lv_DefaultPropValue_10_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getPropertyRule());
                    						}
                    						add(
                    							current,
                    							"DefaultPropValue",
                    							lv_DefaultPropValue_10_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalPogoDsl.g:2617:4: (otherlv_11= ',' ( (lv_DefaultPropValue_12_0= ruleEString ) ) )*
                    loop67:
                    do {
                        int alt67=2;
                        int LA67_0 = input.LA(1);

                        if ( (LA67_0==14) ) {
                            alt67=1;
                        }


                        switch (alt67) {
                    	case 1 :
                    	    // InternalPogoDsl.g:2618:5: otherlv_11= ',' ( (lv_DefaultPropValue_12_0= ruleEString ) )
                    	    {
                    	    otherlv_11=(Token)match(input,14,FOLLOW_13); 

                    	    					newLeafNode(otherlv_11, grammarAccess.getPropertyAccess().getCommaKeyword_6_3_0());
                    	    				
                    	    // InternalPogoDsl.g:2622:5: ( (lv_DefaultPropValue_12_0= ruleEString ) )
                    	    // InternalPogoDsl.g:2623:6: (lv_DefaultPropValue_12_0= ruleEString )
                    	    {
                    	    // InternalPogoDsl.g:2623:6: (lv_DefaultPropValue_12_0= ruleEString )
                    	    // InternalPogoDsl.g:2624:7: lv_DefaultPropValue_12_0= ruleEString
                    	    {

                    	    							newCompositeNode(grammarAccess.getPropertyAccess().getDefaultPropValueEStringParserRuleCall_6_3_1_0());
                    	    						
                    	    pushFollow(FOLLOW_6);
                    	    lv_DefaultPropValue_12_0=ruleEString();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getPropertyRule());
                    	    							}
                    	    							add(
                    	    								current,
                    	    								"DefaultPropValue",
                    	    								lv_DefaultPropValue_12_0,
                    	    								"com.mncml.implementation.pogo.dsl.PogoDsl.EString");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop67;
                        }
                    } while (true);

                    otherlv_13=(Token)match(input,15,FOLLOW_76); 

                    				newLeafNode(otherlv_13, grammarAccess.getPropertyAccess().getRightCurlyBracketKeyword_6_4());
                    			

                    }
                    break;

            }

            // InternalPogoDsl.g:2647:3: (otherlv_14= 'type' ( (lv_type_15_0= rulePropType ) ) )?
            int alt69=2;
            int LA69_0 = input.LA(1);

            if ( (LA69_0==59) ) {
                alt69=1;
            }
            switch (alt69) {
                case 1 :
                    // InternalPogoDsl.g:2648:4: otherlv_14= 'type' ( (lv_type_15_0= rulePropType ) )
                    {
                    otherlv_14=(Token)match(input,59,FOLLOW_77); 

                    				newLeafNode(otherlv_14, grammarAccess.getPropertyAccess().getTypeKeyword_7_0());
                    			
                    // InternalPogoDsl.g:2652:4: ( (lv_type_15_0= rulePropType ) )
                    // InternalPogoDsl.g:2653:5: (lv_type_15_0= rulePropType )
                    {
                    // InternalPogoDsl.g:2653:5: (lv_type_15_0= rulePropType )
                    // InternalPogoDsl.g:2654:6: lv_type_15_0= rulePropType
                    {

                    						newCompositeNode(grammarAccess.getPropertyAccess().getTypePropTypeParserRuleCall_7_1_0());
                    					
                    pushFollow(FOLLOW_78);
                    lv_type_15_0=rulePropType();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getPropertyRule());
                    						}
                    						set(
                    							current,
                    							"type",
                    							lv_type_15_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.PropType");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalPogoDsl.g:2672:3: (otherlv_16= 'status' ( (lv_status_17_0= ruleInheritanceStatus ) ) )?
            int alt70=2;
            int LA70_0 = input.LA(1);

            if ( (LA70_0==60) ) {
                alt70=1;
            }
            switch (alt70) {
                case 1 :
                    // InternalPogoDsl.g:2673:4: otherlv_16= 'status' ( (lv_status_17_0= ruleInheritanceStatus ) )
                    {
                    otherlv_16=(Token)match(input,60,FOLLOW_79); 

                    				newLeafNode(otherlv_16, grammarAccess.getPropertyAccess().getStatusKeyword_8_0());
                    			
                    // InternalPogoDsl.g:2677:4: ( (lv_status_17_0= ruleInheritanceStatus ) )
                    // InternalPogoDsl.g:2678:5: (lv_status_17_0= ruleInheritanceStatus )
                    {
                    // InternalPogoDsl.g:2678:5: (lv_status_17_0= ruleInheritanceStatus )
                    // InternalPogoDsl.g:2679:6: lv_status_17_0= ruleInheritanceStatus
                    {

                    						newCompositeNode(grammarAccess.getPropertyAccess().getStatusInheritanceStatusParserRuleCall_8_1_0());
                    					
                    pushFollow(FOLLOW_11);
                    lv_status_17_0=ruleInheritanceStatus();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getPropertyRule());
                    						}
                    						set(
                    							current,
                    							"status",
                    							lv_status_17_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.InheritanceStatus");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            otherlv_18=(Token)match(input,15,FOLLOW_2); 

            			newLeafNode(otherlv_18, grammarAccess.getPropertyAccess().getRightCurlyBracketKeyword_9());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleProperty"


    // $ANTLR start "entryRuleCommand"
    // InternalPogoDsl.g:2705:1: entryRuleCommand returns [EObject current=null] : iv_ruleCommand= ruleCommand EOF ;
    public final EObject entryRuleCommand() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCommand = null;


        try {
            // InternalPogoDsl.g:2705:48: (iv_ruleCommand= ruleCommand EOF )
            // InternalPogoDsl.g:2706:2: iv_ruleCommand= ruleCommand EOF
            {
             newCompositeNode(grammarAccess.getCommandRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleCommand=ruleCommand();

            state._fsp--;

             current =iv_ruleCommand; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCommand"


    // $ANTLR start "ruleCommand"
    // InternalPogoDsl.g:2712:1: ruleCommand returns [EObject current=null] : ( () otherlv_1= 'Command' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'description' ( (lv_description_5_0= ruleEString ) ) )? (otherlv_6= 'execMethod' ( (lv_execMethod_7_0= ruleEString ) ) )? ( (otherlv_8= 'displayLevel' ( (lv_displayLevel_9_0= ruleDisplayLevel ) ) ) | ruleQualifiedName )? (otherlv_11= 'polledPeriod' ( (lv_polledPeriod_12_0= ruleEString ) ) )? (otherlv_13= 'isDynamic' ( (lv_isDynamic_14_0= ruleEString ) ) )? (otherlv_15= 'excludedStates' otherlv_16= '{' ( (lv_excludedStates_17_0= ruleEString ) ) (otherlv_18= ',' ( (lv_excludedStates_19_0= ruleEString ) ) )* otherlv_20= '}' )? (otherlv_21= 'argin' ( (lv_argin_22_0= ruleArgument ) ) )? (otherlv_23= 'argout' ( (lv_argout_24_0= ruleArgument ) ) )? (otherlv_25= 'status' ( (lv_status_26_0= ruleInheritanceStatus ) ) )? otherlv_27= '}' ) ;
    public final EObject ruleCommand() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        Token otherlv_11=null;
        Token otherlv_13=null;
        Token otherlv_15=null;
        Token otherlv_16=null;
        Token otherlv_18=null;
        Token otherlv_20=null;
        Token otherlv_21=null;
        Token otherlv_23=null;
        Token otherlv_25=null;
        Token otherlv_27=null;
        AntlrDatatypeRuleToken lv_name_2_0 = null;

        AntlrDatatypeRuleToken lv_description_5_0 = null;

        AntlrDatatypeRuleToken lv_execMethod_7_0 = null;

        AntlrDatatypeRuleToken lv_displayLevel_9_0 = null;

        AntlrDatatypeRuleToken lv_polledPeriod_12_0 = null;

        AntlrDatatypeRuleToken lv_isDynamic_14_0 = null;

        AntlrDatatypeRuleToken lv_excludedStates_17_0 = null;

        AntlrDatatypeRuleToken lv_excludedStates_19_0 = null;

        EObject lv_argin_22_0 = null;

        EObject lv_argout_24_0 = null;

        EObject lv_status_26_0 = null;



        	enterRule();

        try {
            // InternalPogoDsl.g:2718:2: ( ( () otherlv_1= 'Command' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'description' ( (lv_description_5_0= ruleEString ) ) )? (otherlv_6= 'execMethod' ( (lv_execMethod_7_0= ruleEString ) ) )? ( (otherlv_8= 'displayLevel' ( (lv_displayLevel_9_0= ruleDisplayLevel ) ) ) | ruleQualifiedName )? (otherlv_11= 'polledPeriod' ( (lv_polledPeriod_12_0= ruleEString ) ) )? (otherlv_13= 'isDynamic' ( (lv_isDynamic_14_0= ruleEString ) ) )? (otherlv_15= 'excludedStates' otherlv_16= '{' ( (lv_excludedStates_17_0= ruleEString ) ) (otherlv_18= ',' ( (lv_excludedStates_19_0= ruleEString ) ) )* otherlv_20= '}' )? (otherlv_21= 'argin' ( (lv_argin_22_0= ruleArgument ) ) )? (otherlv_23= 'argout' ( (lv_argout_24_0= ruleArgument ) ) )? (otherlv_25= 'status' ( (lv_status_26_0= ruleInheritanceStatus ) ) )? otherlv_27= '}' ) )
            // InternalPogoDsl.g:2719:2: ( () otherlv_1= 'Command' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'description' ( (lv_description_5_0= ruleEString ) ) )? (otherlv_6= 'execMethod' ( (lv_execMethod_7_0= ruleEString ) ) )? ( (otherlv_8= 'displayLevel' ( (lv_displayLevel_9_0= ruleDisplayLevel ) ) ) | ruleQualifiedName )? (otherlv_11= 'polledPeriod' ( (lv_polledPeriod_12_0= ruleEString ) ) )? (otherlv_13= 'isDynamic' ( (lv_isDynamic_14_0= ruleEString ) ) )? (otherlv_15= 'excludedStates' otherlv_16= '{' ( (lv_excludedStates_17_0= ruleEString ) ) (otherlv_18= ',' ( (lv_excludedStates_19_0= ruleEString ) ) )* otherlv_20= '}' )? (otherlv_21= 'argin' ( (lv_argin_22_0= ruleArgument ) ) )? (otherlv_23= 'argout' ( (lv_argout_24_0= ruleArgument ) ) )? (otherlv_25= 'status' ( (lv_status_26_0= ruleInheritanceStatus ) ) )? otherlv_27= '}' )
            {
            // InternalPogoDsl.g:2719:2: ( () otherlv_1= 'Command' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'description' ( (lv_description_5_0= ruleEString ) ) )? (otherlv_6= 'execMethod' ( (lv_execMethod_7_0= ruleEString ) ) )? ( (otherlv_8= 'displayLevel' ( (lv_displayLevel_9_0= ruleDisplayLevel ) ) ) | ruleQualifiedName )? (otherlv_11= 'polledPeriod' ( (lv_polledPeriod_12_0= ruleEString ) ) )? (otherlv_13= 'isDynamic' ( (lv_isDynamic_14_0= ruleEString ) ) )? (otherlv_15= 'excludedStates' otherlv_16= '{' ( (lv_excludedStates_17_0= ruleEString ) ) (otherlv_18= ',' ( (lv_excludedStates_19_0= ruleEString ) ) )* otherlv_20= '}' )? (otherlv_21= 'argin' ( (lv_argin_22_0= ruleArgument ) ) )? (otherlv_23= 'argout' ( (lv_argout_24_0= ruleArgument ) ) )? (otherlv_25= 'status' ( (lv_status_26_0= ruleInheritanceStatus ) ) )? otherlv_27= '}' )
            // InternalPogoDsl.g:2720:3: () otherlv_1= 'Command' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'description' ( (lv_description_5_0= ruleEString ) ) )? (otherlv_6= 'execMethod' ( (lv_execMethod_7_0= ruleEString ) ) )? ( (otherlv_8= 'displayLevel' ( (lv_displayLevel_9_0= ruleDisplayLevel ) ) ) | ruleQualifiedName )? (otherlv_11= 'polledPeriod' ( (lv_polledPeriod_12_0= ruleEString ) ) )? (otherlv_13= 'isDynamic' ( (lv_isDynamic_14_0= ruleEString ) ) )? (otherlv_15= 'excludedStates' otherlv_16= '{' ( (lv_excludedStates_17_0= ruleEString ) ) (otherlv_18= ',' ( (lv_excludedStates_19_0= ruleEString ) ) )* otherlv_20= '}' )? (otherlv_21= 'argin' ( (lv_argin_22_0= ruleArgument ) ) )? (otherlv_23= 'argout' ( (lv_argout_24_0= ruleArgument ) ) )? (otherlv_25= 'status' ( (lv_status_26_0= ruleInheritanceStatus ) ) )? otherlv_27= '}'
            {
            // InternalPogoDsl.g:2720:3: ()
            // InternalPogoDsl.g:2721:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getCommandAccess().getCommandAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,61,FOLLOW_13); 

            			newLeafNode(otherlv_1, grammarAccess.getCommandAccess().getCommandKeyword_1());
            		
            // InternalPogoDsl.g:2731:3: ( (lv_name_2_0= ruleEString ) )
            // InternalPogoDsl.g:2732:4: (lv_name_2_0= ruleEString )
            {
            // InternalPogoDsl.g:2732:4: (lv_name_2_0= ruleEString )
            // InternalPogoDsl.g:2733:5: lv_name_2_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getCommandAccess().getNameEStringParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_3);
            lv_name_2_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getCommandRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_2_0,
            						"com.mncml.implementation.pogo.dsl.PogoDsl.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_3=(Token)match(input,12,FOLLOW_80); 

            			newLeafNode(otherlv_3, grammarAccess.getCommandAccess().getLeftCurlyBracketKeyword_3());
            		
            // InternalPogoDsl.g:2754:3: (otherlv_4= 'description' ( (lv_description_5_0= ruleEString ) ) )?
            int alt71=2;
            int LA71_0 = input.LA(1);

            if ( (LA71_0==27) ) {
                alt71=1;
            }
            switch (alt71) {
                case 1 :
                    // InternalPogoDsl.g:2755:4: otherlv_4= 'description' ( (lv_description_5_0= ruleEString ) )
                    {
                    otherlv_4=(Token)match(input,27,FOLLOW_13); 

                    				newLeafNode(otherlv_4, grammarAccess.getCommandAccess().getDescriptionKeyword_4_0());
                    			
                    // InternalPogoDsl.g:2759:4: ( (lv_description_5_0= ruleEString ) )
                    // InternalPogoDsl.g:2760:5: (lv_description_5_0= ruleEString )
                    {
                    // InternalPogoDsl.g:2760:5: (lv_description_5_0= ruleEString )
                    // InternalPogoDsl.g:2761:6: lv_description_5_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getCommandAccess().getDescriptionEStringParserRuleCall_4_1_0());
                    					
                    pushFollow(FOLLOW_81);
                    lv_description_5_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getCommandRule());
                    						}
                    						set(
                    							current,
                    							"description",
                    							lv_description_5_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalPogoDsl.g:2779:3: (otherlv_6= 'execMethod' ( (lv_execMethod_7_0= ruleEString ) ) )?
            int alt72=2;
            int LA72_0 = input.LA(1);

            if ( (LA72_0==62) ) {
                alt72=1;
            }
            switch (alt72) {
                case 1 :
                    // InternalPogoDsl.g:2780:4: otherlv_6= 'execMethod' ( (lv_execMethod_7_0= ruleEString ) )
                    {
                    otherlv_6=(Token)match(input,62,FOLLOW_13); 

                    				newLeafNode(otherlv_6, grammarAccess.getCommandAccess().getExecMethodKeyword_5_0());
                    			
                    // InternalPogoDsl.g:2784:4: ( (lv_execMethod_7_0= ruleEString ) )
                    // InternalPogoDsl.g:2785:5: (lv_execMethod_7_0= ruleEString )
                    {
                    // InternalPogoDsl.g:2785:5: (lv_execMethod_7_0= ruleEString )
                    // InternalPogoDsl.g:2786:6: lv_execMethod_7_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getCommandAccess().getExecMethodEStringParserRuleCall_5_1_0());
                    					
                    pushFollow(FOLLOW_82);
                    lv_execMethod_7_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getCommandRule());
                    						}
                    						set(
                    							current,
                    							"execMethod",
                    							lv_execMethod_7_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalPogoDsl.g:2804:3: ( (otherlv_8= 'displayLevel' ( (lv_displayLevel_9_0= ruleDisplayLevel ) ) ) | ruleQualifiedName )?
            int alt73=3;
            int LA73_0 = input.LA(1);

            if ( (LA73_0==63) ) {
                alt73=1;
            }
            else if ( (LA73_0==RULE_ID) ) {
                alt73=2;
            }
            switch (alt73) {
                case 1 :
                    // InternalPogoDsl.g:2805:4: (otherlv_8= 'displayLevel' ( (lv_displayLevel_9_0= ruleDisplayLevel ) ) )
                    {
                    // InternalPogoDsl.g:2805:4: (otherlv_8= 'displayLevel' ( (lv_displayLevel_9_0= ruleDisplayLevel ) ) )
                    // InternalPogoDsl.g:2806:5: otherlv_8= 'displayLevel' ( (lv_displayLevel_9_0= ruleDisplayLevel ) )
                    {
                    otherlv_8=(Token)match(input,63,FOLLOW_83); 

                    					newLeafNode(otherlv_8, grammarAccess.getCommandAccess().getDisplayLevelKeyword_6_0_0());
                    				
                    // InternalPogoDsl.g:2810:5: ( (lv_displayLevel_9_0= ruleDisplayLevel ) )
                    // InternalPogoDsl.g:2811:6: (lv_displayLevel_9_0= ruleDisplayLevel )
                    {
                    // InternalPogoDsl.g:2811:6: (lv_displayLevel_9_0= ruleDisplayLevel )
                    // InternalPogoDsl.g:2812:7: lv_displayLevel_9_0= ruleDisplayLevel
                    {

                    							newCompositeNode(grammarAccess.getCommandAccess().getDisplayLevelDisplayLevelParserRuleCall_6_0_1_0());
                    						
                    pushFollow(FOLLOW_84);
                    lv_displayLevel_9_0=ruleDisplayLevel();

                    state._fsp--;


                    							if (current==null) {
                    								current = createModelElementForParent(grammarAccess.getCommandRule());
                    							}
                    							set(
                    								current,
                    								"displayLevel",
                    								lv_displayLevel_9_0,
                    								"com.mncml.implementation.pogo.dsl.PogoDsl.DisplayLevel");
                    							afterParserOrEnumRuleCall();
                    						

                    }


                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalPogoDsl.g:2831:4: ruleQualifiedName
                    {

                    				newCompositeNode(grammarAccess.getCommandAccess().getQualifiedNameParserRuleCall_6_1());
                    			
                    pushFollow(FOLLOW_84);
                    ruleQualifiedName();

                    state._fsp--;


                    				afterParserOrEnumRuleCall();
                    			

                    }
                    break;

            }

            // InternalPogoDsl.g:2839:3: (otherlv_11= 'polledPeriod' ( (lv_polledPeriod_12_0= ruleEString ) ) )?
            int alt74=2;
            int LA74_0 = input.LA(1);

            if ( (LA74_0==64) ) {
                alt74=1;
            }
            switch (alt74) {
                case 1 :
                    // InternalPogoDsl.g:2840:4: otherlv_11= 'polledPeriod' ( (lv_polledPeriod_12_0= ruleEString ) )
                    {
                    otherlv_11=(Token)match(input,64,FOLLOW_13); 

                    				newLeafNode(otherlv_11, grammarAccess.getCommandAccess().getPolledPeriodKeyword_7_0());
                    			
                    // InternalPogoDsl.g:2844:4: ( (lv_polledPeriod_12_0= ruleEString ) )
                    // InternalPogoDsl.g:2845:5: (lv_polledPeriod_12_0= ruleEString )
                    {
                    // InternalPogoDsl.g:2845:5: (lv_polledPeriod_12_0= ruleEString )
                    // InternalPogoDsl.g:2846:6: lv_polledPeriod_12_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getCommandAccess().getPolledPeriodEStringParserRuleCall_7_1_0());
                    					
                    pushFollow(FOLLOW_85);
                    lv_polledPeriod_12_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getCommandRule());
                    						}
                    						set(
                    							current,
                    							"polledPeriod",
                    							lv_polledPeriod_12_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalPogoDsl.g:2864:3: (otherlv_13= 'isDynamic' ( (lv_isDynamic_14_0= ruleEString ) ) )?
            int alt75=2;
            int LA75_0 = input.LA(1);

            if ( (LA75_0==65) ) {
                alt75=1;
            }
            switch (alt75) {
                case 1 :
                    // InternalPogoDsl.g:2865:4: otherlv_13= 'isDynamic' ( (lv_isDynamic_14_0= ruleEString ) )
                    {
                    otherlv_13=(Token)match(input,65,FOLLOW_13); 

                    				newLeafNode(otherlv_13, grammarAccess.getCommandAccess().getIsDynamicKeyword_8_0());
                    			
                    // InternalPogoDsl.g:2869:4: ( (lv_isDynamic_14_0= ruleEString ) )
                    // InternalPogoDsl.g:2870:5: (lv_isDynamic_14_0= ruleEString )
                    {
                    // InternalPogoDsl.g:2870:5: (lv_isDynamic_14_0= ruleEString )
                    // InternalPogoDsl.g:2871:6: lv_isDynamic_14_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getCommandAccess().getIsDynamicEStringParserRuleCall_8_1_0());
                    					
                    pushFollow(FOLLOW_86);
                    lv_isDynamic_14_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getCommandRule());
                    						}
                    						set(
                    							current,
                    							"isDynamic",
                    							lv_isDynamic_14_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalPogoDsl.g:2889:3: (otherlv_15= 'excludedStates' otherlv_16= '{' ( (lv_excludedStates_17_0= ruleEString ) ) (otherlv_18= ',' ( (lv_excludedStates_19_0= ruleEString ) ) )* otherlv_20= '}' )?
            int alt77=2;
            int LA77_0 = input.LA(1);

            if ( (LA77_0==66) ) {
                alt77=1;
            }
            switch (alt77) {
                case 1 :
                    // InternalPogoDsl.g:2890:4: otherlv_15= 'excludedStates' otherlv_16= '{' ( (lv_excludedStates_17_0= ruleEString ) ) (otherlv_18= ',' ( (lv_excludedStates_19_0= ruleEString ) ) )* otherlv_20= '}'
                    {
                    otherlv_15=(Token)match(input,66,FOLLOW_3); 

                    				newLeafNode(otherlv_15, grammarAccess.getCommandAccess().getExcludedStatesKeyword_9_0());
                    			
                    otherlv_16=(Token)match(input,12,FOLLOW_13); 

                    				newLeafNode(otherlv_16, grammarAccess.getCommandAccess().getLeftCurlyBracketKeyword_9_1());
                    			
                    // InternalPogoDsl.g:2898:4: ( (lv_excludedStates_17_0= ruleEString ) )
                    // InternalPogoDsl.g:2899:5: (lv_excludedStates_17_0= ruleEString )
                    {
                    // InternalPogoDsl.g:2899:5: (lv_excludedStates_17_0= ruleEString )
                    // InternalPogoDsl.g:2900:6: lv_excludedStates_17_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getCommandAccess().getExcludedStatesEStringParserRuleCall_9_2_0());
                    					
                    pushFollow(FOLLOW_6);
                    lv_excludedStates_17_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getCommandRule());
                    						}
                    						add(
                    							current,
                    							"excludedStates",
                    							lv_excludedStates_17_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalPogoDsl.g:2917:4: (otherlv_18= ',' ( (lv_excludedStates_19_0= ruleEString ) ) )*
                    loop76:
                    do {
                        int alt76=2;
                        int LA76_0 = input.LA(1);

                        if ( (LA76_0==14) ) {
                            alt76=1;
                        }


                        switch (alt76) {
                    	case 1 :
                    	    // InternalPogoDsl.g:2918:5: otherlv_18= ',' ( (lv_excludedStates_19_0= ruleEString ) )
                    	    {
                    	    otherlv_18=(Token)match(input,14,FOLLOW_13); 

                    	    					newLeafNode(otherlv_18, grammarAccess.getCommandAccess().getCommaKeyword_9_3_0());
                    	    				
                    	    // InternalPogoDsl.g:2922:5: ( (lv_excludedStates_19_0= ruleEString ) )
                    	    // InternalPogoDsl.g:2923:6: (lv_excludedStates_19_0= ruleEString )
                    	    {
                    	    // InternalPogoDsl.g:2923:6: (lv_excludedStates_19_0= ruleEString )
                    	    // InternalPogoDsl.g:2924:7: lv_excludedStates_19_0= ruleEString
                    	    {

                    	    							newCompositeNode(grammarAccess.getCommandAccess().getExcludedStatesEStringParserRuleCall_9_3_1_0());
                    	    						
                    	    pushFollow(FOLLOW_6);
                    	    lv_excludedStates_19_0=ruleEString();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getCommandRule());
                    	    							}
                    	    							add(
                    	    								current,
                    	    								"excludedStates",
                    	    								lv_excludedStates_19_0,
                    	    								"com.mncml.implementation.pogo.dsl.PogoDsl.EString");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop76;
                        }
                    } while (true);

                    otherlv_20=(Token)match(input,15,FOLLOW_87); 

                    				newLeafNode(otherlv_20, grammarAccess.getCommandAccess().getRightCurlyBracketKeyword_9_4());
                    			

                    }
                    break;

            }

            // InternalPogoDsl.g:2947:3: (otherlv_21= 'argin' ( (lv_argin_22_0= ruleArgument ) ) )?
            int alt78=2;
            int LA78_0 = input.LA(1);

            if ( (LA78_0==67) ) {
                alt78=1;
            }
            switch (alt78) {
                case 1 :
                    // InternalPogoDsl.g:2948:4: otherlv_21= 'argin' ( (lv_argin_22_0= ruleArgument ) )
                    {
                    otherlv_21=(Token)match(input,67,FOLLOW_88); 

                    				newLeafNode(otherlv_21, grammarAccess.getCommandAccess().getArginKeyword_10_0());
                    			
                    // InternalPogoDsl.g:2952:4: ( (lv_argin_22_0= ruleArgument ) )
                    // InternalPogoDsl.g:2953:5: (lv_argin_22_0= ruleArgument )
                    {
                    // InternalPogoDsl.g:2953:5: (lv_argin_22_0= ruleArgument )
                    // InternalPogoDsl.g:2954:6: lv_argin_22_0= ruleArgument
                    {

                    						newCompositeNode(grammarAccess.getCommandAccess().getArginArgumentParserRuleCall_10_1_0());
                    					
                    pushFollow(FOLLOW_89);
                    lv_argin_22_0=ruleArgument();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getCommandRule());
                    						}
                    						set(
                    							current,
                    							"argin",
                    							lv_argin_22_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.Argument");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalPogoDsl.g:2972:3: (otherlv_23= 'argout' ( (lv_argout_24_0= ruleArgument ) ) )?
            int alt79=2;
            int LA79_0 = input.LA(1);

            if ( (LA79_0==68) ) {
                alt79=1;
            }
            switch (alt79) {
                case 1 :
                    // InternalPogoDsl.g:2973:4: otherlv_23= 'argout' ( (lv_argout_24_0= ruleArgument ) )
                    {
                    otherlv_23=(Token)match(input,68,FOLLOW_88); 

                    				newLeafNode(otherlv_23, grammarAccess.getCommandAccess().getArgoutKeyword_11_0());
                    			
                    // InternalPogoDsl.g:2977:4: ( (lv_argout_24_0= ruleArgument ) )
                    // InternalPogoDsl.g:2978:5: (lv_argout_24_0= ruleArgument )
                    {
                    // InternalPogoDsl.g:2978:5: (lv_argout_24_0= ruleArgument )
                    // InternalPogoDsl.g:2979:6: lv_argout_24_0= ruleArgument
                    {

                    						newCompositeNode(grammarAccess.getCommandAccess().getArgoutArgumentParserRuleCall_11_1_0());
                    					
                    pushFollow(FOLLOW_78);
                    lv_argout_24_0=ruleArgument();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getCommandRule());
                    						}
                    						set(
                    							current,
                    							"argout",
                    							lv_argout_24_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.Argument");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalPogoDsl.g:2997:3: (otherlv_25= 'status' ( (lv_status_26_0= ruleInheritanceStatus ) ) )?
            int alt80=2;
            int LA80_0 = input.LA(1);

            if ( (LA80_0==60) ) {
                alt80=1;
            }
            switch (alt80) {
                case 1 :
                    // InternalPogoDsl.g:2998:4: otherlv_25= 'status' ( (lv_status_26_0= ruleInheritanceStatus ) )
                    {
                    otherlv_25=(Token)match(input,60,FOLLOW_79); 

                    				newLeafNode(otherlv_25, grammarAccess.getCommandAccess().getStatusKeyword_12_0());
                    			
                    // InternalPogoDsl.g:3002:4: ( (lv_status_26_0= ruleInheritanceStatus ) )
                    // InternalPogoDsl.g:3003:5: (lv_status_26_0= ruleInheritanceStatus )
                    {
                    // InternalPogoDsl.g:3003:5: (lv_status_26_0= ruleInheritanceStatus )
                    // InternalPogoDsl.g:3004:6: lv_status_26_0= ruleInheritanceStatus
                    {

                    						newCompositeNode(grammarAccess.getCommandAccess().getStatusInheritanceStatusParserRuleCall_12_1_0());
                    					
                    pushFollow(FOLLOW_11);
                    lv_status_26_0=ruleInheritanceStatus();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getCommandRule());
                    						}
                    						set(
                    							current,
                    							"status",
                    							lv_status_26_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.InheritanceStatus");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            otherlv_27=(Token)match(input,15,FOLLOW_2); 

            			newLeafNode(otherlv_27, grammarAccess.getCommandAccess().getRightCurlyBracketKeyword_13());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCommand"


    // $ANTLR start "entryRuleAttribute"
    // InternalPogoDsl.g:3030:1: entryRuleAttribute returns [EObject current=null] : iv_ruleAttribute= ruleAttribute EOF ;
    public final EObject entryRuleAttribute() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAttribute = null;


        try {
            // InternalPogoDsl.g:3030:50: (iv_ruleAttribute= ruleAttribute EOF )
            // InternalPogoDsl.g:3031:2: iv_ruleAttribute= ruleAttribute EOF
            {
             newCompositeNode(grammarAccess.getAttributeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAttribute=ruleAttribute();

            state._fsp--;

             current =iv_ruleAttribute; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAttribute"


    // $ANTLR start "ruleAttribute"
    // InternalPogoDsl.g:3037:1: ruleAttribute returns [EObject current=null] : ( () otherlv_1= 'Attribute' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'attType' ( (lv_attType_5_0= ruleAttrType ) ) )? (otherlv_6= 'rwType' ( (lv_rwType_7_0= ruleRW_Type ) ) )? (otherlv_8= 'displayLevel' ( (lv_displayLevel_9_0= ruleDisplayLevel ) ) )? (otherlv_10= 'polledPeriod' ( (lv_polledPeriod_11_0= ruleEString ) ) )? (otherlv_12= 'maxX' ( (lv_maxX_13_0= ruleEString ) ) )? (otherlv_14= 'maxY' ( (lv_maxY_15_0= ruleEString ) ) )? (otherlv_16= 'associatedAttr' ( (lv_associatedAttr_17_0= ruleEString ) ) )? (otherlv_18= 'memorized' ( (lv_memorized_19_0= ruleEString ) ) )? (otherlv_20= 'memorizedAtInit' ( (lv_memorizedAtInit_21_0= ruleEString ) ) )? (otherlv_22= 'allocReadMember' ( (lv_allocReadMember_23_0= ruleEString ) ) )? (otherlv_24= 'isDynamic' ( (lv_isDynamic_25_0= ruleEString ) ) )? (otherlv_26= 'enumLabels' otherlv_27= '{' ( (lv_enumLabels_28_0= ruleEString ) ) (otherlv_29= ',' ( (lv_enumLabels_30_0= ruleEString ) ) )* otherlv_31= '}' )? (otherlv_32= 'readExcludedStates' otherlv_33= '{' ( (lv_readExcludedStates_34_0= ruleEString ) ) (otherlv_35= ',' ( (lv_readExcludedStates_36_0= ruleEString ) ) )* otherlv_37= '}' )? (otherlv_38= 'writeExcludedStates' otherlv_39= '{' ( (lv_writeExcludedStates_40_0= ruleEString ) ) (otherlv_41= ',' ( (lv_writeExcludedStates_42_0= ruleEString ) ) )* otherlv_43= '}' )? (otherlv_44= 'dataType' ( (lv_dataType_45_0= ruleType ) ) )? (otherlv_46= 'changeEvent' ( (lv_changeEvent_47_0= ruleFireEvents ) ) )? (otherlv_48= 'archiveEvent' ( (lv_archiveEvent_49_0= ruleFireEvents ) ) )? (otherlv_50= 'dataReadyEvent' ( (lv_dataReadyEvent_51_0= ruleFireEvents ) ) )? (otherlv_52= 'status' ( (lv_status_53_0= ruleInheritanceStatus ) ) )? (otherlv_54= 'properties' ( (lv_properties_55_0= ruleAttrProperties ) ) )? (otherlv_56= 'eventCriteria' ( (lv_eventCriteria_57_0= ruleEventCriteria ) ) )? (otherlv_58= 'evArchiveCriteria' ( (lv_evArchiveCriteria_59_0= ruleEventCriteria ) ) )? otherlv_60= '}' ) ;
    public final EObject ruleAttribute() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        Token otherlv_10=null;
        Token otherlv_12=null;
        Token otherlv_14=null;
        Token otherlv_16=null;
        Token otherlv_18=null;
        Token otherlv_20=null;
        Token otherlv_22=null;
        Token otherlv_24=null;
        Token otherlv_26=null;
        Token otherlv_27=null;
        Token otherlv_29=null;
        Token otherlv_31=null;
        Token otherlv_32=null;
        Token otherlv_33=null;
        Token otherlv_35=null;
        Token otherlv_37=null;
        Token otherlv_38=null;
        Token otherlv_39=null;
        Token otherlv_41=null;
        Token otherlv_43=null;
        Token otherlv_44=null;
        Token otherlv_46=null;
        Token otherlv_48=null;
        Token otherlv_50=null;
        Token otherlv_52=null;
        Token otherlv_54=null;
        Token otherlv_56=null;
        Token otherlv_58=null;
        Token otherlv_60=null;
        AntlrDatatypeRuleToken lv_name_2_0 = null;

        AntlrDatatypeRuleToken lv_attType_5_0 = null;

        AntlrDatatypeRuleToken lv_rwType_7_0 = null;

        AntlrDatatypeRuleToken lv_displayLevel_9_0 = null;

        AntlrDatatypeRuleToken lv_polledPeriod_11_0 = null;

        AntlrDatatypeRuleToken lv_maxX_13_0 = null;

        AntlrDatatypeRuleToken lv_maxY_15_0 = null;

        AntlrDatatypeRuleToken lv_associatedAttr_17_0 = null;

        AntlrDatatypeRuleToken lv_memorized_19_0 = null;

        AntlrDatatypeRuleToken lv_memorizedAtInit_21_0 = null;

        AntlrDatatypeRuleToken lv_allocReadMember_23_0 = null;

        AntlrDatatypeRuleToken lv_isDynamic_25_0 = null;

        AntlrDatatypeRuleToken lv_enumLabels_28_0 = null;

        AntlrDatatypeRuleToken lv_enumLabels_30_0 = null;

        AntlrDatatypeRuleToken lv_readExcludedStates_34_0 = null;

        AntlrDatatypeRuleToken lv_readExcludedStates_36_0 = null;

        AntlrDatatypeRuleToken lv_writeExcludedStates_40_0 = null;

        AntlrDatatypeRuleToken lv_writeExcludedStates_42_0 = null;

        EObject lv_dataType_45_0 = null;

        EObject lv_changeEvent_47_0 = null;

        EObject lv_archiveEvent_49_0 = null;

        EObject lv_dataReadyEvent_51_0 = null;

        EObject lv_status_53_0 = null;

        EObject lv_properties_55_0 = null;

        EObject lv_eventCriteria_57_0 = null;

        EObject lv_evArchiveCriteria_59_0 = null;



        	enterRule();

        try {
            // InternalPogoDsl.g:3043:2: ( ( () otherlv_1= 'Attribute' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'attType' ( (lv_attType_5_0= ruleAttrType ) ) )? (otherlv_6= 'rwType' ( (lv_rwType_7_0= ruleRW_Type ) ) )? (otherlv_8= 'displayLevel' ( (lv_displayLevel_9_0= ruleDisplayLevel ) ) )? (otherlv_10= 'polledPeriod' ( (lv_polledPeriod_11_0= ruleEString ) ) )? (otherlv_12= 'maxX' ( (lv_maxX_13_0= ruleEString ) ) )? (otherlv_14= 'maxY' ( (lv_maxY_15_0= ruleEString ) ) )? (otherlv_16= 'associatedAttr' ( (lv_associatedAttr_17_0= ruleEString ) ) )? (otherlv_18= 'memorized' ( (lv_memorized_19_0= ruleEString ) ) )? (otherlv_20= 'memorizedAtInit' ( (lv_memorizedAtInit_21_0= ruleEString ) ) )? (otherlv_22= 'allocReadMember' ( (lv_allocReadMember_23_0= ruleEString ) ) )? (otherlv_24= 'isDynamic' ( (lv_isDynamic_25_0= ruleEString ) ) )? (otherlv_26= 'enumLabels' otherlv_27= '{' ( (lv_enumLabels_28_0= ruleEString ) ) (otherlv_29= ',' ( (lv_enumLabels_30_0= ruleEString ) ) )* otherlv_31= '}' )? (otherlv_32= 'readExcludedStates' otherlv_33= '{' ( (lv_readExcludedStates_34_0= ruleEString ) ) (otherlv_35= ',' ( (lv_readExcludedStates_36_0= ruleEString ) ) )* otherlv_37= '}' )? (otherlv_38= 'writeExcludedStates' otherlv_39= '{' ( (lv_writeExcludedStates_40_0= ruleEString ) ) (otherlv_41= ',' ( (lv_writeExcludedStates_42_0= ruleEString ) ) )* otherlv_43= '}' )? (otherlv_44= 'dataType' ( (lv_dataType_45_0= ruleType ) ) )? (otherlv_46= 'changeEvent' ( (lv_changeEvent_47_0= ruleFireEvents ) ) )? (otherlv_48= 'archiveEvent' ( (lv_archiveEvent_49_0= ruleFireEvents ) ) )? (otherlv_50= 'dataReadyEvent' ( (lv_dataReadyEvent_51_0= ruleFireEvents ) ) )? (otherlv_52= 'status' ( (lv_status_53_0= ruleInheritanceStatus ) ) )? (otherlv_54= 'properties' ( (lv_properties_55_0= ruleAttrProperties ) ) )? (otherlv_56= 'eventCriteria' ( (lv_eventCriteria_57_0= ruleEventCriteria ) ) )? (otherlv_58= 'evArchiveCriteria' ( (lv_evArchiveCriteria_59_0= ruleEventCriteria ) ) )? otherlv_60= '}' ) )
            // InternalPogoDsl.g:3044:2: ( () otherlv_1= 'Attribute' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'attType' ( (lv_attType_5_0= ruleAttrType ) ) )? (otherlv_6= 'rwType' ( (lv_rwType_7_0= ruleRW_Type ) ) )? (otherlv_8= 'displayLevel' ( (lv_displayLevel_9_0= ruleDisplayLevel ) ) )? (otherlv_10= 'polledPeriod' ( (lv_polledPeriod_11_0= ruleEString ) ) )? (otherlv_12= 'maxX' ( (lv_maxX_13_0= ruleEString ) ) )? (otherlv_14= 'maxY' ( (lv_maxY_15_0= ruleEString ) ) )? (otherlv_16= 'associatedAttr' ( (lv_associatedAttr_17_0= ruleEString ) ) )? (otherlv_18= 'memorized' ( (lv_memorized_19_0= ruleEString ) ) )? (otherlv_20= 'memorizedAtInit' ( (lv_memorizedAtInit_21_0= ruleEString ) ) )? (otherlv_22= 'allocReadMember' ( (lv_allocReadMember_23_0= ruleEString ) ) )? (otherlv_24= 'isDynamic' ( (lv_isDynamic_25_0= ruleEString ) ) )? (otherlv_26= 'enumLabels' otherlv_27= '{' ( (lv_enumLabels_28_0= ruleEString ) ) (otherlv_29= ',' ( (lv_enumLabels_30_0= ruleEString ) ) )* otherlv_31= '}' )? (otherlv_32= 'readExcludedStates' otherlv_33= '{' ( (lv_readExcludedStates_34_0= ruleEString ) ) (otherlv_35= ',' ( (lv_readExcludedStates_36_0= ruleEString ) ) )* otherlv_37= '}' )? (otherlv_38= 'writeExcludedStates' otherlv_39= '{' ( (lv_writeExcludedStates_40_0= ruleEString ) ) (otherlv_41= ',' ( (lv_writeExcludedStates_42_0= ruleEString ) ) )* otherlv_43= '}' )? (otherlv_44= 'dataType' ( (lv_dataType_45_0= ruleType ) ) )? (otherlv_46= 'changeEvent' ( (lv_changeEvent_47_0= ruleFireEvents ) ) )? (otherlv_48= 'archiveEvent' ( (lv_archiveEvent_49_0= ruleFireEvents ) ) )? (otherlv_50= 'dataReadyEvent' ( (lv_dataReadyEvent_51_0= ruleFireEvents ) ) )? (otherlv_52= 'status' ( (lv_status_53_0= ruleInheritanceStatus ) ) )? (otherlv_54= 'properties' ( (lv_properties_55_0= ruleAttrProperties ) ) )? (otherlv_56= 'eventCriteria' ( (lv_eventCriteria_57_0= ruleEventCriteria ) ) )? (otherlv_58= 'evArchiveCriteria' ( (lv_evArchiveCriteria_59_0= ruleEventCriteria ) ) )? otherlv_60= '}' )
            {
            // InternalPogoDsl.g:3044:2: ( () otherlv_1= 'Attribute' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'attType' ( (lv_attType_5_0= ruleAttrType ) ) )? (otherlv_6= 'rwType' ( (lv_rwType_7_0= ruleRW_Type ) ) )? (otherlv_8= 'displayLevel' ( (lv_displayLevel_9_0= ruleDisplayLevel ) ) )? (otherlv_10= 'polledPeriod' ( (lv_polledPeriod_11_0= ruleEString ) ) )? (otherlv_12= 'maxX' ( (lv_maxX_13_0= ruleEString ) ) )? (otherlv_14= 'maxY' ( (lv_maxY_15_0= ruleEString ) ) )? (otherlv_16= 'associatedAttr' ( (lv_associatedAttr_17_0= ruleEString ) ) )? (otherlv_18= 'memorized' ( (lv_memorized_19_0= ruleEString ) ) )? (otherlv_20= 'memorizedAtInit' ( (lv_memorizedAtInit_21_0= ruleEString ) ) )? (otherlv_22= 'allocReadMember' ( (lv_allocReadMember_23_0= ruleEString ) ) )? (otherlv_24= 'isDynamic' ( (lv_isDynamic_25_0= ruleEString ) ) )? (otherlv_26= 'enumLabels' otherlv_27= '{' ( (lv_enumLabels_28_0= ruleEString ) ) (otherlv_29= ',' ( (lv_enumLabels_30_0= ruleEString ) ) )* otherlv_31= '}' )? (otherlv_32= 'readExcludedStates' otherlv_33= '{' ( (lv_readExcludedStates_34_0= ruleEString ) ) (otherlv_35= ',' ( (lv_readExcludedStates_36_0= ruleEString ) ) )* otherlv_37= '}' )? (otherlv_38= 'writeExcludedStates' otherlv_39= '{' ( (lv_writeExcludedStates_40_0= ruleEString ) ) (otherlv_41= ',' ( (lv_writeExcludedStates_42_0= ruleEString ) ) )* otherlv_43= '}' )? (otherlv_44= 'dataType' ( (lv_dataType_45_0= ruleType ) ) )? (otherlv_46= 'changeEvent' ( (lv_changeEvent_47_0= ruleFireEvents ) ) )? (otherlv_48= 'archiveEvent' ( (lv_archiveEvent_49_0= ruleFireEvents ) ) )? (otherlv_50= 'dataReadyEvent' ( (lv_dataReadyEvent_51_0= ruleFireEvents ) ) )? (otherlv_52= 'status' ( (lv_status_53_0= ruleInheritanceStatus ) ) )? (otherlv_54= 'properties' ( (lv_properties_55_0= ruleAttrProperties ) ) )? (otherlv_56= 'eventCriteria' ( (lv_eventCriteria_57_0= ruleEventCriteria ) ) )? (otherlv_58= 'evArchiveCriteria' ( (lv_evArchiveCriteria_59_0= ruleEventCriteria ) ) )? otherlv_60= '}' )
            // InternalPogoDsl.g:3045:3: () otherlv_1= 'Attribute' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'attType' ( (lv_attType_5_0= ruleAttrType ) ) )? (otherlv_6= 'rwType' ( (lv_rwType_7_0= ruleRW_Type ) ) )? (otherlv_8= 'displayLevel' ( (lv_displayLevel_9_0= ruleDisplayLevel ) ) )? (otherlv_10= 'polledPeriod' ( (lv_polledPeriod_11_0= ruleEString ) ) )? (otherlv_12= 'maxX' ( (lv_maxX_13_0= ruleEString ) ) )? (otherlv_14= 'maxY' ( (lv_maxY_15_0= ruleEString ) ) )? (otherlv_16= 'associatedAttr' ( (lv_associatedAttr_17_0= ruleEString ) ) )? (otherlv_18= 'memorized' ( (lv_memorized_19_0= ruleEString ) ) )? (otherlv_20= 'memorizedAtInit' ( (lv_memorizedAtInit_21_0= ruleEString ) ) )? (otherlv_22= 'allocReadMember' ( (lv_allocReadMember_23_0= ruleEString ) ) )? (otherlv_24= 'isDynamic' ( (lv_isDynamic_25_0= ruleEString ) ) )? (otherlv_26= 'enumLabels' otherlv_27= '{' ( (lv_enumLabels_28_0= ruleEString ) ) (otherlv_29= ',' ( (lv_enumLabels_30_0= ruleEString ) ) )* otherlv_31= '}' )? (otherlv_32= 'readExcludedStates' otherlv_33= '{' ( (lv_readExcludedStates_34_0= ruleEString ) ) (otherlv_35= ',' ( (lv_readExcludedStates_36_0= ruleEString ) ) )* otherlv_37= '}' )? (otherlv_38= 'writeExcludedStates' otherlv_39= '{' ( (lv_writeExcludedStates_40_0= ruleEString ) ) (otherlv_41= ',' ( (lv_writeExcludedStates_42_0= ruleEString ) ) )* otherlv_43= '}' )? (otherlv_44= 'dataType' ( (lv_dataType_45_0= ruleType ) ) )? (otherlv_46= 'changeEvent' ( (lv_changeEvent_47_0= ruleFireEvents ) ) )? (otherlv_48= 'archiveEvent' ( (lv_archiveEvent_49_0= ruleFireEvents ) ) )? (otherlv_50= 'dataReadyEvent' ( (lv_dataReadyEvent_51_0= ruleFireEvents ) ) )? (otherlv_52= 'status' ( (lv_status_53_0= ruleInheritanceStatus ) ) )? (otherlv_54= 'properties' ( (lv_properties_55_0= ruleAttrProperties ) ) )? (otherlv_56= 'eventCriteria' ( (lv_eventCriteria_57_0= ruleEventCriteria ) ) )? (otherlv_58= 'evArchiveCriteria' ( (lv_evArchiveCriteria_59_0= ruleEventCriteria ) ) )? otherlv_60= '}'
            {
            // InternalPogoDsl.g:3045:3: ()
            // InternalPogoDsl.g:3046:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getAttributeAccess().getAttributeAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,69,FOLLOW_13); 

            			newLeafNode(otherlv_1, grammarAccess.getAttributeAccess().getAttributeKeyword_1());
            		
            // InternalPogoDsl.g:3056:3: ( (lv_name_2_0= ruleEString ) )
            // InternalPogoDsl.g:3057:4: (lv_name_2_0= ruleEString )
            {
            // InternalPogoDsl.g:3057:4: (lv_name_2_0= ruleEString )
            // InternalPogoDsl.g:3058:5: lv_name_2_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getAttributeAccess().getNameEStringParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_3);
            lv_name_2_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getAttributeRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_2_0,
            						"com.mncml.implementation.pogo.dsl.PogoDsl.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_3=(Token)match(input,12,FOLLOW_90); 

            			newLeafNode(otherlv_3, grammarAccess.getAttributeAccess().getLeftCurlyBracketKeyword_3());
            		
            // InternalPogoDsl.g:3079:3: (otherlv_4= 'attType' ( (lv_attType_5_0= ruleAttrType ) ) )?
            int alt81=2;
            int LA81_0 = input.LA(1);

            if ( (LA81_0==70) ) {
                alt81=1;
            }
            switch (alt81) {
                case 1 :
                    // InternalPogoDsl.g:3080:4: otherlv_4= 'attType' ( (lv_attType_5_0= ruleAttrType ) )
                    {
                    otherlv_4=(Token)match(input,70,FOLLOW_91); 

                    				newLeafNode(otherlv_4, grammarAccess.getAttributeAccess().getAttTypeKeyword_4_0());
                    			
                    // InternalPogoDsl.g:3084:4: ( (lv_attType_5_0= ruleAttrType ) )
                    // InternalPogoDsl.g:3085:5: (lv_attType_5_0= ruleAttrType )
                    {
                    // InternalPogoDsl.g:3085:5: (lv_attType_5_0= ruleAttrType )
                    // InternalPogoDsl.g:3086:6: lv_attType_5_0= ruleAttrType
                    {

                    						newCompositeNode(grammarAccess.getAttributeAccess().getAttTypeAttrTypeParserRuleCall_4_1_0());
                    					
                    pushFollow(FOLLOW_92);
                    lv_attType_5_0=ruleAttrType();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getAttributeRule());
                    						}
                    						set(
                    							current,
                    							"attType",
                    							lv_attType_5_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.AttrType");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalPogoDsl.g:3104:3: (otherlv_6= 'rwType' ( (lv_rwType_7_0= ruleRW_Type ) ) )?
            int alt82=2;
            int LA82_0 = input.LA(1);

            if ( (LA82_0==71) ) {
                alt82=1;
            }
            switch (alt82) {
                case 1 :
                    // InternalPogoDsl.g:3105:4: otherlv_6= 'rwType' ( (lv_rwType_7_0= ruleRW_Type ) )
                    {
                    otherlv_6=(Token)match(input,71,FOLLOW_93); 

                    				newLeafNode(otherlv_6, grammarAccess.getAttributeAccess().getRwTypeKeyword_5_0());
                    			
                    // InternalPogoDsl.g:3109:4: ( (lv_rwType_7_0= ruleRW_Type ) )
                    // InternalPogoDsl.g:3110:5: (lv_rwType_7_0= ruleRW_Type )
                    {
                    // InternalPogoDsl.g:3110:5: (lv_rwType_7_0= ruleRW_Type )
                    // InternalPogoDsl.g:3111:6: lv_rwType_7_0= ruleRW_Type
                    {

                    						newCompositeNode(grammarAccess.getAttributeAccess().getRwTypeRW_TypeParserRuleCall_5_1_0());
                    					
                    pushFollow(FOLLOW_94);
                    lv_rwType_7_0=ruleRW_Type();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getAttributeRule());
                    						}
                    						set(
                    							current,
                    							"rwType",
                    							lv_rwType_7_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.RW_Type");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalPogoDsl.g:3129:3: (otherlv_8= 'displayLevel' ( (lv_displayLevel_9_0= ruleDisplayLevel ) ) )?
            int alt83=2;
            int LA83_0 = input.LA(1);

            if ( (LA83_0==63) ) {
                alt83=1;
            }
            switch (alt83) {
                case 1 :
                    // InternalPogoDsl.g:3130:4: otherlv_8= 'displayLevel' ( (lv_displayLevel_9_0= ruleDisplayLevel ) )
                    {
                    otherlv_8=(Token)match(input,63,FOLLOW_83); 

                    				newLeafNode(otherlv_8, grammarAccess.getAttributeAccess().getDisplayLevelKeyword_6_0());
                    			
                    // InternalPogoDsl.g:3134:4: ( (lv_displayLevel_9_0= ruleDisplayLevel ) )
                    // InternalPogoDsl.g:3135:5: (lv_displayLevel_9_0= ruleDisplayLevel )
                    {
                    // InternalPogoDsl.g:3135:5: (lv_displayLevel_9_0= ruleDisplayLevel )
                    // InternalPogoDsl.g:3136:6: lv_displayLevel_9_0= ruleDisplayLevel
                    {

                    						newCompositeNode(grammarAccess.getAttributeAccess().getDisplayLevelDisplayLevelParserRuleCall_6_1_0());
                    					
                    pushFollow(FOLLOW_95);
                    lv_displayLevel_9_0=ruleDisplayLevel();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getAttributeRule());
                    						}
                    						set(
                    							current,
                    							"displayLevel",
                    							lv_displayLevel_9_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.DisplayLevel");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalPogoDsl.g:3154:3: (otherlv_10= 'polledPeriod' ( (lv_polledPeriod_11_0= ruleEString ) ) )?
            int alt84=2;
            int LA84_0 = input.LA(1);

            if ( (LA84_0==64) ) {
                alt84=1;
            }
            switch (alt84) {
                case 1 :
                    // InternalPogoDsl.g:3155:4: otherlv_10= 'polledPeriod' ( (lv_polledPeriod_11_0= ruleEString ) )
                    {
                    otherlv_10=(Token)match(input,64,FOLLOW_13); 

                    				newLeafNode(otherlv_10, grammarAccess.getAttributeAccess().getPolledPeriodKeyword_7_0());
                    			
                    // InternalPogoDsl.g:3159:4: ( (lv_polledPeriod_11_0= ruleEString ) )
                    // InternalPogoDsl.g:3160:5: (lv_polledPeriod_11_0= ruleEString )
                    {
                    // InternalPogoDsl.g:3160:5: (lv_polledPeriod_11_0= ruleEString )
                    // InternalPogoDsl.g:3161:6: lv_polledPeriod_11_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getAttributeAccess().getPolledPeriodEStringParserRuleCall_7_1_0());
                    					
                    pushFollow(FOLLOW_96);
                    lv_polledPeriod_11_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getAttributeRule());
                    						}
                    						set(
                    							current,
                    							"polledPeriod",
                    							lv_polledPeriod_11_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalPogoDsl.g:3179:3: (otherlv_12= 'maxX' ( (lv_maxX_13_0= ruleEString ) ) )?
            int alt85=2;
            int LA85_0 = input.LA(1);

            if ( (LA85_0==72) ) {
                alt85=1;
            }
            switch (alt85) {
                case 1 :
                    // InternalPogoDsl.g:3180:4: otherlv_12= 'maxX' ( (lv_maxX_13_0= ruleEString ) )
                    {
                    otherlv_12=(Token)match(input,72,FOLLOW_13); 

                    				newLeafNode(otherlv_12, grammarAccess.getAttributeAccess().getMaxXKeyword_8_0());
                    			
                    // InternalPogoDsl.g:3184:4: ( (lv_maxX_13_0= ruleEString ) )
                    // InternalPogoDsl.g:3185:5: (lv_maxX_13_0= ruleEString )
                    {
                    // InternalPogoDsl.g:3185:5: (lv_maxX_13_0= ruleEString )
                    // InternalPogoDsl.g:3186:6: lv_maxX_13_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getAttributeAccess().getMaxXEStringParserRuleCall_8_1_0());
                    					
                    pushFollow(FOLLOW_97);
                    lv_maxX_13_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getAttributeRule());
                    						}
                    						set(
                    							current,
                    							"maxX",
                    							lv_maxX_13_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalPogoDsl.g:3204:3: (otherlv_14= 'maxY' ( (lv_maxY_15_0= ruleEString ) ) )?
            int alt86=2;
            int LA86_0 = input.LA(1);

            if ( (LA86_0==73) ) {
                alt86=1;
            }
            switch (alt86) {
                case 1 :
                    // InternalPogoDsl.g:3205:4: otherlv_14= 'maxY' ( (lv_maxY_15_0= ruleEString ) )
                    {
                    otherlv_14=(Token)match(input,73,FOLLOW_13); 

                    				newLeafNode(otherlv_14, grammarAccess.getAttributeAccess().getMaxYKeyword_9_0());
                    			
                    // InternalPogoDsl.g:3209:4: ( (lv_maxY_15_0= ruleEString ) )
                    // InternalPogoDsl.g:3210:5: (lv_maxY_15_0= ruleEString )
                    {
                    // InternalPogoDsl.g:3210:5: (lv_maxY_15_0= ruleEString )
                    // InternalPogoDsl.g:3211:6: lv_maxY_15_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getAttributeAccess().getMaxYEStringParserRuleCall_9_1_0());
                    					
                    pushFollow(FOLLOW_98);
                    lv_maxY_15_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getAttributeRule());
                    						}
                    						set(
                    							current,
                    							"maxY",
                    							lv_maxY_15_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalPogoDsl.g:3229:3: (otherlv_16= 'associatedAttr' ( (lv_associatedAttr_17_0= ruleEString ) ) )?
            int alt87=2;
            int LA87_0 = input.LA(1);

            if ( (LA87_0==74) ) {
                alt87=1;
            }
            switch (alt87) {
                case 1 :
                    // InternalPogoDsl.g:3230:4: otherlv_16= 'associatedAttr' ( (lv_associatedAttr_17_0= ruleEString ) )
                    {
                    otherlv_16=(Token)match(input,74,FOLLOW_13); 

                    				newLeafNode(otherlv_16, grammarAccess.getAttributeAccess().getAssociatedAttrKeyword_10_0());
                    			
                    // InternalPogoDsl.g:3234:4: ( (lv_associatedAttr_17_0= ruleEString ) )
                    // InternalPogoDsl.g:3235:5: (lv_associatedAttr_17_0= ruleEString )
                    {
                    // InternalPogoDsl.g:3235:5: (lv_associatedAttr_17_0= ruleEString )
                    // InternalPogoDsl.g:3236:6: lv_associatedAttr_17_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getAttributeAccess().getAssociatedAttrEStringParserRuleCall_10_1_0());
                    					
                    pushFollow(FOLLOW_99);
                    lv_associatedAttr_17_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getAttributeRule());
                    						}
                    						set(
                    							current,
                    							"associatedAttr",
                    							lv_associatedAttr_17_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalPogoDsl.g:3254:3: (otherlv_18= 'memorized' ( (lv_memorized_19_0= ruleEString ) ) )?
            int alt88=2;
            int LA88_0 = input.LA(1);

            if ( (LA88_0==75) ) {
                alt88=1;
            }
            switch (alt88) {
                case 1 :
                    // InternalPogoDsl.g:3255:4: otherlv_18= 'memorized' ( (lv_memorized_19_0= ruleEString ) )
                    {
                    otherlv_18=(Token)match(input,75,FOLLOW_13); 

                    				newLeafNode(otherlv_18, grammarAccess.getAttributeAccess().getMemorizedKeyword_11_0());
                    			
                    // InternalPogoDsl.g:3259:4: ( (lv_memorized_19_0= ruleEString ) )
                    // InternalPogoDsl.g:3260:5: (lv_memorized_19_0= ruleEString )
                    {
                    // InternalPogoDsl.g:3260:5: (lv_memorized_19_0= ruleEString )
                    // InternalPogoDsl.g:3261:6: lv_memorized_19_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getAttributeAccess().getMemorizedEStringParserRuleCall_11_1_0());
                    					
                    pushFollow(FOLLOW_100);
                    lv_memorized_19_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getAttributeRule());
                    						}
                    						set(
                    							current,
                    							"memorized",
                    							lv_memorized_19_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalPogoDsl.g:3279:3: (otherlv_20= 'memorizedAtInit' ( (lv_memorizedAtInit_21_0= ruleEString ) ) )?
            int alt89=2;
            int LA89_0 = input.LA(1);

            if ( (LA89_0==76) ) {
                alt89=1;
            }
            switch (alt89) {
                case 1 :
                    // InternalPogoDsl.g:3280:4: otherlv_20= 'memorizedAtInit' ( (lv_memorizedAtInit_21_0= ruleEString ) )
                    {
                    otherlv_20=(Token)match(input,76,FOLLOW_13); 

                    				newLeafNode(otherlv_20, grammarAccess.getAttributeAccess().getMemorizedAtInitKeyword_12_0());
                    			
                    // InternalPogoDsl.g:3284:4: ( (lv_memorizedAtInit_21_0= ruleEString ) )
                    // InternalPogoDsl.g:3285:5: (lv_memorizedAtInit_21_0= ruleEString )
                    {
                    // InternalPogoDsl.g:3285:5: (lv_memorizedAtInit_21_0= ruleEString )
                    // InternalPogoDsl.g:3286:6: lv_memorizedAtInit_21_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getAttributeAccess().getMemorizedAtInitEStringParserRuleCall_12_1_0());
                    					
                    pushFollow(FOLLOW_101);
                    lv_memorizedAtInit_21_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getAttributeRule());
                    						}
                    						set(
                    							current,
                    							"memorizedAtInit",
                    							lv_memorizedAtInit_21_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalPogoDsl.g:3304:3: (otherlv_22= 'allocReadMember' ( (lv_allocReadMember_23_0= ruleEString ) ) )?
            int alt90=2;
            int LA90_0 = input.LA(1);

            if ( (LA90_0==77) ) {
                alt90=1;
            }
            switch (alt90) {
                case 1 :
                    // InternalPogoDsl.g:3305:4: otherlv_22= 'allocReadMember' ( (lv_allocReadMember_23_0= ruleEString ) )
                    {
                    otherlv_22=(Token)match(input,77,FOLLOW_13); 

                    				newLeafNode(otherlv_22, grammarAccess.getAttributeAccess().getAllocReadMemberKeyword_13_0());
                    			
                    // InternalPogoDsl.g:3309:4: ( (lv_allocReadMember_23_0= ruleEString ) )
                    // InternalPogoDsl.g:3310:5: (lv_allocReadMember_23_0= ruleEString )
                    {
                    // InternalPogoDsl.g:3310:5: (lv_allocReadMember_23_0= ruleEString )
                    // InternalPogoDsl.g:3311:6: lv_allocReadMember_23_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getAttributeAccess().getAllocReadMemberEStringParserRuleCall_13_1_0());
                    					
                    pushFollow(FOLLOW_102);
                    lv_allocReadMember_23_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getAttributeRule());
                    						}
                    						set(
                    							current,
                    							"allocReadMember",
                    							lv_allocReadMember_23_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalPogoDsl.g:3329:3: (otherlv_24= 'isDynamic' ( (lv_isDynamic_25_0= ruleEString ) ) )?
            int alt91=2;
            int LA91_0 = input.LA(1);

            if ( (LA91_0==65) ) {
                alt91=1;
            }
            switch (alt91) {
                case 1 :
                    // InternalPogoDsl.g:3330:4: otherlv_24= 'isDynamic' ( (lv_isDynamic_25_0= ruleEString ) )
                    {
                    otherlv_24=(Token)match(input,65,FOLLOW_13); 

                    				newLeafNode(otherlv_24, grammarAccess.getAttributeAccess().getIsDynamicKeyword_14_0());
                    			
                    // InternalPogoDsl.g:3334:4: ( (lv_isDynamic_25_0= ruleEString ) )
                    // InternalPogoDsl.g:3335:5: (lv_isDynamic_25_0= ruleEString )
                    {
                    // InternalPogoDsl.g:3335:5: (lv_isDynamic_25_0= ruleEString )
                    // InternalPogoDsl.g:3336:6: lv_isDynamic_25_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getAttributeAccess().getIsDynamicEStringParserRuleCall_14_1_0());
                    					
                    pushFollow(FOLLOW_103);
                    lv_isDynamic_25_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getAttributeRule());
                    						}
                    						set(
                    							current,
                    							"isDynamic",
                    							lv_isDynamic_25_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalPogoDsl.g:3354:3: (otherlv_26= 'enumLabels' otherlv_27= '{' ( (lv_enumLabels_28_0= ruleEString ) ) (otherlv_29= ',' ( (lv_enumLabels_30_0= ruleEString ) ) )* otherlv_31= '}' )?
            int alt93=2;
            int LA93_0 = input.LA(1);

            if ( (LA93_0==78) ) {
                alt93=1;
            }
            switch (alt93) {
                case 1 :
                    // InternalPogoDsl.g:3355:4: otherlv_26= 'enumLabels' otherlv_27= '{' ( (lv_enumLabels_28_0= ruleEString ) ) (otherlv_29= ',' ( (lv_enumLabels_30_0= ruleEString ) ) )* otherlv_31= '}'
                    {
                    otherlv_26=(Token)match(input,78,FOLLOW_3); 

                    				newLeafNode(otherlv_26, grammarAccess.getAttributeAccess().getEnumLabelsKeyword_15_0());
                    			
                    otherlv_27=(Token)match(input,12,FOLLOW_13); 

                    				newLeafNode(otherlv_27, grammarAccess.getAttributeAccess().getLeftCurlyBracketKeyword_15_1());
                    			
                    // InternalPogoDsl.g:3363:4: ( (lv_enumLabels_28_0= ruleEString ) )
                    // InternalPogoDsl.g:3364:5: (lv_enumLabels_28_0= ruleEString )
                    {
                    // InternalPogoDsl.g:3364:5: (lv_enumLabels_28_0= ruleEString )
                    // InternalPogoDsl.g:3365:6: lv_enumLabels_28_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getAttributeAccess().getEnumLabelsEStringParserRuleCall_15_2_0());
                    					
                    pushFollow(FOLLOW_6);
                    lv_enumLabels_28_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getAttributeRule());
                    						}
                    						add(
                    							current,
                    							"enumLabels",
                    							lv_enumLabels_28_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalPogoDsl.g:3382:4: (otherlv_29= ',' ( (lv_enumLabels_30_0= ruleEString ) ) )*
                    loop92:
                    do {
                        int alt92=2;
                        int LA92_0 = input.LA(1);

                        if ( (LA92_0==14) ) {
                            alt92=1;
                        }


                        switch (alt92) {
                    	case 1 :
                    	    // InternalPogoDsl.g:3383:5: otherlv_29= ',' ( (lv_enumLabels_30_0= ruleEString ) )
                    	    {
                    	    otherlv_29=(Token)match(input,14,FOLLOW_13); 

                    	    					newLeafNode(otherlv_29, grammarAccess.getAttributeAccess().getCommaKeyword_15_3_0());
                    	    				
                    	    // InternalPogoDsl.g:3387:5: ( (lv_enumLabels_30_0= ruleEString ) )
                    	    // InternalPogoDsl.g:3388:6: (lv_enumLabels_30_0= ruleEString )
                    	    {
                    	    // InternalPogoDsl.g:3388:6: (lv_enumLabels_30_0= ruleEString )
                    	    // InternalPogoDsl.g:3389:7: lv_enumLabels_30_0= ruleEString
                    	    {

                    	    							newCompositeNode(grammarAccess.getAttributeAccess().getEnumLabelsEStringParserRuleCall_15_3_1_0());
                    	    						
                    	    pushFollow(FOLLOW_6);
                    	    lv_enumLabels_30_0=ruleEString();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getAttributeRule());
                    	    							}
                    	    							add(
                    	    								current,
                    	    								"enumLabels",
                    	    								lv_enumLabels_30_0,
                    	    								"com.mncml.implementation.pogo.dsl.PogoDsl.EString");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop92;
                        }
                    } while (true);

                    otherlv_31=(Token)match(input,15,FOLLOW_104); 

                    				newLeafNode(otherlv_31, grammarAccess.getAttributeAccess().getRightCurlyBracketKeyword_15_4());
                    			

                    }
                    break;

            }

            // InternalPogoDsl.g:3412:3: (otherlv_32= 'readExcludedStates' otherlv_33= '{' ( (lv_readExcludedStates_34_0= ruleEString ) ) (otherlv_35= ',' ( (lv_readExcludedStates_36_0= ruleEString ) ) )* otherlv_37= '}' )?
            int alt95=2;
            int LA95_0 = input.LA(1);

            if ( (LA95_0==79) ) {
                alt95=1;
            }
            switch (alt95) {
                case 1 :
                    // InternalPogoDsl.g:3413:4: otherlv_32= 'readExcludedStates' otherlv_33= '{' ( (lv_readExcludedStates_34_0= ruleEString ) ) (otherlv_35= ',' ( (lv_readExcludedStates_36_0= ruleEString ) ) )* otherlv_37= '}'
                    {
                    otherlv_32=(Token)match(input,79,FOLLOW_3); 

                    				newLeafNode(otherlv_32, grammarAccess.getAttributeAccess().getReadExcludedStatesKeyword_16_0());
                    			
                    otherlv_33=(Token)match(input,12,FOLLOW_13); 

                    				newLeafNode(otherlv_33, grammarAccess.getAttributeAccess().getLeftCurlyBracketKeyword_16_1());
                    			
                    // InternalPogoDsl.g:3421:4: ( (lv_readExcludedStates_34_0= ruleEString ) )
                    // InternalPogoDsl.g:3422:5: (lv_readExcludedStates_34_0= ruleEString )
                    {
                    // InternalPogoDsl.g:3422:5: (lv_readExcludedStates_34_0= ruleEString )
                    // InternalPogoDsl.g:3423:6: lv_readExcludedStates_34_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getAttributeAccess().getReadExcludedStatesEStringParserRuleCall_16_2_0());
                    					
                    pushFollow(FOLLOW_6);
                    lv_readExcludedStates_34_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getAttributeRule());
                    						}
                    						add(
                    							current,
                    							"readExcludedStates",
                    							lv_readExcludedStates_34_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalPogoDsl.g:3440:4: (otherlv_35= ',' ( (lv_readExcludedStates_36_0= ruleEString ) ) )*
                    loop94:
                    do {
                        int alt94=2;
                        int LA94_0 = input.LA(1);

                        if ( (LA94_0==14) ) {
                            alt94=1;
                        }


                        switch (alt94) {
                    	case 1 :
                    	    // InternalPogoDsl.g:3441:5: otherlv_35= ',' ( (lv_readExcludedStates_36_0= ruleEString ) )
                    	    {
                    	    otherlv_35=(Token)match(input,14,FOLLOW_13); 

                    	    					newLeafNode(otherlv_35, grammarAccess.getAttributeAccess().getCommaKeyword_16_3_0());
                    	    				
                    	    // InternalPogoDsl.g:3445:5: ( (lv_readExcludedStates_36_0= ruleEString ) )
                    	    // InternalPogoDsl.g:3446:6: (lv_readExcludedStates_36_0= ruleEString )
                    	    {
                    	    // InternalPogoDsl.g:3446:6: (lv_readExcludedStates_36_0= ruleEString )
                    	    // InternalPogoDsl.g:3447:7: lv_readExcludedStates_36_0= ruleEString
                    	    {

                    	    							newCompositeNode(grammarAccess.getAttributeAccess().getReadExcludedStatesEStringParserRuleCall_16_3_1_0());
                    	    						
                    	    pushFollow(FOLLOW_6);
                    	    lv_readExcludedStates_36_0=ruleEString();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getAttributeRule());
                    	    							}
                    	    							add(
                    	    								current,
                    	    								"readExcludedStates",
                    	    								lv_readExcludedStates_36_0,
                    	    								"com.mncml.implementation.pogo.dsl.PogoDsl.EString");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop94;
                        }
                    } while (true);

                    otherlv_37=(Token)match(input,15,FOLLOW_105); 

                    				newLeafNode(otherlv_37, grammarAccess.getAttributeAccess().getRightCurlyBracketKeyword_16_4());
                    			

                    }
                    break;

            }

            // InternalPogoDsl.g:3470:3: (otherlv_38= 'writeExcludedStates' otherlv_39= '{' ( (lv_writeExcludedStates_40_0= ruleEString ) ) (otherlv_41= ',' ( (lv_writeExcludedStates_42_0= ruleEString ) ) )* otherlv_43= '}' )?
            int alt97=2;
            int LA97_0 = input.LA(1);

            if ( (LA97_0==80) ) {
                alt97=1;
            }
            switch (alt97) {
                case 1 :
                    // InternalPogoDsl.g:3471:4: otherlv_38= 'writeExcludedStates' otherlv_39= '{' ( (lv_writeExcludedStates_40_0= ruleEString ) ) (otherlv_41= ',' ( (lv_writeExcludedStates_42_0= ruleEString ) ) )* otherlv_43= '}'
                    {
                    otherlv_38=(Token)match(input,80,FOLLOW_3); 

                    				newLeafNode(otherlv_38, grammarAccess.getAttributeAccess().getWriteExcludedStatesKeyword_17_0());
                    			
                    otherlv_39=(Token)match(input,12,FOLLOW_13); 

                    				newLeafNode(otherlv_39, grammarAccess.getAttributeAccess().getLeftCurlyBracketKeyword_17_1());
                    			
                    // InternalPogoDsl.g:3479:4: ( (lv_writeExcludedStates_40_0= ruleEString ) )
                    // InternalPogoDsl.g:3480:5: (lv_writeExcludedStates_40_0= ruleEString )
                    {
                    // InternalPogoDsl.g:3480:5: (lv_writeExcludedStates_40_0= ruleEString )
                    // InternalPogoDsl.g:3481:6: lv_writeExcludedStates_40_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getAttributeAccess().getWriteExcludedStatesEStringParserRuleCall_17_2_0());
                    					
                    pushFollow(FOLLOW_6);
                    lv_writeExcludedStates_40_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getAttributeRule());
                    						}
                    						add(
                    							current,
                    							"writeExcludedStates",
                    							lv_writeExcludedStates_40_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalPogoDsl.g:3498:4: (otherlv_41= ',' ( (lv_writeExcludedStates_42_0= ruleEString ) ) )*
                    loop96:
                    do {
                        int alt96=2;
                        int LA96_0 = input.LA(1);

                        if ( (LA96_0==14) ) {
                            alt96=1;
                        }


                        switch (alt96) {
                    	case 1 :
                    	    // InternalPogoDsl.g:3499:5: otherlv_41= ',' ( (lv_writeExcludedStates_42_0= ruleEString ) )
                    	    {
                    	    otherlv_41=(Token)match(input,14,FOLLOW_13); 

                    	    					newLeafNode(otherlv_41, grammarAccess.getAttributeAccess().getCommaKeyword_17_3_0());
                    	    				
                    	    // InternalPogoDsl.g:3503:5: ( (lv_writeExcludedStates_42_0= ruleEString ) )
                    	    // InternalPogoDsl.g:3504:6: (lv_writeExcludedStates_42_0= ruleEString )
                    	    {
                    	    // InternalPogoDsl.g:3504:6: (lv_writeExcludedStates_42_0= ruleEString )
                    	    // InternalPogoDsl.g:3505:7: lv_writeExcludedStates_42_0= ruleEString
                    	    {

                    	    							newCompositeNode(grammarAccess.getAttributeAccess().getWriteExcludedStatesEStringParserRuleCall_17_3_1_0());
                    	    						
                    	    pushFollow(FOLLOW_6);
                    	    lv_writeExcludedStates_42_0=ruleEString();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getAttributeRule());
                    	    							}
                    	    							add(
                    	    								current,
                    	    								"writeExcludedStates",
                    	    								lv_writeExcludedStates_42_0,
                    	    								"com.mncml.implementation.pogo.dsl.PogoDsl.EString");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop96;
                        }
                    } while (true);

                    otherlv_43=(Token)match(input,15,FOLLOW_106); 

                    				newLeafNode(otherlv_43, grammarAccess.getAttributeAccess().getRightCurlyBracketKeyword_17_4());
                    			

                    }
                    break;

            }

            // InternalPogoDsl.g:3528:3: (otherlv_44= 'dataType' ( (lv_dataType_45_0= ruleType ) ) )?
            int alt98=2;
            int LA98_0 = input.LA(1);

            if ( (LA98_0==81) ) {
                alt98=1;
            }
            switch (alt98) {
                case 1 :
                    // InternalPogoDsl.g:3529:4: otherlv_44= 'dataType' ( (lv_dataType_45_0= ruleType ) )
                    {
                    otherlv_44=(Token)match(input,81,FOLLOW_107); 

                    				newLeafNode(otherlv_44, grammarAccess.getAttributeAccess().getDataTypeKeyword_18_0());
                    			
                    // InternalPogoDsl.g:3533:4: ( (lv_dataType_45_0= ruleType ) )
                    // InternalPogoDsl.g:3534:5: (lv_dataType_45_0= ruleType )
                    {
                    // InternalPogoDsl.g:3534:5: (lv_dataType_45_0= ruleType )
                    // InternalPogoDsl.g:3535:6: lv_dataType_45_0= ruleType
                    {

                    						newCompositeNode(grammarAccess.getAttributeAccess().getDataTypeTypeParserRuleCall_18_1_0());
                    					
                    pushFollow(FOLLOW_108);
                    lv_dataType_45_0=ruleType();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getAttributeRule());
                    						}
                    						set(
                    							current,
                    							"dataType",
                    							lv_dataType_45_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.Type");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalPogoDsl.g:3553:3: (otherlv_46= 'changeEvent' ( (lv_changeEvent_47_0= ruleFireEvents ) ) )?
            int alt99=2;
            int LA99_0 = input.LA(1);

            if ( (LA99_0==82) ) {
                alt99=1;
            }
            switch (alt99) {
                case 1 :
                    // InternalPogoDsl.g:3554:4: otherlv_46= 'changeEvent' ( (lv_changeEvent_47_0= ruleFireEvents ) )
                    {
                    otherlv_46=(Token)match(input,82,FOLLOW_109); 

                    				newLeafNode(otherlv_46, grammarAccess.getAttributeAccess().getChangeEventKeyword_19_0());
                    			
                    // InternalPogoDsl.g:3558:4: ( (lv_changeEvent_47_0= ruleFireEvents ) )
                    // InternalPogoDsl.g:3559:5: (lv_changeEvent_47_0= ruleFireEvents )
                    {
                    // InternalPogoDsl.g:3559:5: (lv_changeEvent_47_0= ruleFireEvents )
                    // InternalPogoDsl.g:3560:6: lv_changeEvent_47_0= ruleFireEvents
                    {

                    						newCompositeNode(grammarAccess.getAttributeAccess().getChangeEventFireEventsParserRuleCall_19_1_0());
                    					
                    pushFollow(FOLLOW_110);
                    lv_changeEvent_47_0=ruleFireEvents();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getAttributeRule());
                    						}
                    						set(
                    							current,
                    							"changeEvent",
                    							lv_changeEvent_47_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.FireEvents");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalPogoDsl.g:3578:3: (otherlv_48= 'archiveEvent' ( (lv_archiveEvent_49_0= ruleFireEvents ) ) )?
            int alt100=2;
            int LA100_0 = input.LA(1);

            if ( (LA100_0==83) ) {
                alt100=1;
            }
            switch (alt100) {
                case 1 :
                    // InternalPogoDsl.g:3579:4: otherlv_48= 'archiveEvent' ( (lv_archiveEvent_49_0= ruleFireEvents ) )
                    {
                    otherlv_48=(Token)match(input,83,FOLLOW_109); 

                    				newLeafNode(otherlv_48, grammarAccess.getAttributeAccess().getArchiveEventKeyword_20_0());
                    			
                    // InternalPogoDsl.g:3583:4: ( (lv_archiveEvent_49_0= ruleFireEvents ) )
                    // InternalPogoDsl.g:3584:5: (lv_archiveEvent_49_0= ruleFireEvents )
                    {
                    // InternalPogoDsl.g:3584:5: (lv_archiveEvent_49_0= ruleFireEvents )
                    // InternalPogoDsl.g:3585:6: lv_archiveEvent_49_0= ruleFireEvents
                    {

                    						newCompositeNode(grammarAccess.getAttributeAccess().getArchiveEventFireEventsParserRuleCall_20_1_0());
                    					
                    pushFollow(FOLLOW_111);
                    lv_archiveEvent_49_0=ruleFireEvents();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getAttributeRule());
                    						}
                    						set(
                    							current,
                    							"archiveEvent",
                    							lv_archiveEvent_49_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.FireEvents");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalPogoDsl.g:3603:3: (otherlv_50= 'dataReadyEvent' ( (lv_dataReadyEvent_51_0= ruleFireEvents ) ) )?
            int alt101=2;
            int LA101_0 = input.LA(1);

            if ( (LA101_0==84) ) {
                alt101=1;
            }
            switch (alt101) {
                case 1 :
                    // InternalPogoDsl.g:3604:4: otherlv_50= 'dataReadyEvent' ( (lv_dataReadyEvent_51_0= ruleFireEvents ) )
                    {
                    otherlv_50=(Token)match(input,84,FOLLOW_109); 

                    				newLeafNode(otherlv_50, grammarAccess.getAttributeAccess().getDataReadyEventKeyword_21_0());
                    			
                    // InternalPogoDsl.g:3608:4: ( (lv_dataReadyEvent_51_0= ruleFireEvents ) )
                    // InternalPogoDsl.g:3609:5: (lv_dataReadyEvent_51_0= ruleFireEvents )
                    {
                    // InternalPogoDsl.g:3609:5: (lv_dataReadyEvent_51_0= ruleFireEvents )
                    // InternalPogoDsl.g:3610:6: lv_dataReadyEvent_51_0= ruleFireEvents
                    {

                    						newCompositeNode(grammarAccess.getAttributeAccess().getDataReadyEventFireEventsParserRuleCall_21_1_0());
                    					
                    pushFollow(FOLLOW_112);
                    lv_dataReadyEvent_51_0=ruleFireEvents();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getAttributeRule());
                    						}
                    						set(
                    							current,
                    							"dataReadyEvent",
                    							lv_dataReadyEvent_51_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.FireEvents");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalPogoDsl.g:3628:3: (otherlv_52= 'status' ( (lv_status_53_0= ruleInheritanceStatus ) ) )?
            int alt102=2;
            int LA102_0 = input.LA(1);

            if ( (LA102_0==60) ) {
                alt102=1;
            }
            switch (alt102) {
                case 1 :
                    // InternalPogoDsl.g:3629:4: otherlv_52= 'status' ( (lv_status_53_0= ruleInheritanceStatus ) )
                    {
                    otherlv_52=(Token)match(input,60,FOLLOW_79); 

                    				newLeafNode(otherlv_52, grammarAccess.getAttributeAccess().getStatusKeyword_22_0());
                    			
                    // InternalPogoDsl.g:3633:4: ( (lv_status_53_0= ruleInheritanceStatus ) )
                    // InternalPogoDsl.g:3634:5: (lv_status_53_0= ruleInheritanceStatus )
                    {
                    // InternalPogoDsl.g:3634:5: (lv_status_53_0= ruleInheritanceStatus )
                    // InternalPogoDsl.g:3635:6: lv_status_53_0= ruleInheritanceStatus
                    {

                    						newCompositeNode(grammarAccess.getAttributeAccess().getStatusInheritanceStatusParserRuleCall_22_1_0());
                    					
                    pushFollow(FOLLOW_113);
                    lv_status_53_0=ruleInheritanceStatus();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getAttributeRule());
                    						}
                    						set(
                    							current,
                    							"status",
                    							lv_status_53_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.InheritanceStatus");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalPogoDsl.g:3653:3: (otherlv_54= 'properties' ( (lv_properties_55_0= ruleAttrProperties ) ) )?
            int alt103=2;
            int LA103_0 = input.LA(1);

            if ( (LA103_0==85) ) {
                alt103=1;
            }
            switch (alt103) {
                case 1 :
                    // InternalPogoDsl.g:3654:4: otherlv_54= 'properties' ( (lv_properties_55_0= ruleAttrProperties ) )
                    {
                    otherlv_54=(Token)match(input,85,FOLLOW_114); 

                    				newLeafNode(otherlv_54, grammarAccess.getAttributeAccess().getPropertiesKeyword_23_0());
                    			
                    // InternalPogoDsl.g:3658:4: ( (lv_properties_55_0= ruleAttrProperties ) )
                    // InternalPogoDsl.g:3659:5: (lv_properties_55_0= ruleAttrProperties )
                    {
                    // InternalPogoDsl.g:3659:5: (lv_properties_55_0= ruleAttrProperties )
                    // InternalPogoDsl.g:3660:6: lv_properties_55_0= ruleAttrProperties
                    {

                    						newCompositeNode(grammarAccess.getAttributeAccess().getPropertiesAttrPropertiesParserRuleCall_23_1_0());
                    					
                    pushFollow(FOLLOW_115);
                    lv_properties_55_0=ruleAttrProperties();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getAttributeRule());
                    						}
                    						set(
                    							current,
                    							"properties",
                    							lv_properties_55_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.AttrProperties");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalPogoDsl.g:3678:3: (otherlv_56= 'eventCriteria' ( (lv_eventCriteria_57_0= ruleEventCriteria ) ) )?
            int alt104=2;
            int LA104_0 = input.LA(1);

            if ( (LA104_0==86) ) {
                alt104=1;
            }
            switch (alt104) {
                case 1 :
                    // InternalPogoDsl.g:3679:4: otherlv_56= 'eventCriteria' ( (lv_eventCriteria_57_0= ruleEventCriteria ) )
                    {
                    otherlv_56=(Token)match(input,86,FOLLOW_116); 

                    				newLeafNode(otherlv_56, grammarAccess.getAttributeAccess().getEventCriteriaKeyword_24_0());
                    			
                    // InternalPogoDsl.g:3683:4: ( (lv_eventCriteria_57_0= ruleEventCriteria ) )
                    // InternalPogoDsl.g:3684:5: (lv_eventCriteria_57_0= ruleEventCriteria )
                    {
                    // InternalPogoDsl.g:3684:5: (lv_eventCriteria_57_0= ruleEventCriteria )
                    // InternalPogoDsl.g:3685:6: lv_eventCriteria_57_0= ruleEventCriteria
                    {

                    						newCompositeNode(grammarAccess.getAttributeAccess().getEventCriteriaEventCriteriaParserRuleCall_24_1_0());
                    					
                    pushFollow(FOLLOW_117);
                    lv_eventCriteria_57_0=ruleEventCriteria();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getAttributeRule());
                    						}
                    						set(
                    							current,
                    							"eventCriteria",
                    							lv_eventCriteria_57_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.EventCriteria");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalPogoDsl.g:3703:3: (otherlv_58= 'evArchiveCriteria' ( (lv_evArchiveCriteria_59_0= ruleEventCriteria ) ) )?
            int alt105=2;
            int LA105_0 = input.LA(1);

            if ( (LA105_0==87) ) {
                alt105=1;
            }
            switch (alt105) {
                case 1 :
                    // InternalPogoDsl.g:3704:4: otherlv_58= 'evArchiveCriteria' ( (lv_evArchiveCriteria_59_0= ruleEventCriteria ) )
                    {
                    otherlv_58=(Token)match(input,87,FOLLOW_116); 

                    				newLeafNode(otherlv_58, grammarAccess.getAttributeAccess().getEvArchiveCriteriaKeyword_25_0());
                    			
                    // InternalPogoDsl.g:3708:4: ( (lv_evArchiveCriteria_59_0= ruleEventCriteria ) )
                    // InternalPogoDsl.g:3709:5: (lv_evArchiveCriteria_59_0= ruleEventCriteria )
                    {
                    // InternalPogoDsl.g:3709:5: (lv_evArchiveCriteria_59_0= ruleEventCriteria )
                    // InternalPogoDsl.g:3710:6: lv_evArchiveCriteria_59_0= ruleEventCriteria
                    {

                    						newCompositeNode(grammarAccess.getAttributeAccess().getEvArchiveCriteriaEventCriteriaParserRuleCall_25_1_0());
                    					
                    pushFollow(FOLLOW_11);
                    lv_evArchiveCriteria_59_0=ruleEventCriteria();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getAttributeRule());
                    						}
                    						set(
                    							current,
                    							"evArchiveCriteria",
                    							lv_evArchiveCriteria_59_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.EventCriteria");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            otherlv_60=(Token)match(input,15,FOLLOW_2); 

            			newLeafNode(otherlv_60, grammarAccess.getAttributeAccess().getRightCurlyBracketKeyword_26());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAttribute"


    // $ANTLR start "entryRuleForwardedAttribute"
    // InternalPogoDsl.g:3736:1: entryRuleForwardedAttribute returns [EObject current=null] : iv_ruleForwardedAttribute= ruleForwardedAttribute EOF ;
    public final EObject entryRuleForwardedAttribute() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleForwardedAttribute = null;


        try {
            // InternalPogoDsl.g:3736:59: (iv_ruleForwardedAttribute= ruleForwardedAttribute EOF )
            // InternalPogoDsl.g:3737:2: iv_ruleForwardedAttribute= ruleForwardedAttribute EOF
            {
             newCompositeNode(grammarAccess.getForwardedAttributeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleForwardedAttribute=ruleForwardedAttribute();

            state._fsp--;

             current =iv_ruleForwardedAttribute; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleForwardedAttribute"


    // $ANTLR start "ruleForwardedAttribute"
    // InternalPogoDsl.g:3743:1: ruleForwardedAttribute returns [EObject current=null] : ( () otherlv_1= 'ForwardedAttribute' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'label' ( (lv_label_5_0= ruleEString ) ) )? (otherlv_6= 'status' ( (lv_status_7_0= ruleInheritanceStatus ) ) )? otherlv_8= '}' ) ;
    public final EObject ruleForwardedAttribute() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        AntlrDatatypeRuleToken lv_name_2_0 = null;

        AntlrDatatypeRuleToken lv_label_5_0 = null;

        EObject lv_status_7_0 = null;



        	enterRule();

        try {
            // InternalPogoDsl.g:3749:2: ( ( () otherlv_1= 'ForwardedAttribute' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'label' ( (lv_label_5_0= ruleEString ) ) )? (otherlv_6= 'status' ( (lv_status_7_0= ruleInheritanceStatus ) ) )? otherlv_8= '}' ) )
            // InternalPogoDsl.g:3750:2: ( () otherlv_1= 'ForwardedAttribute' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'label' ( (lv_label_5_0= ruleEString ) ) )? (otherlv_6= 'status' ( (lv_status_7_0= ruleInheritanceStatus ) ) )? otherlv_8= '}' )
            {
            // InternalPogoDsl.g:3750:2: ( () otherlv_1= 'ForwardedAttribute' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'label' ( (lv_label_5_0= ruleEString ) ) )? (otherlv_6= 'status' ( (lv_status_7_0= ruleInheritanceStatus ) ) )? otherlv_8= '}' )
            // InternalPogoDsl.g:3751:3: () otherlv_1= 'ForwardedAttribute' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'label' ( (lv_label_5_0= ruleEString ) ) )? (otherlv_6= 'status' ( (lv_status_7_0= ruleInheritanceStatus ) ) )? otherlv_8= '}'
            {
            // InternalPogoDsl.g:3751:3: ()
            // InternalPogoDsl.g:3752:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getForwardedAttributeAccess().getForwardedAttributeAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,88,FOLLOW_13); 

            			newLeafNode(otherlv_1, grammarAccess.getForwardedAttributeAccess().getForwardedAttributeKeyword_1());
            		
            // InternalPogoDsl.g:3762:3: ( (lv_name_2_0= ruleEString ) )
            // InternalPogoDsl.g:3763:4: (lv_name_2_0= ruleEString )
            {
            // InternalPogoDsl.g:3763:4: (lv_name_2_0= ruleEString )
            // InternalPogoDsl.g:3764:5: lv_name_2_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getForwardedAttributeAccess().getNameEStringParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_3);
            lv_name_2_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getForwardedAttributeRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_2_0,
            						"com.mncml.implementation.pogo.dsl.PogoDsl.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_3=(Token)match(input,12,FOLLOW_118); 

            			newLeafNode(otherlv_3, grammarAccess.getForwardedAttributeAccess().getLeftCurlyBracketKeyword_3());
            		
            // InternalPogoDsl.g:3785:3: (otherlv_4= 'label' ( (lv_label_5_0= ruleEString ) ) )?
            int alt106=2;
            int LA106_0 = input.LA(1);

            if ( (LA106_0==89) ) {
                alt106=1;
            }
            switch (alt106) {
                case 1 :
                    // InternalPogoDsl.g:3786:4: otherlv_4= 'label' ( (lv_label_5_0= ruleEString ) )
                    {
                    otherlv_4=(Token)match(input,89,FOLLOW_13); 

                    				newLeafNode(otherlv_4, grammarAccess.getForwardedAttributeAccess().getLabelKeyword_4_0());
                    			
                    // InternalPogoDsl.g:3790:4: ( (lv_label_5_0= ruleEString ) )
                    // InternalPogoDsl.g:3791:5: (lv_label_5_0= ruleEString )
                    {
                    // InternalPogoDsl.g:3791:5: (lv_label_5_0= ruleEString )
                    // InternalPogoDsl.g:3792:6: lv_label_5_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getForwardedAttributeAccess().getLabelEStringParserRuleCall_4_1_0());
                    					
                    pushFollow(FOLLOW_78);
                    lv_label_5_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getForwardedAttributeRule());
                    						}
                    						set(
                    							current,
                    							"label",
                    							lv_label_5_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalPogoDsl.g:3810:3: (otherlv_6= 'status' ( (lv_status_7_0= ruleInheritanceStatus ) ) )?
            int alt107=2;
            int LA107_0 = input.LA(1);

            if ( (LA107_0==60) ) {
                alt107=1;
            }
            switch (alt107) {
                case 1 :
                    // InternalPogoDsl.g:3811:4: otherlv_6= 'status' ( (lv_status_7_0= ruleInheritanceStatus ) )
                    {
                    otherlv_6=(Token)match(input,60,FOLLOW_79); 

                    				newLeafNode(otherlv_6, grammarAccess.getForwardedAttributeAccess().getStatusKeyword_5_0());
                    			
                    // InternalPogoDsl.g:3815:4: ( (lv_status_7_0= ruleInheritanceStatus ) )
                    // InternalPogoDsl.g:3816:5: (lv_status_7_0= ruleInheritanceStatus )
                    {
                    // InternalPogoDsl.g:3816:5: (lv_status_7_0= ruleInheritanceStatus )
                    // InternalPogoDsl.g:3817:6: lv_status_7_0= ruleInheritanceStatus
                    {

                    						newCompositeNode(grammarAccess.getForwardedAttributeAccess().getStatusInheritanceStatusParserRuleCall_5_1_0());
                    					
                    pushFollow(FOLLOW_11);
                    lv_status_7_0=ruleInheritanceStatus();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getForwardedAttributeRule());
                    						}
                    						set(
                    							current,
                    							"status",
                    							lv_status_7_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.InheritanceStatus");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            otherlv_8=(Token)match(input,15,FOLLOW_2); 

            			newLeafNode(otherlv_8, grammarAccess.getForwardedAttributeAccess().getRightCurlyBracketKeyword_6());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleForwardedAttribute"


    // $ANTLR start "entryRulePipe"
    // InternalPogoDsl.g:3843:1: entryRulePipe returns [EObject current=null] : iv_rulePipe= rulePipe EOF ;
    public final EObject entryRulePipe() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePipe = null;


        try {
            // InternalPogoDsl.g:3843:45: (iv_rulePipe= rulePipe EOF )
            // InternalPogoDsl.g:3844:2: iv_rulePipe= rulePipe EOF
            {
             newCompositeNode(grammarAccess.getPipeRule()); 
            pushFollow(FOLLOW_1);
            iv_rulePipe=rulePipe();

            state._fsp--;

             current =iv_rulePipe; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePipe"


    // $ANTLR start "rulePipe"
    // InternalPogoDsl.g:3850:1: rulePipe returns [EObject current=null] : ( () otherlv_1= 'Pipe' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'description' ( (lv_description_5_0= ruleEString ) ) )? (otherlv_6= 'label' ( (lv_label_7_0= ruleEString ) ) )? (otherlv_8= 'rwType' ( (lv_rwType_9_0= ruleEString ) ) )? ( (otherlv_10= 'displayLevel' ( (lv_displayLevel_11_0= ruleDisplayLevel ) ) ) | ruleQualifiedName )? (otherlv_13= 'readExcludedStates' otherlv_14= '{' ( (lv_readExcludedStates_15_0= ruleEString ) ) (otherlv_16= ',' ( (lv_readExcludedStates_17_0= ruleEString ) ) )* otherlv_18= '}' )? (otherlv_19= 'writeExcludedStates' otherlv_20= '{' ( (lv_writeExcludedStates_21_0= ruleEString ) ) (otherlv_22= ',' ( (lv_writeExcludedStates_23_0= ruleEString ) ) )* otherlv_24= '}' )? otherlv_25= '}' ) ;
    public final EObject rulePipe() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        Token otherlv_10=null;
        Token otherlv_13=null;
        Token otherlv_14=null;
        Token otherlv_16=null;
        Token otherlv_18=null;
        Token otherlv_19=null;
        Token otherlv_20=null;
        Token otherlv_22=null;
        Token otherlv_24=null;
        Token otherlv_25=null;
        AntlrDatatypeRuleToken lv_name_2_0 = null;

        AntlrDatatypeRuleToken lv_description_5_0 = null;

        AntlrDatatypeRuleToken lv_label_7_0 = null;

        AntlrDatatypeRuleToken lv_rwType_9_0 = null;

        AntlrDatatypeRuleToken lv_displayLevel_11_0 = null;

        AntlrDatatypeRuleToken lv_readExcludedStates_15_0 = null;

        AntlrDatatypeRuleToken lv_readExcludedStates_17_0 = null;

        AntlrDatatypeRuleToken lv_writeExcludedStates_21_0 = null;

        AntlrDatatypeRuleToken lv_writeExcludedStates_23_0 = null;



        	enterRule();

        try {
            // InternalPogoDsl.g:3856:2: ( ( () otherlv_1= 'Pipe' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'description' ( (lv_description_5_0= ruleEString ) ) )? (otherlv_6= 'label' ( (lv_label_7_0= ruleEString ) ) )? (otherlv_8= 'rwType' ( (lv_rwType_9_0= ruleEString ) ) )? ( (otherlv_10= 'displayLevel' ( (lv_displayLevel_11_0= ruleDisplayLevel ) ) ) | ruleQualifiedName )? (otherlv_13= 'readExcludedStates' otherlv_14= '{' ( (lv_readExcludedStates_15_0= ruleEString ) ) (otherlv_16= ',' ( (lv_readExcludedStates_17_0= ruleEString ) ) )* otherlv_18= '}' )? (otherlv_19= 'writeExcludedStates' otherlv_20= '{' ( (lv_writeExcludedStates_21_0= ruleEString ) ) (otherlv_22= ',' ( (lv_writeExcludedStates_23_0= ruleEString ) ) )* otherlv_24= '}' )? otherlv_25= '}' ) )
            // InternalPogoDsl.g:3857:2: ( () otherlv_1= 'Pipe' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'description' ( (lv_description_5_0= ruleEString ) ) )? (otherlv_6= 'label' ( (lv_label_7_0= ruleEString ) ) )? (otherlv_8= 'rwType' ( (lv_rwType_9_0= ruleEString ) ) )? ( (otherlv_10= 'displayLevel' ( (lv_displayLevel_11_0= ruleDisplayLevel ) ) ) | ruleQualifiedName )? (otherlv_13= 'readExcludedStates' otherlv_14= '{' ( (lv_readExcludedStates_15_0= ruleEString ) ) (otherlv_16= ',' ( (lv_readExcludedStates_17_0= ruleEString ) ) )* otherlv_18= '}' )? (otherlv_19= 'writeExcludedStates' otherlv_20= '{' ( (lv_writeExcludedStates_21_0= ruleEString ) ) (otherlv_22= ',' ( (lv_writeExcludedStates_23_0= ruleEString ) ) )* otherlv_24= '}' )? otherlv_25= '}' )
            {
            // InternalPogoDsl.g:3857:2: ( () otherlv_1= 'Pipe' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'description' ( (lv_description_5_0= ruleEString ) ) )? (otherlv_6= 'label' ( (lv_label_7_0= ruleEString ) ) )? (otherlv_8= 'rwType' ( (lv_rwType_9_0= ruleEString ) ) )? ( (otherlv_10= 'displayLevel' ( (lv_displayLevel_11_0= ruleDisplayLevel ) ) ) | ruleQualifiedName )? (otherlv_13= 'readExcludedStates' otherlv_14= '{' ( (lv_readExcludedStates_15_0= ruleEString ) ) (otherlv_16= ',' ( (lv_readExcludedStates_17_0= ruleEString ) ) )* otherlv_18= '}' )? (otherlv_19= 'writeExcludedStates' otherlv_20= '{' ( (lv_writeExcludedStates_21_0= ruleEString ) ) (otherlv_22= ',' ( (lv_writeExcludedStates_23_0= ruleEString ) ) )* otherlv_24= '}' )? otherlv_25= '}' )
            // InternalPogoDsl.g:3858:3: () otherlv_1= 'Pipe' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'description' ( (lv_description_5_0= ruleEString ) ) )? (otherlv_6= 'label' ( (lv_label_7_0= ruleEString ) ) )? (otherlv_8= 'rwType' ( (lv_rwType_9_0= ruleEString ) ) )? ( (otherlv_10= 'displayLevel' ( (lv_displayLevel_11_0= ruleDisplayLevel ) ) ) | ruleQualifiedName )? (otherlv_13= 'readExcludedStates' otherlv_14= '{' ( (lv_readExcludedStates_15_0= ruleEString ) ) (otherlv_16= ',' ( (lv_readExcludedStates_17_0= ruleEString ) ) )* otherlv_18= '}' )? (otherlv_19= 'writeExcludedStates' otherlv_20= '{' ( (lv_writeExcludedStates_21_0= ruleEString ) ) (otherlv_22= ',' ( (lv_writeExcludedStates_23_0= ruleEString ) ) )* otherlv_24= '}' )? otherlv_25= '}'
            {
            // InternalPogoDsl.g:3858:3: ()
            // InternalPogoDsl.g:3859:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getPipeAccess().getPipeAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,90,FOLLOW_13); 

            			newLeafNode(otherlv_1, grammarAccess.getPipeAccess().getPipeKeyword_1());
            		
            // InternalPogoDsl.g:3869:3: ( (lv_name_2_0= ruleEString ) )
            // InternalPogoDsl.g:3870:4: (lv_name_2_0= ruleEString )
            {
            // InternalPogoDsl.g:3870:4: (lv_name_2_0= ruleEString )
            // InternalPogoDsl.g:3871:5: lv_name_2_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getPipeAccess().getNameEStringParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_3);
            lv_name_2_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getPipeRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_2_0,
            						"com.mncml.implementation.pogo.dsl.PogoDsl.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_3=(Token)match(input,12,FOLLOW_119); 

            			newLeafNode(otherlv_3, grammarAccess.getPipeAccess().getLeftCurlyBracketKeyword_3());
            		
            // InternalPogoDsl.g:3892:3: (otherlv_4= 'description' ( (lv_description_5_0= ruleEString ) ) )?
            int alt108=2;
            int LA108_0 = input.LA(1);

            if ( (LA108_0==27) ) {
                alt108=1;
            }
            switch (alt108) {
                case 1 :
                    // InternalPogoDsl.g:3893:4: otherlv_4= 'description' ( (lv_description_5_0= ruleEString ) )
                    {
                    otherlv_4=(Token)match(input,27,FOLLOW_13); 

                    				newLeafNode(otherlv_4, grammarAccess.getPipeAccess().getDescriptionKeyword_4_0());
                    			
                    // InternalPogoDsl.g:3897:4: ( (lv_description_5_0= ruleEString ) )
                    // InternalPogoDsl.g:3898:5: (lv_description_5_0= ruleEString )
                    {
                    // InternalPogoDsl.g:3898:5: (lv_description_5_0= ruleEString )
                    // InternalPogoDsl.g:3899:6: lv_description_5_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getPipeAccess().getDescriptionEStringParserRuleCall_4_1_0());
                    					
                    pushFollow(FOLLOW_120);
                    lv_description_5_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getPipeRule());
                    						}
                    						set(
                    							current,
                    							"description",
                    							lv_description_5_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalPogoDsl.g:3917:3: (otherlv_6= 'label' ( (lv_label_7_0= ruleEString ) ) )?
            int alt109=2;
            int LA109_0 = input.LA(1);

            if ( (LA109_0==89) ) {
                alt109=1;
            }
            switch (alt109) {
                case 1 :
                    // InternalPogoDsl.g:3918:4: otherlv_6= 'label' ( (lv_label_7_0= ruleEString ) )
                    {
                    otherlv_6=(Token)match(input,89,FOLLOW_13); 

                    				newLeafNode(otherlv_6, grammarAccess.getPipeAccess().getLabelKeyword_5_0());
                    			
                    // InternalPogoDsl.g:3922:4: ( (lv_label_7_0= ruleEString ) )
                    // InternalPogoDsl.g:3923:5: (lv_label_7_0= ruleEString )
                    {
                    // InternalPogoDsl.g:3923:5: (lv_label_7_0= ruleEString )
                    // InternalPogoDsl.g:3924:6: lv_label_7_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getPipeAccess().getLabelEStringParserRuleCall_5_1_0());
                    					
                    pushFollow(FOLLOW_121);
                    lv_label_7_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getPipeRule());
                    						}
                    						set(
                    							current,
                    							"label",
                    							lv_label_7_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalPogoDsl.g:3942:3: (otherlv_8= 'rwType' ( (lv_rwType_9_0= ruleEString ) ) )?
            int alt110=2;
            int LA110_0 = input.LA(1);

            if ( (LA110_0==71) ) {
                alt110=1;
            }
            switch (alt110) {
                case 1 :
                    // InternalPogoDsl.g:3943:4: otherlv_8= 'rwType' ( (lv_rwType_9_0= ruleEString ) )
                    {
                    otherlv_8=(Token)match(input,71,FOLLOW_13); 

                    				newLeafNode(otherlv_8, grammarAccess.getPipeAccess().getRwTypeKeyword_6_0());
                    			
                    // InternalPogoDsl.g:3947:4: ( (lv_rwType_9_0= ruleEString ) )
                    // InternalPogoDsl.g:3948:5: (lv_rwType_9_0= ruleEString )
                    {
                    // InternalPogoDsl.g:3948:5: (lv_rwType_9_0= ruleEString )
                    // InternalPogoDsl.g:3949:6: lv_rwType_9_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getPipeAccess().getRwTypeEStringParserRuleCall_6_1_0());
                    					
                    pushFollow(FOLLOW_122);
                    lv_rwType_9_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getPipeRule());
                    						}
                    						set(
                    							current,
                    							"rwType",
                    							lv_rwType_9_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalPogoDsl.g:3967:3: ( (otherlv_10= 'displayLevel' ( (lv_displayLevel_11_0= ruleDisplayLevel ) ) ) | ruleQualifiedName )?
            int alt111=3;
            int LA111_0 = input.LA(1);

            if ( (LA111_0==63) ) {
                alt111=1;
            }
            else if ( (LA111_0==RULE_ID) ) {
                alt111=2;
            }
            switch (alt111) {
                case 1 :
                    // InternalPogoDsl.g:3968:4: (otherlv_10= 'displayLevel' ( (lv_displayLevel_11_0= ruleDisplayLevel ) ) )
                    {
                    // InternalPogoDsl.g:3968:4: (otherlv_10= 'displayLevel' ( (lv_displayLevel_11_0= ruleDisplayLevel ) ) )
                    // InternalPogoDsl.g:3969:5: otherlv_10= 'displayLevel' ( (lv_displayLevel_11_0= ruleDisplayLevel ) )
                    {
                    otherlv_10=(Token)match(input,63,FOLLOW_83); 

                    					newLeafNode(otherlv_10, grammarAccess.getPipeAccess().getDisplayLevelKeyword_7_0_0());
                    				
                    // InternalPogoDsl.g:3973:5: ( (lv_displayLevel_11_0= ruleDisplayLevel ) )
                    // InternalPogoDsl.g:3974:6: (lv_displayLevel_11_0= ruleDisplayLevel )
                    {
                    // InternalPogoDsl.g:3974:6: (lv_displayLevel_11_0= ruleDisplayLevel )
                    // InternalPogoDsl.g:3975:7: lv_displayLevel_11_0= ruleDisplayLevel
                    {

                    							newCompositeNode(grammarAccess.getPipeAccess().getDisplayLevelDisplayLevelParserRuleCall_7_0_1_0());
                    						
                    pushFollow(FOLLOW_123);
                    lv_displayLevel_11_0=ruleDisplayLevel();

                    state._fsp--;


                    							if (current==null) {
                    								current = createModelElementForParent(grammarAccess.getPipeRule());
                    							}
                    							set(
                    								current,
                    								"displayLevel",
                    								lv_displayLevel_11_0,
                    								"com.mncml.implementation.pogo.dsl.PogoDsl.DisplayLevel");
                    							afterParserOrEnumRuleCall();
                    						

                    }


                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalPogoDsl.g:3994:4: ruleQualifiedName
                    {

                    				newCompositeNode(grammarAccess.getPipeAccess().getQualifiedNameParserRuleCall_7_1());
                    			
                    pushFollow(FOLLOW_123);
                    ruleQualifiedName();

                    state._fsp--;


                    				afterParserOrEnumRuleCall();
                    			

                    }
                    break;

            }

            // InternalPogoDsl.g:4002:3: (otherlv_13= 'readExcludedStates' otherlv_14= '{' ( (lv_readExcludedStates_15_0= ruleEString ) ) (otherlv_16= ',' ( (lv_readExcludedStates_17_0= ruleEString ) ) )* otherlv_18= '}' )?
            int alt113=2;
            int LA113_0 = input.LA(1);

            if ( (LA113_0==79) ) {
                alt113=1;
            }
            switch (alt113) {
                case 1 :
                    // InternalPogoDsl.g:4003:4: otherlv_13= 'readExcludedStates' otherlv_14= '{' ( (lv_readExcludedStates_15_0= ruleEString ) ) (otherlv_16= ',' ( (lv_readExcludedStates_17_0= ruleEString ) ) )* otherlv_18= '}'
                    {
                    otherlv_13=(Token)match(input,79,FOLLOW_3); 

                    				newLeafNode(otherlv_13, grammarAccess.getPipeAccess().getReadExcludedStatesKeyword_8_0());
                    			
                    otherlv_14=(Token)match(input,12,FOLLOW_13); 

                    				newLeafNode(otherlv_14, grammarAccess.getPipeAccess().getLeftCurlyBracketKeyword_8_1());
                    			
                    // InternalPogoDsl.g:4011:4: ( (lv_readExcludedStates_15_0= ruleEString ) )
                    // InternalPogoDsl.g:4012:5: (lv_readExcludedStates_15_0= ruleEString )
                    {
                    // InternalPogoDsl.g:4012:5: (lv_readExcludedStates_15_0= ruleEString )
                    // InternalPogoDsl.g:4013:6: lv_readExcludedStates_15_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getPipeAccess().getReadExcludedStatesEStringParserRuleCall_8_2_0());
                    					
                    pushFollow(FOLLOW_6);
                    lv_readExcludedStates_15_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getPipeRule());
                    						}
                    						add(
                    							current,
                    							"readExcludedStates",
                    							lv_readExcludedStates_15_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalPogoDsl.g:4030:4: (otherlv_16= ',' ( (lv_readExcludedStates_17_0= ruleEString ) ) )*
                    loop112:
                    do {
                        int alt112=2;
                        int LA112_0 = input.LA(1);

                        if ( (LA112_0==14) ) {
                            alt112=1;
                        }


                        switch (alt112) {
                    	case 1 :
                    	    // InternalPogoDsl.g:4031:5: otherlv_16= ',' ( (lv_readExcludedStates_17_0= ruleEString ) )
                    	    {
                    	    otherlv_16=(Token)match(input,14,FOLLOW_13); 

                    	    					newLeafNode(otherlv_16, grammarAccess.getPipeAccess().getCommaKeyword_8_3_0());
                    	    				
                    	    // InternalPogoDsl.g:4035:5: ( (lv_readExcludedStates_17_0= ruleEString ) )
                    	    // InternalPogoDsl.g:4036:6: (lv_readExcludedStates_17_0= ruleEString )
                    	    {
                    	    // InternalPogoDsl.g:4036:6: (lv_readExcludedStates_17_0= ruleEString )
                    	    // InternalPogoDsl.g:4037:7: lv_readExcludedStates_17_0= ruleEString
                    	    {

                    	    							newCompositeNode(grammarAccess.getPipeAccess().getReadExcludedStatesEStringParserRuleCall_8_3_1_0());
                    	    						
                    	    pushFollow(FOLLOW_6);
                    	    lv_readExcludedStates_17_0=ruleEString();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getPipeRule());
                    	    							}
                    	    							add(
                    	    								current,
                    	    								"readExcludedStates",
                    	    								lv_readExcludedStates_17_0,
                    	    								"com.mncml.implementation.pogo.dsl.PogoDsl.EString");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop112;
                        }
                    } while (true);

                    otherlv_18=(Token)match(input,15,FOLLOW_124); 

                    				newLeafNode(otherlv_18, grammarAccess.getPipeAccess().getRightCurlyBracketKeyword_8_4());
                    			

                    }
                    break;

            }

            // InternalPogoDsl.g:4060:3: (otherlv_19= 'writeExcludedStates' otherlv_20= '{' ( (lv_writeExcludedStates_21_0= ruleEString ) ) (otherlv_22= ',' ( (lv_writeExcludedStates_23_0= ruleEString ) ) )* otherlv_24= '}' )?
            int alt115=2;
            int LA115_0 = input.LA(1);

            if ( (LA115_0==80) ) {
                alt115=1;
            }
            switch (alt115) {
                case 1 :
                    // InternalPogoDsl.g:4061:4: otherlv_19= 'writeExcludedStates' otherlv_20= '{' ( (lv_writeExcludedStates_21_0= ruleEString ) ) (otherlv_22= ',' ( (lv_writeExcludedStates_23_0= ruleEString ) ) )* otherlv_24= '}'
                    {
                    otherlv_19=(Token)match(input,80,FOLLOW_3); 

                    				newLeafNode(otherlv_19, grammarAccess.getPipeAccess().getWriteExcludedStatesKeyword_9_0());
                    			
                    otherlv_20=(Token)match(input,12,FOLLOW_13); 

                    				newLeafNode(otherlv_20, grammarAccess.getPipeAccess().getLeftCurlyBracketKeyword_9_1());
                    			
                    // InternalPogoDsl.g:4069:4: ( (lv_writeExcludedStates_21_0= ruleEString ) )
                    // InternalPogoDsl.g:4070:5: (lv_writeExcludedStates_21_0= ruleEString )
                    {
                    // InternalPogoDsl.g:4070:5: (lv_writeExcludedStates_21_0= ruleEString )
                    // InternalPogoDsl.g:4071:6: lv_writeExcludedStates_21_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getPipeAccess().getWriteExcludedStatesEStringParserRuleCall_9_2_0());
                    					
                    pushFollow(FOLLOW_6);
                    lv_writeExcludedStates_21_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getPipeRule());
                    						}
                    						add(
                    							current,
                    							"writeExcludedStates",
                    							lv_writeExcludedStates_21_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalPogoDsl.g:4088:4: (otherlv_22= ',' ( (lv_writeExcludedStates_23_0= ruleEString ) ) )*
                    loop114:
                    do {
                        int alt114=2;
                        int LA114_0 = input.LA(1);

                        if ( (LA114_0==14) ) {
                            alt114=1;
                        }


                        switch (alt114) {
                    	case 1 :
                    	    // InternalPogoDsl.g:4089:5: otherlv_22= ',' ( (lv_writeExcludedStates_23_0= ruleEString ) )
                    	    {
                    	    otherlv_22=(Token)match(input,14,FOLLOW_13); 

                    	    					newLeafNode(otherlv_22, grammarAccess.getPipeAccess().getCommaKeyword_9_3_0());
                    	    				
                    	    // InternalPogoDsl.g:4093:5: ( (lv_writeExcludedStates_23_0= ruleEString ) )
                    	    // InternalPogoDsl.g:4094:6: (lv_writeExcludedStates_23_0= ruleEString )
                    	    {
                    	    // InternalPogoDsl.g:4094:6: (lv_writeExcludedStates_23_0= ruleEString )
                    	    // InternalPogoDsl.g:4095:7: lv_writeExcludedStates_23_0= ruleEString
                    	    {

                    	    							newCompositeNode(grammarAccess.getPipeAccess().getWriteExcludedStatesEStringParserRuleCall_9_3_1_0());
                    	    						
                    	    pushFollow(FOLLOW_6);
                    	    lv_writeExcludedStates_23_0=ruleEString();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getPipeRule());
                    	    							}
                    	    							add(
                    	    								current,
                    	    								"writeExcludedStates",
                    	    								lv_writeExcludedStates_23_0,
                    	    								"com.mncml.implementation.pogo.dsl.PogoDsl.EString");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop114;
                        }
                    } while (true);

                    otherlv_24=(Token)match(input,15,FOLLOW_11); 

                    				newLeafNode(otherlv_24, grammarAccess.getPipeAccess().getRightCurlyBracketKeyword_9_4());
                    			

                    }
                    break;

            }

            otherlv_25=(Token)match(input,15,FOLLOW_2); 

            			newLeafNode(otherlv_25, grammarAccess.getPipeAccess().getRightCurlyBracketKeyword_10());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePipe"


    // $ANTLR start "entryRuleState"
    // InternalPogoDsl.g:4126:1: entryRuleState returns [EObject current=null] : iv_ruleState= ruleState EOF ;
    public final EObject entryRuleState() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleState = null;


        try {
            // InternalPogoDsl.g:4126:46: (iv_ruleState= ruleState EOF )
            // InternalPogoDsl.g:4127:2: iv_ruleState= ruleState EOF
            {
             newCompositeNode(grammarAccess.getStateRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleState=ruleState();

            state._fsp--;

             current =iv_ruleState; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleState"


    // $ANTLR start "ruleState"
    // InternalPogoDsl.g:4133:1: ruleState returns [EObject current=null] : ( () otherlv_1= 'State' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'description' ( (lv_description_5_0= ruleEString ) ) )? (otherlv_6= 'status' ( (lv_status_7_0= ruleInheritanceStatus ) ) )? otherlv_8= '}' ) ;
    public final EObject ruleState() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        AntlrDatatypeRuleToken lv_name_2_0 = null;

        AntlrDatatypeRuleToken lv_description_5_0 = null;

        EObject lv_status_7_0 = null;



        	enterRule();

        try {
            // InternalPogoDsl.g:4139:2: ( ( () otherlv_1= 'State' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'description' ( (lv_description_5_0= ruleEString ) ) )? (otherlv_6= 'status' ( (lv_status_7_0= ruleInheritanceStatus ) ) )? otherlv_8= '}' ) )
            // InternalPogoDsl.g:4140:2: ( () otherlv_1= 'State' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'description' ( (lv_description_5_0= ruleEString ) ) )? (otherlv_6= 'status' ( (lv_status_7_0= ruleInheritanceStatus ) ) )? otherlv_8= '}' )
            {
            // InternalPogoDsl.g:4140:2: ( () otherlv_1= 'State' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'description' ( (lv_description_5_0= ruleEString ) ) )? (otherlv_6= 'status' ( (lv_status_7_0= ruleInheritanceStatus ) ) )? otherlv_8= '}' )
            // InternalPogoDsl.g:4141:3: () otherlv_1= 'State' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'description' ( (lv_description_5_0= ruleEString ) ) )? (otherlv_6= 'status' ( (lv_status_7_0= ruleInheritanceStatus ) ) )? otherlv_8= '}'
            {
            // InternalPogoDsl.g:4141:3: ()
            // InternalPogoDsl.g:4142:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getStateAccess().getStateAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,91,FOLLOW_13); 

            			newLeafNode(otherlv_1, grammarAccess.getStateAccess().getStateKeyword_1());
            		
            // InternalPogoDsl.g:4152:3: ( (lv_name_2_0= ruleEString ) )
            // InternalPogoDsl.g:4153:4: (lv_name_2_0= ruleEString )
            {
            // InternalPogoDsl.g:4153:4: (lv_name_2_0= ruleEString )
            // InternalPogoDsl.g:4154:5: lv_name_2_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getStateAccess().getNameEStringParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_3);
            lv_name_2_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getStateRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_2_0,
            						"com.mncml.implementation.pogo.dsl.PogoDsl.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_3=(Token)match(input,12,FOLLOW_125); 

            			newLeafNode(otherlv_3, grammarAccess.getStateAccess().getLeftCurlyBracketKeyword_3());
            		
            // InternalPogoDsl.g:4175:3: (otherlv_4= 'description' ( (lv_description_5_0= ruleEString ) ) )?
            int alt116=2;
            int LA116_0 = input.LA(1);

            if ( (LA116_0==27) ) {
                alt116=1;
            }
            switch (alt116) {
                case 1 :
                    // InternalPogoDsl.g:4176:4: otherlv_4= 'description' ( (lv_description_5_0= ruleEString ) )
                    {
                    otherlv_4=(Token)match(input,27,FOLLOW_13); 

                    				newLeafNode(otherlv_4, grammarAccess.getStateAccess().getDescriptionKeyword_4_0());
                    			
                    // InternalPogoDsl.g:4180:4: ( (lv_description_5_0= ruleEString ) )
                    // InternalPogoDsl.g:4181:5: (lv_description_5_0= ruleEString )
                    {
                    // InternalPogoDsl.g:4181:5: (lv_description_5_0= ruleEString )
                    // InternalPogoDsl.g:4182:6: lv_description_5_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getStateAccess().getDescriptionEStringParserRuleCall_4_1_0());
                    					
                    pushFollow(FOLLOW_78);
                    lv_description_5_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getStateRule());
                    						}
                    						set(
                    							current,
                    							"description",
                    							lv_description_5_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalPogoDsl.g:4200:3: (otherlv_6= 'status' ( (lv_status_7_0= ruleInheritanceStatus ) ) )?
            int alt117=2;
            int LA117_0 = input.LA(1);

            if ( (LA117_0==60) ) {
                alt117=1;
            }
            switch (alt117) {
                case 1 :
                    // InternalPogoDsl.g:4201:4: otherlv_6= 'status' ( (lv_status_7_0= ruleInheritanceStatus ) )
                    {
                    otherlv_6=(Token)match(input,60,FOLLOW_79); 

                    				newLeafNode(otherlv_6, grammarAccess.getStateAccess().getStatusKeyword_5_0());
                    			
                    // InternalPogoDsl.g:4205:4: ( (lv_status_7_0= ruleInheritanceStatus ) )
                    // InternalPogoDsl.g:4206:5: (lv_status_7_0= ruleInheritanceStatus )
                    {
                    // InternalPogoDsl.g:4206:5: (lv_status_7_0= ruleInheritanceStatus )
                    // InternalPogoDsl.g:4207:6: lv_status_7_0= ruleInheritanceStatus
                    {

                    						newCompositeNode(grammarAccess.getStateAccess().getStatusInheritanceStatusParserRuleCall_5_1_0());
                    					
                    pushFollow(FOLLOW_11);
                    lv_status_7_0=ruleInheritanceStatus();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getStateRule());
                    						}
                    						set(
                    							current,
                    							"status",
                    							lv_status_7_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.InheritanceStatus");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            otherlv_8=(Token)match(input,15,FOLLOW_2); 

            			newLeafNode(otherlv_8, grammarAccess.getStateAccess().getRightCurlyBracketKeyword_6());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleState"


    // $ANTLR start "entryRulePreferences"
    // InternalPogoDsl.g:4233:1: entryRulePreferences returns [EObject current=null] : iv_rulePreferences= rulePreferences EOF ;
    public final EObject entryRulePreferences() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePreferences = null;


        try {
            // InternalPogoDsl.g:4233:52: (iv_rulePreferences= rulePreferences EOF )
            // InternalPogoDsl.g:4234:2: iv_rulePreferences= rulePreferences EOF
            {
             newCompositeNode(grammarAccess.getPreferencesRule()); 
            pushFollow(FOLLOW_1);
            iv_rulePreferences=rulePreferences();

            state._fsp--;

             current =iv_rulePreferences; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePreferences"


    // $ANTLR start "rulePreferences"
    // InternalPogoDsl.g:4240:1: rulePreferences returns [EObject current=null] : ( () otherlv_1= 'Preferences' otherlv_2= '{' (otherlv_3= 'docHome' ( (lv_docHome_4_0= ruleEString ) ) )? (otherlv_5= 'makefileHome' ( (lv_makefileHome_6_0= ruleEString ) ) )? (otherlv_7= 'installHome' ( (lv_installHome_8_0= ruleEString ) ) )? (otherlv_9= 'htmlVersion' ( (lv_htmlVersion_10_0= ruleEString ) ) )? otherlv_11= '}' ) ;
    public final EObject rulePreferences() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        Token otherlv_11=null;
        AntlrDatatypeRuleToken lv_docHome_4_0 = null;

        AntlrDatatypeRuleToken lv_makefileHome_6_0 = null;

        AntlrDatatypeRuleToken lv_installHome_8_0 = null;

        AntlrDatatypeRuleToken lv_htmlVersion_10_0 = null;



        	enterRule();

        try {
            // InternalPogoDsl.g:4246:2: ( ( () otherlv_1= 'Preferences' otherlv_2= '{' (otherlv_3= 'docHome' ( (lv_docHome_4_0= ruleEString ) ) )? (otherlv_5= 'makefileHome' ( (lv_makefileHome_6_0= ruleEString ) ) )? (otherlv_7= 'installHome' ( (lv_installHome_8_0= ruleEString ) ) )? (otherlv_9= 'htmlVersion' ( (lv_htmlVersion_10_0= ruleEString ) ) )? otherlv_11= '}' ) )
            // InternalPogoDsl.g:4247:2: ( () otherlv_1= 'Preferences' otherlv_2= '{' (otherlv_3= 'docHome' ( (lv_docHome_4_0= ruleEString ) ) )? (otherlv_5= 'makefileHome' ( (lv_makefileHome_6_0= ruleEString ) ) )? (otherlv_7= 'installHome' ( (lv_installHome_8_0= ruleEString ) ) )? (otherlv_9= 'htmlVersion' ( (lv_htmlVersion_10_0= ruleEString ) ) )? otherlv_11= '}' )
            {
            // InternalPogoDsl.g:4247:2: ( () otherlv_1= 'Preferences' otherlv_2= '{' (otherlv_3= 'docHome' ( (lv_docHome_4_0= ruleEString ) ) )? (otherlv_5= 'makefileHome' ( (lv_makefileHome_6_0= ruleEString ) ) )? (otherlv_7= 'installHome' ( (lv_installHome_8_0= ruleEString ) ) )? (otherlv_9= 'htmlVersion' ( (lv_htmlVersion_10_0= ruleEString ) ) )? otherlv_11= '}' )
            // InternalPogoDsl.g:4248:3: () otherlv_1= 'Preferences' otherlv_2= '{' (otherlv_3= 'docHome' ( (lv_docHome_4_0= ruleEString ) ) )? (otherlv_5= 'makefileHome' ( (lv_makefileHome_6_0= ruleEString ) ) )? (otherlv_7= 'installHome' ( (lv_installHome_8_0= ruleEString ) ) )? (otherlv_9= 'htmlVersion' ( (lv_htmlVersion_10_0= ruleEString ) ) )? otherlv_11= '}'
            {
            // InternalPogoDsl.g:4248:3: ()
            // InternalPogoDsl.g:4249:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getPreferencesAccess().getPreferencesAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,92,FOLLOW_3); 

            			newLeafNode(otherlv_1, grammarAccess.getPreferencesAccess().getPreferencesKeyword_1());
            		
            otherlv_2=(Token)match(input,12,FOLLOW_126); 

            			newLeafNode(otherlv_2, grammarAccess.getPreferencesAccess().getLeftCurlyBracketKeyword_2());
            		
            // InternalPogoDsl.g:4263:3: (otherlv_3= 'docHome' ( (lv_docHome_4_0= ruleEString ) ) )?
            int alt118=2;
            int LA118_0 = input.LA(1);

            if ( (LA118_0==93) ) {
                alt118=1;
            }
            switch (alt118) {
                case 1 :
                    // InternalPogoDsl.g:4264:4: otherlv_3= 'docHome' ( (lv_docHome_4_0= ruleEString ) )
                    {
                    otherlv_3=(Token)match(input,93,FOLLOW_13); 

                    				newLeafNode(otherlv_3, grammarAccess.getPreferencesAccess().getDocHomeKeyword_3_0());
                    			
                    // InternalPogoDsl.g:4268:4: ( (lv_docHome_4_0= ruleEString ) )
                    // InternalPogoDsl.g:4269:5: (lv_docHome_4_0= ruleEString )
                    {
                    // InternalPogoDsl.g:4269:5: (lv_docHome_4_0= ruleEString )
                    // InternalPogoDsl.g:4270:6: lv_docHome_4_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getPreferencesAccess().getDocHomeEStringParserRuleCall_3_1_0());
                    					
                    pushFollow(FOLLOW_127);
                    lv_docHome_4_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getPreferencesRule());
                    						}
                    						set(
                    							current,
                    							"docHome",
                    							lv_docHome_4_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalPogoDsl.g:4288:3: (otherlv_5= 'makefileHome' ( (lv_makefileHome_6_0= ruleEString ) ) )?
            int alt119=2;
            int LA119_0 = input.LA(1);

            if ( (LA119_0==94) ) {
                alt119=1;
            }
            switch (alt119) {
                case 1 :
                    // InternalPogoDsl.g:4289:4: otherlv_5= 'makefileHome' ( (lv_makefileHome_6_0= ruleEString ) )
                    {
                    otherlv_5=(Token)match(input,94,FOLLOW_13); 

                    				newLeafNode(otherlv_5, grammarAccess.getPreferencesAccess().getMakefileHomeKeyword_4_0());
                    			
                    // InternalPogoDsl.g:4293:4: ( (lv_makefileHome_6_0= ruleEString ) )
                    // InternalPogoDsl.g:4294:5: (lv_makefileHome_6_0= ruleEString )
                    {
                    // InternalPogoDsl.g:4294:5: (lv_makefileHome_6_0= ruleEString )
                    // InternalPogoDsl.g:4295:6: lv_makefileHome_6_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getPreferencesAccess().getMakefileHomeEStringParserRuleCall_4_1_0());
                    					
                    pushFollow(FOLLOW_128);
                    lv_makefileHome_6_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getPreferencesRule());
                    						}
                    						set(
                    							current,
                    							"makefileHome",
                    							lv_makefileHome_6_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalPogoDsl.g:4313:3: (otherlv_7= 'installHome' ( (lv_installHome_8_0= ruleEString ) ) )?
            int alt120=2;
            int LA120_0 = input.LA(1);

            if ( (LA120_0==95) ) {
                alt120=1;
            }
            switch (alt120) {
                case 1 :
                    // InternalPogoDsl.g:4314:4: otherlv_7= 'installHome' ( (lv_installHome_8_0= ruleEString ) )
                    {
                    otherlv_7=(Token)match(input,95,FOLLOW_13); 

                    				newLeafNode(otherlv_7, grammarAccess.getPreferencesAccess().getInstallHomeKeyword_5_0());
                    			
                    // InternalPogoDsl.g:4318:4: ( (lv_installHome_8_0= ruleEString ) )
                    // InternalPogoDsl.g:4319:5: (lv_installHome_8_0= ruleEString )
                    {
                    // InternalPogoDsl.g:4319:5: (lv_installHome_8_0= ruleEString )
                    // InternalPogoDsl.g:4320:6: lv_installHome_8_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getPreferencesAccess().getInstallHomeEStringParserRuleCall_5_1_0());
                    					
                    pushFollow(FOLLOW_129);
                    lv_installHome_8_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getPreferencesRule());
                    						}
                    						set(
                    							current,
                    							"installHome",
                    							lv_installHome_8_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalPogoDsl.g:4338:3: (otherlv_9= 'htmlVersion' ( (lv_htmlVersion_10_0= ruleEString ) ) )?
            int alt121=2;
            int LA121_0 = input.LA(1);

            if ( (LA121_0==96) ) {
                alt121=1;
            }
            switch (alt121) {
                case 1 :
                    // InternalPogoDsl.g:4339:4: otherlv_9= 'htmlVersion' ( (lv_htmlVersion_10_0= ruleEString ) )
                    {
                    otherlv_9=(Token)match(input,96,FOLLOW_13); 

                    				newLeafNode(otherlv_9, grammarAccess.getPreferencesAccess().getHtmlVersionKeyword_6_0());
                    			
                    // InternalPogoDsl.g:4343:4: ( (lv_htmlVersion_10_0= ruleEString ) )
                    // InternalPogoDsl.g:4344:5: (lv_htmlVersion_10_0= ruleEString )
                    {
                    // InternalPogoDsl.g:4344:5: (lv_htmlVersion_10_0= ruleEString )
                    // InternalPogoDsl.g:4345:6: lv_htmlVersion_10_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getPreferencesAccess().getHtmlVersionEStringParserRuleCall_6_1_0());
                    					
                    pushFollow(FOLLOW_11);
                    lv_htmlVersion_10_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getPreferencesRule());
                    						}
                    						set(
                    							current,
                    							"htmlVersion",
                    							lv_htmlVersion_10_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            otherlv_11=(Token)match(input,15,FOLLOW_2); 

            			newLeafNode(otherlv_11, grammarAccess.getPreferencesAccess().getRightCurlyBracketKeyword_7());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePreferences"


    // $ANTLR start "entryRuleAdditionalFile"
    // InternalPogoDsl.g:4371:1: entryRuleAdditionalFile returns [EObject current=null] : iv_ruleAdditionalFile= ruleAdditionalFile EOF ;
    public final EObject entryRuleAdditionalFile() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAdditionalFile = null;


        try {
            // InternalPogoDsl.g:4371:55: (iv_ruleAdditionalFile= ruleAdditionalFile EOF )
            // InternalPogoDsl.g:4372:2: iv_ruleAdditionalFile= ruleAdditionalFile EOF
            {
             newCompositeNode(grammarAccess.getAdditionalFileRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAdditionalFile=ruleAdditionalFile();

            state._fsp--;

             current =iv_ruleAdditionalFile; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAdditionalFile"


    // $ANTLR start "ruleAdditionalFile"
    // InternalPogoDsl.g:4378:1: ruleAdditionalFile returns [EObject current=null] : ( () otherlv_1= 'AdditionalFile' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'path' ( (lv_path_5_0= ruleEString ) ) )? otherlv_6= '}' ) ;
    public final EObject ruleAdditionalFile() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        AntlrDatatypeRuleToken lv_name_2_0 = null;

        AntlrDatatypeRuleToken lv_path_5_0 = null;



        	enterRule();

        try {
            // InternalPogoDsl.g:4384:2: ( ( () otherlv_1= 'AdditionalFile' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'path' ( (lv_path_5_0= ruleEString ) ) )? otherlv_6= '}' ) )
            // InternalPogoDsl.g:4385:2: ( () otherlv_1= 'AdditionalFile' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'path' ( (lv_path_5_0= ruleEString ) ) )? otherlv_6= '}' )
            {
            // InternalPogoDsl.g:4385:2: ( () otherlv_1= 'AdditionalFile' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'path' ( (lv_path_5_0= ruleEString ) ) )? otherlv_6= '}' )
            // InternalPogoDsl.g:4386:3: () otherlv_1= 'AdditionalFile' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'path' ( (lv_path_5_0= ruleEString ) ) )? otherlv_6= '}'
            {
            // InternalPogoDsl.g:4386:3: ()
            // InternalPogoDsl.g:4387:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getAdditionalFileAccess().getAdditionalFileAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,97,FOLLOW_13); 

            			newLeafNode(otherlv_1, grammarAccess.getAdditionalFileAccess().getAdditionalFileKeyword_1());
            		
            // InternalPogoDsl.g:4397:3: ( (lv_name_2_0= ruleEString ) )
            // InternalPogoDsl.g:4398:4: (lv_name_2_0= ruleEString )
            {
            // InternalPogoDsl.g:4398:4: (lv_name_2_0= ruleEString )
            // InternalPogoDsl.g:4399:5: lv_name_2_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getAdditionalFileAccess().getNameEStringParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_3);
            lv_name_2_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getAdditionalFileRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_2_0,
            						"com.mncml.implementation.pogo.dsl.PogoDsl.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_3=(Token)match(input,12,FOLLOW_130); 

            			newLeafNode(otherlv_3, grammarAccess.getAdditionalFileAccess().getLeftCurlyBracketKeyword_3());
            		
            // InternalPogoDsl.g:4420:3: (otherlv_4= 'path' ( (lv_path_5_0= ruleEString ) ) )?
            int alt122=2;
            int LA122_0 = input.LA(1);

            if ( (LA122_0==98) ) {
                alt122=1;
            }
            switch (alt122) {
                case 1 :
                    // InternalPogoDsl.g:4421:4: otherlv_4= 'path' ( (lv_path_5_0= ruleEString ) )
                    {
                    otherlv_4=(Token)match(input,98,FOLLOW_13); 

                    				newLeafNode(otherlv_4, grammarAccess.getAdditionalFileAccess().getPathKeyword_4_0());
                    			
                    // InternalPogoDsl.g:4425:4: ( (lv_path_5_0= ruleEString ) )
                    // InternalPogoDsl.g:4426:5: (lv_path_5_0= ruleEString )
                    {
                    // InternalPogoDsl.g:4426:5: (lv_path_5_0= ruleEString )
                    // InternalPogoDsl.g:4427:6: lv_path_5_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getAdditionalFileAccess().getPathEStringParserRuleCall_4_1_0());
                    					
                    pushFollow(FOLLOW_11);
                    lv_path_5_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getAdditionalFileRule());
                    						}
                    						set(
                    							current,
                    							"path",
                    							lv_path_5_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            otherlv_6=(Token)match(input,15,FOLLOW_2); 

            			newLeafNode(otherlv_6, grammarAccess.getAdditionalFileAccess().getRightCurlyBracketKeyword_5());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAdditionalFile"


    // $ANTLR start "entryRuleOverlodedPollPeriodObject"
    // InternalPogoDsl.g:4453:1: entryRuleOverlodedPollPeriodObject returns [EObject current=null] : iv_ruleOverlodedPollPeriodObject= ruleOverlodedPollPeriodObject EOF ;
    public final EObject entryRuleOverlodedPollPeriodObject() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleOverlodedPollPeriodObject = null;


        try {
            // InternalPogoDsl.g:4453:66: (iv_ruleOverlodedPollPeriodObject= ruleOverlodedPollPeriodObject EOF )
            // InternalPogoDsl.g:4454:2: iv_ruleOverlodedPollPeriodObject= ruleOverlodedPollPeriodObject EOF
            {
             newCompositeNode(grammarAccess.getOverlodedPollPeriodObjectRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleOverlodedPollPeriodObject=ruleOverlodedPollPeriodObject();

            state._fsp--;

             current =iv_ruleOverlodedPollPeriodObject; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOverlodedPollPeriodObject"


    // $ANTLR start "ruleOverlodedPollPeriodObject"
    // InternalPogoDsl.g:4460:1: ruleOverlodedPollPeriodObject returns [EObject current=null] : ( () otherlv_1= 'OverlodedPollPeriodObject' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'type' ( (lv_type_5_0= ruleEString ) ) )? (otherlv_6= 'pollPeriod' ( (lv_pollPeriod_7_0= ruleEString ) ) )? otherlv_8= '}' ) ;
    public final EObject ruleOverlodedPollPeriodObject() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        AntlrDatatypeRuleToken lv_name_2_0 = null;

        AntlrDatatypeRuleToken lv_type_5_0 = null;

        AntlrDatatypeRuleToken lv_pollPeriod_7_0 = null;



        	enterRule();

        try {
            // InternalPogoDsl.g:4466:2: ( ( () otherlv_1= 'OverlodedPollPeriodObject' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'type' ( (lv_type_5_0= ruleEString ) ) )? (otherlv_6= 'pollPeriod' ( (lv_pollPeriod_7_0= ruleEString ) ) )? otherlv_8= '}' ) )
            // InternalPogoDsl.g:4467:2: ( () otherlv_1= 'OverlodedPollPeriodObject' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'type' ( (lv_type_5_0= ruleEString ) ) )? (otherlv_6= 'pollPeriod' ( (lv_pollPeriod_7_0= ruleEString ) ) )? otherlv_8= '}' )
            {
            // InternalPogoDsl.g:4467:2: ( () otherlv_1= 'OverlodedPollPeriodObject' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'type' ( (lv_type_5_0= ruleEString ) ) )? (otherlv_6= 'pollPeriod' ( (lv_pollPeriod_7_0= ruleEString ) ) )? otherlv_8= '}' )
            // InternalPogoDsl.g:4468:3: () otherlv_1= 'OverlodedPollPeriodObject' ( (lv_name_2_0= ruleEString ) ) otherlv_3= '{' (otherlv_4= 'type' ( (lv_type_5_0= ruleEString ) ) )? (otherlv_6= 'pollPeriod' ( (lv_pollPeriod_7_0= ruleEString ) ) )? otherlv_8= '}'
            {
            // InternalPogoDsl.g:4468:3: ()
            // InternalPogoDsl.g:4469:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getOverlodedPollPeriodObjectAccess().getOverlodedPollPeriodObjectAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,99,FOLLOW_13); 

            			newLeafNode(otherlv_1, grammarAccess.getOverlodedPollPeriodObjectAccess().getOverlodedPollPeriodObjectKeyword_1());
            		
            // InternalPogoDsl.g:4479:3: ( (lv_name_2_0= ruleEString ) )
            // InternalPogoDsl.g:4480:4: (lv_name_2_0= ruleEString )
            {
            // InternalPogoDsl.g:4480:4: (lv_name_2_0= ruleEString )
            // InternalPogoDsl.g:4481:5: lv_name_2_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getOverlodedPollPeriodObjectAccess().getNameEStringParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_3);
            lv_name_2_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getOverlodedPollPeriodObjectRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_2_0,
            						"com.mncml.implementation.pogo.dsl.PogoDsl.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_3=(Token)match(input,12,FOLLOW_131); 

            			newLeafNode(otherlv_3, grammarAccess.getOverlodedPollPeriodObjectAccess().getLeftCurlyBracketKeyword_3());
            		
            // InternalPogoDsl.g:4502:3: (otherlv_4= 'type' ( (lv_type_5_0= ruleEString ) ) )?
            int alt123=2;
            int LA123_0 = input.LA(1);

            if ( (LA123_0==59) ) {
                alt123=1;
            }
            switch (alt123) {
                case 1 :
                    // InternalPogoDsl.g:4503:4: otherlv_4= 'type' ( (lv_type_5_0= ruleEString ) )
                    {
                    otherlv_4=(Token)match(input,59,FOLLOW_13); 

                    				newLeafNode(otherlv_4, grammarAccess.getOverlodedPollPeriodObjectAccess().getTypeKeyword_4_0());
                    			
                    // InternalPogoDsl.g:4507:4: ( (lv_type_5_0= ruleEString ) )
                    // InternalPogoDsl.g:4508:5: (lv_type_5_0= ruleEString )
                    {
                    // InternalPogoDsl.g:4508:5: (lv_type_5_0= ruleEString )
                    // InternalPogoDsl.g:4509:6: lv_type_5_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getOverlodedPollPeriodObjectAccess().getTypeEStringParserRuleCall_4_1_0());
                    					
                    pushFollow(FOLLOW_132);
                    lv_type_5_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getOverlodedPollPeriodObjectRule());
                    						}
                    						set(
                    							current,
                    							"type",
                    							lv_type_5_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalPogoDsl.g:4527:3: (otherlv_6= 'pollPeriod' ( (lv_pollPeriod_7_0= ruleEString ) ) )?
            int alt124=2;
            int LA124_0 = input.LA(1);

            if ( (LA124_0==100) ) {
                alt124=1;
            }
            switch (alt124) {
                case 1 :
                    // InternalPogoDsl.g:4528:4: otherlv_6= 'pollPeriod' ( (lv_pollPeriod_7_0= ruleEString ) )
                    {
                    otherlv_6=(Token)match(input,100,FOLLOW_13); 

                    				newLeafNode(otherlv_6, grammarAccess.getOverlodedPollPeriodObjectAccess().getPollPeriodKeyword_5_0());
                    			
                    // InternalPogoDsl.g:4532:4: ( (lv_pollPeriod_7_0= ruleEString ) )
                    // InternalPogoDsl.g:4533:5: (lv_pollPeriod_7_0= ruleEString )
                    {
                    // InternalPogoDsl.g:4533:5: (lv_pollPeriod_7_0= ruleEString )
                    // InternalPogoDsl.g:4534:6: lv_pollPeriod_7_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getOverlodedPollPeriodObjectAccess().getPollPeriodEStringParserRuleCall_5_1_0());
                    					
                    pushFollow(FOLLOW_11);
                    lv_pollPeriod_7_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getOverlodedPollPeriodObjectRule());
                    						}
                    						set(
                    							current,
                    							"pollPeriod",
                    							lv_pollPeriod_7_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            otherlv_8=(Token)match(input,15,FOLLOW_2); 

            			newLeafNode(otherlv_8, grammarAccess.getOverlodedPollPeriodObjectAccess().getRightCurlyBracketKeyword_6());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOverlodedPollPeriodObject"


    // $ANTLR start "entryRuleInheritance"
    // InternalPogoDsl.g:4560:1: entryRuleInheritance returns [EObject current=null] : iv_ruleInheritance= ruleInheritance EOF ;
    public final EObject entryRuleInheritance() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleInheritance = null;


        try {
            // InternalPogoDsl.g:4560:52: (iv_ruleInheritance= ruleInheritance EOF )
            // InternalPogoDsl.g:4561:2: iv_ruleInheritance= ruleInheritance EOF
            {
             newCompositeNode(grammarAccess.getInheritanceRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleInheritance=ruleInheritance();

            state._fsp--;

             current =iv_ruleInheritance; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleInheritance"


    // $ANTLR start "ruleInheritance"
    // InternalPogoDsl.g:4567:1: ruleInheritance returns [EObject current=null] : ( () otherlv_1= 'Inheritance' otherlv_2= '{' (otherlv_3= 'classname' ( (lv_classname_4_0= ruleEString ) ) )? (otherlv_5= 'sourcePath' ( (lv_sourcePath_6_0= ruleEString ) ) )? otherlv_7= '}' ) ;
    public final EObject ruleInheritance() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        AntlrDatatypeRuleToken lv_classname_4_0 = null;

        AntlrDatatypeRuleToken lv_sourcePath_6_0 = null;



        	enterRule();

        try {
            // InternalPogoDsl.g:4573:2: ( ( () otherlv_1= 'Inheritance' otherlv_2= '{' (otherlv_3= 'classname' ( (lv_classname_4_0= ruleEString ) ) )? (otherlv_5= 'sourcePath' ( (lv_sourcePath_6_0= ruleEString ) ) )? otherlv_7= '}' ) )
            // InternalPogoDsl.g:4574:2: ( () otherlv_1= 'Inheritance' otherlv_2= '{' (otherlv_3= 'classname' ( (lv_classname_4_0= ruleEString ) ) )? (otherlv_5= 'sourcePath' ( (lv_sourcePath_6_0= ruleEString ) ) )? otherlv_7= '}' )
            {
            // InternalPogoDsl.g:4574:2: ( () otherlv_1= 'Inheritance' otherlv_2= '{' (otherlv_3= 'classname' ( (lv_classname_4_0= ruleEString ) ) )? (otherlv_5= 'sourcePath' ( (lv_sourcePath_6_0= ruleEString ) ) )? otherlv_7= '}' )
            // InternalPogoDsl.g:4575:3: () otherlv_1= 'Inheritance' otherlv_2= '{' (otherlv_3= 'classname' ( (lv_classname_4_0= ruleEString ) ) )? (otherlv_5= 'sourcePath' ( (lv_sourcePath_6_0= ruleEString ) ) )? otherlv_7= '}'
            {
            // InternalPogoDsl.g:4575:3: ()
            // InternalPogoDsl.g:4576:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getInheritanceAccess().getInheritanceAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,101,FOLLOW_3); 

            			newLeafNode(otherlv_1, grammarAccess.getInheritanceAccess().getInheritanceKeyword_1());
            		
            otherlv_2=(Token)match(input,12,FOLLOW_133); 

            			newLeafNode(otherlv_2, grammarAccess.getInheritanceAccess().getLeftCurlyBracketKeyword_2());
            		
            // InternalPogoDsl.g:4590:3: (otherlv_3= 'classname' ( (lv_classname_4_0= ruleEString ) ) )?
            int alt125=2;
            int LA125_0 = input.LA(1);

            if ( (LA125_0==102) ) {
                alt125=1;
            }
            switch (alt125) {
                case 1 :
                    // InternalPogoDsl.g:4591:4: otherlv_3= 'classname' ( (lv_classname_4_0= ruleEString ) )
                    {
                    otherlv_3=(Token)match(input,102,FOLLOW_13); 

                    				newLeafNode(otherlv_3, grammarAccess.getInheritanceAccess().getClassnameKeyword_3_0());
                    			
                    // InternalPogoDsl.g:4595:4: ( (lv_classname_4_0= ruleEString ) )
                    // InternalPogoDsl.g:4596:5: (lv_classname_4_0= ruleEString )
                    {
                    // InternalPogoDsl.g:4596:5: (lv_classname_4_0= ruleEString )
                    // InternalPogoDsl.g:4597:6: lv_classname_4_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getInheritanceAccess().getClassnameEStringParserRuleCall_3_1_0());
                    					
                    pushFollow(FOLLOW_134);
                    lv_classname_4_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getInheritanceRule());
                    						}
                    						set(
                    							current,
                    							"classname",
                    							lv_classname_4_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalPogoDsl.g:4615:3: (otherlv_5= 'sourcePath' ( (lv_sourcePath_6_0= ruleEString ) ) )?
            int alt126=2;
            int LA126_0 = input.LA(1);

            if ( (LA126_0==42) ) {
                alt126=1;
            }
            switch (alt126) {
                case 1 :
                    // InternalPogoDsl.g:4616:4: otherlv_5= 'sourcePath' ( (lv_sourcePath_6_0= ruleEString ) )
                    {
                    otherlv_5=(Token)match(input,42,FOLLOW_13); 

                    				newLeafNode(otherlv_5, grammarAccess.getInheritanceAccess().getSourcePathKeyword_4_0());
                    			
                    // InternalPogoDsl.g:4620:4: ( (lv_sourcePath_6_0= ruleEString ) )
                    // InternalPogoDsl.g:4621:5: (lv_sourcePath_6_0= ruleEString )
                    {
                    // InternalPogoDsl.g:4621:5: (lv_sourcePath_6_0= ruleEString )
                    // InternalPogoDsl.g:4622:6: lv_sourcePath_6_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getInheritanceAccess().getSourcePathEStringParserRuleCall_4_1_0());
                    					
                    pushFollow(FOLLOW_11);
                    lv_sourcePath_6_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getInheritanceRule());
                    						}
                    						set(
                    							current,
                    							"sourcePath",
                    							lv_sourcePath_6_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            otherlv_7=(Token)match(input,15,FOLLOW_2); 

            			newLeafNode(otherlv_7, grammarAccess.getInheritanceAccess().getRightCurlyBracketKeyword_5());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleInheritance"


    // $ANTLR start "entryRuleClassIdentification"
    // InternalPogoDsl.g:4648:1: entryRuleClassIdentification returns [EObject current=null] : iv_ruleClassIdentification= ruleClassIdentification EOF ;
    public final EObject entryRuleClassIdentification() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleClassIdentification = null;


        try {
            // InternalPogoDsl.g:4648:60: (iv_ruleClassIdentification= ruleClassIdentification EOF )
            // InternalPogoDsl.g:4649:2: iv_ruleClassIdentification= ruleClassIdentification EOF
            {
             newCompositeNode(grammarAccess.getClassIdentificationRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleClassIdentification=ruleClassIdentification();

            state._fsp--;

             current =iv_ruleClassIdentification; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleClassIdentification"


    // $ANTLR start "ruleClassIdentification"
    // InternalPogoDsl.g:4655:1: ruleClassIdentification returns [EObject current=null] : ( () otherlv_1= 'ClassIdentification' otherlv_2= '{' (otherlv_3= 'contact' ( (lv_contact_4_0= ruleEString ) ) )? (otherlv_5= 'author' ( (lv_author_6_0= ruleEString ) ) )? (otherlv_7= 'emailDomain' ( (lv_emailDomain_8_0= ruleEString ) ) )? (otherlv_9= 'classFamily' ( (lv_classFamily_10_0= ruleEString ) ) )? (otherlv_11= 'siteSpecific' ( (lv_siteSpecific_12_0= ruleEString ) ) )? (otherlv_13= 'platform' ( (lv_platform_14_0= ruleEString ) ) )? (otherlv_15= 'bus' ( (lv_bus_16_0= ruleEString ) ) )? (otherlv_17= 'manufacturer' ( (lv_manufacturer_18_0= ruleEString ) ) )? (otherlv_19= 'reference' ( (lv_reference_20_0= ruleEString ) ) )? (otherlv_21= 'keyWords' otherlv_22= '{' ( (lv_keyWords_23_0= ruleEString ) ) (otherlv_24= ',' ( (lv_keyWords_25_0= ruleEString ) ) )* otherlv_26= '}' )? otherlv_27= '}' ) ;
    public final EObject ruleClassIdentification() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        Token otherlv_11=null;
        Token otherlv_13=null;
        Token otherlv_15=null;
        Token otherlv_17=null;
        Token otherlv_19=null;
        Token otherlv_21=null;
        Token otherlv_22=null;
        Token otherlv_24=null;
        Token otherlv_26=null;
        Token otherlv_27=null;
        AntlrDatatypeRuleToken lv_contact_4_0 = null;

        AntlrDatatypeRuleToken lv_author_6_0 = null;

        AntlrDatatypeRuleToken lv_emailDomain_8_0 = null;

        AntlrDatatypeRuleToken lv_classFamily_10_0 = null;

        AntlrDatatypeRuleToken lv_siteSpecific_12_0 = null;

        AntlrDatatypeRuleToken lv_platform_14_0 = null;

        AntlrDatatypeRuleToken lv_bus_16_0 = null;

        AntlrDatatypeRuleToken lv_manufacturer_18_0 = null;

        AntlrDatatypeRuleToken lv_reference_20_0 = null;

        AntlrDatatypeRuleToken lv_keyWords_23_0 = null;

        AntlrDatatypeRuleToken lv_keyWords_25_0 = null;



        	enterRule();

        try {
            // InternalPogoDsl.g:4661:2: ( ( () otherlv_1= 'ClassIdentification' otherlv_2= '{' (otherlv_3= 'contact' ( (lv_contact_4_0= ruleEString ) ) )? (otherlv_5= 'author' ( (lv_author_6_0= ruleEString ) ) )? (otherlv_7= 'emailDomain' ( (lv_emailDomain_8_0= ruleEString ) ) )? (otherlv_9= 'classFamily' ( (lv_classFamily_10_0= ruleEString ) ) )? (otherlv_11= 'siteSpecific' ( (lv_siteSpecific_12_0= ruleEString ) ) )? (otherlv_13= 'platform' ( (lv_platform_14_0= ruleEString ) ) )? (otherlv_15= 'bus' ( (lv_bus_16_0= ruleEString ) ) )? (otherlv_17= 'manufacturer' ( (lv_manufacturer_18_0= ruleEString ) ) )? (otherlv_19= 'reference' ( (lv_reference_20_0= ruleEString ) ) )? (otherlv_21= 'keyWords' otherlv_22= '{' ( (lv_keyWords_23_0= ruleEString ) ) (otherlv_24= ',' ( (lv_keyWords_25_0= ruleEString ) ) )* otherlv_26= '}' )? otherlv_27= '}' ) )
            // InternalPogoDsl.g:4662:2: ( () otherlv_1= 'ClassIdentification' otherlv_2= '{' (otherlv_3= 'contact' ( (lv_contact_4_0= ruleEString ) ) )? (otherlv_5= 'author' ( (lv_author_6_0= ruleEString ) ) )? (otherlv_7= 'emailDomain' ( (lv_emailDomain_8_0= ruleEString ) ) )? (otherlv_9= 'classFamily' ( (lv_classFamily_10_0= ruleEString ) ) )? (otherlv_11= 'siteSpecific' ( (lv_siteSpecific_12_0= ruleEString ) ) )? (otherlv_13= 'platform' ( (lv_platform_14_0= ruleEString ) ) )? (otherlv_15= 'bus' ( (lv_bus_16_0= ruleEString ) ) )? (otherlv_17= 'manufacturer' ( (lv_manufacturer_18_0= ruleEString ) ) )? (otherlv_19= 'reference' ( (lv_reference_20_0= ruleEString ) ) )? (otherlv_21= 'keyWords' otherlv_22= '{' ( (lv_keyWords_23_0= ruleEString ) ) (otherlv_24= ',' ( (lv_keyWords_25_0= ruleEString ) ) )* otherlv_26= '}' )? otherlv_27= '}' )
            {
            // InternalPogoDsl.g:4662:2: ( () otherlv_1= 'ClassIdentification' otherlv_2= '{' (otherlv_3= 'contact' ( (lv_contact_4_0= ruleEString ) ) )? (otherlv_5= 'author' ( (lv_author_6_0= ruleEString ) ) )? (otherlv_7= 'emailDomain' ( (lv_emailDomain_8_0= ruleEString ) ) )? (otherlv_9= 'classFamily' ( (lv_classFamily_10_0= ruleEString ) ) )? (otherlv_11= 'siteSpecific' ( (lv_siteSpecific_12_0= ruleEString ) ) )? (otherlv_13= 'platform' ( (lv_platform_14_0= ruleEString ) ) )? (otherlv_15= 'bus' ( (lv_bus_16_0= ruleEString ) ) )? (otherlv_17= 'manufacturer' ( (lv_manufacturer_18_0= ruleEString ) ) )? (otherlv_19= 'reference' ( (lv_reference_20_0= ruleEString ) ) )? (otherlv_21= 'keyWords' otherlv_22= '{' ( (lv_keyWords_23_0= ruleEString ) ) (otherlv_24= ',' ( (lv_keyWords_25_0= ruleEString ) ) )* otherlv_26= '}' )? otherlv_27= '}' )
            // InternalPogoDsl.g:4663:3: () otherlv_1= 'ClassIdentification' otherlv_2= '{' (otherlv_3= 'contact' ( (lv_contact_4_0= ruleEString ) ) )? (otherlv_5= 'author' ( (lv_author_6_0= ruleEString ) ) )? (otherlv_7= 'emailDomain' ( (lv_emailDomain_8_0= ruleEString ) ) )? (otherlv_9= 'classFamily' ( (lv_classFamily_10_0= ruleEString ) ) )? (otherlv_11= 'siteSpecific' ( (lv_siteSpecific_12_0= ruleEString ) ) )? (otherlv_13= 'platform' ( (lv_platform_14_0= ruleEString ) ) )? (otherlv_15= 'bus' ( (lv_bus_16_0= ruleEString ) ) )? (otherlv_17= 'manufacturer' ( (lv_manufacturer_18_0= ruleEString ) ) )? (otherlv_19= 'reference' ( (lv_reference_20_0= ruleEString ) ) )? (otherlv_21= 'keyWords' otherlv_22= '{' ( (lv_keyWords_23_0= ruleEString ) ) (otherlv_24= ',' ( (lv_keyWords_25_0= ruleEString ) ) )* otherlv_26= '}' )? otherlv_27= '}'
            {
            // InternalPogoDsl.g:4663:3: ()
            // InternalPogoDsl.g:4664:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getClassIdentificationAccess().getClassIdentificationAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,103,FOLLOW_3); 

            			newLeafNode(otherlv_1, grammarAccess.getClassIdentificationAccess().getClassIdentificationKeyword_1());
            		
            otherlv_2=(Token)match(input,12,FOLLOW_135); 

            			newLeafNode(otherlv_2, grammarAccess.getClassIdentificationAccess().getLeftCurlyBracketKeyword_2());
            		
            // InternalPogoDsl.g:4678:3: (otherlv_3= 'contact' ( (lv_contact_4_0= ruleEString ) ) )?
            int alt127=2;
            int LA127_0 = input.LA(1);

            if ( (LA127_0==104) ) {
                alt127=1;
            }
            switch (alt127) {
                case 1 :
                    // InternalPogoDsl.g:4679:4: otherlv_3= 'contact' ( (lv_contact_4_0= ruleEString ) )
                    {
                    otherlv_3=(Token)match(input,104,FOLLOW_13); 

                    				newLeafNode(otherlv_3, grammarAccess.getClassIdentificationAccess().getContactKeyword_3_0());
                    			
                    // InternalPogoDsl.g:4683:4: ( (lv_contact_4_0= ruleEString ) )
                    // InternalPogoDsl.g:4684:5: (lv_contact_4_0= ruleEString )
                    {
                    // InternalPogoDsl.g:4684:5: (lv_contact_4_0= ruleEString )
                    // InternalPogoDsl.g:4685:6: lv_contact_4_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getClassIdentificationAccess().getContactEStringParserRuleCall_3_1_0());
                    					
                    pushFollow(FOLLOW_136);
                    lv_contact_4_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getClassIdentificationRule());
                    						}
                    						set(
                    							current,
                    							"contact",
                    							lv_contact_4_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalPogoDsl.g:4703:3: (otherlv_5= 'author' ( (lv_author_6_0= ruleEString ) ) )?
            int alt128=2;
            int LA128_0 = input.LA(1);

            if ( (LA128_0==105) ) {
                alt128=1;
            }
            switch (alt128) {
                case 1 :
                    // InternalPogoDsl.g:4704:4: otherlv_5= 'author' ( (lv_author_6_0= ruleEString ) )
                    {
                    otherlv_5=(Token)match(input,105,FOLLOW_13); 

                    				newLeafNode(otherlv_5, grammarAccess.getClassIdentificationAccess().getAuthorKeyword_4_0());
                    			
                    // InternalPogoDsl.g:4708:4: ( (lv_author_6_0= ruleEString ) )
                    // InternalPogoDsl.g:4709:5: (lv_author_6_0= ruleEString )
                    {
                    // InternalPogoDsl.g:4709:5: (lv_author_6_0= ruleEString )
                    // InternalPogoDsl.g:4710:6: lv_author_6_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getClassIdentificationAccess().getAuthorEStringParserRuleCall_4_1_0());
                    					
                    pushFollow(FOLLOW_137);
                    lv_author_6_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getClassIdentificationRule());
                    						}
                    						set(
                    							current,
                    							"author",
                    							lv_author_6_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalPogoDsl.g:4728:3: (otherlv_7= 'emailDomain' ( (lv_emailDomain_8_0= ruleEString ) ) )?
            int alt129=2;
            int LA129_0 = input.LA(1);

            if ( (LA129_0==106) ) {
                alt129=1;
            }
            switch (alt129) {
                case 1 :
                    // InternalPogoDsl.g:4729:4: otherlv_7= 'emailDomain' ( (lv_emailDomain_8_0= ruleEString ) )
                    {
                    otherlv_7=(Token)match(input,106,FOLLOW_13); 

                    				newLeafNode(otherlv_7, grammarAccess.getClassIdentificationAccess().getEmailDomainKeyword_5_0());
                    			
                    // InternalPogoDsl.g:4733:4: ( (lv_emailDomain_8_0= ruleEString ) )
                    // InternalPogoDsl.g:4734:5: (lv_emailDomain_8_0= ruleEString )
                    {
                    // InternalPogoDsl.g:4734:5: (lv_emailDomain_8_0= ruleEString )
                    // InternalPogoDsl.g:4735:6: lv_emailDomain_8_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getClassIdentificationAccess().getEmailDomainEStringParserRuleCall_5_1_0());
                    					
                    pushFollow(FOLLOW_138);
                    lv_emailDomain_8_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getClassIdentificationRule());
                    						}
                    						set(
                    							current,
                    							"emailDomain",
                    							lv_emailDomain_8_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalPogoDsl.g:4753:3: (otherlv_9= 'classFamily' ( (lv_classFamily_10_0= ruleEString ) ) )?
            int alt130=2;
            int LA130_0 = input.LA(1);

            if ( (LA130_0==107) ) {
                alt130=1;
            }
            switch (alt130) {
                case 1 :
                    // InternalPogoDsl.g:4754:4: otherlv_9= 'classFamily' ( (lv_classFamily_10_0= ruleEString ) )
                    {
                    otherlv_9=(Token)match(input,107,FOLLOW_13); 

                    				newLeafNode(otherlv_9, grammarAccess.getClassIdentificationAccess().getClassFamilyKeyword_6_0());
                    			
                    // InternalPogoDsl.g:4758:4: ( (lv_classFamily_10_0= ruleEString ) )
                    // InternalPogoDsl.g:4759:5: (lv_classFamily_10_0= ruleEString )
                    {
                    // InternalPogoDsl.g:4759:5: (lv_classFamily_10_0= ruleEString )
                    // InternalPogoDsl.g:4760:6: lv_classFamily_10_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getClassIdentificationAccess().getClassFamilyEStringParserRuleCall_6_1_0());
                    					
                    pushFollow(FOLLOW_139);
                    lv_classFamily_10_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getClassIdentificationRule());
                    						}
                    						set(
                    							current,
                    							"classFamily",
                    							lv_classFamily_10_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalPogoDsl.g:4778:3: (otherlv_11= 'siteSpecific' ( (lv_siteSpecific_12_0= ruleEString ) ) )?
            int alt131=2;
            int LA131_0 = input.LA(1);

            if ( (LA131_0==108) ) {
                alt131=1;
            }
            switch (alt131) {
                case 1 :
                    // InternalPogoDsl.g:4779:4: otherlv_11= 'siteSpecific' ( (lv_siteSpecific_12_0= ruleEString ) )
                    {
                    otherlv_11=(Token)match(input,108,FOLLOW_13); 

                    				newLeafNode(otherlv_11, grammarAccess.getClassIdentificationAccess().getSiteSpecificKeyword_7_0());
                    			
                    // InternalPogoDsl.g:4783:4: ( (lv_siteSpecific_12_0= ruleEString ) )
                    // InternalPogoDsl.g:4784:5: (lv_siteSpecific_12_0= ruleEString )
                    {
                    // InternalPogoDsl.g:4784:5: (lv_siteSpecific_12_0= ruleEString )
                    // InternalPogoDsl.g:4785:6: lv_siteSpecific_12_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getClassIdentificationAccess().getSiteSpecificEStringParserRuleCall_7_1_0());
                    					
                    pushFollow(FOLLOW_140);
                    lv_siteSpecific_12_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getClassIdentificationRule());
                    						}
                    						set(
                    							current,
                    							"siteSpecific",
                    							lv_siteSpecific_12_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalPogoDsl.g:4803:3: (otherlv_13= 'platform' ( (lv_platform_14_0= ruleEString ) ) )?
            int alt132=2;
            int LA132_0 = input.LA(1);

            if ( (LA132_0==109) ) {
                alt132=1;
            }
            switch (alt132) {
                case 1 :
                    // InternalPogoDsl.g:4804:4: otherlv_13= 'platform' ( (lv_platform_14_0= ruleEString ) )
                    {
                    otherlv_13=(Token)match(input,109,FOLLOW_13); 

                    				newLeafNode(otherlv_13, grammarAccess.getClassIdentificationAccess().getPlatformKeyword_8_0());
                    			
                    // InternalPogoDsl.g:4808:4: ( (lv_platform_14_0= ruleEString ) )
                    // InternalPogoDsl.g:4809:5: (lv_platform_14_0= ruleEString )
                    {
                    // InternalPogoDsl.g:4809:5: (lv_platform_14_0= ruleEString )
                    // InternalPogoDsl.g:4810:6: lv_platform_14_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getClassIdentificationAccess().getPlatformEStringParserRuleCall_8_1_0());
                    					
                    pushFollow(FOLLOW_141);
                    lv_platform_14_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getClassIdentificationRule());
                    						}
                    						set(
                    							current,
                    							"platform",
                    							lv_platform_14_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalPogoDsl.g:4828:3: (otherlv_15= 'bus' ( (lv_bus_16_0= ruleEString ) ) )?
            int alt133=2;
            int LA133_0 = input.LA(1);

            if ( (LA133_0==110) ) {
                alt133=1;
            }
            switch (alt133) {
                case 1 :
                    // InternalPogoDsl.g:4829:4: otherlv_15= 'bus' ( (lv_bus_16_0= ruleEString ) )
                    {
                    otherlv_15=(Token)match(input,110,FOLLOW_13); 

                    				newLeafNode(otherlv_15, grammarAccess.getClassIdentificationAccess().getBusKeyword_9_0());
                    			
                    // InternalPogoDsl.g:4833:4: ( (lv_bus_16_0= ruleEString ) )
                    // InternalPogoDsl.g:4834:5: (lv_bus_16_0= ruleEString )
                    {
                    // InternalPogoDsl.g:4834:5: (lv_bus_16_0= ruleEString )
                    // InternalPogoDsl.g:4835:6: lv_bus_16_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getClassIdentificationAccess().getBusEStringParserRuleCall_9_1_0());
                    					
                    pushFollow(FOLLOW_142);
                    lv_bus_16_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getClassIdentificationRule());
                    						}
                    						set(
                    							current,
                    							"bus",
                    							lv_bus_16_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalPogoDsl.g:4853:3: (otherlv_17= 'manufacturer' ( (lv_manufacturer_18_0= ruleEString ) ) )?
            int alt134=2;
            int LA134_0 = input.LA(1);

            if ( (LA134_0==111) ) {
                alt134=1;
            }
            switch (alt134) {
                case 1 :
                    // InternalPogoDsl.g:4854:4: otherlv_17= 'manufacturer' ( (lv_manufacturer_18_0= ruleEString ) )
                    {
                    otherlv_17=(Token)match(input,111,FOLLOW_13); 

                    				newLeafNode(otherlv_17, grammarAccess.getClassIdentificationAccess().getManufacturerKeyword_10_0());
                    			
                    // InternalPogoDsl.g:4858:4: ( (lv_manufacturer_18_0= ruleEString ) )
                    // InternalPogoDsl.g:4859:5: (lv_manufacturer_18_0= ruleEString )
                    {
                    // InternalPogoDsl.g:4859:5: (lv_manufacturer_18_0= ruleEString )
                    // InternalPogoDsl.g:4860:6: lv_manufacturer_18_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getClassIdentificationAccess().getManufacturerEStringParserRuleCall_10_1_0());
                    					
                    pushFollow(FOLLOW_143);
                    lv_manufacturer_18_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getClassIdentificationRule());
                    						}
                    						set(
                    							current,
                    							"manufacturer",
                    							lv_manufacturer_18_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalPogoDsl.g:4878:3: (otherlv_19= 'reference' ( (lv_reference_20_0= ruleEString ) ) )?
            int alt135=2;
            int LA135_0 = input.LA(1);

            if ( (LA135_0==112) ) {
                alt135=1;
            }
            switch (alt135) {
                case 1 :
                    // InternalPogoDsl.g:4879:4: otherlv_19= 'reference' ( (lv_reference_20_0= ruleEString ) )
                    {
                    otherlv_19=(Token)match(input,112,FOLLOW_13); 

                    				newLeafNode(otherlv_19, grammarAccess.getClassIdentificationAccess().getReferenceKeyword_11_0());
                    			
                    // InternalPogoDsl.g:4883:4: ( (lv_reference_20_0= ruleEString ) )
                    // InternalPogoDsl.g:4884:5: (lv_reference_20_0= ruleEString )
                    {
                    // InternalPogoDsl.g:4884:5: (lv_reference_20_0= ruleEString )
                    // InternalPogoDsl.g:4885:6: lv_reference_20_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getClassIdentificationAccess().getReferenceEStringParserRuleCall_11_1_0());
                    					
                    pushFollow(FOLLOW_144);
                    lv_reference_20_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getClassIdentificationRule());
                    						}
                    						set(
                    							current,
                    							"reference",
                    							lv_reference_20_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalPogoDsl.g:4903:3: (otherlv_21= 'keyWords' otherlv_22= '{' ( (lv_keyWords_23_0= ruleEString ) ) (otherlv_24= ',' ( (lv_keyWords_25_0= ruleEString ) ) )* otherlv_26= '}' )?
            int alt137=2;
            int LA137_0 = input.LA(1);

            if ( (LA137_0==113) ) {
                alt137=1;
            }
            switch (alt137) {
                case 1 :
                    // InternalPogoDsl.g:4904:4: otherlv_21= 'keyWords' otherlv_22= '{' ( (lv_keyWords_23_0= ruleEString ) ) (otherlv_24= ',' ( (lv_keyWords_25_0= ruleEString ) ) )* otherlv_26= '}'
                    {
                    otherlv_21=(Token)match(input,113,FOLLOW_3); 

                    				newLeafNode(otherlv_21, grammarAccess.getClassIdentificationAccess().getKeyWordsKeyword_12_0());
                    			
                    otherlv_22=(Token)match(input,12,FOLLOW_13); 

                    				newLeafNode(otherlv_22, grammarAccess.getClassIdentificationAccess().getLeftCurlyBracketKeyword_12_1());
                    			
                    // InternalPogoDsl.g:4912:4: ( (lv_keyWords_23_0= ruleEString ) )
                    // InternalPogoDsl.g:4913:5: (lv_keyWords_23_0= ruleEString )
                    {
                    // InternalPogoDsl.g:4913:5: (lv_keyWords_23_0= ruleEString )
                    // InternalPogoDsl.g:4914:6: lv_keyWords_23_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getClassIdentificationAccess().getKeyWordsEStringParserRuleCall_12_2_0());
                    					
                    pushFollow(FOLLOW_6);
                    lv_keyWords_23_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getClassIdentificationRule());
                    						}
                    						add(
                    							current,
                    							"keyWords",
                    							lv_keyWords_23_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalPogoDsl.g:4931:4: (otherlv_24= ',' ( (lv_keyWords_25_0= ruleEString ) ) )*
                    loop136:
                    do {
                        int alt136=2;
                        int LA136_0 = input.LA(1);

                        if ( (LA136_0==14) ) {
                            alt136=1;
                        }


                        switch (alt136) {
                    	case 1 :
                    	    // InternalPogoDsl.g:4932:5: otherlv_24= ',' ( (lv_keyWords_25_0= ruleEString ) )
                    	    {
                    	    otherlv_24=(Token)match(input,14,FOLLOW_13); 

                    	    					newLeafNode(otherlv_24, grammarAccess.getClassIdentificationAccess().getCommaKeyword_12_3_0());
                    	    				
                    	    // InternalPogoDsl.g:4936:5: ( (lv_keyWords_25_0= ruleEString ) )
                    	    // InternalPogoDsl.g:4937:6: (lv_keyWords_25_0= ruleEString )
                    	    {
                    	    // InternalPogoDsl.g:4937:6: (lv_keyWords_25_0= ruleEString )
                    	    // InternalPogoDsl.g:4938:7: lv_keyWords_25_0= ruleEString
                    	    {

                    	    							newCompositeNode(grammarAccess.getClassIdentificationAccess().getKeyWordsEStringParserRuleCall_12_3_1_0());
                    	    						
                    	    pushFollow(FOLLOW_6);
                    	    lv_keyWords_25_0=ruleEString();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getClassIdentificationRule());
                    	    							}
                    	    							add(
                    	    								current,
                    	    								"keyWords",
                    	    								lv_keyWords_25_0,
                    	    								"com.mncml.implementation.pogo.dsl.PogoDsl.EString");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop136;
                        }
                    } while (true);

                    otherlv_26=(Token)match(input,15,FOLLOW_11); 

                    				newLeafNode(otherlv_26, grammarAccess.getClassIdentificationAccess().getRightCurlyBracketKeyword_12_4());
                    			

                    }
                    break;

            }

            otherlv_27=(Token)match(input,15,FOLLOW_2); 

            			newLeafNode(otherlv_27, grammarAccess.getClassIdentificationAccess().getRightCurlyBracketKeyword_13());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleClassIdentification"


    // $ANTLR start "entryRuleComments"
    // InternalPogoDsl.g:4969:1: entryRuleComments returns [EObject current=null] : iv_ruleComments= ruleComments EOF ;
    public final EObject entryRuleComments() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleComments = null;


        try {
            // InternalPogoDsl.g:4969:49: (iv_ruleComments= ruleComments EOF )
            // InternalPogoDsl.g:4970:2: iv_ruleComments= ruleComments EOF
            {
             newCompositeNode(grammarAccess.getCommentsRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleComments=ruleComments();

            state._fsp--;

             current =iv_ruleComments; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleComments"


    // $ANTLR start "ruleComments"
    // InternalPogoDsl.g:4976:1: ruleComments returns [EObject current=null] : ( () otherlv_1= 'Comments' otherlv_2= '{' (otherlv_3= 'commandsTable' ( (lv_commandsTable_4_0= ruleEString ) ) )? otherlv_5= '}' ) ;
    public final EObject ruleComments() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        AntlrDatatypeRuleToken lv_commandsTable_4_0 = null;



        	enterRule();

        try {
            // InternalPogoDsl.g:4982:2: ( ( () otherlv_1= 'Comments' otherlv_2= '{' (otherlv_3= 'commandsTable' ( (lv_commandsTable_4_0= ruleEString ) ) )? otherlv_5= '}' ) )
            // InternalPogoDsl.g:4983:2: ( () otherlv_1= 'Comments' otherlv_2= '{' (otherlv_3= 'commandsTable' ( (lv_commandsTable_4_0= ruleEString ) ) )? otherlv_5= '}' )
            {
            // InternalPogoDsl.g:4983:2: ( () otherlv_1= 'Comments' otherlv_2= '{' (otherlv_3= 'commandsTable' ( (lv_commandsTable_4_0= ruleEString ) ) )? otherlv_5= '}' )
            // InternalPogoDsl.g:4984:3: () otherlv_1= 'Comments' otherlv_2= '{' (otherlv_3= 'commandsTable' ( (lv_commandsTable_4_0= ruleEString ) ) )? otherlv_5= '}'
            {
            // InternalPogoDsl.g:4984:3: ()
            // InternalPogoDsl.g:4985:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getCommentsAccess().getCommentsAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,114,FOLLOW_3); 

            			newLeafNode(otherlv_1, grammarAccess.getCommentsAccess().getCommentsKeyword_1());
            		
            otherlv_2=(Token)match(input,12,FOLLOW_145); 

            			newLeafNode(otherlv_2, grammarAccess.getCommentsAccess().getLeftCurlyBracketKeyword_2());
            		
            // InternalPogoDsl.g:4999:3: (otherlv_3= 'commandsTable' ( (lv_commandsTable_4_0= ruleEString ) ) )?
            int alt138=2;
            int LA138_0 = input.LA(1);

            if ( (LA138_0==115) ) {
                alt138=1;
            }
            switch (alt138) {
                case 1 :
                    // InternalPogoDsl.g:5000:4: otherlv_3= 'commandsTable' ( (lv_commandsTable_4_0= ruleEString ) )
                    {
                    otherlv_3=(Token)match(input,115,FOLLOW_13); 

                    				newLeafNode(otherlv_3, grammarAccess.getCommentsAccess().getCommandsTableKeyword_3_0());
                    			
                    // InternalPogoDsl.g:5004:4: ( (lv_commandsTable_4_0= ruleEString ) )
                    // InternalPogoDsl.g:5005:5: (lv_commandsTable_4_0= ruleEString )
                    {
                    // InternalPogoDsl.g:5005:5: (lv_commandsTable_4_0= ruleEString )
                    // InternalPogoDsl.g:5006:6: lv_commandsTable_4_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getCommentsAccess().getCommandsTableEStringParserRuleCall_3_1_0());
                    					
                    pushFollow(FOLLOW_11);
                    lv_commandsTable_4_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getCommentsRule());
                    						}
                    						set(
                    							current,
                    							"commandsTable",
                    							lv_commandsTable_4_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            otherlv_5=(Token)match(input,15,FOLLOW_2); 

            			newLeafNode(otherlv_5, grammarAccess.getCommentsAccess().getRightCurlyBracketKeyword_4());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleComments"


    // $ANTLR start "entryRulePropType_Impl"
    // InternalPogoDsl.g:5032:1: entryRulePropType_Impl returns [EObject current=null] : iv_rulePropType_Impl= rulePropType_Impl EOF ;
    public final EObject entryRulePropType_Impl() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePropType_Impl = null;


        try {
            // InternalPogoDsl.g:5032:54: (iv_rulePropType_Impl= rulePropType_Impl EOF )
            // InternalPogoDsl.g:5033:2: iv_rulePropType_Impl= rulePropType_Impl EOF
            {
             newCompositeNode(grammarAccess.getPropType_ImplRule()); 
            pushFollow(FOLLOW_1);
            iv_rulePropType_Impl=rulePropType_Impl();

            state._fsp--;

             current =iv_rulePropType_Impl; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePropType_Impl"


    // $ANTLR start "rulePropType_Impl"
    // InternalPogoDsl.g:5039:1: rulePropType_Impl returns [EObject current=null] : ( () otherlv_1= 'PropType' ) ;
    public final EObject rulePropType_Impl() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;


        	enterRule();

        try {
            // InternalPogoDsl.g:5045:2: ( ( () otherlv_1= 'PropType' ) )
            // InternalPogoDsl.g:5046:2: ( () otherlv_1= 'PropType' )
            {
            // InternalPogoDsl.g:5046:2: ( () otherlv_1= 'PropType' )
            // InternalPogoDsl.g:5047:3: () otherlv_1= 'PropType'
            {
            // InternalPogoDsl.g:5047:3: ()
            // InternalPogoDsl.g:5048:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getPropType_ImplAccess().getPropTypeAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,116,FOLLOW_2); 

            			newLeafNode(otherlv_1, grammarAccess.getPropType_ImplAccess().getPropTypeKeyword_1());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePropType_Impl"


    // $ANTLR start "entryRuleInheritanceStatus"
    // InternalPogoDsl.g:5062:1: entryRuleInheritanceStatus returns [EObject current=null] : iv_ruleInheritanceStatus= ruleInheritanceStatus EOF ;
    public final EObject entryRuleInheritanceStatus() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleInheritanceStatus = null;


        try {
            // InternalPogoDsl.g:5062:58: (iv_ruleInheritanceStatus= ruleInheritanceStatus EOF )
            // InternalPogoDsl.g:5063:2: iv_ruleInheritanceStatus= ruleInheritanceStatus EOF
            {
             newCompositeNode(grammarAccess.getInheritanceStatusRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleInheritanceStatus=ruleInheritanceStatus();

            state._fsp--;

             current =iv_ruleInheritanceStatus; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleInheritanceStatus"


    // $ANTLR start "ruleInheritanceStatus"
    // InternalPogoDsl.g:5069:1: ruleInheritanceStatus returns [EObject current=null] : ( () otherlv_1= 'InheritanceStatus' otherlv_2= '{' (otherlv_3= 'abstract' ( (lv_abstract_4_0= ruleEString ) ) )? (otherlv_5= 'inherited' ( (lv_inherited_6_0= ruleEString ) ) )? (otherlv_7= 'concrete' ( (lv_concrete_8_0= ruleEString ) ) )? (otherlv_9= 'concreteHere' ( (lv_concreteHere_10_0= ruleEString ) ) )? (otherlv_11= 'hasChanged' ( (lv_hasChanged_12_0= ruleEString ) ) )? otherlv_13= '}' ) ;
    public final EObject ruleInheritanceStatus() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        Token otherlv_11=null;
        Token otherlv_13=null;
        AntlrDatatypeRuleToken lv_abstract_4_0 = null;

        AntlrDatatypeRuleToken lv_inherited_6_0 = null;

        AntlrDatatypeRuleToken lv_concrete_8_0 = null;

        AntlrDatatypeRuleToken lv_concreteHere_10_0 = null;

        AntlrDatatypeRuleToken lv_hasChanged_12_0 = null;



        	enterRule();

        try {
            // InternalPogoDsl.g:5075:2: ( ( () otherlv_1= 'InheritanceStatus' otherlv_2= '{' (otherlv_3= 'abstract' ( (lv_abstract_4_0= ruleEString ) ) )? (otherlv_5= 'inherited' ( (lv_inherited_6_0= ruleEString ) ) )? (otherlv_7= 'concrete' ( (lv_concrete_8_0= ruleEString ) ) )? (otherlv_9= 'concreteHere' ( (lv_concreteHere_10_0= ruleEString ) ) )? (otherlv_11= 'hasChanged' ( (lv_hasChanged_12_0= ruleEString ) ) )? otherlv_13= '}' ) )
            // InternalPogoDsl.g:5076:2: ( () otherlv_1= 'InheritanceStatus' otherlv_2= '{' (otherlv_3= 'abstract' ( (lv_abstract_4_0= ruleEString ) ) )? (otherlv_5= 'inherited' ( (lv_inherited_6_0= ruleEString ) ) )? (otherlv_7= 'concrete' ( (lv_concrete_8_0= ruleEString ) ) )? (otherlv_9= 'concreteHere' ( (lv_concreteHere_10_0= ruleEString ) ) )? (otherlv_11= 'hasChanged' ( (lv_hasChanged_12_0= ruleEString ) ) )? otherlv_13= '}' )
            {
            // InternalPogoDsl.g:5076:2: ( () otherlv_1= 'InheritanceStatus' otherlv_2= '{' (otherlv_3= 'abstract' ( (lv_abstract_4_0= ruleEString ) ) )? (otherlv_5= 'inherited' ( (lv_inherited_6_0= ruleEString ) ) )? (otherlv_7= 'concrete' ( (lv_concrete_8_0= ruleEString ) ) )? (otherlv_9= 'concreteHere' ( (lv_concreteHere_10_0= ruleEString ) ) )? (otherlv_11= 'hasChanged' ( (lv_hasChanged_12_0= ruleEString ) ) )? otherlv_13= '}' )
            // InternalPogoDsl.g:5077:3: () otherlv_1= 'InheritanceStatus' otherlv_2= '{' (otherlv_3= 'abstract' ( (lv_abstract_4_0= ruleEString ) ) )? (otherlv_5= 'inherited' ( (lv_inherited_6_0= ruleEString ) ) )? (otherlv_7= 'concrete' ( (lv_concrete_8_0= ruleEString ) ) )? (otherlv_9= 'concreteHere' ( (lv_concreteHere_10_0= ruleEString ) ) )? (otherlv_11= 'hasChanged' ( (lv_hasChanged_12_0= ruleEString ) ) )? otherlv_13= '}'
            {
            // InternalPogoDsl.g:5077:3: ()
            // InternalPogoDsl.g:5078:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getInheritanceStatusAccess().getInheritanceStatusAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,117,FOLLOW_3); 

            			newLeafNode(otherlv_1, grammarAccess.getInheritanceStatusAccess().getInheritanceStatusKeyword_1());
            		
            otherlv_2=(Token)match(input,12,FOLLOW_146); 

            			newLeafNode(otherlv_2, grammarAccess.getInheritanceStatusAccess().getLeftCurlyBracketKeyword_2());
            		
            // InternalPogoDsl.g:5092:3: (otherlv_3= 'abstract' ( (lv_abstract_4_0= ruleEString ) ) )?
            int alt139=2;
            int LA139_0 = input.LA(1);

            if ( (LA139_0==118) ) {
                alt139=1;
            }
            switch (alt139) {
                case 1 :
                    // InternalPogoDsl.g:5093:4: otherlv_3= 'abstract' ( (lv_abstract_4_0= ruleEString ) )
                    {
                    otherlv_3=(Token)match(input,118,FOLLOW_13); 

                    				newLeafNode(otherlv_3, grammarAccess.getInheritanceStatusAccess().getAbstractKeyword_3_0());
                    			
                    // InternalPogoDsl.g:5097:4: ( (lv_abstract_4_0= ruleEString ) )
                    // InternalPogoDsl.g:5098:5: (lv_abstract_4_0= ruleEString )
                    {
                    // InternalPogoDsl.g:5098:5: (lv_abstract_4_0= ruleEString )
                    // InternalPogoDsl.g:5099:6: lv_abstract_4_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getInheritanceStatusAccess().getAbstractEStringParserRuleCall_3_1_0());
                    					
                    pushFollow(FOLLOW_147);
                    lv_abstract_4_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getInheritanceStatusRule());
                    						}
                    						set(
                    							current,
                    							"abstract",
                    							lv_abstract_4_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalPogoDsl.g:5117:3: (otherlv_5= 'inherited' ( (lv_inherited_6_0= ruleEString ) ) )?
            int alt140=2;
            int LA140_0 = input.LA(1);

            if ( (LA140_0==119) ) {
                alt140=1;
            }
            switch (alt140) {
                case 1 :
                    // InternalPogoDsl.g:5118:4: otherlv_5= 'inherited' ( (lv_inherited_6_0= ruleEString ) )
                    {
                    otherlv_5=(Token)match(input,119,FOLLOW_13); 

                    				newLeafNode(otherlv_5, grammarAccess.getInheritanceStatusAccess().getInheritedKeyword_4_0());
                    			
                    // InternalPogoDsl.g:5122:4: ( (lv_inherited_6_0= ruleEString ) )
                    // InternalPogoDsl.g:5123:5: (lv_inherited_6_0= ruleEString )
                    {
                    // InternalPogoDsl.g:5123:5: (lv_inherited_6_0= ruleEString )
                    // InternalPogoDsl.g:5124:6: lv_inherited_6_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getInheritanceStatusAccess().getInheritedEStringParserRuleCall_4_1_0());
                    					
                    pushFollow(FOLLOW_148);
                    lv_inherited_6_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getInheritanceStatusRule());
                    						}
                    						set(
                    							current,
                    							"inherited",
                    							lv_inherited_6_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalPogoDsl.g:5142:3: (otherlv_7= 'concrete' ( (lv_concrete_8_0= ruleEString ) ) )?
            int alt141=2;
            int LA141_0 = input.LA(1);

            if ( (LA141_0==120) ) {
                alt141=1;
            }
            switch (alt141) {
                case 1 :
                    // InternalPogoDsl.g:5143:4: otherlv_7= 'concrete' ( (lv_concrete_8_0= ruleEString ) )
                    {
                    otherlv_7=(Token)match(input,120,FOLLOW_13); 

                    				newLeafNode(otherlv_7, grammarAccess.getInheritanceStatusAccess().getConcreteKeyword_5_0());
                    			
                    // InternalPogoDsl.g:5147:4: ( (lv_concrete_8_0= ruleEString ) )
                    // InternalPogoDsl.g:5148:5: (lv_concrete_8_0= ruleEString )
                    {
                    // InternalPogoDsl.g:5148:5: (lv_concrete_8_0= ruleEString )
                    // InternalPogoDsl.g:5149:6: lv_concrete_8_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getInheritanceStatusAccess().getConcreteEStringParserRuleCall_5_1_0());
                    					
                    pushFollow(FOLLOW_149);
                    lv_concrete_8_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getInheritanceStatusRule());
                    						}
                    						set(
                    							current,
                    							"concrete",
                    							lv_concrete_8_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalPogoDsl.g:5167:3: (otherlv_9= 'concreteHere' ( (lv_concreteHere_10_0= ruleEString ) ) )?
            int alt142=2;
            int LA142_0 = input.LA(1);

            if ( (LA142_0==121) ) {
                alt142=1;
            }
            switch (alt142) {
                case 1 :
                    // InternalPogoDsl.g:5168:4: otherlv_9= 'concreteHere' ( (lv_concreteHere_10_0= ruleEString ) )
                    {
                    otherlv_9=(Token)match(input,121,FOLLOW_13); 

                    				newLeafNode(otherlv_9, grammarAccess.getInheritanceStatusAccess().getConcreteHereKeyword_6_0());
                    			
                    // InternalPogoDsl.g:5172:4: ( (lv_concreteHere_10_0= ruleEString ) )
                    // InternalPogoDsl.g:5173:5: (lv_concreteHere_10_0= ruleEString )
                    {
                    // InternalPogoDsl.g:5173:5: (lv_concreteHere_10_0= ruleEString )
                    // InternalPogoDsl.g:5174:6: lv_concreteHere_10_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getInheritanceStatusAccess().getConcreteHereEStringParserRuleCall_6_1_0());
                    					
                    pushFollow(FOLLOW_150);
                    lv_concreteHere_10_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getInheritanceStatusRule());
                    						}
                    						set(
                    							current,
                    							"concreteHere",
                    							lv_concreteHere_10_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalPogoDsl.g:5192:3: (otherlv_11= 'hasChanged' ( (lv_hasChanged_12_0= ruleEString ) ) )?
            int alt143=2;
            int LA143_0 = input.LA(1);

            if ( (LA143_0==122) ) {
                alt143=1;
            }
            switch (alt143) {
                case 1 :
                    // InternalPogoDsl.g:5193:4: otherlv_11= 'hasChanged' ( (lv_hasChanged_12_0= ruleEString ) )
                    {
                    otherlv_11=(Token)match(input,122,FOLLOW_13); 

                    				newLeafNode(otherlv_11, grammarAccess.getInheritanceStatusAccess().getHasChangedKeyword_7_0());
                    			
                    // InternalPogoDsl.g:5197:4: ( (lv_hasChanged_12_0= ruleEString ) )
                    // InternalPogoDsl.g:5198:5: (lv_hasChanged_12_0= ruleEString )
                    {
                    // InternalPogoDsl.g:5198:5: (lv_hasChanged_12_0= ruleEString )
                    // InternalPogoDsl.g:5199:6: lv_hasChanged_12_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getInheritanceStatusAccess().getHasChangedEStringParserRuleCall_7_1_0());
                    					
                    pushFollow(FOLLOW_11);
                    lv_hasChanged_12_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getInheritanceStatusRule());
                    						}
                    						set(
                    							current,
                    							"hasChanged",
                    							lv_hasChanged_12_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            otherlv_13=(Token)match(input,15,FOLLOW_2); 

            			newLeafNode(otherlv_13, grammarAccess.getInheritanceStatusAccess().getRightCurlyBracketKeyword_8());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleInheritanceStatus"


    // $ANTLR start "entryRuleSimpleType_Impl"
    // InternalPogoDsl.g:5225:1: entryRuleSimpleType_Impl returns [EObject current=null] : iv_ruleSimpleType_Impl= ruleSimpleType_Impl EOF ;
    public final EObject entryRuleSimpleType_Impl() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSimpleType_Impl = null;


        try {
            // InternalPogoDsl.g:5225:56: (iv_ruleSimpleType_Impl= ruleSimpleType_Impl EOF )
            // InternalPogoDsl.g:5226:2: iv_ruleSimpleType_Impl= ruleSimpleType_Impl EOF
            {
             newCompositeNode(grammarAccess.getSimpleType_ImplRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleSimpleType_Impl=ruleSimpleType_Impl();

            state._fsp--;

             current =iv_ruleSimpleType_Impl; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSimpleType_Impl"


    // $ANTLR start "ruleSimpleType_Impl"
    // InternalPogoDsl.g:5232:1: ruleSimpleType_Impl returns [EObject current=null] : ( () otherlv_1= 'SimpleType' ) ;
    public final EObject ruleSimpleType_Impl() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;


        	enterRule();

        try {
            // InternalPogoDsl.g:5238:2: ( ( () otherlv_1= 'SimpleType' ) )
            // InternalPogoDsl.g:5239:2: ( () otherlv_1= 'SimpleType' )
            {
            // InternalPogoDsl.g:5239:2: ( () otherlv_1= 'SimpleType' )
            // InternalPogoDsl.g:5240:3: () otherlv_1= 'SimpleType'
            {
            // InternalPogoDsl.g:5240:3: ()
            // InternalPogoDsl.g:5241:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getSimpleType_ImplAccess().getSimpleTypeAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,123,FOLLOW_2); 

            			newLeafNode(otherlv_1, grammarAccess.getSimpleType_ImplAccess().getSimpleTypeKeyword_1());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSimpleType_Impl"


    // $ANTLR start "entryRuleVectorType_Impl"
    // InternalPogoDsl.g:5255:1: entryRuleVectorType_Impl returns [EObject current=null] : iv_ruleVectorType_Impl= ruleVectorType_Impl EOF ;
    public final EObject entryRuleVectorType_Impl() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleVectorType_Impl = null;


        try {
            // InternalPogoDsl.g:5255:56: (iv_ruleVectorType_Impl= ruleVectorType_Impl EOF )
            // InternalPogoDsl.g:5256:2: iv_ruleVectorType_Impl= ruleVectorType_Impl EOF
            {
             newCompositeNode(grammarAccess.getVectorType_ImplRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleVectorType_Impl=ruleVectorType_Impl();

            state._fsp--;

             current =iv_ruleVectorType_Impl; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleVectorType_Impl"


    // $ANTLR start "ruleVectorType_Impl"
    // InternalPogoDsl.g:5262:1: ruleVectorType_Impl returns [EObject current=null] : ( () otherlv_1= 'VectorType' ) ;
    public final EObject ruleVectorType_Impl() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;


        	enterRule();

        try {
            // InternalPogoDsl.g:5268:2: ( ( () otherlv_1= 'VectorType' ) )
            // InternalPogoDsl.g:5269:2: ( () otherlv_1= 'VectorType' )
            {
            // InternalPogoDsl.g:5269:2: ( () otherlv_1= 'VectorType' )
            // InternalPogoDsl.g:5270:3: () otherlv_1= 'VectorType'
            {
            // InternalPogoDsl.g:5270:3: ()
            // InternalPogoDsl.g:5271:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getVectorType_ImplAccess().getVectorTypeAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,124,FOLLOW_2); 

            			newLeafNode(otherlv_1, grammarAccess.getVectorType_ImplAccess().getVectorTypeKeyword_1());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleVectorType_Impl"


    // $ANTLR start "entryRuleBooleanType"
    // InternalPogoDsl.g:5285:1: entryRuleBooleanType returns [EObject current=null] : iv_ruleBooleanType= ruleBooleanType EOF ;
    public final EObject entryRuleBooleanType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBooleanType = null;


        try {
            // InternalPogoDsl.g:5285:52: (iv_ruleBooleanType= ruleBooleanType EOF )
            // InternalPogoDsl.g:5286:2: iv_ruleBooleanType= ruleBooleanType EOF
            {
             newCompositeNode(grammarAccess.getBooleanTypeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleBooleanType=ruleBooleanType();

            state._fsp--;

             current =iv_ruleBooleanType; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBooleanType"


    // $ANTLR start "ruleBooleanType"
    // InternalPogoDsl.g:5292:1: ruleBooleanType returns [EObject current=null] : ( () otherlv_1= 'BooleanType' ) ;
    public final EObject ruleBooleanType() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;


        	enterRule();

        try {
            // InternalPogoDsl.g:5298:2: ( ( () otherlv_1= 'BooleanType' ) )
            // InternalPogoDsl.g:5299:2: ( () otherlv_1= 'BooleanType' )
            {
            // InternalPogoDsl.g:5299:2: ( () otherlv_1= 'BooleanType' )
            // InternalPogoDsl.g:5300:3: () otherlv_1= 'BooleanType'
            {
            // InternalPogoDsl.g:5300:3: ()
            // InternalPogoDsl.g:5301:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getBooleanTypeAccess().getBooleanTypeAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,125,FOLLOW_2); 

            			newLeafNode(otherlv_1, grammarAccess.getBooleanTypeAccess().getBooleanTypeKeyword_1());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBooleanType"


    // $ANTLR start "entryRuleShortType"
    // InternalPogoDsl.g:5315:1: entryRuleShortType returns [EObject current=null] : iv_ruleShortType= ruleShortType EOF ;
    public final EObject entryRuleShortType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleShortType = null;


        try {
            // InternalPogoDsl.g:5315:50: (iv_ruleShortType= ruleShortType EOF )
            // InternalPogoDsl.g:5316:2: iv_ruleShortType= ruleShortType EOF
            {
             newCompositeNode(grammarAccess.getShortTypeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleShortType=ruleShortType();

            state._fsp--;

             current =iv_ruleShortType; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleShortType"


    // $ANTLR start "ruleShortType"
    // InternalPogoDsl.g:5322:1: ruleShortType returns [EObject current=null] : ( () otherlv_1= 'ShortType' ) ;
    public final EObject ruleShortType() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;


        	enterRule();

        try {
            // InternalPogoDsl.g:5328:2: ( ( () otherlv_1= 'ShortType' ) )
            // InternalPogoDsl.g:5329:2: ( () otherlv_1= 'ShortType' )
            {
            // InternalPogoDsl.g:5329:2: ( () otherlv_1= 'ShortType' )
            // InternalPogoDsl.g:5330:3: () otherlv_1= 'ShortType'
            {
            // InternalPogoDsl.g:5330:3: ()
            // InternalPogoDsl.g:5331:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getShortTypeAccess().getShortTypeAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,126,FOLLOW_2); 

            			newLeafNode(otherlv_1, grammarAccess.getShortTypeAccess().getShortTypeKeyword_1());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleShortType"


    // $ANTLR start "entryRuleUShortType"
    // InternalPogoDsl.g:5345:1: entryRuleUShortType returns [EObject current=null] : iv_ruleUShortType= ruleUShortType EOF ;
    public final EObject entryRuleUShortType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleUShortType = null;


        try {
            // InternalPogoDsl.g:5345:51: (iv_ruleUShortType= ruleUShortType EOF )
            // InternalPogoDsl.g:5346:2: iv_ruleUShortType= ruleUShortType EOF
            {
             newCompositeNode(grammarAccess.getUShortTypeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleUShortType=ruleUShortType();

            state._fsp--;

             current =iv_ruleUShortType; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleUShortType"


    // $ANTLR start "ruleUShortType"
    // InternalPogoDsl.g:5352:1: ruleUShortType returns [EObject current=null] : ( () otherlv_1= 'UShortType' ) ;
    public final EObject ruleUShortType() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;


        	enterRule();

        try {
            // InternalPogoDsl.g:5358:2: ( ( () otherlv_1= 'UShortType' ) )
            // InternalPogoDsl.g:5359:2: ( () otherlv_1= 'UShortType' )
            {
            // InternalPogoDsl.g:5359:2: ( () otherlv_1= 'UShortType' )
            // InternalPogoDsl.g:5360:3: () otherlv_1= 'UShortType'
            {
            // InternalPogoDsl.g:5360:3: ()
            // InternalPogoDsl.g:5361:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getUShortTypeAccess().getUShortTypeAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,127,FOLLOW_2); 

            			newLeafNode(otherlv_1, grammarAccess.getUShortTypeAccess().getUShortTypeKeyword_1());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleUShortType"


    // $ANTLR start "entryRuleIntType"
    // InternalPogoDsl.g:5375:1: entryRuleIntType returns [EObject current=null] : iv_ruleIntType= ruleIntType EOF ;
    public final EObject entryRuleIntType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleIntType = null;


        try {
            // InternalPogoDsl.g:5375:48: (iv_ruleIntType= ruleIntType EOF )
            // InternalPogoDsl.g:5376:2: iv_ruleIntType= ruleIntType EOF
            {
             newCompositeNode(grammarAccess.getIntTypeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleIntType=ruleIntType();

            state._fsp--;

             current =iv_ruleIntType; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleIntType"


    // $ANTLR start "ruleIntType"
    // InternalPogoDsl.g:5382:1: ruleIntType returns [EObject current=null] : ( () otherlv_1= 'IntType' ) ;
    public final EObject ruleIntType() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;


        	enterRule();

        try {
            // InternalPogoDsl.g:5388:2: ( ( () otherlv_1= 'IntType' ) )
            // InternalPogoDsl.g:5389:2: ( () otherlv_1= 'IntType' )
            {
            // InternalPogoDsl.g:5389:2: ( () otherlv_1= 'IntType' )
            // InternalPogoDsl.g:5390:3: () otherlv_1= 'IntType'
            {
            // InternalPogoDsl.g:5390:3: ()
            // InternalPogoDsl.g:5391:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getIntTypeAccess().getIntTypeAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,128,FOLLOW_2); 

            			newLeafNode(otherlv_1, grammarAccess.getIntTypeAccess().getIntTypeKeyword_1());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleIntType"


    // $ANTLR start "entryRuleUIntType"
    // InternalPogoDsl.g:5405:1: entryRuleUIntType returns [EObject current=null] : iv_ruleUIntType= ruleUIntType EOF ;
    public final EObject entryRuleUIntType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleUIntType = null;


        try {
            // InternalPogoDsl.g:5405:49: (iv_ruleUIntType= ruleUIntType EOF )
            // InternalPogoDsl.g:5406:2: iv_ruleUIntType= ruleUIntType EOF
            {
             newCompositeNode(grammarAccess.getUIntTypeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleUIntType=ruleUIntType();

            state._fsp--;

             current =iv_ruleUIntType; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleUIntType"


    // $ANTLR start "ruleUIntType"
    // InternalPogoDsl.g:5412:1: ruleUIntType returns [EObject current=null] : ( () otherlv_1= 'UIntType' ) ;
    public final EObject ruleUIntType() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;


        	enterRule();

        try {
            // InternalPogoDsl.g:5418:2: ( ( () otherlv_1= 'UIntType' ) )
            // InternalPogoDsl.g:5419:2: ( () otherlv_1= 'UIntType' )
            {
            // InternalPogoDsl.g:5419:2: ( () otherlv_1= 'UIntType' )
            // InternalPogoDsl.g:5420:3: () otherlv_1= 'UIntType'
            {
            // InternalPogoDsl.g:5420:3: ()
            // InternalPogoDsl.g:5421:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getUIntTypeAccess().getUIntTypeAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,129,FOLLOW_2); 

            			newLeafNode(otherlv_1, grammarAccess.getUIntTypeAccess().getUIntTypeKeyword_1());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleUIntType"


    // $ANTLR start "entryRuleFloatType"
    // InternalPogoDsl.g:5435:1: entryRuleFloatType returns [EObject current=null] : iv_ruleFloatType= ruleFloatType EOF ;
    public final EObject entryRuleFloatType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFloatType = null;


        try {
            // InternalPogoDsl.g:5435:50: (iv_ruleFloatType= ruleFloatType EOF )
            // InternalPogoDsl.g:5436:2: iv_ruleFloatType= ruleFloatType EOF
            {
             newCompositeNode(grammarAccess.getFloatTypeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleFloatType=ruleFloatType();

            state._fsp--;

             current =iv_ruleFloatType; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFloatType"


    // $ANTLR start "ruleFloatType"
    // InternalPogoDsl.g:5442:1: ruleFloatType returns [EObject current=null] : ( () otherlv_1= 'FloatType' ) ;
    public final EObject ruleFloatType() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;


        	enterRule();

        try {
            // InternalPogoDsl.g:5448:2: ( ( () otherlv_1= 'FloatType' ) )
            // InternalPogoDsl.g:5449:2: ( () otherlv_1= 'FloatType' )
            {
            // InternalPogoDsl.g:5449:2: ( () otherlv_1= 'FloatType' )
            // InternalPogoDsl.g:5450:3: () otherlv_1= 'FloatType'
            {
            // InternalPogoDsl.g:5450:3: ()
            // InternalPogoDsl.g:5451:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getFloatTypeAccess().getFloatTypeAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,130,FOLLOW_2); 

            			newLeafNode(otherlv_1, grammarAccess.getFloatTypeAccess().getFloatTypeKeyword_1());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFloatType"


    // $ANTLR start "entryRuleDoubleType"
    // InternalPogoDsl.g:5465:1: entryRuleDoubleType returns [EObject current=null] : iv_ruleDoubleType= ruleDoubleType EOF ;
    public final EObject entryRuleDoubleType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDoubleType = null;


        try {
            // InternalPogoDsl.g:5465:51: (iv_ruleDoubleType= ruleDoubleType EOF )
            // InternalPogoDsl.g:5466:2: iv_ruleDoubleType= ruleDoubleType EOF
            {
             newCompositeNode(grammarAccess.getDoubleTypeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleDoubleType=ruleDoubleType();

            state._fsp--;

             current =iv_ruleDoubleType; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDoubleType"


    // $ANTLR start "ruleDoubleType"
    // InternalPogoDsl.g:5472:1: ruleDoubleType returns [EObject current=null] : ( () otherlv_1= 'DoubleType' ) ;
    public final EObject ruleDoubleType() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;


        	enterRule();

        try {
            // InternalPogoDsl.g:5478:2: ( ( () otherlv_1= 'DoubleType' ) )
            // InternalPogoDsl.g:5479:2: ( () otherlv_1= 'DoubleType' )
            {
            // InternalPogoDsl.g:5479:2: ( () otherlv_1= 'DoubleType' )
            // InternalPogoDsl.g:5480:3: () otherlv_1= 'DoubleType'
            {
            // InternalPogoDsl.g:5480:3: ()
            // InternalPogoDsl.g:5481:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getDoubleTypeAccess().getDoubleTypeAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,131,FOLLOW_2); 

            			newLeafNode(otherlv_1, grammarAccess.getDoubleTypeAccess().getDoubleTypeKeyword_1());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDoubleType"


    // $ANTLR start "entryRuleStringType"
    // InternalPogoDsl.g:5495:1: entryRuleStringType returns [EObject current=null] : iv_ruleStringType= ruleStringType EOF ;
    public final EObject entryRuleStringType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleStringType = null;


        try {
            // InternalPogoDsl.g:5495:51: (iv_ruleStringType= ruleStringType EOF )
            // InternalPogoDsl.g:5496:2: iv_ruleStringType= ruleStringType EOF
            {
             newCompositeNode(grammarAccess.getStringTypeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleStringType=ruleStringType();

            state._fsp--;

             current =iv_ruleStringType; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleStringType"


    // $ANTLR start "ruleStringType"
    // InternalPogoDsl.g:5502:1: ruleStringType returns [EObject current=null] : ( () otherlv_1= 'StringType' ) ;
    public final EObject ruleStringType() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;


        	enterRule();

        try {
            // InternalPogoDsl.g:5508:2: ( ( () otherlv_1= 'StringType' ) )
            // InternalPogoDsl.g:5509:2: ( () otherlv_1= 'StringType' )
            {
            // InternalPogoDsl.g:5509:2: ( () otherlv_1= 'StringType' )
            // InternalPogoDsl.g:5510:3: () otherlv_1= 'StringType'
            {
            // InternalPogoDsl.g:5510:3: ()
            // InternalPogoDsl.g:5511:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getStringTypeAccess().getStringTypeAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,132,FOLLOW_2); 

            			newLeafNode(otherlv_1, grammarAccess.getStringTypeAccess().getStringTypeKeyword_1());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleStringType"


    // $ANTLR start "entryRuleShortVectorType"
    // InternalPogoDsl.g:5525:1: entryRuleShortVectorType returns [EObject current=null] : iv_ruleShortVectorType= ruleShortVectorType EOF ;
    public final EObject entryRuleShortVectorType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleShortVectorType = null;


        try {
            // InternalPogoDsl.g:5525:56: (iv_ruleShortVectorType= ruleShortVectorType EOF )
            // InternalPogoDsl.g:5526:2: iv_ruleShortVectorType= ruleShortVectorType EOF
            {
             newCompositeNode(grammarAccess.getShortVectorTypeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleShortVectorType=ruleShortVectorType();

            state._fsp--;

             current =iv_ruleShortVectorType; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleShortVectorType"


    // $ANTLR start "ruleShortVectorType"
    // InternalPogoDsl.g:5532:1: ruleShortVectorType returns [EObject current=null] : ( () otherlv_1= 'ShortVectorType' ) ;
    public final EObject ruleShortVectorType() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;


        	enterRule();

        try {
            // InternalPogoDsl.g:5538:2: ( ( () otherlv_1= 'ShortVectorType' ) )
            // InternalPogoDsl.g:5539:2: ( () otherlv_1= 'ShortVectorType' )
            {
            // InternalPogoDsl.g:5539:2: ( () otherlv_1= 'ShortVectorType' )
            // InternalPogoDsl.g:5540:3: () otherlv_1= 'ShortVectorType'
            {
            // InternalPogoDsl.g:5540:3: ()
            // InternalPogoDsl.g:5541:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getShortVectorTypeAccess().getShortVectorTypeAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,133,FOLLOW_2); 

            			newLeafNode(otherlv_1, grammarAccess.getShortVectorTypeAccess().getShortVectorTypeKeyword_1());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleShortVectorType"


    // $ANTLR start "entryRuleIntVectorType"
    // InternalPogoDsl.g:5555:1: entryRuleIntVectorType returns [EObject current=null] : iv_ruleIntVectorType= ruleIntVectorType EOF ;
    public final EObject entryRuleIntVectorType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleIntVectorType = null;


        try {
            // InternalPogoDsl.g:5555:54: (iv_ruleIntVectorType= ruleIntVectorType EOF )
            // InternalPogoDsl.g:5556:2: iv_ruleIntVectorType= ruleIntVectorType EOF
            {
             newCompositeNode(grammarAccess.getIntVectorTypeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleIntVectorType=ruleIntVectorType();

            state._fsp--;

             current =iv_ruleIntVectorType; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleIntVectorType"


    // $ANTLR start "ruleIntVectorType"
    // InternalPogoDsl.g:5562:1: ruleIntVectorType returns [EObject current=null] : ( () otherlv_1= 'IntVectorType' ) ;
    public final EObject ruleIntVectorType() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;


        	enterRule();

        try {
            // InternalPogoDsl.g:5568:2: ( ( () otherlv_1= 'IntVectorType' ) )
            // InternalPogoDsl.g:5569:2: ( () otherlv_1= 'IntVectorType' )
            {
            // InternalPogoDsl.g:5569:2: ( () otherlv_1= 'IntVectorType' )
            // InternalPogoDsl.g:5570:3: () otherlv_1= 'IntVectorType'
            {
            // InternalPogoDsl.g:5570:3: ()
            // InternalPogoDsl.g:5571:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getIntVectorTypeAccess().getIntVectorTypeAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,134,FOLLOW_2); 

            			newLeafNode(otherlv_1, grammarAccess.getIntVectorTypeAccess().getIntVectorTypeKeyword_1());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleIntVectorType"


    // $ANTLR start "entryRuleFloatVectorType"
    // InternalPogoDsl.g:5585:1: entryRuleFloatVectorType returns [EObject current=null] : iv_ruleFloatVectorType= ruleFloatVectorType EOF ;
    public final EObject entryRuleFloatVectorType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFloatVectorType = null;


        try {
            // InternalPogoDsl.g:5585:56: (iv_ruleFloatVectorType= ruleFloatVectorType EOF )
            // InternalPogoDsl.g:5586:2: iv_ruleFloatVectorType= ruleFloatVectorType EOF
            {
             newCompositeNode(grammarAccess.getFloatVectorTypeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleFloatVectorType=ruleFloatVectorType();

            state._fsp--;

             current =iv_ruleFloatVectorType; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFloatVectorType"


    // $ANTLR start "ruleFloatVectorType"
    // InternalPogoDsl.g:5592:1: ruleFloatVectorType returns [EObject current=null] : ( () otherlv_1= 'FloatVectorType' ) ;
    public final EObject ruleFloatVectorType() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;


        	enterRule();

        try {
            // InternalPogoDsl.g:5598:2: ( ( () otherlv_1= 'FloatVectorType' ) )
            // InternalPogoDsl.g:5599:2: ( () otherlv_1= 'FloatVectorType' )
            {
            // InternalPogoDsl.g:5599:2: ( () otherlv_1= 'FloatVectorType' )
            // InternalPogoDsl.g:5600:3: () otherlv_1= 'FloatVectorType'
            {
            // InternalPogoDsl.g:5600:3: ()
            // InternalPogoDsl.g:5601:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getFloatVectorTypeAccess().getFloatVectorTypeAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,135,FOLLOW_2); 

            			newLeafNode(otherlv_1, grammarAccess.getFloatVectorTypeAccess().getFloatVectorTypeKeyword_1());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFloatVectorType"


    // $ANTLR start "entryRuleDoubleVectorType"
    // InternalPogoDsl.g:5615:1: entryRuleDoubleVectorType returns [EObject current=null] : iv_ruleDoubleVectorType= ruleDoubleVectorType EOF ;
    public final EObject entryRuleDoubleVectorType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDoubleVectorType = null;


        try {
            // InternalPogoDsl.g:5615:57: (iv_ruleDoubleVectorType= ruleDoubleVectorType EOF )
            // InternalPogoDsl.g:5616:2: iv_ruleDoubleVectorType= ruleDoubleVectorType EOF
            {
             newCompositeNode(grammarAccess.getDoubleVectorTypeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleDoubleVectorType=ruleDoubleVectorType();

            state._fsp--;

             current =iv_ruleDoubleVectorType; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDoubleVectorType"


    // $ANTLR start "ruleDoubleVectorType"
    // InternalPogoDsl.g:5622:1: ruleDoubleVectorType returns [EObject current=null] : ( () otherlv_1= 'DoubleVectorType' ) ;
    public final EObject ruleDoubleVectorType() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;


        	enterRule();

        try {
            // InternalPogoDsl.g:5628:2: ( ( () otherlv_1= 'DoubleVectorType' ) )
            // InternalPogoDsl.g:5629:2: ( () otherlv_1= 'DoubleVectorType' )
            {
            // InternalPogoDsl.g:5629:2: ( () otherlv_1= 'DoubleVectorType' )
            // InternalPogoDsl.g:5630:3: () otherlv_1= 'DoubleVectorType'
            {
            // InternalPogoDsl.g:5630:3: ()
            // InternalPogoDsl.g:5631:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getDoubleVectorTypeAccess().getDoubleVectorTypeAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,136,FOLLOW_2); 

            			newLeafNode(otherlv_1, grammarAccess.getDoubleVectorTypeAccess().getDoubleVectorTypeKeyword_1());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDoubleVectorType"


    // $ANTLR start "entryRuleStringVectorType"
    // InternalPogoDsl.g:5645:1: entryRuleStringVectorType returns [EObject current=null] : iv_ruleStringVectorType= ruleStringVectorType EOF ;
    public final EObject entryRuleStringVectorType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleStringVectorType = null;


        try {
            // InternalPogoDsl.g:5645:57: (iv_ruleStringVectorType= ruleStringVectorType EOF )
            // InternalPogoDsl.g:5646:2: iv_ruleStringVectorType= ruleStringVectorType EOF
            {
             newCompositeNode(grammarAccess.getStringVectorTypeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleStringVectorType=ruleStringVectorType();

            state._fsp--;

             current =iv_ruleStringVectorType; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleStringVectorType"


    // $ANTLR start "ruleStringVectorType"
    // InternalPogoDsl.g:5652:1: ruleStringVectorType returns [EObject current=null] : ( () otherlv_1= 'StringVectorType' ) ;
    public final EObject ruleStringVectorType() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;


        	enterRule();

        try {
            // InternalPogoDsl.g:5658:2: ( ( () otherlv_1= 'StringVectorType' ) )
            // InternalPogoDsl.g:5659:2: ( () otherlv_1= 'StringVectorType' )
            {
            // InternalPogoDsl.g:5659:2: ( () otherlv_1= 'StringVectorType' )
            // InternalPogoDsl.g:5660:3: () otherlv_1= 'StringVectorType'
            {
            // InternalPogoDsl.g:5660:3: ()
            // InternalPogoDsl.g:5661:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getStringVectorTypeAccess().getStringVectorTypeAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,137,FOLLOW_2); 

            			newLeafNode(otherlv_1, grammarAccess.getStringVectorTypeAccess().getStringVectorTypeKeyword_1());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleStringVectorType"


    // $ANTLR start "entryRuleArgument"
    // InternalPogoDsl.g:5675:1: entryRuleArgument returns [EObject current=null] : iv_ruleArgument= ruleArgument EOF ;
    public final EObject entryRuleArgument() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleArgument = null;


        try {
            // InternalPogoDsl.g:5675:49: (iv_ruleArgument= ruleArgument EOF )
            // InternalPogoDsl.g:5676:2: iv_ruleArgument= ruleArgument EOF
            {
             newCompositeNode(grammarAccess.getArgumentRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleArgument=ruleArgument();

            state._fsp--;

             current =iv_ruleArgument; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleArgument"


    // $ANTLR start "ruleArgument"
    // InternalPogoDsl.g:5682:1: ruleArgument returns [EObject current=null] : ( () otherlv_1= 'Argument' otherlv_2= '{' (otherlv_3= 'description' ( (lv_description_4_0= ruleEString ) ) )? (otherlv_5= 'type' ( (lv_type_6_0= ruleType ) ) )? otherlv_7= '}' ) ;
    public final EObject ruleArgument() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        AntlrDatatypeRuleToken lv_description_4_0 = null;

        EObject lv_type_6_0 = null;



        	enterRule();

        try {
            // InternalPogoDsl.g:5688:2: ( ( () otherlv_1= 'Argument' otherlv_2= '{' (otherlv_3= 'description' ( (lv_description_4_0= ruleEString ) ) )? (otherlv_5= 'type' ( (lv_type_6_0= ruleType ) ) )? otherlv_7= '}' ) )
            // InternalPogoDsl.g:5689:2: ( () otherlv_1= 'Argument' otherlv_2= '{' (otherlv_3= 'description' ( (lv_description_4_0= ruleEString ) ) )? (otherlv_5= 'type' ( (lv_type_6_0= ruleType ) ) )? otherlv_7= '}' )
            {
            // InternalPogoDsl.g:5689:2: ( () otherlv_1= 'Argument' otherlv_2= '{' (otherlv_3= 'description' ( (lv_description_4_0= ruleEString ) ) )? (otherlv_5= 'type' ( (lv_type_6_0= ruleType ) ) )? otherlv_7= '}' )
            // InternalPogoDsl.g:5690:3: () otherlv_1= 'Argument' otherlv_2= '{' (otherlv_3= 'description' ( (lv_description_4_0= ruleEString ) ) )? (otherlv_5= 'type' ( (lv_type_6_0= ruleType ) ) )? otherlv_7= '}'
            {
            // InternalPogoDsl.g:5690:3: ()
            // InternalPogoDsl.g:5691:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getArgumentAccess().getArgumentAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,138,FOLLOW_3); 

            			newLeafNode(otherlv_1, grammarAccess.getArgumentAccess().getArgumentKeyword_1());
            		
            otherlv_2=(Token)match(input,12,FOLLOW_151); 

            			newLeafNode(otherlv_2, grammarAccess.getArgumentAccess().getLeftCurlyBracketKeyword_2());
            		
            // InternalPogoDsl.g:5705:3: (otherlv_3= 'description' ( (lv_description_4_0= ruleEString ) ) )?
            int alt144=2;
            int LA144_0 = input.LA(1);

            if ( (LA144_0==27) ) {
                alt144=1;
            }
            switch (alt144) {
                case 1 :
                    // InternalPogoDsl.g:5706:4: otherlv_3= 'description' ( (lv_description_4_0= ruleEString ) )
                    {
                    otherlv_3=(Token)match(input,27,FOLLOW_13); 

                    				newLeafNode(otherlv_3, grammarAccess.getArgumentAccess().getDescriptionKeyword_3_0());
                    			
                    // InternalPogoDsl.g:5710:4: ( (lv_description_4_0= ruleEString ) )
                    // InternalPogoDsl.g:5711:5: (lv_description_4_0= ruleEString )
                    {
                    // InternalPogoDsl.g:5711:5: (lv_description_4_0= ruleEString )
                    // InternalPogoDsl.g:5712:6: lv_description_4_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getArgumentAccess().getDescriptionEStringParserRuleCall_3_1_0());
                    					
                    pushFollow(FOLLOW_152);
                    lv_description_4_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getArgumentRule());
                    						}
                    						set(
                    							current,
                    							"description",
                    							lv_description_4_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalPogoDsl.g:5730:3: (otherlv_5= 'type' ( (lv_type_6_0= ruleType ) ) )?
            int alt145=2;
            int LA145_0 = input.LA(1);

            if ( (LA145_0==59) ) {
                alt145=1;
            }
            switch (alt145) {
                case 1 :
                    // InternalPogoDsl.g:5731:4: otherlv_5= 'type' ( (lv_type_6_0= ruleType ) )
                    {
                    otherlv_5=(Token)match(input,59,FOLLOW_107); 

                    				newLeafNode(otherlv_5, grammarAccess.getArgumentAccess().getTypeKeyword_4_0());
                    			
                    // InternalPogoDsl.g:5735:4: ( (lv_type_6_0= ruleType ) )
                    // InternalPogoDsl.g:5736:5: (lv_type_6_0= ruleType )
                    {
                    // InternalPogoDsl.g:5736:5: (lv_type_6_0= ruleType )
                    // InternalPogoDsl.g:5737:6: lv_type_6_0= ruleType
                    {

                    						newCompositeNode(grammarAccess.getArgumentAccess().getTypeTypeParserRuleCall_4_1_0());
                    					
                    pushFollow(FOLLOW_11);
                    lv_type_6_0=ruleType();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getArgumentRule());
                    						}
                    						set(
                    							current,
                    							"type",
                    							lv_type_6_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.Type");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            otherlv_7=(Token)match(input,15,FOLLOW_2); 

            			newLeafNode(otherlv_7, grammarAccess.getArgumentAccess().getRightCurlyBracketKeyword_5());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleArgument"


    // $ANTLR start "entryRuleType_Impl"
    // InternalPogoDsl.g:5763:1: entryRuleType_Impl returns [EObject current=null] : iv_ruleType_Impl= ruleType_Impl EOF ;
    public final EObject entryRuleType_Impl() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleType_Impl = null;


        try {
            // InternalPogoDsl.g:5763:50: (iv_ruleType_Impl= ruleType_Impl EOF )
            // InternalPogoDsl.g:5764:2: iv_ruleType_Impl= ruleType_Impl EOF
            {
             newCompositeNode(grammarAccess.getType_ImplRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleType_Impl=ruleType_Impl();

            state._fsp--;

             current =iv_ruleType_Impl; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleType_Impl"


    // $ANTLR start "ruleType_Impl"
    // InternalPogoDsl.g:5770:1: ruleType_Impl returns [EObject current=null] : ( () otherlv_1= 'Type' ) ;
    public final EObject ruleType_Impl() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;


        	enterRule();

        try {
            // InternalPogoDsl.g:5776:2: ( ( () otherlv_1= 'Type' ) )
            // InternalPogoDsl.g:5777:2: ( () otherlv_1= 'Type' )
            {
            // InternalPogoDsl.g:5777:2: ( () otherlv_1= 'Type' )
            // InternalPogoDsl.g:5778:3: () otherlv_1= 'Type'
            {
            // InternalPogoDsl.g:5778:3: ()
            // InternalPogoDsl.g:5779:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getType_ImplAccess().getTypeAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,139,FOLLOW_2); 

            			newLeafNode(otherlv_1, grammarAccess.getType_ImplAccess().getTypeKeyword_1());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleType_Impl"


    // $ANTLR start "entryRuleVoidType"
    // InternalPogoDsl.g:5793:1: entryRuleVoidType returns [EObject current=null] : iv_ruleVoidType= ruleVoidType EOF ;
    public final EObject entryRuleVoidType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleVoidType = null;


        try {
            // InternalPogoDsl.g:5793:49: (iv_ruleVoidType= ruleVoidType EOF )
            // InternalPogoDsl.g:5794:2: iv_ruleVoidType= ruleVoidType EOF
            {
             newCompositeNode(grammarAccess.getVoidTypeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleVoidType=ruleVoidType();

            state._fsp--;

             current =iv_ruleVoidType; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleVoidType"


    // $ANTLR start "ruleVoidType"
    // InternalPogoDsl.g:5800:1: ruleVoidType returns [EObject current=null] : ( () otherlv_1= 'VoidType' ) ;
    public final EObject ruleVoidType() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;


        	enterRule();

        try {
            // InternalPogoDsl.g:5806:2: ( ( () otherlv_1= 'VoidType' ) )
            // InternalPogoDsl.g:5807:2: ( () otherlv_1= 'VoidType' )
            {
            // InternalPogoDsl.g:5807:2: ( () otherlv_1= 'VoidType' )
            // InternalPogoDsl.g:5808:3: () otherlv_1= 'VoidType'
            {
            // InternalPogoDsl.g:5808:3: ()
            // InternalPogoDsl.g:5809:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getVoidTypeAccess().getVoidTypeAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,140,FOLLOW_2); 

            			newLeafNode(otherlv_1, grammarAccess.getVoidTypeAccess().getVoidTypeKeyword_1());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleVoidType"


    // $ANTLR start "entryRuleCharArrayType"
    // InternalPogoDsl.g:5823:1: entryRuleCharArrayType returns [EObject current=null] : iv_ruleCharArrayType= ruleCharArrayType EOF ;
    public final EObject entryRuleCharArrayType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCharArrayType = null;


        try {
            // InternalPogoDsl.g:5823:54: (iv_ruleCharArrayType= ruleCharArrayType EOF )
            // InternalPogoDsl.g:5824:2: iv_ruleCharArrayType= ruleCharArrayType EOF
            {
             newCompositeNode(grammarAccess.getCharArrayTypeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleCharArrayType=ruleCharArrayType();

            state._fsp--;

             current =iv_ruleCharArrayType; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCharArrayType"


    // $ANTLR start "ruleCharArrayType"
    // InternalPogoDsl.g:5830:1: ruleCharArrayType returns [EObject current=null] : ( () otherlv_1= 'CharArrayType' ) ;
    public final EObject ruleCharArrayType() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;


        	enterRule();

        try {
            // InternalPogoDsl.g:5836:2: ( ( () otherlv_1= 'CharArrayType' ) )
            // InternalPogoDsl.g:5837:2: ( () otherlv_1= 'CharArrayType' )
            {
            // InternalPogoDsl.g:5837:2: ( () otherlv_1= 'CharArrayType' )
            // InternalPogoDsl.g:5838:3: () otherlv_1= 'CharArrayType'
            {
            // InternalPogoDsl.g:5838:3: ()
            // InternalPogoDsl.g:5839:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getCharArrayTypeAccess().getCharArrayTypeAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,141,FOLLOW_2); 

            			newLeafNode(otherlv_1, grammarAccess.getCharArrayTypeAccess().getCharArrayTypeKeyword_1());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCharArrayType"


    // $ANTLR start "entryRuleShortArrayType"
    // InternalPogoDsl.g:5853:1: entryRuleShortArrayType returns [EObject current=null] : iv_ruleShortArrayType= ruleShortArrayType EOF ;
    public final EObject entryRuleShortArrayType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleShortArrayType = null;


        try {
            // InternalPogoDsl.g:5853:55: (iv_ruleShortArrayType= ruleShortArrayType EOF )
            // InternalPogoDsl.g:5854:2: iv_ruleShortArrayType= ruleShortArrayType EOF
            {
             newCompositeNode(grammarAccess.getShortArrayTypeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleShortArrayType=ruleShortArrayType();

            state._fsp--;

             current =iv_ruleShortArrayType; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleShortArrayType"


    // $ANTLR start "ruleShortArrayType"
    // InternalPogoDsl.g:5860:1: ruleShortArrayType returns [EObject current=null] : ( () otherlv_1= 'ShortArrayType' ) ;
    public final EObject ruleShortArrayType() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;


        	enterRule();

        try {
            // InternalPogoDsl.g:5866:2: ( ( () otherlv_1= 'ShortArrayType' ) )
            // InternalPogoDsl.g:5867:2: ( () otherlv_1= 'ShortArrayType' )
            {
            // InternalPogoDsl.g:5867:2: ( () otherlv_1= 'ShortArrayType' )
            // InternalPogoDsl.g:5868:3: () otherlv_1= 'ShortArrayType'
            {
            // InternalPogoDsl.g:5868:3: ()
            // InternalPogoDsl.g:5869:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getShortArrayTypeAccess().getShortArrayTypeAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,142,FOLLOW_2); 

            			newLeafNode(otherlv_1, grammarAccess.getShortArrayTypeAccess().getShortArrayTypeKeyword_1());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleShortArrayType"


    // $ANTLR start "entryRuleUShortArrayType"
    // InternalPogoDsl.g:5883:1: entryRuleUShortArrayType returns [EObject current=null] : iv_ruleUShortArrayType= ruleUShortArrayType EOF ;
    public final EObject entryRuleUShortArrayType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleUShortArrayType = null;


        try {
            // InternalPogoDsl.g:5883:56: (iv_ruleUShortArrayType= ruleUShortArrayType EOF )
            // InternalPogoDsl.g:5884:2: iv_ruleUShortArrayType= ruleUShortArrayType EOF
            {
             newCompositeNode(grammarAccess.getUShortArrayTypeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleUShortArrayType=ruleUShortArrayType();

            state._fsp--;

             current =iv_ruleUShortArrayType; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleUShortArrayType"


    // $ANTLR start "ruleUShortArrayType"
    // InternalPogoDsl.g:5890:1: ruleUShortArrayType returns [EObject current=null] : ( () otherlv_1= 'UShortArrayType' ) ;
    public final EObject ruleUShortArrayType() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;


        	enterRule();

        try {
            // InternalPogoDsl.g:5896:2: ( ( () otherlv_1= 'UShortArrayType' ) )
            // InternalPogoDsl.g:5897:2: ( () otherlv_1= 'UShortArrayType' )
            {
            // InternalPogoDsl.g:5897:2: ( () otherlv_1= 'UShortArrayType' )
            // InternalPogoDsl.g:5898:3: () otherlv_1= 'UShortArrayType'
            {
            // InternalPogoDsl.g:5898:3: ()
            // InternalPogoDsl.g:5899:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getUShortArrayTypeAccess().getUShortArrayTypeAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,143,FOLLOW_2); 

            			newLeafNode(otherlv_1, grammarAccess.getUShortArrayTypeAccess().getUShortArrayTypeKeyword_1());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleUShortArrayType"


    // $ANTLR start "entryRuleIntArrayType"
    // InternalPogoDsl.g:5913:1: entryRuleIntArrayType returns [EObject current=null] : iv_ruleIntArrayType= ruleIntArrayType EOF ;
    public final EObject entryRuleIntArrayType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleIntArrayType = null;


        try {
            // InternalPogoDsl.g:5913:53: (iv_ruleIntArrayType= ruleIntArrayType EOF )
            // InternalPogoDsl.g:5914:2: iv_ruleIntArrayType= ruleIntArrayType EOF
            {
             newCompositeNode(grammarAccess.getIntArrayTypeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleIntArrayType=ruleIntArrayType();

            state._fsp--;

             current =iv_ruleIntArrayType; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleIntArrayType"


    // $ANTLR start "ruleIntArrayType"
    // InternalPogoDsl.g:5920:1: ruleIntArrayType returns [EObject current=null] : ( () otherlv_1= 'IntArrayType' ) ;
    public final EObject ruleIntArrayType() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;


        	enterRule();

        try {
            // InternalPogoDsl.g:5926:2: ( ( () otherlv_1= 'IntArrayType' ) )
            // InternalPogoDsl.g:5927:2: ( () otherlv_1= 'IntArrayType' )
            {
            // InternalPogoDsl.g:5927:2: ( () otherlv_1= 'IntArrayType' )
            // InternalPogoDsl.g:5928:3: () otherlv_1= 'IntArrayType'
            {
            // InternalPogoDsl.g:5928:3: ()
            // InternalPogoDsl.g:5929:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getIntArrayTypeAccess().getIntArrayTypeAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,144,FOLLOW_2); 

            			newLeafNode(otherlv_1, grammarAccess.getIntArrayTypeAccess().getIntArrayTypeKeyword_1());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleIntArrayType"


    // $ANTLR start "entryRuleUIntArrayType"
    // InternalPogoDsl.g:5943:1: entryRuleUIntArrayType returns [EObject current=null] : iv_ruleUIntArrayType= ruleUIntArrayType EOF ;
    public final EObject entryRuleUIntArrayType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleUIntArrayType = null;


        try {
            // InternalPogoDsl.g:5943:54: (iv_ruleUIntArrayType= ruleUIntArrayType EOF )
            // InternalPogoDsl.g:5944:2: iv_ruleUIntArrayType= ruleUIntArrayType EOF
            {
             newCompositeNode(grammarAccess.getUIntArrayTypeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleUIntArrayType=ruleUIntArrayType();

            state._fsp--;

             current =iv_ruleUIntArrayType; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleUIntArrayType"


    // $ANTLR start "ruleUIntArrayType"
    // InternalPogoDsl.g:5950:1: ruleUIntArrayType returns [EObject current=null] : ( () otherlv_1= 'UIntArrayType' ) ;
    public final EObject ruleUIntArrayType() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;


        	enterRule();

        try {
            // InternalPogoDsl.g:5956:2: ( ( () otherlv_1= 'UIntArrayType' ) )
            // InternalPogoDsl.g:5957:2: ( () otherlv_1= 'UIntArrayType' )
            {
            // InternalPogoDsl.g:5957:2: ( () otherlv_1= 'UIntArrayType' )
            // InternalPogoDsl.g:5958:3: () otherlv_1= 'UIntArrayType'
            {
            // InternalPogoDsl.g:5958:3: ()
            // InternalPogoDsl.g:5959:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getUIntArrayTypeAccess().getUIntArrayTypeAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,145,FOLLOW_2); 

            			newLeafNode(otherlv_1, grammarAccess.getUIntArrayTypeAccess().getUIntArrayTypeKeyword_1());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleUIntArrayType"


    // $ANTLR start "entryRuleFloatArrayType"
    // InternalPogoDsl.g:5973:1: entryRuleFloatArrayType returns [EObject current=null] : iv_ruleFloatArrayType= ruleFloatArrayType EOF ;
    public final EObject entryRuleFloatArrayType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFloatArrayType = null;


        try {
            // InternalPogoDsl.g:5973:55: (iv_ruleFloatArrayType= ruleFloatArrayType EOF )
            // InternalPogoDsl.g:5974:2: iv_ruleFloatArrayType= ruleFloatArrayType EOF
            {
             newCompositeNode(grammarAccess.getFloatArrayTypeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleFloatArrayType=ruleFloatArrayType();

            state._fsp--;

             current =iv_ruleFloatArrayType; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFloatArrayType"


    // $ANTLR start "ruleFloatArrayType"
    // InternalPogoDsl.g:5980:1: ruleFloatArrayType returns [EObject current=null] : ( () otherlv_1= 'FloatArrayType' ) ;
    public final EObject ruleFloatArrayType() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;


        	enterRule();

        try {
            // InternalPogoDsl.g:5986:2: ( ( () otherlv_1= 'FloatArrayType' ) )
            // InternalPogoDsl.g:5987:2: ( () otherlv_1= 'FloatArrayType' )
            {
            // InternalPogoDsl.g:5987:2: ( () otherlv_1= 'FloatArrayType' )
            // InternalPogoDsl.g:5988:3: () otherlv_1= 'FloatArrayType'
            {
            // InternalPogoDsl.g:5988:3: ()
            // InternalPogoDsl.g:5989:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getFloatArrayTypeAccess().getFloatArrayTypeAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,146,FOLLOW_2); 

            			newLeafNode(otherlv_1, grammarAccess.getFloatArrayTypeAccess().getFloatArrayTypeKeyword_1());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFloatArrayType"


    // $ANTLR start "entryRuleDoubleArrayType"
    // InternalPogoDsl.g:6003:1: entryRuleDoubleArrayType returns [EObject current=null] : iv_ruleDoubleArrayType= ruleDoubleArrayType EOF ;
    public final EObject entryRuleDoubleArrayType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDoubleArrayType = null;


        try {
            // InternalPogoDsl.g:6003:56: (iv_ruleDoubleArrayType= ruleDoubleArrayType EOF )
            // InternalPogoDsl.g:6004:2: iv_ruleDoubleArrayType= ruleDoubleArrayType EOF
            {
             newCompositeNode(grammarAccess.getDoubleArrayTypeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleDoubleArrayType=ruleDoubleArrayType();

            state._fsp--;

             current =iv_ruleDoubleArrayType; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDoubleArrayType"


    // $ANTLR start "ruleDoubleArrayType"
    // InternalPogoDsl.g:6010:1: ruleDoubleArrayType returns [EObject current=null] : ( () otherlv_1= 'DoubleArrayType' ) ;
    public final EObject ruleDoubleArrayType() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;


        	enterRule();

        try {
            // InternalPogoDsl.g:6016:2: ( ( () otherlv_1= 'DoubleArrayType' ) )
            // InternalPogoDsl.g:6017:2: ( () otherlv_1= 'DoubleArrayType' )
            {
            // InternalPogoDsl.g:6017:2: ( () otherlv_1= 'DoubleArrayType' )
            // InternalPogoDsl.g:6018:3: () otherlv_1= 'DoubleArrayType'
            {
            // InternalPogoDsl.g:6018:3: ()
            // InternalPogoDsl.g:6019:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getDoubleArrayTypeAccess().getDoubleArrayTypeAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,147,FOLLOW_2); 

            			newLeafNode(otherlv_1, grammarAccess.getDoubleArrayTypeAccess().getDoubleArrayTypeKeyword_1());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDoubleArrayType"


    // $ANTLR start "entryRuleStringArrayType"
    // InternalPogoDsl.g:6033:1: entryRuleStringArrayType returns [EObject current=null] : iv_ruleStringArrayType= ruleStringArrayType EOF ;
    public final EObject entryRuleStringArrayType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleStringArrayType = null;


        try {
            // InternalPogoDsl.g:6033:56: (iv_ruleStringArrayType= ruleStringArrayType EOF )
            // InternalPogoDsl.g:6034:2: iv_ruleStringArrayType= ruleStringArrayType EOF
            {
             newCompositeNode(grammarAccess.getStringArrayTypeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleStringArrayType=ruleStringArrayType();

            state._fsp--;

             current =iv_ruleStringArrayType; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleStringArrayType"


    // $ANTLR start "ruleStringArrayType"
    // InternalPogoDsl.g:6040:1: ruleStringArrayType returns [EObject current=null] : ( () otherlv_1= 'StringArrayType' ) ;
    public final EObject ruleStringArrayType() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;


        	enterRule();

        try {
            // InternalPogoDsl.g:6046:2: ( ( () otherlv_1= 'StringArrayType' ) )
            // InternalPogoDsl.g:6047:2: ( () otherlv_1= 'StringArrayType' )
            {
            // InternalPogoDsl.g:6047:2: ( () otherlv_1= 'StringArrayType' )
            // InternalPogoDsl.g:6048:3: () otherlv_1= 'StringArrayType'
            {
            // InternalPogoDsl.g:6048:3: ()
            // InternalPogoDsl.g:6049:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getStringArrayTypeAccess().getStringArrayTypeAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,148,FOLLOW_2); 

            			newLeafNode(otherlv_1, grammarAccess.getStringArrayTypeAccess().getStringArrayTypeKeyword_1());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleStringArrayType"


    // $ANTLR start "entryRuleLongStringArrayType"
    // InternalPogoDsl.g:6063:1: entryRuleLongStringArrayType returns [EObject current=null] : iv_ruleLongStringArrayType= ruleLongStringArrayType EOF ;
    public final EObject entryRuleLongStringArrayType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleLongStringArrayType = null;


        try {
            // InternalPogoDsl.g:6063:60: (iv_ruleLongStringArrayType= ruleLongStringArrayType EOF )
            // InternalPogoDsl.g:6064:2: iv_ruleLongStringArrayType= ruleLongStringArrayType EOF
            {
             newCompositeNode(grammarAccess.getLongStringArrayTypeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleLongStringArrayType=ruleLongStringArrayType();

            state._fsp--;

             current =iv_ruleLongStringArrayType; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleLongStringArrayType"


    // $ANTLR start "ruleLongStringArrayType"
    // InternalPogoDsl.g:6070:1: ruleLongStringArrayType returns [EObject current=null] : ( () otherlv_1= 'LongStringArrayType' ) ;
    public final EObject ruleLongStringArrayType() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;


        	enterRule();

        try {
            // InternalPogoDsl.g:6076:2: ( ( () otherlv_1= 'LongStringArrayType' ) )
            // InternalPogoDsl.g:6077:2: ( () otherlv_1= 'LongStringArrayType' )
            {
            // InternalPogoDsl.g:6077:2: ( () otherlv_1= 'LongStringArrayType' )
            // InternalPogoDsl.g:6078:3: () otherlv_1= 'LongStringArrayType'
            {
            // InternalPogoDsl.g:6078:3: ()
            // InternalPogoDsl.g:6079:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getLongStringArrayTypeAccess().getLongStringArrayTypeAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,149,FOLLOW_2); 

            			newLeafNode(otherlv_1, grammarAccess.getLongStringArrayTypeAccess().getLongStringArrayTypeKeyword_1());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLongStringArrayType"


    // $ANTLR start "entryRuleDoubleStringArrayType"
    // InternalPogoDsl.g:6093:1: entryRuleDoubleStringArrayType returns [EObject current=null] : iv_ruleDoubleStringArrayType= ruleDoubleStringArrayType EOF ;
    public final EObject entryRuleDoubleStringArrayType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDoubleStringArrayType = null;


        try {
            // InternalPogoDsl.g:6093:62: (iv_ruleDoubleStringArrayType= ruleDoubleStringArrayType EOF )
            // InternalPogoDsl.g:6094:2: iv_ruleDoubleStringArrayType= ruleDoubleStringArrayType EOF
            {
             newCompositeNode(grammarAccess.getDoubleStringArrayTypeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleDoubleStringArrayType=ruleDoubleStringArrayType();

            state._fsp--;

             current =iv_ruleDoubleStringArrayType; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDoubleStringArrayType"


    // $ANTLR start "ruleDoubleStringArrayType"
    // InternalPogoDsl.g:6100:1: ruleDoubleStringArrayType returns [EObject current=null] : ( () otherlv_1= 'DoubleStringArrayType' ) ;
    public final EObject ruleDoubleStringArrayType() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;


        	enterRule();

        try {
            // InternalPogoDsl.g:6106:2: ( ( () otherlv_1= 'DoubleStringArrayType' ) )
            // InternalPogoDsl.g:6107:2: ( () otherlv_1= 'DoubleStringArrayType' )
            {
            // InternalPogoDsl.g:6107:2: ( () otherlv_1= 'DoubleStringArrayType' )
            // InternalPogoDsl.g:6108:3: () otherlv_1= 'DoubleStringArrayType'
            {
            // InternalPogoDsl.g:6108:3: ()
            // InternalPogoDsl.g:6109:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getDoubleStringArrayTypeAccess().getDoubleStringArrayTypeAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,150,FOLLOW_2); 

            			newLeafNode(otherlv_1, grammarAccess.getDoubleStringArrayTypeAccess().getDoubleStringArrayTypeKeyword_1());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDoubleStringArrayType"


    // $ANTLR start "entryRuleStateType"
    // InternalPogoDsl.g:6123:1: entryRuleStateType returns [EObject current=null] : iv_ruleStateType= ruleStateType EOF ;
    public final EObject entryRuleStateType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleStateType = null;


        try {
            // InternalPogoDsl.g:6123:50: (iv_ruleStateType= ruleStateType EOF )
            // InternalPogoDsl.g:6124:2: iv_ruleStateType= ruleStateType EOF
            {
             newCompositeNode(grammarAccess.getStateTypeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleStateType=ruleStateType();

            state._fsp--;

             current =iv_ruleStateType; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleStateType"


    // $ANTLR start "ruleStateType"
    // InternalPogoDsl.g:6130:1: ruleStateType returns [EObject current=null] : ( () otherlv_1= 'StateType' ) ;
    public final EObject ruleStateType() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;


        	enterRule();

        try {
            // InternalPogoDsl.g:6136:2: ( ( () otherlv_1= 'StateType' ) )
            // InternalPogoDsl.g:6137:2: ( () otherlv_1= 'StateType' )
            {
            // InternalPogoDsl.g:6137:2: ( () otherlv_1= 'StateType' )
            // InternalPogoDsl.g:6138:3: () otherlv_1= 'StateType'
            {
            // InternalPogoDsl.g:6138:3: ()
            // InternalPogoDsl.g:6139:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getStateTypeAccess().getStateTypeAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,151,FOLLOW_2); 

            			newLeafNode(otherlv_1, grammarAccess.getStateTypeAccess().getStateTypeKeyword_1());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleStateType"


    // $ANTLR start "entryRuleConstStringType"
    // InternalPogoDsl.g:6153:1: entryRuleConstStringType returns [EObject current=null] : iv_ruleConstStringType= ruleConstStringType EOF ;
    public final EObject entryRuleConstStringType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleConstStringType = null;


        try {
            // InternalPogoDsl.g:6153:56: (iv_ruleConstStringType= ruleConstStringType EOF )
            // InternalPogoDsl.g:6154:2: iv_ruleConstStringType= ruleConstStringType EOF
            {
             newCompositeNode(grammarAccess.getConstStringTypeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleConstStringType=ruleConstStringType();

            state._fsp--;

             current =iv_ruleConstStringType; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleConstStringType"


    // $ANTLR start "ruleConstStringType"
    // InternalPogoDsl.g:6160:1: ruleConstStringType returns [EObject current=null] : ( () otherlv_1= 'ConstStringType' ) ;
    public final EObject ruleConstStringType() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;


        	enterRule();

        try {
            // InternalPogoDsl.g:6166:2: ( ( () otherlv_1= 'ConstStringType' ) )
            // InternalPogoDsl.g:6167:2: ( () otherlv_1= 'ConstStringType' )
            {
            // InternalPogoDsl.g:6167:2: ( () otherlv_1= 'ConstStringType' )
            // InternalPogoDsl.g:6168:3: () otherlv_1= 'ConstStringType'
            {
            // InternalPogoDsl.g:6168:3: ()
            // InternalPogoDsl.g:6169:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getConstStringTypeAccess().getConstStringTypeAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,152,FOLLOW_2); 

            			newLeafNode(otherlv_1, grammarAccess.getConstStringTypeAccess().getConstStringTypeKeyword_1());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleConstStringType"


    // $ANTLR start "entryRuleBooleanArrayType"
    // InternalPogoDsl.g:6183:1: entryRuleBooleanArrayType returns [EObject current=null] : iv_ruleBooleanArrayType= ruleBooleanArrayType EOF ;
    public final EObject entryRuleBooleanArrayType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBooleanArrayType = null;


        try {
            // InternalPogoDsl.g:6183:57: (iv_ruleBooleanArrayType= ruleBooleanArrayType EOF )
            // InternalPogoDsl.g:6184:2: iv_ruleBooleanArrayType= ruleBooleanArrayType EOF
            {
             newCompositeNode(grammarAccess.getBooleanArrayTypeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleBooleanArrayType=ruleBooleanArrayType();

            state._fsp--;

             current =iv_ruleBooleanArrayType; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBooleanArrayType"


    // $ANTLR start "ruleBooleanArrayType"
    // InternalPogoDsl.g:6190:1: ruleBooleanArrayType returns [EObject current=null] : ( () otherlv_1= 'BooleanArrayType' ) ;
    public final EObject ruleBooleanArrayType() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;


        	enterRule();

        try {
            // InternalPogoDsl.g:6196:2: ( ( () otherlv_1= 'BooleanArrayType' ) )
            // InternalPogoDsl.g:6197:2: ( () otherlv_1= 'BooleanArrayType' )
            {
            // InternalPogoDsl.g:6197:2: ( () otherlv_1= 'BooleanArrayType' )
            // InternalPogoDsl.g:6198:3: () otherlv_1= 'BooleanArrayType'
            {
            // InternalPogoDsl.g:6198:3: ()
            // InternalPogoDsl.g:6199:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getBooleanArrayTypeAccess().getBooleanArrayTypeAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,153,FOLLOW_2); 

            			newLeafNode(otherlv_1, grammarAccess.getBooleanArrayTypeAccess().getBooleanArrayTypeKeyword_1());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBooleanArrayType"


    // $ANTLR start "entryRuleUCharType"
    // InternalPogoDsl.g:6213:1: entryRuleUCharType returns [EObject current=null] : iv_ruleUCharType= ruleUCharType EOF ;
    public final EObject entryRuleUCharType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleUCharType = null;


        try {
            // InternalPogoDsl.g:6213:50: (iv_ruleUCharType= ruleUCharType EOF )
            // InternalPogoDsl.g:6214:2: iv_ruleUCharType= ruleUCharType EOF
            {
             newCompositeNode(grammarAccess.getUCharTypeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleUCharType=ruleUCharType();

            state._fsp--;

             current =iv_ruleUCharType; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleUCharType"


    // $ANTLR start "ruleUCharType"
    // InternalPogoDsl.g:6220:1: ruleUCharType returns [EObject current=null] : ( () otherlv_1= 'UCharType' ) ;
    public final EObject ruleUCharType() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;


        	enterRule();

        try {
            // InternalPogoDsl.g:6226:2: ( ( () otherlv_1= 'UCharType' ) )
            // InternalPogoDsl.g:6227:2: ( () otherlv_1= 'UCharType' )
            {
            // InternalPogoDsl.g:6227:2: ( () otherlv_1= 'UCharType' )
            // InternalPogoDsl.g:6228:3: () otherlv_1= 'UCharType'
            {
            // InternalPogoDsl.g:6228:3: ()
            // InternalPogoDsl.g:6229:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getUCharTypeAccess().getUCharTypeAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,154,FOLLOW_2); 

            			newLeafNode(otherlv_1, grammarAccess.getUCharTypeAccess().getUCharTypeKeyword_1());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleUCharType"


    // $ANTLR start "entryRuleLongType"
    // InternalPogoDsl.g:6243:1: entryRuleLongType returns [EObject current=null] : iv_ruleLongType= ruleLongType EOF ;
    public final EObject entryRuleLongType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleLongType = null;


        try {
            // InternalPogoDsl.g:6243:49: (iv_ruleLongType= ruleLongType EOF )
            // InternalPogoDsl.g:6244:2: iv_ruleLongType= ruleLongType EOF
            {
             newCompositeNode(grammarAccess.getLongTypeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleLongType=ruleLongType();

            state._fsp--;

             current =iv_ruleLongType; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleLongType"


    // $ANTLR start "ruleLongType"
    // InternalPogoDsl.g:6250:1: ruleLongType returns [EObject current=null] : ( () otherlv_1= 'LongType' ) ;
    public final EObject ruleLongType() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;


        	enterRule();

        try {
            // InternalPogoDsl.g:6256:2: ( ( () otherlv_1= 'LongType' ) )
            // InternalPogoDsl.g:6257:2: ( () otherlv_1= 'LongType' )
            {
            // InternalPogoDsl.g:6257:2: ( () otherlv_1= 'LongType' )
            // InternalPogoDsl.g:6258:3: () otherlv_1= 'LongType'
            {
            // InternalPogoDsl.g:6258:3: ()
            // InternalPogoDsl.g:6259:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getLongTypeAccess().getLongTypeAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,155,FOLLOW_2); 

            			newLeafNode(otherlv_1, grammarAccess.getLongTypeAccess().getLongTypeKeyword_1());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLongType"


    // $ANTLR start "entryRuleULongType"
    // InternalPogoDsl.g:6273:1: entryRuleULongType returns [EObject current=null] : iv_ruleULongType= ruleULongType EOF ;
    public final EObject entryRuleULongType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleULongType = null;


        try {
            // InternalPogoDsl.g:6273:50: (iv_ruleULongType= ruleULongType EOF )
            // InternalPogoDsl.g:6274:2: iv_ruleULongType= ruleULongType EOF
            {
             newCompositeNode(grammarAccess.getULongTypeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleULongType=ruleULongType();

            state._fsp--;

             current =iv_ruleULongType; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleULongType"


    // $ANTLR start "ruleULongType"
    // InternalPogoDsl.g:6280:1: ruleULongType returns [EObject current=null] : ( () otherlv_1= 'ULongType' ) ;
    public final EObject ruleULongType() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;


        	enterRule();

        try {
            // InternalPogoDsl.g:6286:2: ( ( () otherlv_1= 'ULongType' ) )
            // InternalPogoDsl.g:6287:2: ( () otherlv_1= 'ULongType' )
            {
            // InternalPogoDsl.g:6287:2: ( () otherlv_1= 'ULongType' )
            // InternalPogoDsl.g:6288:3: () otherlv_1= 'ULongType'
            {
            // InternalPogoDsl.g:6288:3: ()
            // InternalPogoDsl.g:6289:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getULongTypeAccess().getULongTypeAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,156,FOLLOW_2); 

            			newLeafNode(otherlv_1, grammarAccess.getULongTypeAccess().getULongTypeKeyword_1());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleULongType"


    // $ANTLR start "entryRuleLongArrayType"
    // InternalPogoDsl.g:6303:1: entryRuleLongArrayType returns [EObject current=null] : iv_ruleLongArrayType= ruleLongArrayType EOF ;
    public final EObject entryRuleLongArrayType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleLongArrayType = null;


        try {
            // InternalPogoDsl.g:6303:54: (iv_ruleLongArrayType= ruleLongArrayType EOF )
            // InternalPogoDsl.g:6304:2: iv_ruleLongArrayType= ruleLongArrayType EOF
            {
             newCompositeNode(grammarAccess.getLongArrayTypeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleLongArrayType=ruleLongArrayType();

            state._fsp--;

             current =iv_ruleLongArrayType; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleLongArrayType"


    // $ANTLR start "ruleLongArrayType"
    // InternalPogoDsl.g:6310:1: ruleLongArrayType returns [EObject current=null] : ( () otherlv_1= 'LongArrayType' ) ;
    public final EObject ruleLongArrayType() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;


        	enterRule();

        try {
            // InternalPogoDsl.g:6316:2: ( ( () otherlv_1= 'LongArrayType' ) )
            // InternalPogoDsl.g:6317:2: ( () otherlv_1= 'LongArrayType' )
            {
            // InternalPogoDsl.g:6317:2: ( () otherlv_1= 'LongArrayType' )
            // InternalPogoDsl.g:6318:3: () otherlv_1= 'LongArrayType'
            {
            // InternalPogoDsl.g:6318:3: ()
            // InternalPogoDsl.g:6319:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getLongArrayTypeAccess().getLongArrayTypeAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,157,FOLLOW_2); 

            			newLeafNode(otherlv_1, grammarAccess.getLongArrayTypeAccess().getLongArrayTypeKeyword_1());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLongArrayType"


    // $ANTLR start "entryRuleULongArrayType"
    // InternalPogoDsl.g:6333:1: entryRuleULongArrayType returns [EObject current=null] : iv_ruleULongArrayType= ruleULongArrayType EOF ;
    public final EObject entryRuleULongArrayType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleULongArrayType = null;


        try {
            // InternalPogoDsl.g:6333:55: (iv_ruleULongArrayType= ruleULongArrayType EOF )
            // InternalPogoDsl.g:6334:2: iv_ruleULongArrayType= ruleULongArrayType EOF
            {
             newCompositeNode(grammarAccess.getULongArrayTypeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleULongArrayType=ruleULongArrayType();

            state._fsp--;

             current =iv_ruleULongArrayType; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleULongArrayType"


    // $ANTLR start "ruleULongArrayType"
    // InternalPogoDsl.g:6340:1: ruleULongArrayType returns [EObject current=null] : ( () otherlv_1= 'ULongArrayType' ) ;
    public final EObject ruleULongArrayType() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;


        	enterRule();

        try {
            // InternalPogoDsl.g:6346:2: ( ( () otherlv_1= 'ULongArrayType' ) )
            // InternalPogoDsl.g:6347:2: ( () otherlv_1= 'ULongArrayType' )
            {
            // InternalPogoDsl.g:6347:2: ( () otherlv_1= 'ULongArrayType' )
            // InternalPogoDsl.g:6348:3: () otherlv_1= 'ULongArrayType'
            {
            // InternalPogoDsl.g:6348:3: ()
            // InternalPogoDsl.g:6349:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getULongArrayTypeAccess().getULongArrayTypeAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,158,FOLLOW_2); 

            			newLeafNode(otherlv_1, grammarAccess.getULongArrayTypeAccess().getULongArrayTypeKeyword_1());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleULongArrayType"


    // $ANTLR start "entryRuleDevIntType"
    // InternalPogoDsl.g:6363:1: entryRuleDevIntType returns [EObject current=null] : iv_ruleDevIntType= ruleDevIntType EOF ;
    public final EObject entryRuleDevIntType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDevIntType = null;


        try {
            // InternalPogoDsl.g:6363:51: (iv_ruleDevIntType= ruleDevIntType EOF )
            // InternalPogoDsl.g:6364:2: iv_ruleDevIntType= ruleDevIntType EOF
            {
             newCompositeNode(grammarAccess.getDevIntTypeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleDevIntType=ruleDevIntType();

            state._fsp--;

             current =iv_ruleDevIntType; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDevIntType"


    // $ANTLR start "ruleDevIntType"
    // InternalPogoDsl.g:6370:1: ruleDevIntType returns [EObject current=null] : ( () otherlv_1= 'DevIntType' ) ;
    public final EObject ruleDevIntType() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;


        	enterRule();

        try {
            // InternalPogoDsl.g:6376:2: ( ( () otherlv_1= 'DevIntType' ) )
            // InternalPogoDsl.g:6377:2: ( () otherlv_1= 'DevIntType' )
            {
            // InternalPogoDsl.g:6377:2: ( () otherlv_1= 'DevIntType' )
            // InternalPogoDsl.g:6378:3: () otherlv_1= 'DevIntType'
            {
            // InternalPogoDsl.g:6378:3: ()
            // InternalPogoDsl.g:6379:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getDevIntTypeAccess().getDevIntTypeAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,159,FOLLOW_2); 

            			newLeafNode(otherlv_1, grammarAccess.getDevIntTypeAccess().getDevIntTypeKeyword_1());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDevIntType"


    // $ANTLR start "entryRuleEncodedType"
    // InternalPogoDsl.g:6393:1: entryRuleEncodedType returns [EObject current=null] : iv_ruleEncodedType= ruleEncodedType EOF ;
    public final EObject entryRuleEncodedType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleEncodedType = null;


        try {
            // InternalPogoDsl.g:6393:52: (iv_ruleEncodedType= ruleEncodedType EOF )
            // InternalPogoDsl.g:6394:2: iv_ruleEncodedType= ruleEncodedType EOF
            {
             newCompositeNode(grammarAccess.getEncodedTypeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleEncodedType=ruleEncodedType();

            state._fsp--;

             current =iv_ruleEncodedType; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEncodedType"


    // $ANTLR start "ruleEncodedType"
    // InternalPogoDsl.g:6400:1: ruleEncodedType returns [EObject current=null] : ( () otherlv_1= 'EncodedType' ) ;
    public final EObject ruleEncodedType() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;


        	enterRule();

        try {
            // InternalPogoDsl.g:6406:2: ( ( () otherlv_1= 'EncodedType' ) )
            // InternalPogoDsl.g:6407:2: ( () otherlv_1= 'EncodedType' )
            {
            // InternalPogoDsl.g:6407:2: ( () otherlv_1= 'EncodedType' )
            // InternalPogoDsl.g:6408:3: () otherlv_1= 'EncodedType'
            {
            // InternalPogoDsl.g:6408:3: ()
            // InternalPogoDsl.g:6409:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getEncodedTypeAccess().getEncodedTypeAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,160,FOLLOW_2); 

            			newLeafNode(otherlv_1, grammarAccess.getEncodedTypeAccess().getEncodedTypeKeyword_1());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEncodedType"


    // $ANTLR start "entryRuleEnumType"
    // InternalPogoDsl.g:6423:1: entryRuleEnumType returns [EObject current=null] : iv_ruleEnumType= ruleEnumType EOF ;
    public final EObject entryRuleEnumType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleEnumType = null;


        try {
            // InternalPogoDsl.g:6423:49: (iv_ruleEnumType= ruleEnumType EOF )
            // InternalPogoDsl.g:6424:2: iv_ruleEnumType= ruleEnumType EOF
            {
             newCompositeNode(grammarAccess.getEnumTypeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleEnumType=ruleEnumType();

            state._fsp--;

             current =iv_ruleEnumType; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEnumType"


    // $ANTLR start "ruleEnumType"
    // InternalPogoDsl.g:6430:1: ruleEnumType returns [EObject current=null] : ( () otherlv_1= 'EnumType' ) ;
    public final EObject ruleEnumType() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;


        	enterRule();

        try {
            // InternalPogoDsl.g:6436:2: ( ( () otherlv_1= 'EnumType' ) )
            // InternalPogoDsl.g:6437:2: ( () otherlv_1= 'EnumType' )
            {
            // InternalPogoDsl.g:6437:2: ( () otherlv_1= 'EnumType' )
            // InternalPogoDsl.g:6438:3: () otherlv_1= 'EnumType'
            {
            // InternalPogoDsl.g:6438:3: ()
            // InternalPogoDsl.g:6439:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getEnumTypeAccess().getEnumTypeAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,161,FOLLOW_2); 

            			newLeafNode(otherlv_1, grammarAccess.getEnumTypeAccess().getEnumTypeKeyword_1());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEnumType"


    // $ANTLR start "entryRuleFireEvents"
    // InternalPogoDsl.g:6453:1: entryRuleFireEvents returns [EObject current=null] : iv_ruleFireEvents= ruleFireEvents EOF ;
    public final EObject entryRuleFireEvents() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFireEvents = null;


        try {
            // InternalPogoDsl.g:6453:51: (iv_ruleFireEvents= ruleFireEvents EOF )
            // InternalPogoDsl.g:6454:2: iv_ruleFireEvents= ruleFireEvents EOF
            {
             newCompositeNode(grammarAccess.getFireEventsRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleFireEvents=ruleFireEvents();

            state._fsp--;

             current =iv_ruleFireEvents; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFireEvents"


    // $ANTLR start "ruleFireEvents"
    // InternalPogoDsl.g:6460:1: ruleFireEvents returns [EObject current=null] : ( () otherlv_1= 'FireEvents' otherlv_2= '{' (otherlv_3= 'fire' ( (lv_fire_4_0= ruleEString ) ) )? (otherlv_5= 'libCheckCriteria' ( (lv_libCheckCriteria_6_0= ruleEString ) ) )? otherlv_7= '}' ) ;
    public final EObject ruleFireEvents() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        AntlrDatatypeRuleToken lv_fire_4_0 = null;

        AntlrDatatypeRuleToken lv_libCheckCriteria_6_0 = null;



        	enterRule();

        try {
            // InternalPogoDsl.g:6466:2: ( ( () otherlv_1= 'FireEvents' otherlv_2= '{' (otherlv_3= 'fire' ( (lv_fire_4_0= ruleEString ) ) )? (otherlv_5= 'libCheckCriteria' ( (lv_libCheckCriteria_6_0= ruleEString ) ) )? otherlv_7= '}' ) )
            // InternalPogoDsl.g:6467:2: ( () otherlv_1= 'FireEvents' otherlv_2= '{' (otherlv_3= 'fire' ( (lv_fire_4_0= ruleEString ) ) )? (otherlv_5= 'libCheckCriteria' ( (lv_libCheckCriteria_6_0= ruleEString ) ) )? otherlv_7= '}' )
            {
            // InternalPogoDsl.g:6467:2: ( () otherlv_1= 'FireEvents' otherlv_2= '{' (otherlv_3= 'fire' ( (lv_fire_4_0= ruleEString ) ) )? (otherlv_5= 'libCheckCriteria' ( (lv_libCheckCriteria_6_0= ruleEString ) ) )? otherlv_7= '}' )
            // InternalPogoDsl.g:6468:3: () otherlv_1= 'FireEvents' otherlv_2= '{' (otherlv_3= 'fire' ( (lv_fire_4_0= ruleEString ) ) )? (otherlv_5= 'libCheckCriteria' ( (lv_libCheckCriteria_6_0= ruleEString ) ) )? otherlv_7= '}'
            {
            // InternalPogoDsl.g:6468:3: ()
            // InternalPogoDsl.g:6469:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getFireEventsAccess().getFireEventsAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,162,FOLLOW_3); 

            			newLeafNode(otherlv_1, grammarAccess.getFireEventsAccess().getFireEventsKeyword_1());
            		
            otherlv_2=(Token)match(input,12,FOLLOW_153); 

            			newLeafNode(otherlv_2, grammarAccess.getFireEventsAccess().getLeftCurlyBracketKeyword_2());
            		
            // InternalPogoDsl.g:6483:3: (otherlv_3= 'fire' ( (lv_fire_4_0= ruleEString ) ) )?
            int alt146=2;
            int LA146_0 = input.LA(1);

            if ( (LA146_0==163) ) {
                alt146=1;
            }
            switch (alt146) {
                case 1 :
                    // InternalPogoDsl.g:6484:4: otherlv_3= 'fire' ( (lv_fire_4_0= ruleEString ) )
                    {
                    otherlv_3=(Token)match(input,163,FOLLOW_13); 

                    				newLeafNode(otherlv_3, grammarAccess.getFireEventsAccess().getFireKeyword_3_0());
                    			
                    // InternalPogoDsl.g:6488:4: ( (lv_fire_4_0= ruleEString ) )
                    // InternalPogoDsl.g:6489:5: (lv_fire_4_0= ruleEString )
                    {
                    // InternalPogoDsl.g:6489:5: (lv_fire_4_0= ruleEString )
                    // InternalPogoDsl.g:6490:6: lv_fire_4_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getFireEventsAccess().getFireEStringParserRuleCall_3_1_0());
                    					
                    pushFollow(FOLLOW_154);
                    lv_fire_4_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getFireEventsRule());
                    						}
                    						set(
                    							current,
                    							"fire",
                    							lv_fire_4_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalPogoDsl.g:6508:3: (otherlv_5= 'libCheckCriteria' ( (lv_libCheckCriteria_6_0= ruleEString ) ) )?
            int alt147=2;
            int LA147_0 = input.LA(1);

            if ( (LA147_0==164) ) {
                alt147=1;
            }
            switch (alt147) {
                case 1 :
                    // InternalPogoDsl.g:6509:4: otherlv_5= 'libCheckCriteria' ( (lv_libCheckCriteria_6_0= ruleEString ) )
                    {
                    otherlv_5=(Token)match(input,164,FOLLOW_13); 

                    				newLeafNode(otherlv_5, grammarAccess.getFireEventsAccess().getLibCheckCriteriaKeyword_4_0());
                    			
                    // InternalPogoDsl.g:6513:4: ( (lv_libCheckCriteria_6_0= ruleEString ) )
                    // InternalPogoDsl.g:6514:5: (lv_libCheckCriteria_6_0= ruleEString )
                    {
                    // InternalPogoDsl.g:6514:5: (lv_libCheckCriteria_6_0= ruleEString )
                    // InternalPogoDsl.g:6515:6: lv_libCheckCriteria_6_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getFireEventsAccess().getLibCheckCriteriaEStringParserRuleCall_4_1_0());
                    					
                    pushFollow(FOLLOW_11);
                    lv_libCheckCriteria_6_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getFireEventsRule());
                    						}
                    						set(
                    							current,
                    							"libCheckCriteria",
                    							lv_libCheckCriteria_6_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            otherlv_7=(Token)match(input,15,FOLLOW_2); 

            			newLeafNode(otherlv_7, grammarAccess.getFireEventsAccess().getRightCurlyBracketKeyword_5());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFireEvents"


    // $ANTLR start "entryRuleAttrProperties"
    // InternalPogoDsl.g:6541:1: entryRuleAttrProperties returns [EObject current=null] : iv_ruleAttrProperties= ruleAttrProperties EOF ;
    public final EObject entryRuleAttrProperties() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAttrProperties = null;


        try {
            // InternalPogoDsl.g:6541:55: (iv_ruleAttrProperties= ruleAttrProperties EOF )
            // InternalPogoDsl.g:6542:2: iv_ruleAttrProperties= ruleAttrProperties EOF
            {
             newCompositeNode(grammarAccess.getAttrPropertiesRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAttrProperties=ruleAttrProperties();

            state._fsp--;

             current =iv_ruleAttrProperties; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAttrProperties"


    // $ANTLR start "ruleAttrProperties"
    // InternalPogoDsl.g:6548:1: ruleAttrProperties returns [EObject current=null] : ( () otherlv_1= 'AttrProperties' otherlv_2= '{' (otherlv_3= 'description' ( (lv_description_4_0= ruleEString ) ) )? (otherlv_5= 'label' ( (lv_label_6_0= ruleEString ) ) )? (otherlv_7= 'unit' ( (lv_unit_8_0= ruleEString ) ) )? (otherlv_9= 'standardUnit' ( (lv_standardUnit_10_0= ruleEString ) ) )? (otherlv_11= 'displayUnit' ( (lv_displayUnit_12_0= ruleEString ) ) )? (otherlv_13= 'format' ( (lv_format_14_0= ruleEString ) ) )? (otherlv_15= 'maxValue' ( (lv_maxValue_16_0= ruleEString ) ) )? (otherlv_17= 'minValue' ( (lv_minValue_18_0= ruleEString ) ) )? (otherlv_19= 'maxAlarm' ( (lv_maxAlarm_20_0= ruleEString ) ) )? (otherlv_21= 'minAlarm' ( (lv_minAlarm_22_0= ruleEString ) ) )? (otherlv_23= 'maxWarning' ( (lv_maxWarning_24_0= ruleEString ) ) )? (otherlv_25= 'minWarning' ( (lv_minWarning_26_0= ruleEString ) ) )? (otherlv_27= 'deltaTime' ( (lv_deltaTime_28_0= ruleEString ) ) )? (otherlv_29= 'deltaValue' ( (lv_deltaValue_30_0= ruleEString ) ) )? otherlv_31= '}' ) ;
    public final EObject ruleAttrProperties() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        Token otherlv_11=null;
        Token otherlv_13=null;
        Token otherlv_15=null;
        Token otherlv_17=null;
        Token otherlv_19=null;
        Token otherlv_21=null;
        Token otherlv_23=null;
        Token otherlv_25=null;
        Token otherlv_27=null;
        Token otherlv_29=null;
        Token otherlv_31=null;
        AntlrDatatypeRuleToken lv_description_4_0 = null;

        AntlrDatatypeRuleToken lv_label_6_0 = null;

        AntlrDatatypeRuleToken lv_unit_8_0 = null;

        AntlrDatatypeRuleToken lv_standardUnit_10_0 = null;

        AntlrDatatypeRuleToken lv_displayUnit_12_0 = null;

        AntlrDatatypeRuleToken lv_format_14_0 = null;

        AntlrDatatypeRuleToken lv_maxValue_16_0 = null;

        AntlrDatatypeRuleToken lv_minValue_18_0 = null;

        AntlrDatatypeRuleToken lv_maxAlarm_20_0 = null;

        AntlrDatatypeRuleToken lv_minAlarm_22_0 = null;

        AntlrDatatypeRuleToken lv_maxWarning_24_0 = null;

        AntlrDatatypeRuleToken lv_minWarning_26_0 = null;

        AntlrDatatypeRuleToken lv_deltaTime_28_0 = null;

        AntlrDatatypeRuleToken lv_deltaValue_30_0 = null;



        	enterRule();

        try {
            // InternalPogoDsl.g:6554:2: ( ( () otherlv_1= 'AttrProperties' otherlv_2= '{' (otherlv_3= 'description' ( (lv_description_4_0= ruleEString ) ) )? (otherlv_5= 'label' ( (lv_label_6_0= ruleEString ) ) )? (otherlv_7= 'unit' ( (lv_unit_8_0= ruleEString ) ) )? (otherlv_9= 'standardUnit' ( (lv_standardUnit_10_0= ruleEString ) ) )? (otherlv_11= 'displayUnit' ( (lv_displayUnit_12_0= ruleEString ) ) )? (otherlv_13= 'format' ( (lv_format_14_0= ruleEString ) ) )? (otherlv_15= 'maxValue' ( (lv_maxValue_16_0= ruleEString ) ) )? (otherlv_17= 'minValue' ( (lv_minValue_18_0= ruleEString ) ) )? (otherlv_19= 'maxAlarm' ( (lv_maxAlarm_20_0= ruleEString ) ) )? (otherlv_21= 'minAlarm' ( (lv_minAlarm_22_0= ruleEString ) ) )? (otherlv_23= 'maxWarning' ( (lv_maxWarning_24_0= ruleEString ) ) )? (otherlv_25= 'minWarning' ( (lv_minWarning_26_0= ruleEString ) ) )? (otherlv_27= 'deltaTime' ( (lv_deltaTime_28_0= ruleEString ) ) )? (otherlv_29= 'deltaValue' ( (lv_deltaValue_30_0= ruleEString ) ) )? otherlv_31= '}' ) )
            // InternalPogoDsl.g:6555:2: ( () otherlv_1= 'AttrProperties' otherlv_2= '{' (otherlv_3= 'description' ( (lv_description_4_0= ruleEString ) ) )? (otherlv_5= 'label' ( (lv_label_6_0= ruleEString ) ) )? (otherlv_7= 'unit' ( (lv_unit_8_0= ruleEString ) ) )? (otherlv_9= 'standardUnit' ( (lv_standardUnit_10_0= ruleEString ) ) )? (otherlv_11= 'displayUnit' ( (lv_displayUnit_12_0= ruleEString ) ) )? (otherlv_13= 'format' ( (lv_format_14_0= ruleEString ) ) )? (otherlv_15= 'maxValue' ( (lv_maxValue_16_0= ruleEString ) ) )? (otherlv_17= 'minValue' ( (lv_minValue_18_0= ruleEString ) ) )? (otherlv_19= 'maxAlarm' ( (lv_maxAlarm_20_0= ruleEString ) ) )? (otherlv_21= 'minAlarm' ( (lv_minAlarm_22_0= ruleEString ) ) )? (otherlv_23= 'maxWarning' ( (lv_maxWarning_24_0= ruleEString ) ) )? (otherlv_25= 'minWarning' ( (lv_minWarning_26_0= ruleEString ) ) )? (otherlv_27= 'deltaTime' ( (lv_deltaTime_28_0= ruleEString ) ) )? (otherlv_29= 'deltaValue' ( (lv_deltaValue_30_0= ruleEString ) ) )? otherlv_31= '}' )
            {
            // InternalPogoDsl.g:6555:2: ( () otherlv_1= 'AttrProperties' otherlv_2= '{' (otherlv_3= 'description' ( (lv_description_4_0= ruleEString ) ) )? (otherlv_5= 'label' ( (lv_label_6_0= ruleEString ) ) )? (otherlv_7= 'unit' ( (lv_unit_8_0= ruleEString ) ) )? (otherlv_9= 'standardUnit' ( (lv_standardUnit_10_0= ruleEString ) ) )? (otherlv_11= 'displayUnit' ( (lv_displayUnit_12_0= ruleEString ) ) )? (otherlv_13= 'format' ( (lv_format_14_0= ruleEString ) ) )? (otherlv_15= 'maxValue' ( (lv_maxValue_16_0= ruleEString ) ) )? (otherlv_17= 'minValue' ( (lv_minValue_18_0= ruleEString ) ) )? (otherlv_19= 'maxAlarm' ( (lv_maxAlarm_20_0= ruleEString ) ) )? (otherlv_21= 'minAlarm' ( (lv_minAlarm_22_0= ruleEString ) ) )? (otherlv_23= 'maxWarning' ( (lv_maxWarning_24_0= ruleEString ) ) )? (otherlv_25= 'minWarning' ( (lv_minWarning_26_0= ruleEString ) ) )? (otherlv_27= 'deltaTime' ( (lv_deltaTime_28_0= ruleEString ) ) )? (otherlv_29= 'deltaValue' ( (lv_deltaValue_30_0= ruleEString ) ) )? otherlv_31= '}' )
            // InternalPogoDsl.g:6556:3: () otherlv_1= 'AttrProperties' otherlv_2= '{' (otherlv_3= 'description' ( (lv_description_4_0= ruleEString ) ) )? (otherlv_5= 'label' ( (lv_label_6_0= ruleEString ) ) )? (otherlv_7= 'unit' ( (lv_unit_8_0= ruleEString ) ) )? (otherlv_9= 'standardUnit' ( (lv_standardUnit_10_0= ruleEString ) ) )? (otherlv_11= 'displayUnit' ( (lv_displayUnit_12_0= ruleEString ) ) )? (otherlv_13= 'format' ( (lv_format_14_0= ruleEString ) ) )? (otherlv_15= 'maxValue' ( (lv_maxValue_16_0= ruleEString ) ) )? (otherlv_17= 'minValue' ( (lv_minValue_18_0= ruleEString ) ) )? (otherlv_19= 'maxAlarm' ( (lv_maxAlarm_20_0= ruleEString ) ) )? (otherlv_21= 'minAlarm' ( (lv_minAlarm_22_0= ruleEString ) ) )? (otherlv_23= 'maxWarning' ( (lv_maxWarning_24_0= ruleEString ) ) )? (otherlv_25= 'minWarning' ( (lv_minWarning_26_0= ruleEString ) ) )? (otherlv_27= 'deltaTime' ( (lv_deltaTime_28_0= ruleEString ) ) )? (otherlv_29= 'deltaValue' ( (lv_deltaValue_30_0= ruleEString ) ) )? otherlv_31= '}'
            {
            // InternalPogoDsl.g:6556:3: ()
            // InternalPogoDsl.g:6557:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getAttrPropertiesAccess().getAttrPropertiesAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,165,FOLLOW_3); 

            			newLeafNode(otherlv_1, grammarAccess.getAttrPropertiesAccess().getAttrPropertiesKeyword_1());
            		
            otherlv_2=(Token)match(input,12,FOLLOW_155); 

            			newLeafNode(otherlv_2, grammarAccess.getAttrPropertiesAccess().getLeftCurlyBracketKeyword_2());
            		
            // InternalPogoDsl.g:6571:3: (otherlv_3= 'description' ( (lv_description_4_0= ruleEString ) ) )?
            int alt148=2;
            int LA148_0 = input.LA(1);

            if ( (LA148_0==27) ) {
                alt148=1;
            }
            switch (alt148) {
                case 1 :
                    // InternalPogoDsl.g:6572:4: otherlv_3= 'description' ( (lv_description_4_0= ruleEString ) )
                    {
                    otherlv_3=(Token)match(input,27,FOLLOW_13); 

                    				newLeafNode(otherlv_3, grammarAccess.getAttrPropertiesAccess().getDescriptionKeyword_3_0());
                    			
                    // InternalPogoDsl.g:6576:4: ( (lv_description_4_0= ruleEString ) )
                    // InternalPogoDsl.g:6577:5: (lv_description_4_0= ruleEString )
                    {
                    // InternalPogoDsl.g:6577:5: (lv_description_4_0= ruleEString )
                    // InternalPogoDsl.g:6578:6: lv_description_4_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getAttrPropertiesAccess().getDescriptionEStringParserRuleCall_3_1_0());
                    					
                    pushFollow(FOLLOW_156);
                    lv_description_4_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getAttrPropertiesRule());
                    						}
                    						set(
                    							current,
                    							"description",
                    							lv_description_4_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalPogoDsl.g:6596:3: (otherlv_5= 'label' ( (lv_label_6_0= ruleEString ) ) )?
            int alt149=2;
            int LA149_0 = input.LA(1);

            if ( (LA149_0==89) ) {
                alt149=1;
            }
            switch (alt149) {
                case 1 :
                    // InternalPogoDsl.g:6597:4: otherlv_5= 'label' ( (lv_label_6_0= ruleEString ) )
                    {
                    otherlv_5=(Token)match(input,89,FOLLOW_13); 

                    				newLeafNode(otherlv_5, grammarAccess.getAttrPropertiesAccess().getLabelKeyword_4_0());
                    			
                    // InternalPogoDsl.g:6601:4: ( (lv_label_6_0= ruleEString ) )
                    // InternalPogoDsl.g:6602:5: (lv_label_6_0= ruleEString )
                    {
                    // InternalPogoDsl.g:6602:5: (lv_label_6_0= ruleEString )
                    // InternalPogoDsl.g:6603:6: lv_label_6_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getAttrPropertiesAccess().getLabelEStringParserRuleCall_4_1_0());
                    					
                    pushFollow(FOLLOW_157);
                    lv_label_6_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getAttrPropertiesRule());
                    						}
                    						set(
                    							current,
                    							"label",
                    							lv_label_6_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalPogoDsl.g:6621:3: (otherlv_7= 'unit' ( (lv_unit_8_0= ruleEString ) ) )?
            int alt150=2;
            int LA150_0 = input.LA(1);

            if ( (LA150_0==166) ) {
                alt150=1;
            }
            switch (alt150) {
                case 1 :
                    // InternalPogoDsl.g:6622:4: otherlv_7= 'unit' ( (lv_unit_8_0= ruleEString ) )
                    {
                    otherlv_7=(Token)match(input,166,FOLLOW_13); 

                    				newLeafNode(otherlv_7, grammarAccess.getAttrPropertiesAccess().getUnitKeyword_5_0());
                    			
                    // InternalPogoDsl.g:6626:4: ( (lv_unit_8_0= ruleEString ) )
                    // InternalPogoDsl.g:6627:5: (lv_unit_8_0= ruleEString )
                    {
                    // InternalPogoDsl.g:6627:5: (lv_unit_8_0= ruleEString )
                    // InternalPogoDsl.g:6628:6: lv_unit_8_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getAttrPropertiesAccess().getUnitEStringParserRuleCall_5_1_0());
                    					
                    pushFollow(FOLLOW_158);
                    lv_unit_8_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getAttrPropertiesRule());
                    						}
                    						set(
                    							current,
                    							"unit",
                    							lv_unit_8_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalPogoDsl.g:6646:3: (otherlv_9= 'standardUnit' ( (lv_standardUnit_10_0= ruleEString ) ) )?
            int alt151=2;
            int LA151_0 = input.LA(1);

            if ( (LA151_0==167) ) {
                alt151=1;
            }
            switch (alt151) {
                case 1 :
                    // InternalPogoDsl.g:6647:4: otherlv_9= 'standardUnit' ( (lv_standardUnit_10_0= ruleEString ) )
                    {
                    otherlv_9=(Token)match(input,167,FOLLOW_13); 

                    				newLeafNode(otherlv_9, grammarAccess.getAttrPropertiesAccess().getStandardUnitKeyword_6_0());
                    			
                    // InternalPogoDsl.g:6651:4: ( (lv_standardUnit_10_0= ruleEString ) )
                    // InternalPogoDsl.g:6652:5: (lv_standardUnit_10_0= ruleEString )
                    {
                    // InternalPogoDsl.g:6652:5: (lv_standardUnit_10_0= ruleEString )
                    // InternalPogoDsl.g:6653:6: lv_standardUnit_10_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getAttrPropertiesAccess().getStandardUnitEStringParserRuleCall_6_1_0());
                    					
                    pushFollow(FOLLOW_159);
                    lv_standardUnit_10_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getAttrPropertiesRule());
                    						}
                    						set(
                    							current,
                    							"standardUnit",
                    							lv_standardUnit_10_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalPogoDsl.g:6671:3: (otherlv_11= 'displayUnit' ( (lv_displayUnit_12_0= ruleEString ) ) )?
            int alt152=2;
            int LA152_0 = input.LA(1);

            if ( (LA152_0==168) ) {
                alt152=1;
            }
            switch (alt152) {
                case 1 :
                    // InternalPogoDsl.g:6672:4: otherlv_11= 'displayUnit' ( (lv_displayUnit_12_0= ruleEString ) )
                    {
                    otherlv_11=(Token)match(input,168,FOLLOW_13); 

                    				newLeafNode(otherlv_11, grammarAccess.getAttrPropertiesAccess().getDisplayUnitKeyword_7_0());
                    			
                    // InternalPogoDsl.g:6676:4: ( (lv_displayUnit_12_0= ruleEString ) )
                    // InternalPogoDsl.g:6677:5: (lv_displayUnit_12_0= ruleEString )
                    {
                    // InternalPogoDsl.g:6677:5: (lv_displayUnit_12_0= ruleEString )
                    // InternalPogoDsl.g:6678:6: lv_displayUnit_12_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getAttrPropertiesAccess().getDisplayUnitEStringParserRuleCall_7_1_0());
                    					
                    pushFollow(FOLLOW_160);
                    lv_displayUnit_12_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getAttrPropertiesRule());
                    						}
                    						set(
                    							current,
                    							"displayUnit",
                    							lv_displayUnit_12_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalPogoDsl.g:6696:3: (otherlv_13= 'format' ( (lv_format_14_0= ruleEString ) ) )?
            int alt153=2;
            int LA153_0 = input.LA(1);

            if ( (LA153_0==169) ) {
                alt153=1;
            }
            switch (alt153) {
                case 1 :
                    // InternalPogoDsl.g:6697:4: otherlv_13= 'format' ( (lv_format_14_0= ruleEString ) )
                    {
                    otherlv_13=(Token)match(input,169,FOLLOW_13); 

                    				newLeafNode(otherlv_13, grammarAccess.getAttrPropertiesAccess().getFormatKeyword_8_0());
                    			
                    // InternalPogoDsl.g:6701:4: ( (lv_format_14_0= ruleEString ) )
                    // InternalPogoDsl.g:6702:5: (lv_format_14_0= ruleEString )
                    {
                    // InternalPogoDsl.g:6702:5: (lv_format_14_0= ruleEString )
                    // InternalPogoDsl.g:6703:6: lv_format_14_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getAttrPropertiesAccess().getFormatEStringParserRuleCall_8_1_0());
                    					
                    pushFollow(FOLLOW_161);
                    lv_format_14_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getAttrPropertiesRule());
                    						}
                    						set(
                    							current,
                    							"format",
                    							lv_format_14_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalPogoDsl.g:6721:3: (otherlv_15= 'maxValue' ( (lv_maxValue_16_0= ruleEString ) ) )?
            int alt154=2;
            int LA154_0 = input.LA(1);

            if ( (LA154_0==170) ) {
                alt154=1;
            }
            switch (alt154) {
                case 1 :
                    // InternalPogoDsl.g:6722:4: otherlv_15= 'maxValue' ( (lv_maxValue_16_0= ruleEString ) )
                    {
                    otherlv_15=(Token)match(input,170,FOLLOW_13); 

                    				newLeafNode(otherlv_15, grammarAccess.getAttrPropertiesAccess().getMaxValueKeyword_9_0());
                    			
                    // InternalPogoDsl.g:6726:4: ( (lv_maxValue_16_0= ruleEString ) )
                    // InternalPogoDsl.g:6727:5: (lv_maxValue_16_0= ruleEString )
                    {
                    // InternalPogoDsl.g:6727:5: (lv_maxValue_16_0= ruleEString )
                    // InternalPogoDsl.g:6728:6: lv_maxValue_16_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getAttrPropertiesAccess().getMaxValueEStringParserRuleCall_9_1_0());
                    					
                    pushFollow(FOLLOW_162);
                    lv_maxValue_16_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getAttrPropertiesRule());
                    						}
                    						set(
                    							current,
                    							"maxValue",
                    							lv_maxValue_16_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalPogoDsl.g:6746:3: (otherlv_17= 'minValue' ( (lv_minValue_18_0= ruleEString ) ) )?
            int alt155=2;
            int LA155_0 = input.LA(1);

            if ( (LA155_0==171) ) {
                alt155=1;
            }
            switch (alt155) {
                case 1 :
                    // InternalPogoDsl.g:6747:4: otherlv_17= 'minValue' ( (lv_minValue_18_0= ruleEString ) )
                    {
                    otherlv_17=(Token)match(input,171,FOLLOW_13); 

                    				newLeafNode(otherlv_17, grammarAccess.getAttrPropertiesAccess().getMinValueKeyword_10_0());
                    			
                    // InternalPogoDsl.g:6751:4: ( (lv_minValue_18_0= ruleEString ) )
                    // InternalPogoDsl.g:6752:5: (lv_minValue_18_0= ruleEString )
                    {
                    // InternalPogoDsl.g:6752:5: (lv_minValue_18_0= ruleEString )
                    // InternalPogoDsl.g:6753:6: lv_minValue_18_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getAttrPropertiesAccess().getMinValueEStringParserRuleCall_10_1_0());
                    					
                    pushFollow(FOLLOW_163);
                    lv_minValue_18_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getAttrPropertiesRule());
                    						}
                    						set(
                    							current,
                    							"minValue",
                    							lv_minValue_18_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalPogoDsl.g:6771:3: (otherlv_19= 'maxAlarm' ( (lv_maxAlarm_20_0= ruleEString ) ) )?
            int alt156=2;
            int LA156_0 = input.LA(1);

            if ( (LA156_0==172) ) {
                alt156=1;
            }
            switch (alt156) {
                case 1 :
                    // InternalPogoDsl.g:6772:4: otherlv_19= 'maxAlarm' ( (lv_maxAlarm_20_0= ruleEString ) )
                    {
                    otherlv_19=(Token)match(input,172,FOLLOW_13); 

                    				newLeafNode(otherlv_19, grammarAccess.getAttrPropertiesAccess().getMaxAlarmKeyword_11_0());
                    			
                    // InternalPogoDsl.g:6776:4: ( (lv_maxAlarm_20_0= ruleEString ) )
                    // InternalPogoDsl.g:6777:5: (lv_maxAlarm_20_0= ruleEString )
                    {
                    // InternalPogoDsl.g:6777:5: (lv_maxAlarm_20_0= ruleEString )
                    // InternalPogoDsl.g:6778:6: lv_maxAlarm_20_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getAttrPropertiesAccess().getMaxAlarmEStringParserRuleCall_11_1_0());
                    					
                    pushFollow(FOLLOW_164);
                    lv_maxAlarm_20_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getAttrPropertiesRule());
                    						}
                    						set(
                    							current,
                    							"maxAlarm",
                    							lv_maxAlarm_20_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalPogoDsl.g:6796:3: (otherlv_21= 'minAlarm' ( (lv_minAlarm_22_0= ruleEString ) ) )?
            int alt157=2;
            int LA157_0 = input.LA(1);

            if ( (LA157_0==173) ) {
                alt157=1;
            }
            switch (alt157) {
                case 1 :
                    // InternalPogoDsl.g:6797:4: otherlv_21= 'minAlarm' ( (lv_minAlarm_22_0= ruleEString ) )
                    {
                    otherlv_21=(Token)match(input,173,FOLLOW_13); 

                    				newLeafNode(otherlv_21, grammarAccess.getAttrPropertiesAccess().getMinAlarmKeyword_12_0());
                    			
                    // InternalPogoDsl.g:6801:4: ( (lv_minAlarm_22_0= ruleEString ) )
                    // InternalPogoDsl.g:6802:5: (lv_minAlarm_22_0= ruleEString )
                    {
                    // InternalPogoDsl.g:6802:5: (lv_minAlarm_22_0= ruleEString )
                    // InternalPogoDsl.g:6803:6: lv_minAlarm_22_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getAttrPropertiesAccess().getMinAlarmEStringParserRuleCall_12_1_0());
                    					
                    pushFollow(FOLLOW_165);
                    lv_minAlarm_22_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getAttrPropertiesRule());
                    						}
                    						set(
                    							current,
                    							"minAlarm",
                    							lv_minAlarm_22_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalPogoDsl.g:6821:3: (otherlv_23= 'maxWarning' ( (lv_maxWarning_24_0= ruleEString ) ) )?
            int alt158=2;
            int LA158_0 = input.LA(1);

            if ( (LA158_0==174) ) {
                alt158=1;
            }
            switch (alt158) {
                case 1 :
                    // InternalPogoDsl.g:6822:4: otherlv_23= 'maxWarning' ( (lv_maxWarning_24_0= ruleEString ) )
                    {
                    otherlv_23=(Token)match(input,174,FOLLOW_13); 

                    				newLeafNode(otherlv_23, grammarAccess.getAttrPropertiesAccess().getMaxWarningKeyword_13_0());
                    			
                    // InternalPogoDsl.g:6826:4: ( (lv_maxWarning_24_0= ruleEString ) )
                    // InternalPogoDsl.g:6827:5: (lv_maxWarning_24_0= ruleEString )
                    {
                    // InternalPogoDsl.g:6827:5: (lv_maxWarning_24_0= ruleEString )
                    // InternalPogoDsl.g:6828:6: lv_maxWarning_24_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getAttrPropertiesAccess().getMaxWarningEStringParserRuleCall_13_1_0());
                    					
                    pushFollow(FOLLOW_166);
                    lv_maxWarning_24_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getAttrPropertiesRule());
                    						}
                    						set(
                    							current,
                    							"maxWarning",
                    							lv_maxWarning_24_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalPogoDsl.g:6846:3: (otherlv_25= 'minWarning' ( (lv_minWarning_26_0= ruleEString ) ) )?
            int alt159=2;
            int LA159_0 = input.LA(1);

            if ( (LA159_0==175) ) {
                alt159=1;
            }
            switch (alt159) {
                case 1 :
                    // InternalPogoDsl.g:6847:4: otherlv_25= 'minWarning' ( (lv_minWarning_26_0= ruleEString ) )
                    {
                    otherlv_25=(Token)match(input,175,FOLLOW_13); 

                    				newLeafNode(otherlv_25, grammarAccess.getAttrPropertiesAccess().getMinWarningKeyword_14_0());
                    			
                    // InternalPogoDsl.g:6851:4: ( (lv_minWarning_26_0= ruleEString ) )
                    // InternalPogoDsl.g:6852:5: (lv_minWarning_26_0= ruleEString )
                    {
                    // InternalPogoDsl.g:6852:5: (lv_minWarning_26_0= ruleEString )
                    // InternalPogoDsl.g:6853:6: lv_minWarning_26_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getAttrPropertiesAccess().getMinWarningEStringParserRuleCall_14_1_0());
                    					
                    pushFollow(FOLLOW_167);
                    lv_minWarning_26_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getAttrPropertiesRule());
                    						}
                    						set(
                    							current,
                    							"minWarning",
                    							lv_minWarning_26_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalPogoDsl.g:6871:3: (otherlv_27= 'deltaTime' ( (lv_deltaTime_28_0= ruleEString ) ) )?
            int alt160=2;
            int LA160_0 = input.LA(1);

            if ( (LA160_0==176) ) {
                alt160=1;
            }
            switch (alt160) {
                case 1 :
                    // InternalPogoDsl.g:6872:4: otherlv_27= 'deltaTime' ( (lv_deltaTime_28_0= ruleEString ) )
                    {
                    otherlv_27=(Token)match(input,176,FOLLOW_13); 

                    				newLeafNode(otherlv_27, grammarAccess.getAttrPropertiesAccess().getDeltaTimeKeyword_15_0());
                    			
                    // InternalPogoDsl.g:6876:4: ( (lv_deltaTime_28_0= ruleEString ) )
                    // InternalPogoDsl.g:6877:5: (lv_deltaTime_28_0= ruleEString )
                    {
                    // InternalPogoDsl.g:6877:5: (lv_deltaTime_28_0= ruleEString )
                    // InternalPogoDsl.g:6878:6: lv_deltaTime_28_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getAttrPropertiesAccess().getDeltaTimeEStringParserRuleCall_15_1_0());
                    					
                    pushFollow(FOLLOW_168);
                    lv_deltaTime_28_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getAttrPropertiesRule());
                    						}
                    						set(
                    							current,
                    							"deltaTime",
                    							lv_deltaTime_28_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalPogoDsl.g:6896:3: (otherlv_29= 'deltaValue' ( (lv_deltaValue_30_0= ruleEString ) ) )?
            int alt161=2;
            int LA161_0 = input.LA(1);

            if ( (LA161_0==177) ) {
                alt161=1;
            }
            switch (alt161) {
                case 1 :
                    // InternalPogoDsl.g:6897:4: otherlv_29= 'deltaValue' ( (lv_deltaValue_30_0= ruleEString ) )
                    {
                    otherlv_29=(Token)match(input,177,FOLLOW_13); 

                    				newLeafNode(otherlv_29, grammarAccess.getAttrPropertiesAccess().getDeltaValueKeyword_16_0());
                    			
                    // InternalPogoDsl.g:6901:4: ( (lv_deltaValue_30_0= ruleEString ) )
                    // InternalPogoDsl.g:6902:5: (lv_deltaValue_30_0= ruleEString )
                    {
                    // InternalPogoDsl.g:6902:5: (lv_deltaValue_30_0= ruleEString )
                    // InternalPogoDsl.g:6903:6: lv_deltaValue_30_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getAttrPropertiesAccess().getDeltaValueEStringParserRuleCall_16_1_0());
                    					
                    pushFollow(FOLLOW_11);
                    lv_deltaValue_30_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getAttrPropertiesRule());
                    						}
                    						set(
                    							current,
                    							"deltaValue",
                    							lv_deltaValue_30_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            otherlv_31=(Token)match(input,15,FOLLOW_2); 

            			newLeafNode(otherlv_31, grammarAccess.getAttrPropertiesAccess().getRightCurlyBracketKeyword_17());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAttrProperties"


    // $ANTLR start "entryRuleLanguage"
    // InternalPogoDsl.g:6929:1: entryRuleLanguage returns [String current=null] : iv_ruleLanguage= ruleLanguage EOF ;
    public final String entryRuleLanguage() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleLanguage = null;


        try {
            // InternalPogoDsl.g:6929:48: (iv_ruleLanguage= ruleLanguage EOF )
            // InternalPogoDsl.g:6930:2: iv_ruleLanguage= ruleLanguage EOF
            {
             newCompositeNode(grammarAccess.getLanguageRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleLanguage=ruleLanguage();

            state._fsp--;

             current =iv_ruleLanguage.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleLanguage"


    // $ANTLR start "ruleLanguage"
    // InternalPogoDsl.g:6936:1: ruleLanguage returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (kw= 'Cpp' | kw= 'Java' | kw= 'Python' ) ;
    public final AntlrDatatypeRuleToken ruleLanguage() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;


        	enterRule();

        try {
            // InternalPogoDsl.g:6942:2: ( (kw= 'Cpp' | kw= 'Java' | kw= 'Python' ) )
            // InternalPogoDsl.g:6943:2: (kw= 'Cpp' | kw= 'Java' | kw= 'Python' )
            {
            // InternalPogoDsl.g:6943:2: (kw= 'Cpp' | kw= 'Java' | kw= 'Python' )
            int alt162=3;
            switch ( input.LA(1) ) {
            case 178:
                {
                alt162=1;
                }
                break;
            case 179:
                {
                alt162=2;
                }
                break;
            case 180:
                {
                alt162=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 162, 0, input);

                throw nvae;
            }

            switch (alt162) {
                case 1 :
                    // InternalPogoDsl.g:6944:3: kw= 'Cpp'
                    {
                    kw=(Token)match(input,178,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getLanguageAccess().getCppKeyword_0());
                    		

                    }
                    break;
                case 2 :
                    // InternalPogoDsl.g:6950:3: kw= 'Java'
                    {
                    kw=(Token)match(input,179,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getLanguageAccess().getJavaKeyword_1());
                    		

                    }
                    break;
                case 3 :
                    // InternalPogoDsl.g:6956:3: kw= 'Python'
                    {
                    kw=(Token)match(input,180,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getLanguageAccess().getPythonKeyword_2());
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLanguage"


    // $ANTLR start "entryRuleDisplayLevel"
    // InternalPogoDsl.g:6965:1: entryRuleDisplayLevel returns [String current=null] : iv_ruleDisplayLevel= ruleDisplayLevel EOF ;
    public final String entryRuleDisplayLevel() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleDisplayLevel = null;


        try {
            // InternalPogoDsl.g:6965:52: (iv_ruleDisplayLevel= ruleDisplayLevel EOF )
            // InternalPogoDsl.g:6966:2: iv_ruleDisplayLevel= ruleDisplayLevel EOF
            {
             newCompositeNode(grammarAccess.getDisplayLevelRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleDisplayLevel=ruleDisplayLevel();

            state._fsp--;

             current =iv_ruleDisplayLevel.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDisplayLevel"


    // $ANTLR start "ruleDisplayLevel"
    // InternalPogoDsl.g:6972:1: ruleDisplayLevel returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (kw= 'OPERATOR' | kw= 'EXPERT' ) ;
    public final AntlrDatatypeRuleToken ruleDisplayLevel() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;


        	enterRule();

        try {
            // InternalPogoDsl.g:6978:2: ( (kw= 'OPERATOR' | kw= 'EXPERT' ) )
            // InternalPogoDsl.g:6979:2: (kw= 'OPERATOR' | kw= 'EXPERT' )
            {
            // InternalPogoDsl.g:6979:2: (kw= 'OPERATOR' | kw= 'EXPERT' )
            int alt163=2;
            int LA163_0 = input.LA(1);

            if ( (LA163_0==181) ) {
                alt163=1;
            }
            else if ( (LA163_0==182) ) {
                alt163=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 163, 0, input);

                throw nvae;
            }
            switch (alt163) {
                case 1 :
                    // InternalPogoDsl.g:6980:3: kw= 'OPERATOR'
                    {
                    kw=(Token)match(input,181,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getDisplayLevelAccess().getOPERATORKeyword_0());
                    		

                    }
                    break;
                case 2 :
                    // InternalPogoDsl.g:6986:3: kw= 'EXPERT'
                    {
                    kw=(Token)match(input,182,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getDisplayLevelAccess().getEXPERTKeyword_1());
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDisplayLevel"


    // $ANTLR start "entryRuleAttrType"
    // InternalPogoDsl.g:6995:1: entryRuleAttrType returns [String current=null] : iv_ruleAttrType= ruleAttrType EOF ;
    public final String entryRuleAttrType() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleAttrType = null;


        try {
            // InternalPogoDsl.g:6995:48: (iv_ruleAttrType= ruleAttrType EOF )
            // InternalPogoDsl.g:6996:2: iv_ruleAttrType= ruleAttrType EOF
            {
             newCompositeNode(grammarAccess.getAttrTypeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAttrType=ruleAttrType();

            state._fsp--;

             current =iv_ruleAttrType.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAttrType"


    // $ANTLR start "ruleAttrType"
    // InternalPogoDsl.g:7002:1: ruleAttrType returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (kw= 'Scalar' | kw= 'Spectrum' | kw= 'Image' ) ;
    public final AntlrDatatypeRuleToken ruleAttrType() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;


        	enterRule();

        try {
            // InternalPogoDsl.g:7008:2: ( (kw= 'Scalar' | kw= 'Spectrum' | kw= 'Image' ) )
            // InternalPogoDsl.g:7009:2: (kw= 'Scalar' | kw= 'Spectrum' | kw= 'Image' )
            {
            // InternalPogoDsl.g:7009:2: (kw= 'Scalar' | kw= 'Spectrum' | kw= 'Image' )
            int alt164=3;
            switch ( input.LA(1) ) {
            case 183:
                {
                alt164=1;
                }
                break;
            case 184:
                {
                alt164=2;
                }
                break;
            case 185:
                {
                alt164=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 164, 0, input);

                throw nvae;
            }

            switch (alt164) {
                case 1 :
                    // InternalPogoDsl.g:7010:3: kw= 'Scalar'
                    {
                    kw=(Token)match(input,183,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getAttrTypeAccess().getScalarKeyword_0());
                    		

                    }
                    break;
                case 2 :
                    // InternalPogoDsl.g:7016:3: kw= 'Spectrum'
                    {
                    kw=(Token)match(input,184,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getAttrTypeAccess().getSpectrumKeyword_1());
                    		

                    }
                    break;
                case 3 :
                    // InternalPogoDsl.g:7022:3: kw= 'Image'
                    {
                    kw=(Token)match(input,185,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getAttrTypeAccess().getImageKeyword_2());
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAttrType"


    // $ANTLR start "entryRuleRW_Type"
    // InternalPogoDsl.g:7031:1: entryRuleRW_Type returns [String current=null] : iv_ruleRW_Type= ruleRW_Type EOF ;
    public final String entryRuleRW_Type() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleRW_Type = null;


        try {
            // InternalPogoDsl.g:7031:47: (iv_ruleRW_Type= ruleRW_Type EOF )
            // InternalPogoDsl.g:7032:2: iv_ruleRW_Type= ruleRW_Type EOF
            {
             newCompositeNode(grammarAccess.getRW_TypeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleRW_Type=ruleRW_Type();

            state._fsp--;

             current =iv_ruleRW_Type.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRW_Type"


    // $ANTLR start "ruleRW_Type"
    // InternalPogoDsl.g:7038:1: ruleRW_Type returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (kw= 'READ' | kw= 'WRITE' | kw= 'READ_WRITE' | kw= 'READ_WITH_WRITE' ) ;
    public final AntlrDatatypeRuleToken ruleRW_Type() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;


        	enterRule();

        try {
            // InternalPogoDsl.g:7044:2: ( (kw= 'READ' | kw= 'WRITE' | kw= 'READ_WRITE' | kw= 'READ_WITH_WRITE' ) )
            // InternalPogoDsl.g:7045:2: (kw= 'READ' | kw= 'WRITE' | kw= 'READ_WRITE' | kw= 'READ_WITH_WRITE' )
            {
            // InternalPogoDsl.g:7045:2: (kw= 'READ' | kw= 'WRITE' | kw= 'READ_WRITE' | kw= 'READ_WITH_WRITE' )
            int alt165=4;
            switch ( input.LA(1) ) {
            case 186:
                {
                alt165=1;
                }
                break;
            case 187:
                {
                alt165=2;
                }
                break;
            case 188:
                {
                alt165=3;
                }
                break;
            case 189:
                {
                alt165=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 165, 0, input);

                throw nvae;
            }

            switch (alt165) {
                case 1 :
                    // InternalPogoDsl.g:7046:3: kw= 'READ'
                    {
                    kw=(Token)match(input,186,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getRW_TypeAccess().getREADKeyword_0());
                    		

                    }
                    break;
                case 2 :
                    // InternalPogoDsl.g:7052:3: kw= 'WRITE'
                    {
                    kw=(Token)match(input,187,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getRW_TypeAccess().getWRITEKeyword_1());
                    		

                    }
                    break;
                case 3 :
                    // InternalPogoDsl.g:7058:3: kw= 'READ_WRITE'
                    {
                    kw=(Token)match(input,188,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getRW_TypeAccess().getREAD_WRITEKeyword_2());
                    		

                    }
                    break;
                case 4 :
                    // InternalPogoDsl.g:7064:3: kw= 'READ_WITH_WRITE'
                    {
                    kw=(Token)match(input,189,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getRW_TypeAccess().getREAD_WITH_WRITEKeyword_3());
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRW_Type"


    // $ANTLR start "entryRuleBoolean"
    // InternalPogoDsl.g:7073:1: entryRuleBoolean returns [String current=null] : iv_ruleBoolean= ruleBoolean EOF ;
    public final String entryRuleBoolean() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleBoolean = null;


        try {
            // InternalPogoDsl.g:7073:47: (iv_ruleBoolean= ruleBoolean EOF )
            // InternalPogoDsl.g:7074:2: iv_ruleBoolean= ruleBoolean EOF
            {
             newCompositeNode(grammarAccess.getBooleanRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleBoolean=ruleBoolean();

            state._fsp--;

             current =iv_ruleBoolean.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBoolean"


    // $ANTLR start "ruleBoolean"
    // InternalPogoDsl.g:7080:1: ruleBoolean returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (kw= 'true' | kw= 'false' ) ;
    public final AntlrDatatypeRuleToken ruleBoolean() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;


        	enterRule();

        try {
            // InternalPogoDsl.g:7086:2: ( (kw= 'true' | kw= 'false' ) )
            // InternalPogoDsl.g:7087:2: (kw= 'true' | kw= 'false' )
            {
            // InternalPogoDsl.g:7087:2: (kw= 'true' | kw= 'false' )
            int alt166=2;
            int LA166_0 = input.LA(1);

            if ( (LA166_0==190) ) {
                alt166=1;
            }
            else if ( (LA166_0==191) ) {
                alt166=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 166, 0, input);

                throw nvae;
            }
            switch (alt166) {
                case 1 :
                    // InternalPogoDsl.g:7088:3: kw= 'true'
                    {
                    kw=(Token)match(input,190,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getBooleanAccess().getTrueKeyword_0());
                    		

                    }
                    break;
                case 2 :
                    // InternalPogoDsl.g:7094:3: kw= 'false'
                    {
                    kw=(Token)match(input,191,FOLLOW_2); 

                    			current.merge(kw);
                    			newLeafNode(kw, grammarAccess.getBooleanAccess().getFalseKeyword_1());
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBoolean"


    // $ANTLR start "entryRuleEventCriteria"
    // InternalPogoDsl.g:7103:1: entryRuleEventCriteria returns [EObject current=null] : iv_ruleEventCriteria= ruleEventCriteria EOF ;
    public final EObject entryRuleEventCriteria() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleEventCriteria = null;


        try {
            // InternalPogoDsl.g:7103:54: (iv_ruleEventCriteria= ruleEventCriteria EOF )
            // InternalPogoDsl.g:7104:2: iv_ruleEventCriteria= ruleEventCriteria EOF
            {
             newCompositeNode(grammarAccess.getEventCriteriaRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleEventCriteria=ruleEventCriteria();

            state._fsp--;

             current =iv_ruleEventCriteria; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEventCriteria"


    // $ANTLR start "ruleEventCriteria"
    // InternalPogoDsl.g:7110:1: ruleEventCriteria returns [EObject current=null] : ( () otherlv_1= 'EventCriteria' otherlv_2= '{' (otherlv_3= 'relChange' ( (lv_relChange_4_0= ruleEString ) ) )? (otherlv_5= 'absChange' ( (lv_absChange_6_0= ruleEString ) ) )? (otherlv_7= 'period' ( (lv_period_8_0= ruleEString ) ) )? otherlv_9= '}' ) ;
    public final EObject ruleEventCriteria() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        AntlrDatatypeRuleToken lv_relChange_4_0 = null;

        AntlrDatatypeRuleToken lv_absChange_6_0 = null;

        AntlrDatatypeRuleToken lv_period_8_0 = null;



        	enterRule();

        try {
            // InternalPogoDsl.g:7116:2: ( ( () otherlv_1= 'EventCriteria' otherlv_2= '{' (otherlv_3= 'relChange' ( (lv_relChange_4_0= ruleEString ) ) )? (otherlv_5= 'absChange' ( (lv_absChange_6_0= ruleEString ) ) )? (otherlv_7= 'period' ( (lv_period_8_0= ruleEString ) ) )? otherlv_9= '}' ) )
            // InternalPogoDsl.g:7117:2: ( () otherlv_1= 'EventCriteria' otherlv_2= '{' (otherlv_3= 'relChange' ( (lv_relChange_4_0= ruleEString ) ) )? (otherlv_5= 'absChange' ( (lv_absChange_6_0= ruleEString ) ) )? (otherlv_7= 'period' ( (lv_period_8_0= ruleEString ) ) )? otherlv_9= '}' )
            {
            // InternalPogoDsl.g:7117:2: ( () otherlv_1= 'EventCriteria' otherlv_2= '{' (otherlv_3= 'relChange' ( (lv_relChange_4_0= ruleEString ) ) )? (otherlv_5= 'absChange' ( (lv_absChange_6_0= ruleEString ) ) )? (otherlv_7= 'period' ( (lv_period_8_0= ruleEString ) ) )? otherlv_9= '}' )
            // InternalPogoDsl.g:7118:3: () otherlv_1= 'EventCriteria' otherlv_2= '{' (otherlv_3= 'relChange' ( (lv_relChange_4_0= ruleEString ) ) )? (otherlv_5= 'absChange' ( (lv_absChange_6_0= ruleEString ) ) )? (otherlv_7= 'period' ( (lv_period_8_0= ruleEString ) ) )? otherlv_9= '}'
            {
            // InternalPogoDsl.g:7118:3: ()
            // InternalPogoDsl.g:7119:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getEventCriteriaAccess().getEventCriteriaAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,192,FOLLOW_3); 

            			newLeafNode(otherlv_1, grammarAccess.getEventCriteriaAccess().getEventCriteriaKeyword_1());
            		
            otherlv_2=(Token)match(input,12,FOLLOW_169); 

            			newLeafNode(otherlv_2, grammarAccess.getEventCriteriaAccess().getLeftCurlyBracketKeyword_2());
            		
            // InternalPogoDsl.g:7133:3: (otherlv_3= 'relChange' ( (lv_relChange_4_0= ruleEString ) ) )?
            int alt167=2;
            int LA167_0 = input.LA(1);

            if ( (LA167_0==193) ) {
                alt167=1;
            }
            switch (alt167) {
                case 1 :
                    // InternalPogoDsl.g:7134:4: otherlv_3= 'relChange' ( (lv_relChange_4_0= ruleEString ) )
                    {
                    otherlv_3=(Token)match(input,193,FOLLOW_13); 

                    				newLeafNode(otherlv_3, grammarAccess.getEventCriteriaAccess().getRelChangeKeyword_3_0());
                    			
                    // InternalPogoDsl.g:7138:4: ( (lv_relChange_4_0= ruleEString ) )
                    // InternalPogoDsl.g:7139:5: (lv_relChange_4_0= ruleEString )
                    {
                    // InternalPogoDsl.g:7139:5: (lv_relChange_4_0= ruleEString )
                    // InternalPogoDsl.g:7140:6: lv_relChange_4_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getEventCriteriaAccess().getRelChangeEStringParserRuleCall_3_1_0());
                    					
                    pushFollow(FOLLOW_170);
                    lv_relChange_4_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getEventCriteriaRule());
                    						}
                    						set(
                    							current,
                    							"relChange",
                    							lv_relChange_4_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalPogoDsl.g:7158:3: (otherlv_5= 'absChange' ( (lv_absChange_6_0= ruleEString ) ) )?
            int alt168=2;
            int LA168_0 = input.LA(1);

            if ( (LA168_0==194) ) {
                alt168=1;
            }
            switch (alt168) {
                case 1 :
                    // InternalPogoDsl.g:7159:4: otherlv_5= 'absChange' ( (lv_absChange_6_0= ruleEString ) )
                    {
                    otherlv_5=(Token)match(input,194,FOLLOW_13); 

                    				newLeafNode(otherlv_5, grammarAccess.getEventCriteriaAccess().getAbsChangeKeyword_4_0());
                    			
                    // InternalPogoDsl.g:7163:4: ( (lv_absChange_6_0= ruleEString ) )
                    // InternalPogoDsl.g:7164:5: (lv_absChange_6_0= ruleEString )
                    {
                    // InternalPogoDsl.g:7164:5: (lv_absChange_6_0= ruleEString )
                    // InternalPogoDsl.g:7165:6: lv_absChange_6_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getEventCriteriaAccess().getAbsChangeEStringParserRuleCall_4_1_0());
                    					
                    pushFollow(FOLLOW_171);
                    lv_absChange_6_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getEventCriteriaRule());
                    						}
                    						set(
                    							current,
                    							"absChange",
                    							lv_absChange_6_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalPogoDsl.g:7183:3: (otherlv_7= 'period' ( (lv_period_8_0= ruleEString ) ) )?
            int alt169=2;
            int LA169_0 = input.LA(1);

            if ( (LA169_0==195) ) {
                alt169=1;
            }
            switch (alt169) {
                case 1 :
                    // InternalPogoDsl.g:7184:4: otherlv_7= 'period' ( (lv_period_8_0= ruleEString ) )
                    {
                    otherlv_7=(Token)match(input,195,FOLLOW_13); 

                    				newLeafNode(otherlv_7, grammarAccess.getEventCriteriaAccess().getPeriodKeyword_5_0());
                    			
                    // InternalPogoDsl.g:7188:4: ( (lv_period_8_0= ruleEString ) )
                    // InternalPogoDsl.g:7189:5: (lv_period_8_0= ruleEString )
                    {
                    // InternalPogoDsl.g:7189:5: (lv_period_8_0= ruleEString )
                    // InternalPogoDsl.g:7190:6: lv_period_8_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getEventCriteriaAccess().getPeriodEStringParserRuleCall_5_1_0());
                    					
                    pushFollow(FOLLOW_11);
                    lv_period_8_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getEventCriteriaRule());
                    						}
                    						set(
                    							current,
                    							"period",
                    							lv_period_8_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            otherlv_9=(Token)match(input,15,FOLLOW_2); 

            			newLeafNode(otherlv_9, grammarAccess.getEventCriteriaAccess().getRightCurlyBracketKeyword_6());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEventCriteria"


    // $ANTLR start "entryRuleOneClassSimpleDef"
    // InternalPogoDsl.g:7216:1: entryRuleOneClassSimpleDef returns [EObject current=null] : iv_ruleOneClassSimpleDef= ruleOneClassSimpleDef EOF ;
    public final EObject entryRuleOneClassSimpleDef() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleOneClassSimpleDef = null;


        try {
            // InternalPogoDsl.g:7216:58: (iv_ruleOneClassSimpleDef= ruleOneClassSimpleDef EOF )
            // InternalPogoDsl.g:7217:2: iv_ruleOneClassSimpleDef= ruleOneClassSimpleDef EOF
            {
             newCompositeNode(grammarAccess.getOneClassSimpleDefRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleOneClassSimpleDef=ruleOneClassSimpleDef();

            state._fsp--;

             current =iv_ruleOneClassSimpleDef; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOneClassSimpleDef"


    // $ANTLR start "ruleOneClassSimpleDef"
    // InternalPogoDsl.g:7223:1: ruleOneClassSimpleDef returns [EObject current=null] : ( () otherlv_1= 'OneClassSimpleDef' otherlv_2= '{' (otherlv_3= 'classname' ( (lv_classname_4_0= ruleEString ) ) )? (otherlv_5= 'sourcePath' ( (lv_sourcePath_6_0= ruleEString ) ) )? (otherlv_7= 'hasDynamic' ( (lv_hasDynamic_8_0= ruleEString ) ) )? (otherlv_9= 'pogo6' ( (lv_pogo6_10_0= ruleEString ) ) )? (otherlv_11= 'parentClasses' otherlv_12= '{' ( (lv_parentClasses_13_0= ruleEString ) ) (otherlv_14= ',' ( (lv_parentClasses_15_0= ruleEString ) ) )* otherlv_16= '}' )? (otherlv_17= 'inheritances' otherlv_18= '{' ( (lv_inheritances_19_0= ruleInheritance ) ) (otherlv_20= ',' ( (lv_inheritances_21_0= ruleInheritance ) ) )* otherlv_22= '}' )? (otherlv_23= 'additionalFiles' otherlv_24= '{' ( (lv_additionalFiles_25_0= ruleAdditionalFile ) ) (otherlv_26= ',' ( (lv_additionalFiles_27_0= ruleAdditionalFile ) ) )* otherlv_28= '}' )? otherlv_29= '}' ) ;
    public final EObject ruleOneClassSimpleDef() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        Token otherlv_11=null;
        Token otherlv_12=null;
        Token otherlv_14=null;
        Token otherlv_16=null;
        Token otherlv_17=null;
        Token otherlv_18=null;
        Token otherlv_20=null;
        Token otherlv_22=null;
        Token otherlv_23=null;
        Token otherlv_24=null;
        Token otherlv_26=null;
        Token otherlv_28=null;
        Token otherlv_29=null;
        AntlrDatatypeRuleToken lv_classname_4_0 = null;

        AntlrDatatypeRuleToken lv_sourcePath_6_0 = null;

        AntlrDatatypeRuleToken lv_hasDynamic_8_0 = null;

        AntlrDatatypeRuleToken lv_pogo6_10_0 = null;

        AntlrDatatypeRuleToken lv_parentClasses_13_0 = null;

        AntlrDatatypeRuleToken lv_parentClasses_15_0 = null;

        EObject lv_inheritances_19_0 = null;

        EObject lv_inheritances_21_0 = null;

        EObject lv_additionalFiles_25_0 = null;

        EObject lv_additionalFiles_27_0 = null;



        	enterRule();

        try {
            // InternalPogoDsl.g:7229:2: ( ( () otherlv_1= 'OneClassSimpleDef' otherlv_2= '{' (otherlv_3= 'classname' ( (lv_classname_4_0= ruleEString ) ) )? (otherlv_5= 'sourcePath' ( (lv_sourcePath_6_0= ruleEString ) ) )? (otherlv_7= 'hasDynamic' ( (lv_hasDynamic_8_0= ruleEString ) ) )? (otherlv_9= 'pogo6' ( (lv_pogo6_10_0= ruleEString ) ) )? (otherlv_11= 'parentClasses' otherlv_12= '{' ( (lv_parentClasses_13_0= ruleEString ) ) (otherlv_14= ',' ( (lv_parentClasses_15_0= ruleEString ) ) )* otherlv_16= '}' )? (otherlv_17= 'inheritances' otherlv_18= '{' ( (lv_inheritances_19_0= ruleInheritance ) ) (otherlv_20= ',' ( (lv_inheritances_21_0= ruleInheritance ) ) )* otherlv_22= '}' )? (otherlv_23= 'additionalFiles' otherlv_24= '{' ( (lv_additionalFiles_25_0= ruleAdditionalFile ) ) (otherlv_26= ',' ( (lv_additionalFiles_27_0= ruleAdditionalFile ) ) )* otherlv_28= '}' )? otherlv_29= '}' ) )
            // InternalPogoDsl.g:7230:2: ( () otherlv_1= 'OneClassSimpleDef' otherlv_2= '{' (otherlv_3= 'classname' ( (lv_classname_4_0= ruleEString ) ) )? (otherlv_5= 'sourcePath' ( (lv_sourcePath_6_0= ruleEString ) ) )? (otherlv_7= 'hasDynamic' ( (lv_hasDynamic_8_0= ruleEString ) ) )? (otherlv_9= 'pogo6' ( (lv_pogo6_10_0= ruleEString ) ) )? (otherlv_11= 'parentClasses' otherlv_12= '{' ( (lv_parentClasses_13_0= ruleEString ) ) (otherlv_14= ',' ( (lv_parentClasses_15_0= ruleEString ) ) )* otherlv_16= '}' )? (otherlv_17= 'inheritances' otherlv_18= '{' ( (lv_inheritances_19_0= ruleInheritance ) ) (otherlv_20= ',' ( (lv_inheritances_21_0= ruleInheritance ) ) )* otherlv_22= '}' )? (otherlv_23= 'additionalFiles' otherlv_24= '{' ( (lv_additionalFiles_25_0= ruleAdditionalFile ) ) (otherlv_26= ',' ( (lv_additionalFiles_27_0= ruleAdditionalFile ) ) )* otherlv_28= '}' )? otherlv_29= '}' )
            {
            // InternalPogoDsl.g:7230:2: ( () otherlv_1= 'OneClassSimpleDef' otherlv_2= '{' (otherlv_3= 'classname' ( (lv_classname_4_0= ruleEString ) ) )? (otherlv_5= 'sourcePath' ( (lv_sourcePath_6_0= ruleEString ) ) )? (otherlv_7= 'hasDynamic' ( (lv_hasDynamic_8_0= ruleEString ) ) )? (otherlv_9= 'pogo6' ( (lv_pogo6_10_0= ruleEString ) ) )? (otherlv_11= 'parentClasses' otherlv_12= '{' ( (lv_parentClasses_13_0= ruleEString ) ) (otherlv_14= ',' ( (lv_parentClasses_15_0= ruleEString ) ) )* otherlv_16= '}' )? (otherlv_17= 'inheritances' otherlv_18= '{' ( (lv_inheritances_19_0= ruleInheritance ) ) (otherlv_20= ',' ( (lv_inheritances_21_0= ruleInheritance ) ) )* otherlv_22= '}' )? (otherlv_23= 'additionalFiles' otherlv_24= '{' ( (lv_additionalFiles_25_0= ruleAdditionalFile ) ) (otherlv_26= ',' ( (lv_additionalFiles_27_0= ruleAdditionalFile ) ) )* otherlv_28= '}' )? otherlv_29= '}' )
            // InternalPogoDsl.g:7231:3: () otherlv_1= 'OneClassSimpleDef' otherlv_2= '{' (otherlv_3= 'classname' ( (lv_classname_4_0= ruleEString ) ) )? (otherlv_5= 'sourcePath' ( (lv_sourcePath_6_0= ruleEString ) ) )? (otherlv_7= 'hasDynamic' ( (lv_hasDynamic_8_0= ruleEString ) ) )? (otherlv_9= 'pogo6' ( (lv_pogo6_10_0= ruleEString ) ) )? (otherlv_11= 'parentClasses' otherlv_12= '{' ( (lv_parentClasses_13_0= ruleEString ) ) (otherlv_14= ',' ( (lv_parentClasses_15_0= ruleEString ) ) )* otherlv_16= '}' )? (otherlv_17= 'inheritances' otherlv_18= '{' ( (lv_inheritances_19_0= ruleInheritance ) ) (otherlv_20= ',' ( (lv_inheritances_21_0= ruleInheritance ) ) )* otherlv_22= '}' )? (otherlv_23= 'additionalFiles' otherlv_24= '{' ( (lv_additionalFiles_25_0= ruleAdditionalFile ) ) (otherlv_26= ',' ( (lv_additionalFiles_27_0= ruleAdditionalFile ) ) )* otherlv_28= '}' )? otherlv_29= '}'
            {
            // InternalPogoDsl.g:7231:3: ()
            // InternalPogoDsl.g:7232:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getOneClassSimpleDefAccess().getOneClassSimpleDefAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,196,FOLLOW_3); 

            			newLeafNode(otherlv_1, grammarAccess.getOneClassSimpleDefAccess().getOneClassSimpleDefKeyword_1());
            		
            otherlv_2=(Token)match(input,12,FOLLOW_172); 

            			newLeafNode(otherlv_2, grammarAccess.getOneClassSimpleDefAccess().getLeftCurlyBracketKeyword_2());
            		
            // InternalPogoDsl.g:7246:3: (otherlv_3= 'classname' ( (lv_classname_4_0= ruleEString ) ) )?
            int alt170=2;
            int LA170_0 = input.LA(1);

            if ( (LA170_0==102) ) {
                alt170=1;
            }
            switch (alt170) {
                case 1 :
                    // InternalPogoDsl.g:7247:4: otherlv_3= 'classname' ( (lv_classname_4_0= ruleEString ) )
                    {
                    otherlv_3=(Token)match(input,102,FOLLOW_13); 

                    				newLeafNode(otherlv_3, grammarAccess.getOneClassSimpleDefAccess().getClassnameKeyword_3_0());
                    			
                    // InternalPogoDsl.g:7251:4: ( (lv_classname_4_0= ruleEString ) )
                    // InternalPogoDsl.g:7252:5: (lv_classname_4_0= ruleEString )
                    {
                    // InternalPogoDsl.g:7252:5: (lv_classname_4_0= ruleEString )
                    // InternalPogoDsl.g:7253:6: lv_classname_4_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getOneClassSimpleDefAccess().getClassnameEStringParserRuleCall_3_1_0());
                    					
                    pushFollow(FOLLOW_173);
                    lv_classname_4_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getOneClassSimpleDefRule());
                    						}
                    						set(
                    							current,
                    							"classname",
                    							lv_classname_4_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalPogoDsl.g:7271:3: (otherlv_5= 'sourcePath' ( (lv_sourcePath_6_0= ruleEString ) ) )?
            int alt171=2;
            int LA171_0 = input.LA(1);

            if ( (LA171_0==42) ) {
                alt171=1;
            }
            switch (alt171) {
                case 1 :
                    // InternalPogoDsl.g:7272:4: otherlv_5= 'sourcePath' ( (lv_sourcePath_6_0= ruleEString ) )
                    {
                    otherlv_5=(Token)match(input,42,FOLLOW_13); 

                    				newLeafNode(otherlv_5, grammarAccess.getOneClassSimpleDefAccess().getSourcePathKeyword_4_0());
                    			
                    // InternalPogoDsl.g:7276:4: ( (lv_sourcePath_6_0= ruleEString ) )
                    // InternalPogoDsl.g:7277:5: (lv_sourcePath_6_0= ruleEString )
                    {
                    // InternalPogoDsl.g:7277:5: (lv_sourcePath_6_0= ruleEString )
                    // InternalPogoDsl.g:7278:6: lv_sourcePath_6_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getOneClassSimpleDefAccess().getSourcePathEStringParserRuleCall_4_1_0());
                    					
                    pushFollow(FOLLOW_174);
                    lv_sourcePath_6_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getOneClassSimpleDefRule());
                    						}
                    						set(
                    							current,
                    							"sourcePath",
                    							lv_sourcePath_6_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalPogoDsl.g:7296:3: (otherlv_7= 'hasDynamic' ( (lv_hasDynamic_8_0= ruleEString ) ) )?
            int alt172=2;
            int LA172_0 = input.LA(1);

            if ( (LA172_0==197) ) {
                alt172=1;
            }
            switch (alt172) {
                case 1 :
                    // InternalPogoDsl.g:7297:4: otherlv_7= 'hasDynamic' ( (lv_hasDynamic_8_0= ruleEString ) )
                    {
                    otherlv_7=(Token)match(input,197,FOLLOW_13); 

                    				newLeafNode(otherlv_7, grammarAccess.getOneClassSimpleDefAccess().getHasDynamicKeyword_5_0());
                    			
                    // InternalPogoDsl.g:7301:4: ( (lv_hasDynamic_8_0= ruleEString ) )
                    // InternalPogoDsl.g:7302:5: (lv_hasDynamic_8_0= ruleEString )
                    {
                    // InternalPogoDsl.g:7302:5: (lv_hasDynamic_8_0= ruleEString )
                    // InternalPogoDsl.g:7303:6: lv_hasDynamic_8_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getOneClassSimpleDefAccess().getHasDynamicEStringParserRuleCall_5_1_0());
                    					
                    pushFollow(FOLLOW_175);
                    lv_hasDynamic_8_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getOneClassSimpleDefRule());
                    						}
                    						set(
                    							current,
                    							"hasDynamic",
                    							lv_hasDynamic_8_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalPogoDsl.g:7321:3: (otherlv_9= 'pogo6' ( (lv_pogo6_10_0= ruleEString ) ) )?
            int alt173=2;
            int LA173_0 = input.LA(1);

            if ( (LA173_0==198) ) {
                alt173=1;
            }
            switch (alt173) {
                case 1 :
                    // InternalPogoDsl.g:7322:4: otherlv_9= 'pogo6' ( (lv_pogo6_10_0= ruleEString ) )
                    {
                    otherlv_9=(Token)match(input,198,FOLLOW_13); 

                    				newLeafNode(otherlv_9, grammarAccess.getOneClassSimpleDefAccess().getPogo6Keyword_6_0());
                    			
                    // InternalPogoDsl.g:7326:4: ( (lv_pogo6_10_0= ruleEString ) )
                    // InternalPogoDsl.g:7327:5: (lv_pogo6_10_0= ruleEString )
                    {
                    // InternalPogoDsl.g:7327:5: (lv_pogo6_10_0= ruleEString )
                    // InternalPogoDsl.g:7328:6: lv_pogo6_10_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getOneClassSimpleDefAccess().getPogo6EStringParserRuleCall_6_1_0());
                    					
                    pushFollow(FOLLOW_176);
                    lv_pogo6_10_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getOneClassSimpleDefRule());
                    						}
                    						set(
                    							current,
                    							"pogo6",
                    							lv_pogo6_10_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalPogoDsl.g:7346:3: (otherlv_11= 'parentClasses' otherlv_12= '{' ( (lv_parentClasses_13_0= ruleEString ) ) (otherlv_14= ',' ( (lv_parentClasses_15_0= ruleEString ) ) )* otherlv_16= '}' )?
            int alt175=2;
            int LA175_0 = input.LA(1);

            if ( (LA175_0==199) ) {
                alt175=1;
            }
            switch (alt175) {
                case 1 :
                    // InternalPogoDsl.g:7347:4: otherlv_11= 'parentClasses' otherlv_12= '{' ( (lv_parentClasses_13_0= ruleEString ) ) (otherlv_14= ',' ( (lv_parentClasses_15_0= ruleEString ) ) )* otherlv_16= '}'
                    {
                    otherlv_11=(Token)match(input,199,FOLLOW_3); 

                    				newLeafNode(otherlv_11, grammarAccess.getOneClassSimpleDefAccess().getParentClassesKeyword_7_0());
                    			
                    otherlv_12=(Token)match(input,12,FOLLOW_13); 

                    				newLeafNode(otherlv_12, grammarAccess.getOneClassSimpleDefAccess().getLeftCurlyBracketKeyword_7_1());
                    			
                    // InternalPogoDsl.g:7355:4: ( (lv_parentClasses_13_0= ruleEString ) )
                    // InternalPogoDsl.g:7356:5: (lv_parentClasses_13_0= ruleEString )
                    {
                    // InternalPogoDsl.g:7356:5: (lv_parentClasses_13_0= ruleEString )
                    // InternalPogoDsl.g:7357:6: lv_parentClasses_13_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getOneClassSimpleDefAccess().getParentClassesEStringParserRuleCall_7_2_0());
                    					
                    pushFollow(FOLLOW_6);
                    lv_parentClasses_13_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getOneClassSimpleDefRule());
                    						}
                    						add(
                    							current,
                    							"parentClasses",
                    							lv_parentClasses_13_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalPogoDsl.g:7374:4: (otherlv_14= ',' ( (lv_parentClasses_15_0= ruleEString ) ) )*
                    loop174:
                    do {
                        int alt174=2;
                        int LA174_0 = input.LA(1);

                        if ( (LA174_0==14) ) {
                            alt174=1;
                        }


                        switch (alt174) {
                    	case 1 :
                    	    // InternalPogoDsl.g:7375:5: otherlv_14= ',' ( (lv_parentClasses_15_0= ruleEString ) )
                    	    {
                    	    otherlv_14=(Token)match(input,14,FOLLOW_13); 

                    	    					newLeafNode(otherlv_14, grammarAccess.getOneClassSimpleDefAccess().getCommaKeyword_7_3_0());
                    	    				
                    	    // InternalPogoDsl.g:7379:5: ( (lv_parentClasses_15_0= ruleEString ) )
                    	    // InternalPogoDsl.g:7380:6: (lv_parentClasses_15_0= ruleEString )
                    	    {
                    	    // InternalPogoDsl.g:7380:6: (lv_parentClasses_15_0= ruleEString )
                    	    // InternalPogoDsl.g:7381:7: lv_parentClasses_15_0= ruleEString
                    	    {

                    	    							newCompositeNode(grammarAccess.getOneClassSimpleDefAccess().getParentClassesEStringParserRuleCall_7_3_1_0());
                    	    						
                    	    pushFollow(FOLLOW_6);
                    	    lv_parentClasses_15_0=ruleEString();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getOneClassSimpleDefRule());
                    	    							}
                    	    							add(
                    	    								current,
                    	    								"parentClasses",
                    	    								lv_parentClasses_15_0,
                    	    								"com.mncml.implementation.pogo.dsl.PogoDsl.EString");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop174;
                        }
                    } while (true);

                    otherlv_16=(Token)match(input,15,FOLLOW_177); 

                    				newLeafNode(otherlv_16, grammarAccess.getOneClassSimpleDefAccess().getRightCurlyBracketKeyword_7_4());
                    			

                    }
                    break;

            }

            // InternalPogoDsl.g:7404:3: (otherlv_17= 'inheritances' otherlv_18= '{' ( (lv_inheritances_19_0= ruleInheritance ) ) (otherlv_20= ',' ( (lv_inheritances_21_0= ruleInheritance ) ) )* otherlv_22= '}' )?
            int alt177=2;
            int LA177_0 = input.LA(1);

            if ( (LA177_0==53) ) {
                alt177=1;
            }
            switch (alt177) {
                case 1 :
                    // InternalPogoDsl.g:7405:4: otherlv_17= 'inheritances' otherlv_18= '{' ( (lv_inheritances_19_0= ruleInheritance ) ) (otherlv_20= ',' ( (lv_inheritances_21_0= ruleInheritance ) ) )* otherlv_22= '}'
                    {
                    otherlv_17=(Token)match(input,53,FOLLOW_3); 

                    				newLeafNode(otherlv_17, grammarAccess.getOneClassSimpleDefAccess().getInheritancesKeyword_8_0());
                    			
                    otherlv_18=(Token)match(input,12,FOLLOW_68); 

                    				newLeafNode(otherlv_18, grammarAccess.getOneClassSimpleDefAccess().getLeftCurlyBracketKeyword_8_1());
                    			
                    // InternalPogoDsl.g:7413:4: ( (lv_inheritances_19_0= ruleInheritance ) )
                    // InternalPogoDsl.g:7414:5: (lv_inheritances_19_0= ruleInheritance )
                    {
                    // InternalPogoDsl.g:7414:5: (lv_inheritances_19_0= ruleInheritance )
                    // InternalPogoDsl.g:7415:6: lv_inheritances_19_0= ruleInheritance
                    {

                    						newCompositeNode(grammarAccess.getOneClassSimpleDefAccess().getInheritancesInheritanceParserRuleCall_8_2_0());
                    					
                    pushFollow(FOLLOW_6);
                    lv_inheritances_19_0=ruleInheritance();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getOneClassSimpleDefRule());
                    						}
                    						add(
                    							current,
                    							"inheritances",
                    							lv_inheritances_19_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.Inheritance");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalPogoDsl.g:7432:4: (otherlv_20= ',' ( (lv_inheritances_21_0= ruleInheritance ) ) )*
                    loop176:
                    do {
                        int alt176=2;
                        int LA176_0 = input.LA(1);

                        if ( (LA176_0==14) ) {
                            alt176=1;
                        }


                        switch (alt176) {
                    	case 1 :
                    	    // InternalPogoDsl.g:7433:5: otherlv_20= ',' ( (lv_inheritances_21_0= ruleInheritance ) )
                    	    {
                    	    otherlv_20=(Token)match(input,14,FOLLOW_68); 

                    	    					newLeafNode(otherlv_20, grammarAccess.getOneClassSimpleDefAccess().getCommaKeyword_8_3_0());
                    	    				
                    	    // InternalPogoDsl.g:7437:5: ( (lv_inheritances_21_0= ruleInheritance ) )
                    	    // InternalPogoDsl.g:7438:6: (lv_inheritances_21_0= ruleInheritance )
                    	    {
                    	    // InternalPogoDsl.g:7438:6: (lv_inheritances_21_0= ruleInheritance )
                    	    // InternalPogoDsl.g:7439:7: lv_inheritances_21_0= ruleInheritance
                    	    {

                    	    							newCompositeNode(grammarAccess.getOneClassSimpleDefAccess().getInheritancesInheritanceParserRuleCall_8_3_1_0());
                    	    						
                    	    pushFollow(FOLLOW_6);
                    	    lv_inheritances_21_0=ruleInheritance();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getOneClassSimpleDefRule());
                    	    							}
                    	    							add(
                    	    								current,
                    	    								"inheritances",
                    	    								lv_inheritances_21_0,
                    	    								"com.mncml.implementation.pogo.dsl.PogoDsl.Inheritance");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop176;
                        }
                    } while (true);

                    otherlv_22=(Token)match(input,15,FOLLOW_178); 

                    				newLeafNode(otherlv_22, grammarAccess.getOneClassSimpleDefAccess().getRightCurlyBracketKeyword_8_4());
                    			

                    }
                    break;

            }

            // InternalPogoDsl.g:7462:3: (otherlv_23= 'additionalFiles' otherlv_24= '{' ( (lv_additionalFiles_25_0= ruleAdditionalFile ) ) (otherlv_26= ',' ( (lv_additionalFiles_27_0= ruleAdditionalFile ) ) )* otherlv_28= '}' )?
            int alt179=2;
            int LA179_0 = input.LA(1);

            if ( (LA179_0==38) ) {
                alt179=1;
            }
            switch (alt179) {
                case 1 :
                    // InternalPogoDsl.g:7463:4: otherlv_23= 'additionalFiles' otherlv_24= '{' ( (lv_additionalFiles_25_0= ruleAdditionalFile ) ) (otherlv_26= ',' ( (lv_additionalFiles_27_0= ruleAdditionalFile ) ) )* otherlv_28= '}'
                    {
                    otherlv_23=(Token)match(input,38,FOLLOW_3); 

                    				newLeafNode(otherlv_23, grammarAccess.getOneClassSimpleDefAccess().getAdditionalFilesKeyword_9_0());
                    			
                    otherlv_24=(Token)match(input,12,FOLLOW_41); 

                    				newLeafNode(otherlv_24, grammarAccess.getOneClassSimpleDefAccess().getLeftCurlyBracketKeyword_9_1());
                    			
                    // InternalPogoDsl.g:7471:4: ( (lv_additionalFiles_25_0= ruleAdditionalFile ) )
                    // InternalPogoDsl.g:7472:5: (lv_additionalFiles_25_0= ruleAdditionalFile )
                    {
                    // InternalPogoDsl.g:7472:5: (lv_additionalFiles_25_0= ruleAdditionalFile )
                    // InternalPogoDsl.g:7473:6: lv_additionalFiles_25_0= ruleAdditionalFile
                    {

                    						newCompositeNode(grammarAccess.getOneClassSimpleDefAccess().getAdditionalFilesAdditionalFileParserRuleCall_9_2_0());
                    					
                    pushFollow(FOLLOW_6);
                    lv_additionalFiles_25_0=ruleAdditionalFile();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getOneClassSimpleDefRule());
                    						}
                    						add(
                    							current,
                    							"additionalFiles",
                    							lv_additionalFiles_25_0,
                    							"com.mncml.implementation.pogo.dsl.PogoDsl.AdditionalFile");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalPogoDsl.g:7490:4: (otherlv_26= ',' ( (lv_additionalFiles_27_0= ruleAdditionalFile ) ) )*
                    loop178:
                    do {
                        int alt178=2;
                        int LA178_0 = input.LA(1);

                        if ( (LA178_0==14) ) {
                            alt178=1;
                        }


                        switch (alt178) {
                    	case 1 :
                    	    // InternalPogoDsl.g:7491:5: otherlv_26= ',' ( (lv_additionalFiles_27_0= ruleAdditionalFile ) )
                    	    {
                    	    otherlv_26=(Token)match(input,14,FOLLOW_41); 

                    	    					newLeafNode(otherlv_26, grammarAccess.getOneClassSimpleDefAccess().getCommaKeyword_9_3_0());
                    	    				
                    	    // InternalPogoDsl.g:7495:5: ( (lv_additionalFiles_27_0= ruleAdditionalFile ) )
                    	    // InternalPogoDsl.g:7496:6: (lv_additionalFiles_27_0= ruleAdditionalFile )
                    	    {
                    	    // InternalPogoDsl.g:7496:6: (lv_additionalFiles_27_0= ruleAdditionalFile )
                    	    // InternalPogoDsl.g:7497:7: lv_additionalFiles_27_0= ruleAdditionalFile
                    	    {

                    	    							newCompositeNode(grammarAccess.getOneClassSimpleDefAccess().getAdditionalFilesAdditionalFileParserRuleCall_9_3_1_0());
                    	    						
                    	    pushFollow(FOLLOW_6);
                    	    lv_additionalFiles_27_0=ruleAdditionalFile();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getOneClassSimpleDefRule());
                    	    							}
                    	    							add(
                    	    								current,
                    	    								"additionalFiles",
                    	    								lv_additionalFiles_27_0,
                    	    								"com.mncml.implementation.pogo.dsl.PogoDsl.AdditionalFile");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop178;
                        }
                    } while (true);

                    otherlv_28=(Token)match(input,15,FOLLOW_11); 

                    				newLeafNode(otherlv_28, grammarAccess.getOneClassSimpleDefAccess().getRightCurlyBracketKeyword_9_4());
                    			

                    }
                    break;

            }

            otherlv_29=(Token)match(input,15,FOLLOW_2); 

            			newLeafNode(otherlv_29, grammarAccess.getOneClassSimpleDefAccess().getRightCurlyBracketKeyword_10());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOneClassSimpleDef"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x000000000003A000L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x000000000000C000L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000038000L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000300000L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000028000L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000020000000000L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000000088000L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000000030L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000000400000L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x000000FFFF808000L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x000000FFFF008000L});
    public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x000000FFFE008000L});
    public static final BitSet FOLLOW_20 = new BitSet(new long[]{0x000000FFFC008000L});
    public static final BitSet FOLLOW_21 = new BitSet(new long[]{0x000000FFF8008000L});
    public static final BitSet FOLLOW_22 = new BitSet(new long[]{0x0000400000000000L});
    public static final BitSet FOLLOW_23 = new BitSet(new long[]{0x000000FFF0008000L});
    public static final BitSet FOLLOW_24 = new BitSet(new long[]{0x0100000000000000L});
    public static final BitSet FOLLOW_25 = new BitSet(new long[]{0x000000FFE0008000L});
    public static final BitSet FOLLOW_26 = new BitSet(new long[]{0x000000FFC0008000L});
    public static final BitSet FOLLOW_27 = new BitSet(new long[]{0x2000000000000000L});
    public static final BitSet FOLLOW_28 = new BitSet(new long[]{0x000000FF80008000L});
    public static final BitSet FOLLOW_29 = new BitSet(new long[]{0x000000FF00008000L});
    public static final BitSet FOLLOW_30 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000020L});
    public static final BitSet FOLLOW_31 = new BitSet(new long[]{0x000000FE00008000L});
    public static final BitSet FOLLOW_32 = new BitSet(new long[]{0x000000FC00008000L});
    public static final BitSet FOLLOW_33 = new BitSet(new long[]{0x0000000000000000L,0x0000000001000000L});
    public static final BitSet FOLLOW_34 = new BitSet(new long[]{0x000000F800008000L});
    public static final BitSet FOLLOW_35 = new BitSet(new long[]{0x0000000000000000L,0x0000000004000000L});
    public static final BitSet FOLLOW_36 = new BitSet(new long[]{0x000000F000008000L});
    public static final BitSet FOLLOW_37 = new BitSet(new long[]{0x0000000000000000L,0x0000000008000000L});
    public static final BitSet FOLLOW_38 = new BitSet(new long[]{0x000000E000008000L});
    public static final BitSet FOLLOW_39 = new BitSet(new long[]{0x0000000000000000L,0x0000000010000000L});
    public static final BitSet FOLLOW_40 = new BitSet(new long[]{0x000000C000008000L});
    public static final BitSet FOLLOW_41 = new BitSet(new long[]{0x0000000000000000L,0x0000000200000000L});
    public static final BitSet FOLLOW_42 = new BitSet(new long[]{0x0000008000008000L});
    public static final BitSet FOLLOW_43 = new BitSet(new long[]{0x0000000000000000L,0x0000000800000000L});
    public static final BitSet FOLLOW_44 = new BitSet(new long[]{0x0000010000000002L});
    public static final BitSet FOLLOW_45 = new BitSet(new long[]{0x00003C2009018000L});
    public static final BitSet FOLLOW_46 = new BitSet(new long[]{0x00003C2008018000L});
    public static final BitSet FOLLOW_47 = new BitSet(new long[]{0x0000382008018000L});
    public static final BitSet FOLLOW_48 = new BitSet(new long[]{0x0000382000018000L});
    public static final BitSet FOLLOW_49 = new BitSet(new long[]{0x0000302000018000L});
    public static final BitSet FOLLOW_50 = new BitSet(new long[]{0x0000202000018000L});
    public static final BitSet FOLLOW_51 = new BitSet(new long[]{0x0000002000018000L});
    public static final BitSet FOLLOW_52 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0000000000000010L});
    public static final BitSet FOLLOW_53 = new BitSet(new long[]{0x0000002000008000L});
    public static final BitSet FOLLOW_54 = new BitSet(new long[]{0x00FFBC0008008010L});
    public static final BitSet FOLLOW_55 = new BitSet(new long[]{0x00FFBC0000008010L});
    public static final BitSet FOLLOW_56 = new BitSet(new long[]{0x00FFB40000008010L});
    public static final BitSet FOLLOW_57 = new BitSet(new long[]{0x00FFB00000008010L});
    public static final BitSet FOLLOW_58 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x001C000000000000L});
    public static final BitSet FOLLOW_59 = new BitSet(new long[]{0x00FF300000008010L});
    public static final BitSet FOLLOW_60 = new BitSet(new long[]{0x00FF100000008010L});
    public static final BitSet FOLLOW_61 = new BitSet(new long[]{0x00FF000000008010L});
    public static final BitSet FOLLOW_62 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0xC000000000000000L});
    public static final BitSet FOLLOW_63 = new BitSet(new long[]{0x00FE000000008010L});
    public static final BitSet FOLLOW_64 = new BitSet(new long[]{0x00FC000000008010L});
    public static final BitSet FOLLOW_65 = new BitSet(new long[]{0x00F8000000008010L});
    public static final BitSet FOLLOW_66 = new BitSet(new long[]{0x00F0000000008010L});
    public static final BitSet FOLLOW_67 = new BitSet(new long[]{0x00E0000000008000L});
    public static final BitSet FOLLOW_68 = new BitSet(new long[]{0x0000000000000000L,0x0000002000000000L});
    public static final BitSet FOLLOW_69 = new BitSet(new long[]{0x00C0000000008000L});
    public static final BitSet FOLLOW_70 = new BitSet(new long[]{0x0000000000000000L,0x0000008000000000L});
    public static final BitSet FOLLOW_71 = new BitSet(new long[]{0x0080000000008000L});
    public static final BitSet FOLLOW_72 = new BitSet(new long[]{0x0000000000000000L,0x0004000000000000L});
    public static final BitSet FOLLOW_73 = new BitSet(new long[]{0x1E00000008008000L});
    public static final BitSet FOLLOW_74 = new BitSet(new long[]{0x1C00000008008000L});
    public static final BitSet FOLLOW_75 = new BitSet(new long[]{0x1C00000000008000L});
    public static final BitSet FOLLOW_76 = new BitSet(new long[]{0x1800000000008000L});
    public static final BitSet FOLLOW_77 = new BitSet(new long[]{0x0000000000000000L,0xF810000000000000L,0x00000000000003FFL});
    public static final BitSet FOLLOW_78 = new BitSet(new long[]{0x1000000000008000L});
    public static final BitSet FOLLOW_79 = new BitSet(new long[]{0x0000000000000000L,0x0020000000000000L});
    public static final BitSet FOLLOW_80 = new BitSet(new long[]{0xD000000008008010L,0x000000000000001FL});
    public static final BitSet FOLLOW_81 = new BitSet(new long[]{0xD000000000008010L,0x000000000000001FL});
    public static final BitSet FOLLOW_82 = new BitSet(new long[]{0x9000000000008010L,0x000000000000001FL});
    public static final BitSet FOLLOW_83 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0060000000000000L});
    public static final BitSet FOLLOW_84 = new BitSet(new long[]{0x1000000000008000L,0x000000000000001FL});
    public static final BitSet FOLLOW_85 = new BitSet(new long[]{0x1000000000008000L,0x000000000000001EL});
    public static final BitSet FOLLOW_86 = new BitSet(new long[]{0x1000000000008000L,0x000000000000001CL});
    public static final BitSet FOLLOW_87 = new BitSet(new long[]{0x1000000000008000L,0x0000000000000018L});
    public static final BitSet FOLLOW_88 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000400L});
    public static final BitSet FOLLOW_89 = new BitSet(new long[]{0x1000000000008000L,0x0000000000000010L});
    public static final BitSet FOLLOW_90 = new BitSet(new long[]{0x9000000000008000L,0x0000000000FFFFC3L});
    public static final BitSet FOLLOW_91 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0380000000000000L});
    public static final BitSet FOLLOW_92 = new BitSet(new long[]{0x9000000000008000L,0x0000000000FFFF83L});
    public static final BitSet FOLLOW_93 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x3C00000000000000L});
    public static final BitSet FOLLOW_94 = new BitSet(new long[]{0x9000000000008000L,0x0000000000FFFF03L});
    public static final BitSet FOLLOW_95 = new BitSet(new long[]{0x1000000000008000L,0x0000000000FFFF03L});
    public static final BitSet FOLLOW_96 = new BitSet(new long[]{0x1000000000008000L,0x0000000000FFFF02L});
    public static final BitSet FOLLOW_97 = new BitSet(new long[]{0x1000000000008000L,0x0000000000FFFE02L});
    public static final BitSet FOLLOW_98 = new BitSet(new long[]{0x1000000000008000L,0x0000000000FFFC02L});
    public static final BitSet FOLLOW_99 = new BitSet(new long[]{0x1000000000008000L,0x0000000000FFF802L});
    public static final BitSet FOLLOW_100 = new BitSet(new long[]{0x1000000000008000L,0x0000000000FFF002L});
    public static final BitSet FOLLOW_101 = new BitSet(new long[]{0x1000000000008000L,0x0000000000FFE002L});
    public static final BitSet FOLLOW_102 = new BitSet(new long[]{0x1000000000008000L,0x0000000000FFC002L});
    public static final BitSet FOLLOW_103 = new BitSet(new long[]{0x1000000000008000L,0x0000000000FFC000L});
    public static final BitSet FOLLOW_104 = new BitSet(new long[]{0x1000000000008000L,0x0000000000FF8000L});
    public static final BitSet FOLLOW_105 = new BitSet(new long[]{0x1000000000008000L,0x0000000000FF0000L});
    public static final BitSet FOLLOW_106 = new BitSet(new long[]{0x1000000000008000L,0x0000000000FE0000L});
    public static final BitSet FOLLOW_107 = new BitSet(new long[]{0x0000000000000000L,0xE000000000000000L,0x00000003FFFFF81FL});
    public static final BitSet FOLLOW_108 = new BitSet(new long[]{0x1000000000008000L,0x0000000000FC0000L});
    public static final BitSet FOLLOW_109 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000400000000L});
    public static final BitSet FOLLOW_110 = new BitSet(new long[]{0x1000000000008000L,0x0000000000F80000L});
    public static final BitSet FOLLOW_111 = new BitSet(new long[]{0x1000000000008000L,0x0000000000F00000L});
    public static final BitSet FOLLOW_112 = new BitSet(new long[]{0x1000000000008000L,0x0000000000E00000L});
    public static final BitSet FOLLOW_113 = new BitSet(new long[]{0x0000000000008000L,0x0000000000E00000L});
    public static final BitSet FOLLOW_114 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000002000000000L});
    public static final BitSet FOLLOW_115 = new BitSet(new long[]{0x0000000000008000L,0x0000000000C00000L});
    public static final BitSet FOLLOW_116 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0000000000000001L});
    public static final BitSet FOLLOW_117 = new BitSet(new long[]{0x0000000000008000L,0x0000000000800000L});
    public static final BitSet FOLLOW_118 = new BitSet(new long[]{0x1000000000008000L,0x0000000002000000L});
    public static final BitSet FOLLOW_119 = new BitSet(new long[]{0x8000000008008010L,0x0000000002018080L});
    public static final BitSet FOLLOW_120 = new BitSet(new long[]{0x8000000000008010L,0x0000000002018080L});
    public static final BitSet FOLLOW_121 = new BitSet(new long[]{0x8000000000008010L,0x0000000000018080L});
    public static final BitSet FOLLOW_122 = new BitSet(new long[]{0x8000000000008010L,0x0000000000018000L});
    public static final BitSet FOLLOW_123 = new BitSet(new long[]{0x0000000000008000L,0x0000000000018000L});
    public static final BitSet FOLLOW_124 = new BitSet(new long[]{0x0000000000008000L,0x0000000000010000L});
    public static final BitSet FOLLOW_125 = new BitSet(new long[]{0x1000000008008000L});
    public static final BitSet FOLLOW_126 = new BitSet(new long[]{0x0000000000008000L,0x00000001E0000000L});
    public static final BitSet FOLLOW_127 = new BitSet(new long[]{0x0000000000008000L,0x00000001C0000000L});
    public static final BitSet FOLLOW_128 = new BitSet(new long[]{0x0000000000008000L,0x0000000180000000L});
    public static final BitSet FOLLOW_129 = new BitSet(new long[]{0x0000000000008000L,0x0000000100000000L});
    public static final BitSet FOLLOW_130 = new BitSet(new long[]{0x0000000000008000L,0x0000000400000000L});
    public static final BitSet FOLLOW_131 = new BitSet(new long[]{0x0800000000008000L,0x0000001000000000L});
    public static final BitSet FOLLOW_132 = new BitSet(new long[]{0x0000000000008000L,0x0000001000000000L});
    public static final BitSet FOLLOW_133 = new BitSet(new long[]{0x0000040000008000L,0x0000004000000000L});
    public static final BitSet FOLLOW_134 = new BitSet(new long[]{0x0000040000008000L});
    public static final BitSet FOLLOW_135 = new BitSet(new long[]{0x0000000000008000L,0x0003FF0000000000L});
    public static final BitSet FOLLOW_136 = new BitSet(new long[]{0x0000000000008000L,0x0003FE0000000000L});
    public static final BitSet FOLLOW_137 = new BitSet(new long[]{0x0000000000008000L,0x0003FC0000000000L});
    public static final BitSet FOLLOW_138 = new BitSet(new long[]{0x0000000000008000L,0x0003F80000000000L});
    public static final BitSet FOLLOW_139 = new BitSet(new long[]{0x0000000000008000L,0x0003F00000000000L});
    public static final BitSet FOLLOW_140 = new BitSet(new long[]{0x0000000000008000L,0x0003E00000000000L});
    public static final BitSet FOLLOW_141 = new BitSet(new long[]{0x0000000000008000L,0x0003C00000000000L});
    public static final BitSet FOLLOW_142 = new BitSet(new long[]{0x0000000000008000L,0x0003800000000000L});
    public static final BitSet FOLLOW_143 = new BitSet(new long[]{0x0000000000008000L,0x0003000000000000L});
    public static final BitSet FOLLOW_144 = new BitSet(new long[]{0x0000000000008000L,0x0002000000000000L});
    public static final BitSet FOLLOW_145 = new BitSet(new long[]{0x0000000000008000L,0x0008000000000000L});
    public static final BitSet FOLLOW_146 = new BitSet(new long[]{0x0000000000008000L,0x07C0000000000000L});
    public static final BitSet FOLLOW_147 = new BitSet(new long[]{0x0000000000008000L,0x0780000000000000L});
    public static final BitSet FOLLOW_148 = new BitSet(new long[]{0x0000000000008000L,0x0700000000000000L});
    public static final BitSet FOLLOW_149 = new BitSet(new long[]{0x0000000000008000L,0x0600000000000000L});
    public static final BitSet FOLLOW_150 = new BitSet(new long[]{0x0000000000008000L,0x0400000000000000L});
    public static final BitSet FOLLOW_151 = new BitSet(new long[]{0x0800000008008000L});
    public static final BitSet FOLLOW_152 = new BitSet(new long[]{0x0800000000008000L});
    public static final BitSet FOLLOW_153 = new BitSet(new long[]{0x0000000000008000L,0x0000000000000000L,0x0000001800000000L});
    public static final BitSet FOLLOW_154 = new BitSet(new long[]{0x0000000000008000L,0x0000000000000000L,0x0000001000000000L});
    public static final BitSet FOLLOW_155 = new BitSet(new long[]{0x0000000008008000L,0x0000000002000000L,0x0003FFC000000000L});
    public static final BitSet FOLLOW_156 = new BitSet(new long[]{0x0000000000008000L,0x0000000002000000L,0x0003FFC000000000L});
    public static final BitSet FOLLOW_157 = new BitSet(new long[]{0x0000000000008000L,0x0000000000000000L,0x0003FFC000000000L});
    public static final BitSet FOLLOW_158 = new BitSet(new long[]{0x0000000000008000L,0x0000000000000000L,0x0003FF8000000000L});
    public static final BitSet FOLLOW_159 = new BitSet(new long[]{0x0000000000008000L,0x0000000000000000L,0x0003FF0000000000L});
    public static final BitSet FOLLOW_160 = new BitSet(new long[]{0x0000000000008000L,0x0000000000000000L,0x0003FE0000000000L});
    public static final BitSet FOLLOW_161 = new BitSet(new long[]{0x0000000000008000L,0x0000000000000000L,0x0003FC0000000000L});
    public static final BitSet FOLLOW_162 = new BitSet(new long[]{0x0000000000008000L,0x0000000000000000L,0x0003F80000000000L});
    public static final BitSet FOLLOW_163 = new BitSet(new long[]{0x0000000000008000L,0x0000000000000000L,0x0003F00000000000L});
    public static final BitSet FOLLOW_164 = new BitSet(new long[]{0x0000000000008000L,0x0000000000000000L,0x0003E00000000000L});
    public static final BitSet FOLLOW_165 = new BitSet(new long[]{0x0000000000008000L,0x0000000000000000L,0x0003C00000000000L});
    public static final BitSet FOLLOW_166 = new BitSet(new long[]{0x0000000000008000L,0x0000000000000000L,0x0003800000000000L});
    public static final BitSet FOLLOW_167 = new BitSet(new long[]{0x0000000000008000L,0x0000000000000000L,0x0003000000000000L});
    public static final BitSet FOLLOW_168 = new BitSet(new long[]{0x0000000000008000L,0x0000000000000000L,0x0002000000000000L});
    public static final BitSet FOLLOW_169 = new BitSet(new long[]{0x0000000000008000L,0x0000000000000000L,0x0000000000000000L,0x000000000000000EL});
    public static final BitSet FOLLOW_170 = new BitSet(new long[]{0x0000000000008000L,0x0000000000000000L,0x0000000000000000L,0x000000000000000CL});
    public static final BitSet FOLLOW_171 = new BitSet(new long[]{0x0000000000008000L,0x0000000000000000L,0x0000000000000000L,0x0000000000000008L});
    public static final BitSet FOLLOW_172 = new BitSet(new long[]{0x0020044000008000L,0x0000004000000000L,0x0000000000000000L,0x00000000000000E0L});
    public static final BitSet FOLLOW_173 = new BitSet(new long[]{0x0020044000008000L,0x0000000000000000L,0x0000000000000000L,0x00000000000000E0L});
    public static final BitSet FOLLOW_174 = new BitSet(new long[]{0x0020004000008000L,0x0000000000000000L,0x0000000000000000L,0x00000000000000E0L});
    public static final BitSet FOLLOW_175 = new BitSet(new long[]{0x0020004000008000L,0x0000000000000000L,0x0000000000000000L,0x00000000000000C0L});
    public static final BitSet FOLLOW_176 = new BitSet(new long[]{0x0020004000008000L,0x0000000000000000L,0x0000000000000000L,0x0000000000000080L});
    public static final BitSet FOLLOW_177 = new BitSet(new long[]{0x0020004000008000L});
    public static final BitSet FOLLOW_178 = new BitSet(new long[]{0x0000004000008000L});

}