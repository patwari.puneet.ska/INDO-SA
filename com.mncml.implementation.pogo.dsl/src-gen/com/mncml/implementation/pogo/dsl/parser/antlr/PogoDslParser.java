/*
 * generated by Xtext 2.11.0
 */
package com.mncml.implementation.pogo.dsl.parser.antlr;

import com.google.inject.Inject;
import com.mncml.implementation.pogo.dsl.parser.antlr.internal.InternalPogoDslParser;
import com.mncml.implementation.pogo.dsl.services.PogoDslGrammarAccess;
import org.eclipse.xtext.parser.antlr.AbstractAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;

public class PogoDslParser extends AbstractAntlrParser {

	@Inject
	private PogoDslGrammarAccess grammarAccess;

	@Override
	protected void setInitialHiddenTokens(XtextTokenStream tokenStream) {
		tokenStream.setInitialHiddenTokens("RULE_WS", "RULE_ML_COMMENT", "RULE_SL_COMMENT");
	}
	

	@Override
	protected InternalPogoDslParser createParser(XtextTokenStream stream) {
		return new InternalPogoDslParser(stream, getGrammarAccess());
	}

	@Override 
	protected String getDefaultRuleName() {
		return "PogoSystem";
	}

	public PogoDslGrammarAccess getGrammarAccess() {
		return this.grammarAccess;
	}

	public void setGrammarAccess(PogoDslGrammarAccess grammarAccess) {
		this.grammarAccess = grammarAccess;
	}
}
