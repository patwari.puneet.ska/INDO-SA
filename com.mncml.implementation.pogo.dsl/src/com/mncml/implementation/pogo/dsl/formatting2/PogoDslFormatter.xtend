/*
 * generated by Xtext 2.11.0
 */
package com.mncml.implementation.pogo.dsl.formatting2

import com.google.inject.Inject
import com.mncml.implementation.pogo.dsl.services.PogoDslGrammarAccess
import org.eclipse.xtext.formatting2.AbstractFormatter2
import org.eclipse.xtext.formatting2.IFormattableDocument
import pogoDsl.AdditionalFile
import pogoDsl.Attribute
import pogoDsl.Command
import pogoDsl.ForwardedAttribute
import pogoDsl.Import
import pogoDsl.OverlodedPollPeriodObject
import pogoDsl.Pipe
import pogoDsl.PogoDeviceClass
import pogoDsl.PogoMultiClasses
import pogoDsl.PogoSystem
import pogoDsl.Property
import pogoDsl.State
import pogoDsl.PogoDslPackage

class PogoDslFormatter extends AbstractFormatter2{
	
	@Inject extension PogoDslGrammarAccess

	def dispatch void format(PogoSystem pogoSystem, extension IFormattableDocument document) {
		// TODO: format HiddenRegions around keywords, attributes, cross references, etc. 
//		pogoSystem.format.regionFor.keyword('{').append[newLine];
//		pogoSystem.format.regionFor.keyword('}').prepend[newLine];
	  	
		for (Import _import : pogoSystem.getImports()) {
//			_import.format.regionFor.keyword('{').append[newLine];
		}
		for (PogoDeviceClass pogoDeviceClass : pogoSystem.getClasses()) {
			pogoDeviceClass.format;
		}
		for (PogoMultiClasses pogoMultiClasses : pogoSystem.getMultiClasses()) {
			pogoMultiClasses.format;
		}
	}

	def dispatch void format(PogoDeviceClass pogoDeviceClass, extension IFormattableDocument document) {
		// TODO: format HiddenRegions around keywords, attributes, cross references, etc. 
		pogoDeviceClass.getDescription.identification.format.regionFor.keyword('ClassIdentification {').append[newLine];
		pogoDeviceClass.getDescription.identification.format.regionFor.keyword('}').append[newLine];
		
		pogoDeviceClass.getDescription.format.regionFor.keyword('commands {').append[newLine];
		pogoDeviceClass.getDescription.format.regionFor.keyword('}').append[newLine];
		
		for (Property property : pogoDeviceClass.getClassProperties()) {
		
			pogoDeviceClass.format.regionFor.keyword('{').append[newLine];
			pogoDeviceClass.format.regionFor.keyword('}').prepend[newLine];
		}
		for (Property property : pogoDeviceClass.getDeviceProperties()) {
			
			pogoDeviceClass.format.regionFor.keyword('{').append[newLine];
			pogoDeviceClass.format.regionFor.keyword('}').prepend[newLine];
		}
		for (Command command : pogoDeviceClass.getCommands()) {
		
			command.format.regionFor.keyword('{').append[commandRule.name newLine];
			command.format.regionFor.keyword('}').append[newLine];
		   	
			command.argin.format.regionFor.keyword('Argument {').append[newLine];
			command.argin.format.regionFor.keyword('}').prepend[newLine];	
			
			command.argout.format.regionFor.keyword('Argument {').append[newLine];
			command.argout.format.regionFor.keyword('}').prepend[newLine];			
		}
		for (Command command : pogoDeviceClass.getDynamicCommands()) {
			
			command.format.regionFor.keyword('{').append[newLine];
			command.format.regionFor.keyword('}').prepend[newLine];
		}
		for (Attribute attribute : pogoDeviceClass.getAttributes()) {
		
			attribute.format.regionFor.keyword('{').append[newLine];
			attribute.format.regionFor.keyword('}').prepend[newLine];
		}
		for (Attribute attribute : pogoDeviceClass.getDynamicAttributes()) {
			
			attribute.format.regionFor.keyword('{').append[newLine];
			attribute.format.regionFor.keyword('}').prepend[newLine];
		}
		for (ForwardedAttribute forwardedAttribute : pogoDeviceClass.getForwardedAttributes()) {
			
			forwardedAttribute.format.regionFor.keyword('{').append[newLine];
			forwardedAttribute.format.regionFor.keyword('}').prepend[newLine];
		}
		for (Pipe pipe : pogoDeviceClass.getPipes()) {
		
			pipe.format.regionFor.keyword('{').append[newLine];
			pipe.format.regionFor.keyword('}').prepend[newLine];
		}
		for (State state : pogoDeviceClass.getStates()) {
			
			state.format.regionFor.keyword('{').append[newLine];
			state.format.regionFor.keyword('}').prepend[newLine];
		}
		pogoDeviceClass.getPreferences.format;
		for (AdditionalFile additionalFile : pogoDeviceClass.getAdditionalFiles()) {
		
			additionalFile.format.regionFor.keyword('{').append[newLine];
			additionalFile.format.regionFor.keyword('}').prepend[newLine];
		}
		for (OverlodedPollPeriodObject overlodedPollPeriodObject : pogoDeviceClass.getOverlodedPollPeriodObject()) {
			
			overlodedPollPeriodObject.format.regionFor.keyword('{').append[newLine];
			overlodedPollPeriodObject.format.regionFor.keyword('}').prepend[newLine];
		}
	}
		
	// TODO: implement for PogoMultiClasses, ClassDescription, Property, Command, Attribute, ForwardedAttribute, State, Argument, OneClassSimpleDef
}
