package com.mncml.implementation.pogo.dsl.formatting2

import org.eclipse.xtext.formatting.impl.AbstractDeclarativeFormatter
import com.google.inject.Inject
import com.mncml.implementation.pogo.dsl.services.PogoDslGrammarAccess
import org.eclipse.xtext.formatting.impl.FormattingConfig

class PogoDslFormatter2 extends AbstractDeclarativeFormatter {
	
	@Inject extension PogoDslGrammarAccess
	
	override protected configureFormatting(FormattingConfig c) {
		for(pair: findKeywordPairs('{', '}')) {
			c.setIndentation(pair.first, pair.second)
			c.setLinewrap(1).after(pair.first)
			c.setLinewrap(1).before(pair.second)
			c.setLinewrap(1).after(pair.second)
		}
		for(comma: findKeywords(',')) {
			c.setNoLinewrap().before(comma)
			c.setNoSpace().before(comma)
			c.setLinewrap().after(comma)
		}
		c.setLinewrap(0, 1, 2).before(SL_COMMENTRule)
		c.setLinewrap(0, 1, 2).before(ML_COMMENTRule)
		c.setLinewrap(0, 1, 1).after(ML_COMMENTRule)
	}
		
}