package com.mncml.implementation.pogo.dsl.scoping

import org.eclipse.xtext.resource.impl.DefaultResourceDescriptionStrategy
import org.eclipse.emf.ecore.EObject
import org.eclipse.xtext.util.IAcceptor
import org.eclipse.xtext.resource.IEObjectDescription
import org.eclipse.xtext.resource.EObjectDescription
import mncModel.ControlNode

class PogoResourceDescriptionStrategy extends DefaultResourceDescriptionStrategy {
    
    override public boolean createEObjectDescriptions(EObject eObject,
            IAcceptor<IEObjectDescription> acceptor) {
        if (eObject instanceof ControlNode) {
            var fullyQualifiedName = getQualifiedNameProvider().getFullyQualifiedName(eObject);
            var ieObjectDescription = EObjectDescription.create(fullyQualifiedName, eObject);
            acceptor.accept(ieObjectDescription);
            return true;
        }
        return super.createEObjectDescriptions(eObject, acceptor);
    }
}