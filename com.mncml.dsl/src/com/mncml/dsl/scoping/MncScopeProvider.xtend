/*
 * generated by Xtext
 */
package com.mncml.dsl.scoping

import static extension org.eclipse.xtext.EcoreUtil2.*
import com.google.inject.Inject
import org.eclipse.xtext.naming.IQualifiedNameProvider
import mncModel.OperatingState
import mncModel.ControlNode
import java.util.List
import mncModel.Transition
import org.eclipse.emf.ecore.EReference
import org.eclipse.xtext.scoping.Scopes
import com.google.common.base.Function
import org.eclipse.xtext.naming.QualifiedName
import org.eclipse.xtext.scoping.IScope
import mncModel.InterfaceDescription
import mncModel.Event
import mncModel.Response
import mncModel.Alarm
import mncModel.Command
import mncModel.Action
import mncModel.Model
import mncModel.ResponseValidation
import mncModel.CheckParameterCondition
import mncModel.CommandValidation
import mncModel.DataPoint
import mncModel.CommandResponseBlock
import mncModel.ParameterTranslation
import mncModel.Parameter
import mncModel.ResponseTranslationRule
import mncModel.CommandDistribution
import mncModel.CommandTranslation
import mncModel.impl.ControlNodeImpl
import mncModel.ResponseBlock
import mncModel.ResponsibleItemList
import mncModel.AlarmBlock
import mncModel.EventBlock
import mncModel.DataPointBlock

/**
 * This class contains custom scoping description.
 * 
 * see : http://www.eclipse.org/Xtext/documentation.html#scoping
 * on how and when to use it 
 * 
 */
class MncScopeProvider extends org.eclipse.xtext.scoping.impl.AbstractDeclarativeScopeProvider {

	@Inject
	IQualifiedNameProvider qualifiedNameProvider

	def getCandidateOperatingStatesForTransitions(Transition t) {
		var List<OperatingState> os = newArrayList()
		var id = t.getContainerOfType(ControlNode).interfaceDescription
		if (id.operatingStates !== null)
			os.addAll(id.operatingStates.operatingStates)
		for (ids : id.uses)
			if (ids.operatingStates !== null)
				os.addAll(ids.operatingStates.operatingStates)
		os
	} 
 
	def List<Command> getCandidateCommands(ControlNode cn) {
		var List<Command> c = newArrayList()
		var id = cn.interfaceDescription

		if (id.commands !== null)
			c.addAll(id.commands.commands)

		for (ids : id.uses) {
			if (ids.commands !== null)
				c.addAll(ids.commands.commands)
		}
		if (cn.childNodes !== null && cn.childNodes.size > 0) {
			var List<Command> temp = newArrayList()
			for (chcn : cn.childNodes) {
				temp.addAll(getCandidateCommands(chcn))
			}
			c.addAll(temp)
		}

		c
	}

	def getCandidateAlarms(InterfaceDescription id) {
		var List<Alarm> r = newArrayList()
		if (id.alarms !== null)
			r.addAll(id.alarms.alarms)
		for (ids : id.uses)
			if (ids.alarms !== null)
				r.addAll(ids.alarms.alarms)
		r
	}

	def getCandidateEvents(InterfaceDescription id) {
		var List<Event> e = newArrayList()
		if (id.events !== null)
			e.addAll(id.events.events)
		for (ids : id.uses)
			if (ids.events !== null)
				e.addAll(ids.events.events)
		e
	}

	def getCandidateResponses(InterfaceDescription id) {
		var List<Response> r = newArrayList()
		if (id.responses !== null)
			r.addAll(id.responses.responses)
		for (ids : id.uses)
			if (ids.responses !== null)
				r.addAll(ids.responses.responses)
		r
	}

	def getCandidateDataPoints(InterfaceDescription id) {
		var List<DataPoint> d = newArrayList()
		if (id.dataPoints !== null)
			d.addAll(id.dataPoints.dataPoints)
		for (ids : id.uses)
			if (ids.dataPoints !== null)
				d.addAll(ids.dataPoints.dataPoints)
		d
	}

	def getCandidateChildNodes(ControlNode node) {
		var List<ControlNode> cn = newArrayList()
		var model = node.getContainerOfType(Model)
		for (s : model.systems) {
			if (s instanceof ControlNode && s != node)
				cn.add(s as ControlNode)
		}
		cn
	}

	def getCandidateResponsesFromChildNode(ControlNode cn) {
		var List<Response> r = newArrayList()
		for (ccn : cn.childNodes) {
			if (ccn.interfaceDescription.responses !== null)
				r.addAll(ccn.interfaceDescription.responses.responses)
			for (ids : ccn.interfaceDescription.uses)
				r.addAll(ids.responses.responses)
		}
		r
	}

	def getCandidateCommandsFromChildNode(ControlNode cn) {
		var List<Command> c = newArrayList()
		for (ccn : cn.childNodes) {
			if (ccn.interfaceDescription.commands !== null)
				c.addAll(ccn.interfaceDescription.commands.commands)
			for (ids : ccn.interfaceDescription.uses)
				c.addAll(ids.commands.commands)
		}
		c
	}

	def scope_Transition_currentState(Transition t, EReference ref) {
		Scopes.scopeFor(
			getCandidateOperatingStatesForTransitions(t),
			new Function<OperatingState, QualifiedName> {

				override apply(OperatingState input) {
					qualifiedNameProvider.getFullyQualifiedName(input)
				}

				override equals(Object object) {
					throw new UnsupportedOperationException("TODO: auto-generated method stub")
				}

			},
			IScope.NULLSCOPE
		)
	}

	def scope_Transition_nextState(Transition t, EReference ref) {
		Scopes.scopeFor(
			getCandidateOperatingStatesForTransitions(t),
			new Function<OperatingState, QualifiedName> {

				override apply(OperatingState input) {
					qualifiedNameProvider.getFullyQualifiedName(input)
				}

				override equals(Object object) {
					throw new UnsupportedOperationException("TODO: auto-generated method stub")
				}

			},
			IScope.NULLSCOPE
		)
	}

	def scope_Action_command(Action a, EReference ref) {
//		println(a.alarm.get(0).name)
		Scopes.scopeFor(
			getCandidateCommands(a.getContainerOfType(ControlNode)),
			new Function<Command, QualifiedName> {

				override apply(Command input) {
					qualifiedNameProvider.getFullyQualifiedName(input)
				}

				override equals(Object object) {
					throw new UnsupportedOperationException("TODO: auto-generated method stub")
				}

			},
			IScope.NULLSCOPE
		)
	}

	def scope_ResponseBlock_response(ResponseBlock rb, EReference ref) {

		Scopes.scopeFor(
			getCandidateResponses(rb.getContainerOfType(typeof(ControlNode)).interfaceDescription),
			new Function<Response, QualifiedName> {

				override apply(Response input) {
					qualifiedNameProvider.getFullyQualifiedName(input)
				}

				override equals(Object object) {
					throw new UnsupportedOperationException("TODO: auto-generated method stub")
				}

			},
			IScope.NULLSCOPE
		)
	}

	def scope_CheckParameterCondition_parameter(CheckParameterCondition cpc, EReference ref) {
		if (cpc.getContainerOfType(CommandValidation) != null)
			Scopes.scopeFor(cpc.getContainerOfType(CommandResponseBlock).command.parameters)
		else
			Scopes.scopeFor(cpc.getContainerOfType(ResponseValidation).response.parameters)
	}

	def scope_CommandTranslation_translatedCommands(CommandTranslation ct, EReference ref) {
		Scopes.scopeFor(
			getCandidateCommandsFromChildNode(ct.getContainerOfType(ControlNode)),
			new Function<Command, QualifiedName> {

				override apply(Command input) {
					qualifiedNameProvider.getFullyQualifiedName(input)
				}

				override equals(Object object) {
					throw new UnsupportedOperationException("TODO: auto-generated method stub")
				}

			},
			IScope.NULLSCOPE
		)
	}

//	def scope_AlarmCondition_checkDataPoint(AlarmCondition ac, EReference ref) {
//		Scopes.scopeFor(
//			getCandidateDataPoints(ac.getContainerOfType(typeof(ControlNode)).interfaceDescription),
//			new Function<DataPoint, QualifiedName> {
//
//				override apply(DataPoint input) {
//					qualifiedNameProvider.getFullyQualifiedName(input)
//				}
//
//				override equals(Object object) {
//					throw new UnsupportedOperationException("TODO: auto-generated method stub")
//				}
//
//			},
//			IScope.NULLSCOPE
//		)
//	}
	def scope_CommandResponseBlock_command(CommandResponseBlock crb, EReference ref) {
		Scopes.scopeFor(
			getCandidateCommands(crb.getContainerOfType(ControlNode)),
			new Function<Command, QualifiedName> {

				override apply(Command input) {
					qualifiedNameProvider.getFullyQualifiedName(input)
				}

				override equals(Object object) {
					throw new UnsupportedOperationException("TODO: auto-generated method stub")
				}

			},
			IScope.NULLSCOPE
		)

	}

	def scope_CommandResponseBlock_triggeredEvents(CommandResponseBlock crb, EReference ref) {
		Scopes.scopeFor(
			getCandidateEvents(crb.getContainerOfType(ControlNode).interfaceDescription),
			new Function<Event, QualifiedName> {

				override apply(Event input) {
					qualifiedNameProvider.getFullyQualifiedName(input)
				}

				override equals(Object object) {
					throw new UnsupportedOperationException("TODO: auto-generated method stub")
				}

			},
			IScope.NULLSCOPE
		)
	}

	def scope_ParameterTranslation_inputParameters(ParameterTranslation pt, EReference ref) {
		if (pt.getContainerOfType(CommandTranslation) != null) {
			Scopes.scopeFor(pt.getContainerOfType(CommandResponseBlock).command.parameters) // Use Hyperlink to link to actual element
		} else {
			var List<Parameter> p = newArrayList()
			for (r : pt.getContainerOfType(ResponseTranslationRule).inputResponses) {
				p.addAll(r.parameters)
			}
			Scopes.scopeFor(
				p,
				new Function<Parameter, QualifiedName> {

					override apply(Parameter input) {
						qualifiedNameProvider.getFullyQualifiedName(input)
					}

					override equals(Object object) {
						throw new UnsupportedOperationException("TODO: auto-generated method stub")
					}

				},
				IScope.NULLSCOPE
			)
		}
	}

	def scope_ParameterTranslation_translatedParameters(ParameterTranslation pt, EReference ref) {
		var List<Parameter> p = newArrayList()
		if (pt.getContainerOfType(CommandTranslation) != null) {
			for (c : pt.getContainerOfType(CommandTranslation).translatedCommands) {
				p.addAll(c.parameters)
			}
			Scopes.scopeFor(
				p,
				new Function<Parameter, QualifiedName> {

					override apply(Parameter input) {
						qualifiedNameProvider.getFullyQualifiedName(input)
					}

					override equals(Object object) {
						throw new UnsupportedOperationException("TODO: auto-generated method stub")
					}

				},
				IScope.NULLSCOPE
			)
		} else {
//			var Response r = newArrayList()
//			for (rv : pt.getContainerOfType(ResponseBlock).response)
//				r.add(rv)
//			for (res : r) {
			p.addAll(pt.getContainerOfType(ResponseBlock).response.parameters)
//			}
			Scopes.scopeFor(
				p,
				new Function<Parameter, QualifiedName> {

					override apply(Parameter input) {
						qualifiedNameProvider.getFullyQualifiedName(input)
					}

					override equals(Object object) {
						throw new UnsupportedOperationException("TODO: auto-generated method stub")
					}

				},
				IScope.NULLSCOPE
			)

		}
	}

	def scope_CommandDistribution_command(CommandDistribution cd, EReference ref) {
		Scopes.scopeFor(
			cd.getContainerOfType(CommandResponseBlock).commandTranslation.translatedCommands,
			new Function<Command, QualifiedName> {

				override apply(Command input) {
					qualifiedNameProvider.getFullyQualifiedName(input)
				}

				override equals(Object object) {
					throw new UnsupportedOperationException("TODO: auto-generated method stub")
				}

			},
			IScope.NULLSCOPE
		)
	}

	def scope_CommandDistribution_destinationNodes(CommandDistribution cd, EReference ref) {
		Scopes.scopeFor(cd.getContainerOfType(ControlNode).childNodes)
	}

	def scope_ResponseTranslationRule_inputResponses(ResponseTranslationRule rtr, EReference ref) {
		Scopes.scopeFor(
			getCandidateResponsesFromChildNode(rtr.getContainerOfType(ControlNode)),
			new Function<Response, QualifiedName> {

				override apply(Response input) {
					qualifiedNameProvider.getFullyQualifiedName(input)
				}

				override equals(Object object) {
					throw new UnsupportedOperationException("TODO: auto-generated method stub")
				}

			},
			IScope.NULLSCOPE
		)
	}

	def scope_ResponsibleItemList_responsibleCommands(ResponsibleItemList ril, EReference ref) {
		Scopes.scopeFor(
			getCandidateCommands(ril.getContainerOfType(ControlNode)),
			new Function<Command, QualifiedName> {

				override apply(Command input) {
					qualifiedNameProvider.getFullyQualifiedName(input)
				}

				override equals(Object object) {
					throw new UnsupportedOperationException("TODO: auto-generated method stub")
				}

			},
			IScope.NULLSCOPE
		)
	}

	def scope_ResponsibleItemList_responsibleAlarms(ResponsibleItemList ril, EReference ref) {
		var List<Alarm> a = getCandidateAlarms(ril.getContainerOfType(ControlNode).interfaceDescription)
		if (ril.getContainerOfType(AlarmBlock) != null) {
			for (ccn : ril.getContainerOfType(ControlNode).childNodes)
				a.addAll(getCandidateAlarms(ccn.interfaceDescription))
		}
		Scopes.scopeFor(
			a,
			new Function<Alarm, QualifiedName> {

				override apply(Alarm input) {
					qualifiedNameProvider.getFullyQualifiedName(input)
				}

				override equals(Object object) {
					throw new UnsupportedOperationException("TODO: auto-generated method stub")
				}

			},
			IScope.NULLSCOPE
		)

	}

	def scope_ResponsibleItemList_responsibleEvents(ResponsibleItemList ril, EReference ref) {
		var List<Event> e = getCandidateEvents(ril.getContainerOfType(ControlNode).interfaceDescription)
		if (ril.getContainerOfType(EventBlock) != null) {
			for (ccn : ril.getContainerOfType(ControlNode).childNodes)
				e.addAll(getCandidateEvents(ccn.interfaceDescription))
		}

		Scopes.scopeFor(
			e,
			new Function<Event, QualifiedName> {

				override apply(Event input) {
					qualifiedNameProvider.getFullyQualifiedName(input)
				}

				override equals(Object object) {
					throw new UnsupportedOperationException("TODO: auto-generated method stub")
				}

			},
			IScope.NULLSCOPE
		)
	}

	def scope_ResponsibleItemList_responsibleDataPoints(ResponsibleItemList ril, EReference ref) {
		var List<DataPoint> dp = getCandidateDataPoints(ril.getContainerOfType(ControlNode).interfaceDescription)
		if (ril.getContainerOfType(DataPointBlock) != null) {
			for (ccn : ril.getContainerOfType(ControlNode).childNodes)
				dp.addAll(getCandidateDataPoints(ccn.interfaceDescription))
		}
		Scopes.scopeFor(
			dp,
			new Function<DataPoint, QualifiedName> {

				override apply(DataPoint input) {
					qualifiedNameProvider.getFullyQualifiedName(input)
				}

				override equals(Object object) {
					throw new UnsupportedOperationException("TODO: auto-generated method stub")
				}

			},
			IScope.NULLSCOPE
		)
	}

}
