/*
 * generated by Xtext
 */
package com.mncml.dsl.validation

import static extension org.eclipse.xtext.EcoreUtil2.*
import mncModel.ControlNode
import mncModel.MncModelPackage
import org.eclipse.xtext.validation.Check
import mncModel.Model
import mncModel.InterfaceDescription
import mncModel.SimpleType
import mncModel.IntValue
import mncModel.BoolValue
import mncModel.FloatValue
import mncModel.StringValue
import mncModel.Command
import mncModel.Response
import mncModel.Alarm
import mncModel.OperatingState
import mncModel.utility.UtilityPackage
import mncModel.CheckParameterCondition
import mncModel.CommandValidation
import mncModel.CommandResponseBlock
import mncModel.ArrayType
import mncModel.ResponseValidation
import com.google.inject.Inject
import org.eclipse.xtext.naming.IQualifiedNameProvider
import mncModel.Address
import mncModel.Port
import mncModel.DataPointValidCondition
import mncModel.CommandTranslation
import org.eclipse.xtext.resource.impl.ResourceDescriptionsProvider
import java.util.List
import org.eclipse.emf.ecore.util.EcoreUtil
import org.eclipse.emf.ecore.EObject
import mncModel.EventBlock
import mncModel.AlarmBlock
import mncModel.DataPointBlock
import mncModel.Action
import mncModel.Event
import org.eclipse.xtext.EcoreUtil2
import org.eclipse.xtext.resource.XtextResource
import org.eclipse.xtext.ui.editor.model.XtextDocument
import org.eclipse.ui.handlers.HandlerUtil
import org.eclipse.ui.PlatformUI 
import org.eclipse.xtext.ui.editor.utils.EditorUtils
import org.eclipse.swt.widgets.Display
import mncModel.MncModelFactory
import org.eclipse.xtext.ui.editor.XtextEditor
import mncModel.CommandDistribution
import mncModel.utility.CommandDistributionUtility
import mncModel.ResponseTranslation
import mncModel.ResponseBlock

//import org.eclipse.xtext.validation.Check
/**
 * Custom validation rules. 
 *
 * see http://www.eclipse.org/Xtext/documentation.html#validation
 */
class MncValidator extends AbstractMncValidator {

	@Inject
	extension IQualifiedNameProvider qualifiedNameProvider

	@Inject
	ResourceDescriptionsProvider rdp

	public static val INVALID_NAME = 'invalidName'

	@Check 
	def checkSystemStartsWithCapital(ControlNode cn) {
		if (!Character.isUpperCase(cn.name.charAt(0))) {

			warning('Name should start with a capital', MncModelPackage.Literals.CONTROL_NODE__NAME, INVALID_NAME)
		}
	}

	@Check
	def checkSystemHasName(Model model) {
		var count = 0
		for (mncModel.System s : model.systems) {
			if (s instanceof ControlNode && (s as ControlNode).name == null) {
				error("Control Node must have a name", MncModelPackage.Literals.MODEL__SYSTEMS, count)
			} else if (s instanceof InterfaceDescription && (s as InterfaceDescription).name == null) {
				error("Interface Description must have a name", MncModelPackage.Literals.MODEL__SYSTEMS, count)
			}
			count++
		}
	}

	@Check
	def checkNoDuplicateControlNode(ControlNode cn) {
		var c = cn

		//		if(cn.getContainerOfType(typeof(Model)).systems.exists[
		//			
		//			if(it instanceof ControlNode){
		//				val c = (it as ControlNode) 
		//				c!=cn && c.name == cn.name
		//			}
		//			else
		//			{} 			
		//		])
		//		error("Duplicate Control Node '"+cn.name+"'",MncModelPackage.eINSTANCE.controlNode_Name)
		for (ControlNode cnode : getAllControlNode()) {
			c = cnode
			if (cnode.eIsProxy) {
				c = EcoreUtil.resolve(c, cn) as ControlNode
			}
			if (c != cn && c.name == cn.name)
				error("Duplicate Control Node '" + cn.name + "' in resource '" + c.eResource.URI + "'", cn,
					MncModelPackage.eINSTANCE.controlNode_Name)
		}
	}

	def getAllControlNode() {
		var List<ControlNode> cn = newArrayList()
		for (eod : rdp.createResourceDescriptions.getExportedObjectsByType(MncModelPackage.Literals.CONTROL_NODE))
			cn.add(eod.EObjectOrProxy as ControlNode)
		cn
	}

	@Check
	def checkNoDuplicateInterfaceDescription(InterfaceDescription id) {
		if (id.getContainerOfType(typeof(Model)).systems.exists [
			if (it instanceof InterfaceDescription) {
				val c = (it as InterfaceDescription)
				c != id && c.name == id.name
			} else {
			}
		])
			error("Duplicate Interface Description '" + id.name + "'",
				MncModelPackage.eINSTANCE.interfaceDescription_Name)
	}

	@Check
	def checkParameterValueWithType(SimpleType p) {
		var type = p.type.getName
		var value = p.value
		var flag = false
		switch (type) {
			case "int": if(!(value instanceof IntValue)) flag = true
			case "boolean": if(!(value instanceof BoolValue)) flag = true
			case "float": if(!(value instanceof FloatValue || value instanceof IntValue)) flag = true
			case "string": if(!(value instanceof StringValue)) flag = true
		}
		if (value == null)
			flag = false
		if (flag)
			error("Parameter value does not match with the parameter type '" + type + "'",
				MncModelPackage.Literals.SIMPLE_TYPE__VALUE)

	}

	@Check
	def checkChildNodeDifferentFromParentNode(ControlNode cn) {
		var count = 0
		var pos = 0
		var bool = false
		for (ccn : cn.childNodes) {
			if (ccn == cn) {
				bool = true
				pos = count
			}
			count++
		}
		if (bool)
			error("Child node cannot be same as the parent node", MncModelPackage.Literals.CONTROL_NODE__CHILD_NODES,
				pos)
	}

	@Check
	def checkNoDuplicateParameter(SimpleType st) {

		// In Commands
		if (st.getContainerOfType(typeof(Command)).parameters.exists [
			if (it instanceof SimpleType)
				(it as SimpleType) != st && (it as SimpleType).name == st.name
			else {
			}
		])
			error("Duplicate Parameter '" + st.name + "'", MncModelPackage.eINSTANCE.simpleType_Name)

		// In Response
		else if (st.getContainerOfType(typeof(Response)).parameters.exists [
			if (it instanceof SimpleType)
				(it as SimpleType) != st && (it as SimpleType).name == st.name
			else {
			}
		])
			error("Duplicate Parameter '" + st.name + "'", MncModelPackage.eINSTANCE.simpleType_Name)
		
		else 
		// In Systems
		(st.getContainerOfType(typeof(InterfaceDescription)).dataPoints.dataPoints.exists [
			it != st && it.name == st.name
		])
			error("Duplicate Parameter '" + st.name + "'", MncModelPackage.eINSTANCE.simpleType_Name)
		
	}

	@Check
	def checkNoDuplicateCommand(Command c) {
		if (c.getContainerOfType(typeof(InterfaceDescription)).commands.commands.exists [
			it != c && it.name == c.name
		]) {
			error("Duplicate Command '" + c.name + "'", MncModelPackage.eINSTANCE.command_Name)

		//			println(c.name)
		}
	}

	@Check
	def checkNoDuplicateResponse(Response r) {
		if (r.getContainerOfType(typeof(InterfaceDescription)).responses.responses.exists [
			it != r && it.name == r.name
		])
			error("Duplicate Response '" + r.name + "'", MncModelPackage.eINSTANCE.response_Name)
	}

	@Check
	def checkNoDuplicateAlarm(Alarm a) {
		if (a.getContainerOfType(typeof(InterfaceDescription)).alarms.alarms.exists [
			it != a && it.name == a.name
		])
			error("Duplicate Alarm '" + a.name + "'", MncModelPackage.eINSTANCE.alarm_Name)
	}

	@Check
	def checkNoDuplicateEvent(Event e) {
		if (e.getContainerOfType(typeof(InterfaceDescription)).events.events.exists [
			it != e && it.name == e.name
		])
			error("Duplicate Event '" + e.name + "'", MncModelPackage.eINSTANCE.event_Name)
	}
	
	@Check
	def checkNoDuplicateCommandResponse(InterfaceDescription id) {

		for (Command c : id.commands.commands) {
			for (Response r : id.responses.responses) {
				if (c.name == r.name) {
					error("Command '" + c.name + "' is duplicate of Response '" + r.name + "'", c,
						MncModelPackage.Literals.COMMAND__NAME)
					error("Response '" + r.name + "' is duplicate of Command '" + c.name + "'", r,
						MncModelPackage.Literals.RESPONSE__NAME)
				}
			}

		}
	}

	@Check
	def checkNoDuplicateState(OperatingState s) {
		if (s.getContainerOfType(typeof(InterfaceDescription)).operatingStates.operatingStates.exists [
			it != s && it.name == s.name
		])
			error("Duplicate state '" + s.name + "'", MncModelPackage.eINSTANCE.operatingState_Name)
	}

	@Check
	def checkTranslatedCommandsInChildNodes(CommandTranslation ctr) {
		var count = 0
		var bool = false
		val cmdList = ctr.translatedCommands
		for (cmd : cmdList) {
			for (cn : ctr.getContainerOfType(ControlNode).childNodes) {
				if (cn.interfaceDescription.commands.commands.exists[it == cmd]) {
					bool = true;
				}
				for (id : cn.interfaceDescription.uses) {
					if (id.commands.commands.exists[it == cmd]) {
						bool = true;
					}
				}
			}
			if (!bool)
				error("Command does not map to any child node commands.",
					MncModelPackage.Literals.COMMAND_TRANSLATION__TRANSLATED_COMMANDS, count) //"+(cmd.fullyQualifiedName?:'p')+"
			count++
			bool = false
		}

	}

	@Check
	def checkParameterinCheckParameterCondition(CheckParameterCondition cpc) {
		var cv = cpc.getContainerOfType(CommandValidation)
		if (cpc.getContainerOfType(CommandValidation) != null) {
			var cmd = cv.getContainerOfType(CommandResponseBlock).command
			if (!cmd.parameters.exists [
				if (it instanceof SimpleType && cpc.parameter instanceof SimpleType)
					(it as SimpleType).name == (cpc.parameter as SimpleType).name
				else if (it instanceof ArrayType && cpc.parameter instanceof ArrayType)
					(it as ArrayType).name == (cpc.parameter as ArrayType).name
				else
					false
			])
				error(
					"Parameter '" + (cpc.parameter.fullyQualifiedName ?: "") + "' does'nt belong to command '" + cmd.
						name + "'", MncModelPackage.Literals.CHECK_PARAMETER_CONDITION__PARAMETER)
		} else {

			var res = cpc.getContainerOfType(ResponseValidation).response
			if (!res.parameters.exists[
				if (it instanceof SimpleType && cpc.parameter instanceof SimpleType)
					(it as SimpleType).name == (cpc.parameter as SimpleType).name
				else if (it instanceof ArrayType && cpc.parameter instanceof ArrayType)
					(it as ArrayType).name == (cpc.parameter as ArrayType).name
				else
					false])
				error(
					"Parameter '" + (cpc.parameter.fullyQualifiedName ?: "") + "' does'nt belong to response '" +
						res.name + "'", MncModelPackage.Literals.CHECK_PARAMETER_CONDITION__PARAMETER)
		}

	}

	@Check
	def checkResponseinResponseValidation(ResponseValidation rv) {
		if (!rv.getContainerOfType(InterfaceDescription).responses.responses.exists[it == rv.response]) {
			var bool = false
			for (cn : rv.getContainerOfType(ControlNode).childNodes) {
				if (cn.interfaceDescription.responses.responses.exists[it == rv.response])
					bool = true
			}
			if (!bool)
				error("Response '" + (rv.response.fullyQualifiedName ?: "") + "' is unexpected here",
					MncModelPackage.Literals.RESPONSE_VALIDATION__RESPONSE);
		}
	}

	@Check
	def checkValidIPAdrress(Address a) {
		val String[] s = a.ipaddress.split("\\.")
		var bool = false
		for (j : 0 ..< s.size) {
			var i = Integer.parseInt(s.get(j))
			if (i < 0 || i > 255)
				bool = true
		}
		if (bool)
			error("IPAddress '" + a.ipaddress + "' is invalid.", MncModelPackage.Literals.ADDRESS__IPADDRESS)
	}

	@Check
	def checkPortNumber(Port p) {
		if (p.value < 1 || p.value > 65535)
			error("Port value '" + p.value + "' is invalid.", MncModelPackage.Literals.PORT__VALUE)
	}

	@Check
	def checkDataPointinCheckDataPointCondition(DataPointValidCondition cdpc) {
		if (!cdpc.getContainerOfType(ControlNode).interfaceDescription.dataPoints.dataPoints.exists[it == cdpc.dataPoint])
			error("DataPoint '" + (cdpc.dataPoint.fullyQualifiedName ?: "") + "' is unexpected here",
				MncModelPackage.Literals.DATA_POINT_VALID_CONDITION__DATA_POINT)

	}

	@Check
	def checkNoAssociatedInterfaceDescriptioninControlNode(ControlNode cn) {
		if (cn.interfaceDescription == null)
			error("Control Node must have an associated Interface Description.",
				MncModelPackage.Literals.CONTROL_NODE__NAME)
	}

//	@Check
//	def checkNoInfiniteLoopForCommandInControlNode(CommandResponseBlock commandRespBlock) {
//		var cn = commandRespBlock.getContainerOfType(ControlNode)
//		var eventBlocks = cn.eventBlocks.eventBlocks
//		var commandResponseBlocks = cn.commandResponseBlocks.commandResponseBlocks
//		var alarmBlocks = cn.alarmBlocks.alarmBlocks
//		var triggeredEvents = commandRespBlock.triggeredEvents
//
//		for (eventBlock : eventBlocks) {
//			var event = eventBlock.event
//			var eventHandlingActionCommand = eventBlock.eventHandling.triggerAction.command
//			for (triggeredEvent : triggeredEvents) {
//
//				if (eventBlock.event.equals(triggeredEvent) &&
//					eventHandlingActionCommand.contains(commandRespBlock.command)) {
//					error("Cyclic Infinite Loop Detected in Command and Event.",
//						MncModelPackage.Literals.COMMAND_RESPONSE_BLOCK__TRIGGERED_EVENTS)
//				}
//
//			}
//			var transitions = commandRespBlock.transition.transitions
//			for (transition : transitions) {
//				var entryAction = transition.entryAction
//				var exitAction = transition.exitAction
//				if ((entryAction.event.contains(eventBlock.event) || exitAction.event.contains(eventBlock.event)) &&
//					eventHandlingActionCommand.contains(commandRespBlock.command)) {
//					error("Cyclic Infinite Loop Detected in Command and Event.",
//						MncModelPackage.Literals.COMMAND_RESPONSE_BLOCK__TRANSITION)
//				}
//			}
//
//		}
//	}

	@Check
	def checkNoCommandCallsItSelfInTransitionAction(CommandResponseBlock commandResponseBlock) {

		var transitions = commandResponseBlock.transition.transitions
		for (transition : transitions) {
			if (transition.entryAction.command.contains(commandResponseBlock.command) ||
				transition.exitAction.command.contains(commandResponseBlock.command)) {
				error("Command calls itself as part of transition actions.",
					MncModelPackage.Literals.COMMAND_RESPONSE_BLOCK__TRANSITION)
			}

		}

	}

	@Check
	def checkNoDuplicateAlarmBlockInControlNode(AlarmBlock a) {
		if (a.getContainerOfType(typeof(ControlNode)).alarmBlocks.alarmBlocks.exists [
			it != a && it.alarm == a.alarm
		])
			error("Duplicate Alarm Block'" + a.alarm.name + "'", MncModelPackage.Literals.ALARM_BLOCK__ALARM)
	}

	@Check
	def checkNoDuplicateCommandResponseBlockInControlNode(CommandResponseBlock a) {
		if (a.getContainerOfType(typeof(ControlNode)).commandResponseBlocks.commandResponseBlocks.exists [
			it != a && it.command == a.command
		])
			error("Duplicate Command Block'" + a.command.name + "'",
				MncModelPackage.Literals.COMMAND_RESPONSE_BLOCK__COMMAND)
	}

	@Check
	def checkNoDuplicateEventBlockInControlNode(EventBlock a) {
		if (a.getContainerOfType(typeof(ControlNode)).eventBlocks.eventBlocks.exists [
			it != a && it.event == a.event
		])
			error("Duplicate Event Block'" + a.event.name + "'", MncModelPackage.Literals.EVENT_BLOCK__EVENT)
	}

	@Check
	def checkNoDuplicateDataPointBlockInControlNode(DataPointBlock a) {
		if (a.getContainerOfType(typeof(ControlNode)).dataPointBlocks.dataPointBlocks.exists [
			it != a && it.dataPoint == a.dataPoint
		])
			error("Duplicate Alarm Block'" + a.dataPoint.name + "'",
				MncModelPackage.Literals.DATA_POINT_BLOCK__DATA_POINT)
	}

//	@Check
//	def checkNoCommandIsBlank(CommandResponseBlock commandResponseBlock) {
//		var command = commandResponseBlock.command
//		if (command.name == null) {
//			error("Command Block Must Have a Command Name.",
//				MncModelPackage.Literals.COMMAND_RESPONSE_BLOCK__COMMAND)
//		} else {
//			if (commandResponseBlock.commandValidation == null && commandResponseBlock.commandDistributions == null &&
//				commandResponseBlock.commandTranslation == null &&
//				commandResponseBlock.responseDistributions.empty == true &&
//				commandResponseBlock.responseValidation == null && commandResponseBlock.responseTranslation == null &&
//				commandResponseBlock.transition == null && commandResponseBlock.triggeredEvents.empty == true)
//				warning("Command  Response Block Should Have a Command Response Details.",
//					MncModelPackage.Literals.COMMAND_RESPONSE_BLOCK__COMMAND)
//		}
//	}

	@Check
	def checkNoEventBlockIsBlank(EventBlock eventBlock) {
		var event = eventBlock.event
		if (event.name == null) {
			error("Event Block Must Have a Event Name.", MncModelPackage.Literals.EVENT_BLOCK__EVENT)
		} else {
			if (eventBlock.eventCondition == null && eventBlock.eventHandling == null)
				warning("Event Block Should Have Event Details.", MncModelPackage.Literals.EVENT_BLOCK__EVENT)
		}
	} 

	@Check 
	def checkIfActionsAreRepeatedOrNot(Action action) {
		var actionEvents = action.event
		var actionAlarms = action.alarm
		var actionCommands = action.command
		
		for(var m = 0 ; m< actionCommands.size ;m++)
		{
			for(var n = m+1; n<actionCommands.size;n++)
			{
				if(actionCommands.get(n).equals(actionCommands.get(m)))
				{
					warning("Repeating Commands in Action", MncModelPackage.Literals.ACTION__COMMAND)
				}
			}
		}
		
		for(var m = 0 ; m< actionEvents.size ;m++)
		{
			for(var n = m+1; n<actionEvents.size;n++)
			{
				if(actionEvents.get(n).equals(actionEvents.get(m)))
				{
					warning("Repeating Events in Action", MncModelPackage.Literals.ACTION__EVENT)
				} 
			}
		}
		
		for(var m = 0 ; m< actionAlarms.size ;m++)
		{
			for(var n = m+1; n<actionAlarms.size;n++)
			{ 
				if(actionAlarms.get(n).equals(actionAlarms.get(m)))
				{ 
					warning("Repeating Alarms in Action", MncModelPackage.Literals.ACTION__ALARM)
				} 
			} 
		}
	}
	
	@Check 
	def noEventCallsItSelfInHandlingAction(EventBlock event)
	{ 
		var eventActions = event.eventHandling.triggerAction.event
		eventActions.forEach(thisEvent|
			if(thisEvent.equals(event.event)){ 
				warning("Event called in Event", MncModelPackage.Literals.EVENT_BLOCK__EVENT_HANDLING)
			}
		)
	}
	
	@Check
	def noAlarmCallsItSelfInHandlingAction(AlarmBlock alarm)
	{
		var alarmActions = alarm.alarmHandling.triggerAction.alarm
		alarmActions.forEach(thisAlarm|
			if(thisAlarm.equals(alarm.alarm)){ 
				warning("Alarm called in Alarm", MncModelPackage.Literals.ALARM_BLOCK__ALARM_HANDLING)
			}
		)
	}
	
	@Check
	def checkNoChildNodesOrCommandsForCommandDistribution(CommandDistributionUtility cdu)
	{
		if(cdu.getContainerOfType(ControlNode).childNodes.size==0)  
			warning("No Child Control Nodes to distribute commands",UtilityPackage.Literals.COMMAND_DISTRIBUTION_UTILITY__COMMAND_DISTRIBUTIONS)
		else if(cdu.getContainerOfType(ControlNode).childNodes.forall[it.interfaceDescription.commands==null || it.interfaceDescription.commands.commands.size==0 ])
			warning("Child Control Node do not contain any commands for distribution",UtilityPackage.Literals.COMMAND_DISTRIBUTION_UTILITY__COMMAND_DISTRIBUTIONS)
	}
	
	@Check
	def checkNoChildNodesOrCommandsForCommandTranslation(CommandTranslation ct)
	{
		if(ct.getContainerOfType(ControlNode).childNodes.size==0)  
			warning("No Child Control Nodes to translate commands",ct.getContainerOfType(CommandResponseBlock),MncModelPackage.Literals.COMMAND_RESPONSE_BLOCK__COMMAND_TRANSLATION)
		else if(ct.getContainerOfType(ControlNode).childNodes.forall[it.interfaceDescription.commands==null || it.interfaceDescription.commands.commands.size==0 ])
			warning("Child Control Node do not contain any commands for translation",ct.getContainerOfType(CommandResponseBlock),MncModelPackage.Literals.COMMAND_RESPONSE_BLOCK__COMMAND_TRANSLATION)
	}
	
	@Check
	def checkNoChildNodesOrCommandsForResponseTranslation(ResponseTranslation rt)
	{
		if(rt.getContainerOfType(ControlNode).childNodes.size==0)  
			warning("No Child Control Nodes to translate commands",rt.getContainerOfType(ResponseBlock),MncModelPackage.Literals.RESPONSE_BLOCK__RESPONSE_TRANSLATION)
		else if(rt.getContainerOfType(ControlNode).childNodes.forall[it.interfaceDescription.responses==null || it.interfaceDescription.responses.responses.size==0 ])
			warning("Child Control Node do not contain any commands for translation",rt.getContainerOfType(ResponseBlock),MncModelPackage.Literals.RESPONSE_BLOCK__RESPONSE_TRANSLATION)
	}
	
	
//	 Modifying Xtext Documnet
//	@Check 
//	def getCommandWhichDoesNotExitsInControlNode(CommandResponseBlock command) throws Exception
//	{
//		
//		 
//		val xdoc = EditorUtils.getActiveXtextEditor.document
//		
//		var currentUiThread = Display.getCurrent()     
//		 currentUiThread.asyncExec 
//		(new Runnable(){
//			
//			override run() {     
//	 			xdoc.modify(new org.eclipse.xtext.util.concurrent.IUnitOfWork.Void<XtextResource>(){
//			  
//			override process(XtextResource state) throws Exception {
//				var newCmdRespBlock = MncModelFactory.eINSTANCE.createCommandResponseBlock=>[
//				 	]
//				 	newCmdRespBlock.command =MncModelFactory.eINSTANCE.createCommand=>[
//				 		name= "Demo"
//				 	]
//				 	
//				(state.contents.head as  Model).eContents.filter(ControlNode).get(0).commandResponseBlocks.commandResponseBlocks.add(newCmdRespBlock)
//			} 
//			
//		})
//			}
//			
//		})
//	}

}
