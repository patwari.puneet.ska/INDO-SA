/**
 */
package tangoSimLibSimulator;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Runtime Specified Waveform</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link tangoSimLibSimulator.RuntimeSpecifiedWaveform#getDefaultValue <em>Default Value</em>}</li>
 *   <li>{@link tangoSimLibSimulator.RuntimeSpecifiedWaveform#getTimestamp <em>Timestamp</em>}</li>
 *   <li>{@link tangoSimLibSimulator.RuntimeSpecifiedWaveform#getAttribute_qualities <em>Attribute qualities</em>}</li>
 * </ul>
 *
 * @see tangoSimLibSimulator.TangoSimLibSimulatorPackage#getRuntimeSpecifiedWaveform()
 * @model
 * @generated
 */
public interface RuntimeSpecifiedWaveform extends Simulation {
	/**
	 * Returns the value of the '<em><b>Default Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Default Value</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Default Value</em>' attribute.
	 * @see #setDefaultValue(float)
	 * @see tangoSimLibSimulator.TangoSimLibSimulatorPackage#getRuntimeSpecifiedWaveform_DefaultValue()
	 * @model
	 * @generated
	 */
	float getDefaultValue();

	/**
	 * Sets the value of the '{@link tangoSimLibSimulator.RuntimeSpecifiedWaveform#getDefaultValue <em>Default Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Default Value</em>' attribute.
	 * @see #getDefaultValue()
	 * @generated
	 */
	void setDefaultValue(float value);

	/**
	 * Returns the value of the '<em><b>Timestamp</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Timestamp</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Timestamp</em>' attribute.
	 * @see #setTimestamp(String)
	 * @see tangoSimLibSimulator.TangoSimLibSimulatorPackage#getRuntimeSpecifiedWaveform_Timestamp()
	 * @model
	 * @generated
	 */
	String getTimestamp();

	/**
	 * Sets the value of the '{@link tangoSimLibSimulator.RuntimeSpecifiedWaveform#getTimestamp <em>Timestamp</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Timestamp</em>' attribute.
	 * @see #getTimestamp()
	 * @generated
	 */
	void setTimestamp(String value);

	/**
	 * Returns the value of the '<em><b>Attribute qualities</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Attribute qualities</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Attribute qualities</em>' attribute.
	 * @see #setAttribute_qualities(float)
	 * @see tangoSimLibSimulator.TangoSimLibSimulatorPackage#getRuntimeSpecifiedWaveform_Attribute_qualities()
	 * @model
	 * @generated
	 */
	float getAttribute_qualities();

	/**
	 * Sets the value of the '{@link tangoSimLibSimulator.RuntimeSpecifiedWaveform#getAttribute_qualities <em>Attribute qualities</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Attribute qualities</em>' attribute.
	 * @see #getAttribute_qualities()
	 * @generated
	 */
	void setAttribute_qualities(float value);

} // RuntimeSpecifiedWaveform
