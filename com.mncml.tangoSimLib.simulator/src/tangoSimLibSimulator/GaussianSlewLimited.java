/**
 */
package tangoSimLibSimulator;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Gaussian Slew Limited</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link tangoSimLibSimulator.GaussianSlewLimited#getMinBound <em>Min Bound</em>}</li>
 *   <li>{@link tangoSimLibSimulator.GaussianSlewLimited#getMaxBound <em>Max Bound</em>}</li>
 *   <li>{@link tangoSimLibSimulator.GaussianSlewLimited#getMean <em>Mean</em>}</li>
 *   <li>{@link tangoSimLibSimulator.GaussianSlewLimited#getSlewRate <em>Slew Rate</em>}</li>
 *   <li>{@link tangoSimLibSimulator.GaussianSlewLimited#getUpdatePeriod <em>Update Period</em>}</li>
 * </ul>
 *
 * @see tangoSimLibSimulator.TangoSimLibSimulatorPackage#getGaussianSlewLimited()
 * @model
 * @generated
 */
public interface GaussianSlewLimited extends Simulation {
	/**
	 * Returns the value of the '<em><b>Min Bound</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Min Bound</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Min Bound</em>' attribute.
	 * @see #setMinBound(float)
	 * @see tangoSimLibSimulator.TangoSimLibSimulatorPackage#getGaussianSlewLimited_MinBound()
	 * @model
	 * @generated
	 */
	float getMinBound();

	/**
	 * Sets the value of the '{@link tangoSimLibSimulator.GaussianSlewLimited#getMinBound <em>Min Bound</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Min Bound</em>' attribute.
	 * @see #getMinBound()
	 * @generated
	 */
	void setMinBound(float value);

	/**
	 * Returns the value of the '<em><b>Max Bound</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Max Bound</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Max Bound</em>' attribute.
	 * @see #setMaxBound(float)
	 * @see tangoSimLibSimulator.TangoSimLibSimulatorPackage#getGaussianSlewLimited_MaxBound()
	 * @model
	 * @generated
	 */
	float getMaxBound();

	/**
	 * Sets the value of the '{@link tangoSimLibSimulator.GaussianSlewLimited#getMaxBound <em>Max Bound</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Max Bound</em>' attribute.
	 * @see #getMaxBound()
	 * @generated
	 */
	void setMaxBound(float value);

	/**
	 * Returns the value of the '<em><b>Mean</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Mean</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Mean</em>' attribute.
	 * @see #setMean(float)
	 * @see tangoSimLibSimulator.TangoSimLibSimulatorPackage#getGaussianSlewLimited_Mean()
	 * @model
	 * @generated
	 */
	float getMean();

	/**
	 * Sets the value of the '{@link tangoSimLibSimulator.GaussianSlewLimited#getMean <em>Mean</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Mean</em>' attribute.
	 * @see #getMean()
	 * @generated
	 */
	void setMean(float value);

	/**
	 * Returns the value of the '<em><b>Slew Rate</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Slew Rate</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Slew Rate</em>' attribute.
	 * @see #setSlewRate(float)
	 * @see tangoSimLibSimulator.TangoSimLibSimulatorPackage#getGaussianSlewLimited_SlewRate()
	 * @model
	 * @generated
	 */
	float getSlewRate();

	/**
	 * Sets the value of the '{@link tangoSimLibSimulator.GaussianSlewLimited#getSlewRate <em>Slew Rate</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Slew Rate</em>' attribute.
	 * @see #getSlewRate()
	 * @generated
	 */
	void setSlewRate(float value);

	/**
	 * Returns the value of the '<em><b>Update Period</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Update Period</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Update Period</em>' attribute.
	 * @see #setUpdatePeriod(float)
	 * @see tangoSimLibSimulator.TangoSimLibSimulatorPackage#getGaussianSlewLimited_UpdatePeriod()
	 * @model
	 * @generated
	 */
	float getUpdatePeriod();

	/**
	 * Sets the value of the '{@link tangoSimLibSimulator.GaussianSlewLimited#getUpdatePeriod <em>Update Period</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Update Period</em>' attribute.
	 * @see #getUpdatePeriod()
	 * @generated
	 */
	void setUpdatePeriod(float value);

} // GaussianSlewLimited
