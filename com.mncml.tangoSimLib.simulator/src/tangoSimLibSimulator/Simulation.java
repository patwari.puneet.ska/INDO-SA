/**
 */
package tangoSimLibSimulator;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Simulation</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see tangoSimLibSimulator.TangoSimLibSimulatorPackage#getSimulation()
 * @model
 * @generated
 */
public interface Simulation extends EObject {
} // Simulation
