/**
 */
package tangoSimLibSimulator.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import pogoDsl.Attribute;

import tangoSimLibSimulator.DataSimulation;
import tangoSimLibSimulator.Simulation;
import tangoSimLibSimulator.TangoSimLibSimulatorPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Data Simulation</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link tangoSimLibSimulator.impl.DataSimulationImpl#getPogoAttr <em>Pogo Attr</em>}</li>
 *   <li>{@link tangoSimLibSimulator.impl.DataSimulationImpl#getSimulationType <em>Simulation Type</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DataSimulationImpl extends MinimalEObjectImpl.Container implements DataSimulation {
	/**
	 * The cached value of the '{@link #getPogoAttr() <em>Pogo Attr</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPogoAttr()
	 * @generated
	 * @ordered
	 */
	protected Attribute pogoAttr;

	/**
	 * The cached value of the '{@link #getSimulationType() <em>Simulation Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSimulationType()
	 * @generated
	 * @ordered
	 */
	protected Simulation simulationType;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DataSimulationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TangoSimLibSimulatorPackage.Literals.DATA_SIMULATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Attribute getPogoAttr() {
		if (pogoAttr != null && pogoAttr.eIsProxy()) {
			InternalEObject oldPogoAttr = (InternalEObject)pogoAttr;
			pogoAttr = (Attribute)eResolveProxy(oldPogoAttr);
			if (pogoAttr != oldPogoAttr) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TangoSimLibSimulatorPackage.DATA_SIMULATION__POGO_ATTR, oldPogoAttr, pogoAttr));
			}
		}
		return pogoAttr;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Attribute basicGetPogoAttr() {
		return pogoAttr;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPogoAttr(Attribute newPogoAttr) {
		Attribute oldPogoAttr = pogoAttr;
		pogoAttr = newPogoAttr;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TangoSimLibSimulatorPackage.DATA_SIMULATION__POGO_ATTR, oldPogoAttr, pogoAttr));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Simulation getSimulationType() {
		return simulationType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSimulationType(Simulation newSimulationType, NotificationChain msgs) {
		Simulation oldSimulationType = simulationType;
		simulationType = newSimulationType;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TangoSimLibSimulatorPackage.DATA_SIMULATION__SIMULATION_TYPE, oldSimulationType, newSimulationType);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSimulationType(Simulation newSimulationType) {
		if (newSimulationType != simulationType) {
			NotificationChain msgs = null;
			if (simulationType != null)
				msgs = ((InternalEObject)simulationType).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TangoSimLibSimulatorPackage.DATA_SIMULATION__SIMULATION_TYPE, null, msgs);
			if (newSimulationType != null)
				msgs = ((InternalEObject)newSimulationType).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TangoSimLibSimulatorPackage.DATA_SIMULATION__SIMULATION_TYPE, null, msgs);
			msgs = basicSetSimulationType(newSimulationType, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TangoSimLibSimulatorPackage.DATA_SIMULATION__SIMULATION_TYPE, newSimulationType, newSimulationType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case TangoSimLibSimulatorPackage.DATA_SIMULATION__SIMULATION_TYPE:
				return basicSetSimulationType(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TangoSimLibSimulatorPackage.DATA_SIMULATION__POGO_ATTR:
				if (resolve) return getPogoAttr();
				return basicGetPogoAttr();
			case TangoSimLibSimulatorPackage.DATA_SIMULATION__SIMULATION_TYPE:
				return getSimulationType();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TangoSimLibSimulatorPackage.DATA_SIMULATION__POGO_ATTR:
				setPogoAttr((Attribute)newValue);
				return;
			case TangoSimLibSimulatorPackage.DATA_SIMULATION__SIMULATION_TYPE:
				setSimulationType((Simulation)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TangoSimLibSimulatorPackage.DATA_SIMULATION__POGO_ATTR:
				setPogoAttr((Attribute)null);
				return;
			case TangoSimLibSimulatorPackage.DATA_SIMULATION__SIMULATION_TYPE:
				setSimulationType((Simulation)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TangoSimLibSimulatorPackage.DATA_SIMULATION__POGO_ATTR:
				return pogoAttr != null;
			case TangoSimLibSimulatorPackage.DATA_SIMULATION__SIMULATION_TYPE:
				return simulationType != null;
		}
		return super.eIsSet(featureID);
	}

} //DataSimulationImpl
