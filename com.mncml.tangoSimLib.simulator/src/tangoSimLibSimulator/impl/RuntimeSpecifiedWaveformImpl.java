/**
 */
package tangoSimLibSimulator.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import tangoSimLibSimulator.RuntimeSpecifiedWaveform;
import tangoSimLibSimulator.TangoSimLibSimulatorPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Runtime Specified Waveform</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link tangoSimLibSimulator.impl.RuntimeSpecifiedWaveformImpl#getDefaultValue <em>Default Value</em>}</li>
 *   <li>{@link tangoSimLibSimulator.impl.RuntimeSpecifiedWaveformImpl#getTimestamp <em>Timestamp</em>}</li>
 *   <li>{@link tangoSimLibSimulator.impl.RuntimeSpecifiedWaveformImpl#getAttribute_qualities <em>Attribute qualities</em>}</li>
 * </ul>
 *
 * @generated
 */
public class RuntimeSpecifiedWaveformImpl extends SimulationImpl implements RuntimeSpecifiedWaveform {
	/**
	 * The default value of the '{@link #getDefaultValue() <em>Default Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDefaultValue()
	 * @generated
	 * @ordered
	 */
	protected static final float DEFAULT_VALUE_EDEFAULT = 0.0F;

	/**
	 * The cached value of the '{@link #getDefaultValue() <em>Default Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDefaultValue()
	 * @generated
	 * @ordered
	 */
	protected float defaultValue = DEFAULT_VALUE_EDEFAULT;

	/**
	 * The default value of the '{@link #getTimestamp() <em>Timestamp</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTimestamp()
	 * @generated
	 * @ordered
	 */
	protected static final String TIMESTAMP_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getTimestamp() <em>Timestamp</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTimestamp()
	 * @generated
	 * @ordered
	 */
	protected String timestamp = TIMESTAMP_EDEFAULT;

	/**
	 * The default value of the '{@link #getAttribute_qualities() <em>Attribute qualities</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAttribute_qualities()
	 * @generated
	 * @ordered
	 */
	protected static final float ATTRIBUTE_QUALITIES_EDEFAULT = 0.0F;

	/**
	 * The cached value of the '{@link #getAttribute_qualities() <em>Attribute qualities</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAttribute_qualities()
	 * @generated
	 * @ordered
	 */
	protected float attribute_qualities = ATTRIBUTE_QUALITIES_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RuntimeSpecifiedWaveformImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TangoSimLibSimulatorPackage.Literals.RUNTIME_SPECIFIED_WAVEFORM;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public float getDefaultValue() {
		return defaultValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDefaultValue(float newDefaultValue) {
		float oldDefaultValue = defaultValue;
		defaultValue = newDefaultValue;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TangoSimLibSimulatorPackage.RUNTIME_SPECIFIED_WAVEFORM__DEFAULT_VALUE, oldDefaultValue, defaultValue));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getTimestamp() {
		return timestamp;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTimestamp(String newTimestamp) {
		String oldTimestamp = timestamp;
		timestamp = newTimestamp;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TangoSimLibSimulatorPackage.RUNTIME_SPECIFIED_WAVEFORM__TIMESTAMP, oldTimestamp, timestamp));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public float getAttribute_qualities() {
		return attribute_qualities;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAttribute_qualities(float newAttribute_qualities) {
		float oldAttribute_qualities = attribute_qualities;
		attribute_qualities = newAttribute_qualities;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TangoSimLibSimulatorPackage.RUNTIME_SPECIFIED_WAVEFORM__ATTRIBUTE_QUALITIES, oldAttribute_qualities, attribute_qualities));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TangoSimLibSimulatorPackage.RUNTIME_SPECIFIED_WAVEFORM__DEFAULT_VALUE:
				return getDefaultValue();
			case TangoSimLibSimulatorPackage.RUNTIME_SPECIFIED_WAVEFORM__TIMESTAMP:
				return getTimestamp();
			case TangoSimLibSimulatorPackage.RUNTIME_SPECIFIED_WAVEFORM__ATTRIBUTE_QUALITIES:
				return getAttribute_qualities();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TangoSimLibSimulatorPackage.RUNTIME_SPECIFIED_WAVEFORM__DEFAULT_VALUE:
				setDefaultValue((Float)newValue);
				return;
			case TangoSimLibSimulatorPackage.RUNTIME_SPECIFIED_WAVEFORM__TIMESTAMP:
				setTimestamp((String)newValue);
				return;
			case TangoSimLibSimulatorPackage.RUNTIME_SPECIFIED_WAVEFORM__ATTRIBUTE_QUALITIES:
				setAttribute_qualities((Float)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TangoSimLibSimulatorPackage.RUNTIME_SPECIFIED_WAVEFORM__DEFAULT_VALUE:
				setDefaultValue(DEFAULT_VALUE_EDEFAULT);
				return;
			case TangoSimLibSimulatorPackage.RUNTIME_SPECIFIED_WAVEFORM__TIMESTAMP:
				setTimestamp(TIMESTAMP_EDEFAULT);
				return;
			case TangoSimLibSimulatorPackage.RUNTIME_SPECIFIED_WAVEFORM__ATTRIBUTE_QUALITIES:
				setAttribute_qualities(ATTRIBUTE_QUALITIES_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TangoSimLibSimulatorPackage.RUNTIME_SPECIFIED_WAVEFORM__DEFAULT_VALUE:
				return defaultValue != DEFAULT_VALUE_EDEFAULT;
			case TangoSimLibSimulatorPackage.RUNTIME_SPECIFIED_WAVEFORM__TIMESTAMP:
				return TIMESTAMP_EDEFAULT == null ? timestamp != null : !TIMESTAMP_EDEFAULT.equals(timestamp);
			case TangoSimLibSimulatorPackage.RUNTIME_SPECIFIED_WAVEFORM__ATTRIBUTE_QUALITIES:
				return attribute_qualities != ATTRIBUTE_QUALITIES_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (defaultValue: ");
		result.append(defaultValue);
		result.append(", timestamp: ");
		result.append(timestamp);
		result.append(", attribute_qualities: ");
		result.append(attribute_qualities);
		result.append(')');
		return result.toString();
	}

} //RuntimeSpecifiedWaveformImpl
