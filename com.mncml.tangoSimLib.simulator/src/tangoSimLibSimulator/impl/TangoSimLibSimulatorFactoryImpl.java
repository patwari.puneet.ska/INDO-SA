/**
 */
package tangoSimLibSimulator.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import tangoSimLibSimulator.*;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class TangoSimLibSimulatorFactoryImpl extends EFactoryImpl implements TangoSimLibSimulatorFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static TangoSimLibSimulatorFactory init() {
		try {
			TangoSimLibSimulatorFactory theTangoSimLibSimulatorFactory = (TangoSimLibSimulatorFactory)EPackage.Registry.INSTANCE.getEFactory(TangoSimLibSimulatorPackage.eNS_URI);
			if (theTangoSimLibSimulatorFactory != null) {
				return theTangoSimLibSimulatorFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new TangoSimLibSimulatorFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TangoSimLibSimulatorFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case TangoSimLibSimulatorPackage.TANGO_SIM_LIB: return createTangoSimLib();
			case TangoSimLibSimulatorPackage.DATA_SIMULATION: return createDataSimulation();
			case TangoSimLibSimulatorPackage.COMMAND_SIMULATION: return createCommandSimulation();
			case TangoSimLibSimulatorPackage.GAUSSIAN_SLEW_LIMITED: return createGaussianSlewLimited();
			case TangoSimLibSimulatorPackage.CONSTANT_QUANTITY: return createConstantQuantity();
			case TangoSimLibSimulatorPackage.DETERMINISTIC_SIGNAL: return createDeterministicSignal();
			case TangoSimLibSimulatorPackage.RUNTIME_SPECIFIED_WAVEFORM: return createRuntimeSpecifiedWaveform();
			case TangoSimLibSimulatorPackage.INPUT_TRANSFORM: return createInputTransform();
			case TangoSimLibSimulatorPackage.SIDE_EFFECTS: return createSideEffects();
			case TangoSimLibSimulatorPackage.OUTPUT_RETURN: return createOutputReturn();
			case TangoSimLibSimulatorPackage.OVERRIDE_CLASS: return createOverrideClass();
			case TangoSimLibSimulatorPackage.SIMULATION: return createSimulation();
			case TangoSimLibSimulatorPackage.BEHAVIOUR: return createBehaviour();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TangoSimLib createTangoSimLib() {
		TangoSimLibImpl tangoSimLib = new TangoSimLibImpl();
		return tangoSimLib;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataSimulation createDataSimulation() {
		DataSimulationImpl dataSimulation = new DataSimulationImpl();
		return dataSimulation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CommandSimulation createCommandSimulation() {
		CommandSimulationImpl commandSimulation = new CommandSimulationImpl();
		return commandSimulation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GaussianSlewLimited createGaussianSlewLimited() {
		GaussianSlewLimitedImpl gaussianSlewLimited = new GaussianSlewLimitedImpl();
		return gaussianSlewLimited;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConstantQuantity createConstantQuantity() {
		ConstantQuantityImpl constantQuantity = new ConstantQuantityImpl();
		return constantQuantity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DeterministicSignal createDeterministicSignal() {
		DeterministicSignalImpl deterministicSignal = new DeterministicSignalImpl();
		return deterministicSignal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RuntimeSpecifiedWaveform createRuntimeSpecifiedWaveform() {
		RuntimeSpecifiedWaveformImpl runtimeSpecifiedWaveform = new RuntimeSpecifiedWaveformImpl();
		return runtimeSpecifiedWaveform;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InputTransform createInputTransform() {
		InputTransformImpl inputTransform = new InputTransformImpl();
		return inputTransform;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SideEffects createSideEffects() {
		SideEffectsImpl sideEffects = new SideEffectsImpl();
		return sideEffects;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OutputReturn createOutputReturn() {
		OutputReturnImpl outputReturn = new OutputReturnImpl();
		return outputReturn;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OverrideClass createOverrideClass() {
		OverrideClassImpl overrideClass = new OverrideClassImpl();
		return overrideClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Simulation createSimulation() {
		SimulationImpl simulation = new SimulationImpl();
		return simulation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Behaviour createBehaviour() {
		BehaviourImpl behaviour = new BehaviourImpl();
		return behaviour;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TangoSimLibSimulatorPackage getTangoSimLibSimulatorPackage() {
		return (TangoSimLibSimulatorPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static TangoSimLibSimulatorPackage getPackage() {
		return TangoSimLibSimulatorPackage.eINSTANCE;
	}

} //TangoSimLibSimulatorFactoryImpl
