/**
 */
package tangoSimLibSimulator.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import tangoSimLibSimulator.OverrideClass;
import tangoSimLibSimulator.TangoSimLibSimulatorPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Override Class</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link tangoSimLibSimulator.impl.OverrideClassImpl#getName <em>Name</em>}</li>
 *   <li>{@link tangoSimLibSimulator.impl.OverrideClassImpl#getModule_directory <em>Module directory</em>}</li>
 *   <li>{@link tangoSimLibSimulator.impl.OverrideClassImpl#getModule_name <em>Module name</em>}</li>
 *   <li>{@link tangoSimLibSimulator.impl.OverrideClassImpl#getClass_name <em>Class name</em>}</li>
 * </ul>
 *
 * @generated
 */
public class OverrideClassImpl extends MinimalEObjectImpl.Container implements OverrideClass {
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getModule_directory() <em>Module directory</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getModule_directory()
	 * @generated
	 * @ordered
	 */
	protected static final String MODULE_DIRECTORY_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getModule_directory() <em>Module directory</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getModule_directory()
	 * @generated
	 * @ordered
	 */
	protected String module_directory = MODULE_DIRECTORY_EDEFAULT;

	/**
	 * The default value of the '{@link #getModule_name() <em>Module name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getModule_name()
	 * @generated
	 * @ordered
	 */
	protected static final String MODULE_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getModule_name() <em>Module name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getModule_name()
	 * @generated
	 * @ordered
	 */
	protected String module_name = MODULE_NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getClass_name() <em>Class name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getClass_name()
	 * @generated
	 * @ordered
	 */
	protected static final String CLASS_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getClass_name() <em>Class name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getClass_name()
	 * @generated
	 * @ordered
	 */
	protected String class_name = CLASS_NAME_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected OverrideClassImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TangoSimLibSimulatorPackage.Literals.OVERRIDE_CLASS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TangoSimLibSimulatorPackage.OVERRIDE_CLASS__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getModule_directory() {
		return module_directory;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setModule_directory(String newModule_directory) {
		String oldModule_directory = module_directory;
		module_directory = newModule_directory;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TangoSimLibSimulatorPackage.OVERRIDE_CLASS__MODULE_DIRECTORY, oldModule_directory, module_directory));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getModule_name() {
		return module_name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setModule_name(String newModule_name) {
		String oldModule_name = module_name;
		module_name = newModule_name;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TangoSimLibSimulatorPackage.OVERRIDE_CLASS__MODULE_NAME, oldModule_name, module_name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getClass_name() {
		return class_name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setClass_name(String newClass_name) {
		String oldClass_name = class_name;
		class_name = newClass_name;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TangoSimLibSimulatorPackage.OVERRIDE_CLASS__CLASS_NAME, oldClass_name, class_name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TangoSimLibSimulatorPackage.OVERRIDE_CLASS__NAME:
				return getName();
			case TangoSimLibSimulatorPackage.OVERRIDE_CLASS__MODULE_DIRECTORY:
				return getModule_directory();
			case TangoSimLibSimulatorPackage.OVERRIDE_CLASS__MODULE_NAME:
				return getModule_name();
			case TangoSimLibSimulatorPackage.OVERRIDE_CLASS__CLASS_NAME:
				return getClass_name();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TangoSimLibSimulatorPackage.OVERRIDE_CLASS__NAME:
				setName((String)newValue);
				return;
			case TangoSimLibSimulatorPackage.OVERRIDE_CLASS__MODULE_DIRECTORY:
				setModule_directory((String)newValue);
				return;
			case TangoSimLibSimulatorPackage.OVERRIDE_CLASS__MODULE_NAME:
				setModule_name((String)newValue);
				return;
			case TangoSimLibSimulatorPackage.OVERRIDE_CLASS__CLASS_NAME:
				setClass_name((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TangoSimLibSimulatorPackage.OVERRIDE_CLASS__NAME:
				setName(NAME_EDEFAULT);
				return;
			case TangoSimLibSimulatorPackage.OVERRIDE_CLASS__MODULE_DIRECTORY:
				setModule_directory(MODULE_DIRECTORY_EDEFAULT);
				return;
			case TangoSimLibSimulatorPackage.OVERRIDE_CLASS__MODULE_NAME:
				setModule_name(MODULE_NAME_EDEFAULT);
				return;
			case TangoSimLibSimulatorPackage.OVERRIDE_CLASS__CLASS_NAME:
				setClass_name(CLASS_NAME_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TangoSimLibSimulatorPackage.OVERRIDE_CLASS__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case TangoSimLibSimulatorPackage.OVERRIDE_CLASS__MODULE_DIRECTORY:
				return MODULE_DIRECTORY_EDEFAULT == null ? module_directory != null : !MODULE_DIRECTORY_EDEFAULT.equals(module_directory);
			case TangoSimLibSimulatorPackage.OVERRIDE_CLASS__MODULE_NAME:
				return MODULE_NAME_EDEFAULT == null ? module_name != null : !MODULE_NAME_EDEFAULT.equals(module_name);
			case TangoSimLibSimulatorPackage.OVERRIDE_CLASS__CLASS_NAME:
				return CLASS_NAME_EDEFAULT == null ? class_name != null : !CLASS_NAME_EDEFAULT.equals(class_name);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(", module_directory: ");
		result.append(module_directory);
		result.append(", module_name: ");
		result.append(module_name);
		result.append(", class_name: ");
		result.append(class_name);
		result.append(')');
		return result.toString();
	}

} //OverrideClassImpl
