/**
 */
package tangoSimLibSimulator.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;
import pogoDsl.PogoDeviceClass;

import tangoSimLibSimulator.CommandSimulation;
import tangoSimLibSimulator.DataSimulation;
import tangoSimLibSimulator.OverrideClass;
import tangoSimLibSimulator.TangoSimLib;
import tangoSimLibSimulator.TangoSimLibSimulatorPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Tango Sim Lib</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link tangoSimLibSimulator.impl.TangoSimLibImpl#getRefferedPogoDeviceClass <em>Reffered Pogo Device Class</em>}</li>
 *   <li>{@link tangoSimLibSimulator.impl.TangoSimLibImpl#getDataSimulations <em>Data Simulations</em>}</li>
 *   <li>{@link tangoSimLibSimulator.impl.TangoSimLibImpl#getCommandSimulations <em>Command Simulations</em>}</li>
 *   <li>{@link tangoSimLibSimulator.impl.TangoSimLibImpl#getOverrideClass <em>Override Class</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TangoSimLibImpl extends MinimalEObjectImpl.Container implements TangoSimLib {
	/**
	 * The cached value of the '{@link #getRefferedPogoDeviceClass() <em>Reffered Pogo Device Class</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRefferedPogoDeviceClass()
	 * @generated
	 * @ordered
	 */
	protected PogoDeviceClass refferedPogoDeviceClass;

	/**
	 * The cached value of the '{@link #getDataSimulations() <em>Data Simulations</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDataSimulations()
	 * @generated
	 * @ordered
	 */
	protected EList<DataSimulation> dataSimulations;

	/**
	 * The cached value of the '{@link #getCommandSimulations() <em>Command Simulations</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCommandSimulations()
	 * @generated
	 * @ordered
	 */
	protected EList<CommandSimulation> commandSimulations;

	/**
	 * The cached value of the '{@link #getOverrideClass() <em>Override Class</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOverrideClass()
	 * @generated
	 * @ordered
	 */
	protected EList<OverrideClass> overrideClass;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TangoSimLibImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TangoSimLibSimulatorPackage.Literals.TANGO_SIM_LIB;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DataSimulation> getDataSimulations() {
		if (dataSimulations == null) {
			dataSimulations = new EObjectContainmentEList<DataSimulation>(DataSimulation.class, this, TangoSimLibSimulatorPackage.TANGO_SIM_LIB__DATA_SIMULATIONS);
		}
		return dataSimulations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<CommandSimulation> getCommandSimulations() {
		if (commandSimulations == null) {
			commandSimulations = new EObjectContainmentEList<CommandSimulation>(CommandSimulation.class, this, TangoSimLibSimulatorPackage.TANGO_SIM_LIB__COMMAND_SIMULATIONS);
		}
		return commandSimulations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<OverrideClass> getOverrideClass() {
		if (overrideClass == null) {
			overrideClass = new EObjectContainmentEList<OverrideClass>(OverrideClass.class, this, TangoSimLibSimulatorPackage.TANGO_SIM_LIB__OVERRIDE_CLASS);
		}
		return overrideClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case TangoSimLibSimulatorPackage.TANGO_SIM_LIB__DATA_SIMULATIONS:
				return ((InternalEList<?>)getDataSimulations()).basicRemove(otherEnd, msgs);
			case TangoSimLibSimulatorPackage.TANGO_SIM_LIB__COMMAND_SIMULATIONS:
				return ((InternalEList<?>)getCommandSimulations()).basicRemove(otherEnd, msgs);
			case TangoSimLibSimulatorPackage.TANGO_SIM_LIB__OVERRIDE_CLASS:
				return ((InternalEList<?>)getOverrideClass()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PogoDeviceClass getRefferedPogoDeviceClass() {
		if (refferedPogoDeviceClass != null && refferedPogoDeviceClass.eIsProxy()) {
			InternalEObject oldRefferedPogoDeviceClass = (InternalEObject)refferedPogoDeviceClass;
			refferedPogoDeviceClass = (PogoDeviceClass)eResolveProxy(oldRefferedPogoDeviceClass);
			if (refferedPogoDeviceClass != oldRefferedPogoDeviceClass) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TangoSimLibSimulatorPackage.TANGO_SIM_LIB__REFFERED_POGO_DEVICE_CLASS, oldRefferedPogoDeviceClass, refferedPogoDeviceClass));
			}
		}
		return refferedPogoDeviceClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PogoDeviceClass basicGetRefferedPogoDeviceClass() {
		return refferedPogoDeviceClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRefferedPogoDeviceClass(PogoDeviceClass newRefferedPogoDeviceClass) {
		PogoDeviceClass oldRefferedPogoDeviceClass = refferedPogoDeviceClass;
		refferedPogoDeviceClass = newRefferedPogoDeviceClass;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TangoSimLibSimulatorPackage.TANGO_SIM_LIB__REFFERED_POGO_DEVICE_CLASS, oldRefferedPogoDeviceClass, refferedPogoDeviceClass));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TangoSimLibSimulatorPackage.TANGO_SIM_LIB__REFFERED_POGO_DEVICE_CLASS:
				if (resolve) return getRefferedPogoDeviceClass();
				return basicGetRefferedPogoDeviceClass();
			case TangoSimLibSimulatorPackage.TANGO_SIM_LIB__DATA_SIMULATIONS:
				return getDataSimulations();
			case TangoSimLibSimulatorPackage.TANGO_SIM_LIB__COMMAND_SIMULATIONS:
				return getCommandSimulations();
			case TangoSimLibSimulatorPackage.TANGO_SIM_LIB__OVERRIDE_CLASS:
				return getOverrideClass();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TangoSimLibSimulatorPackage.TANGO_SIM_LIB__REFFERED_POGO_DEVICE_CLASS:
				setRefferedPogoDeviceClass((PogoDeviceClass)newValue);
				return;
			case TangoSimLibSimulatorPackage.TANGO_SIM_LIB__DATA_SIMULATIONS:
				getDataSimulations().clear();
				getDataSimulations().addAll((Collection<? extends DataSimulation>)newValue);
				return;
			case TangoSimLibSimulatorPackage.TANGO_SIM_LIB__COMMAND_SIMULATIONS:
				getCommandSimulations().clear();
				getCommandSimulations().addAll((Collection<? extends CommandSimulation>)newValue);
				return;
			case TangoSimLibSimulatorPackage.TANGO_SIM_LIB__OVERRIDE_CLASS:
				getOverrideClass().clear();
				getOverrideClass().addAll((Collection<? extends OverrideClass>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TangoSimLibSimulatorPackage.TANGO_SIM_LIB__REFFERED_POGO_DEVICE_CLASS:
				setRefferedPogoDeviceClass((PogoDeviceClass)null);
				return;
			case TangoSimLibSimulatorPackage.TANGO_SIM_LIB__DATA_SIMULATIONS:
				getDataSimulations().clear();
				return;
			case TangoSimLibSimulatorPackage.TANGO_SIM_LIB__COMMAND_SIMULATIONS:
				getCommandSimulations().clear();
				return;
			case TangoSimLibSimulatorPackage.TANGO_SIM_LIB__OVERRIDE_CLASS:
				getOverrideClass().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TangoSimLibSimulatorPackage.TANGO_SIM_LIB__REFFERED_POGO_DEVICE_CLASS:
				return refferedPogoDeviceClass != null;
			case TangoSimLibSimulatorPackage.TANGO_SIM_LIB__DATA_SIMULATIONS:
				return dataSimulations != null && !dataSimulations.isEmpty();
			case TangoSimLibSimulatorPackage.TANGO_SIM_LIB__COMMAND_SIMULATIONS:
				return commandSimulations != null && !commandSimulations.isEmpty();
			case TangoSimLibSimulatorPackage.TANGO_SIM_LIB__OVERRIDE_CLASS:
				return overrideClass != null && !overrideClass.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //TangoSimLibImpl
