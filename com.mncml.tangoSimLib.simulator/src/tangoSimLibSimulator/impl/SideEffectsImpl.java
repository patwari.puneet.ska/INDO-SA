/**
 */
package tangoSimLibSimulator.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import tangoSimLibSimulator.SideEffects;
import tangoSimLibSimulator.TangoSimLibSimulatorPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Side Effects</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link tangoSimLibSimulator.impl.SideEffectsImpl#getSource_variable <em>Source variable</em>}</li>
 *   <li>{@link tangoSimLibSimulator.impl.SideEffectsImpl#getDestination_quantity <em>Destination quantity</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SideEffectsImpl extends BehaviourImpl implements SideEffects {
	/**
	 * The default value of the '{@link #getSource_variable() <em>Source variable</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSource_variable()
	 * @generated
	 * @ordered
	 */
	protected static final String SOURCE_VARIABLE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSource_variable() <em>Source variable</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSource_variable()
	 * @generated
	 * @ordered
	 */
	protected String source_variable = SOURCE_VARIABLE_EDEFAULT;

	/**
	 * The default value of the '{@link #getDestination_quantity() <em>Destination quantity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDestination_quantity()
	 * @generated
	 * @ordered
	 */
	protected static final String DESTINATION_QUANTITY_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDestination_quantity() <em>Destination quantity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDestination_quantity()
	 * @generated
	 * @ordered
	 */
	protected String destination_quantity = DESTINATION_QUANTITY_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SideEffectsImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TangoSimLibSimulatorPackage.Literals.SIDE_EFFECTS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getSource_variable() {
		return source_variable;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSource_variable(String newSource_variable) {
		String oldSource_variable = source_variable;
		source_variable = newSource_variable;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TangoSimLibSimulatorPackage.SIDE_EFFECTS__SOURCE_VARIABLE, oldSource_variable, source_variable));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDestination_quantity() {
		return destination_quantity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDestination_quantity(String newDestination_quantity) {
		String oldDestination_quantity = destination_quantity;
		destination_quantity = newDestination_quantity;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TangoSimLibSimulatorPackage.SIDE_EFFECTS__DESTINATION_QUANTITY, oldDestination_quantity, destination_quantity));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TangoSimLibSimulatorPackage.SIDE_EFFECTS__SOURCE_VARIABLE:
				return getSource_variable();
			case TangoSimLibSimulatorPackage.SIDE_EFFECTS__DESTINATION_QUANTITY:
				return getDestination_quantity();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TangoSimLibSimulatorPackage.SIDE_EFFECTS__SOURCE_VARIABLE:
				setSource_variable((String)newValue);
				return;
			case TangoSimLibSimulatorPackage.SIDE_EFFECTS__DESTINATION_QUANTITY:
				setDestination_quantity((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TangoSimLibSimulatorPackage.SIDE_EFFECTS__SOURCE_VARIABLE:
				setSource_variable(SOURCE_VARIABLE_EDEFAULT);
				return;
			case TangoSimLibSimulatorPackage.SIDE_EFFECTS__DESTINATION_QUANTITY:
				setDestination_quantity(DESTINATION_QUANTITY_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TangoSimLibSimulatorPackage.SIDE_EFFECTS__SOURCE_VARIABLE:
				return SOURCE_VARIABLE_EDEFAULT == null ? source_variable != null : !SOURCE_VARIABLE_EDEFAULT.equals(source_variable);
			case TangoSimLibSimulatorPackage.SIDE_EFFECTS__DESTINATION_QUANTITY:
				return DESTINATION_QUANTITY_EDEFAULT == null ? destination_quantity != null : !DESTINATION_QUANTITY_EDEFAULT.equals(destination_quantity);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (source_variable: ");
		result.append(source_variable);
		result.append(", destination_quantity: ");
		result.append(destination_quantity);
		result.append(')');
		return result.toString();
	}

} //SideEffectsImpl
