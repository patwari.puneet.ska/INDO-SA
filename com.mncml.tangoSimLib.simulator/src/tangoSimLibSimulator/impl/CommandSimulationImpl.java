/**
 */
package tangoSimLibSimulator.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import pogoDsl.Command;

import tangoSimLibSimulator.Behaviour;
import tangoSimLibSimulator.CommandSimulation;
import tangoSimLibSimulator.TangoSimLibSimulatorPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Command Simulation</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link tangoSimLibSimulator.impl.CommandSimulationImpl#getPogoCommand <em>Pogo Command</em>}</li>
 *   <li>{@link tangoSimLibSimulator.impl.CommandSimulationImpl#getBehaviorType <em>Behavior Type</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CommandSimulationImpl extends MinimalEObjectImpl.Container implements CommandSimulation {
	/**
	 * The cached value of the '{@link #getPogoCommand() <em>Pogo Command</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPogoCommand()
	 * @generated
	 * @ordered
	 */
	protected Command pogoCommand;

	/**
	 * The cached value of the '{@link #getBehaviorType() <em>Behavior Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBehaviorType()
	 * @generated
	 * @ordered
	 */
	protected Behaviour behaviorType;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CommandSimulationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TangoSimLibSimulatorPackage.Literals.COMMAND_SIMULATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Command getPogoCommand() {
		if (pogoCommand != null && pogoCommand.eIsProxy()) {
			InternalEObject oldPogoCommand = (InternalEObject)pogoCommand;
			pogoCommand = (Command)eResolveProxy(oldPogoCommand);
			if (pogoCommand != oldPogoCommand) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TangoSimLibSimulatorPackage.COMMAND_SIMULATION__POGO_COMMAND, oldPogoCommand, pogoCommand));
			}
		}
		return pogoCommand;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Command basicGetPogoCommand() {
		return pogoCommand;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPogoCommand(Command newPogoCommand) {
		Command oldPogoCommand = pogoCommand;
		pogoCommand = newPogoCommand;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TangoSimLibSimulatorPackage.COMMAND_SIMULATION__POGO_COMMAND, oldPogoCommand, pogoCommand));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Behaviour getBehaviorType() {
		return behaviorType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetBehaviorType(Behaviour newBehaviorType, NotificationChain msgs) {
		Behaviour oldBehaviorType = behaviorType;
		behaviorType = newBehaviorType;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TangoSimLibSimulatorPackage.COMMAND_SIMULATION__BEHAVIOR_TYPE, oldBehaviorType, newBehaviorType);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBehaviorType(Behaviour newBehaviorType) {
		if (newBehaviorType != behaviorType) {
			NotificationChain msgs = null;
			if (behaviorType != null)
				msgs = ((InternalEObject)behaviorType).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TangoSimLibSimulatorPackage.COMMAND_SIMULATION__BEHAVIOR_TYPE, null, msgs);
			if (newBehaviorType != null)
				msgs = ((InternalEObject)newBehaviorType).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TangoSimLibSimulatorPackage.COMMAND_SIMULATION__BEHAVIOR_TYPE, null, msgs);
			msgs = basicSetBehaviorType(newBehaviorType, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TangoSimLibSimulatorPackage.COMMAND_SIMULATION__BEHAVIOR_TYPE, newBehaviorType, newBehaviorType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case TangoSimLibSimulatorPackage.COMMAND_SIMULATION__BEHAVIOR_TYPE:
				return basicSetBehaviorType(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TangoSimLibSimulatorPackage.COMMAND_SIMULATION__POGO_COMMAND:
				if (resolve) return getPogoCommand();
				return basicGetPogoCommand();
			case TangoSimLibSimulatorPackage.COMMAND_SIMULATION__BEHAVIOR_TYPE:
				return getBehaviorType();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TangoSimLibSimulatorPackage.COMMAND_SIMULATION__POGO_COMMAND:
				setPogoCommand((Command)newValue);
				return;
			case TangoSimLibSimulatorPackage.COMMAND_SIMULATION__BEHAVIOR_TYPE:
				setBehaviorType((Behaviour)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TangoSimLibSimulatorPackage.COMMAND_SIMULATION__POGO_COMMAND:
				setPogoCommand((Command)null);
				return;
			case TangoSimLibSimulatorPackage.COMMAND_SIMULATION__BEHAVIOR_TYPE:
				setBehaviorType((Behaviour)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TangoSimLibSimulatorPackage.COMMAND_SIMULATION__POGO_COMMAND:
				return pogoCommand != null;
			case TangoSimLibSimulatorPackage.COMMAND_SIMULATION__BEHAVIOR_TYPE:
				return behaviorType != null;
		}
		return super.eIsSet(featureID);
	}

} //CommandSimulationImpl
