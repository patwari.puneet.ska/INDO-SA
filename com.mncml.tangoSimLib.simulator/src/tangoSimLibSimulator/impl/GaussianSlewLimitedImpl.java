/**
 */
package tangoSimLibSimulator.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import tangoSimLibSimulator.GaussianSlewLimited;
import tangoSimLibSimulator.TangoSimLibSimulatorPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Gaussian Slew Limited</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link tangoSimLibSimulator.impl.GaussianSlewLimitedImpl#getMinBound <em>Min Bound</em>}</li>
 *   <li>{@link tangoSimLibSimulator.impl.GaussianSlewLimitedImpl#getMaxBound <em>Max Bound</em>}</li>
 *   <li>{@link tangoSimLibSimulator.impl.GaussianSlewLimitedImpl#getMean <em>Mean</em>}</li>
 *   <li>{@link tangoSimLibSimulator.impl.GaussianSlewLimitedImpl#getSlewRate <em>Slew Rate</em>}</li>
 *   <li>{@link tangoSimLibSimulator.impl.GaussianSlewLimitedImpl#getUpdatePeriod <em>Update Period</em>}</li>
 * </ul>
 *
 * @generated
 */
public class GaussianSlewLimitedImpl extends SimulationImpl implements GaussianSlewLimited {
	/**
	 * The default value of the '{@link #getMinBound() <em>Min Bound</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMinBound()
	 * @generated
	 * @ordered
	 */
	protected static final float MIN_BOUND_EDEFAULT = 0.0F;

	/**
	 * The cached value of the '{@link #getMinBound() <em>Min Bound</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMinBound()
	 * @generated
	 * @ordered
	 */
	protected float minBound = MIN_BOUND_EDEFAULT;

	/**
	 * The default value of the '{@link #getMaxBound() <em>Max Bound</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaxBound()
	 * @generated
	 * @ordered
	 */
	protected static final float MAX_BOUND_EDEFAULT = 0.0F;

	/**
	 * The cached value of the '{@link #getMaxBound() <em>Max Bound</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaxBound()
	 * @generated
	 * @ordered
	 */
	protected float maxBound = MAX_BOUND_EDEFAULT;

	/**
	 * The default value of the '{@link #getMean() <em>Mean</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMean()
	 * @generated
	 * @ordered
	 */
	protected static final float MEAN_EDEFAULT = 0.0F;

	/**
	 * The cached value of the '{@link #getMean() <em>Mean</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMean()
	 * @generated
	 * @ordered
	 */
	protected float mean = MEAN_EDEFAULT;

	/**
	 * The default value of the '{@link #getSlewRate() <em>Slew Rate</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSlewRate()
	 * @generated
	 * @ordered
	 */
	protected static final float SLEW_RATE_EDEFAULT = 0.0F;

	/**
	 * The cached value of the '{@link #getSlewRate() <em>Slew Rate</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSlewRate()
	 * @generated
	 * @ordered
	 */
	protected float slewRate = SLEW_RATE_EDEFAULT;

	/**
	 * The default value of the '{@link #getUpdatePeriod() <em>Update Period</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUpdatePeriod()
	 * @generated
	 * @ordered
	 */
	protected static final float UPDATE_PERIOD_EDEFAULT = 0.0F;

	/**
	 * The cached value of the '{@link #getUpdatePeriod() <em>Update Period</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUpdatePeriod()
	 * @generated
	 * @ordered
	 */
	protected float updatePeriod = UPDATE_PERIOD_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected GaussianSlewLimitedImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TangoSimLibSimulatorPackage.Literals.GAUSSIAN_SLEW_LIMITED;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public float getMinBound() {
		return minBound;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMinBound(float newMinBound) {
		float oldMinBound = minBound;
		minBound = newMinBound;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TangoSimLibSimulatorPackage.GAUSSIAN_SLEW_LIMITED__MIN_BOUND, oldMinBound, minBound));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public float getMaxBound() {
		return maxBound;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMaxBound(float newMaxBound) {
		float oldMaxBound = maxBound;
		maxBound = newMaxBound;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TangoSimLibSimulatorPackage.GAUSSIAN_SLEW_LIMITED__MAX_BOUND, oldMaxBound, maxBound));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public float getMean() {
		return mean;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMean(float newMean) {
		float oldMean = mean;
		mean = newMean;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TangoSimLibSimulatorPackage.GAUSSIAN_SLEW_LIMITED__MEAN, oldMean, mean));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public float getSlewRate() {
		return slewRate;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSlewRate(float newSlewRate) {
		float oldSlewRate = slewRate;
		slewRate = newSlewRate;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TangoSimLibSimulatorPackage.GAUSSIAN_SLEW_LIMITED__SLEW_RATE, oldSlewRate, slewRate));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public float getUpdatePeriod() {
		return updatePeriod;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUpdatePeriod(float newUpdatePeriod) {
		float oldUpdatePeriod = updatePeriod;
		updatePeriod = newUpdatePeriod;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TangoSimLibSimulatorPackage.GAUSSIAN_SLEW_LIMITED__UPDATE_PERIOD, oldUpdatePeriod, updatePeriod));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TangoSimLibSimulatorPackage.GAUSSIAN_SLEW_LIMITED__MIN_BOUND:
				return getMinBound();
			case TangoSimLibSimulatorPackage.GAUSSIAN_SLEW_LIMITED__MAX_BOUND:
				return getMaxBound();
			case TangoSimLibSimulatorPackage.GAUSSIAN_SLEW_LIMITED__MEAN:
				return getMean();
			case TangoSimLibSimulatorPackage.GAUSSIAN_SLEW_LIMITED__SLEW_RATE:
				return getSlewRate();
			case TangoSimLibSimulatorPackage.GAUSSIAN_SLEW_LIMITED__UPDATE_PERIOD:
				return getUpdatePeriod();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TangoSimLibSimulatorPackage.GAUSSIAN_SLEW_LIMITED__MIN_BOUND:
				setMinBound((Float)newValue);
				return;
			case TangoSimLibSimulatorPackage.GAUSSIAN_SLEW_LIMITED__MAX_BOUND:
				setMaxBound((Float)newValue);
				return;
			case TangoSimLibSimulatorPackage.GAUSSIAN_SLEW_LIMITED__MEAN:
				setMean((Float)newValue);
				return;
			case TangoSimLibSimulatorPackage.GAUSSIAN_SLEW_LIMITED__SLEW_RATE:
				setSlewRate((Float)newValue);
				return;
			case TangoSimLibSimulatorPackage.GAUSSIAN_SLEW_LIMITED__UPDATE_PERIOD:
				setUpdatePeriod((Float)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TangoSimLibSimulatorPackage.GAUSSIAN_SLEW_LIMITED__MIN_BOUND:
				setMinBound(MIN_BOUND_EDEFAULT);
				return;
			case TangoSimLibSimulatorPackage.GAUSSIAN_SLEW_LIMITED__MAX_BOUND:
				setMaxBound(MAX_BOUND_EDEFAULT);
				return;
			case TangoSimLibSimulatorPackage.GAUSSIAN_SLEW_LIMITED__MEAN:
				setMean(MEAN_EDEFAULT);
				return;
			case TangoSimLibSimulatorPackage.GAUSSIAN_SLEW_LIMITED__SLEW_RATE:
				setSlewRate(SLEW_RATE_EDEFAULT);
				return;
			case TangoSimLibSimulatorPackage.GAUSSIAN_SLEW_LIMITED__UPDATE_PERIOD:
				setUpdatePeriod(UPDATE_PERIOD_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TangoSimLibSimulatorPackage.GAUSSIAN_SLEW_LIMITED__MIN_BOUND:
				return minBound != MIN_BOUND_EDEFAULT;
			case TangoSimLibSimulatorPackage.GAUSSIAN_SLEW_LIMITED__MAX_BOUND:
				return maxBound != MAX_BOUND_EDEFAULT;
			case TangoSimLibSimulatorPackage.GAUSSIAN_SLEW_LIMITED__MEAN:
				return mean != MEAN_EDEFAULT;
			case TangoSimLibSimulatorPackage.GAUSSIAN_SLEW_LIMITED__SLEW_RATE:
				return slewRate != SLEW_RATE_EDEFAULT;
			case TangoSimLibSimulatorPackage.GAUSSIAN_SLEW_LIMITED__UPDATE_PERIOD:
				return updatePeriod != UPDATE_PERIOD_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (minBound: ");
		result.append(minBound);
		result.append(", maxBound: ");
		result.append(maxBound);
		result.append(", mean: ");
		result.append(mean);
		result.append(", slewRate: ");
		result.append(slewRate);
		result.append(", updatePeriod: ");
		result.append(updatePeriod);
		result.append(')');
		return result.toString();
	}

} //GaussianSlewLimitedImpl
