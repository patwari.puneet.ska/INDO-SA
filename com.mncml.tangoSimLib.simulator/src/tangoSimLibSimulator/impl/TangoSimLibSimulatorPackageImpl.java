/**
 */
package tangoSimLibSimulator.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import pogoDsl.PogoDslPackage;

import tangoSimLibSimulator.Behaviour;
import tangoSimLibSimulator.CommandSimulation;
import tangoSimLibSimulator.ConstantQuantity;
import tangoSimLibSimulator.DataSimulation;
import tangoSimLibSimulator.DeterministicSignal;
import tangoSimLibSimulator.GaussianSlewLimited;
import tangoSimLibSimulator.InputTransform;
import tangoSimLibSimulator.OutputReturn;
import tangoSimLibSimulator.OverrideClass;
import tangoSimLibSimulator.RuntimeSpecifiedWaveform;
import tangoSimLibSimulator.SideEffects;
import tangoSimLibSimulator.Simulation;
import tangoSimLibSimulator.TangoSimLib;
import tangoSimLibSimulator.TangoSimLibSimulatorFactory;
import tangoSimLibSimulator.TangoSimLibSimulatorPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class TangoSimLibSimulatorPackageImpl extends EPackageImpl implements TangoSimLibSimulatorPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass tangoSimLibEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataSimulationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass commandSimulationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass gaussianSlewLimitedEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass constantQuantityEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass deterministicSignalEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass runtimeSpecifiedWaveformEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass inputTransformEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass sideEffectsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass outputReturnEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass overrideClassEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass simulationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass behaviourEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see tangoSimLibSimulator.TangoSimLibSimulatorPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private TangoSimLibSimulatorPackageImpl() {
		super(eNS_URI, TangoSimLibSimulatorFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link TangoSimLibSimulatorPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static TangoSimLibSimulatorPackage init() {
		if (isInited) return (TangoSimLibSimulatorPackage)EPackage.Registry.INSTANCE.getEPackage(TangoSimLibSimulatorPackage.eNS_URI);

		// Obtain or create and register package
		TangoSimLibSimulatorPackageImpl theTangoSimLibSimulatorPackage = (TangoSimLibSimulatorPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof TangoSimLibSimulatorPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new TangoSimLibSimulatorPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		PogoDslPackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theTangoSimLibSimulatorPackage.createPackageContents();

		// Initialize created meta-data
		theTangoSimLibSimulatorPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theTangoSimLibSimulatorPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(TangoSimLibSimulatorPackage.eNS_URI, theTangoSimLibSimulatorPackage);
		return theTangoSimLibSimulatorPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTangoSimLib() {
		return tangoSimLibEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTangoSimLib_DataSimulations() {
		return (EReference)tangoSimLibEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTangoSimLib_CommandSimulations() {
		return (EReference)tangoSimLibEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTangoSimLib_OverrideClass() {
		return (EReference)tangoSimLibEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTangoSimLib_RefferedPogoDeviceClass() {
		return (EReference)tangoSimLibEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataSimulation() {
		return dataSimulationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataSimulation_PogoAttr() {
		return (EReference)dataSimulationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataSimulation_SimulationType() {
		return (EReference)dataSimulationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCommandSimulation() {
		return commandSimulationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCommandSimulation_PogoCommand() {
		return (EReference)commandSimulationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCommandSimulation_BehaviorType() {
		return (EReference)commandSimulationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getGaussianSlewLimited() {
		return gaussianSlewLimitedEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getGaussianSlewLimited_MinBound() {
		return (EAttribute)gaussianSlewLimitedEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getGaussianSlewLimited_MaxBound() {
		return (EAttribute)gaussianSlewLimitedEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getGaussianSlewLimited_Mean() {
		return (EAttribute)gaussianSlewLimitedEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getGaussianSlewLimited_SlewRate() {
		return (EAttribute)gaussianSlewLimitedEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getGaussianSlewLimited_UpdatePeriod() {
		return (EAttribute)gaussianSlewLimitedEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getConstantQuantity() {
		return constantQuantityEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getConstantQuantity_InitialValue() {
		return (EAttribute)constantQuantityEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getConstantQuantity_Quality() {
		return (EAttribute)constantQuantityEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDeterministicSignal() {
		return deterministicSignalEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDeterministicSignal_Type() {
		return (EAttribute)deterministicSignalEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDeterministicSignal_Amplitude() {
		return (EAttribute)deterministicSignalEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDeterministicSignal_Period() {
		return (EAttribute)deterministicSignalEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDeterministicSignal_Offset() {
		return (EAttribute)deterministicSignalEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRuntimeSpecifiedWaveform() {
		return runtimeSpecifiedWaveformEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRuntimeSpecifiedWaveform_DefaultValue() {
		return (EAttribute)runtimeSpecifiedWaveformEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRuntimeSpecifiedWaveform_Timestamp() {
		return (EAttribute)runtimeSpecifiedWaveformEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRuntimeSpecifiedWaveform_Attribute_qualities() {
		return (EAttribute)runtimeSpecifiedWaveformEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getInputTransform() {
		return inputTransformEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getInputTransform_DestinationVariableName() {
		return (EAttribute)inputTransformEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSideEffects() {
		return sideEffectsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSideEffects_Source_variable() {
		return (EAttribute)sideEffectsEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSideEffects_Destination_quantity() {
		return (EAttribute)sideEffectsEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOutputReturn() {
		return outputReturnEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOutputReturn_Source_variable() {
		return (EAttribute)outputReturnEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOutputReturn_Source_quantity() {
		return (EAttribute)outputReturnEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOverrideClass() {
		return overrideClassEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOverrideClass_Name() {
		return (EAttribute)overrideClassEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOverrideClass_Module_directory() {
		return (EAttribute)overrideClassEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOverrideClass_Module_name() {
		return (EAttribute)overrideClassEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOverrideClass_Class_name() {
		return (EAttribute)overrideClassEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSimulation() {
		return simulationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBehaviour() {
		return behaviourEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TangoSimLibSimulatorFactory getTangoSimLibSimulatorFactory() {
		return (TangoSimLibSimulatorFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		tangoSimLibEClass = createEClass(TANGO_SIM_LIB);
		createEReference(tangoSimLibEClass, TANGO_SIM_LIB__REFFERED_POGO_DEVICE_CLASS);
		createEReference(tangoSimLibEClass, TANGO_SIM_LIB__DATA_SIMULATIONS);
		createEReference(tangoSimLibEClass, TANGO_SIM_LIB__COMMAND_SIMULATIONS);
		createEReference(tangoSimLibEClass, TANGO_SIM_LIB__OVERRIDE_CLASS);

		dataSimulationEClass = createEClass(DATA_SIMULATION);
		createEReference(dataSimulationEClass, DATA_SIMULATION__POGO_ATTR);
		createEReference(dataSimulationEClass, DATA_SIMULATION__SIMULATION_TYPE);

		commandSimulationEClass = createEClass(COMMAND_SIMULATION);
		createEReference(commandSimulationEClass, COMMAND_SIMULATION__POGO_COMMAND);
		createEReference(commandSimulationEClass, COMMAND_SIMULATION__BEHAVIOR_TYPE);

		gaussianSlewLimitedEClass = createEClass(GAUSSIAN_SLEW_LIMITED);
		createEAttribute(gaussianSlewLimitedEClass, GAUSSIAN_SLEW_LIMITED__MIN_BOUND);
		createEAttribute(gaussianSlewLimitedEClass, GAUSSIAN_SLEW_LIMITED__MAX_BOUND);
		createEAttribute(gaussianSlewLimitedEClass, GAUSSIAN_SLEW_LIMITED__MEAN);
		createEAttribute(gaussianSlewLimitedEClass, GAUSSIAN_SLEW_LIMITED__SLEW_RATE);
		createEAttribute(gaussianSlewLimitedEClass, GAUSSIAN_SLEW_LIMITED__UPDATE_PERIOD);

		constantQuantityEClass = createEClass(CONSTANT_QUANTITY);
		createEAttribute(constantQuantityEClass, CONSTANT_QUANTITY__INITIAL_VALUE);
		createEAttribute(constantQuantityEClass, CONSTANT_QUANTITY__QUALITY);

		deterministicSignalEClass = createEClass(DETERMINISTIC_SIGNAL);
		createEAttribute(deterministicSignalEClass, DETERMINISTIC_SIGNAL__TYPE);
		createEAttribute(deterministicSignalEClass, DETERMINISTIC_SIGNAL__AMPLITUDE);
		createEAttribute(deterministicSignalEClass, DETERMINISTIC_SIGNAL__PERIOD);
		createEAttribute(deterministicSignalEClass, DETERMINISTIC_SIGNAL__OFFSET);

		runtimeSpecifiedWaveformEClass = createEClass(RUNTIME_SPECIFIED_WAVEFORM);
		createEAttribute(runtimeSpecifiedWaveformEClass, RUNTIME_SPECIFIED_WAVEFORM__DEFAULT_VALUE);
		createEAttribute(runtimeSpecifiedWaveformEClass, RUNTIME_SPECIFIED_WAVEFORM__TIMESTAMP);
		createEAttribute(runtimeSpecifiedWaveformEClass, RUNTIME_SPECIFIED_WAVEFORM__ATTRIBUTE_QUALITIES);

		inputTransformEClass = createEClass(INPUT_TRANSFORM);
		createEAttribute(inputTransformEClass, INPUT_TRANSFORM__DESTINATION_VARIABLE_NAME);

		sideEffectsEClass = createEClass(SIDE_EFFECTS);
		createEAttribute(sideEffectsEClass, SIDE_EFFECTS__SOURCE_VARIABLE);
		createEAttribute(sideEffectsEClass, SIDE_EFFECTS__DESTINATION_QUANTITY);

		outputReturnEClass = createEClass(OUTPUT_RETURN);
		createEAttribute(outputReturnEClass, OUTPUT_RETURN__SOURCE_VARIABLE);
		createEAttribute(outputReturnEClass, OUTPUT_RETURN__SOURCE_QUANTITY);

		overrideClassEClass = createEClass(OVERRIDE_CLASS);
		createEAttribute(overrideClassEClass, OVERRIDE_CLASS__NAME);
		createEAttribute(overrideClassEClass, OVERRIDE_CLASS__MODULE_DIRECTORY);
		createEAttribute(overrideClassEClass, OVERRIDE_CLASS__MODULE_NAME);
		createEAttribute(overrideClassEClass, OVERRIDE_CLASS__CLASS_NAME);

		simulationEClass = createEClass(SIMULATION);

		behaviourEClass = createEClass(BEHAVIOUR);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		PogoDslPackage thePogoDslPackage = (PogoDslPackage)EPackage.Registry.INSTANCE.getEPackage(PogoDslPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		gaussianSlewLimitedEClass.getESuperTypes().add(this.getSimulation());
		constantQuantityEClass.getESuperTypes().add(this.getSimulation());
		deterministicSignalEClass.getESuperTypes().add(this.getSimulation());
		runtimeSpecifiedWaveformEClass.getESuperTypes().add(this.getSimulation());
		inputTransformEClass.getESuperTypes().add(this.getBehaviour());
		sideEffectsEClass.getESuperTypes().add(this.getBehaviour());
		outputReturnEClass.getESuperTypes().add(this.getBehaviour());

		// Initialize classes, features, and operations; add parameters
		initEClass(tangoSimLibEClass, TangoSimLib.class, "TangoSimLib", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getTangoSimLib_RefferedPogoDeviceClass(), thePogoDslPackage.getPogoDeviceClass(), null, "refferedPogoDeviceClass", null, 0, 1, TangoSimLib.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTangoSimLib_DataSimulations(), this.getDataSimulation(), null, "dataSimulations", null, 0, -1, TangoSimLib.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTangoSimLib_CommandSimulations(), this.getCommandSimulation(), null, "commandSimulations", null, 0, -1, TangoSimLib.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTangoSimLib_OverrideClass(), this.getOverrideClass(), null, "overrideClass", null, 0, -1, TangoSimLib.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(dataSimulationEClass, DataSimulation.class, "DataSimulation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDataSimulation_PogoAttr(), thePogoDslPackage.getAttribute(), null, "pogoAttr", null, 0, 1, DataSimulation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDataSimulation_SimulationType(), this.getSimulation(), null, "simulationType", null, 0, 1, DataSimulation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(commandSimulationEClass, CommandSimulation.class, "CommandSimulation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getCommandSimulation_PogoCommand(), thePogoDslPackage.getCommand(), null, "pogoCommand", null, 0, 1, CommandSimulation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCommandSimulation_BehaviorType(), this.getBehaviour(), null, "behaviorType", null, 0, 1, CommandSimulation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(gaussianSlewLimitedEClass, GaussianSlewLimited.class, "GaussianSlewLimited", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getGaussianSlewLimited_MinBound(), ecorePackage.getEFloat(), "minBound", null, 0, 1, GaussianSlewLimited.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getGaussianSlewLimited_MaxBound(), ecorePackage.getEFloat(), "maxBound", null, 0, 1, GaussianSlewLimited.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getGaussianSlewLimited_Mean(), ecorePackage.getEFloat(), "mean", null, 0, 1, GaussianSlewLimited.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getGaussianSlewLimited_SlewRate(), ecorePackage.getEFloat(), "slewRate", null, 0, 1, GaussianSlewLimited.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getGaussianSlewLimited_UpdatePeriod(), ecorePackage.getEFloat(), "updatePeriod", null, 0, 1, GaussianSlewLimited.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(constantQuantityEClass, ConstantQuantity.class, "ConstantQuantity", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getConstantQuantity_InitialValue(), ecorePackage.getEFloat(), "initialValue", null, 0, 1, ConstantQuantity.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getConstantQuantity_Quality(), ecorePackage.getEFloat(), "quality", null, 0, 1, ConstantQuantity.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(deterministicSignalEClass, DeterministicSignal.class, "DeterministicSignal", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getDeterministicSignal_Type(), ecorePackage.getEString(), "type", null, 0, 1, DeterministicSignal.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDeterministicSignal_Amplitude(), ecorePackage.getEFloat(), "amplitude", null, 0, 1, DeterministicSignal.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDeterministicSignal_Period(), ecorePackage.getEInt(), "period", null, 0, 1, DeterministicSignal.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDeterministicSignal_Offset(), ecorePackage.getEFloat(), "offset", null, 0, 1, DeterministicSignal.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(runtimeSpecifiedWaveformEClass, RuntimeSpecifiedWaveform.class, "RuntimeSpecifiedWaveform", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getRuntimeSpecifiedWaveform_DefaultValue(), ecorePackage.getEFloat(), "defaultValue", null, 0, 1, RuntimeSpecifiedWaveform.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRuntimeSpecifiedWaveform_Timestamp(), ecorePackage.getEString(), "timestamp", null, 0, 1, RuntimeSpecifiedWaveform.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRuntimeSpecifiedWaveform_Attribute_qualities(), ecorePackage.getEFloat(), "attribute_qualities", null, 0, 1, RuntimeSpecifiedWaveform.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(inputTransformEClass, InputTransform.class, "InputTransform", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getInputTransform_DestinationVariableName(), ecorePackage.getEString(), "destinationVariableName", null, 0, 1, InputTransform.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(sideEffectsEClass, SideEffects.class, "SideEffects", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getSideEffects_Source_variable(), ecorePackage.getEString(), "source_variable", null, 0, 1, SideEffects.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSideEffects_Destination_quantity(), ecorePackage.getEString(), "destination_quantity", null, 0, 1, SideEffects.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(outputReturnEClass, OutputReturn.class, "OutputReturn", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getOutputReturn_Source_variable(), ecorePackage.getEString(), "source_variable", null, 0, 1, OutputReturn.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getOutputReturn_Source_quantity(), ecorePackage.getEString(), "source_quantity", null, 0, 1, OutputReturn.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(overrideClassEClass, OverrideClass.class, "OverrideClass", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getOverrideClass_Name(), ecorePackage.getEString(), "name", null, 0, 1, OverrideClass.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getOverrideClass_Module_directory(), ecorePackage.getEString(), "module_directory", null, 0, 1, OverrideClass.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getOverrideClass_Module_name(), ecorePackage.getEString(), "module_name", null, 0, 1, OverrideClass.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getOverrideClass_Class_name(), ecorePackage.getEString(), "class_name", null, 0, 1, OverrideClass.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(simulationEClass, Simulation.class, "Simulation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(behaviourEClass, Behaviour.class, "Behaviour", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		// Create resource
		createResource(eNS_URI);
	}

} //TangoSimLibSimulatorPackageImpl
