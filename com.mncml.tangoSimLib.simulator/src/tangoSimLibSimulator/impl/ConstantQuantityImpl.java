/**
 */
package tangoSimLibSimulator.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import tangoSimLibSimulator.ConstantQuantity;
import tangoSimLibSimulator.TangoSimLibSimulatorPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Constant Quantity</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link tangoSimLibSimulator.impl.ConstantQuantityImpl#getInitialValue <em>Initial Value</em>}</li>
 *   <li>{@link tangoSimLibSimulator.impl.ConstantQuantityImpl#getQuality <em>Quality</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ConstantQuantityImpl extends SimulationImpl implements ConstantQuantity {
	/**
	 * The default value of the '{@link #getInitialValue() <em>Initial Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInitialValue()
	 * @generated
	 * @ordered
	 */
	protected static final float INITIAL_VALUE_EDEFAULT = 0.0F;

	/**
	 * The cached value of the '{@link #getInitialValue() <em>Initial Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInitialValue()
	 * @generated
	 * @ordered
	 */
	protected float initialValue = INITIAL_VALUE_EDEFAULT;

	/**
	 * The default value of the '{@link #getQuality() <em>Quality</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getQuality()
	 * @generated
	 * @ordered
	 */
	protected static final float QUALITY_EDEFAULT = 0.0F;

	/**
	 * The cached value of the '{@link #getQuality() <em>Quality</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getQuality()
	 * @generated
	 * @ordered
	 */
	protected float quality = QUALITY_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ConstantQuantityImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TangoSimLibSimulatorPackage.Literals.CONSTANT_QUANTITY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public float getInitialValue() {
		return initialValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInitialValue(float newInitialValue) {
		float oldInitialValue = initialValue;
		initialValue = newInitialValue;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TangoSimLibSimulatorPackage.CONSTANT_QUANTITY__INITIAL_VALUE, oldInitialValue, initialValue));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public float getQuality() {
		return quality;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setQuality(float newQuality) {
		float oldQuality = quality;
		quality = newQuality;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TangoSimLibSimulatorPackage.CONSTANT_QUANTITY__QUALITY, oldQuality, quality));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TangoSimLibSimulatorPackage.CONSTANT_QUANTITY__INITIAL_VALUE:
				return getInitialValue();
			case TangoSimLibSimulatorPackage.CONSTANT_QUANTITY__QUALITY:
				return getQuality();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TangoSimLibSimulatorPackage.CONSTANT_QUANTITY__INITIAL_VALUE:
				setInitialValue((Float)newValue);
				return;
			case TangoSimLibSimulatorPackage.CONSTANT_QUANTITY__QUALITY:
				setQuality((Float)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TangoSimLibSimulatorPackage.CONSTANT_QUANTITY__INITIAL_VALUE:
				setInitialValue(INITIAL_VALUE_EDEFAULT);
				return;
			case TangoSimLibSimulatorPackage.CONSTANT_QUANTITY__QUALITY:
				setQuality(QUALITY_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TangoSimLibSimulatorPackage.CONSTANT_QUANTITY__INITIAL_VALUE:
				return initialValue != INITIAL_VALUE_EDEFAULT;
			case TangoSimLibSimulatorPackage.CONSTANT_QUANTITY__QUALITY:
				return quality != QUALITY_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (initialValue: ");
		result.append(initialValue);
		result.append(", quality: ");
		result.append(quality);
		result.append(')');
		return result.toString();
	}

} //ConstantQuantityImpl
