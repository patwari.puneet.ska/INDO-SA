/**
 */
package tangoSimLibSimulator.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import tangoSimLibSimulator.InputTransform;
import tangoSimLibSimulator.TangoSimLibSimulatorPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Input Transform</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link tangoSimLibSimulator.impl.InputTransformImpl#getDestinationVariableName <em>Destination Variable Name</em>}</li>
 * </ul>
 *
 * @generated
 */
public class InputTransformImpl extends BehaviourImpl implements InputTransform {
	/**
	 * The default value of the '{@link #getDestinationVariableName() <em>Destination Variable Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDestinationVariableName()
	 * @generated
	 * @ordered
	 */
	protected static final String DESTINATION_VARIABLE_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDestinationVariableName() <em>Destination Variable Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDestinationVariableName()
	 * @generated
	 * @ordered
	 */
	protected String destinationVariableName = DESTINATION_VARIABLE_NAME_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected InputTransformImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TangoSimLibSimulatorPackage.Literals.INPUT_TRANSFORM;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDestinationVariableName() {
		return destinationVariableName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDestinationVariableName(String newDestinationVariableName) {
		String oldDestinationVariableName = destinationVariableName;
		destinationVariableName = newDestinationVariableName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TangoSimLibSimulatorPackage.INPUT_TRANSFORM__DESTINATION_VARIABLE_NAME, oldDestinationVariableName, destinationVariableName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TangoSimLibSimulatorPackage.INPUT_TRANSFORM__DESTINATION_VARIABLE_NAME:
				return getDestinationVariableName();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TangoSimLibSimulatorPackage.INPUT_TRANSFORM__DESTINATION_VARIABLE_NAME:
				setDestinationVariableName((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TangoSimLibSimulatorPackage.INPUT_TRANSFORM__DESTINATION_VARIABLE_NAME:
				setDestinationVariableName(DESTINATION_VARIABLE_NAME_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TangoSimLibSimulatorPackage.INPUT_TRANSFORM__DESTINATION_VARIABLE_NAME:
				return DESTINATION_VARIABLE_NAME_EDEFAULT == null ? destinationVariableName != null : !DESTINATION_VARIABLE_NAME_EDEFAULT.equals(destinationVariableName);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (destinationVariableName: ");
		result.append(destinationVariableName);
		result.append(')');
		return result.toString();
	}

} //InputTransformImpl
