/**
 */
package tangoSimLibSimulator.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import tangoSimLibSimulator.OutputReturn;
import tangoSimLibSimulator.TangoSimLibSimulatorPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Output Return</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link tangoSimLibSimulator.impl.OutputReturnImpl#getSource_variable <em>Source variable</em>}</li>
 *   <li>{@link tangoSimLibSimulator.impl.OutputReturnImpl#getSource_quantity <em>Source quantity</em>}</li>
 * </ul>
 *
 * @generated
 */
public class OutputReturnImpl extends BehaviourImpl implements OutputReturn {
	/**
	 * The default value of the '{@link #getSource_variable() <em>Source variable</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSource_variable()
	 * @generated
	 * @ordered
	 */
	protected static final String SOURCE_VARIABLE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSource_variable() <em>Source variable</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSource_variable()
	 * @generated
	 * @ordered
	 */
	protected String source_variable = SOURCE_VARIABLE_EDEFAULT;

	/**
	 * The default value of the '{@link #getSource_quantity() <em>Source quantity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSource_quantity()
	 * @generated
	 * @ordered
	 */
	protected static final String SOURCE_QUANTITY_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSource_quantity() <em>Source quantity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSource_quantity()
	 * @generated
	 * @ordered
	 */
	protected String source_quantity = SOURCE_QUANTITY_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected OutputReturnImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TangoSimLibSimulatorPackage.Literals.OUTPUT_RETURN;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getSource_variable() {
		return source_variable;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSource_variable(String newSource_variable) {
		String oldSource_variable = source_variable;
		source_variable = newSource_variable;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TangoSimLibSimulatorPackage.OUTPUT_RETURN__SOURCE_VARIABLE, oldSource_variable, source_variable));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getSource_quantity() {
		return source_quantity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSource_quantity(String newSource_quantity) {
		String oldSource_quantity = source_quantity;
		source_quantity = newSource_quantity;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TangoSimLibSimulatorPackage.OUTPUT_RETURN__SOURCE_QUANTITY, oldSource_quantity, source_quantity));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TangoSimLibSimulatorPackage.OUTPUT_RETURN__SOURCE_VARIABLE:
				return getSource_variable();
			case TangoSimLibSimulatorPackage.OUTPUT_RETURN__SOURCE_QUANTITY:
				return getSource_quantity();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TangoSimLibSimulatorPackage.OUTPUT_RETURN__SOURCE_VARIABLE:
				setSource_variable((String)newValue);
				return;
			case TangoSimLibSimulatorPackage.OUTPUT_RETURN__SOURCE_QUANTITY:
				setSource_quantity((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TangoSimLibSimulatorPackage.OUTPUT_RETURN__SOURCE_VARIABLE:
				setSource_variable(SOURCE_VARIABLE_EDEFAULT);
				return;
			case TangoSimLibSimulatorPackage.OUTPUT_RETURN__SOURCE_QUANTITY:
				setSource_quantity(SOURCE_QUANTITY_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TangoSimLibSimulatorPackage.OUTPUT_RETURN__SOURCE_VARIABLE:
				return SOURCE_VARIABLE_EDEFAULT == null ? source_variable != null : !SOURCE_VARIABLE_EDEFAULT.equals(source_variable);
			case TangoSimLibSimulatorPackage.OUTPUT_RETURN__SOURCE_QUANTITY:
				return SOURCE_QUANTITY_EDEFAULT == null ? source_quantity != null : !SOURCE_QUANTITY_EDEFAULT.equals(source_quantity);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (source_variable: ");
		result.append(source_variable);
		result.append(", source_quantity: ");
		result.append(source_quantity);
		result.append(')');
		return result.toString();
	}

} //OutputReturnImpl
