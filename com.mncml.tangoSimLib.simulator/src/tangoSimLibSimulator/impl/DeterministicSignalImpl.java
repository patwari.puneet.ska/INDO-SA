/**
 */
package tangoSimLibSimulator.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import tangoSimLibSimulator.DeterministicSignal;
import tangoSimLibSimulator.TangoSimLibSimulatorPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Deterministic Signal</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link tangoSimLibSimulator.impl.DeterministicSignalImpl#getType <em>Type</em>}</li>
 *   <li>{@link tangoSimLibSimulator.impl.DeterministicSignalImpl#getAmplitude <em>Amplitude</em>}</li>
 *   <li>{@link tangoSimLibSimulator.impl.DeterministicSignalImpl#getPeriod <em>Period</em>}</li>
 *   <li>{@link tangoSimLibSimulator.impl.DeterministicSignalImpl#getOffset <em>Offset</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DeterministicSignalImpl extends SimulationImpl implements DeterministicSignal {
	/**
	 * The default value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected static final String TYPE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected String type = TYPE_EDEFAULT;

	/**
	 * The default value of the '{@link #getAmplitude() <em>Amplitude</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAmplitude()
	 * @generated
	 * @ordered
	 */
	protected static final float AMPLITUDE_EDEFAULT = 0.0F;

	/**
	 * The cached value of the '{@link #getAmplitude() <em>Amplitude</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAmplitude()
	 * @generated
	 * @ordered
	 */
	protected float amplitude = AMPLITUDE_EDEFAULT;

	/**
	 * The default value of the '{@link #getPeriod() <em>Period</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPeriod()
	 * @generated
	 * @ordered
	 */
	protected static final int PERIOD_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getPeriod() <em>Period</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPeriod()
	 * @generated
	 * @ordered
	 */
	protected int period = PERIOD_EDEFAULT;

	/**
	 * The default value of the '{@link #getOffset() <em>Offset</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOffset()
	 * @generated
	 * @ordered
	 */
	protected static final float OFFSET_EDEFAULT = 0.0F;

	/**
	 * The cached value of the '{@link #getOffset() <em>Offset</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOffset()
	 * @generated
	 * @ordered
	 */
	protected float offset = OFFSET_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DeterministicSignalImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TangoSimLibSimulatorPackage.Literals.DETERMINISTIC_SIGNAL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getType() {
		return type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setType(String newType) {
		String oldType = type;
		type = newType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TangoSimLibSimulatorPackage.DETERMINISTIC_SIGNAL__TYPE, oldType, type));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public float getAmplitude() {
		return amplitude;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAmplitude(float newAmplitude) {
		float oldAmplitude = amplitude;
		amplitude = newAmplitude;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TangoSimLibSimulatorPackage.DETERMINISTIC_SIGNAL__AMPLITUDE, oldAmplitude, amplitude));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getPeriod() {
		return period;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPeriod(int newPeriod) {
		int oldPeriod = period;
		period = newPeriod;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TangoSimLibSimulatorPackage.DETERMINISTIC_SIGNAL__PERIOD, oldPeriod, period));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public float getOffset() {
		return offset;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOffset(float newOffset) {
		float oldOffset = offset;
		offset = newOffset;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TangoSimLibSimulatorPackage.DETERMINISTIC_SIGNAL__OFFSET, oldOffset, offset));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TangoSimLibSimulatorPackage.DETERMINISTIC_SIGNAL__TYPE:
				return getType();
			case TangoSimLibSimulatorPackage.DETERMINISTIC_SIGNAL__AMPLITUDE:
				return getAmplitude();
			case TangoSimLibSimulatorPackage.DETERMINISTIC_SIGNAL__PERIOD:
				return getPeriod();
			case TangoSimLibSimulatorPackage.DETERMINISTIC_SIGNAL__OFFSET:
				return getOffset();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TangoSimLibSimulatorPackage.DETERMINISTIC_SIGNAL__TYPE:
				setType((String)newValue);
				return;
			case TangoSimLibSimulatorPackage.DETERMINISTIC_SIGNAL__AMPLITUDE:
				setAmplitude((Float)newValue);
				return;
			case TangoSimLibSimulatorPackage.DETERMINISTIC_SIGNAL__PERIOD:
				setPeriod((Integer)newValue);
				return;
			case TangoSimLibSimulatorPackage.DETERMINISTIC_SIGNAL__OFFSET:
				setOffset((Float)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TangoSimLibSimulatorPackage.DETERMINISTIC_SIGNAL__TYPE:
				setType(TYPE_EDEFAULT);
				return;
			case TangoSimLibSimulatorPackage.DETERMINISTIC_SIGNAL__AMPLITUDE:
				setAmplitude(AMPLITUDE_EDEFAULT);
				return;
			case TangoSimLibSimulatorPackage.DETERMINISTIC_SIGNAL__PERIOD:
				setPeriod(PERIOD_EDEFAULT);
				return;
			case TangoSimLibSimulatorPackage.DETERMINISTIC_SIGNAL__OFFSET:
				setOffset(OFFSET_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TangoSimLibSimulatorPackage.DETERMINISTIC_SIGNAL__TYPE:
				return TYPE_EDEFAULT == null ? type != null : !TYPE_EDEFAULT.equals(type);
			case TangoSimLibSimulatorPackage.DETERMINISTIC_SIGNAL__AMPLITUDE:
				return amplitude != AMPLITUDE_EDEFAULT;
			case TangoSimLibSimulatorPackage.DETERMINISTIC_SIGNAL__PERIOD:
				return period != PERIOD_EDEFAULT;
			case TangoSimLibSimulatorPackage.DETERMINISTIC_SIGNAL__OFFSET:
				return offset != OFFSET_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (type: ");
		result.append(type);
		result.append(", amplitude: ");
		result.append(amplitude);
		result.append(", period: ");
		result.append(period);
		result.append(", offset: ");
		result.append(offset);
		result.append(')');
		return result.toString();
	}

} //DeterministicSignalImpl
