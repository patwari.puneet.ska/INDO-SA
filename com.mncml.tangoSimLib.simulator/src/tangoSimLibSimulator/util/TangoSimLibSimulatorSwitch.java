/**
 */
package tangoSimLibSimulator.util;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

import tangoSimLibSimulator.*;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see tangoSimLibSimulator.TangoSimLibSimulatorPackage
 * @generated
 */
public class TangoSimLibSimulatorSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static TangoSimLibSimulatorPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TangoSimLibSimulatorSwitch() {
		if (modelPackage == null) {
			modelPackage = TangoSimLibSimulatorPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case TangoSimLibSimulatorPackage.TANGO_SIM_LIB: {
				TangoSimLib tangoSimLib = (TangoSimLib)theEObject;
				T result = caseTangoSimLib(tangoSimLib);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TangoSimLibSimulatorPackage.DATA_SIMULATION: {
				DataSimulation dataSimulation = (DataSimulation)theEObject;
				T result = caseDataSimulation(dataSimulation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TangoSimLibSimulatorPackage.COMMAND_SIMULATION: {
				CommandSimulation commandSimulation = (CommandSimulation)theEObject;
				T result = caseCommandSimulation(commandSimulation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TangoSimLibSimulatorPackage.GAUSSIAN_SLEW_LIMITED: {
				GaussianSlewLimited gaussianSlewLimited = (GaussianSlewLimited)theEObject;
				T result = caseGaussianSlewLimited(gaussianSlewLimited);
				if (result == null) result = caseSimulation(gaussianSlewLimited);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TangoSimLibSimulatorPackage.CONSTANT_QUANTITY: {
				ConstantQuantity constantQuantity = (ConstantQuantity)theEObject;
				T result = caseConstantQuantity(constantQuantity);
				if (result == null) result = caseSimulation(constantQuantity);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TangoSimLibSimulatorPackage.DETERMINISTIC_SIGNAL: {
				DeterministicSignal deterministicSignal = (DeterministicSignal)theEObject;
				T result = caseDeterministicSignal(deterministicSignal);
				if (result == null) result = caseSimulation(deterministicSignal);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TangoSimLibSimulatorPackage.RUNTIME_SPECIFIED_WAVEFORM: {
				RuntimeSpecifiedWaveform runtimeSpecifiedWaveform = (RuntimeSpecifiedWaveform)theEObject;
				T result = caseRuntimeSpecifiedWaveform(runtimeSpecifiedWaveform);
				if (result == null) result = caseSimulation(runtimeSpecifiedWaveform);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TangoSimLibSimulatorPackage.INPUT_TRANSFORM: {
				InputTransform inputTransform = (InputTransform)theEObject;
				T result = caseInputTransform(inputTransform);
				if (result == null) result = caseBehaviour(inputTransform);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TangoSimLibSimulatorPackage.SIDE_EFFECTS: {
				SideEffects sideEffects = (SideEffects)theEObject;
				T result = caseSideEffects(sideEffects);
				if (result == null) result = caseBehaviour(sideEffects);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TangoSimLibSimulatorPackage.OUTPUT_RETURN: {
				OutputReturn outputReturn = (OutputReturn)theEObject;
				T result = caseOutputReturn(outputReturn);
				if (result == null) result = caseBehaviour(outputReturn);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TangoSimLibSimulatorPackage.OVERRIDE_CLASS: {
				OverrideClass overrideClass = (OverrideClass)theEObject;
				T result = caseOverrideClass(overrideClass);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TangoSimLibSimulatorPackage.SIMULATION: {
				Simulation simulation = (Simulation)theEObject;
				T result = caseSimulation(simulation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case TangoSimLibSimulatorPackage.BEHAVIOUR: {
				Behaviour behaviour = (Behaviour)theEObject;
				T result = caseBehaviour(behaviour);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Tango Sim Lib</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Tango Sim Lib</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTangoSimLib(TangoSimLib object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Simulation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Simulation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataSimulation(DataSimulation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Command Simulation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Command Simulation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCommandSimulation(CommandSimulation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Gaussian Slew Limited</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Gaussian Slew Limited</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGaussianSlewLimited(GaussianSlewLimited object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Constant Quantity</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Constant Quantity</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseConstantQuantity(ConstantQuantity object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Deterministic Signal</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Deterministic Signal</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDeterministicSignal(DeterministicSignal object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Runtime Specified Waveform</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Runtime Specified Waveform</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRuntimeSpecifiedWaveform(RuntimeSpecifiedWaveform object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Input Transform</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Input Transform</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseInputTransform(InputTransform object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Side Effects</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Side Effects</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSideEffects(SideEffects object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Output Return</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Output Return</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOutputReturn(OutputReturn object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Override Class</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Override Class</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOverrideClass(OverrideClass object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Simulation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Simulation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSimulation(Simulation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Behaviour</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Behaviour</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBehaviour(Behaviour object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //TangoSimLibSimulatorSwitch
