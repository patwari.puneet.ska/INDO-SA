/**
 */
package tangoSimLibSimulator.util;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

import tangoSimLibSimulator.*;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see tangoSimLibSimulator.TangoSimLibSimulatorPackage
 * @generated
 */
public class TangoSimLibSimulatorAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static TangoSimLibSimulatorPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TangoSimLibSimulatorAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = TangoSimLibSimulatorPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TangoSimLibSimulatorSwitch<Adapter> modelSwitch =
		new TangoSimLibSimulatorSwitch<Adapter>() {
			@Override
			public Adapter caseTangoSimLib(TangoSimLib object) {
				return createTangoSimLibAdapter();
			}
			@Override
			public Adapter caseDataSimulation(DataSimulation object) {
				return createDataSimulationAdapter();
			}
			@Override
			public Adapter caseCommandSimulation(CommandSimulation object) {
				return createCommandSimulationAdapter();
			}
			@Override
			public Adapter caseGaussianSlewLimited(GaussianSlewLimited object) {
				return createGaussianSlewLimitedAdapter();
			}
			@Override
			public Adapter caseConstantQuantity(ConstantQuantity object) {
				return createConstantQuantityAdapter();
			}
			@Override
			public Adapter caseDeterministicSignal(DeterministicSignal object) {
				return createDeterministicSignalAdapter();
			}
			@Override
			public Adapter caseRuntimeSpecifiedWaveform(RuntimeSpecifiedWaveform object) {
				return createRuntimeSpecifiedWaveformAdapter();
			}
			@Override
			public Adapter caseInputTransform(InputTransform object) {
				return createInputTransformAdapter();
			}
			@Override
			public Adapter caseSideEffects(SideEffects object) {
				return createSideEffectsAdapter();
			}
			@Override
			public Adapter caseOutputReturn(OutputReturn object) {
				return createOutputReturnAdapter();
			}
			@Override
			public Adapter caseOverrideClass(OverrideClass object) {
				return createOverrideClassAdapter();
			}
			@Override
			public Adapter caseSimulation(Simulation object) {
				return createSimulationAdapter();
			}
			@Override
			public Adapter caseBehaviour(Behaviour object) {
				return createBehaviourAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link tangoSimLibSimulator.TangoSimLib <em>Tango Sim Lib</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see tangoSimLibSimulator.TangoSimLib
	 * @generated
	 */
	public Adapter createTangoSimLibAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link tangoSimLibSimulator.DataSimulation <em>Data Simulation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see tangoSimLibSimulator.DataSimulation
	 * @generated
	 */
	public Adapter createDataSimulationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link tangoSimLibSimulator.CommandSimulation <em>Command Simulation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see tangoSimLibSimulator.CommandSimulation
	 * @generated
	 */
	public Adapter createCommandSimulationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link tangoSimLibSimulator.GaussianSlewLimited <em>Gaussian Slew Limited</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see tangoSimLibSimulator.GaussianSlewLimited
	 * @generated
	 */
	public Adapter createGaussianSlewLimitedAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link tangoSimLibSimulator.ConstantQuantity <em>Constant Quantity</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see tangoSimLibSimulator.ConstantQuantity
	 * @generated
	 */
	public Adapter createConstantQuantityAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link tangoSimLibSimulator.DeterministicSignal <em>Deterministic Signal</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see tangoSimLibSimulator.DeterministicSignal
	 * @generated
	 */
	public Adapter createDeterministicSignalAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link tangoSimLibSimulator.RuntimeSpecifiedWaveform <em>Runtime Specified Waveform</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see tangoSimLibSimulator.RuntimeSpecifiedWaveform
	 * @generated
	 */
	public Adapter createRuntimeSpecifiedWaveformAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link tangoSimLibSimulator.InputTransform <em>Input Transform</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see tangoSimLibSimulator.InputTransform
	 * @generated
	 */
	public Adapter createInputTransformAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link tangoSimLibSimulator.SideEffects <em>Side Effects</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see tangoSimLibSimulator.SideEffects
	 * @generated
	 */
	public Adapter createSideEffectsAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link tangoSimLibSimulator.OutputReturn <em>Output Return</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see tangoSimLibSimulator.OutputReturn
	 * @generated
	 */
	public Adapter createOutputReturnAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link tangoSimLibSimulator.OverrideClass <em>Override Class</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see tangoSimLibSimulator.OverrideClass
	 * @generated
	 */
	public Adapter createOverrideClassAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link tangoSimLibSimulator.Simulation <em>Simulation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see tangoSimLibSimulator.Simulation
	 * @generated
	 */
	public Adapter createSimulationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link tangoSimLibSimulator.Behaviour <em>Behaviour</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see tangoSimLibSimulator.Behaviour
	 * @generated
	 */
	public Adapter createBehaviourAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //TangoSimLibSimulatorAdapterFactory
