/**
 */
package tangoSimLibSimulator;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Input Transform</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link tangoSimLibSimulator.InputTransform#getDestinationVariableName <em>Destination Variable Name</em>}</li>
 * </ul>
 *
 * @see tangoSimLibSimulator.TangoSimLibSimulatorPackage#getInputTransform()
 * @model
 * @generated
 */
public interface InputTransform extends Behaviour {
	/**
	 * Returns the value of the '<em><b>Destination Variable Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Destination Variable Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Destination Variable Name</em>' attribute.
	 * @see #setDestinationVariableName(String)
	 * @see tangoSimLibSimulator.TangoSimLibSimulatorPackage#getInputTransform_DestinationVariableName()
	 * @model
	 * @generated
	 */
	String getDestinationVariableName();

	/**
	 * Sets the value of the '{@link tangoSimLibSimulator.InputTransform#getDestinationVariableName <em>Destination Variable Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Destination Variable Name</em>' attribute.
	 * @see #getDestinationVariableName()
	 * @generated
	 */
	void setDestinationVariableName(String value);

} // InputTransform
