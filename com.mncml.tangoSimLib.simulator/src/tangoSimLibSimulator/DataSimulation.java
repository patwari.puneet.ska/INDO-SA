/**
 */
package tangoSimLibSimulator;

import org.eclipse.emf.ecore.EObject;

import pogoDsl.Attribute;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Data Simulation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link tangoSimLibSimulator.DataSimulation#getPogoAttr <em>Pogo Attr</em>}</li>
 *   <li>{@link tangoSimLibSimulator.DataSimulation#getSimulationType <em>Simulation Type</em>}</li>
 * </ul>
 *
 * @see tangoSimLibSimulator.TangoSimLibSimulatorPackage#getDataSimulation()
 * @model
 * @generated
 */
public interface DataSimulation extends EObject {
	/**
	 * Returns the value of the '<em><b>Pogo Attr</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Pogo Attr</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Pogo Attr</em>' reference.
	 * @see #setPogoAttr(Attribute)
	 * @see tangoSimLibSimulator.TangoSimLibSimulatorPackage#getDataSimulation_PogoAttr()
	 * @model
	 * @generated
	 */
	Attribute getPogoAttr();

	/**
	 * Sets the value of the '{@link tangoSimLibSimulator.DataSimulation#getPogoAttr <em>Pogo Attr</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Pogo Attr</em>' reference.
	 * @see #getPogoAttr()
	 * @generated
	 */
	void setPogoAttr(Attribute value);

	/**
	 * Returns the value of the '<em><b>Simulation Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Simulation Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Simulation Type</em>' containment reference.
	 * @see #setSimulationType(Simulation)
	 * @see tangoSimLibSimulator.TangoSimLibSimulatorPackage#getDataSimulation_SimulationType()
	 * @model containment="true"
	 * @generated
	 */
	Simulation getSimulationType();

	/**
	 * Sets the value of the '{@link tangoSimLibSimulator.DataSimulation#getSimulationType <em>Simulation Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Simulation Type</em>' containment reference.
	 * @see #getSimulationType()
	 * @generated
	 */
	void setSimulationType(Simulation value);

} // DataSimulation
