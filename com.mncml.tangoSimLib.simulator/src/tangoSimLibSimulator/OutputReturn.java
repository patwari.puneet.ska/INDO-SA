/**
 */
package tangoSimLibSimulator;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Output Return</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link tangoSimLibSimulator.OutputReturn#getSource_variable <em>Source variable</em>}</li>
 *   <li>{@link tangoSimLibSimulator.OutputReturn#getSource_quantity <em>Source quantity</em>}</li>
 * </ul>
 *
 * @see tangoSimLibSimulator.TangoSimLibSimulatorPackage#getOutputReturn()
 * @model
 * @generated
 */
public interface OutputReturn extends Behaviour {
	/**
	 * Returns the value of the '<em><b>Source variable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Source variable</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source variable</em>' attribute.
	 * @see #setSource_variable(String)
	 * @see tangoSimLibSimulator.TangoSimLibSimulatorPackage#getOutputReturn_Source_variable()
	 * @model
	 * @generated
	 */
	String getSource_variable();

	/**
	 * Sets the value of the '{@link tangoSimLibSimulator.OutputReturn#getSource_variable <em>Source variable</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Source variable</em>' attribute.
	 * @see #getSource_variable()
	 * @generated
	 */
	void setSource_variable(String value);

	/**
	 * Returns the value of the '<em><b>Source quantity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Source quantity</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source quantity</em>' attribute.
	 * @see #setSource_quantity(String)
	 * @see tangoSimLibSimulator.TangoSimLibSimulatorPackage#getOutputReturn_Source_quantity()
	 * @model
	 * @generated
	 */
	String getSource_quantity();

	/**
	 * Sets the value of the '{@link tangoSimLibSimulator.OutputReturn#getSource_quantity <em>Source quantity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Source quantity</em>' attribute.
	 * @see #getSource_quantity()
	 * @generated
	 */
	void setSource_quantity(String value);

} // OutputReturn
