/**
 */
package tangoSimLibSimulator;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

import pogoDsl.PogoDeviceClass;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Tango Sim Lib</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link tangoSimLibSimulator.TangoSimLib#getRefferedPogoDeviceClass <em>Reffered Pogo Device Class</em>}</li>
 *   <li>{@link tangoSimLibSimulator.TangoSimLib#getDataSimulations <em>Data Simulations</em>}</li>
 *   <li>{@link tangoSimLibSimulator.TangoSimLib#getCommandSimulations <em>Command Simulations</em>}</li>
 *   <li>{@link tangoSimLibSimulator.TangoSimLib#getOverrideClass <em>Override Class</em>}</li>
 * </ul>
 *
 * @see tangoSimLibSimulator.TangoSimLibSimulatorPackage#getTangoSimLib()
 * @model
 * @generated
 */
public interface TangoSimLib extends EObject {
	/**
	 * Returns the value of the '<em><b>Data Simulations</b></em>' containment reference list.
	 * The list contents are of type {@link tangoSimLibSimulator.DataSimulation}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Data Simulations</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data Simulations</em>' containment reference list.
	 * @see tangoSimLibSimulator.TangoSimLibSimulatorPackage#getTangoSimLib_DataSimulations()
	 * @model containment="true"
	 * @generated
	 */
	EList<DataSimulation> getDataSimulations();

	/**
	 * Returns the value of the '<em><b>Command Simulations</b></em>' containment reference list.
	 * The list contents are of type {@link tangoSimLibSimulator.CommandSimulation}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Command Simulations</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Command Simulations</em>' containment reference list.
	 * @see tangoSimLibSimulator.TangoSimLibSimulatorPackage#getTangoSimLib_CommandSimulations()
	 * @model containment="true"
	 * @generated
	 */
	EList<CommandSimulation> getCommandSimulations();

	/**
	 * Returns the value of the '<em><b>Override Class</b></em>' containment reference list.
	 * The list contents are of type {@link tangoSimLibSimulator.OverrideClass}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Override Class</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Override Class</em>' containment reference list.
	 * @see tangoSimLibSimulator.TangoSimLibSimulatorPackage#getTangoSimLib_OverrideClass()
	 * @model containment="true"
	 * @generated
	 */
	EList<OverrideClass> getOverrideClass();

	/**
	 * Returns the value of the '<em><b>Reffered Pogo Device Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Reffered Pogo Device Class</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Reffered Pogo Device Class</em>' reference.
	 * @see #setRefferedPogoDeviceClass(PogoDeviceClass)
	 * @see tangoSimLibSimulator.TangoSimLibSimulatorPackage#getTangoSimLib_RefferedPogoDeviceClass()
	 * @model
	 * @generated
	 */
	PogoDeviceClass getRefferedPogoDeviceClass();

	/**
	 * Sets the value of the '{@link tangoSimLibSimulator.TangoSimLib#getRefferedPogoDeviceClass <em>Reffered Pogo Device Class</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Reffered Pogo Device Class</em>' reference.
	 * @see #getRefferedPogoDeviceClass()
	 * @generated
	 */
	void setRefferedPogoDeviceClass(PogoDeviceClass value);

} // TangoSimLib
