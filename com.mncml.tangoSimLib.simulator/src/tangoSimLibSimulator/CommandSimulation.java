/**
 */
package tangoSimLibSimulator;

import org.eclipse.emf.ecore.EObject;

import pogoDsl.Command;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Command Simulation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link tangoSimLibSimulator.CommandSimulation#getPogoCommand <em>Pogo Command</em>}</li>
 *   <li>{@link tangoSimLibSimulator.CommandSimulation#getBehaviorType <em>Behavior Type</em>}</li>
 * </ul>
 *
 * @see tangoSimLibSimulator.TangoSimLibSimulatorPackage#getCommandSimulation()
 * @model
 * @generated
 */
public interface CommandSimulation extends EObject {
	/**
	 * Returns the value of the '<em><b>Pogo Command</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Pogo Command</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Pogo Command</em>' reference.
	 * @see #setPogoCommand(Command)
	 * @see tangoSimLibSimulator.TangoSimLibSimulatorPackage#getCommandSimulation_PogoCommand()
	 * @model
	 * @generated
	 */
	Command getPogoCommand();

	/**
	 * Sets the value of the '{@link tangoSimLibSimulator.CommandSimulation#getPogoCommand <em>Pogo Command</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Pogo Command</em>' reference.
	 * @see #getPogoCommand()
	 * @generated
	 */
	void setPogoCommand(Command value);

	/**
	 * Returns the value of the '<em><b>Behavior Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Behavior Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Behavior Type</em>' containment reference.
	 * @see #setBehaviorType(Behaviour)
	 * @see tangoSimLibSimulator.TangoSimLibSimulatorPackage#getCommandSimulation_BehaviorType()
	 * @model containment="true"
	 * @generated
	 */
	Behaviour getBehaviorType();

	/**
	 * Sets the value of the '{@link tangoSimLibSimulator.CommandSimulation#getBehaviorType <em>Behavior Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Behavior Type</em>' containment reference.
	 * @see #getBehaviorType()
	 * @generated
	 */
	void setBehaviorType(Behaviour value);

} // CommandSimulation
