/**
 */
package tangoSimLibSimulator;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Override Class</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link tangoSimLibSimulator.OverrideClass#getName <em>Name</em>}</li>
 *   <li>{@link tangoSimLibSimulator.OverrideClass#getModule_directory <em>Module directory</em>}</li>
 *   <li>{@link tangoSimLibSimulator.OverrideClass#getModule_name <em>Module name</em>}</li>
 *   <li>{@link tangoSimLibSimulator.OverrideClass#getClass_name <em>Class name</em>}</li>
 * </ul>
 *
 * @see tangoSimLibSimulator.TangoSimLibSimulatorPackage#getOverrideClass()
 * @model
 * @generated
 */
public interface OverrideClass extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see tangoSimLibSimulator.TangoSimLibSimulatorPackage#getOverrideClass_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link tangoSimLibSimulator.OverrideClass#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Module directory</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Module directory</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Module directory</em>' attribute.
	 * @see #setModule_directory(String)
	 * @see tangoSimLibSimulator.TangoSimLibSimulatorPackage#getOverrideClass_Module_directory()
	 * @model
	 * @generated
	 */
	String getModule_directory();

	/**
	 * Sets the value of the '{@link tangoSimLibSimulator.OverrideClass#getModule_directory <em>Module directory</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Module directory</em>' attribute.
	 * @see #getModule_directory()
	 * @generated
	 */
	void setModule_directory(String value);

	/**
	 * Returns the value of the '<em><b>Module name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Module name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Module name</em>' attribute.
	 * @see #setModule_name(String)
	 * @see tangoSimLibSimulator.TangoSimLibSimulatorPackage#getOverrideClass_Module_name()
	 * @model
	 * @generated
	 */
	String getModule_name();

	/**
	 * Sets the value of the '{@link tangoSimLibSimulator.OverrideClass#getModule_name <em>Module name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Module name</em>' attribute.
	 * @see #getModule_name()
	 * @generated
	 */
	void setModule_name(String value);

	/**
	 * Returns the value of the '<em><b>Class name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Class name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Class name</em>' attribute.
	 * @see #setClass_name(String)
	 * @see tangoSimLibSimulator.TangoSimLibSimulatorPackage#getOverrideClass_Class_name()
	 * @model
	 * @generated
	 */
	String getClass_name();

	/**
	 * Sets the value of the '{@link tangoSimLibSimulator.OverrideClass#getClass_name <em>Class name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Class name</em>' attribute.
	 * @see #getClass_name()
	 * @generated
	 */
	void setClass_name(String value);

} // OverrideClass
