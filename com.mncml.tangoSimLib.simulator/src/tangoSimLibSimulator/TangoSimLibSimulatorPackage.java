/**
 */
package tangoSimLibSimulator;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see tangoSimLibSimulator.TangoSimLibSimulatorFactory
 * @model kind="package"
 * @generated
 */
public interface TangoSimLibSimulatorPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "tangoSimLibSimulator";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://tangoSimLibSimulator/1.0";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "tangoSimLibSimulator";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	TangoSimLibSimulatorPackage eINSTANCE = tangoSimLibSimulator.impl.TangoSimLibSimulatorPackageImpl.init();

	/**
	 * The meta object id for the '{@link tangoSimLibSimulator.impl.TangoSimLibImpl <em>Tango Sim Lib</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tangoSimLibSimulator.impl.TangoSimLibImpl
	 * @see tangoSimLibSimulator.impl.TangoSimLibSimulatorPackageImpl#getTangoSimLib()
	 * @generated
	 */
	int TANGO_SIM_LIB = 0;

	/**
	 * The feature id for the '<em><b>Reffered Pogo Device Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TANGO_SIM_LIB__REFFERED_POGO_DEVICE_CLASS = 0;

	/**
	 * The feature id for the '<em><b>Data Simulations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TANGO_SIM_LIB__DATA_SIMULATIONS = 1;

	/**
	 * The feature id for the '<em><b>Command Simulations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TANGO_SIM_LIB__COMMAND_SIMULATIONS = 2;

	/**
	 * The feature id for the '<em><b>Override Class</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TANGO_SIM_LIB__OVERRIDE_CLASS = 3;

	/**
	 * The number of structural features of the '<em>Tango Sim Lib</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TANGO_SIM_LIB_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>Tango Sim Lib</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TANGO_SIM_LIB_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link tangoSimLibSimulator.impl.DataSimulationImpl <em>Data Simulation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tangoSimLibSimulator.impl.DataSimulationImpl
	 * @see tangoSimLibSimulator.impl.TangoSimLibSimulatorPackageImpl#getDataSimulation()
	 * @generated
	 */
	int DATA_SIMULATION = 1;

	/**
	 * The feature id for the '<em><b>Pogo Attr</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_SIMULATION__POGO_ATTR = 0;

	/**
	 * The feature id for the '<em><b>Simulation Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_SIMULATION__SIMULATION_TYPE = 1;

	/**
	 * The number of structural features of the '<em>Data Simulation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_SIMULATION_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Data Simulation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_SIMULATION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link tangoSimLibSimulator.impl.CommandSimulationImpl <em>Command Simulation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tangoSimLibSimulator.impl.CommandSimulationImpl
	 * @see tangoSimLibSimulator.impl.TangoSimLibSimulatorPackageImpl#getCommandSimulation()
	 * @generated
	 */
	int COMMAND_SIMULATION = 2;

	/**
	 * The feature id for the '<em><b>Pogo Command</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMAND_SIMULATION__POGO_COMMAND = 0;

	/**
	 * The feature id for the '<em><b>Behavior Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMAND_SIMULATION__BEHAVIOR_TYPE = 1;

	/**
	 * The number of structural features of the '<em>Command Simulation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMAND_SIMULATION_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Command Simulation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMAND_SIMULATION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link tangoSimLibSimulator.impl.SimulationImpl <em>Simulation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tangoSimLibSimulator.impl.SimulationImpl
	 * @see tangoSimLibSimulator.impl.TangoSimLibSimulatorPackageImpl#getSimulation()
	 * @generated
	 */
	int SIMULATION = 11;

	/**
	 * The number of structural features of the '<em>Simulation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMULATION_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Simulation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMULATION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link tangoSimLibSimulator.impl.GaussianSlewLimitedImpl <em>Gaussian Slew Limited</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tangoSimLibSimulator.impl.GaussianSlewLimitedImpl
	 * @see tangoSimLibSimulator.impl.TangoSimLibSimulatorPackageImpl#getGaussianSlewLimited()
	 * @generated
	 */
	int GAUSSIAN_SLEW_LIMITED = 3;

	/**
	 * The feature id for the '<em><b>Min Bound</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GAUSSIAN_SLEW_LIMITED__MIN_BOUND = SIMULATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Max Bound</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GAUSSIAN_SLEW_LIMITED__MAX_BOUND = SIMULATION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Mean</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GAUSSIAN_SLEW_LIMITED__MEAN = SIMULATION_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Slew Rate</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GAUSSIAN_SLEW_LIMITED__SLEW_RATE = SIMULATION_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Update Period</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GAUSSIAN_SLEW_LIMITED__UPDATE_PERIOD = SIMULATION_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Gaussian Slew Limited</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GAUSSIAN_SLEW_LIMITED_FEATURE_COUNT = SIMULATION_FEATURE_COUNT + 5;

	/**
	 * The number of operations of the '<em>Gaussian Slew Limited</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GAUSSIAN_SLEW_LIMITED_OPERATION_COUNT = SIMULATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link tangoSimLibSimulator.impl.ConstantQuantityImpl <em>Constant Quantity</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tangoSimLibSimulator.impl.ConstantQuantityImpl
	 * @see tangoSimLibSimulator.impl.TangoSimLibSimulatorPackageImpl#getConstantQuantity()
	 * @generated
	 */
	int CONSTANT_QUANTITY = 4;

	/**
	 * The feature id for the '<em><b>Initial Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_QUANTITY__INITIAL_VALUE = SIMULATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Quality</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_QUANTITY__QUALITY = SIMULATION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Constant Quantity</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_QUANTITY_FEATURE_COUNT = SIMULATION_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Constant Quantity</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_QUANTITY_OPERATION_COUNT = SIMULATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link tangoSimLibSimulator.impl.DeterministicSignalImpl <em>Deterministic Signal</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tangoSimLibSimulator.impl.DeterministicSignalImpl
	 * @see tangoSimLibSimulator.impl.TangoSimLibSimulatorPackageImpl#getDeterministicSignal()
	 * @generated
	 */
	int DETERMINISTIC_SIGNAL = 5;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DETERMINISTIC_SIGNAL__TYPE = SIMULATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Amplitude</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DETERMINISTIC_SIGNAL__AMPLITUDE = SIMULATION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Period</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DETERMINISTIC_SIGNAL__PERIOD = SIMULATION_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Offset</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DETERMINISTIC_SIGNAL__OFFSET = SIMULATION_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Deterministic Signal</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DETERMINISTIC_SIGNAL_FEATURE_COUNT = SIMULATION_FEATURE_COUNT + 4;

	/**
	 * The number of operations of the '<em>Deterministic Signal</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DETERMINISTIC_SIGNAL_OPERATION_COUNT = SIMULATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link tangoSimLibSimulator.impl.RuntimeSpecifiedWaveformImpl <em>Runtime Specified Waveform</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tangoSimLibSimulator.impl.RuntimeSpecifiedWaveformImpl
	 * @see tangoSimLibSimulator.impl.TangoSimLibSimulatorPackageImpl#getRuntimeSpecifiedWaveform()
	 * @generated
	 */
	int RUNTIME_SPECIFIED_WAVEFORM = 6;

	/**
	 * The feature id for the '<em><b>Default Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RUNTIME_SPECIFIED_WAVEFORM__DEFAULT_VALUE = SIMULATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Timestamp</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RUNTIME_SPECIFIED_WAVEFORM__TIMESTAMP = SIMULATION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Attribute qualities</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RUNTIME_SPECIFIED_WAVEFORM__ATTRIBUTE_QUALITIES = SIMULATION_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Runtime Specified Waveform</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RUNTIME_SPECIFIED_WAVEFORM_FEATURE_COUNT = SIMULATION_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Runtime Specified Waveform</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RUNTIME_SPECIFIED_WAVEFORM_OPERATION_COUNT = SIMULATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link tangoSimLibSimulator.impl.BehaviourImpl <em>Behaviour</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tangoSimLibSimulator.impl.BehaviourImpl
	 * @see tangoSimLibSimulator.impl.TangoSimLibSimulatorPackageImpl#getBehaviour()
	 * @generated
	 */
	int BEHAVIOUR = 12;

	/**
	 * The number of structural features of the '<em>Behaviour</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BEHAVIOUR_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Behaviour</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BEHAVIOUR_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link tangoSimLibSimulator.impl.InputTransformImpl <em>Input Transform</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tangoSimLibSimulator.impl.InputTransformImpl
	 * @see tangoSimLibSimulator.impl.TangoSimLibSimulatorPackageImpl#getInputTransform()
	 * @generated
	 */
	int INPUT_TRANSFORM = 7;

	/**
	 * The feature id for the '<em><b>Destination Variable Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INPUT_TRANSFORM__DESTINATION_VARIABLE_NAME = BEHAVIOUR_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Input Transform</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INPUT_TRANSFORM_FEATURE_COUNT = BEHAVIOUR_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Input Transform</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INPUT_TRANSFORM_OPERATION_COUNT = BEHAVIOUR_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link tangoSimLibSimulator.impl.SideEffectsImpl <em>Side Effects</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tangoSimLibSimulator.impl.SideEffectsImpl
	 * @see tangoSimLibSimulator.impl.TangoSimLibSimulatorPackageImpl#getSideEffects()
	 * @generated
	 */
	int SIDE_EFFECTS = 8;

	/**
	 * The feature id for the '<em><b>Source variable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIDE_EFFECTS__SOURCE_VARIABLE = BEHAVIOUR_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Destination quantity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIDE_EFFECTS__DESTINATION_QUANTITY = BEHAVIOUR_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Side Effects</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIDE_EFFECTS_FEATURE_COUNT = BEHAVIOUR_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Side Effects</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIDE_EFFECTS_OPERATION_COUNT = BEHAVIOUR_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link tangoSimLibSimulator.impl.OutputReturnImpl <em>Output Return</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tangoSimLibSimulator.impl.OutputReturnImpl
	 * @see tangoSimLibSimulator.impl.TangoSimLibSimulatorPackageImpl#getOutputReturn()
	 * @generated
	 */
	int OUTPUT_RETURN = 9;

	/**
	 * The feature id for the '<em><b>Source variable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OUTPUT_RETURN__SOURCE_VARIABLE = BEHAVIOUR_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Source quantity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OUTPUT_RETURN__SOURCE_QUANTITY = BEHAVIOUR_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Output Return</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OUTPUT_RETURN_FEATURE_COUNT = BEHAVIOUR_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Output Return</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OUTPUT_RETURN_OPERATION_COUNT = BEHAVIOUR_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link tangoSimLibSimulator.impl.OverrideClassImpl <em>Override Class</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tangoSimLibSimulator.impl.OverrideClassImpl
	 * @see tangoSimLibSimulator.impl.TangoSimLibSimulatorPackageImpl#getOverrideClass()
	 * @generated
	 */
	int OVERRIDE_CLASS = 10;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OVERRIDE_CLASS__NAME = 0;

	/**
	 * The feature id for the '<em><b>Module directory</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OVERRIDE_CLASS__MODULE_DIRECTORY = 1;

	/**
	 * The feature id for the '<em><b>Module name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OVERRIDE_CLASS__MODULE_NAME = 2;

	/**
	 * The feature id for the '<em><b>Class name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OVERRIDE_CLASS__CLASS_NAME = 3;

	/**
	 * The number of structural features of the '<em>Override Class</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OVERRIDE_CLASS_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>Override Class</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OVERRIDE_CLASS_OPERATION_COUNT = 0;


	/**
	 * Returns the meta object for class '{@link tangoSimLibSimulator.TangoSimLib <em>Tango Sim Lib</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Tango Sim Lib</em>'.
	 * @see tangoSimLibSimulator.TangoSimLib
	 * @generated
	 */
	EClass getTangoSimLib();

	/**
	 * Returns the meta object for the containment reference list '{@link tangoSimLibSimulator.TangoSimLib#getDataSimulations <em>Data Simulations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Data Simulations</em>'.
	 * @see tangoSimLibSimulator.TangoSimLib#getDataSimulations()
	 * @see #getTangoSimLib()
	 * @generated
	 */
	EReference getTangoSimLib_DataSimulations();

	/**
	 * Returns the meta object for the containment reference list '{@link tangoSimLibSimulator.TangoSimLib#getCommandSimulations <em>Command Simulations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Command Simulations</em>'.
	 * @see tangoSimLibSimulator.TangoSimLib#getCommandSimulations()
	 * @see #getTangoSimLib()
	 * @generated
	 */
	EReference getTangoSimLib_CommandSimulations();

	/**
	 * Returns the meta object for the containment reference list '{@link tangoSimLibSimulator.TangoSimLib#getOverrideClass <em>Override Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Override Class</em>'.
	 * @see tangoSimLibSimulator.TangoSimLib#getOverrideClass()
	 * @see #getTangoSimLib()
	 * @generated
	 */
	EReference getTangoSimLib_OverrideClass();

	/**
	 * Returns the meta object for the reference '{@link tangoSimLibSimulator.TangoSimLib#getRefferedPogoDeviceClass <em>Reffered Pogo Device Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Reffered Pogo Device Class</em>'.
	 * @see tangoSimLibSimulator.TangoSimLib#getRefferedPogoDeviceClass()
	 * @see #getTangoSimLib()
	 * @generated
	 */
	EReference getTangoSimLib_RefferedPogoDeviceClass();

	/**
	 * Returns the meta object for class '{@link tangoSimLibSimulator.DataSimulation <em>Data Simulation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Data Simulation</em>'.
	 * @see tangoSimLibSimulator.DataSimulation
	 * @generated
	 */
	EClass getDataSimulation();

	/**
	 * Returns the meta object for the reference '{@link tangoSimLibSimulator.DataSimulation#getPogoAttr <em>Pogo Attr</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Pogo Attr</em>'.
	 * @see tangoSimLibSimulator.DataSimulation#getPogoAttr()
	 * @see #getDataSimulation()
	 * @generated
	 */
	EReference getDataSimulation_PogoAttr();

	/**
	 * Returns the meta object for the containment reference '{@link tangoSimLibSimulator.DataSimulation#getSimulationType <em>Simulation Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Simulation Type</em>'.
	 * @see tangoSimLibSimulator.DataSimulation#getSimulationType()
	 * @see #getDataSimulation()
	 * @generated
	 */
	EReference getDataSimulation_SimulationType();

	/**
	 * Returns the meta object for class '{@link tangoSimLibSimulator.CommandSimulation <em>Command Simulation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Command Simulation</em>'.
	 * @see tangoSimLibSimulator.CommandSimulation
	 * @generated
	 */
	EClass getCommandSimulation();

	/**
	 * Returns the meta object for the reference '{@link tangoSimLibSimulator.CommandSimulation#getPogoCommand <em>Pogo Command</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Pogo Command</em>'.
	 * @see tangoSimLibSimulator.CommandSimulation#getPogoCommand()
	 * @see #getCommandSimulation()
	 * @generated
	 */
	EReference getCommandSimulation_PogoCommand();

	/**
	 * Returns the meta object for the containment reference '{@link tangoSimLibSimulator.CommandSimulation#getBehaviorType <em>Behavior Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Behavior Type</em>'.
	 * @see tangoSimLibSimulator.CommandSimulation#getBehaviorType()
	 * @see #getCommandSimulation()
	 * @generated
	 */
	EReference getCommandSimulation_BehaviorType();

	/**
	 * Returns the meta object for class '{@link tangoSimLibSimulator.GaussianSlewLimited <em>Gaussian Slew Limited</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Gaussian Slew Limited</em>'.
	 * @see tangoSimLibSimulator.GaussianSlewLimited
	 * @generated
	 */
	EClass getGaussianSlewLimited();

	/**
	 * Returns the meta object for the attribute '{@link tangoSimLibSimulator.GaussianSlewLimited#getMinBound <em>Min Bound</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Min Bound</em>'.
	 * @see tangoSimLibSimulator.GaussianSlewLimited#getMinBound()
	 * @see #getGaussianSlewLimited()
	 * @generated
	 */
	EAttribute getGaussianSlewLimited_MinBound();

	/**
	 * Returns the meta object for the attribute '{@link tangoSimLibSimulator.GaussianSlewLimited#getMaxBound <em>Max Bound</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Max Bound</em>'.
	 * @see tangoSimLibSimulator.GaussianSlewLimited#getMaxBound()
	 * @see #getGaussianSlewLimited()
	 * @generated
	 */
	EAttribute getGaussianSlewLimited_MaxBound();

	/**
	 * Returns the meta object for the attribute '{@link tangoSimLibSimulator.GaussianSlewLimited#getMean <em>Mean</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Mean</em>'.
	 * @see tangoSimLibSimulator.GaussianSlewLimited#getMean()
	 * @see #getGaussianSlewLimited()
	 * @generated
	 */
	EAttribute getGaussianSlewLimited_Mean();

	/**
	 * Returns the meta object for the attribute '{@link tangoSimLibSimulator.GaussianSlewLimited#getSlewRate <em>Slew Rate</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Slew Rate</em>'.
	 * @see tangoSimLibSimulator.GaussianSlewLimited#getSlewRate()
	 * @see #getGaussianSlewLimited()
	 * @generated
	 */
	EAttribute getGaussianSlewLimited_SlewRate();

	/**
	 * Returns the meta object for the attribute '{@link tangoSimLibSimulator.GaussianSlewLimited#getUpdatePeriod <em>Update Period</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Update Period</em>'.
	 * @see tangoSimLibSimulator.GaussianSlewLimited#getUpdatePeriod()
	 * @see #getGaussianSlewLimited()
	 * @generated
	 */
	EAttribute getGaussianSlewLimited_UpdatePeriod();

	/**
	 * Returns the meta object for class '{@link tangoSimLibSimulator.ConstantQuantity <em>Constant Quantity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Constant Quantity</em>'.
	 * @see tangoSimLibSimulator.ConstantQuantity
	 * @generated
	 */
	EClass getConstantQuantity();

	/**
	 * Returns the meta object for the attribute '{@link tangoSimLibSimulator.ConstantQuantity#getInitialValue <em>Initial Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Initial Value</em>'.
	 * @see tangoSimLibSimulator.ConstantQuantity#getInitialValue()
	 * @see #getConstantQuantity()
	 * @generated
	 */
	EAttribute getConstantQuantity_InitialValue();

	/**
	 * Returns the meta object for the attribute '{@link tangoSimLibSimulator.ConstantQuantity#getQuality <em>Quality</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Quality</em>'.
	 * @see tangoSimLibSimulator.ConstantQuantity#getQuality()
	 * @see #getConstantQuantity()
	 * @generated
	 */
	EAttribute getConstantQuantity_Quality();

	/**
	 * Returns the meta object for class '{@link tangoSimLibSimulator.DeterministicSignal <em>Deterministic Signal</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Deterministic Signal</em>'.
	 * @see tangoSimLibSimulator.DeterministicSignal
	 * @generated
	 */
	EClass getDeterministicSignal();

	/**
	 * Returns the meta object for the attribute '{@link tangoSimLibSimulator.DeterministicSignal#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see tangoSimLibSimulator.DeterministicSignal#getType()
	 * @see #getDeterministicSignal()
	 * @generated
	 */
	EAttribute getDeterministicSignal_Type();

	/**
	 * Returns the meta object for the attribute '{@link tangoSimLibSimulator.DeterministicSignal#getAmplitude <em>Amplitude</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Amplitude</em>'.
	 * @see tangoSimLibSimulator.DeterministicSignal#getAmplitude()
	 * @see #getDeterministicSignal()
	 * @generated
	 */
	EAttribute getDeterministicSignal_Amplitude();

	/**
	 * Returns the meta object for the attribute '{@link tangoSimLibSimulator.DeterministicSignal#getPeriod <em>Period</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Period</em>'.
	 * @see tangoSimLibSimulator.DeterministicSignal#getPeriod()
	 * @see #getDeterministicSignal()
	 * @generated
	 */
	EAttribute getDeterministicSignal_Period();

	/**
	 * Returns the meta object for the attribute '{@link tangoSimLibSimulator.DeterministicSignal#getOffset <em>Offset</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Offset</em>'.
	 * @see tangoSimLibSimulator.DeterministicSignal#getOffset()
	 * @see #getDeterministicSignal()
	 * @generated
	 */
	EAttribute getDeterministicSignal_Offset();

	/**
	 * Returns the meta object for class '{@link tangoSimLibSimulator.RuntimeSpecifiedWaveform <em>Runtime Specified Waveform</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Runtime Specified Waveform</em>'.
	 * @see tangoSimLibSimulator.RuntimeSpecifiedWaveform
	 * @generated
	 */
	EClass getRuntimeSpecifiedWaveform();

	/**
	 * Returns the meta object for the attribute '{@link tangoSimLibSimulator.RuntimeSpecifiedWaveform#getDefaultValue <em>Default Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Default Value</em>'.
	 * @see tangoSimLibSimulator.RuntimeSpecifiedWaveform#getDefaultValue()
	 * @see #getRuntimeSpecifiedWaveform()
	 * @generated
	 */
	EAttribute getRuntimeSpecifiedWaveform_DefaultValue();

	/**
	 * Returns the meta object for the attribute '{@link tangoSimLibSimulator.RuntimeSpecifiedWaveform#getTimestamp <em>Timestamp</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Timestamp</em>'.
	 * @see tangoSimLibSimulator.RuntimeSpecifiedWaveform#getTimestamp()
	 * @see #getRuntimeSpecifiedWaveform()
	 * @generated
	 */
	EAttribute getRuntimeSpecifiedWaveform_Timestamp();

	/**
	 * Returns the meta object for the attribute '{@link tangoSimLibSimulator.RuntimeSpecifiedWaveform#getAttribute_qualities <em>Attribute qualities</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Attribute qualities</em>'.
	 * @see tangoSimLibSimulator.RuntimeSpecifiedWaveform#getAttribute_qualities()
	 * @see #getRuntimeSpecifiedWaveform()
	 * @generated
	 */
	EAttribute getRuntimeSpecifiedWaveform_Attribute_qualities();

	/**
	 * Returns the meta object for class '{@link tangoSimLibSimulator.InputTransform <em>Input Transform</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Input Transform</em>'.
	 * @see tangoSimLibSimulator.InputTransform
	 * @generated
	 */
	EClass getInputTransform();

	/**
	 * Returns the meta object for the attribute '{@link tangoSimLibSimulator.InputTransform#getDestinationVariableName <em>Destination Variable Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Destination Variable Name</em>'.
	 * @see tangoSimLibSimulator.InputTransform#getDestinationVariableName()
	 * @see #getInputTransform()
	 * @generated
	 */
	EAttribute getInputTransform_DestinationVariableName();

	/**
	 * Returns the meta object for class '{@link tangoSimLibSimulator.SideEffects <em>Side Effects</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Side Effects</em>'.
	 * @see tangoSimLibSimulator.SideEffects
	 * @generated
	 */
	EClass getSideEffects();

	/**
	 * Returns the meta object for the attribute '{@link tangoSimLibSimulator.SideEffects#getSource_variable <em>Source variable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Source variable</em>'.
	 * @see tangoSimLibSimulator.SideEffects#getSource_variable()
	 * @see #getSideEffects()
	 * @generated
	 */
	EAttribute getSideEffects_Source_variable();

	/**
	 * Returns the meta object for the attribute '{@link tangoSimLibSimulator.SideEffects#getDestination_quantity <em>Destination quantity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Destination quantity</em>'.
	 * @see tangoSimLibSimulator.SideEffects#getDestination_quantity()
	 * @see #getSideEffects()
	 * @generated
	 */
	EAttribute getSideEffects_Destination_quantity();

	/**
	 * Returns the meta object for class '{@link tangoSimLibSimulator.OutputReturn <em>Output Return</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Output Return</em>'.
	 * @see tangoSimLibSimulator.OutputReturn
	 * @generated
	 */
	EClass getOutputReturn();

	/**
	 * Returns the meta object for the attribute '{@link tangoSimLibSimulator.OutputReturn#getSource_variable <em>Source variable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Source variable</em>'.
	 * @see tangoSimLibSimulator.OutputReturn#getSource_variable()
	 * @see #getOutputReturn()
	 * @generated
	 */
	EAttribute getOutputReturn_Source_variable();

	/**
	 * Returns the meta object for the attribute '{@link tangoSimLibSimulator.OutputReturn#getSource_quantity <em>Source quantity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Source quantity</em>'.
	 * @see tangoSimLibSimulator.OutputReturn#getSource_quantity()
	 * @see #getOutputReturn()
	 * @generated
	 */
	EAttribute getOutputReturn_Source_quantity();

	/**
	 * Returns the meta object for class '{@link tangoSimLibSimulator.OverrideClass <em>Override Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Override Class</em>'.
	 * @see tangoSimLibSimulator.OverrideClass
	 * @generated
	 */
	EClass getOverrideClass();

	/**
	 * Returns the meta object for the attribute '{@link tangoSimLibSimulator.OverrideClass#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see tangoSimLibSimulator.OverrideClass#getName()
	 * @see #getOverrideClass()
	 * @generated
	 */
	EAttribute getOverrideClass_Name();

	/**
	 * Returns the meta object for the attribute '{@link tangoSimLibSimulator.OverrideClass#getModule_directory <em>Module directory</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Module directory</em>'.
	 * @see tangoSimLibSimulator.OverrideClass#getModule_directory()
	 * @see #getOverrideClass()
	 * @generated
	 */
	EAttribute getOverrideClass_Module_directory();

	/**
	 * Returns the meta object for the attribute '{@link tangoSimLibSimulator.OverrideClass#getModule_name <em>Module name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Module name</em>'.
	 * @see tangoSimLibSimulator.OverrideClass#getModule_name()
	 * @see #getOverrideClass()
	 * @generated
	 */
	EAttribute getOverrideClass_Module_name();

	/**
	 * Returns the meta object for the attribute '{@link tangoSimLibSimulator.OverrideClass#getClass_name <em>Class name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Class name</em>'.
	 * @see tangoSimLibSimulator.OverrideClass#getClass_name()
	 * @see #getOverrideClass()
	 * @generated
	 */
	EAttribute getOverrideClass_Class_name();

	/**
	 * Returns the meta object for class '{@link tangoSimLibSimulator.Simulation <em>Simulation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Simulation</em>'.
	 * @see tangoSimLibSimulator.Simulation
	 * @generated
	 */
	EClass getSimulation();

	/**
	 * Returns the meta object for class '{@link tangoSimLibSimulator.Behaviour <em>Behaviour</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Behaviour</em>'.
	 * @see tangoSimLibSimulator.Behaviour
	 * @generated
	 */
	EClass getBehaviour();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	TangoSimLibSimulatorFactory getTangoSimLibSimulatorFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link tangoSimLibSimulator.impl.TangoSimLibImpl <em>Tango Sim Lib</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see tangoSimLibSimulator.impl.TangoSimLibImpl
		 * @see tangoSimLibSimulator.impl.TangoSimLibSimulatorPackageImpl#getTangoSimLib()
		 * @generated
		 */
		EClass TANGO_SIM_LIB = eINSTANCE.getTangoSimLib();

		/**
		 * The meta object literal for the '<em><b>Data Simulations</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TANGO_SIM_LIB__DATA_SIMULATIONS = eINSTANCE.getTangoSimLib_DataSimulations();

		/**
		 * The meta object literal for the '<em><b>Command Simulations</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TANGO_SIM_LIB__COMMAND_SIMULATIONS = eINSTANCE.getTangoSimLib_CommandSimulations();

		/**
		 * The meta object literal for the '<em><b>Override Class</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TANGO_SIM_LIB__OVERRIDE_CLASS = eINSTANCE.getTangoSimLib_OverrideClass();

		/**
		 * The meta object literal for the '<em><b>Reffered Pogo Device Class</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TANGO_SIM_LIB__REFFERED_POGO_DEVICE_CLASS = eINSTANCE.getTangoSimLib_RefferedPogoDeviceClass();

		/**
		 * The meta object literal for the '{@link tangoSimLibSimulator.impl.DataSimulationImpl <em>Data Simulation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see tangoSimLibSimulator.impl.DataSimulationImpl
		 * @see tangoSimLibSimulator.impl.TangoSimLibSimulatorPackageImpl#getDataSimulation()
		 * @generated
		 */
		EClass DATA_SIMULATION = eINSTANCE.getDataSimulation();

		/**
		 * The meta object literal for the '<em><b>Pogo Attr</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DATA_SIMULATION__POGO_ATTR = eINSTANCE.getDataSimulation_PogoAttr();

		/**
		 * The meta object literal for the '<em><b>Simulation Type</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DATA_SIMULATION__SIMULATION_TYPE = eINSTANCE.getDataSimulation_SimulationType();

		/**
		 * The meta object literal for the '{@link tangoSimLibSimulator.impl.CommandSimulationImpl <em>Command Simulation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see tangoSimLibSimulator.impl.CommandSimulationImpl
		 * @see tangoSimLibSimulator.impl.TangoSimLibSimulatorPackageImpl#getCommandSimulation()
		 * @generated
		 */
		EClass COMMAND_SIMULATION = eINSTANCE.getCommandSimulation();

		/**
		 * The meta object literal for the '<em><b>Pogo Command</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMMAND_SIMULATION__POGO_COMMAND = eINSTANCE.getCommandSimulation_PogoCommand();

		/**
		 * The meta object literal for the '<em><b>Behavior Type</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMMAND_SIMULATION__BEHAVIOR_TYPE = eINSTANCE.getCommandSimulation_BehaviorType();

		/**
		 * The meta object literal for the '{@link tangoSimLibSimulator.impl.GaussianSlewLimitedImpl <em>Gaussian Slew Limited</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see tangoSimLibSimulator.impl.GaussianSlewLimitedImpl
		 * @see tangoSimLibSimulator.impl.TangoSimLibSimulatorPackageImpl#getGaussianSlewLimited()
		 * @generated
		 */
		EClass GAUSSIAN_SLEW_LIMITED = eINSTANCE.getGaussianSlewLimited();

		/**
		 * The meta object literal for the '<em><b>Min Bound</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GAUSSIAN_SLEW_LIMITED__MIN_BOUND = eINSTANCE.getGaussianSlewLimited_MinBound();

		/**
		 * The meta object literal for the '<em><b>Max Bound</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GAUSSIAN_SLEW_LIMITED__MAX_BOUND = eINSTANCE.getGaussianSlewLimited_MaxBound();

		/**
		 * The meta object literal for the '<em><b>Mean</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GAUSSIAN_SLEW_LIMITED__MEAN = eINSTANCE.getGaussianSlewLimited_Mean();

		/**
		 * The meta object literal for the '<em><b>Slew Rate</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GAUSSIAN_SLEW_LIMITED__SLEW_RATE = eINSTANCE.getGaussianSlewLimited_SlewRate();

		/**
		 * The meta object literal for the '<em><b>Update Period</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GAUSSIAN_SLEW_LIMITED__UPDATE_PERIOD = eINSTANCE.getGaussianSlewLimited_UpdatePeriod();

		/**
		 * The meta object literal for the '{@link tangoSimLibSimulator.impl.ConstantQuantityImpl <em>Constant Quantity</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see tangoSimLibSimulator.impl.ConstantQuantityImpl
		 * @see tangoSimLibSimulator.impl.TangoSimLibSimulatorPackageImpl#getConstantQuantity()
		 * @generated
		 */
		EClass CONSTANT_QUANTITY = eINSTANCE.getConstantQuantity();

		/**
		 * The meta object literal for the '<em><b>Initial Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CONSTANT_QUANTITY__INITIAL_VALUE = eINSTANCE.getConstantQuantity_InitialValue();

		/**
		 * The meta object literal for the '<em><b>Quality</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CONSTANT_QUANTITY__QUALITY = eINSTANCE.getConstantQuantity_Quality();

		/**
		 * The meta object literal for the '{@link tangoSimLibSimulator.impl.DeterministicSignalImpl <em>Deterministic Signal</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see tangoSimLibSimulator.impl.DeterministicSignalImpl
		 * @see tangoSimLibSimulator.impl.TangoSimLibSimulatorPackageImpl#getDeterministicSignal()
		 * @generated
		 */
		EClass DETERMINISTIC_SIGNAL = eINSTANCE.getDeterministicSignal();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DETERMINISTIC_SIGNAL__TYPE = eINSTANCE.getDeterministicSignal_Type();

		/**
		 * The meta object literal for the '<em><b>Amplitude</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DETERMINISTIC_SIGNAL__AMPLITUDE = eINSTANCE.getDeterministicSignal_Amplitude();

		/**
		 * The meta object literal for the '<em><b>Period</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DETERMINISTIC_SIGNAL__PERIOD = eINSTANCE.getDeterministicSignal_Period();

		/**
		 * The meta object literal for the '<em><b>Offset</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DETERMINISTIC_SIGNAL__OFFSET = eINSTANCE.getDeterministicSignal_Offset();

		/**
		 * The meta object literal for the '{@link tangoSimLibSimulator.impl.RuntimeSpecifiedWaveformImpl <em>Runtime Specified Waveform</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see tangoSimLibSimulator.impl.RuntimeSpecifiedWaveformImpl
		 * @see tangoSimLibSimulator.impl.TangoSimLibSimulatorPackageImpl#getRuntimeSpecifiedWaveform()
		 * @generated
		 */
		EClass RUNTIME_SPECIFIED_WAVEFORM = eINSTANCE.getRuntimeSpecifiedWaveform();

		/**
		 * The meta object literal for the '<em><b>Default Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RUNTIME_SPECIFIED_WAVEFORM__DEFAULT_VALUE = eINSTANCE.getRuntimeSpecifiedWaveform_DefaultValue();

		/**
		 * The meta object literal for the '<em><b>Timestamp</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RUNTIME_SPECIFIED_WAVEFORM__TIMESTAMP = eINSTANCE.getRuntimeSpecifiedWaveform_Timestamp();

		/**
		 * The meta object literal for the '<em><b>Attribute qualities</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RUNTIME_SPECIFIED_WAVEFORM__ATTRIBUTE_QUALITIES = eINSTANCE.getRuntimeSpecifiedWaveform_Attribute_qualities();

		/**
		 * The meta object literal for the '{@link tangoSimLibSimulator.impl.InputTransformImpl <em>Input Transform</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see tangoSimLibSimulator.impl.InputTransformImpl
		 * @see tangoSimLibSimulator.impl.TangoSimLibSimulatorPackageImpl#getInputTransform()
		 * @generated
		 */
		EClass INPUT_TRANSFORM = eINSTANCE.getInputTransform();

		/**
		 * The meta object literal for the '<em><b>Destination Variable Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute INPUT_TRANSFORM__DESTINATION_VARIABLE_NAME = eINSTANCE.getInputTransform_DestinationVariableName();

		/**
		 * The meta object literal for the '{@link tangoSimLibSimulator.impl.SideEffectsImpl <em>Side Effects</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see tangoSimLibSimulator.impl.SideEffectsImpl
		 * @see tangoSimLibSimulator.impl.TangoSimLibSimulatorPackageImpl#getSideEffects()
		 * @generated
		 */
		EClass SIDE_EFFECTS = eINSTANCE.getSideEffects();

		/**
		 * The meta object literal for the '<em><b>Source variable</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SIDE_EFFECTS__SOURCE_VARIABLE = eINSTANCE.getSideEffects_Source_variable();

		/**
		 * The meta object literal for the '<em><b>Destination quantity</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SIDE_EFFECTS__DESTINATION_QUANTITY = eINSTANCE.getSideEffects_Destination_quantity();

		/**
		 * The meta object literal for the '{@link tangoSimLibSimulator.impl.OutputReturnImpl <em>Output Return</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see tangoSimLibSimulator.impl.OutputReturnImpl
		 * @see tangoSimLibSimulator.impl.TangoSimLibSimulatorPackageImpl#getOutputReturn()
		 * @generated
		 */
		EClass OUTPUT_RETURN = eINSTANCE.getOutputReturn();

		/**
		 * The meta object literal for the '<em><b>Source variable</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OUTPUT_RETURN__SOURCE_VARIABLE = eINSTANCE.getOutputReturn_Source_variable();

		/**
		 * The meta object literal for the '<em><b>Source quantity</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OUTPUT_RETURN__SOURCE_QUANTITY = eINSTANCE.getOutputReturn_Source_quantity();

		/**
		 * The meta object literal for the '{@link tangoSimLibSimulator.impl.OverrideClassImpl <em>Override Class</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see tangoSimLibSimulator.impl.OverrideClassImpl
		 * @see tangoSimLibSimulator.impl.TangoSimLibSimulatorPackageImpl#getOverrideClass()
		 * @generated
		 */
		EClass OVERRIDE_CLASS = eINSTANCE.getOverrideClass();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OVERRIDE_CLASS__NAME = eINSTANCE.getOverrideClass_Name();

		/**
		 * The meta object literal for the '<em><b>Module directory</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OVERRIDE_CLASS__MODULE_DIRECTORY = eINSTANCE.getOverrideClass_Module_directory();

		/**
		 * The meta object literal for the '<em><b>Module name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OVERRIDE_CLASS__MODULE_NAME = eINSTANCE.getOverrideClass_Module_name();

		/**
		 * The meta object literal for the '<em><b>Class name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OVERRIDE_CLASS__CLASS_NAME = eINSTANCE.getOverrideClass_Class_name();

		/**
		 * The meta object literal for the '{@link tangoSimLibSimulator.impl.SimulationImpl <em>Simulation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see tangoSimLibSimulator.impl.SimulationImpl
		 * @see tangoSimLibSimulator.impl.TangoSimLibSimulatorPackageImpl#getSimulation()
		 * @generated
		 */
		EClass SIMULATION = eINSTANCE.getSimulation();

		/**
		 * The meta object literal for the '{@link tangoSimLibSimulator.impl.BehaviourImpl <em>Behaviour</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see tangoSimLibSimulator.impl.BehaviourImpl
		 * @see tangoSimLibSimulator.impl.TangoSimLibSimulatorPackageImpl#getBehaviour()
		 * @generated
		 */
		EClass BEHAVIOUR = eINSTANCE.getBehaviour();

	}

} //TangoSimLibSimulatorPackage
