/**
 */
package tangoSimLibSimulator;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Constant Quantity</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link tangoSimLibSimulator.ConstantQuantity#getInitialValue <em>Initial Value</em>}</li>
 *   <li>{@link tangoSimLibSimulator.ConstantQuantity#getQuality <em>Quality</em>}</li>
 * </ul>
 *
 * @see tangoSimLibSimulator.TangoSimLibSimulatorPackage#getConstantQuantity()
 * @model
 * @generated
 */
public interface ConstantQuantity extends Simulation {
	/**
	 * Returns the value of the '<em><b>Initial Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Initial Value</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Initial Value</em>' attribute.
	 * @see #setInitialValue(float)
	 * @see tangoSimLibSimulator.TangoSimLibSimulatorPackage#getConstantQuantity_InitialValue()
	 * @model
	 * @generated
	 */
	float getInitialValue();

	/**
	 * Sets the value of the '{@link tangoSimLibSimulator.ConstantQuantity#getInitialValue <em>Initial Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Initial Value</em>' attribute.
	 * @see #getInitialValue()
	 * @generated
	 */
	void setInitialValue(float value);

	/**
	 * Returns the value of the '<em><b>Quality</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Quality</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Quality</em>' attribute.
	 * @see #setQuality(float)
	 * @see tangoSimLibSimulator.TangoSimLibSimulatorPackage#getConstantQuantity_Quality()
	 * @model
	 * @generated
	 */
	float getQuality();

	/**
	 * Sets the value of the '{@link tangoSimLibSimulator.ConstantQuantity#getQuality <em>Quality</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Quality</em>' attribute.
	 * @see #getQuality()
	 * @generated
	 */
	void setQuality(float value);

} // ConstantQuantity
