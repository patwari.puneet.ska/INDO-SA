/**
 */
package tangoSimLibSimulator;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see tangoSimLibSimulator.TangoSimLibSimulatorPackage
 * @generated
 */
public interface TangoSimLibSimulatorFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	TangoSimLibSimulatorFactory eINSTANCE = tangoSimLibSimulator.impl.TangoSimLibSimulatorFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Tango Sim Lib</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Tango Sim Lib</em>'.
	 * @generated
	 */
	TangoSimLib createTangoSimLib();

	/**
	 * Returns a new object of class '<em>Data Simulation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Simulation</em>'.
	 * @generated
	 */
	DataSimulation createDataSimulation();

	/**
	 * Returns a new object of class '<em>Command Simulation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Command Simulation</em>'.
	 * @generated
	 */
	CommandSimulation createCommandSimulation();

	/**
	 * Returns a new object of class '<em>Gaussian Slew Limited</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Gaussian Slew Limited</em>'.
	 * @generated
	 */
	GaussianSlewLimited createGaussianSlewLimited();

	/**
	 * Returns a new object of class '<em>Constant Quantity</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Constant Quantity</em>'.
	 * @generated
	 */
	ConstantQuantity createConstantQuantity();

	/**
	 * Returns a new object of class '<em>Deterministic Signal</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Deterministic Signal</em>'.
	 * @generated
	 */
	DeterministicSignal createDeterministicSignal();

	/**
	 * Returns a new object of class '<em>Runtime Specified Waveform</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Runtime Specified Waveform</em>'.
	 * @generated
	 */
	RuntimeSpecifiedWaveform createRuntimeSpecifiedWaveform();

	/**
	 * Returns a new object of class '<em>Input Transform</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Input Transform</em>'.
	 * @generated
	 */
	InputTransform createInputTransform();

	/**
	 * Returns a new object of class '<em>Side Effects</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Side Effects</em>'.
	 * @generated
	 */
	SideEffects createSideEffects();

	/**
	 * Returns a new object of class '<em>Output Return</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Output Return</em>'.
	 * @generated
	 */
	OutputReturn createOutputReturn();

	/**
	 * Returns a new object of class '<em>Override Class</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Override Class</em>'.
	 * @generated
	 */
	OverrideClass createOverrideClass();

	/**
	 * Returns a new object of class '<em>Simulation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Simulation</em>'.
	 * @generated
	 */
	Simulation createSimulation();

	/**
	 * Returns a new object of class '<em>Behaviour</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Behaviour</em>'.
	 * @generated
	 */
	Behaviour createBehaviour();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	TangoSimLibSimulatorPackage getTangoSimLibSimulatorPackage();

} //TangoSimLibSimulatorFactory
