/**
 */
package tangoSimLibSimulator;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Side Effects</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link tangoSimLibSimulator.SideEffects#getSource_variable <em>Source variable</em>}</li>
 *   <li>{@link tangoSimLibSimulator.SideEffects#getDestination_quantity <em>Destination quantity</em>}</li>
 * </ul>
 *
 * @see tangoSimLibSimulator.TangoSimLibSimulatorPackage#getSideEffects()
 * @model
 * @generated
 */
public interface SideEffects extends Behaviour {
	/**
	 * Returns the value of the '<em><b>Source variable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Source variable</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source variable</em>' attribute.
	 * @see #setSource_variable(String)
	 * @see tangoSimLibSimulator.TangoSimLibSimulatorPackage#getSideEffects_Source_variable()
	 * @model
	 * @generated
	 */
	String getSource_variable();

	/**
	 * Sets the value of the '{@link tangoSimLibSimulator.SideEffects#getSource_variable <em>Source variable</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Source variable</em>' attribute.
	 * @see #getSource_variable()
	 * @generated
	 */
	void setSource_variable(String value);

	/**
	 * Returns the value of the '<em><b>Destination quantity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Destination quantity</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Destination quantity</em>' attribute.
	 * @see #setDestination_quantity(String)
	 * @see tangoSimLibSimulator.TangoSimLibSimulatorPackage#getSideEffects_Destination_quantity()
	 * @model
	 * @generated
	 */
	String getDestination_quantity();

	/**
	 * Sets the value of the '{@link tangoSimLibSimulator.SideEffects#getDestination_quantity <em>Destination quantity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Destination quantity</em>' attribute.
	 * @see #getDestination_quantity()
	 * @generated
	 */
	void setDestination_quantity(String value);

} // SideEffects
