/**
 */
package tangoSimLibSimulator;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Deterministic Signal</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link tangoSimLibSimulator.DeterministicSignal#getType <em>Type</em>}</li>
 *   <li>{@link tangoSimLibSimulator.DeterministicSignal#getAmplitude <em>Amplitude</em>}</li>
 *   <li>{@link tangoSimLibSimulator.DeterministicSignal#getPeriod <em>Period</em>}</li>
 *   <li>{@link tangoSimLibSimulator.DeterministicSignal#getOffset <em>Offset</em>}</li>
 * </ul>
 *
 * @see tangoSimLibSimulator.TangoSimLibSimulatorPackage#getDeterministicSignal()
 * @model
 * @generated
 */
public interface DeterministicSignal extends Simulation {
	/**
	 * Returns the value of the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' attribute.
	 * @see #setType(String)
	 * @see tangoSimLibSimulator.TangoSimLibSimulatorPackage#getDeterministicSignal_Type()
	 * @model
	 * @generated
	 */
	String getType();

	/**
	 * Sets the value of the '{@link tangoSimLibSimulator.DeterministicSignal#getType <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' attribute.
	 * @see #getType()
	 * @generated
	 */
	void setType(String value);

	/**
	 * Returns the value of the '<em><b>Amplitude</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Amplitude</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Amplitude</em>' attribute.
	 * @see #setAmplitude(float)
	 * @see tangoSimLibSimulator.TangoSimLibSimulatorPackage#getDeterministicSignal_Amplitude()
	 * @model
	 * @generated
	 */
	float getAmplitude();

	/**
	 * Sets the value of the '{@link tangoSimLibSimulator.DeterministicSignal#getAmplitude <em>Amplitude</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Amplitude</em>' attribute.
	 * @see #getAmplitude()
	 * @generated
	 */
	void setAmplitude(float value);

	/**
	 * Returns the value of the '<em><b>Period</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Period</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Period</em>' attribute.
	 * @see #setPeriod(int)
	 * @see tangoSimLibSimulator.TangoSimLibSimulatorPackage#getDeterministicSignal_Period()
	 * @model
	 * @generated
	 */
	int getPeriod();

	/**
	 * Sets the value of the '{@link tangoSimLibSimulator.DeterministicSignal#getPeriod <em>Period</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Period</em>' attribute.
	 * @see #getPeriod()
	 * @generated
	 */
	void setPeriod(int value);

	/**
	 * Returns the value of the '<em><b>Offset</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Offset</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Offset</em>' attribute.
	 * @see #setOffset(float)
	 * @see tangoSimLibSimulator.TangoSimLibSimulatorPackage#getDeterministicSignal_Offset()
	 * @model
	 * @generated
	 */
	float getOffset();

	/**
	 * Sets the value of the '{@link tangoSimLibSimulator.DeterministicSignal#getOffset <em>Offset</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Offset</em>' attribute.
	 * @see #getOffset()
	 * @generated
	 */
	void setOffset(float value);

} // DeterministicSignal
