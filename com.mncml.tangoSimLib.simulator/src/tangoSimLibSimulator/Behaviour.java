/**
 */
package tangoSimLibSimulator;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Behaviour</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see tangoSimLibSimulator.TangoSimLibSimulatorPackage#getBehaviour()
 * @model
 * @generated
 */
public interface Behaviour extends EObject {
} // Behaviour
