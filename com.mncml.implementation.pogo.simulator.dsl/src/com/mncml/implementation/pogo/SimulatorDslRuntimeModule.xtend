/*
 * generated by Xtext 2.11.0
 */
package com.mncml.implementation.pogo


/**
 * Use this class to register components to be used at runtime / without the Equinox extension registry.
 */
class SimulatorDslRuntimeModule extends AbstractSimulatorDslRuntimeModule {
}
