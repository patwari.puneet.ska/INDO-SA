package com.mncml.implementation.pogo.parser.antlr.internal;

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import com.mncml.implementation.pogo.services.SimulatorDslGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalSimulatorDslParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_STRING", "RULE_ID", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'TangoSimLib'", "'for'", "'{'", "'dataSimulations'", "','", "'}'", "'behaviours'", "'override'", "'OverrideClass'", "'name'", "'module_directory'", "'module_name'", "'class_name'", "'.'", "'Command'", "'Attribute'", "'data_Simulation_Algorithm'", "'GaussianSlewLimited'", "'minBound'", "'maxBound'", "'mean'", "'slewRate'", "'updatePeriod'", "'ConstantQuantity'", "'initialValue'", "'quality'", "'DeterministicSignal'", "'type'", "'amplitude'", "'period'", "'offset'", "'RuntimeSpecifiedWaveform'", "'defaultValue'", "'timestamp'", "'attribute_qualities'", "'-'", "'E'", "'e'", "'InputTransform'", "'destinationVariableName'", "'SideEffects'", "'source_variable'", "'destination_quantity'", "'OutputReturn'", "'source_quantity'"
    };
    public static final int T__50=50;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__55=55;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int T__51=51;
    public static final int T__52=52;
    public static final int T__53=53;
    public static final int T__54=54;
    public static final int RULE_ID=5;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=6;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;
    public static final int RULE_STRING=4;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__37=37;
    public static final int T__38=38;
    public static final int T__39=39;
    public static final int T__33=33;
    public static final int T__34=34;
    public static final int T__35=35;
    public static final int T__36=36;
    public static final int EOF=-1;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__48=48;
    public static final int T__49=49;
    public static final int T__44=44;
    public static final int T__45=45;
    public static final int T__46=46;
    public static final int T__47=47;
    public static final int T__40=40;
    public static final int T__41=41;
    public static final int T__42=42;
    public static final int T__43=43;

    // delegates
    // delegators


        public InternalSimulatorDslParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalSimulatorDslParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalSimulatorDslParser.tokenNames; }
    public String getGrammarFileName() { return "InternalSimulatorDsl.g"; }



     	private SimulatorDslGrammarAccess grammarAccess;

        public InternalSimulatorDslParser(TokenStream input, SimulatorDslGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }

        @Override
        protected String getFirstRuleName() {
        	return "TangoSimLib";
       	}

       	@Override
       	protected SimulatorDslGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}




    // $ANTLR start "entryRuleTangoSimLib"
    // InternalSimulatorDsl.g:64:1: entryRuleTangoSimLib returns [EObject current=null] : iv_ruleTangoSimLib= ruleTangoSimLib EOF ;
    public final EObject entryRuleTangoSimLib() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTangoSimLib = null;


        try {
            // InternalSimulatorDsl.g:64:52: (iv_ruleTangoSimLib= ruleTangoSimLib EOF )
            // InternalSimulatorDsl.g:65:2: iv_ruleTangoSimLib= ruleTangoSimLib EOF
            {
             newCompositeNode(grammarAccess.getTangoSimLibRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleTangoSimLib=ruleTangoSimLib();

            state._fsp--;

             current =iv_ruleTangoSimLib; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTangoSimLib"


    // $ANTLR start "ruleTangoSimLib"
    // InternalSimulatorDsl.g:71:1: ruleTangoSimLib returns [EObject current=null] : ( () otherlv_1= 'TangoSimLib' otherlv_2= 'for' ( ( ruleQualifiedName ) )? otherlv_4= '{' (otherlv_5= 'dataSimulations' otherlv_6= '{' ( ( (lv_dataSimulations_7_0= ruleDataSimulation ) ) (otherlv_8= ',' ( (lv_dataSimulations_9_0= ruleDataSimulation ) ) )* )? otherlv_10= '}' )? (otherlv_11= 'behaviours' otherlv_12= '{' ( ( (lv_commandSimulations_13_0= ruleCommandSimulation ) ) (otherlv_14= ',' ( (lv_commandSimulations_15_0= ruleCommandSimulation ) )* ) )? otherlv_16= '}' )? (otherlv_17= 'override' otherlv_18= '{' ( ( (lv_overrideClass_19_0= ruleOverrideClass ) ) otherlv_20= ',' ( (lv_overrideClass_21_0= ruleOverrideClass ) )* )? otherlv_22= '}' )? otherlv_23= '}' ) ;
    public final EObject ruleTangoSimLib() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        Token otherlv_10=null;
        Token otherlv_11=null;
        Token otherlv_12=null;
        Token otherlv_14=null;
        Token otherlv_16=null;
        Token otherlv_17=null;
        Token otherlv_18=null;
        Token otherlv_20=null;
        Token otherlv_22=null;
        Token otherlv_23=null;
        EObject lv_dataSimulations_7_0 = null;

        EObject lv_dataSimulations_9_0 = null;

        EObject lv_commandSimulations_13_0 = null;

        EObject lv_commandSimulations_15_0 = null;

        EObject lv_overrideClass_19_0 = null;

        EObject lv_overrideClass_21_0 = null;



        	enterRule();

        try {
            // InternalSimulatorDsl.g:77:2: ( ( () otherlv_1= 'TangoSimLib' otherlv_2= 'for' ( ( ruleQualifiedName ) )? otherlv_4= '{' (otherlv_5= 'dataSimulations' otherlv_6= '{' ( ( (lv_dataSimulations_7_0= ruleDataSimulation ) ) (otherlv_8= ',' ( (lv_dataSimulations_9_0= ruleDataSimulation ) ) )* )? otherlv_10= '}' )? (otherlv_11= 'behaviours' otherlv_12= '{' ( ( (lv_commandSimulations_13_0= ruleCommandSimulation ) ) (otherlv_14= ',' ( (lv_commandSimulations_15_0= ruleCommandSimulation ) )* ) )? otherlv_16= '}' )? (otherlv_17= 'override' otherlv_18= '{' ( ( (lv_overrideClass_19_0= ruleOverrideClass ) ) otherlv_20= ',' ( (lv_overrideClass_21_0= ruleOverrideClass ) )* )? otherlv_22= '}' )? otherlv_23= '}' ) )
            // InternalSimulatorDsl.g:78:2: ( () otherlv_1= 'TangoSimLib' otherlv_2= 'for' ( ( ruleQualifiedName ) )? otherlv_4= '{' (otherlv_5= 'dataSimulations' otherlv_6= '{' ( ( (lv_dataSimulations_7_0= ruleDataSimulation ) ) (otherlv_8= ',' ( (lv_dataSimulations_9_0= ruleDataSimulation ) ) )* )? otherlv_10= '}' )? (otherlv_11= 'behaviours' otherlv_12= '{' ( ( (lv_commandSimulations_13_0= ruleCommandSimulation ) ) (otherlv_14= ',' ( (lv_commandSimulations_15_0= ruleCommandSimulation ) )* ) )? otherlv_16= '}' )? (otherlv_17= 'override' otherlv_18= '{' ( ( (lv_overrideClass_19_0= ruleOverrideClass ) ) otherlv_20= ',' ( (lv_overrideClass_21_0= ruleOverrideClass ) )* )? otherlv_22= '}' )? otherlv_23= '}' )
            {
            // InternalSimulatorDsl.g:78:2: ( () otherlv_1= 'TangoSimLib' otherlv_2= 'for' ( ( ruleQualifiedName ) )? otherlv_4= '{' (otherlv_5= 'dataSimulations' otherlv_6= '{' ( ( (lv_dataSimulations_7_0= ruleDataSimulation ) ) (otherlv_8= ',' ( (lv_dataSimulations_9_0= ruleDataSimulation ) ) )* )? otherlv_10= '}' )? (otherlv_11= 'behaviours' otherlv_12= '{' ( ( (lv_commandSimulations_13_0= ruleCommandSimulation ) ) (otherlv_14= ',' ( (lv_commandSimulations_15_0= ruleCommandSimulation ) )* ) )? otherlv_16= '}' )? (otherlv_17= 'override' otherlv_18= '{' ( ( (lv_overrideClass_19_0= ruleOverrideClass ) ) otherlv_20= ',' ( (lv_overrideClass_21_0= ruleOverrideClass ) )* )? otherlv_22= '}' )? otherlv_23= '}' )
            // InternalSimulatorDsl.g:79:3: () otherlv_1= 'TangoSimLib' otherlv_2= 'for' ( ( ruleQualifiedName ) )? otherlv_4= '{' (otherlv_5= 'dataSimulations' otherlv_6= '{' ( ( (lv_dataSimulations_7_0= ruleDataSimulation ) ) (otherlv_8= ',' ( (lv_dataSimulations_9_0= ruleDataSimulation ) ) )* )? otherlv_10= '}' )? (otherlv_11= 'behaviours' otherlv_12= '{' ( ( (lv_commandSimulations_13_0= ruleCommandSimulation ) ) (otherlv_14= ',' ( (lv_commandSimulations_15_0= ruleCommandSimulation ) )* ) )? otherlv_16= '}' )? (otherlv_17= 'override' otherlv_18= '{' ( ( (lv_overrideClass_19_0= ruleOverrideClass ) ) otherlv_20= ',' ( (lv_overrideClass_21_0= ruleOverrideClass ) )* )? otherlv_22= '}' )? otherlv_23= '}'
            {
            // InternalSimulatorDsl.g:79:3: ()
            // InternalSimulatorDsl.g:80:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getTangoSimLibAccess().getTangoSimLibAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,11,FOLLOW_3); 

            			newLeafNode(otherlv_1, grammarAccess.getTangoSimLibAccess().getTangoSimLibKeyword_1());
            		
            otherlv_2=(Token)match(input,12,FOLLOW_4); 

            			newLeafNode(otherlv_2, grammarAccess.getTangoSimLibAccess().getForKeyword_2());
            		
            // InternalSimulatorDsl.g:94:3: ( ( ruleQualifiedName ) )?
            int alt1=2;
            int LA1_0 = input.LA(1);

            if ( (LA1_0==RULE_ID) ) {
                alt1=1;
            }
            switch (alt1) {
                case 1 :
                    // InternalSimulatorDsl.g:95:4: ( ruleQualifiedName )
                    {
                    // InternalSimulatorDsl.g:95:4: ( ruleQualifiedName )
                    // InternalSimulatorDsl.g:96:5: ruleQualifiedName
                    {

                    					if (current==null) {
                    						current = createModelElement(grammarAccess.getTangoSimLibRule());
                    					}
                    				

                    					newCompositeNode(grammarAccess.getTangoSimLibAccess().getRefferedPogoDeviceClassPogoDeviceClassCrossReference_3_0());
                    				
                    pushFollow(FOLLOW_5);
                    ruleQualifiedName();

                    state._fsp--;


                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }
                    break;

            }

            otherlv_4=(Token)match(input,13,FOLLOW_6); 

            			newLeafNode(otherlv_4, grammarAccess.getTangoSimLibAccess().getLeftCurlyBracketKeyword_4());
            		
            // InternalSimulatorDsl.g:114:3: (otherlv_5= 'dataSimulations' otherlv_6= '{' ( ( (lv_dataSimulations_7_0= ruleDataSimulation ) ) (otherlv_8= ',' ( (lv_dataSimulations_9_0= ruleDataSimulation ) ) )* )? otherlv_10= '}' )?
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==14) ) {
                alt4=1;
            }
            switch (alt4) {
                case 1 :
                    // InternalSimulatorDsl.g:115:4: otherlv_5= 'dataSimulations' otherlv_6= '{' ( ( (lv_dataSimulations_7_0= ruleDataSimulation ) ) (otherlv_8= ',' ( (lv_dataSimulations_9_0= ruleDataSimulation ) ) )* )? otherlv_10= '}'
                    {
                    otherlv_5=(Token)match(input,14,FOLLOW_5); 

                    				newLeafNode(otherlv_5, grammarAccess.getTangoSimLibAccess().getDataSimulationsKeyword_5_0());
                    			
                    otherlv_6=(Token)match(input,13,FOLLOW_7); 

                    				newLeafNode(otherlv_6, grammarAccess.getTangoSimLibAccess().getLeftCurlyBracketKeyword_5_1());
                    			
                    // InternalSimulatorDsl.g:123:4: ( ( (lv_dataSimulations_7_0= ruleDataSimulation ) ) (otherlv_8= ',' ( (lv_dataSimulations_9_0= ruleDataSimulation ) ) )* )?
                    int alt3=2;
                    int LA3_0 = input.LA(1);

                    if ( (LA3_0==26) ) {
                        alt3=1;
                    }
                    switch (alt3) {
                        case 1 :
                            // InternalSimulatorDsl.g:124:5: ( (lv_dataSimulations_7_0= ruleDataSimulation ) ) (otherlv_8= ',' ( (lv_dataSimulations_9_0= ruleDataSimulation ) ) )*
                            {
                            // InternalSimulatorDsl.g:124:5: ( (lv_dataSimulations_7_0= ruleDataSimulation ) )
                            // InternalSimulatorDsl.g:125:6: (lv_dataSimulations_7_0= ruleDataSimulation )
                            {
                            // InternalSimulatorDsl.g:125:6: (lv_dataSimulations_7_0= ruleDataSimulation )
                            // InternalSimulatorDsl.g:126:7: lv_dataSimulations_7_0= ruleDataSimulation
                            {

                            							newCompositeNode(grammarAccess.getTangoSimLibAccess().getDataSimulationsDataSimulationParserRuleCall_5_2_0_0());
                            						
                            pushFollow(FOLLOW_8);
                            lv_dataSimulations_7_0=ruleDataSimulation();

                            state._fsp--;


                            							if (current==null) {
                            								current = createModelElementForParent(grammarAccess.getTangoSimLibRule());
                            							}
                            							add(
                            								current,
                            								"dataSimulations",
                            								lv_dataSimulations_7_0,
                            								"com.mncml.implementation.pogo.SimulatorDsl.DataSimulation");
                            							afterParserOrEnumRuleCall();
                            						

                            }


                            }

                            // InternalSimulatorDsl.g:143:5: (otherlv_8= ',' ( (lv_dataSimulations_9_0= ruleDataSimulation ) ) )*
                            loop2:
                            do {
                                int alt2=2;
                                int LA2_0 = input.LA(1);

                                if ( (LA2_0==15) ) {
                                    alt2=1;
                                }


                                switch (alt2) {
                            	case 1 :
                            	    // InternalSimulatorDsl.g:144:6: otherlv_8= ',' ( (lv_dataSimulations_9_0= ruleDataSimulation ) )
                            	    {
                            	    otherlv_8=(Token)match(input,15,FOLLOW_9); 

                            	    						newLeafNode(otherlv_8, grammarAccess.getTangoSimLibAccess().getCommaKeyword_5_2_1_0());
                            	    					
                            	    // InternalSimulatorDsl.g:148:6: ( (lv_dataSimulations_9_0= ruleDataSimulation ) )
                            	    // InternalSimulatorDsl.g:149:7: (lv_dataSimulations_9_0= ruleDataSimulation )
                            	    {
                            	    // InternalSimulatorDsl.g:149:7: (lv_dataSimulations_9_0= ruleDataSimulation )
                            	    // InternalSimulatorDsl.g:150:8: lv_dataSimulations_9_0= ruleDataSimulation
                            	    {

                            	    								newCompositeNode(grammarAccess.getTangoSimLibAccess().getDataSimulationsDataSimulationParserRuleCall_5_2_1_1_0());
                            	    							
                            	    pushFollow(FOLLOW_8);
                            	    lv_dataSimulations_9_0=ruleDataSimulation();

                            	    state._fsp--;


                            	    								if (current==null) {
                            	    									current = createModelElementForParent(grammarAccess.getTangoSimLibRule());
                            	    								}
                            	    								add(
                            	    									current,
                            	    									"dataSimulations",
                            	    									lv_dataSimulations_9_0,
                            	    									"com.mncml.implementation.pogo.SimulatorDsl.DataSimulation");
                            	    								afterParserOrEnumRuleCall();
                            	    							

                            	    }


                            	    }


                            	    }
                            	    break;

                            	default :
                            	    break loop2;
                                }
                            } while (true);


                            }
                            break;

                    }

                    otherlv_10=(Token)match(input,16,FOLLOW_10); 

                    				newLeafNode(otherlv_10, grammarAccess.getTangoSimLibAccess().getRightCurlyBracketKeyword_5_3());
                    			

                    }
                    break;

            }

            // InternalSimulatorDsl.g:174:3: (otherlv_11= 'behaviours' otherlv_12= '{' ( ( (lv_commandSimulations_13_0= ruleCommandSimulation ) ) (otherlv_14= ',' ( (lv_commandSimulations_15_0= ruleCommandSimulation ) )* ) )? otherlv_16= '}' )?
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( (LA7_0==17) ) {
                alt7=1;
            }
            switch (alt7) {
                case 1 :
                    // InternalSimulatorDsl.g:175:4: otherlv_11= 'behaviours' otherlv_12= '{' ( ( (lv_commandSimulations_13_0= ruleCommandSimulation ) ) (otherlv_14= ',' ( (lv_commandSimulations_15_0= ruleCommandSimulation ) )* ) )? otherlv_16= '}'
                    {
                    otherlv_11=(Token)match(input,17,FOLLOW_5); 

                    				newLeafNode(otherlv_11, grammarAccess.getTangoSimLibAccess().getBehavioursKeyword_6_0());
                    			
                    otherlv_12=(Token)match(input,13,FOLLOW_11); 

                    				newLeafNode(otherlv_12, grammarAccess.getTangoSimLibAccess().getLeftCurlyBracketKeyword_6_1());
                    			
                    // InternalSimulatorDsl.g:183:4: ( ( (lv_commandSimulations_13_0= ruleCommandSimulation ) ) (otherlv_14= ',' ( (lv_commandSimulations_15_0= ruleCommandSimulation ) )* ) )?
                    int alt6=2;
                    int LA6_0 = input.LA(1);

                    if ( (LA6_0==25) ) {
                        alt6=1;
                    }
                    switch (alt6) {
                        case 1 :
                            // InternalSimulatorDsl.g:184:5: ( (lv_commandSimulations_13_0= ruleCommandSimulation ) ) (otherlv_14= ',' ( (lv_commandSimulations_15_0= ruleCommandSimulation ) )* )
                            {
                            // InternalSimulatorDsl.g:184:5: ( (lv_commandSimulations_13_0= ruleCommandSimulation ) )
                            // InternalSimulatorDsl.g:185:6: (lv_commandSimulations_13_0= ruleCommandSimulation )
                            {
                            // InternalSimulatorDsl.g:185:6: (lv_commandSimulations_13_0= ruleCommandSimulation )
                            // InternalSimulatorDsl.g:186:7: lv_commandSimulations_13_0= ruleCommandSimulation
                            {

                            							newCompositeNode(grammarAccess.getTangoSimLibAccess().getCommandSimulationsCommandSimulationParserRuleCall_6_2_0_0());
                            						
                            pushFollow(FOLLOW_12);
                            lv_commandSimulations_13_0=ruleCommandSimulation();

                            state._fsp--;


                            							if (current==null) {
                            								current = createModelElementForParent(grammarAccess.getTangoSimLibRule());
                            							}
                            							add(
                            								current,
                            								"commandSimulations",
                            								lv_commandSimulations_13_0,
                            								"com.mncml.implementation.pogo.SimulatorDsl.CommandSimulation");
                            							afterParserOrEnumRuleCall();
                            						

                            }


                            }

                            // InternalSimulatorDsl.g:203:5: (otherlv_14= ',' ( (lv_commandSimulations_15_0= ruleCommandSimulation ) )* )
                            // InternalSimulatorDsl.g:204:6: otherlv_14= ',' ( (lv_commandSimulations_15_0= ruleCommandSimulation ) )*
                            {
                            otherlv_14=(Token)match(input,15,FOLLOW_11); 

                            						newLeafNode(otherlv_14, grammarAccess.getTangoSimLibAccess().getCommaKeyword_6_2_1_0());
                            					
                            // InternalSimulatorDsl.g:208:6: ( (lv_commandSimulations_15_0= ruleCommandSimulation ) )*
                            loop5:
                            do {
                                int alt5=2;
                                int LA5_0 = input.LA(1);

                                if ( (LA5_0==25) ) {
                                    alt5=1;
                                }


                                switch (alt5) {
                            	case 1 :
                            	    // InternalSimulatorDsl.g:209:7: (lv_commandSimulations_15_0= ruleCommandSimulation )
                            	    {
                            	    // InternalSimulatorDsl.g:209:7: (lv_commandSimulations_15_0= ruleCommandSimulation )
                            	    // InternalSimulatorDsl.g:210:8: lv_commandSimulations_15_0= ruleCommandSimulation
                            	    {

                            	    								newCompositeNode(grammarAccess.getTangoSimLibAccess().getCommandSimulationsCommandSimulationParserRuleCall_6_2_1_1_0());
                            	    							
                            	    pushFollow(FOLLOW_11);
                            	    lv_commandSimulations_15_0=ruleCommandSimulation();

                            	    state._fsp--;


                            	    								if (current==null) {
                            	    									current = createModelElementForParent(grammarAccess.getTangoSimLibRule());
                            	    								}
                            	    								add(
                            	    									current,
                            	    									"commandSimulations",
                            	    									lv_commandSimulations_15_0,
                            	    									"com.mncml.implementation.pogo.SimulatorDsl.CommandSimulation");
                            	    								afterParserOrEnumRuleCall();
                            	    							

                            	    }


                            	    }
                            	    break;

                            	default :
                            	    break loop5;
                                }
                            } while (true);


                            }


                            }
                            break;

                    }

                    otherlv_16=(Token)match(input,16,FOLLOW_13); 

                    				newLeafNode(otherlv_16, grammarAccess.getTangoSimLibAccess().getRightCurlyBracketKeyword_6_3());
                    			

                    }
                    break;

            }

            // InternalSimulatorDsl.g:234:3: (otherlv_17= 'override' otherlv_18= '{' ( ( (lv_overrideClass_19_0= ruleOverrideClass ) ) otherlv_20= ',' ( (lv_overrideClass_21_0= ruleOverrideClass ) )* )? otherlv_22= '}' )?
            int alt10=2;
            int LA10_0 = input.LA(1);

            if ( (LA10_0==18) ) {
                alt10=1;
            }
            switch (alt10) {
                case 1 :
                    // InternalSimulatorDsl.g:235:4: otherlv_17= 'override' otherlv_18= '{' ( ( (lv_overrideClass_19_0= ruleOverrideClass ) ) otherlv_20= ',' ( (lv_overrideClass_21_0= ruleOverrideClass ) )* )? otherlv_22= '}'
                    {
                    otherlv_17=(Token)match(input,18,FOLLOW_5); 

                    				newLeafNode(otherlv_17, grammarAccess.getTangoSimLibAccess().getOverrideKeyword_7_0());
                    			
                    otherlv_18=(Token)match(input,13,FOLLOW_14); 

                    				newLeafNode(otherlv_18, grammarAccess.getTangoSimLibAccess().getLeftCurlyBracketKeyword_7_1());
                    			
                    // InternalSimulatorDsl.g:243:4: ( ( (lv_overrideClass_19_0= ruleOverrideClass ) ) otherlv_20= ',' ( (lv_overrideClass_21_0= ruleOverrideClass ) )* )?
                    int alt9=2;
                    int LA9_0 = input.LA(1);

                    if ( (LA9_0==19) ) {
                        alt9=1;
                    }
                    switch (alt9) {
                        case 1 :
                            // InternalSimulatorDsl.g:244:5: ( (lv_overrideClass_19_0= ruleOverrideClass ) ) otherlv_20= ',' ( (lv_overrideClass_21_0= ruleOverrideClass ) )*
                            {
                            // InternalSimulatorDsl.g:244:5: ( (lv_overrideClass_19_0= ruleOverrideClass ) )
                            // InternalSimulatorDsl.g:245:6: (lv_overrideClass_19_0= ruleOverrideClass )
                            {
                            // InternalSimulatorDsl.g:245:6: (lv_overrideClass_19_0= ruleOverrideClass )
                            // InternalSimulatorDsl.g:246:7: lv_overrideClass_19_0= ruleOverrideClass
                            {

                            							newCompositeNode(grammarAccess.getTangoSimLibAccess().getOverrideClassOverrideClassParserRuleCall_7_2_0_0());
                            						
                            pushFollow(FOLLOW_12);
                            lv_overrideClass_19_0=ruleOverrideClass();

                            state._fsp--;


                            							if (current==null) {
                            								current = createModelElementForParent(grammarAccess.getTangoSimLibRule());
                            							}
                            							add(
                            								current,
                            								"overrideClass",
                            								lv_overrideClass_19_0,
                            								"com.mncml.implementation.pogo.SimulatorDsl.OverrideClass");
                            							afterParserOrEnumRuleCall();
                            						

                            }


                            }

                            otherlv_20=(Token)match(input,15,FOLLOW_14); 

                            					newLeafNode(otherlv_20, grammarAccess.getTangoSimLibAccess().getCommaKeyword_7_2_1());
                            				
                            // InternalSimulatorDsl.g:267:5: ( (lv_overrideClass_21_0= ruleOverrideClass ) )*
                            loop8:
                            do {
                                int alt8=2;
                                int LA8_0 = input.LA(1);

                                if ( (LA8_0==19) ) {
                                    alt8=1;
                                }


                                switch (alt8) {
                            	case 1 :
                            	    // InternalSimulatorDsl.g:268:6: (lv_overrideClass_21_0= ruleOverrideClass )
                            	    {
                            	    // InternalSimulatorDsl.g:268:6: (lv_overrideClass_21_0= ruleOverrideClass )
                            	    // InternalSimulatorDsl.g:269:7: lv_overrideClass_21_0= ruleOverrideClass
                            	    {

                            	    							newCompositeNode(grammarAccess.getTangoSimLibAccess().getOverrideClassOverrideClassParserRuleCall_7_2_2_0());
                            	    						
                            	    pushFollow(FOLLOW_14);
                            	    lv_overrideClass_21_0=ruleOverrideClass();

                            	    state._fsp--;


                            	    							if (current==null) {
                            	    								current = createModelElementForParent(grammarAccess.getTangoSimLibRule());
                            	    							}
                            	    							add(
                            	    								current,
                            	    								"overrideClass",
                            	    								lv_overrideClass_21_0,
                            	    								"com.mncml.implementation.pogo.SimulatorDsl.OverrideClass");
                            	    							afterParserOrEnumRuleCall();
                            	    						

                            	    }


                            	    }
                            	    break;

                            	default :
                            	    break loop8;
                                }
                            } while (true);


                            }
                            break;

                    }

                    otherlv_22=(Token)match(input,16,FOLLOW_15); 

                    				newLeafNode(otherlv_22, grammarAccess.getTangoSimLibAccess().getRightCurlyBracketKeyword_7_3());
                    			

                    }
                    break;

            }

            otherlv_23=(Token)match(input,16,FOLLOW_2); 

            			newLeafNode(otherlv_23, grammarAccess.getTangoSimLibAccess().getRightCurlyBracketKeyword_8());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTangoSimLib"


    // $ANTLR start "entryRuleOverrideClass"
    // InternalSimulatorDsl.g:300:1: entryRuleOverrideClass returns [EObject current=null] : iv_ruleOverrideClass= ruleOverrideClass EOF ;
    public final EObject entryRuleOverrideClass() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleOverrideClass = null;


        try {
            // InternalSimulatorDsl.g:300:54: (iv_ruleOverrideClass= ruleOverrideClass EOF )
            // InternalSimulatorDsl.g:301:2: iv_ruleOverrideClass= ruleOverrideClass EOF
            {
             newCompositeNode(grammarAccess.getOverrideClassRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleOverrideClass=ruleOverrideClass();

            state._fsp--;

             current =iv_ruleOverrideClass; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOverrideClass"


    // $ANTLR start "ruleOverrideClass"
    // InternalSimulatorDsl.g:307:1: ruleOverrideClass returns [EObject current=null] : ( () otherlv_1= 'OverrideClass' otherlv_2= '{' (otherlv_3= 'name' ( (lv_name_4_0= RULE_STRING ) ) )? (otherlv_5= 'module_directory' ( (lv_module_directory_6_0= ruleEString ) ) )? (otherlv_7= 'module_name' ( (lv_module_name_8_0= ruleEString ) ) )? (otherlv_9= 'class_name' ( (lv_class_name_10_0= ruleEString ) ) )? otherlv_11= '}' ) ;
    public final EObject ruleOverrideClass() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token lv_name_4_0=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        Token otherlv_11=null;
        AntlrDatatypeRuleToken lv_module_directory_6_0 = null;

        AntlrDatatypeRuleToken lv_module_name_8_0 = null;

        AntlrDatatypeRuleToken lv_class_name_10_0 = null;



        	enterRule();

        try {
            // InternalSimulatorDsl.g:313:2: ( ( () otherlv_1= 'OverrideClass' otherlv_2= '{' (otherlv_3= 'name' ( (lv_name_4_0= RULE_STRING ) ) )? (otherlv_5= 'module_directory' ( (lv_module_directory_6_0= ruleEString ) ) )? (otherlv_7= 'module_name' ( (lv_module_name_8_0= ruleEString ) ) )? (otherlv_9= 'class_name' ( (lv_class_name_10_0= ruleEString ) ) )? otherlv_11= '}' ) )
            // InternalSimulatorDsl.g:314:2: ( () otherlv_1= 'OverrideClass' otherlv_2= '{' (otherlv_3= 'name' ( (lv_name_4_0= RULE_STRING ) ) )? (otherlv_5= 'module_directory' ( (lv_module_directory_6_0= ruleEString ) ) )? (otherlv_7= 'module_name' ( (lv_module_name_8_0= ruleEString ) ) )? (otherlv_9= 'class_name' ( (lv_class_name_10_0= ruleEString ) ) )? otherlv_11= '}' )
            {
            // InternalSimulatorDsl.g:314:2: ( () otherlv_1= 'OverrideClass' otherlv_2= '{' (otherlv_3= 'name' ( (lv_name_4_0= RULE_STRING ) ) )? (otherlv_5= 'module_directory' ( (lv_module_directory_6_0= ruleEString ) ) )? (otherlv_7= 'module_name' ( (lv_module_name_8_0= ruleEString ) ) )? (otherlv_9= 'class_name' ( (lv_class_name_10_0= ruleEString ) ) )? otherlv_11= '}' )
            // InternalSimulatorDsl.g:315:3: () otherlv_1= 'OverrideClass' otherlv_2= '{' (otherlv_3= 'name' ( (lv_name_4_0= RULE_STRING ) ) )? (otherlv_5= 'module_directory' ( (lv_module_directory_6_0= ruleEString ) ) )? (otherlv_7= 'module_name' ( (lv_module_name_8_0= ruleEString ) ) )? (otherlv_9= 'class_name' ( (lv_class_name_10_0= ruleEString ) ) )? otherlv_11= '}'
            {
            // InternalSimulatorDsl.g:315:3: ()
            // InternalSimulatorDsl.g:316:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getOverrideClassAccess().getOverrideClassAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,19,FOLLOW_5); 

            			newLeafNode(otherlv_1, grammarAccess.getOverrideClassAccess().getOverrideClassKeyword_1());
            		
            otherlv_2=(Token)match(input,13,FOLLOW_16); 

            			newLeafNode(otherlv_2, grammarAccess.getOverrideClassAccess().getLeftCurlyBracketKeyword_2());
            		
            // InternalSimulatorDsl.g:330:3: (otherlv_3= 'name' ( (lv_name_4_0= RULE_STRING ) ) )?
            int alt11=2;
            int LA11_0 = input.LA(1);

            if ( (LA11_0==20) ) {
                alt11=1;
            }
            switch (alt11) {
                case 1 :
                    // InternalSimulatorDsl.g:331:4: otherlv_3= 'name' ( (lv_name_4_0= RULE_STRING ) )
                    {
                    otherlv_3=(Token)match(input,20,FOLLOW_17); 

                    				newLeafNode(otherlv_3, grammarAccess.getOverrideClassAccess().getNameKeyword_3_0());
                    			
                    // InternalSimulatorDsl.g:335:4: ( (lv_name_4_0= RULE_STRING ) )
                    // InternalSimulatorDsl.g:336:5: (lv_name_4_0= RULE_STRING )
                    {
                    // InternalSimulatorDsl.g:336:5: (lv_name_4_0= RULE_STRING )
                    // InternalSimulatorDsl.g:337:6: lv_name_4_0= RULE_STRING
                    {
                    lv_name_4_0=(Token)match(input,RULE_STRING,FOLLOW_18); 

                    						newLeafNode(lv_name_4_0, grammarAccess.getOverrideClassAccess().getNameSTRINGTerminalRuleCall_3_1_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getOverrideClassRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"name",
                    							lv_name_4_0,
                    							"org.eclipse.xtext.common.Terminals.STRING");
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalSimulatorDsl.g:354:3: (otherlv_5= 'module_directory' ( (lv_module_directory_6_0= ruleEString ) ) )?
            int alt12=2;
            int LA12_0 = input.LA(1);

            if ( (LA12_0==21) ) {
                alt12=1;
            }
            switch (alt12) {
                case 1 :
                    // InternalSimulatorDsl.g:355:4: otherlv_5= 'module_directory' ( (lv_module_directory_6_0= ruleEString ) )
                    {
                    otherlv_5=(Token)match(input,21,FOLLOW_19); 

                    				newLeafNode(otherlv_5, grammarAccess.getOverrideClassAccess().getModule_directoryKeyword_4_0());
                    			
                    // InternalSimulatorDsl.g:359:4: ( (lv_module_directory_6_0= ruleEString ) )
                    // InternalSimulatorDsl.g:360:5: (lv_module_directory_6_0= ruleEString )
                    {
                    // InternalSimulatorDsl.g:360:5: (lv_module_directory_6_0= ruleEString )
                    // InternalSimulatorDsl.g:361:6: lv_module_directory_6_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getOverrideClassAccess().getModule_directoryEStringParserRuleCall_4_1_0());
                    					
                    pushFollow(FOLLOW_20);
                    lv_module_directory_6_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getOverrideClassRule());
                    						}
                    						set(
                    							current,
                    							"module_directory",
                    							lv_module_directory_6_0,
                    							"com.mncml.implementation.pogo.SimulatorDsl.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalSimulatorDsl.g:379:3: (otherlv_7= 'module_name' ( (lv_module_name_8_0= ruleEString ) ) )?
            int alt13=2;
            int LA13_0 = input.LA(1);

            if ( (LA13_0==22) ) {
                alt13=1;
            }
            switch (alt13) {
                case 1 :
                    // InternalSimulatorDsl.g:380:4: otherlv_7= 'module_name' ( (lv_module_name_8_0= ruleEString ) )
                    {
                    otherlv_7=(Token)match(input,22,FOLLOW_19); 

                    				newLeafNode(otherlv_7, grammarAccess.getOverrideClassAccess().getModule_nameKeyword_5_0());
                    			
                    // InternalSimulatorDsl.g:384:4: ( (lv_module_name_8_0= ruleEString ) )
                    // InternalSimulatorDsl.g:385:5: (lv_module_name_8_0= ruleEString )
                    {
                    // InternalSimulatorDsl.g:385:5: (lv_module_name_8_0= ruleEString )
                    // InternalSimulatorDsl.g:386:6: lv_module_name_8_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getOverrideClassAccess().getModule_nameEStringParserRuleCall_5_1_0());
                    					
                    pushFollow(FOLLOW_21);
                    lv_module_name_8_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getOverrideClassRule());
                    						}
                    						set(
                    							current,
                    							"module_name",
                    							lv_module_name_8_0,
                    							"com.mncml.implementation.pogo.SimulatorDsl.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalSimulatorDsl.g:404:3: (otherlv_9= 'class_name' ( (lv_class_name_10_0= ruleEString ) ) )?
            int alt14=2;
            int LA14_0 = input.LA(1);

            if ( (LA14_0==23) ) {
                alt14=1;
            }
            switch (alt14) {
                case 1 :
                    // InternalSimulatorDsl.g:405:4: otherlv_9= 'class_name' ( (lv_class_name_10_0= ruleEString ) )
                    {
                    otherlv_9=(Token)match(input,23,FOLLOW_19); 

                    				newLeafNode(otherlv_9, grammarAccess.getOverrideClassAccess().getClass_nameKeyword_6_0());
                    			
                    // InternalSimulatorDsl.g:409:4: ( (lv_class_name_10_0= ruleEString ) )
                    // InternalSimulatorDsl.g:410:5: (lv_class_name_10_0= ruleEString )
                    {
                    // InternalSimulatorDsl.g:410:5: (lv_class_name_10_0= ruleEString )
                    // InternalSimulatorDsl.g:411:6: lv_class_name_10_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getOverrideClassAccess().getClass_nameEStringParserRuleCall_6_1_0());
                    					
                    pushFollow(FOLLOW_15);
                    lv_class_name_10_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getOverrideClassRule());
                    						}
                    						set(
                    							current,
                    							"class_name",
                    							lv_class_name_10_0,
                    							"com.mncml.implementation.pogo.SimulatorDsl.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            otherlv_11=(Token)match(input,16,FOLLOW_2); 

            			newLeafNode(otherlv_11, grammarAccess.getOverrideClassAccess().getRightCurlyBracketKeyword_7());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOverrideClass"


    // $ANTLR start "entryRuleQualifiedName"
    // InternalSimulatorDsl.g:437:1: entryRuleQualifiedName returns [String current=null] : iv_ruleQualifiedName= ruleQualifiedName EOF ;
    public final String entryRuleQualifiedName() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleQualifiedName = null;


        try {
            // InternalSimulatorDsl.g:437:53: (iv_ruleQualifiedName= ruleQualifiedName EOF )
            // InternalSimulatorDsl.g:438:2: iv_ruleQualifiedName= ruleQualifiedName EOF
            {
             newCompositeNode(grammarAccess.getQualifiedNameRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleQualifiedName=ruleQualifiedName();

            state._fsp--;

             current =iv_ruleQualifiedName.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleQualifiedName"


    // $ANTLR start "ruleQualifiedName"
    // InternalSimulatorDsl.g:444:1: ruleQualifiedName returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* ) ;
    public final AntlrDatatypeRuleToken ruleQualifiedName() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_ID_0=null;
        Token kw=null;
        Token this_ID_2=null;


        	enterRule();

        try {
            // InternalSimulatorDsl.g:450:2: ( (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* ) )
            // InternalSimulatorDsl.g:451:2: (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* )
            {
            // InternalSimulatorDsl.g:451:2: (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* )
            // InternalSimulatorDsl.g:452:3: this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )*
            {
            this_ID_0=(Token)match(input,RULE_ID,FOLLOW_22); 

            			current.merge(this_ID_0);
            		

            			newLeafNode(this_ID_0, grammarAccess.getQualifiedNameAccess().getIDTerminalRuleCall_0());
            		
            // InternalSimulatorDsl.g:459:3: (kw= '.' this_ID_2= RULE_ID )*
            loop15:
            do {
                int alt15=2;
                int LA15_0 = input.LA(1);

                if ( (LA15_0==24) ) {
                    alt15=1;
                }


                switch (alt15) {
            	case 1 :
            	    // InternalSimulatorDsl.g:460:4: kw= '.' this_ID_2= RULE_ID
            	    {
            	    kw=(Token)match(input,24,FOLLOW_23); 

            	    				current.merge(kw);
            	    				newLeafNode(kw, grammarAccess.getQualifiedNameAccess().getFullStopKeyword_1_0());
            	    			
            	    this_ID_2=(Token)match(input,RULE_ID,FOLLOW_22); 

            	    				current.merge(this_ID_2);
            	    			

            	    				newLeafNode(this_ID_2, grammarAccess.getQualifiedNameAccess().getIDTerminalRuleCall_1_1());
            	    			

            	    }
            	    break;

            	default :
            	    break loop15;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleQualifiedName"


    // $ANTLR start "entryRuleSimulation"
    // InternalSimulatorDsl.g:477:1: entryRuleSimulation returns [EObject current=null] : iv_ruleSimulation= ruleSimulation EOF ;
    public final EObject entryRuleSimulation() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSimulation = null;


        try {
            // InternalSimulatorDsl.g:477:51: (iv_ruleSimulation= ruleSimulation EOF )
            // InternalSimulatorDsl.g:478:2: iv_ruleSimulation= ruleSimulation EOF
            {
             newCompositeNode(grammarAccess.getSimulationRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleSimulation=ruleSimulation();

            state._fsp--;

             current =iv_ruleSimulation; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSimulation"


    // $ANTLR start "ruleSimulation"
    // InternalSimulatorDsl.g:484:1: ruleSimulation returns [EObject current=null] : (this_GaussianSlewLimited_0= ruleGaussianSlewLimited | this_ConstantQuantity_1= ruleConstantQuantity | this_DeterministicSignal_2= ruleDeterministicSignal | this_RuntimeSpecifiedWaveform_3= ruleRuntimeSpecifiedWaveform ) ;
    public final EObject ruleSimulation() throws RecognitionException {
        EObject current = null;

        EObject this_GaussianSlewLimited_0 = null;

        EObject this_ConstantQuantity_1 = null;

        EObject this_DeterministicSignal_2 = null;

        EObject this_RuntimeSpecifiedWaveform_3 = null;



        	enterRule();

        try {
            // InternalSimulatorDsl.g:490:2: ( (this_GaussianSlewLimited_0= ruleGaussianSlewLimited | this_ConstantQuantity_1= ruleConstantQuantity | this_DeterministicSignal_2= ruleDeterministicSignal | this_RuntimeSpecifiedWaveform_3= ruleRuntimeSpecifiedWaveform ) )
            // InternalSimulatorDsl.g:491:2: (this_GaussianSlewLimited_0= ruleGaussianSlewLimited | this_ConstantQuantity_1= ruleConstantQuantity | this_DeterministicSignal_2= ruleDeterministicSignal | this_RuntimeSpecifiedWaveform_3= ruleRuntimeSpecifiedWaveform )
            {
            // InternalSimulatorDsl.g:491:2: (this_GaussianSlewLimited_0= ruleGaussianSlewLimited | this_ConstantQuantity_1= ruleConstantQuantity | this_DeterministicSignal_2= ruleDeterministicSignal | this_RuntimeSpecifiedWaveform_3= ruleRuntimeSpecifiedWaveform )
            int alt16=4;
            switch ( input.LA(1) ) {
            case 28:
                {
                alt16=1;
                }
                break;
            case 34:
                {
                alt16=2;
                }
                break;
            case 37:
                {
                alt16=3;
                }
                break;
            case 42:
                {
                alt16=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 16, 0, input);

                throw nvae;
            }

            switch (alt16) {
                case 1 :
                    // InternalSimulatorDsl.g:492:3: this_GaussianSlewLimited_0= ruleGaussianSlewLimited
                    {

                    			newCompositeNode(grammarAccess.getSimulationAccess().getGaussianSlewLimitedParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_GaussianSlewLimited_0=ruleGaussianSlewLimited();

                    state._fsp--;


                    			current = this_GaussianSlewLimited_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalSimulatorDsl.g:501:3: this_ConstantQuantity_1= ruleConstantQuantity
                    {

                    			newCompositeNode(grammarAccess.getSimulationAccess().getConstantQuantityParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_ConstantQuantity_1=ruleConstantQuantity();

                    state._fsp--;


                    			current = this_ConstantQuantity_1;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 3 :
                    // InternalSimulatorDsl.g:510:3: this_DeterministicSignal_2= ruleDeterministicSignal
                    {

                    			newCompositeNode(grammarAccess.getSimulationAccess().getDeterministicSignalParserRuleCall_2());
                    		
                    pushFollow(FOLLOW_2);
                    this_DeterministicSignal_2=ruleDeterministicSignal();

                    state._fsp--;


                    			current = this_DeterministicSignal_2;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 4 :
                    // InternalSimulatorDsl.g:519:3: this_RuntimeSpecifiedWaveform_3= ruleRuntimeSpecifiedWaveform
                    {

                    			newCompositeNode(grammarAccess.getSimulationAccess().getRuntimeSpecifiedWaveformParserRuleCall_3());
                    		
                    pushFollow(FOLLOW_2);
                    this_RuntimeSpecifiedWaveform_3=ruleRuntimeSpecifiedWaveform();

                    state._fsp--;


                    			current = this_RuntimeSpecifiedWaveform_3;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSimulation"


    // $ANTLR start "entryRuleBehaviour"
    // InternalSimulatorDsl.g:531:1: entryRuleBehaviour returns [EObject current=null] : iv_ruleBehaviour= ruleBehaviour EOF ;
    public final EObject entryRuleBehaviour() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBehaviour = null;


        try {
            // InternalSimulatorDsl.g:531:50: (iv_ruleBehaviour= ruleBehaviour EOF )
            // InternalSimulatorDsl.g:532:2: iv_ruleBehaviour= ruleBehaviour EOF
            {
             newCompositeNode(grammarAccess.getBehaviourRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleBehaviour=ruleBehaviour();

            state._fsp--;

             current =iv_ruleBehaviour; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBehaviour"


    // $ANTLR start "ruleBehaviour"
    // InternalSimulatorDsl.g:538:1: ruleBehaviour returns [EObject current=null] : (this_InputTransform_0= ruleInputTransform | this_SideEffects_1= ruleSideEffects | this_OutputReturn_2= ruleOutputReturn ) ;
    public final EObject ruleBehaviour() throws RecognitionException {
        EObject current = null;

        EObject this_InputTransform_0 = null;

        EObject this_SideEffects_1 = null;

        EObject this_OutputReturn_2 = null;



        	enterRule();

        try {
            // InternalSimulatorDsl.g:544:2: ( (this_InputTransform_0= ruleInputTransform | this_SideEffects_1= ruleSideEffects | this_OutputReturn_2= ruleOutputReturn ) )
            // InternalSimulatorDsl.g:545:2: (this_InputTransform_0= ruleInputTransform | this_SideEffects_1= ruleSideEffects | this_OutputReturn_2= ruleOutputReturn )
            {
            // InternalSimulatorDsl.g:545:2: (this_InputTransform_0= ruleInputTransform | this_SideEffects_1= ruleSideEffects | this_OutputReturn_2= ruleOutputReturn )
            int alt17=3;
            switch ( input.LA(1) ) {
            case 49:
                {
                alt17=1;
                }
                break;
            case 51:
                {
                alt17=2;
                }
                break;
            case 54:
                {
                alt17=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 17, 0, input);

                throw nvae;
            }

            switch (alt17) {
                case 1 :
                    // InternalSimulatorDsl.g:546:3: this_InputTransform_0= ruleInputTransform
                    {

                    			newCompositeNode(grammarAccess.getBehaviourAccess().getInputTransformParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_InputTransform_0=ruleInputTransform();

                    state._fsp--;


                    			current = this_InputTransform_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalSimulatorDsl.g:555:3: this_SideEffects_1= ruleSideEffects
                    {

                    			newCompositeNode(grammarAccess.getBehaviourAccess().getSideEffectsParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_SideEffects_1=ruleSideEffects();

                    state._fsp--;


                    			current = this_SideEffects_1;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 3 :
                    // InternalSimulatorDsl.g:564:3: this_OutputReturn_2= ruleOutputReturn
                    {

                    			newCompositeNode(grammarAccess.getBehaviourAccess().getOutputReturnParserRuleCall_2());
                    		
                    pushFollow(FOLLOW_2);
                    this_OutputReturn_2=ruleOutputReturn();

                    state._fsp--;


                    			current = this_OutputReturn_2;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBehaviour"


    // $ANTLR start "entryRuleCommandSimulation"
    // InternalSimulatorDsl.g:576:1: entryRuleCommandSimulation returns [EObject current=null] : iv_ruleCommandSimulation= ruleCommandSimulation EOF ;
    public final EObject entryRuleCommandSimulation() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCommandSimulation = null;


        try {
            // InternalSimulatorDsl.g:576:58: (iv_ruleCommandSimulation= ruleCommandSimulation EOF )
            // InternalSimulatorDsl.g:577:2: iv_ruleCommandSimulation= ruleCommandSimulation EOF
            {
             newCompositeNode(grammarAccess.getCommandSimulationRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleCommandSimulation=ruleCommandSimulation();

            state._fsp--;

             current =iv_ruleCommandSimulation; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCommandSimulation"


    // $ANTLR start "ruleCommandSimulation"
    // InternalSimulatorDsl.g:583:1: ruleCommandSimulation returns [EObject current=null] : ( () otherlv_1= 'Command' ( ( ruleQualifiedName ) ) otherlv_3= '{' ( (lv_behaviorType_4_0= ruleBehaviour ) )? otherlv_5= '}' ) ;
    public final EObject ruleCommandSimulation() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        EObject lv_behaviorType_4_0 = null;



        	enterRule();

        try {
            // InternalSimulatorDsl.g:589:2: ( ( () otherlv_1= 'Command' ( ( ruleQualifiedName ) ) otherlv_3= '{' ( (lv_behaviorType_4_0= ruleBehaviour ) )? otherlv_5= '}' ) )
            // InternalSimulatorDsl.g:590:2: ( () otherlv_1= 'Command' ( ( ruleQualifiedName ) ) otherlv_3= '{' ( (lv_behaviorType_4_0= ruleBehaviour ) )? otherlv_5= '}' )
            {
            // InternalSimulatorDsl.g:590:2: ( () otherlv_1= 'Command' ( ( ruleQualifiedName ) ) otherlv_3= '{' ( (lv_behaviorType_4_0= ruleBehaviour ) )? otherlv_5= '}' )
            // InternalSimulatorDsl.g:591:3: () otherlv_1= 'Command' ( ( ruleQualifiedName ) ) otherlv_3= '{' ( (lv_behaviorType_4_0= ruleBehaviour ) )? otherlv_5= '}'
            {
            // InternalSimulatorDsl.g:591:3: ()
            // InternalSimulatorDsl.g:592:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getCommandSimulationAccess().getCommandSimulationAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,25,FOLLOW_23); 

            			newLeafNode(otherlv_1, grammarAccess.getCommandSimulationAccess().getCommandKeyword_1());
            		
            // InternalSimulatorDsl.g:602:3: ( ( ruleQualifiedName ) )
            // InternalSimulatorDsl.g:603:4: ( ruleQualifiedName )
            {
            // InternalSimulatorDsl.g:603:4: ( ruleQualifiedName )
            // InternalSimulatorDsl.g:604:5: ruleQualifiedName
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getCommandSimulationRule());
            					}
            				

            					newCompositeNode(grammarAccess.getCommandSimulationAccess().getPogoCommandCommandCrossReference_2_0());
            				
            pushFollow(FOLLOW_5);
            ruleQualifiedName();

            state._fsp--;


            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_3=(Token)match(input,13,FOLLOW_24); 

            			newLeafNode(otherlv_3, grammarAccess.getCommandSimulationAccess().getLeftCurlyBracketKeyword_3());
            		
            // InternalSimulatorDsl.g:622:3: ( (lv_behaviorType_4_0= ruleBehaviour ) )?
            int alt18=2;
            int LA18_0 = input.LA(1);

            if ( (LA18_0==49||LA18_0==51||LA18_0==54) ) {
                alt18=1;
            }
            switch (alt18) {
                case 1 :
                    // InternalSimulatorDsl.g:623:4: (lv_behaviorType_4_0= ruleBehaviour )
                    {
                    // InternalSimulatorDsl.g:623:4: (lv_behaviorType_4_0= ruleBehaviour )
                    // InternalSimulatorDsl.g:624:5: lv_behaviorType_4_0= ruleBehaviour
                    {

                    					newCompositeNode(grammarAccess.getCommandSimulationAccess().getBehaviorTypeBehaviourParserRuleCall_4_0());
                    				
                    pushFollow(FOLLOW_15);
                    lv_behaviorType_4_0=ruleBehaviour();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getCommandSimulationRule());
                    					}
                    					set(
                    						current,
                    						"behaviorType",
                    						lv_behaviorType_4_0,
                    						"com.mncml.implementation.pogo.SimulatorDsl.Behaviour");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }
                    break;

            }

            otherlv_5=(Token)match(input,16,FOLLOW_2); 

            			newLeafNode(otherlv_5, grammarAccess.getCommandSimulationAccess().getRightCurlyBracketKeyword_5());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCommandSimulation"


    // $ANTLR start "entryRuleDataSimulation"
    // InternalSimulatorDsl.g:649:1: entryRuleDataSimulation returns [EObject current=null] : iv_ruleDataSimulation= ruleDataSimulation EOF ;
    public final EObject entryRuleDataSimulation() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDataSimulation = null;


        try {
            // InternalSimulatorDsl.g:649:55: (iv_ruleDataSimulation= ruleDataSimulation EOF )
            // InternalSimulatorDsl.g:650:2: iv_ruleDataSimulation= ruleDataSimulation EOF
            {
             newCompositeNode(grammarAccess.getDataSimulationRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleDataSimulation=ruleDataSimulation();

            state._fsp--;

             current =iv_ruleDataSimulation; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDataSimulation"


    // $ANTLR start "ruleDataSimulation"
    // InternalSimulatorDsl.g:656:1: ruleDataSimulation returns [EObject current=null] : ( () otherlv_1= 'Attribute' ( ( ruleQualifiedName ) )? otherlv_3= '{' otherlv_4= 'data_Simulation_Algorithm' ( (lv_simulationType_5_0= ruleSimulation ) )? otherlv_6= '}' ) ;
    public final EObject ruleDataSimulation() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        EObject lv_simulationType_5_0 = null;



        	enterRule();

        try {
            // InternalSimulatorDsl.g:662:2: ( ( () otherlv_1= 'Attribute' ( ( ruleQualifiedName ) )? otherlv_3= '{' otherlv_4= 'data_Simulation_Algorithm' ( (lv_simulationType_5_0= ruleSimulation ) )? otherlv_6= '}' ) )
            // InternalSimulatorDsl.g:663:2: ( () otherlv_1= 'Attribute' ( ( ruleQualifiedName ) )? otherlv_3= '{' otherlv_4= 'data_Simulation_Algorithm' ( (lv_simulationType_5_0= ruleSimulation ) )? otherlv_6= '}' )
            {
            // InternalSimulatorDsl.g:663:2: ( () otherlv_1= 'Attribute' ( ( ruleQualifiedName ) )? otherlv_3= '{' otherlv_4= 'data_Simulation_Algorithm' ( (lv_simulationType_5_0= ruleSimulation ) )? otherlv_6= '}' )
            // InternalSimulatorDsl.g:664:3: () otherlv_1= 'Attribute' ( ( ruleQualifiedName ) )? otherlv_3= '{' otherlv_4= 'data_Simulation_Algorithm' ( (lv_simulationType_5_0= ruleSimulation ) )? otherlv_6= '}'
            {
            // InternalSimulatorDsl.g:664:3: ()
            // InternalSimulatorDsl.g:665:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getDataSimulationAccess().getDataSimulationAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,26,FOLLOW_4); 

            			newLeafNode(otherlv_1, grammarAccess.getDataSimulationAccess().getAttributeKeyword_1());
            		
            // InternalSimulatorDsl.g:675:3: ( ( ruleQualifiedName ) )?
            int alt19=2;
            int LA19_0 = input.LA(1);

            if ( (LA19_0==RULE_ID) ) {
                alt19=1;
            }
            switch (alt19) {
                case 1 :
                    // InternalSimulatorDsl.g:676:4: ( ruleQualifiedName )
                    {
                    // InternalSimulatorDsl.g:676:4: ( ruleQualifiedName )
                    // InternalSimulatorDsl.g:677:5: ruleQualifiedName
                    {

                    					if (current==null) {
                    						current = createModelElement(grammarAccess.getDataSimulationRule());
                    					}
                    				

                    					newCompositeNode(grammarAccess.getDataSimulationAccess().getPogoAttrAttributeCrossReference_2_0());
                    				
                    pushFollow(FOLLOW_5);
                    ruleQualifiedName();

                    state._fsp--;


                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }
                    break;

            }

            otherlv_3=(Token)match(input,13,FOLLOW_25); 

            			newLeafNode(otherlv_3, grammarAccess.getDataSimulationAccess().getLeftCurlyBracketKeyword_3());
            		
            otherlv_4=(Token)match(input,27,FOLLOW_26); 

            			newLeafNode(otherlv_4, grammarAccess.getDataSimulationAccess().getData_Simulation_AlgorithmKeyword_4());
            		
            // InternalSimulatorDsl.g:699:3: ( (lv_simulationType_5_0= ruleSimulation ) )?
            int alt20=2;
            int LA20_0 = input.LA(1);

            if ( (LA20_0==28||LA20_0==34||LA20_0==37||LA20_0==42) ) {
                alt20=1;
            }
            switch (alt20) {
                case 1 :
                    // InternalSimulatorDsl.g:700:4: (lv_simulationType_5_0= ruleSimulation )
                    {
                    // InternalSimulatorDsl.g:700:4: (lv_simulationType_5_0= ruleSimulation )
                    // InternalSimulatorDsl.g:701:5: lv_simulationType_5_0= ruleSimulation
                    {

                    					newCompositeNode(grammarAccess.getDataSimulationAccess().getSimulationTypeSimulationParserRuleCall_5_0());
                    				
                    pushFollow(FOLLOW_15);
                    lv_simulationType_5_0=ruleSimulation();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getDataSimulationRule());
                    					}
                    					set(
                    						current,
                    						"simulationType",
                    						lv_simulationType_5_0,
                    						"com.mncml.implementation.pogo.SimulatorDsl.Simulation");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }
                    break;

            }

            otherlv_6=(Token)match(input,16,FOLLOW_2); 

            			newLeafNode(otherlv_6, grammarAccess.getDataSimulationAccess().getRightCurlyBracketKeyword_6());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDataSimulation"


    // $ANTLR start "entryRuleGaussianSlewLimited"
    // InternalSimulatorDsl.g:726:1: entryRuleGaussianSlewLimited returns [EObject current=null] : iv_ruleGaussianSlewLimited= ruleGaussianSlewLimited EOF ;
    public final EObject entryRuleGaussianSlewLimited() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleGaussianSlewLimited = null;


        try {
            // InternalSimulatorDsl.g:726:60: (iv_ruleGaussianSlewLimited= ruleGaussianSlewLimited EOF )
            // InternalSimulatorDsl.g:727:2: iv_ruleGaussianSlewLimited= ruleGaussianSlewLimited EOF
            {
             newCompositeNode(grammarAccess.getGaussianSlewLimitedRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleGaussianSlewLimited=ruleGaussianSlewLimited();

            state._fsp--;

             current =iv_ruleGaussianSlewLimited; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleGaussianSlewLimited"


    // $ANTLR start "ruleGaussianSlewLimited"
    // InternalSimulatorDsl.g:733:1: ruleGaussianSlewLimited returns [EObject current=null] : ( () otherlv_1= 'GaussianSlewLimited' otherlv_2= '{' (otherlv_3= 'minBound' ( (lv_minBound_4_0= ruleEFloat ) ) )? (otherlv_5= 'maxBound' ( (lv_maxBound_6_0= ruleEFloat ) ) )? (otherlv_7= 'mean' ( (lv_mean_8_0= ruleEFloat ) ) )? (otherlv_9= 'slewRate' ( (lv_slewRate_10_0= ruleEFloat ) ) )? (otherlv_11= 'updatePeriod' ( (lv_updatePeriod_12_0= ruleEFloat ) ) )? otherlv_13= '}' ) ;
    public final EObject ruleGaussianSlewLimited() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        Token otherlv_11=null;
        Token otherlv_13=null;
        AntlrDatatypeRuleToken lv_minBound_4_0 = null;

        AntlrDatatypeRuleToken lv_maxBound_6_0 = null;

        AntlrDatatypeRuleToken lv_mean_8_0 = null;

        AntlrDatatypeRuleToken lv_slewRate_10_0 = null;

        AntlrDatatypeRuleToken lv_updatePeriod_12_0 = null;



        	enterRule();

        try {
            // InternalSimulatorDsl.g:739:2: ( ( () otherlv_1= 'GaussianSlewLimited' otherlv_2= '{' (otherlv_3= 'minBound' ( (lv_minBound_4_0= ruleEFloat ) ) )? (otherlv_5= 'maxBound' ( (lv_maxBound_6_0= ruleEFloat ) ) )? (otherlv_7= 'mean' ( (lv_mean_8_0= ruleEFloat ) ) )? (otherlv_9= 'slewRate' ( (lv_slewRate_10_0= ruleEFloat ) ) )? (otherlv_11= 'updatePeriod' ( (lv_updatePeriod_12_0= ruleEFloat ) ) )? otherlv_13= '}' ) )
            // InternalSimulatorDsl.g:740:2: ( () otherlv_1= 'GaussianSlewLimited' otherlv_2= '{' (otherlv_3= 'minBound' ( (lv_minBound_4_0= ruleEFloat ) ) )? (otherlv_5= 'maxBound' ( (lv_maxBound_6_0= ruleEFloat ) ) )? (otherlv_7= 'mean' ( (lv_mean_8_0= ruleEFloat ) ) )? (otherlv_9= 'slewRate' ( (lv_slewRate_10_0= ruleEFloat ) ) )? (otherlv_11= 'updatePeriod' ( (lv_updatePeriod_12_0= ruleEFloat ) ) )? otherlv_13= '}' )
            {
            // InternalSimulatorDsl.g:740:2: ( () otherlv_1= 'GaussianSlewLimited' otherlv_2= '{' (otherlv_3= 'minBound' ( (lv_minBound_4_0= ruleEFloat ) ) )? (otherlv_5= 'maxBound' ( (lv_maxBound_6_0= ruleEFloat ) ) )? (otherlv_7= 'mean' ( (lv_mean_8_0= ruleEFloat ) ) )? (otherlv_9= 'slewRate' ( (lv_slewRate_10_0= ruleEFloat ) ) )? (otherlv_11= 'updatePeriod' ( (lv_updatePeriod_12_0= ruleEFloat ) ) )? otherlv_13= '}' )
            // InternalSimulatorDsl.g:741:3: () otherlv_1= 'GaussianSlewLimited' otherlv_2= '{' (otherlv_3= 'minBound' ( (lv_minBound_4_0= ruleEFloat ) ) )? (otherlv_5= 'maxBound' ( (lv_maxBound_6_0= ruleEFloat ) ) )? (otherlv_7= 'mean' ( (lv_mean_8_0= ruleEFloat ) ) )? (otherlv_9= 'slewRate' ( (lv_slewRate_10_0= ruleEFloat ) ) )? (otherlv_11= 'updatePeriod' ( (lv_updatePeriod_12_0= ruleEFloat ) ) )? otherlv_13= '}'
            {
            // InternalSimulatorDsl.g:741:3: ()
            // InternalSimulatorDsl.g:742:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getGaussianSlewLimitedAccess().getGaussianSlewLimitedAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,28,FOLLOW_5); 

            			newLeafNode(otherlv_1, grammarAccess.getGaussianSlewLimitedAccess().getGaussianSlewLimitedKeyword_1());
            		
            otherlv_2=(Token)match(input,13,FOLLOW_27); 

            			newLeafNode(otherlv_2, grammarAccess.getGaussianSlewLimitedAccess().getLeftCurlyBracketKeyword_2());
            		
            // InternalSimulatorDsl.g:756:3: (otherlv_3= 'minBound' ( (lv_minBound_4_0= ruleEFloat ) ) )?
            int alt21=2;
            int LA21_0 = input.LA(1);

            if ( (LA21_0==29) ) {
                alt21=1;
            }
            switch (alt21) {
                case 1 :
                    // InternalSimulatorDsl.g:757:4: otherlv_3= 'minBound' ( (lv_minBound_4_0= ruleEFloat ) )
                    {
                    otherlv_3=(Token)match(input,29,FOLLOW_28); 

                    				newLeafNode(otherlv_3, grammarAccess.getGaussianSlewLimitedAccess().getMinBoundKeyword_3_0());
                    			
                    // InternalSimulatorDsl.g:761:4: ( (lv_minBound_4_0= ruleEFloat ) )
                    // InternalSimulatorDsl.g:762:5: (lv_minBound_4_0= ruleEFloat )
                    {
                    // InternalSimulatorDsl.g:762:5: (lv_minBound_4_0= ruleEFloat )
                    // InternalSimulatorDsl.g:763:6: lv_minBound_4_0= ruleEFloat
                    {

                    						newCompositeNode(grammarAccess.getGaussianSlewLimitedAccess().getMinBoundEFloatParserRuleCall_3_1_0());
                    					
                    pushFollow(FOLLOW_29);
                    lv_minBound_4_0=ruleEFloat();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getGaussianSlewLimitedRule());
                    						}
                    						set(
                    							current,
                    							"minBound",
                    							lv_minBound_4_0,
                    							"com.mncml.implementation.pogo.SimulatorDsl.EFloat");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalSimulatorDsl.g:781:3: (otherlv_5= 'maxBound' ( (lv_maxBound_6_0= ruleEFloat ) ) )?
            int alt22=2;
            int LA22_0 = input.LA(1);

            if ( (LA22_0==30) ) {
                alt22=1;
            }
            switch (alt22) {
                case 1 :
                    // InternalSimulatorDsl.g:782:4: otherlv_5= 'maxBound' ( (lv_maxBound_6_0= ruleEFloat ) )
                    {
                    otherlv_5=(Token)match(input,30,FOLLOW_28); 

                    				newLeafNode(otherlv_5, grammarAccess.getGaussianSlewLimitedAccess().getMaxBoundKeyword_4_0());
                    			
                    // InternalSimulatorDsl.g:786:4: ( (lv_maxBound_6_0= ruleEFloat ) )
                    // InternalSimulatorDsl.g:787:5: (lv_maxBound_6_0= ruleEFloat )
                    {
                    // InternalSimulatorDsl.g:787:5: (lv_maxBound_6_0= ruleEFloat )
                    // InternalSimulatorDsl.g:788:6: lv_maxBound_6_0= ruleEFloat
                    {

                    						newCompositeNode(grammarAccess.getGaussianSlewLimitedAccess().getMaxBoundEFloatParserRuleCall_4_1_0());
                    					
                    pushFollow(FOLLOW_30);
                    lv_maxBound_6_0=ruleEFloat();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getGaussianSlewLimitedRule());
                    						}
                    						set(
                    							current,
                    							"maxBound",
                    							lv_maxBound_6_0,
                    							"com.mncml.implementation.pogo.SimulatorDsl.EFloat");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalSimulatorDsl.g:806:3: (otherlv_7= 'mean' ( (lv_mean_8_0= ruleEFloat ) ) )?
            int alt23=2;
            int LA23_0 = input.LA(1);

            if ( (LA23_0==31) ) {
                alt23=1;
            }
            switch (alt23) {
                case 1 :
                    // InternalSimulatorDsl.g:807:4: otherlv_7= 'mean' ( (lv_mean_8_0= ruleEFloat ) )
                    {
                    otherlv_7=(Token)match(input,31,FOLLOW_28); 

                    				newLeafNode(otherlv_7, grammarAccess.getGaussianSlewLimitedAccess().getMeanKeyword_5_0());
                    			
                    // InternalSimulatorDsl.g:811:4: ( (lv_mean_8_0= ruleEFloat ) )
                    // InternalSimulatorDsl.g:812:5: (lv_mean_8_0= ruleEFloat )
                    {
                    // InternalSimulatorDsl.g:812:5: (lv_mean_8_0= ruleEFloat )
                    // InternalSimulatorDsl.g:813:6: lv_mean_8_0= ruleEFloat
                    {

                    						newCompositeNode(grammarAccess.getGaussianSlewLimitedAccess().getMeanEFloatParserRuleCall_5_1_0());
                    					
                    pushFollow(FOLLOW_31);
                    lv_mean_8_0=ruleEFloat();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getGaussianSlewLimitedRule());
                    						}
                    						set(
                    							current,
                    							"mean",
                    							lv_mean_8_0,
                    							"com.mncml.implementation.pogo.SimulatorDsl.EFloat");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalSimulatorDsl.g:831:3: (otherlv_9= 'slewRate' ( (lv_slewRate_10_0= ruleEFloat ) ) )?
            int alt24=2;
            int LA24_0 = input.LA(1);

            if ( (LA24_0==32) ) {
                alt24=1;
            }
            switch (alt24) {
                case 1 :
                    // InternalSimulatorDsl.g:832:4: otherlv_9= 'slewRate' ( (lv_slewRate_10_0= ruleEFloat ) )
                    {
                    otherlv_9=(Token)match(input,32,FOLLOW_28); 

                    				newLeafNode(otherlv_9, grammarAccess.getGaussianSlewLimitedAccess().getSlewRateKeyword_6_0());
                    			
                    // InternalSimulatorDsl.g:836:4: ( (lv_slewRate_10_0= ruleEFloat ) )
                    // InternalSimulatorDsl.g:837:5: (lv_slewRate_10_0= ruleEFloat )
                    {
                    // InternalSimulatorDsl.g:837:5: (lv_slewRate_10_0= ruleEFloat )
                    // InternalSimulatorDsl.g:838:6: lv_slewRate_10_0= ruleEFloat
                    {

                    						newCompositeNode(grammarAccess.getGaussianSlewLimitedAccess().getSlewRateEFloatParserRuleCall_6_1_0());
                    					
                    pushFollow(FOLLOW_32);
                    lv_slewRate_10_0=ruleEFloat();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getGaussianSlewLimitedRule());
                    						}
                    						set(
                    							current,
                    							"slewRate",
                    							lv_slewRate_10_0,
                    							"com.mncml.implementation.pogo.SimulatorDsl.EFloat");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalSimulatorDsl.g:856:3: (otherlv_11= 'updatePeriod' ( (lv_updatePeriod_12_0= ruleEFloat ) ) )?
            int alt25=2;
            int LA25_0 = input.LA(1);

            if ( (LA25_0==33) ) {
                alt25=1;
            }
            switch (alt25) {
                case 1 :
                    // InternalSimulatorDsl.g:857:4: otherlv_11= 'updatePeriod' ( (lv_updatePeriod_12_0= ruleEFloat ) )
                    {
                    otherlv_11=(Token)match(input,33,FOLLOW_28); 

                    				newLeafNode(otherlv_11, grammarAccess.getGaussianSlewLimitedAccess().getUpdatePeriodKeyword_7_0());
                    			
                    // InternalSimulatorDsl.g:861:4: ( (lv_updatePeriod_12_0= ruleEFloat ) )
                    // InternalSimulatorDsl.g:862:5: (lv_updatePeriod_12_0= ruleEFloat )
                    {
                    // InternalSimulatorDsl.g:862:5: (lv_updatePeriod_12_0= ruleEFloat )
                    // InternalSimulatorDsl.g:863:6: lv_updatePeriod_12_0= ruleEFloat
                    {

                    						newCompositeNode(grammarAccess.getGaussianSlewLimitedAccess().getUpdatePeriodEFloatParserRuleCall_7_1_0());
                    					
                    pushFollow(FOLLOW_15);
                    lv_updatePeriod_12_0=ruleEFloat();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getGaussianSlewLimitedRule());
                    						}
                    						set(
                    							current,
                    							"updatePeriod",
                    							lv_updatePeriod_12_0,
                    							"com.mncml.implementation.pogo.SimulatorDsl.EFloat");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            otherlv_13=(Token)match(input,16,FOLLOW_2); 

            			newLeafNode(otherlv_13, grammarAccess.getGaussianSlewLimitedAccess().getRightCurlyBracketKeyword_8());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleGaussianSlewLimited"


    // $ANTLR start "entryRuleConstantQuantity"
    // InternalSimulatorDsl.g:889:1: entryRuleConstantQuantity returns [EObject current=null] : iv_ruleConstantQuantity= ruleConstantQuantity EOF ;
    public final EObject entryRuleConstantQuantity() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleConstantQuantity = null;


        try {
            // InternalSimulatorDsl.g:889:57: (iv_ruleConstantQuantity= ruleConstantQuantity EOF )
            // InternalSimulatorDsl.g:890:2: iv_ruleConstantQuantity= ruleConstantQuantity EOF
            {
             newCompositeNode(grammarAccess.getConstantQuantityRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleConstantQuantity=ruleConstantQuantity();

            state._fsp--;

             current =iv_ruleConstantQuantity; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleConstantQuantity"


    // $ANTLR start "ruleConstantQuantity"
    // InternalSimulatorDsl.g:896:1: ruleConstantQuantity returns [EObject current=null] : ( () otherlv_1= 'ConstantQuantity' otherlv_2= '{' (otherlv_3= 'initialValue' ( (lv_initialValue_4_0= ruleEFloat ) ) )? (otherlv_5= 'quality' ( (lv_quality_6_0= ruleEFloat ) ) )? otherlv_7= '}' ) ;
    public final EObject ruleConstantQuantity() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        AntlrDatatypeRuleToken lv_initialValue_4_0 = null;

        AntlrDatatypeRuleToken lv_quality_6_0 = null;



        	enterRule();

        try {
            // InternalSimulatorDsl.g:902:2: ( ( () otherlv_1= 'ConstantQuantity' otherlv_2= '{' (otherlv_3= 'initialValue' ( (lv_initialValue_4_0= ruleEFloat ) ) )? (otherlv_5= 'quality' ( (lv_quality_6_0= ruleEFloat ) ) )? otherlv_7= '}' ) )
            // InternalSimulatorDsl.g:903:2: ( () otherlv_1= 'ConstantQuantity' otherlv_2= '{' (otherlv_3= 'initialValue' ( (lv_initialValue_4_0= ruleEFloat ) ) )? (otherlv_5= 'quality' ( (lv_quality_6_0= ruleEFloat ) ) )? otherlv_7= '}' )
            {
            // InternalSimulatorDsl.g:903:2: ( () otherlv_1= 'ConstantQuantity' otherlv_2= '{' (otherlv_3= 'initialValue' ( (lv_initialValue_4_0= ruleEFloat ) ) )? (otherlv_5= 'quality' ( (lv_quality_6_0= ruleEFloat ) ) )? otherlv_7= '}' )
            // InternalSimulatorDsl.g:904:3: () otherlv_1= 'ConstantQuantity' otherlv_2= '{' (otherlv_3= 'initialValue' ( (lv_initialValue_4_0= ruleEFloat ) ) )? (otherlv_5= 'quality' ( (lv_quality_6_0= ruleEFloat ) ) )? otherlv_7= '}'
            {
            // InternalSimulatorDsl.g:904:3: ()
            // InternalSimulatorDsl.g:905:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getConstantQuantityAccess().getConstantQuantityAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,34,FOLLOW_5); 

            			newLeafNode(otherlv_1, grammarAccess.getConstantQuantityAccess().getConstantQuantityKeyword_1());
            		
            otherlv_2=(Token)match(input,13,FOLLOW_33); 

            			newLeafNode(otherlv_2, grammarAccess.getConstantQuantityAccess().getLeftCurlyBracketKeyword_2());
            		
            // InternalSimulatorDsl.g:919:3: (otherlv_3= 'initialValue' ( (lv_initialValue_4_0= ruleEFloat ) ) )?
            int alt26=2;
            int LA26_0 = input.LA(1);

            if ( (LA26_0==35) ) {
                alt26=1;
            }
            switch (alt26) {
                case 1 :
                    // InternalSimulatorDsl.g:920:4: otherlv_3= 'initialValue' ( (lv_initialValue_4_0= ruleEFloat ) )
                    {
                    otherlv_3=(Token)match(input,35,FOLLOW_28); 

                    				newLeafNode(otherlv_3, grammarAccess.getConstantQuantityAccess().getInitialValueKeyword_3_0());
                    			
                    // InternalSimulatorDsl.g:924:4: ( (lv_initialValue_4_0= ruleEFloat ) )
                    // InternalSimulatorDsl.g:925:5: (lv_initialValue_4_0= ruleEFloat )
                    {
                    // InternalSimulatorDsl.g:925:5: (lv_initialValue_4_0= ruleEFloat )
                    // InternalSimulatorDsl.g:926:6: lv_initialValue_4_0= ruleEFloat
                    {

                    						newCompositeNode(grammarAccess.getConstantQuantityAccess().getInitialValueEFloatParserRuleCall_3_1_0());
                    					
                    pushFollow(FOLLOW_34);
                    lv_initialValue_4_0=ruleEFloat();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getConstantQuantityRule());
                    						}
                    						set(
                    							current,
                    							"initialValue",
                    							lv_initialValue_4_0,
                    							"com.mncml.implementation.pogo.SimulatorDsl.EFloat");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalSimulatorDsl.g:944:3: (otherlv_5= 'quality' ( (lv_quality_6_0= ruleEFloat ) ) )?
            int alt27=2;
            int LA27_0 = input.LA(1);

            if ( (LA27_0==36) ) {
                alt27=1;
            }
            switch (alt27) {
                case 1 :
                    // InternalSimulatorDsl.g:945:4: otherlv_5= 'quality' ( (lv_quality_6_0= ruleEFloat ) )
                    {
                    otherlv_5=(Token)match(input,36,FOLLOW_28); 

                    				newLeafNode(otherlv_5, grammarAccess.getConstantQuantityAccess().getQualityKeyword_4_0());
                    			
                    // InternalSimulatorDsl.g:949:4: ( (lv_quality_6_0= ruleEFloat ) )
                    // InternalSimulatorDsl.g:950:5: (lv_quality_6_0= ruleEFloat )
                    {
                    // InternalSimulatorDsl.g:950:5: (lv_quality_6_0= ruleEFloat )
                    // InternalSimulatorDsl.g:951:6: lv_quality_6_0= ruleEFloat
                    {

                    						newCompositeNode(grammarAccess.getConstantQuantityAccess().getQualityEFloatParserRuleCall_4_1_0());
                    					
                    pushFollow(FOLLOW_15);
                    lv_quality_6_0=ruleEFloat();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getConstantQuantityRule());
                    						}
                    						set(
                    							current,
                    							"quality",
                    							lv_quality_6_0,
                    							"com.mncml.implementation.pogo.SimulatorDsl.EFloat");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            otherlv_7=(Token)match(input,16,FOLLOW_2); 

            			newLeafNode(otherlv_7, grammarAccess.getConstantQuantityAccess().getRightCurlyBracketKeyword_5());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleConstantQuantity"


    // $ANTLR start "entryRuleDeterministicSignal"
    // InternalSimulatorDsl.g:977:1: entryRuleDeterministicSignal returns [EObject current=null] : iv_ruleDeterministicSignal= ruleDeterministicSignal EOF ;
    public final EObject entryRuleDeterministicSignal() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDeterministicSignal = null;


        try {
            // InternalSimulatorDsl.g:977:60: (iv_ruleDeterministicSignal= ruleDeterministicSignal EOF )
            // InternalSimulatorDsl.g:978:2: iv_ruleDeterministicSignal= ruleDeterministicSignal EOF
            {
             newCompositeNode(grammarAccess.getDeterministicSignalRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleDeterministicSignal=ruleDeterministicSignal();

            state._fsp--;

             current =iv_ruleDeterministicSignal; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDeterministicSignal"


    // $ANTLR start "ruleDeterministicSignal"
    // InternalSimulatorDsl.g:984:1: ruleDeterministicSignal returns [EObject current=null] : ( () otherlv_1= 'DeterministicSignal' otherlv_2= '{' (otherlv_3= 'type' ( (lv_type_4_0= ruleEString ) ) )? (otherlv_5= 'amplitude' ( (lv_amplitude_6_0= ruleEFloat ) ) )? (otherlv_7= 'period' ( (lv_period_8_0= ruleEInt ) ) )? (otherlv_9= 'offset' ( (lv_offset_10_0= ruleEFloat ) ) )? otherlv_11= '}' ) ;
    public final EObject ruleDeterministicSignal() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        Token otherlv_11=null;
        AntlrDatatypeRuleToken lv_type_4_0 = null;

        AntlrDatatypeRuleToken lv_amplitude_6_0 = null;

        AntlrDatatypeRuleToken lv_period_8_0 = null;

        AntlrDatatypeRuleToken lv_offset_10_0 = null;



        	enterRule();

        try {
            // InternalSimulatorDsl.g:990:2: ( ( () otherlv_1= 'DeterministicSignal' otherlv_2= '{' (otherlv_3= 'type' ( (lv_type_4_0= ruleEString ) ) )? (otherlv_5= 'amplitude' ( (lv_amplitude_6_0= ruleEFloat ) ) )? (otherlv_7= 'period' ( (lv_period_8_0= ruleEInt ) ) )? (otherlv_9= 'offset' ( (lv_offset_10_0= ruleEFloat ) ) )? otherlv_11= '}' ) )
            // InternalSimulatorDsl.g:991:2: ( () otherlv_1= 'DeterministicSignal' otherlv_2= '{' (otherlv_3= 'type' ( (lv_type_4_0= ruleEString ) ) )? (otherlv_5= 'amplitude' ( (lv_amplitude_6_0= ruleEFloat ) ) )? (otherlv_7= 'period' ( (lv_period_8_0= ruleEInt ) ) )? (otherlv_9= 'offset' ( (lv_offset_10_0= ruleEFloat ) ) )? otherlv_11= '}' )
            {
            // InternalSimulatorDsl.g:991:2: ( () otherlv_1= 'DeterministicSignal' otherlv_2= '{' (otherlv_3= 'type' ( (lv_type_4_0= ruleEString ) ) )? (otherlv_5= 'amplitude' ( (lv_amplitude_6_0= ruleEFloat ) ) )? (otherlv_7= 'period' ( (lv_period_8_0= ruleEInt ) ) )? (otherlv_9= 'offset' ( (lv_offset_10_0= ruleEFloat ) ) )? otherlv_11= '}' )
            // InternalSimulatorDsl.g:992:3: () otherlv_1= 'DeterministicSignal' otherlv_2= '{' (otherlv_3= 'type' ( (lv_type_4_0= ruleEString ) ) )? (otherlv_5= 'amplitude' ( (lv_amplitude_6_0= ruleEFloat ) ) )? (otherlv_7= 'period' ( (lv_period_8_0= ruleEInt ) ) )? (otherlv_9= 'offset' ( (lv_offset_10_0= ruleEFloat ) ) )? otherlv_11= '}'
            {
            // InternalSimulatorDsl.g:992:3: ()
            // InternalSimulatorDsl.g:993:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getDeterministicSignalAccess().getDeterministicSignalAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,37,FOLLOW_5); 

            			newLeafNode(otherlv_1, grammarAccess.getDeterministicSignalAccess().getDeterministicSignalKeyword_1());
            		
            otherlv_2=(Token)match(input,13,FOLLOW_35); 

            			newLeafNode(otherlv_2, grammarAccess.getDeterministicSignalAccess().getLeftCurlyBracketKeyword_2());
            		
            // InternalSimulatorDsl.g:1007:3: (otherlv_3= 'type' ( (lv_type_4_0= ruleEString ) ) )?
            int alt28=2;
            int LA28_0 = input.LA(1);

            if ( (LA28_0==38) ) {
                alt28=1;
            }
            switch (alt28) {
                case 1 :
                    // InternalSimulatorDsl.g:1008:4: otherlv_3= 'type' ( (lv_type_4_0= ruleEString ) )
                    {
                    otherlv_3=(Token)match(input,38,FOLLOW_19); 

                    				newLeafNode(otherlv_3, grammarAccess.getDeterministicSignalAccess().getTypeKeyword_3_0());
                    			
                    // InternalSimulatorDsl.g:1012:4: ( (lv_type_4_0= ruleEString ) )
                    // InternalSimulatorDsl.g:1013:5: (lv_type_4_0= ruleEString )
                    {
                    // InternalSimulatorDsl.g:1013:5: (lv_type_4_0= ruleEString )
                    // InternalSimulatorDsl.g:1014:6: lv_type_4_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getDeterministicSignalAccess().getTypeEStringParserRuleCall_3_1_0());
                    					
                    pushFollow(FOLLOW_36);
                    lv_type_4_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getDeterministicSignalRule());
                    						}
                    						set(
                    							current,
                    							"type",
                    							lv_type_4_0,
                    							"com.mncml.implementation.pogo.SimulatorDsl.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalSimulatorDsl.g:1032:3: (otherlv_5= 'amplitude' ( (lv_amplitude_6_0= ruleEFloat ) ) )?
            int alt29=2;
            int LA29_0 = input.LA(1);

            if ( (LA29_0==39) ) {
                alt29=1;
            }
            switch (alt29) {
                case 1 :
                    // InternalSimulatorDsl.g:1033:4: otherlv_5= 'amplitude' ( (lv_amplitude_6_0= ruleEFloat ) )
                    {
                    otherlv_5=(Token)match(input,39,FOLLOW_28); 

                    				newLeafNode(otherlv_5, grammarAccess.getDeterministicSignalAccess().getAmplitudeKeyword_4_0());
                    			
                    // InternalSimulatorDsl.g:1037:4: ( (lv_amplitude_6_0= ruleEFloat ) )
                    // InternalSimulatorDsl.g:1038:5: (lv_amplitude_6_0= ruleEFloat )
                    {
                    // InternalSimulatorDsl.g:1038:5: (lv_amplitude_6_0= ruleEFloat )
                    // InternalSimulatorDsl.g:1039:6: lv_amplitude_6_0= ruleEFloat
                    {

                    						newCompositeNode(grammarAccess.getDeterministicSignalAccess().getAmplitudeEFloatParserRuleCall_4_1_0());
                    					
                    pushFollow(FOLLOW_37);
                    lv_amplitude_6_0=ruleEFloat();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getDeterministicSignalRule());
                    						}
                    						set(
                    							current,
                    							"amplitude",
                    							lv_amplitude_6_0,
                    							"com.mncml.implementation.pogo.SimulatorDsl.EFloat");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalSimulatorDsl.g:1057:3: (otherlv_7= 'period' ( (lv_period_8_0= ruleEInt ) ) )?
            int alt30=2;
            int LA30_0 = input.LA(1);

            if ( (LA30_0==40) ) {
                alt30=1;
            }
            switch (alt30) {
                case 1 :
                    // InternalSimulatorDsl.g:1058:4: otherlv_7= 'period' ( (lv_period_8_0= ruleEInt ) )
                    {
                    otherlv_7=(Token)match(input,40,FOLLOW_38); 

                    				newLeafNode(otherlv_7, grammarAccess.getDeterministicSignalAccess().getPeriodKeyword_5_0());
                    			
                    // InternalSimulatorDsl.g:1062:4: ( (lv_period_8_0= ruleEInt ) )
                    // InternalSimulatorDsl.g:1063:5: (lv_period_8_0= ruleEInt )
                    {
                    // InternalSimulatorDsl.g:1063:5: (lv_period_8_0= ruleEInt )
                    // InternalSimulatorDsl.g:1064:6: lv_period_8_0= ruleEInt
                    {

                    						newCompositeNode(grammarAccess.getDeterministicSignalAccess().getPeriodEIntParserRuleCall_5_1_0());
                    					
                    pushFollow(FOLLOW_39);
                    lv_period_8_0=ruleEInt();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getDeterministicSignalRule());
                    						}
                    						set(
                    							current,
                    							"period",
                    							lv_period_8_0,
                    							"com.mncml.implementation.pogo.SimulatorDsl.EInt");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalSimulatorDsl.g:1082:3: (otherlv_9= 'offset' ( (lv_offset_10_0= ruleEFloat ) ) )?
            int alt31=2;
            int LA31_0 = input.LA(1);

            if ( (LA31_0==41) ) {
                alt31=1;
            }
            switch (alt31) {
                case 1 :
                    // InternalSimulatorDsl.g:1083:4: otherlv_9= 'offset' ( (lv_offset_10_0= ruleEFloat ) )
                    {
                    otherlv_9=(Token)match(input,41,FOLLOW_28); 

                    				newLeafNode(otherlv_9, grammarAccess.getDeterministicSignalAccess().getOffsetKeyword_6_0());
                    			
                    // InternalSimulatorDsl.g:1087:4: ( (lv_offset_10_0= ruleEFloat ) )
                    // InternalSimulatorDsl.g:1088:5: (lv_offset_10_0= ruleEFloat )
                    {
                    // InternalSimulatorDsl.g:1088:5: (lv_offset_10_0= ruleEFloat )
                    // InternalSimulatorDsl.g:1089:6: lv_offset_10_0= ruleEFloat
                    {

                    						newCompositeNode(grammarAccess.getDeterministicSignalAccess().getOffsetEFloatParserRuleCall_6_1_0());
                    					
                    pushFollow(FOLLOW_15);
                    lv_offset_10_0=ruleEFloat();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getDeterministicSignalRule());
                    						}
                    						set(
                    							current,
                    							"offset",
                    							lv_offset_10_0,
                    							"com.mncml.implementation.pogo.SimulatorDsl.EFloat");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            otherlv_11=(Token)match(input,16,FOLLOW_2); 

            			newLeafNode(otherlv_11, grammarAccess.getDeterministicSignalAccess().getRightCurlyBracketKeyword_7());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDeterministicSignal"


    // $ANTLR start "entryRuleRuntimeSpecifiedWaveform"
    // InternalSimulatorDsl.g:1115:1: entryRuleRuntimeSpecifiedWaveform returns [EObject current=null] : iv_ruleRuntimeSpecifiedWaveform= ruleRuntimeSpecifiedWaveform EOF ;
    public final EObject entryRuleRuntimeSpecifiedWaveform() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRuntimeSpecifiedWaveform = null;


        try {
            // InternalSimulatorDsl.g:1115:65: (iv_ruleRuntimeSpecifiedWaveform= ruleRuntimeSpecifiedWaveform EOF )
            // InternalSimulatorDsl.g:1116:2: iv_ruleRuntimeSpecifiedWaveform= ruleRuntimeSpecifiedWaveform EOF
            {
             newCompositeNode(grammarAccess.getRuntimeSpecifiedWaveformRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleRuntimeSpecifiedWaveform=ruleRuntimeSpecifiedWaveform();

            state._fsp--;

             current =iv_ruleRuntimeSpecifiedWaveform; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRuntimeSpecifiedWaveform"


    // $ANTLR start "ruleRuntimeSpecifiedWaveform"
    // InternalSimulatorDsl.g:1122:1: ruleRuntimeSpecifiedWaveform returns [EObject current=null] : ( () otherlv_1= 'RuntimeSpecifiedWaveform' otherlv_2= '{' (otherlv_3= 'defaultValue' ( (lv_defaultValue_4_0= ruleEFloat ) ) )? (otherlv_5= 'timestamp' ( (lv_timestamp_6_0= ruleEString ) ) )? (otherlv_7= 'attribute_qualities' ( (lv_attribute_qualities_8_0= ruleEFloat ) ) )? otherlv_9= '}' ) ;
    public final EObject ruleRuntimeSpecifiedWaveform() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        AntlrDatatypeRuleToken lv_defaultValue_4_0 = null;

        AntlrDatatypeRuleToken lv_timestamp_6_0 = null;

        AntlrDatatypeRuleToken lv_attribute_qualities_8_0 = null;



        	enterRule();

        try {
            // InternalSimulatorDsl.g:1128:2: ( ( () otherlv_1= 'RuntimeSpecifiedWaveform' otherlv_2= '{' (otherlv_3= 'defaultValue' ( (lv_defaultValue_4_0= ruleEFloat ) ) )? (otherlv_5= 'timestamp' ( (lv_timestamp_6_0= ruleEString ) ) )? (otherlv_7= 'attribute_qualities' ( (lv_attribute_qualities_8_0= ruleEFloat ) ) )? otherlv_9= '}' ) )
            // InternalSimulatorDsl.g:1129:2: ( () otherlv_1= 'RuntimeSpecifiedWaveform' otherlv_2= '{' (otherlv_3= 'defaultValue' ( (lv_defaultValue_4_0= ruleEFloat ) ) )? (otherlv_5= 'timestamp' ( (lv_timestamp_6_0= ruleEString ) ) )? (otherlv_7= 'attribute_qualities' ( (lv_attribute_qualities_8_0= ruleEFloat ) ) )? otherlv_9= '}' )
            {
            // InternalSimulatorDsl.g:1129:2: ( () otherlv_1= 'RuntimeSpecifiedWaveform' otherlv_2= '{' (otherlv_3= 'defaultValue' ( (lv_defaultValue_4_0= ruleEFloat ) ) )? (otherlv_5= 'timestamp' ( (lv_timestamp_6_0= ruleEString ) ) )? (otherlv_7= 'attribute_qualities' ( (lv_attribute_qualities_8_0= ruleEFloat ) ) )? otherlv_9= '}' )
            // InternalSimulatorDsl.g:1130:3: () otherlv_1= 'RuntimeSpecifiedWaveform' otherlv_2= '{' (otherlv_3= 'defaultValue' ( (lv_defaultValue_4_0= ruleEFloat ) ) )? (otherlv_5= 'timestamp' ( (lv_timestamp_6_0= ruleEString ) ) )? (otherlv_7= 'attribute_qualities' ( (lv_attribute_qualities_8_0= ruleEFloat ) ) )? otherlv_9= '}'
            {
            // InternalSimulatorDsl.g:1130:3: ()
            // InternalSimulatorDsl.g:1131:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getRuntimeSpecifiedWaveformAccess().getRuntimeSpecifiedWaveformAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,42,FOLLOW_5); 

            			newLeafNode(otherlv_1, grammarAccess.getRuntimeSpecifiedWaveformAccess().getRuntimeSpecifiedWaveformKeyword_1());
            		
            otherlv_2=(Token)match(input,13,FOLLOW_40); 

            			newLeafNode(otherlv_2, grammarAccess.getRuntimeSpecifiedWaveformAccess().getLeftCurlyBracketKeyword_2());
            		
            // InternalSimulatorDsl.g:1145:3: (otherlv_3= 'defaultValue' ( (lv_defaultValue_4_0= ruleEFloat ) ) )?
            int alt32=2;
            int LA32_0 = input.LA(1);

            if ( (LA32_0==43) ) {
                alt32=1;
            }
            switch (alt32) {
                case 1 :
                    // InternalSimulatorDsl.g:1146:4: otherlv_3= 'defaultValue' ( (lv_defaultValue_4_0= ruleEFloat ) )
                    {
                    otherlv_3=(Token)match(input,43,FOLLOW_28); 

                    				newLeafNode(otherlv_3, grammarAccess.getRuntimeSpecifiedWaveformAccess().getDefaultValueKeyword_3_0());
                    			
                    // InternalSimulatorDsl.g:1150:4: ( (lv_defaultValue_4_0= ruleEFloat ) )
                    // InternalSimulatorDsl.g:1151:5: (lv_defaultValue_4_0= ruleEFloat )
                    {
                    // InternalSimulatorDsl.g:1151:5: (lv_defaultValue_4_0= ruleEFloat )
                    // InternalSimulatorDsl.g:1152:6: lv_defaultValue_4_0= ruleEFloat
                    {

                    						newCompositeNode(grammarAccess.getRuntimeSpecifiedWaveformAccess().getDefaultValueEFloatParserRuleCall_3_1_0());
                    					
                    pushFollow(FOLLOW_41);
                    lv_defaultValue_4_0=ruleEFloat();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getRuntimeSpecifiedWaveformRule());
                    						}
                    						set(
                    							current,
                    							"defaultValue",
                    							lv_defaultValue_4_0,
                    							"com.mncml.implementation.pogo.SimulatorDsl.EFloat");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalSimulatorDsl.g:1170:3: (otherlv_5= 'timestamp' ( (lv_timestamp_6_0= ruleEString ) ) )?
            int alt33=2;
            int LA33_0 = input.LA(1);

            if ( (LA33_0==44) ) {
                alt33=1;
            }
            switch (alt33) {
                case 1 :
                    // InternalSimulatorDsl.g:1171:4: otherlv_5= 'timestamp' ( (lv_timestamp_6_0= ruleEString ) )
                    {
                    otherlv_5=(Token)match(input,44,FOLLOW_19); 

                    				newLeafNode(otherlv_5, grammarAccess.getRuntimeSpecifiedWaveformAccess().getTimestampKeyword_4_0());
                    			
                    // InternalSimulatorDsl.g:1175:4: ( (lv_timestamp_6_0= ruleEString ) )
                    // InternalSimulatorDsl.g:1176:5: (lv_timestamp_6_0= ruleEString )
                    {
                    // InternalSimulatorDsl.g:1176:5: (lv_timestamp_6_0= ruleEString )
                    // InternalSimulatorDsl.g:1177:6: lv_timestamp_6_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getRuntimeSpecifiedWaveformAccess().getTimestampEStringParserRuleCall_4_1_0());
                    					
                    pushFollow(FOLLOW_42);
                    lv_timestamp_6_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getRuntimeSpecifiedWaveformRule());
                    						}
                    						set(
                    							current,
                    							"timestamp",
                    							lv_timestamp_6_0,
                    							"com.mncml.implementation.pogo.SimulatorDsl.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalSimulatorDsl.g:1195:3: (otherlv_7= 'attribute_qualities' ( (lv_attribute_qualities_8_0= ruleEFloat ) ) )?
            int alt34=2;
            int LA34_0 = input.LA(1);

            if ( (LA34_0==45) ) {
                alt34=1;
            }
            switch (alt34) {
                case 1 :
                    // InternalSimulatorDsl.g:1196:4: otherlv_7= 'attribute_qualities' ( (lv_attribute_qualities_8_0= ruleEFloat ) )
                    {
                    otherlv_7=(Token)match(input,45,FOLLOW_28); 

                    				newLeafNode(otherlv_7, grammarAccess.getRuntimeSpecifiedWaveformAccess().getAttribute_qualitiesKeyword_5_0());
                    			
                    // InternalSimulatorDsl.g:1200:4: ( (lv_attribute_qualities_8_0= ruleEFloat ) )
                    // InternalSimulatorDsl.g:1201:5: (lv_attribute_qualities_8_0= ruleEFloat )
                    {
                    // InternalSimulatorDsl.g:1201:5: (lv_attribute_qualities_8_0= ruleEFloat )
                    // InternalSimulatorDsl.g:1202:6: lv_attribute_qualities_8_0= ruleEFloat
                    {

                    						newCompositeNode(grammarAccess.getRuntimeSpecifiedWaveformAccess().getAttribute_qualitiesEFloatParserRuleCall_5_1_0());
                    					
                    pushFollow(FOLLOW_15);
                    lv_attribute_qualities_8_0=ruleEFloat();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getRuntimeSpecifiedWaveformRule());
                    						}
                    						set(
                    							current,
                    							"attribute_qualities",
                    							lv_attribute_qualities_8_0,
                    							"com.mncml.implementation.pogo.SimulatorDsl.EFloat");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            otherlv_9=(Token)match(input,16,FOLLOW_2); 

            			newLeafNode(otherlv_9, grammarAccess.getRuntimeSpecifiedWaveformAccess().getRightCurlyBracketKeyword_6());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRuntimeSpecifiedWaveform"


    // $ANTLR start "entryRuleEString"
    // InternalSimulatorDsl.g:1228:1: entryRuleEString returns [String current=null] : iv_ruleEString= ruleEString EOF ;
    public final String entryRuleEString() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleEString = null;


        try {
            // InternalSimulatorDsl.g:1228:47: (iv_ruleEString= ruleEString EOF )
            // InternalSimulatorDsl.g:1229:2: iv_ruleEString= ruleEString EOF
            {
             newCompositeNode(grammarAccess.getEStringRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleEString=ruleEString();

            state._fsp--;

             current =iv_ruleEString.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEString"


    // $ANTLR start "ruleEString"
    // InternalSimulatorDsl.g:1235:1: ruleEString returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID ) ;
    public final AntlrDatatypeRuleToken ruleEString() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_STRING_0=null;
        Token this_ID_1=null;


        	enterRule();

        try {
            // InternalSimulatorDsl.g:1241:2: ( (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID ) )
            // InternalSimulatorDsl.g:1242:2: (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID )
            {
            // InternalSimulatorDsl.g:1242:2: (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID )
            int alt35=2;
            int LA35_0 = input.LA(1);

            if ( (LA35_0==RULE_STRING) ) {
                alt35=1;
            }
            else if ( (LA35_0==RULE_ID) ) {
                alt35=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 35, 0, input);

                throw nvae;
            }
            switch (alt35) {
                case 1 :
                    // InternalSimulatorDsl.g:1243:3: this_STRING_0= RULE_STRING
                    {
                    this_STRING_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

                    			current.merge(this_STRING_0);
                    		

                    			newLeafNode(this_STRING_0, grammarAccess.getEStringAccess().getSTRINGTerminalRuleCall_0());
                    		

                    }
                    break;
                case 2 :
                    // InternalSimulatorDsl.g:1251:3: this_ID_1= RULE_ID
                    {
                    this_ID_1=(Token)match(input,RULE_ID,FOLLOW_2); 

                    			current.merge(this_ID_1);
                    		

                    			newLeafNode(this_ID_1, grammarAccess.getEStringAccess().getIDTerminalRuleCall_1());
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEString"


    // $ANTLR start "entryRuleEFloat"
    // InternalSimulatorDsl.g:1262:1: entryRuleEFloat returns [String current=null] : iv_ruleEFloat= ruleEFloat EOF ;
    public final String entryRuleEFloat() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleEFloat = null;


        try {
            // InternalSimulatorDsl.g:1262:46: (iv_ruleEFloat= ruleEFloat EOF )
            // InternalSimulatorDsl.g:1263:2: iv_ruleEFloat= ruleEFloat EOF
            {
             newCompositeNode(grammarAccess.getEFloatRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleEFloat=ruleEFloat();

            state._fsp--;

             current =iv_ruleEFloat.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEFloat"


    // $ANTLR start "ruleEFloat"
    // InternalSimulatorDsl.g:1269:1: ruleEFloat returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : ( (kw= '-' )? (this_INT_1= RULE_INT )? kw= '.' this_INT_3= RULE_INT ( (kw= 'E' | kw= 'e' ) (kw= '-' )? this_INT_7= RULE_INT )? ) ;
    public final AntlrDatatypeRuleToken ruleEFloat() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;
        Token this_INT_1=null;
        Token this_INT_3=null;
        Token this_INT_7=null;


        	enterRule();

        try {
            // InternalSimulatorDsl.g:1275:2: ( ( (kw= '-' )? (this_INT_1= RULE_INT )? kw= '.' this_INT_3= RULE_INT ( (kw= 'E' | kw= 'e' ) (kw= '-' )? this_INT_7= RULE_INT )? ) )
            // InternalSimulatorDsl.g:1276:2: ( (kw= '-' )? (this_INT_1= RULE_INT )? kw= '.' this_INT_3= RULE_INT ( (kw= 'E' | kw= 'e' ) (kw= '-' )? this_INT_7= RULE_INT )? )
            {
            // InternalSimulatorDsl.g:1276:2: ( (kw= '-' )? (this_INT_1= RULE_INT )? kw= '.' this_INT_3= RULE_INT ( (kw= 'E' | kw= 'e' ) (kw= '-' )? this_INT_7= RULE_INT )? )
            // InternalSimulatorDsl.g:1277:3: (kw= '-' )? (this_INT_1= RULE_INT )? kw= '.' this_INT_3= RULE_INT ( (kw= 'E' | kw= 'e' ) (kw= '-' )? this_INT_7= RULE_INT )?
            {
            // InternalSimulatorDsl.g:1277:3: (kw= '-' )?
            int alt36=2;
            int LA36_0 = input.LA(1);

            if ( (LA36_0==46) ) {
                alt36=1;
            }
            switch (alt36) {
                case 1 :
                    // InternalSimulatorDsl.g:1278:4: kw= '-'
                    {
                    kw=(Token)match(input,46,FOLLOW_43); 

                    				current.merge(kw);
                    				newLeafNode(kw, grammarAccess.getEFloatAccess().getHyphenMinusKeyword_0());
                    			

                    }
                    break;

            }

            // InternalSimulatorDsl.g:1284:3: (this_INT_1= RULE_INT )?
            int alt37=2;
            int LA37_0 = input.LA(1);

            if ( (LA37_0==RULE_INT) ) {
                alt37=1;
            }
            switch (alt37) {
                case 1 :
                    // InternalSimulatorDsl.g:1285:4: this_INT_1= RULE_INT
                    {
                    this_INT_1=(Token)match(input,RULE_INT,FOLLOW_44); 

                    				current.merge(this_INT_1);
                    			

                    				newLeafNode(this_INT_1, grammarAccess.getEFloatAccess().getINTTerminalRuleCall_1());
                    			

                    }
                    break;

            }

            kw=(Token)match(input,24,FOLLOW_45); 

            			current.merge(kw);
            			newLeafNode(kw, grammarAccess.getEFloatAccess().getFullStopKeyword_2());
            		
            this_INT_3=(Token)match(input,RULE_INT,FOLLOW_46); 

            			current.merge(this_INT_3);
            		

            			newLeafNode(this_INT_3, grammarAccess.getEFloatAccess().getINTTerminalRuleCall_3());
            		
            // InternalSimulatorDsl.g:1305:3: ( (kw= 'E' | kw= 'e' ) (kw= '-' )? this_INT_7= RULE_INT )?
            int alt40=2;
            int LA40_0 = input.LA(1);

            if ( ((LA40_0>=47 && LA40_0<=48)) ) {
                alt40=1;
            }
            switch (alt40) {
                case 1 :
                    // InternalSimulatorDsl.g:1306:4: (kw= 'E' | kw= 'e' ) (kw= '-' )? this_INT_7= RULE_INT
                    {
                    // InternalSimulatorDsl.g:1306:4: (kw= 'E' | kw= 'e' )
                    int alt38=2;
                    int LA38_0 = input.LA(1);

                    if ( (LA38_0==47) ) {
                        alt38=1;
                    }
                    else if ( (LA38_0==48) ) {
                        alt38=2;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 38, 0, input);

                        throw nvae;
                    }
                    switch (alt38) {
                        case 1 :
                            // InternalSimulatorDsl.g:1307:5: kw= 'E'
                            {
                            kw=(Token)match(input,47,FOLLOW_38); 

                            					current.merge(kw);
                            					newLeafNode(kw, grammarAccess.getEFloatAccess().getEKeyword_4_0_0());
                            				

                            }
                            break;
                        case 2 :
                            // InternalSimulatorDsl.g:1313:5: kw= 'e'
                            {
                            kw=(Token)match(input,48,FOLLOW_38); 

                            					current.merge(kw);
                            					newLeafNode(kw, grammarAccess.getEFloatAccess().getEKeyword_4_0_1());
                            				

                            }
                            break;

                    }

                    // InternalSimulatorDsl.g:1319:4: (kw= '-' )?
                    int alt39=2;
                    int LA39_0 = input.LA(1);

                    if ( (LA39_0==46) ) {
                        alt39=1;
                    }
                    switch (alt39) {
                        case 1 :
                            // InternalSimulatorDsl.g:1320:5: kw= '-'
                            {
                            kw=(Token)match(input,46,FOLLOW_45); 

                            					current.merge(kw);
                            					newLeafNode(kw, grammarAccess.getEFloatAccess().getHyphenMinusKeyword_4_1());
                            				

                            }
                            break;

                    }

                    this_INT_7=(Token)match(input,RULE_INT,FOLLOW_2); 

                    				current.merge(this_INT_7);
                    			

                    				newLeafNode(this_INT_7, grammarAccess.getEFloatAccess().getINTTerminalRuleCall_4_2());
                    			

                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEFloat"


    // $ANTLR start "entryRuleEInt"
    // InternalSimulatorDsl.g:1338:1: entryRuleEInt returns [String current=null] : iv_ruleEInt= ruleEInt EOF ;
    public final String entryRuleEInt() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleEInt = null;


        try {
            // InternalSimulatorDsl.g:1338:44: (iv_ruleEInt= ruleEInt EOF )
            // InternalSimulatorDsl.g:1339:2: iv_ruleEInt= ruleEInt EOF
            {
             newCompositeNode(grammarAccess.getEIntRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleEInt=ruleEInt();

            state._fsp--;

             current =iv_ruleEInt.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEInt"


    // $ANTLR start "ruleEInt"
    // InternalSimulatorDsl.g:1345:1: ruleEInt returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : ( (kw= '-' )? this_INT_1= RULE_INT ) ;
    public final AntlrDatatypeRuleToken ruleEInt() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;
        Token this_INT_1=null;


        	enterRule();

        try {
            // InternalSimulatorDsl.g:1351:2: ( ( (kw= '-' )? this_INT_1= RULE_INT ) )
            // InternalSimulatorDsl.g:1352:2: ( (kw= '-' )? this_INT_1= RULE_INT )
            {
            // InternalSimulatorDsl.g:1352:2: ( (kw= '-' )? this_INT_1= RULE_INT )
            // InternalSimulatorDsl.g:1353:3: (kw= '-' )? this_INT_1= RULE_INT
            {
            // InternalSimulatorDsl.g:1353:3: (kw= '-' )?
            int alt41=2;
            int LA41_0 = input.LA(1);

            if ( (LA41_0==46) ) {
                alt41=1;
            }
            switch (alt41) {
                case 1 :
                    // InternalSimulatorDsl.g:1354:4: kw= '-'
                    {
                    kw=(Token)match(input,46,FOLLOW_45); 

                    				current.merge(kw);
                    				newLeafNode(kw, grammarAccess.getEIntAccess().getHyphenMinusKeyword_0());
                    			

                    }
                    break;

            }

            this_INT_1=(Token)match(input,RULE_INT,FOLLOW_2); 

            			current.merge(this_INT_1);
            		

            			newLeafNode(this_INT_1, grammarAccess.getEIntAccess().getINTTerminalRuleCall_1());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEInt"


    // $ANTLR start "entryRuleInputTransform"
    // InternalSimulatorDsl.g:1371:1: entryRuleInputTransform returns [EObject current=null] : iv_ruleInputTransform= ruleInputTransform EOF ;
    public final EObject entryRuleInputTransform() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleInputTransform = null;


        try {
            // InternalSimulatorDsl.g:1371:55: (iv_ruleInputTransform= ruleInputTransform EOF )
            // InternalSimulatorDsl.g:1372:2: iv_ruleInputTransform= ruleInputTransform EOF
            {
             newCompositeNode(grammarAccess.getInputTransformRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleInputTransform=ruleInputTransform();

            state._fsp--;

             current =iv_ruleInputTransform; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleInputTransform"


    // $ANTLR start "ruleInputTransform"
    // InternalSimulatorDsl.g:1378:1: ruleInputTransform returns [EObject current=null] : ( () otherlv_1= 'InputTransform' otherlv_2= '{' (otherlv_3= 'destinationVariableName' ( (lv_destinationVariableName_4_0= ruleEString ) ) )? otherlv_5= '}' ) ;
    public final EObject ruleInputTransform() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        AntlrDatatypeRuleToken lv_destinationVariableName_4_0 = null;



        	enterRule();

        try {
            // InternalSimulatorDsl.g:1384:2: ( ( () otherlv_1= 'InputTransform' otherlv_2= '{' (otherlv_3= 'destinationVariableName' ( (lv_destinationVariableName_4_0= ruleEString ) ) )? otherlv_5= '}' ) )
            // InternalSimulatorDsl.g:1385:2: ( () otherlv_1= 'InputTransform' otherlv_2= '{' (otherlv_3= 'destinationVariableName' ( (lv_destinationVariableName_4_0= ruleEString ) ) )? otherlv_5= '}' )
            {
            // InternalSimulatorDsl.g:1385:2: ( () otherlv_1= 'InputTransform' otherlv_2= '{' (otherlv_3= 'destinationVariableName' ( (lv_destinationVariableName_4_0= ruleEString ) ) )? otherlv_5= '}' )
            // InternalSimulatorDsl.g:1386:3: () otherlv_1= 'InputTransform' otherlv_2= '{' (otherlv_3= 'destinationVariableName' ( (lv_destinationVariableName_4_0= ruleEString ) ) )? otherlv_5= '}'
            {
            // InternalSimulatorDsl.g:1386:3: ()
            // InternalSimulatorDsl.g:1387:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getInputTransformAccess().getInputTransformAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,49,FOLLOW_5); 

            			newLeafNode(otherlv_1, grammarAccess.getInputTransformAccess().getInputTransformKeyword_1());
            		
            otherlv_2=(Token)match(input,13,FOLLOW_47); 

            			newLeafNode(otherlv_2, grammarAccess.getInputTransformAccess().getLeftCurlyBracketKeyword_2());
            		
            // InternalSimulatorDsl.g:1401:3: (otherlv_3= 'destinationVariableName' ( (lv_destinationVariableName_4_0= ruleEString ) ) )?
            int alt42=2;
            int LA42_0 = input.LA(1);

            if ( (LA42_0==50) ) {
                alt42=1;
            }
            switch (alt42) {
                case 1 :
                    // InternalSimulatorDsl.g:1402:4: otherlv_3= 'destinationVariableName' ( (lv_destinationVariableName_4_0= ruleEString ) )
                    {
                    otherlv_3=(Token)match(input,50,FOLLOW_19); 

                    				newLeafNode(otherlv_3, grammarAccess.getInputTransformAccess().getDestinationVariableNameKeyword_3_0());
                    			
                    // InternalSimulatorDsl.g:1406:4: ( (lv_destinationVariableName_4_0= ruleEString ) )
                    // InternalSimulatorDsl.g:1407:5: (lv_destinationVariableName_4_0= ruleEString )
                    {
                    // InternalSimulatorDsl.g:1407:5: (lv_destinationVariableName_4_0= ruleEString )
                    // InternalSimulatorDsl.g:1408:6: lv_destinationVariableName_4_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getInputTransformAccess().getDestinationVariableNameEStringParserRuleCall_3_1_0());
                    					
                    pushFollow(FOLLOW_15);
                    lv_destinationVariableName_4_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getInputTransformRule());
                    						}
                    						set(
                    							current,
                    							"destinationVariableName",
                    							lv_destinationVariableName_4_0,
                    							"com.mncml.implementation.pogo.SimulatorDsl.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            otherlv_5=(Token)match(input,16,FOLLOW_2); 

            			newLeafNode(otherlv_5, grammarAccess.getInputTransformAccess().getRightCurlyBracketKeyword_4());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleInputTransform"


    // $ANTLR start "entryRuleSideEffects"
    // InternalSimulatorDsl.g:1434:1: entryRuleSideEffects returns [EObject current=null] : iv_ruleSideEffects= ruleSideEffects EOF ;
    public final EObject entryRuleSideEffects() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSideEffects = null;


        try {
            // InternalSimulatorDsl.g:1434:52: (iv_ruleSideEffects= ruleSideEffects EOF )
            // InternalSimulatorDsl.g:1435:2: iv_ruleSideEffects= ruleSideEffects EOF
            {
             newCompositeNode(grammarAccess.getSideEffectsRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleSideEffects=ruleSideEffects();

            state._fsp--;

             current =iv_ruleSideEffects; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSideEffects"


    // $ANTLR start "ruleSideEffects"
    // InternalSimulatorDsl.g:1441:1: ruleSideEffects returns [EObject current=null] : ( () otherlv_1= 'SideEffects' otherlv_2= '{' (otherlv_3= 'source_variable' ( (lv_source_variable_4_0= ruleEString ) ) )? (otherlv_5= 'destination_quantity' ( (lv_destination_quantity_6_0= ruleEString ) ) )? otherlv_7= '}' ) ;
    public final EObject ruleSideEffects() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        AntlrDatatypeRuleToken lv_source_variable_4_0 = null;

        AntlrDatatypeRuleToken lv_destination_quantity_6_0 = null;



        	enterRule();

        try {
            // InternalSimulatorDsl.g:1447:2: ( ( () otherlv_1= 'SideEffects' otherlv_2= '{' (otherlv_3= 'source_variable' ( (lv_source_variable_4_0= ruleEString ) ) )? (otherlv_5= 'destination_quantity' ( (lv_destination_quantity_6_0= ruleEString ) ) )? otherlv_7= '}' ) )
            // InternalSimulatorDsl.g:1448:2: ( () otherlv_1= 'SideEffects' otherlv_2= '{' (otherlv_3= 'source_variable' ( (lv_source_variable_4_0= ruleEString ) ) )? (otherlv_5= 'destination_quantity' ( (lv_destination_quantity_6_0= ruleEString ) ) )? otherlv_7= '}' )
            {
            // InternalSimulatorDsl.g:1448:2: ( () otherlv_1= 'SideEffects' otherlv_2= '{' (otherlv_3= 'source_variable' ( (lv_source_variable_4_0= ruleEString ) ) )? (otherlv_5= 'destination_quantity' ( (lv_destination_quantity_6_0= ruleEString ) ) )? otherlv_7= '}' )
            // InternalSimulatorDsl.g:1449:3: () otherlv_1= 'SideEffects' otherlv_2= '{' (otherlv_3= 'source_variable' ( (lv_source_variable_4_0= ruleEString ) ) )? (otherlv_5= 'destination_quantity' ( (lv_destination_quantity_6_0= ruleEString ) ) )? otherlv_7= '}'
            {
            // InternalSimulatorDsl.g:1449:3: ()
            // InternalSimulatorDsl.g:1450:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getSideEffectsAccess().getSideEffectsAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,51,FOLLOW_5); 

            			newLeafNode(otherlv_1, grammarAccess.getSideEffectsAccess().getSideEffectsKeyword_1());
            		
            otherlv_2=(Token)match(input,13,FOLLOW_48); 

            			newLeafNode(otherlv_2, grammarAccess.getSideEffectsAccess().getLeftCurlyBracketKeyword_2());
            		
            // InternalSimulatorDsl.g:1464:3: (otherlv_3= 'source_variable' ( (lv_source_variable_4_0= ruleEString ) ) )?
            int alt43=2;
            int LA43_0 = input.LA(1);

            if ( (LA43_0==52) ) {
                alt43=1;
            }
            switch (alt43) {
                case 1 :
                    // InternalSimulatorDsl.g:1465:4: otherlv_3= 'source_variable' ( (lv_source_variable_4_0= ruleEString ) )
                    {
                    otherlv_3=(Token)match(input,52,FOLLOW_19); 

                    				newLeafNode(otherlv_3, grammarAccess.getSideEffectsAccess().getSource_variableKeyword_3_0());
                    			
                    // InternalSimulatorDsl.g:1469:4: ( (lv_source_variable_4_0= ruleEString ) )
                    // InternalSimulatorDsl.g:1470:5: (lv_source_variable_4_0= ruleEString )
                    {
                    // InternalSimulatorDsl.g:1470:5: (lv_source_variable_4_0= ruleEString )
                    // InternalSimulatorDsl.g:1471:6: lv_source_variable_4_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getSideEffectsAccess().getSource_variableEStringParserRuleCall_3_1_0());
                    					
                    pushFollow(FOLLOW_49);
                    lv_source_variable_4_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getSideEffectsRule());
                    						}
                    						set(
                    							current,
                    							"source_variable",
                    							lv_source_variable_4_0,
                    							"com.mncml.implementation.pogo.SimulatorDsl.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalSimulatorDsl.g:1489:3: (otherlv_5= 'destination_quantity' ( (lv_destination_quantity_6_0= ruleEString ) ) )?
            int alt44=2;
            int LA44_0 = input.LA(1);

            if ( (LA44_0==53) ) {
                alt44=1;
            }
            switch (alt44) {
                case 1 :
                    // InternalSimulatorDsl.g:1490:4: otherlv_5= 'destination_quantity' ( (lv_destination_quantity_6_0= ruleEString ) )
                    {
                    otherlv_5=(Token)match(input,53,FOLLOW_19); 

                    				newLeafNode(otherlv_5, grammarAccess.getSideEffectsAccess().getDestination_quantityKeyword_4_0());
                    			
                    // InternalSimulatorDsl.g:1494:4: ( (lv_destination_quantity_6_0= ruleEString ) )
                    // InternalSimulatorDsl.g:1495:5: (lv_destination_quantity_6_0= ruleEString )
                    {
                    // InternalSimulatorDsl.g:1495:5: (lv_destination_quantity_6_0= ruleEString )
                    // InternalSimulatorDsl.g:1496:6: lv_destination_quantity_6_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getSideEffectsAccess().getDestination_quantityEStringParserRuleCall_4_1_0());
                    					
                    pushFollow(FOLLOW_15);
                    lv_destination_quantity_6_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getSideEffectsRule());
                    						}
                    						set(
                    							current,
                    							"destination_quantity",
                    							lv_destination_quantity_6_0,
                    							"com.mncml.implementation.pogo.SimulatorDsl.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            otherlv_7=(Token)match(input,16,FOLLOW_2); 

            			newLeafNode(otherlv_7, grammarAccess.getSideEffectsAccess().getRightCurlyBracketKeyword_5());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSideEffects"


    // $ANTLR start "entryRuleOutputReturn"
    // InternalSimulatorDsl.g:1522:1: entryRuleOutputReturn returns [EObject current=null] : iv_ruleOutputReturn= ruleOutputReturn EOF ;
    public final EObject entryRuleOutputReturn() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleOutputReturn = null;


        try {
            // InternalSimulatorDsl.g:1522:53: (iv_ruleOutputReturn= ruleOutputReturn EOF )
            // InternalSimulatorDsl.g:1523:2: iv_ruleOutputReturn= ruleOutputReturn EOF
            {
             newCompositeNode(grammarAccess.getOutputReturnRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleOutputReturn=ruleOutputReturn();

            state._fsp--;

             current =iv_ruleOutputReturn; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOutputReturn"


    // $ANTLR start "ruleOutputReturn"
    // InternalSimulatorDsl.g:1529:1: ruleOutputReturn returns [EObject current=null] : ( () otherlv_1= 'OutputReturn' otherlv_2= '{' (otherlv_3= 'source_variable' ( (lv_source_variable_4_0= ruleEString ) ) )? (otherlv_5= 'source_quantity' ( (lv_source_quantity_6_0= ruleEString ) ) )? otherlv_7= '}' ) ;
    public final EObject ruleOutputReturn() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        AntlrDatatypeRuleToken lv_source_variable_4_0 = null;

        AntlrDatatypeRuleToken lv_source_quantity_6_0 = null;



        	enterRule();

        try {
            // InternalSimulatorDsl.g:1535:2: ( ( () otherlv_1= 'OutputReturn' otherlv_2= '{' (otherlv_3= 'source_variable' ( (lv_source_variable_4_0= ruleEString ) ) )? (otherlv_5= 'source_quantity' ( (lv_source_quantity_6_0= ruleEString ) ) )? otherlv_7= '}' ) )
            // InternalSimulatorDsl.g:1536:2: ( () otherlv_1= 'OutputReturn' otherlv_2= '{' (otherlv_3= 'source_variable' ( (lv_source_variable_4_0= ruleEString ) ) )? (otherlv_5= 'source_quantity' ( (lv_source_quantity_6_0= ruleEString ) ) )? otherlv_7= '}' )
            {
            // InternalSimulatorDsl.g:1536:2: ( () otherlv_1= 'OutputReturn' otherlv_2= '{' (otherlv_3= 'source_variable' ( (lv_source_variable_4_0= ruleEString ) ) )? (otherlv_5= 'source_quantity' ( (lv_source_quantity_6_0= ruleEString ) ) )? otherlv_7= '}' )
            // InternalSimulatorDsl.g:1537:3: () otherlv_1= 'OutputReturn' otherlv_2= '{' (otherlv_3= 'source_variable' ( (lv_source_variable_4_0= ruleEString ) ) )? (otherlv_5= 'source_quantity' ( (lv_source_quantity_6_0= ruleEString ) ) )? otherlv_7= '}'
            {
            // InternalSimulatorDsl.g:1537:3: ()
            // InternalSimulatorDsl.g:1538:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getOutputReturnAccess().getOutputReturnAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,54,FOLLOW_5); 

            			newLeafNode(otherlv_1, grammarAccess.getOutputReturnAccess().getOutputReturnKeyword_1());
            		
            otherlv_2=(Token)match(input,13,FOLLOW_50); 

            			newLeafNode(otherlv_2, grammarAccess.getOutputReturnAccess().getLeftCurlyBracketKeyword_2());
            		
            // InternalSimulatorDsl.g:1552:3: (otherlv_3= 'source_variable' ( (lv_source_variable_4_0= ruleEString ) ) )?
            int alt45=2;
            int LA45_0 = input.LA(1);

            if ( (LA45_0==52) ) {
                alt45=1;
            }
            switch (alt45) {
                case 1 :
                    // InternalSimulatorDsl.g:1553:4: otherlv_3= 'source_variable' ( (lv_source_variable_4_0= ruleEString ) )
                    {
                    otherlv_3=(Token)match(input,52,FOLLOW_19); 

                    				newLeafNode(otherlv_3, grammarAccess.getOutputReturnAccess().getSource_variableKeyword_3_0());
                    			
                    // InternalSimulatorDsl.g:1557:4: ( (lv_source_variable_4_0= ruleEString ) )
                    // InternalSimulatorDsl.g:1558:5: (lv_source_variable_4_0= ruleEString )
                    {
                    // InternalSimulatorDsl.g:1558:5: (lv_source_variable_4_0= ruleEString )
                    // InternalSimulatorDsl.g:1559:6: lv_source_variable_4_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getOutputReturnAccess().getSource_variableEStringParserRuleCall_3_1_0());
                    					
                    pushFollow(FOLLOW_51);
                    lv_source_variable_4_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getOutputReturnRule());
                    						}
                    						set(
                    							current,
                    							"source_variable",
                    							lv_source_variable_4_0,
                    							"com.mncml.implementation.pogo.SimulatorDsl.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalSimulatorDsl.g:1577:3: (otherlv_5= 'source_quantity' ( (lv_source_quantity_6_0= ruleEString ) ) )?
            int alt46=2;
            int LA46_0 = input.LA(1);

            if ( (LA46_0==55) ) {
                alt46=1;
            }
            switch (alt46) {
                case 1 :
                    // InternalSimulatorDsl.g:1578:4: otherlv_5= 'source_quantity' ( (lv_source_quantity_6_0= ruleEString ) )
                    {
                    otherlv_5=(Token)match(input,55,FOLLOW_19); 

                    				newLeafNode(otherlv_5, grammarAccess.getOutputReturnAccess().getSource_quantityKeyword_4_0());
                    			
                    // InternalSimulatorDsl.g:1582:4: ( (lv_source_quantity_6_0= ruleEString ) )
                    // InternalSimulatorDsl.g:1583:5: (lv_source_quantity_6_0= ruleEString )
                    {
                    // InternalSimulatorDsl.g:1583:5: (lv_source_quantity_6_0= ruleEString )
                    // InternalSimulatorDsl.g:1584:6: lv_source_quantity_6_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getOutputReturnAccess().getSource_quantityEStringParserRuleCall_4_1_0());
                    					
                    pushFollow(FOLLOW_15);
                    lv_source_quantity_6_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getOutputReturnRule());
                    						}
                    						set(
                    							current,
                    							"source_quantity",
                    							lv_source_quantity_6_0,
                    							"com.mncml.implementation.pogo.SimulatorDsl.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            otherlv_7=(Token)match(input,16,FOLLOW_2); 

            			newLeafNode(otherlv_7, grammarAccess.getOutputReturnAccess().getRightCurlyBracketKeyword_5());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOutputReturn"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000002020L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000074000L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000004010000L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000018000L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000004000000L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000070000L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000002010000L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000050000L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000000090000L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000000000F10000L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x0000000000E10000L});
    public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x0000000000000030L});
    public static final BitSet FOLLOW_20 = new BitSet(new long[]{0x0000000000C10000L});
    public static final BitSet FOLLOW_21 = new BitSet(new long[]{0x0000000000810000L});
    public static final BitSet FOLLOW_22 = new BitSet(new long[]{0x0000000001000002L});
    public static final BitSet FOLLOW_23 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_24 = new BitSet(new long[]{0x004A000000010000L});
    public static final BitSet FOLLOW_25 = new BitSet(new long[]{0x0000000008000000L});
    public static final BitSet FOLLOW_26 = new BitSet(new long[]{0x0000042410010000L});
    public static final BitSet FOLLOW_27 = new BitSet(new long[]{0x00000003E0010000L});
    public static final BitSet FOLLOW_28 = new BitSet(new long[]{0x0000400001000040L});
    public static final BitSet FOLLOW_29 = new BitSet(new long[]{0x00000003C0010000L});
    public static final BitSet FOLLOW_30 = new BitSet(new long[]{0x0000000380010000L});
    public static final BitSet FOLLOW_31 = new BitSet(new long[]{0x0000000300010000L});
    public static final BitSet FOLLOW_32 = new BitSet(new long[]{0x0000000200010000L});
    public static final BitSet FOLLOW_33 = new BitSet(new long[]{0x0000001800010000L});
    public static final BitSet FOLLOW_34 = new BitSet(new long[]{0x0000001000010000L});
    public static final BitSet FOLLOW_35 = new BitSet(new long[]{0x000003C000010000L});
    public static final BitSet FOLLOW_36 = new BitSet(new long[]{0x0000038000010000L});
    public static final BitSet FOLLOW_37 = new BitSet(new long[]{0x0000030000010000L});
    public static final BitSet FOLLOW_38 = new BitSet(new long[]{0x0000400000000040L});
    public static final BitSet FOLLOW_39 = new BitSet(new long[]{0x0000020000010000L});
    public static final BitSet FOLLOW_40 = new BitSet(new long[]{0x0000380000010000L});
    public static final BitSet FOLLOW_41 = new BitSet(new long[]{0x0000300000010000L});
    public static final BitSet FOLLOW_42 = new BitSet(new long[]{0x0000200000010000L});
    public static final BitSet FOLLOW_43 = new BitSet(new long[]{0x0000000001000040L});
    public static final BitSet FOLLOW_44 = new BitSet(new long[]{0x0000000001000000L});
    public static final BitSet FOLLOW_45 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_46 = new BitSet(new long[]{0x0001800000000002L});
    public static final BitSet FOLLOW_47 = new BitSet(new long[]{0x0004000000010000L});
    public static final BitSet FOLLOW_48 = new BitSet(new long[]{0x0030000000010000L});
    public static final BitSet FOLLOW_49 = new BitSet(new long[]{0x0020000000010000L});
    public static final BitSet FOLLOW_50 = new BitSet(new long[]{0x0090000000010000L});
    public static final BitSet FOLLOW_51 = new BitSet(new long[]{0x0080000000010000L});

}