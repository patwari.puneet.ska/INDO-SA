/**
 */
package TestSuit;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Data Provider</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link TestSuit.DataProvider#getDataString <em>Data String</em>}</li>
 * </ul>
 * </p>
 *
 * @see TestSuit.TestSuitPackage#getDataProvider()
 * @model
 * @generated
 */
public interface DataProvider extends EObject {
	/**
	 * Returns the value of the '<em><b>Data String</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Data String</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data String</em>' attribute.
	 * @see #setDataString(String)
	 * @see TestSuit.TestSuitPackage#getDataProvider_DataString()
	 * @model
	 * @generated
	 */
	String getDataString();

	/**
	 * Sets the value of the '{@link TestSuit.DataProvider#getDataString <em>Data String</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Data String</em>' attribute.
	 * @see #getDataString()
	 * @generated
	 */
	void setDataString(String value);

} // DataProvider
