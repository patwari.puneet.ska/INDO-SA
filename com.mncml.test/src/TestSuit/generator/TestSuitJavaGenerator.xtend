package TestSuit.generator

import java.util.ArrayList
import java.util.HashMap
import java.util.HashSet
import java.util.Random
import mncModel.CommandResponseBlock
import mncModel.ControlNode
import mncModel.InterfaceDescription
import mncModel.Model
import mncModel.SimpleType
import org.eclipse.emf.common.util.EList
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.xtext.generator.IFileSystemAccess
import org.eclipse.xtext.generator.IGenerator
import org.json.simple.JSONArray
import org.json.simple.JSONObject
import org.json.simple.JSONValue

class TestSuitJavaGenerator implements IGenerator {
	override doGenerate(Resource input, IFileSystemAccess fsa) {
		var instance = input.contents.head as  Model
		var interface = instance.eContents.filter(InterfaceDescription).get(0)
		var controlNode = instance.eContents.filter(ControlNode).get(0)
		if (controlNode !== null) {
			var className = controlNode.name
			var commandResponseBlock = controlNode.commandResponseBlocks

			if (commandResponseBlock != null && commandResponseBlock.commandResponseBlocks != null) {
				fsa.generateFile(className + "Test.java",
					toJavaTestCode(className.toFirstUpper, commandResponseBlock.commandResponseBlocks))
			}
		} 

	}

	def toJavaTestCode(String testClassName, EList<CommandResponseBlock> commandResponseBlockList) {
		'''
package com.mnc.nodes.java.test;
		  
import org.testng.Assert;

import org.testng.annotations.Test;
import org.testng.annotations.DataProvider;
  
import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.DeviceData;
import fr.esrf.TangoApi.DeviceProxy;
 
public class «testClassName»Test
{ 
«««	«commandList.getDataProviders(commVal)»
«commandResponseBlockList.getTestCases(testClassName)»
}'''

	}

	def getTestCases(EList<CommandResponseBlock> commandResponseBlockList, String testClassName) {
		var string = ""
		if (commandResponseBlockList != null) {

			//	"RESPONSE RECEIVED FOR "+map.command.name.toUpperCase
			for (commandResponseBlock : commandResponseBlockList) {

				string = string + "\n" + ''' 
					@Test(dataProvider="«commandResponseBlock.command.name.toLowerCase»") 
							public void «commandResponseBlock.command.name.toUpperCase»(String params) throws DevFailed {
							DeviceProxy dp =new  DeviceProxy("nodes/«testClassName»/test");
			 			DeviceData dd = new fr.esrf.TangoApi.DeviceData();
						dd.insert(params);
						String resp = dp.command_inout("«commandResponseBlock.command.name.toUpperCase»",dd).extractString();
						System.out.println(resp);
						«var respValueList =  new HashMap<String, HashMap<String, Integer>>»
						«IF (commandResponseBlock.responseBlock != null)»
							«respValueList.putAll(commandResponseBlock.chooseResponse)»
							Assert.assertEquals(resp,"«commandResponseBlock.chooseResponse.createAssertString(commandResponseBlock.command.name)»");
						«ELSE»
							Assert.assertEquals(resp,"RESPONSE RECEIVED FOR «commandResponseBlock.command.name.toUpperCase»");
						«ENDIF» 
						}  
						
					@DataProvider(name="«commandResponseBlock.command.name.toLowerCase»")
						public Object[][] «commandResponseBlock.command.name.toLowerCase»DataProvider() {
						return new Object[][]{
						
								«getCommandDataObjects(commandResponseBlock, respValueList)»
						 
						
				 		};
					}  
				'''
			}

		}
		return string
	}

	def createAssertString(HashMap<String, HashMap<String, Integer>> map, String commandName) {
		if (map.size > 0) {
			var demoStr = ""
			var assertStr = map.keySet.get(0)
			var assertValueSet = map.get(assertStr)
			if (assertValueSet != null) {
				for (var g = 0; g < assertValueSet.size; g++) {
					var temp = assertValueSet.keySet.get(g)
					demoStr = demoStr + temp + ":" + assertValueSet.get(temp) + "||"
				}

			}
			return assertStr + ":-" + demoStr
		} else {
			return "RESPONSE RECEIVED FOR " + commandName.toUpperCase
		}
	}

	def chooseResponse(CommandResponseBlock commandRespBlock) {
		var responseValueList = new HashMap<String, HashMap<String, Integer>>
		if (commandRespBlock.command != null && commandRespBlock.responseBlock != null) {
			var intHashMap = new HashMap<String, Integer>
			for (responseBlock : commandRespBlock.responseBlock.responseBlocks) {
				if (responseBlock.response != null) {
					responseValueList.put(responseBlock.response.name, null)

					var responseValRule = responseBlock.responseValidation.validationRules
					for (chkParaCond : responseValRule) {
						var parameter = chkParaCond.parameter as SimpleType
						var parameterName = parameter.name
						var parameterValue = Integer.parseInt(ParameterValueFinder.findValueType(parameter.value))

						var minValue = ParameterValueFinder.findValueType(chkParaCond.checkMinValue)
						var intMinValue = null as Integer
						if (minValue != null) {
							intMinValue = Integer.parseInt(minValue.split("\\.").get(0))
						}

						var maxValue = ParameterValueFinder.findValueType(chkParaCond.checkMaxValue)
						var intMaxValue = null as Integer
						if (maxValue != null) {
							intMaxValue = Integer.parseInt(maxValue.split("\\.").get(0))
						}

						var allowed = ParameterValueFinder.convertIntoArrayList(chkParaCond.checkValues)
						var paraVarValue = chooseAssertResponseParameterValue(parameterValue, intMinValue, intMaxValue,
							allowed)
						intHashMap.put(parameterName, paraVarValue)
						responseValueList.put(responseBlock.response.name, intHashMap)
					}
				}
			}
		}
		return responseValueList
	}

	def chooseAssertResponseParameterValue(Integer parameterValue, Integer minValue, Integer maxValue,
		ArrayList<Integer> allowedValues) {
		var intSet = new HashSet<Integer>();
		intSet.add(parameterValue)
		if (minValue != null) {
			intSet.add(minValue)
		}
		if (maxValue != null) {
			intSet.add(maxValue)
		}
		if (allowedValues != null) {
			intSet.addAll(allowedValues)
		}
		var choosenValue = 0 // shared than this

		var rand = new Random(System.currentTimeMillis());
		var setArray = intSet.toArray();
		choosenValue = setArray.get(rand.nextInt(intSet.size)) as Integer
		return choosenValue
	}
 
	def getCommandDataObjects(CommandResponseBlock commandRespBlock,
		HashMap<String, HashMap<String, Integer>> respValueConditions) {
		if (commandRespBlock.command != null) {
			var majorString = ""
			for (var m = 0; m < 5; m++) {
				var commandJSONObject = new JSONObject
				var commandJSONArray = new JSONArray
				if (commandRespBlock.commandValidation != null) {
					var chkCondList = commandRespBlock.commandValidation.validationRules
					if (chkCondList != null) {
						for (chkCond : chkCondList) {
							var para = chkCond.parameter as SimpleType
							val commandParameterObject = new JSONObject
							commandParameterObject.put("parName", para.name)
							commandParameterObject.put("parType", para.type.getName());
							commandParameterObject.put("parValue", new NumberGenerator().generateParameter(chkCond));
							commandJSONArray.add(commandParameterObject)
						}

					}
 
				}
				commandJSONObject.put(commandRespBlock.command.name.toUpperCase, commandJSONArray)
				if (new Random().nextBoolean) {
					var responseParameterFix = new JSONObject
					var responseName = "RESPONSE RECEIVED FOR " + commandRespBlock.command.name.toUpperCase.toUpperCase
					if (respValueConditions !=null && respValueConditions.size > 0) {
						responseName = respValueConditions.keySet.get(0)
						responseParameterFix.put("Response", responseName)
						if (respValueConditions.get(responseName) != null) {
							var parValuePair = respValueConditions.get(responseName)
							for (var n = 0; n < parValuePair.size; n++) {
								responseParameterFix.put(parValuePair.keySet.get(n),
									parValuePair.get(parValuePair.keySet.get(n)))
							}
						}
					}
					responseParameterFix.put("Response", responseName)
					commandJSONObject.put("fixedResponse", responseParameterFix)

				}
				majorString = majorString + "\n" + '''
					new Object[] {"«JSONValue.escape(commandJSONObject.toJSONString())»"},
				'''
			}
			return majorString
		} else {
			return ""
		}
	}
}
