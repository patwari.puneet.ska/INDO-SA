package TestSuit.generator;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Random;

import mncModel.CheckParameterCondition;
import mncModel.PrimitiveValue;
import mncModel.impl.FloatValueImpl;
import mncModel.impl.IntValueImpl;
import mncModel.impl.SimpleTypeImpl;

import org.eclipse.emf.common.util.EList;
 
public class NumberGenerator {

	public Object generateParameter(CheckParameterCondition checkParDataObj) {
		if (checkParDataObj != null) {
			PrimitiveValue minValue = checkParDataObj.getCheckMinValue();
			PrimitiveValue maxValue = checkParDataObj.getCheckMaxValue();
			EList<PrimitiveValue> allowedValues = checkParDataObj
					.getCheckValues();
			SimpleTypeImpl para = (SimpleTypeImpl) checkParDataObj
					.getParameter();
			PrimitiveValue value = para.getValue();
			Object returnValue = null;
			returnValue = segreGateValuesFromPrimitive(minValue, maxValue,
					allowedValues, value);
			return returnValue;
		} else {
			return "2";
		}
	}

	private Object segreGateValuesFromPrimitive(PrimitiveValue minValue,
			PrimitiveValue maxValue, EList<PrimitiveValue> allowedValues,
			PrimitiveValue value) {

		Integer minI = null, maxI = null, valI = null;
		Float minF = null, maxF = null, valF = null;
		ArrayList<Integer> allowedI = new ArrayList<Integer>();
		ArrayList<Float> allowedF = new ArrayList<Float>();
		if (minValue instanceof IntValueImpl) {
			minI = ((IntValueImpl) minValue).getIntValue();
		}
		if (minValue instanceof FloatValueImpl) {
			minF = ((FloatValueImpl) minValue).getFloatValue();
		}
		if (maxValue instanceof IntValueImpl) {
			maxI = ((IntValueImpl) maxValue).getIntValue();
		} 
		if (maxValue instanceof FloatValueImpl) {
			minF = ((FloatValueImpl) maxValue).getFloatValue();
		}
		if (value instanceof IntValueImpl) {
			valI = ((IntValueImpl) value).getIntValue();
		}
		if (value instanceof FloatValueImpl) {
			valF = ((FloatValueImpl) value).getFloatValue();
		}
		if (allowedValues instanceof EList<?>) {
			for (PrimitiveValue primVal : allowedValues) {
				if (primVal instanceof IntValueImpl) {
					allowedI.add(((IntValueImpl) primVal).getIntValue());
				} else if (primVal instanceof FloatValueImpl) {
		 			allowedF.add(((FloatValueImpl) primVal).getFloatValue());
				}
			}
		}

		return generateValuesBasedOnConstraints(minI, minF, maxI, maxF, valI,
				valF, allowedI, allowedF);
		// TODO Auto-generated method stub

	}

	private Object generateValuesBasedOnConstraints(Integer minI, Float minF,
			Integer maxI, Float maxF, Integer valI, Float valF,
			ArrayList<Integer> allowedI, ArrayList<Float> allowedF) {
		// TODO Auto-generated method stub
		Random randomGenerator = new Random();
		if (minI != null && maxI != null) {
			if (allowedI != null && allowedI.size()>0) {
				   HashSet<Integer> hashSet= new HashSet<Integer>();
				   hashSet.add(minI);
				   hashSet.add(maxI);
				   hashSet.addAll(allowedI);
				int index = randomGenerator.nextInt(hashSet.size());
				return hashSet.toArray()[index];
			} else {
				int randomNum = randomGenerator.nextInt((maxI - minI) + 1)
						+ minI;
				return randomNum;
			} 
		}
 
		else if (minF != null && maxF != null) {
			if (allowedF != null && allowedF.size()>0) {
				   HashSet<Float> hashSet= new HashSet<Float>();
				   hashSet.add(minF);
				   hashSet.add(maxF);
				   hashSet.addAll(allowedF);
				int index = randomGenerator.nextInt(hashSet.size());
				return hashSet.toArray()[index];
		 	} else {
				float randomNum = minF + randomGenerator.nextFloat()
						* (maxF - minF);
				return randomNum;
			}
		} else if (valI != null) {
			return valI;
		} else if (valF != null) {
			return valF;
		} else {
			int randomNum = randomGenerator.nextInt((200 - -200) + 1) + -200;
			return randomNum;
		}

	}

}
