package TestSuit.generator

import Emulator.generator.EmulatorJavaGenerator
import java.io.BufferedInputStream
import java.io.File
import java.io.FileInputStream
import java.net.URI
import java.util.List
import java.util.Set
import mncModel.Model
import mncModel.impl.ControlNodeImpl
import org.eclipse.core.commands.AbstractHandler
import org.eclipse.core.commands.ExecutionEvent
import org.eclipse.core.commands.ExecutionException
import org.eclipse.core.resources.IFile
import org.eclipse.core.resources.IResource
import org.eclipse.core.runtime.CoreException
import org.eclipse.core.runtime.FileLocator
import org.eclipse.core.runtime.NullProgressMonitor
import org.eclipse.core.runtime.Platform
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.jdt.core.IClasspathEntry
import org.eclipse.jdt.core.IPackageFragment
import org.eclipse.jdt.core.JavaCore
import org.eclipse.jface.dialogs.MessageDialog
import org.eclipse.ui.handlers.HandlerUtil
import org.eclipse.xtext.builder.EclipseResourceFileSystemAccess
import org.eclipse.xtext.resource.XtextResource
import org.eclipse.xtext.ui.editor.XtextEditor
import org.eclipse.xtext.util.concurrent.IUnitOfWork

class TestSuitGenerator extends AbstractHandler{
	
	override execute(ExecutionEvent event) throws ExecutionException {
		var tangoTestFolder = null as IPackageFragment
		var is = null as BufferedInputStream
		val generate = MessageDialog.openConfirm(HandlerUtil.getActiveShell(event), "Generate Test Code",
			"Do you want to generate JAVA Test code?");
		if (!generate)
			return null;

		val activeEditor = HandlerUtil.getActiveEditor(event);
		val file = activeEditor.getEditorInput().getAdapter(IFile) as IFile;
		if (file !== null) {
			val project = file.getProject();
				var javaProject = JavaCore.create(project)
				val srcGenFolder = project.getFolder("src-gen");

				try {
					if (!srcGenFolder.exists())
						srcGenFolder.create(true, true, new NullProgressMonitor)

					var srcRoot = javaProject.getPackageFragmentRoot(srcGenFolder);
					var oldEntries = javaProject.getRawClasspath();
					var List<IClasspathEntry> newEntries = newArrayList(oldEntries)

					// Check if the newENtry is already present in oldEntries of sources
					var newEntry = JavaCore.newSourceEntry(srcRoot.getPath)
					newEntries.add(newEntry);
					for (ce : oldEntries)
						if (ce.equals(newEntry))
							newEntries.remove(newEntry)

					tangoTestFolder = srcRoot.createPackageFragment("com.mnc.nodes.java.test", true, null)

					// Putting libraries in classpath of runtime java project
					var jarFolder = project.getFolder("jars")
					if (!jarFolder.exists())
						jarFolder.create(true, true, null)

					var fileURL = Platform.getBundle("com.mncml.dsl.libs").getEntry("libs")
					var resolvedFileLibsURL = FileLocator.toFileURL(fileURL)
					var fileLibs = new File(new URI(resolvedFileLibsURL.protocol, resolvedFileLibsURL.path, null))
					var jars = fileLibs.listFiles.toList
					for (f : jars) {
						if (f.name.contains("JTango") || f.name.contains("SKA_Dependencies") ||
							f.name.contains("json") || f.name.contains("testng")) {
							is = new BufferedInputStream(new FileInputStream(f))
							var jarFile = project.getFile("jars/" + f.name)
							if (!jarFile.exists)
								jarFile.create(is, true, null)
							var jarPath = jarFile.fullPath
							newEntries.add(JavaCore.newLibraryEntry(jarPath, null, null))
							is.close
						}
					}

//						var libProject = ResourcesPlugin.getWorkspace().getRoot().getProject(System.getenv("MNCML")+"/com.mncml.dsl.libs").getNature(JavaCore.NATURE_ID) as IJavaProject
//						var List<IClasspathEntry> libProjectEntries = libProject.rawClasspath
//						newEntries.addAll(libProjectEntries)
					var Set<IClasspathEntry> setOfEntries = newHashSet(newEntries)
//
//					    is.close
					javaProject.setRawClasspath(setOfEntries, new NullProgressMonitor)
				} catch (CoreException e) {
					e.printStackTrace
					return null;
				} finally {
//						is.close
				}
 
			 

			//  final EclipseResourceFileSystemAccess fsa = new EclipseResourceFileSystemAccess();
			 	val fsa = new EclipseResourceFileSystemAccess
				fsa.setOutputPath(tangoTestFolder.path.toString());
				var teste = fsa.getOutputConfigurations();
				fsa.root = project.workspace.root
				var it = teste.entrySet().iterator(); 

				// make a new Outputconfiguration <- needed
				while (it.hasNext()) {

					var next = it.next();
					var out = next.getValue();
					out.isOverrideExistingResources();
					out.setCreateOutputDirectory(true); // <--- do not touch this
				}

 
			if (activeEditor instanceof XtextEditor) {
				(activeEditor as XtextEditor).getDocument().readOnly(
					new IUnitOfWork<Boolean, XtextResource>() {

						override exec(XtextResource state) throws Exception {
							val res = state as Resource;
							val instance = res.contents.head as  Model
 
							//	val interface = instance.eContents.filter(InterfaceDescriptionImpl).get(0)
							val controlNode = instance.eContents.filter(ControlNodeImpl).get(0)

							//val controlNode = res.contents.filter(ControlNodeImpl).get(0)
							project.refreshLocal(IResource.DEPTH_INFINITE, null);
							val srcGenFolder1 = project.getFolder("src-gen/com/mnc/nodes/java");
							if (srcGenFolder1.getFile(controlNode.name + ".java").exists) {
								new TestSuitJavaGenerator().doGenerate(state, fsa);
								return Boolean.TRUE;
							} else {
								val generate = MessageDialog.openConfirm(HandlerUtil.getActiveShell(event),
									"Generate Test Code",
									"The Java Code for This Node is Not Generated  Do you want to generate the Java Code First ?");
								if (!generate) {
									return Boolean.FALSE
								} else {

									fsa.setOutputPath(srcGenFolder1.getFullPath().toString());
									if (!srcGenFolder.exists()) {
										try {
											srcGenFolder.create(true, true, new NullProgressMonitor());
										} catch (CoreException e) {
											return null;
										}
					 				}
									new EmulatorJavaGenerator().doGenerate(state, fsa);
									new TestSuitJavaGenerator().doGenerate(state, fsa);
									return Boolean.TRUE
								}

							}
						}
					});

			}
		}
		return null;
	}
	
}