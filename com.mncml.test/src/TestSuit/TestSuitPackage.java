/**
 */
package TestSuit;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see TestSuit.TestSuitFactory
 * @model kind="package"
 * @generated
 */
public interface TestSuitPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "TestSuit";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://testsuit/1.0";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "TestSuit";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	TestSuitPackage eINSTANCE = TestSuit.impl.TestSuitPackageImpl.init();

	/**
	 * The meta object id for the '{@link TestSuit.impl.TestCasesImpl <em>Test Cases</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see TestSuit.impl.TestCasesImpl
	 * @see TestSuit.impl.TestSuitPackageImpl#getTestCases()
	 * @generated
	 */
	int TEST_CASES = 0;

	/**
	 * The feature id for the '<em><b>Data Providers</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEST_CASES__DATA_PROVIDERS = 0;

	/**
	 * The feature id for the '<em><b>Assert Statement</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEST_CASES__ASSERT_STATEMENT = 1;

	/**
	 * The feature id for the '<em><b>Test Command</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEST_CASES__TEST_COMMAND = 2;

	/**
	 * The number of structural features of the '<em>Test Cases</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEST_CASES_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Test Cases</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEST_CASES_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link TestSuit.impl.DataProviderImpl <em>Data Provider</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see TestSuit.impl.DataProviderImpl
	 * @see TestSuit.impl.TestSuitPackageImpl#getDataProvider()
	 * @generated
	 */
	int DATA_PROVIDER = 1;

	/**
	 * The feature id for the '<em><b>Data String</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_PROVIDER__DATA_STRING = 0;

	/**
	 * The number of structural features of the '<em>Data Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_PROVIDER_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Data Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_PROVIDER_OPERATION_COUNT = 0;


	/**
	 * Returns the meta object for class '{@link TestSuit.TestCases <em>Test Cases</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Test Cases</em>'.
	 * @see TestSuit.TestCases
	 * @generated
	 */
	EClass getTestCases();

	/**
	 * Returns the meta object for the reference list '{@link TestSuit.TestCases#getDataProviders <em>Data Providers</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Data Providers</em>'.
	 * @see TestSuit.TestCases#getDataProviders()
	 * @see #getTestCases()
	 * @generated
	 */
	EReference getTestCases_DataProviders();

	/**
	 * Returns the meta object for the attribute '{@link TestSuit.TestCases#getAssertStatement <em>Assert Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Assert Statement</em>'.
	 * @see TestSuit.TestCases#getAssertStatement()
	 * @see #getTestCases()
	 * @generated
	 */
	EAttribute getTestCases_AssertStatement();

	/**
	 * Returns the meta object for the reference '{@link TestSuit.TestCases#getTestCommand <em>Test Command</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Test Command</em>'.
	 * @see TestSuit.TestCases#getTestCommand()
	 * @see #getTestCases()
	 * @generated
	 */
	EReference getTestCases_TestCommand();

	/**
	 * Returns the meta object for class '{@link TestSuit.DataProvider <em>Data Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Data Provider</em>'.
	 * @see TestSuit.DataProvider
	 * @generated
	 */
	EClass getDataProvider();

	/**
	 * Returns the meta object for the attribute '{@link TestSuit.DataProvider#getDataString <em>Data String</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Data String</em>'.
	 * @see TestSuit.DataProvider#getDataString()
	 * @see #getDataProvider()
	 * @generated
	 */
	EAttribute getDataProvider_DataString();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	TestSuitFactory getTestSuitFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link TestSuit.impl.TestCasesImpl <em>Test Cases</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see TestSuit.impl.TestCasesImpl
		 * @see TestSuit.impl.TestSuitPackageImpl#getTestCases()
		 * @generated
		 */
		EClass TEST_CASES = eINSTANCE.getTestCases();

		/**
		 * The meta object literal for the '<em><b>Data Providers</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TEST_CASES__DATA_PROVIDERS = eINSTANCE.getTestCases_DataProviders();

		/**
		 * The meta object literal for the '<em><b>Assert Statement</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TEST_CASES__ASSERT_STATEMENT = eINSTANCE.getTestCases_AssertStatement();

		/**
		 * The meta object literal for the '<em><b>Test Command</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TEST_CASES__TEST_COMMAND = eINSTANCE.getTestCases_TestCommand();

		/**
		 * The meta object literal for the '{@link TestSuit.impl.DataProviderImpl <em>Data Provider</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see TestSuit.impl.DataProviderImpl
		 * @see TestSuit.impl.TestSuitPackageImpl#getDataProvider()
		 * @generated
		 */
		EClass DATA_PROVIDER = eINSTANCE.getDataProvider();

		/**
		 * The meta object literal for the '<em><b>Data String</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DATA_PROVIDER__DATA_STRING = eINSTANCE.getDataProvider_DataString();

	}

} //TestSuitPackage
