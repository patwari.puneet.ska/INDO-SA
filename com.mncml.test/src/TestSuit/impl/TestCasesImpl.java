/**
 */
package TestSuit.impl;

import TestSuit.DataProvider;
import TestSuit.TestCases;
import TestSuit.TestSuitPackage;

import java.util.Collection;

import mncModel.Command;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Test Cases</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link TestSuit.impl.TestCasesImpl#getDataProviders <em>Data Providers</em>}</li>
 *   <li>{@link TestSuit.impl.TestCasesImpl#getAssertStatement <em>Assert Statement</em>}</li>
 *   <li>{@link TestSuit.impl.TestCasesImpl#getTestCommand <em>Test Command</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class TestCasesImpl extends MinimalEObjectImpl.Container implements TestCases {
	/**
	 * The cached value of the '{@link #getDataProviders() <em>Data Providers</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDataProviders()
	 * @generated
	 * @ordered
	 */
	protected EList<DataProvider> dataProviders;

	/**
	 * The default value of the '{@link #getAssertStatement() <em>Assert Statement</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssertStatement()
	 * @generated
	 * @ordered
	 */
	protected static final String ASSERT_STATEMENT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getAssertStatement() <em>Assert Statement</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssertStatement()
	 * @generated
	 * @ordered
	 */
	protected String assertStatement = ASSERT_STATEMENT_EDEFAULT;

	/**
	 * The cached value of the '{@link #getTestCommand() <em>Test Command</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTestCommand()
	 * @generated
	 * @ordered
	 */
	protected Command testCommand;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TestCasesImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TestSuitPackage.Literals.TEST_CASES;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<DataProvider> getDataProviders() {
		if (dataProviders == null) {
			dataProviders = new EObjectResolvingEList<DataProvider>(DataProvider.class, this, TestSuitPackage.TEST_CASES__DATA_PROVIDERS);
		}
		return dataProviders;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getAssertStatement() {
		return assertStatement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAssertStatement(String newAssertStatement) {
		String oldAssertStatement = assertStatement;
		assertStatement = newAssertStatement;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TestSuitPackage.TEST_CASES__ASSERT_STATEMENT, oldAssertStatement, assertStatement));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Command getTestCommand() {
		if (testCommand != null && testCommand.eIsProxy()) {
			InternalEObject oldTestCommand = (InternalEObject)testCommand;
			testCommand = (Command)eResolveProxy(oldTestCommand);
			if (testCommand != oldTestCommand) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TestSuitPackage.TEST_CASES__TEST_COMMAND, oldTestCommand, testCommand));
			}
		}
		return testCommand;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Command basicGetTestCommand() {
		return testCommand;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTestCommand(Command newTestCommand) {
		Command oldTestCommand = testCommand;
		testCommand = newTestCommand;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TestSuitPackage.TEST_CASES__TEST_COMMAND, oldTestCommand, testCommand));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TestSuitPackage.TEST_CASES__DATA_PROVIDERS:
				return getDataProviders();
			case TestSuitPackage.TEST_CASES__ASSERT_STATEMENT:
				return getAssertStatement();
			case TestSuitPackage.TEST_CASES__TEST_COMMAND:
				if (resolve) return getTestCommand();
				return basicGetTestCommand();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TestSuitPackage.TEST_CASES__DATA_PROVIDERS:
				getDataProviders().clear();
				getDataProviders().addAll((Collection<? extends DataProvider>)newValue);
				return;
			case TestSuitPackage.TEST_CASES__ASSERT_STATEMENT:
				setAssertStatement((String)newValue);
				return;
			case TestSuitPackage.TEST_CASES__TEST_COMMAND:
				setTestCommand((Command)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TestSuitPackage.TEST_CASES__DATA_PROVIDERS:
				getDataProviders().clear();
				return;
			case TestSuitPackage.TEST_CASES__ASSERT_STATEMENT:
				setAssertStatement(ASSERT_STATEMENT_EDEFAULT);
				return;
			case TestSuitPackage.TEST_CASES__TEST_COMMAND:
				setTestCommand((Command)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TestSuitPackage.TEST_CASES__DATA_PROVIDERS:
				return dataProviders != null && !dataProviders.isEmpty();
			case TestSuitPackage.TEST_CASES__ASSERT_STATEMENT:
				return ASSERT_STATEMENT_EDEFAULT == null ? assertStatement != null : !ASSERT_STATEMENT_EDEFAULT.equals(assertStatement);
			case TestSuitPackage.TEST_CASES__TEST_COMMAND:
				return testCommand != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (AssertStatement: ");
		result.append(assertStatement);
		result.append(')');
		return result.toString();
	}

} //TestCasesImpl
