/**
 */
package TestSuit.impl;

import TestSuit.DataProvider;
import TestSuit.TestSuitPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Data Provider</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link TestSuit.impl.DataProviderImpl#getDataString <em>Data String</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class DataProviderImpl extends MinimalEObjectImpl.Container implements DataProvider {
	/**
	 * The default value of the '{@link #getDataString() <em>Data String</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDataString()
	 * @generated
	 * @ordered
	 */
	protected static final String DATA_STRING_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDataString() <em>Data String</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDataString()
	 * @generated
	 * @ordered
	 */
	protected String dataString = DATA_STRING_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DataProviderImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TestSuitPackage.Literals.DATA_PROVIDER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDataString() {
		return dataString;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDataString(String newDataString) {
		String oldDataString = dataString;
		dataString = newDataString;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TestSuitPackage.DATA_PROVIDER__DATA_STRING, oldDataString, dataString));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TestSuitPackage.DATA_PROVIDER__DATA_STRING:
				return getDataString();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TestSuitPackage.DATA_PROVIDER__DATA_STRING:
				setDataString((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TestSuitPackage.DATA_PROVIDER__DATA_STRING:
				setDataString(DATA_STRING_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TestSuitPackage.DATA_PROVIDER__DATA_STRING:
				return DATA_STRING_EDEFAULT == null ? dataString != null : !DATA_STRING_EDEFAULT.equals(dataString);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (DataString: ");
		result.append(dataString);
		result.append(')');
		return result.toString();
	}

} //DataProviderImpl
