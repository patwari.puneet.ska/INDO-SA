/**
 */
package TestSuit;

import mncModel.Command;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Test Cases</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link TestSuit.TestCases#getDataProviders <em>Data Providers</em>}</li>
 *   <li>{@link TestSuit.TestCases#getAssertStatement <em>Assert Statement</em>}</li>
 *   <li>{@link TestSuit.TestCases#getTestCommand <em>Test Command</em>}</li>
 * </ul>
 * </p>
 *
 * @see TestSuit.TestSuitPackage#getTestCases()
 * @model
 * @generated
 */
public interface TestCases extends EObject {
	/**
	 * Returns the value of the '<em><b>Data Providers</b></em>' reference list.
	 * The list contents are of type {@link TestSuit.DataProvider}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Data Providers</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data Providers</em>' reference list.
	 * @see TestSuit.TestSuitPackage#getTestCases_DataProviders()
	 * @model
	 * @generated
	 */
	EList<DataProvider> getDataProviders();

	/**
	 * Returns the value of the '<em><b>Assert Statement</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Assert Statement</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Assert Statement</em>' attribute.
	 * @see #setAssertStatement(String)
	 * @see TestSuit.TestSuitPackage#getTestCases_AssertStatement()
	 * @model
	 * @generated
	 */
	String getAssertStatement();

	/**
	 * Sets the value of the '{@link TestSuit.TestCases#getAssertStatement <em>Assert Statement</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Assert Statement</em>' attribute.
	 * @see #getAssertStatement()
	 * @generated
	 */
	void setAssertStatement(String value);

	/**
	 * Returns the value of the '<em><b>Test Command</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Test Command</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Test Command</em>' reference.
	 * @see #setTestCommand(Command)
	 * @see TestSuit.TestSuitPackage#getTestCases_TestCommand()
	 * @model
	 * @generated
	 */
	Command getTestCommand();

	/**
	 * Sets the value of the '{@link TestSuit.TestCases#getTestCommand <em>Test Command</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Test Command</em>' reference.
	 * @see #getTestCommand()
	 * @generated
	 */
	void setTestCommand(Command value);

} // TestCases
