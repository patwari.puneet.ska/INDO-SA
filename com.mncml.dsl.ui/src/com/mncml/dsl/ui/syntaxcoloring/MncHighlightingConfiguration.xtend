package com.mncml.dsl.ui.syntaxcoloring

import org.eclipse.xtext.ui.editor.syntaxcoloring.IHighlightingConfiguration
import org.eclipse.xtext.ui.editor.syntaxcoloring.IHighlightingConfigurationAcceptor
import org.eclipse.xtext.ui.editor.utils.TextStyle
import org.eclipse.swt.graphics.RGB
import org.eclipse.swt.SWT
import org.eclipse.xtext.ui.editor.syntaxcoloring.DefaultHighlightingConfiguration

class MncHighlightingConfiguration extends DefaultHighlightingConfiguration implements IHighlightingConfiguration {
	
	 public final static String CRB_COMMAND = "CRB_command"
	override configure(IHighlightingConfigurationAcceptor acceptor) {
		println("MncHC")
		super.configure(acceptor)
		acceptor.acceptDefaultHighlighting(CRB_COMMAND, "CRB_command", CRB_CommandTextStyle())
	}
	
	def CRB_CommandTextStyle() {
		var textStyle = new TextStyle
		textStyle.color = new RGB(255,00,00)
		textStyle.style = SWT.ITALIC
		textStyle
	}
	
}