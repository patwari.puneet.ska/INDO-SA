package com.mncml.dsl.ui.syntaxcoloring

import org.eclipse.xtext.ui.editor.syntaxcoloring.ISemanticHighlightingCalculator
import org.eclipse.xtext.resource.XtextResource
import org.eclipse.xtext.ui.editor.syntaxcoloring.IHighlightedPositionAcceptor
import java.util.List
import mncModel.CommandResponseBlock
import org.eclipse.xtext.nodemodel.util.NodeModelUtils
import mncModel.MncModelPackage
import org.eclipse.xtext.nodemodel.INode
import mncModel.utility.UtilityPackage
import org.eclipse.xtext.ui.editor.syntaxcoloring.DefaultSemanticHighlightingCalculator
import org.eclipse.xtext.CrossReference
import mncModel.Command

class MncSemanticHighlightingCalculator extends DefaultSemanticHighlightingCalculator implements ISemanticHighlightingCalculator {
	
	override provideHighlightingFor(XtextResource resource, IHighlightedPositionAcceptor acceptor) {	
		if (resource == null || resource.getParseResult() == null)
    		return;
    	var List<CommandResponseBlock> crb = resource.allContents.filter(CommandResponseBlock).toList
//		var List<INode> crb = NodeModelUtils.findNodesForFeature(MncModelPackage.eINSTANCE.controlNode, UtilityPackage.Literals.COMMAND_RESPONSE_BLOCK_UTILITY__COMMAND_RESPONSE_BLOCKS)
		for(c : crb){
			var node = NodeModelUtils.getNode(c)
			var count = 0
			for(n : node.asTreeIterable)
			{
				if(n.grammarElement instanceof CrossReference && n.semanticElement instanceof CommandResponseBlock) //n.semanticElemnt is a better way (15/12/15)
				{ 
					highlightNode(acceptor, n, MncHighlightingConfiguration.CRB_COMMAND )	
//					count++
//					println(NodeModelUtils.findActualSemanticObjectFor(n))
//					if(count==1) // The first cross reference is for the Command; This is a bad way but the best I can come up with now(14/12/15) 
//					{
//						println(NodeModelUtils.findActualSemanticObjectFor(n) + "  " +n.hasDirectSemanticElement)
//						highlightNode(acceptor, n, MncHighlightingConfiguration.CRB_COMMAND )	
//						
//					}
				}
			
			}
			
		}
	}
	
}