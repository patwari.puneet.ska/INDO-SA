package com.mncml.dsl.ui.hyperlink

import org.eclipse.xtext.ui.editor.hyperlinking.HyperlinkHelper
import org.eclipse.xtext.ui.editor.hyperlinking.XtextHyperlink
import com.google.inject.Provider
import com.google.inject.Inject
import org.eclipse.xtext.resource.EObjectAtOffsetHelper
import org.eclipse.xtext.resource.XtextResource
import org.eclipse.xtext.ui.editor.hyperlinking.IHyperlinkAcceptor
import mncModel.Operation
import org.eclipse.xtext.nodemodel.util.NodeModelUtils
import mncModel.MncModelPackage
import org.eclipse.jface.text.Region
import org.eclipse.emf.common.util.URI

class MncHyperlinkHelper extends HyperlinkHelper {
	 @Inject
     private Provider<XtextHyperlink> hyperlinkProvider;
        
     @Inject 
     private EObjectAtOffsetHelper eObjectAtOffsetHelper;
        
	override createHyperlinksByOffset(XtextResource resource, int offset, IHyperlinkAcceptor acceptor) 
	{
		super.createHyperlinksByOffset(resource, offset, acceptor)
	    var eObject = eObjectAtOffsetHelper.resolveElementAt(resource,offset);
	    if(eObject instanceof Operation)
	    {
	    	var op = eObject as Operation
	    	var node = NodeModelUtils.findNodesForFeature(op, MncModelPackage.Literals.OPERATION__SCRIPT).get(0)
	    	var hl = hyperlinkProvider.get
        	hl.setHyperlinkRegion(new Region(node.offset,node.length));
        	hl.setHyperlinkText("Open included file");
        	var uri = URI.createURI(op.script)
        	hl.setURI(uri)
        	acceptor.accept(hl)
	    }
    }
        
}